﻿using System.Web;
using System.Web.Optimization;

namespace GCSOFT.MVC.AGENCY
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/login/css").Include(
               "~/Scripts/cms/vendor/uikit/css/uikit_almost_flat.css"
               , "~/Content/cms/css/login_page.css"
               , "~/Scripts/cms/vendor/ValidationEngine/validationEngine.css"
               , "~/Scripts/cms/vendor/ToastMessage/resources/css/toastmessage.css"
               ));

            bundles.Add(new ScriptBundle("~/login/js").Include(
                "~/Scripts/cms/js/common.js"
                , "~/Scripts/cms/js/uikit_custom.js"
                , "~/Scripts/cms/js/altair_admin_common.js"
                , "~/Scripts/cms/vendor/ValidationEngine/validationEngine.js"
                , "~/Scripts/cms/vendor/ValidationEngine/validationEngine-vi.js"
                , "~/Scripts/cms/vendor/ToastMessage/javascript/toastmessage.js"
                ));

            bundles.Add(new StyleBundle("~/cms/css").Include(
                "~/Scripts/cms/vendor/uikit/css/uikit_almost_flat.css"
                , "~/Content/cms/css/main.css"
                , "~/Content/cms/css/gcmain.css"
                , "~/Scripts/cms/vendor/ddmenu/ddsmoothmenu.css"
                , "~/Scripts/cms/vendor/select2/css/select2.css"
                , "~/Scripts/cms/vendor/jquery-ui/jquery-ui.css"
                , "~/Scripts/cms/vendor/jqGrid/css/jqgrid.css"
                 , "~/Scripts/cms/vendor/ValidationEngine/validationEngine.css"
                , "~/Scripts/cms/vendor/ToastMessage/resources/css/toastmessage.css"
                , "~/Scripts/cms/vendor/Progress/nprogress.css"
                , "~/Scripts/cms/vendor/fancybox-2/fancybox.css"
                , "~/Scripts/cms/vendor/dropify/css/dropify.css"
                , "~/Scripts/cms/vendor/toast/toast.css"
                ));

            bundles.Add(new ScriptBundle("~/cms/js").Include(
                "~/Scripts/cms/js/common.js"
                , "~/Scripts/cms/js/uikit_custom.js"
                , "~/Scripts/cms/js/altair_admin_common.js"
                , "~/Scripts/cms/js/hotkeys.js"
                , "~/Scripts/cms/vendor/ddmenu/ddsmoothmenu.js"
                , "~/Scripts/cms/vendor/select2/js/select2.js"
                , "~/Scripts/cms/vendor/inputmask/inputmask.js"
                , "~/Scripts/cms/vendor/ckeditor/ckeditor.js"
                , "~/Scripts/cms/vendor/jqGrid/js/i18n/gridlocale-vi.js"
                , "~/Scripts/cms/vendor/jqGrid/js/jqGrid.js"
                , "~/Scripts/cms/vendor/ValidationEngine/validationEngine.js"
                , "~/Scripts/cms/vendor/ValidationEngine/validationEngine-vi.js"
                , "~/Scripts/cms/vendor/ToastMessage/javascript/toastmessage.js"
                , "~/Scripts/cms/vendor/fancybox-2/fancybox.js"
                , "~/Scripts/cms/vendor/Progress/nprogress.js"
                , "~/Scripts/cms/vendor/dropify/js/dropify.js"
                , "~/Scripts/cms/vendor/AjaxFileUploader/JSubmit.js"
                , "~/Scripts/cms/vendor/toast/toast.js"
                , "~/Scripts/cms/js/handlebars.js"
                , "~/Scripts/cms/js/gc-common.js"
                , "~/Scripts/cms/js/Jq-common.js"
                ));
            BundleTable.EnableOptimizations = false;
        }
    }
}
