﻿using GCSOFT.MVC.Service.SystemService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers
{
    public class DashboardController : BaseController
    {
        public DashboardController(IVuViecService vuViecService) : base(vuViecService)
        {
        }

        // GET: Administrator/Dashboard
        public ActionResult Index()
        {
            if (GetUser() == null)
                return RedirectToAction("Index", "Login", new { area = "administrator" });
            return View();
        }
    }
}