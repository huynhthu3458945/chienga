﻿using AutoMapper;
using Dapper;
using GCSOFT.MVC.AGENCY.Areas.Agency.Data;
using GCSOFT.MVC.AGENCY.ViewModels.HienTai;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers
{
    public class LoginController : BaseController
    {
        private readonly IUserService _userService;
        private readonly ICongViecService _congViecService;
        private readonly IAgencyService _agencyService;
        private readonly IUserThuocNhomService _userThuocNhomService;
        private string hienTaiConnection = ConfigurationManager.ConnectionStrings["HienTaiConnection"].ConnectionString;
        private UserLoginVMs userVMs;
        public LoginController
            (
                IVuViecService vuViecService
                , IUserService userService
                , ICongViecService congViecService
                , IAgencyService agencyService
                , IUserThuocNhomService userThuocNhomService
            )
            : base(vuViecService)
        {
            _userService = userService;
            _congViecService = congViecService;
            _agencyService = agencyService;
            _userThuocNhomService = userThuocNhomService;
        }

        #region -- Get Action --
        public ActionResult Index()
        {
            //Session.Remove("User");
            Session["CategoryByAgency"] = null;
            Session.Remove("CategoryByAgency");
            if (System.Web.HttpContext.Current.Request.Cookies["AgencyUser"] != null)
            {
                HttpCookie cookie = this.ControllerContext.HttpContext.Request.Cookies["AgencyUser"];
                cookie.Expires = DateTime.Now.AddDays(-1);
                this.ControllerContext.HttpContext.Response.Cookies.Add(cookie);
            }
            return View();
        }

        public ActionResult Logout(string url)
        {
            Session["CategoryByAgency"] = null;
            Session.Remove("CategoryByAgency");
            if (System.Web.HttpContext.Current.Request.Cookies["AgencyUser"] != null)
            {
                HttpCookie cookie = this.ControllerContext.HttpContext.Request.Cookies["AgencyUser"];
                cookie.Expires = DateTime.Now.AddDays(-1);
                this.ControllerContext.HttpContext.Response.Cookies.Add(cookie);
            }
            return RedirectToAction("Index", "Login", new { area = "administrator", url = url });
        }

        public ActionResult AccessDenied()
        {
            return View();
        }
        #endregion

        #region -- Post Action --
        /// <summary>
        /// 
        /// </summary>
        /// <param name="u">Username</param>
        /// <param name="p">Password</param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1054:UriParametersShouldNotBeStrings",
            Justification = "Needs to take same parameter type as Controller.Redirect()")]
        public ActionResult Index(string u, string p, string url)
        {
            if (!ValidateLogOn(u, p))
            {
                ViewBag.InValid = "1";
                ViewBag.Url = url;
                return View();
            }
            //-- add to Cookie User
            HttpCookie userCookie = new HttpCookie("AgencyUser", Server.UrlEncode(JsonConvert.SerializeObject(userVMs)));
            userCookie.Expires = DateTime.Now.AddYears(8);
            System.Web.HttpContext.Current.Response.Cookies.Add(userCookie);
            //-- add to Cookie User + 

            if (GetUser() == null)
                return RedirectToAction("Index", "Login", new { area = "administrator" });
            //if (!string.IsNullOrEmpty(url))
            //    return Redirect(url);
            return RedirectToAction("Index", "Dashboard", new { area = "administrator" });
        }
        #endregion

        #region -- Functions --
        private bool ValidateLogOn(string userName, string password)
        {
            var cryptor = CryptorEngine.Encrypt(password, true, "Hieu.Le-GC.Soft");
            userVMs = Mapper.Map<UserLoginVMs>(_userService.Get(userName, cryptor));
            if (userVMs == null)
                return false;
            var userThuocNhoms = Mapper.Map<IEnumerable<UserThuocNhomVM>>(_userThuocNhomService.GetMany(userVMs.ID)) ?? new List<UserThuocNhomVM>();
            var userGroups = userThuocNhoms.Select(d => d.UserGroupCode).ToList();
            userVMs.agencyInfo = Mapper.Map<AgencyList>(_agencyService.GetByUserName(userName));
            if (userGroups.Contains("AGEN_ACCOUNTING"))
            {
                userVMs.agencyInfo.Id = userVMs.agencyInfo.ParentAgencyId ?? 0;
            }
            userVMs.teacherInfo = GetTecherInfo(userName);
            if (userVMs.teacherInfo == null)
                userVMs.teacherInfo = new O_TeacherInfo();
            return true;
        }
        private O_TeacherInfo GetTecherInfo(string userName)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "GETBYUserName");
                    paramaters.Add("@Username", userName);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var results = conn.Query<O_TeacherInfo>("SP2_O_Teacher", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    var a = results.FirstOrDefault();
                    return a;
                }
            }
            catch { return new O_TeacherInfo(); }
        }
        #endregion
    }
}