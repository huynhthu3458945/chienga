﻿using AutoMapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Areas.Agency.Data;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GCSOFT.MVC.AGENCY.Helper;
using OfficeOpenXml;
using GCSOFT.MVC.AGENCY.Areas.Transaction.Data;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using GCSOFT.MVC.Model.SystemModels;
using GCSOFT.MVC.AGENCY.Areas.Transaction.Controllers;

namespace GCSOFT.MVC.AGENCY.Areas.Agency.Controllers
{
    [CompressResponseAttribute]
    public class AgencyController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IAgencyService _agencyService;
        private readonly IPackageService _packageService;
        private readonly IProvinceService _provinceService;
        private readonly IDistrictService _districtService;
        private readonly IAgencyCardService _agencyCardService;
        private readonly IUserService _userService;
        private readonly IUserThuocNhomService _userThuocNhomService;
        private readonly IMailSetupService _mailSetupService;
        private readonly ITemplateService _templateService;
        private string sysCategory = "AGEN_AGENCY";
        private bool? permission = false;
        private AgencyCard agencyCard;
        private M_Agency mAgency;
        private string hasValue;
        #endregion

        #region -- Contructor --
        public AgencyController
            (
                IVuViecService vuViecService
                , IAgencyService agencyService
                , IPackageService packageService
                , IProvinceService provinceService
                , IDistrictService districtService
                , IAgencyCardService agencyCardService
                , IUserService userService
                , IUserThuocNhomService userThuocNhomService
                , IMailSetupService mailSetupService
                , ITemplateService templateService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _agencyService = agencyService;
            _packageService = packageService;
            _provinceService = provinceService;
            _districtService = districtService;
            _agencyCardService = agencyCardService;
            _userService = userService;
            _userThuocNhomService = userThuocNhomService;
            _mailSetupService = mailSetupService;
            _templateService = templateService; 
        }
        #endregion
        public void DownloadCardNo(string s)
        {
            var userInfo = GetUser();
            var cards = Mapper.Map<IEnumerable<AgencyAndCard>>(_agencyCardService.FindAll(userInfo.agencyInfo.Id, s, null));
            ExcelPackage Ep = new ExcelPackage();
            ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("Thong Tin");
            Sheet.Cells["A1"].Value = "STT";
            Sheet.Cells["B1"].Value = "Số Seri";
            Sheet.Cells["C1"].Value = "Mã Kích Hoạt";
            Sheet.Cells["D1"].Value = "Giá";
            Sheet.Cells["E1"].Value = "Trạng Thái";
            Sheet.Cells["F1"].Value = "Ngày Cập Nhật";
            int row = 2;
            int stt = 0;
            string _cardNo = "";
            foreach (var item in cards)
            {
                _cardNo = "";
                stt += 1;
                Sheet.Cells[string.Format("A{0}", row)].Value = stt.ToString();
                Sheet.Cells[string.Format("B{0}", row)].Value = item.SerialNumber;
                if (!string.IsNullOrEmpty(item.CardNo))
                {
                    _cardNo = CryptorEngine.Decrypt(item.CardNo, true, "TTLSTNHD@CARD");
                }
                Sheet.Cells[string.Format("C{0}", row)].Value = _cardNo;
                Sheet.Cells[string.Format("D{0}", row)].Value = item.Price.ToString();
                Sheet.Cells[string.Format("E{0}", row)].Value = item.StatusName;
                Sheet.Cells[string.Format("F{0}", row)].Value = string.Format("{0:dd/MM/yyyy}", item.LastDateUpdate);
                row++;
            }
            Sheet.Cells["A:AZ"].AutoFitColumns();
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment: filename=" + "CardInfo.xlsx");
            Response.BinaryWrite(Ep.GetAsByteArray());
            Response.End();
        }
        public ActionResult Index()
        {
            try
            {
                #region -- Role user --
                permission = GetPermission(sysCategory, Authorities.View);
                if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
                if (!permission.Value) return View("AccessDenied");
                #endregion
                var agencyList = Mapper.Map<IEnumerable<AgencyList>>(_agencyService.FindAll(AgencyType.Agency, GetUser().agencyInfo.Id));
                return View(agencyList);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
            
        }
        public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            agencyCard = new AgencyCard();
            agencyCard.RegistrationDate = DateTime.Now;
            agencyCard.RegistrationDateStr = string.Format("{0:dd/MM/yyyy}", agencyCard.RegistrationDate);
            agencyCard.ParentAgencyId = GetUser().agencyInfo.Id;
            agencyCard.CityList = Mapper.Map<IEnumerable<ProvinceViewModel>>(_provinceService.FindAll()).ToSelectListItems(0);
            agencyCard.DistrictList = Mapper.Map<IEnumerable<DistrictViewModel>>(_districtService.FindAll(0)).ToSelectListItems(0);
            agencyCard.Legal = "Cá Nhân";
            ViewBag.hasAccount = false;
            ViewBag.IsAccounting = "";
            ViewBag.username = GetUser().Username;
            return View(agencyCard);
        }

        [HttpPost]
        public ActionResult Create(AgencyCard _agencyCard)
        {
            try
            {
                SetData(_agencyCard, true);
                hasValue = _agencyService.Insert(mAgency);
                hasValue = _vuViecService.Commit();
                CreateAgency(mAgency.Id, mAgency.UserName, "SUBAGENCY", agencyCard);
                if (string.IsNullOrEmpty(hasValue))
                {
                    ResendPassForAgency(agencyCard);
                    return RedirectToAction("Edit", new { id = mAgency.Id, msg = "1" });
                }
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        public ActionResult Edit(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            agencyCard = Mapper.Map<AgencyCard>(_agencyService.GetBy(id));
            if (agencyCard == null)
                return HttpNotFound();
            ViewBag.hasAccount = agencyCard.HasAccount;
            agencyCard.xUserName = agencyCard.UserName;
            agencyCard.CityList = Mapper.Map<IEnumerable<ProvinceViewModel>>(_provinceService.FindAll()).ToSelectListItems(agencyCard.CityId ?? 0);
            agencyCard.DistrictList = Mapper.Map<IEnumerable<DistrictViewModel>>(_districtService.FindAll(agencyCard.CityId ?? 0)).ToSelectListItems(agencyCard.DistrictId ?? 0);
            agencyCard.RegistrationDateStr = string.Format("{0:dd/MM/yyyy}", agencyCard.RegistrationDate);
            agencyCard.DateBlockStr = string.Format("{0:dd/MM/yyyy}", agencyCard.DateBlock);
            ViewBag.IsAccounting = "disabled";
            ViewBag.username = GetUser().Username;
            return View(agencyCard);
        }

        [HttpPost]
        public ActionResult Edit(AgencyCard _agencyCard)
        {
            try
            {
                SetData(_agencyCard, false);
                hasValue = _agencyService.Update(mAgency);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = mAgency.Id, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        public ActionResult Deletes(string id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                int[] _codes = id.Split(',').Select(item => int.Parse(item)).ToArray();
                var teacherImgs = _agencyService.FindAll(_codes).Where(d => !string.IsNullOrEmpty(d.Image)).ToList();

                hasValue = _agencyService.Delete(_codes);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                {
                    UploadHelper _upload = new UploadHelper();
                    string pathImgTeacher = HttpContext.Server.MapPath("~/Files/Agencry/");
                    teacherImgs.ForEach(item =>
                    {
                        _upload.DeleteFileOnServer(pathImgTeacher + item.Id.ToString() + "/", item.Image);
                    });
                    //-- Delete Foloder
                    if (Directory.Exists(pathImgTeacher + teacherImgs.Select(d => d.Id).FirstOrDefault()))
                        Directory.Delete(pathImgTeacher + teacherImgs.Select(d => d.Id).FirstOrDefault(), true);
                    return Json(new { v = true, count = _codes.Count() });
                }
                return Json("Xóa dữ liệu không thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }
        public JsonResult RemoveImg(int id, string img)
        {
            hasValue = _agencyService.DeleteImg(id);
            hasValue = _vuViecService.Commit();
            if (string.IsNullOrEmpty(hasValue))
            {
                UploadHelper _upload = new UploadHelper();
                string PathSave = HttpContext.Server.MapPath("~/Files/Agencry/" + id.ToString() + "/");
                _upload.DeleteFileOnServer(PathSave, img);
            }
            return Json(string.IsNullOrEmpty(hasValue));
        }
        private void SetData(AgencyCard _agencyCard, bool isCreate)
        {
            agencyCard = _agencyCard;
            agencyCard.AgencyType = AgencyType.Agency;
            agencyCard.HasAccount = true;
            agencyCard.IsFirstLogin = true;
            agencyCard.DateCreate = DateTime.Now;
            if (isCreate)
                agencyCard.Id = _agencyService.GetID();
            if (!string.IsNullOrEmpty(agencyCard.RegistrationDateStr))
                agencyCard.RegistrationDate = DateTime.Parse(StringUtil.ConvertStringToDate(agencyCard.RegistrationDateStr));
            else
                agencyCard.RegistrationDate = null;
            if (!string.IsNullOrEmpty(agencyCard.DateBlockStr))
                agencyCard.DateBlock = DateTime.Parse(StringUtil.ConvertStringToDate(agencyCard.DateBlockStr));
            else
                agencyCard.DateBlock = null;
            var provinceInfo = Mapper.Map<ProvinceViewModel>(_provinceService.GetBy(agencyCard.CityId ?? 0)) ?? new ProvinceViewModel();
            agencyCard.CityName = provinceInfo.name;
            var districtInfo = Mapper.Map<DistrictViewModel>(_districtService.GetBy(agencyCard.DistrictId ?? 0)) ?? new DistrictViewModel();
            agencyCard.DistrictName = districtInfo.name;
            UploadHelper _uploadHelper = new UploadHelper();
            _uploadHelper.PathSave = HttpContext.Server.MapPath("~/Files/Agencry/" + agencyCard.Id.ToString() + "/");
            _uploadHelper.UploadFile(Request.Files["fileUpload"]);
            if (!string.IsNullOrEmpty(_uploadHelper.FileSave))
                agencyCard.Image = _uploadHelper.FileSave;

            mAgency = Mapper.Map<M_Agency>(agencyCard);
        }
        public ActionResult LegalForm(int id, string type)
        {
            var _agencyInfo = Mapper.Map<AgencyCard>(_agencyService.GetBy(id)) ?? new AgencyCard();
            _agencyInfo.CityList = Mapper.Map<IEnumerable<ProvinceViewModel>>(_provinceService.FindAll()).ToSelectListItems(0);
            _agencyInfo.DistrictList = Mapper.Map<IEnumerable<DistrictViewModel>>(_districtService.FindAll(0)).ToSelectListItems(0);
            if (type.Equals("Cá Nhân"))
                return PartialView("_personal", _agencyInfo);
            else
                return PartialView("_Company", _agencyInfo);
        }
        public string CreateAgency(int id, string u, string g, AgencyCard agencyInfo)
        {
            try
            {
                RandomNumberGenerator randomNumber = new RandomNumberGenerator();
                var _pass = randomNumber.RandomNumber(10000, 99999);
                var userCard = new UserCard()
                {
                    ID = _userService.GetID(),
                    Password = CryptorEngine.Encrypt(_pass.ToString(), true, "Hieu.Le-GC.Soft"),
                    Username = u,
                    Email = agencyInfo.Email,
                    Phone = agencyInfo.Phone,
                    Description = agencyInfo.PackageName,
                    Position = agencyInfo.LeavelName,
                    FirstName = agencyInfo.FirstName,
                    LastName = agencyInfo.LastName,
                    FullName = agencyInfo.FullName,
                    Address = agencyInfo.Address
                };
                hasValue = _userService.Insert2(Mapper.Map<HT_Users>(userCard));
                hasValue = _vuViecService.Commit();
                var userInfo = Mapper.Map<UserCard>(_userService.GetBy(userCard.Username));
                var _userThuocNhom = new UserThuocNhomVM()
                {
                    UserID = userInfo.ID,
                    UserGroupCode = g
                };
                hasValue = _userThuocNhomService.Insert2(Mapper.Map<HT_UserThuocNhom>(_userThuocNhom));
                hasValue = _vuViecService.Commit();
                return hasValue;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public string ResendPassForAgency(AgencyCard agencyInfo)
        {
            var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
            var mailTemplate = Mapper.Map<Template>(_templateService.Get("AGENCYINFO"));
            var userInfo = Mapper.Map<UserLoginVMs>(_userService.GetBy(agencyInfo.UserName));
            var emailTos = new List<string>();
            emailTos.Add(agencyInfo.Email);
            var _mailHelper = new EmailHelper()
            {
                EmailTos = emailTos,
                DisplayName = emailInfo.DisplayName,
                Title = mailTemplate.Title,
                Body = mailTemplate.Content.Replace("{{Name}}", agencyInfo.FullName).Replace("{{Username}}", agencyInfo.UserName).Replace("{{Password}}", CryptorEngine.Decrypt(userInfo.Password, true, "Hieu.Le-GC.Soft")),
                MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
            };
            string hasValue = _mailHelper.DoSendMail();
            //-- Send SMS
            var content = string.Format("5 Phut Thuoc Bai gui A/C tai khoan doi tac, duong dan he thong https://agency.stnhd.com , Ten Dang Nhap: {0} , Mat Khau {1} vui long kiem tra email de nhan thong tin chi tiet.", agencyInfo.UserName, CryptorEngine.Decrypt(userInfo.Password, true, "Hieu.Le-GC.Soft"));
            var smsHelper = new SMSHelper()
            {
                PhoneNo = agencyInfo.Phone,
                Content = content
            };
            if (smsHelper.IsMobiphone())
            {
                var smsTemplate = Mapper.Map<Template>(_templateService.Get("MOBI_TK_DAILY"));
                smsHelper.Content = string.Format(smsTemplate.SMSContent, agencyInfo.UserName, CryptorEngine.Decrypt(userInfo.Password, true, "Hieu.Le-GC.Soft"));
                hasValue = smsHelper.SendMobiphone();
            }
            else
                hasValue = smsHelper.Send();
            //-- Send SMS +
            return "";
        }

        /// <summary>
        /// Đổi thành cộng tác viên
        /// </summary>
        /// <param name="id">Id Đại Lý</param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [27/09/2021]
        /// </history>
        public JsonResult ChangePartner(string userName)
        {
            var user = _userService.GetBy(userName);
            var model = _userThuocNhomService.GetMany(user.ID).FirstOrDefault();
            hasValue = _userThuocNhomService.Delete(user.ID);
            model.UserGroupCode = "PARTNER";
            hasValue = _userThuocNhomService.Insert2(model);
            var agenncy = _agencyService.GetByUserName(userName);
            agenncy.AgencyType = AgencyType.Partner; // Cộng tác viên
            _agencyService.Update(agenncy);
            _vuViecService.Commit();
            return Json(string.IsNullOrEmpty(hasValue) ? "Đã đổi thành công tác viên!" : hasValue, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Delete(int id)
        {
            var ids = new List<int>();
            ids.Add(id);
            hasValue = _agencyService.HasDelete(ids.ToArray());
            if (string.IsNullOrEmpty(hasValue))
            {
                var agencyInfo = Mapper.Map<AgencyCard>(_agencyService.GetBy2(id));
                hasValue = _agencyService.Delete(ids.ToArray());
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                {
                    var userInfo = Mapper.Map<UserVM>(_userService.GetBy(agencyInfo.UserName));
                    ids = new List<int>();
                    ids.Add(userInfo.ID);
                    hasValue = _userService.Delete(ids.ToArray());
                }
            }
            return Json(new { hasValue = string.IsNullOrEmpty(hasValue) ? "oke" : hasValue }, JsonRequestBehavior.AllowGet);
        }
    }
}