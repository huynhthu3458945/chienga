﻿using AutoMapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Areas.Agency.Data;
using GCSOFT.MVC.AGENCY.Areas.Transaction.Data;
using GCSOFT.MVC.AGENCY.Helper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.StoreProcedue;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.StoreProcedure.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.Agency.Controllers
{
    [CompressResponseAttribute]
    public class CardHistoryController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IStoreProcedureService _storeService;
        private readonly ICardLineService _cardLineService;
        private readonly ICardEntryService _cardEntryService;
        private readonly IAgencyCardService _agencyAndCardService;
        private readonly IOptionLineService _optionLineService;
        private string sysCategory = "CARDHISTORY";
        private bool? permission = false;
        private string hasValue;
        #endregion

        #region -- Contructor --
        public CardHistoryController
            (
                IVuViecService vuViecService
                , IStoreProcedureService storeService
                , ICardLineService cardLineService
                , ICardEntryService cardEntryService
                , IAgencyCardService agencyAndCardService
                , IOptionLineService optionLineService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _storeService = storeService;
            _cardLineService = cardLineService;
            _cardEntryService = cardEntryService;
            _agencyAndCardService = agencyAndCardService;
            _optionLineService = optionLineService;
        }
        #endregion
        public ActionResult Index(int? p, CardHistoryPage c)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var userInfo = GetUser();
            c.AgencyId = userInfo.agencyInfo.Id;
            var cardHistory = c;

            var fromDate = new DateTime();
            var toDate = new DateTime();
            if (string.IsNullOrEmpty(c.fd))
            {
                fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                toDate = fromDate.AddMonths(1).AddDays(-1);
                cardHistory.Paging = new Paging();
                cardHistory.CardSoldByAgencys = new List<SpCardSoldByAgency>();
            }
            else
            {
                fromDate = DateTime.Parse(StringUtil.ConvertStringToDate(c.fd));
                toDate = DateTime.Parse(StringUtil.ConvertStringToDate(c.td));

                string CardNo2 = "";
                if (!string.IsNullOrEmpty(c.CardNo))
                    CardNo2 = CryptorEngine.Encrypt(c.CardNo, true, "TTLSTNHD@CARD");
                int pageString = p ?? 0;
                int page = pageString == 0 ? 1 : pageString;
                int totalRecord = 0;
                //if (c.DateType.Equals("BUY"))
                    totalRecord = _storeService.SP_CountCardSoldByAgency(c.AgencyId, c.FullName ?? "", c.PhoneNo ?? "", c.Email ?? "", CardNo2 ?? "", c.SeriNumber ?? "").FirstOrDefault().NoOfNumber;
                //else
                //    totalRecord = _storeService.SP_CountCardSoldByAgencyActive(c.AgencyId, c.FullName ?? "", c.PhoneNo ?? "", c.Email ?? "", CardNo2 ?? "", c.SeriNumber ?? "").FirstOrDefault().NoOfNumber;
                cardHistory.Paging = new Paging(totalRecord, page);
                //if (c.DateType.Equals("BUY"))
                    cardHistory.CardSoldByAgencys = _storeService.SP_CardSoldByAgency(c.AgencyId, c.FullName ?? "", c.PhoneNo ?? "", c.Email ?? "", CardNo2 ?? "", c.SeriNumber ?? "", cardHistory.Paging.start, cardHistory.Paging.offset, fromDate, toDate, "PAGING");
                //else
                //    cardHistory.CardSoldByAgencys = _storeService.SP_CardSoldByAgencyAcitve(c.AgencyId, c.FullName ?? "", c.PhoneNo ?? "", c.Email ?? "", CardNo2 ?? "", c.SeriNumber ?? "", cardHistory.Paging.start, cardHistory.Paging.offset, fromDate, toDate, "PAGING");
            }
            cardHistory.FromDate = fromDate;
            cardHistory.ToDate = toDate;
          //cardHistory.DateTypeList = CboStatus().ToSelectListItems(c.DateType);
            return View(cardHistory);
        }
        public Dictionary<string, string> CboStatus()
        {
            Dictionary<string, string> dics = new Dictionary<string, string>();
            dics["BUY"] = "Ngày Bán";
            dics["ACTIVE"] = "Ngày Khách Kích Hoạt";
            return dics;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="s">Seri Number</param>
        /// <param name="ai">Agency Id</param>
        /// <returns></returns>
        public JsonResult RecallCard(int ai, string s)
        {
            try
            {
                var userInfo = GetUser();
                var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("CARD", "100")); //Cập nhật trạng thái đã bán mã kích hoạt
                //var agencyAndCard = Mapper.Map<AgencyAndCard>(_agencyAndCardService.GetByStatus(ai, s));
                var agencyAndCard = Mapper.Map<AgencyAndCard>(_agencyAndCardService.GetByStatus(s, "450"));
                agencyAndCard.CardNo = null;
                agencyAndCard.PhoneNo = null;
                agencyAndCard.FulName = null;
                agencyAndCard.VerifyInfo = null;
                agencyAndCard.UserNameActive = userInfo.Username;
                agencyAndCard.UserActiveFullName = userInfo.FullName;
                agencyAndCard.LastDateUpdate = DateTime.Now;
                agencyAndCard.StatusId = statusInfo.LineNo;
                agencyAndCard.StatusCode = statusInfo.Code;
                agencyAndCard.StatusName = statusInfo.Name;

                statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("CARD", "250")); //Cập nhật trạng thái thu hồi
                var cardLine = Mapper.Map<CardLine>(_cardLineService.GetBySerialNumber(s));
                cardLine.LastStatusId = statusInfo.LineNo;
                cardLine.LastStatusCode = statusInfo.Code;
                cardLine.LastStatusName = statusInfo.Name;

                var cardEntry = new CardEntry()
                {
                    LotNumber = cardLine.LotNumber,
                    CardLineId = cardLine.Id,
                    SerialNumber = cardLine.SerialNumber,
                    CardNo = cardLine.CardNo,
                    Price = cardLine.Price,
                    UserName = cardLine.UserName,
                    UserFullName = cardLine.UserFullName,
                    DateCreate = cardLine.DateCreate,

                    UserType = UserType.Agency,
                    UserNameActive = userInfo.Username,
                    UserActiveFullName = userInfo.FullName,
                    Date = agencyAndCard.LastDateUpdate,
                    StatusId = statusInfo.LineNo,
                    StatusCode = statusInfo.Code,
                    StatusName = statusInfo.Name,
                    Source = CardSource.AgencyBuyActivation,
                    SourceNo = cardLine.SerialNumber,
                    AgencyId = userInfo.agencyInfo.Id,
                    AgencyName = userInfo.agencyInfo.FullName
                };
                hasValue = _cardLineService.Update(Mapper.Map<M_CardLine>(cardLine));
                hasValue = _agencyAndCardService.Update(Mapper.Map<M_AgencyAndCard>(agencyAndCard));
                hasValue = _cardEntryService.Insert(Mapper.Map<M_CardEntry>(cardEntry));
                hasValue = _vuViecService.Commit();
                var _status = string.IsNullOrEmpty(hasValue) ? "Oke" : "Error";
                var _msg = string.IsNullOrEmpty(hasValue) ? "" : hasValue;
                return Json(new { s = _status, m = _msg }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex) {
                return Json(new { s = "Error", m = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}