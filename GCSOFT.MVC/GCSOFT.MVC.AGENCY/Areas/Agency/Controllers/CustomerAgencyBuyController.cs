﻿using AutoMapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Areas.Agency.Data;
using GCSOFT.MVC.AGENCY.Areas.Transaction.Data;
using GCSOFT.MVC.AGENCY.Helper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.StoreProcedue;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.ExeclOfficeEngine;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.StoreProcedure.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.Agency.Controllers
{
    [CompressResponseAttribute]
    public class CustomerAgencyBuyController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IStoreProcedureService _storeService;
        private readonly ICardLineService _cardLineService;
        private readonly ICardEntryService _cardEntryService;
        private readonly IAgencyCardService _agencyAndCardService;
        private readonly IOptionLineService _optionLineService;
        private readonly IAgencyService _agencyService;
        private readonly IHistoryRecall _historyRecall;
        private string sysCategory = "AGENCYCUSTBUY";
        private bool? permission = false;
        private string hasValue;
        #endregion

        #region -- Contructor --
        public CustomerAgencyBuyController
            (
                IVuViecService vuViecService
                , IStoreProcedureService storeService
                , ICardLineService cardLineService
                , ICardEntryService cardEntryService
                , IAgencyCardService agencyAndCardService
                , IOptionLineService optionLineService
                , IAgencyService agencyService
                , IHistoryRecall historyRecall
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _storeService = storeService;
            _cardLineService = cardLineService;
            _cardEntryService = cardEntryService;
            _agencyAndCardService = agencyAndCardService;
            _optionLineService = optionLineService;
            _agencyService = agencyService;
            _historyRecall = historyRecall;
        }
        #endregion
        public ActionResult Index(CustomerAgencyBuyPage c)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var userInfo = GetUser();
            c.AgencyId = c.AgencyId ?? userInfo.agencyInfo.Id;
            var cardHistory = c;

            var fromDate = new DateTime();
            var toDate = new DateTime();
            if (string.IsNullOrEmpty(c.fd))
            {
                fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                toDate = fromDate.AddMonths(1).AddDays(-1);
                cardHistory.CustAgencyList = new List<SpCustomerAgencyBuy>();
            }
            else
            {
                fromDate = DateTime.Parse(StringUtil.ConvertStringToDate(c.fd));
                toDate = DateTime.Parse(StringUtil.ConvertStringToDate(c.td));

                cardHistory.CustAgencyList = _storeService.SP_CustomerAgencyBuy(c.AgencyId ?? 0, c.SeriNumber ?? "", c.FullName ?? "", c.PhoneNo ?? "", c.Email ?? "", c.LevelCode ?? "", fromDate, toDate);
            }
            cardHistory.FromDate = fromDate;
            cardHistory.ToDate = toDate;
            cardHistory.LevelList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("LV")).ToSelectListItems(c.LevelCode ?? "");
            cardHistory.AgencyDDL = _storeService.AgencyParentFindAll("FINDALL", GetUser().agencyInfo.Id).ToSelectListItems(c.AgencyId ?? 0);
            return View(cardHistory);
        }

        /// <summary>
        ///     Download báo cáo bán hàng
        /// </summary>
        /// <param name="p"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [16/11/2021]
        /// </history>
        public ActionResult DownloadCustomerAgencyBuy(CustomerAgencyBuyPage c)
        {
            var userInfo = GetUser();
            c.AgencyId = c.AgencyId ?? userInfo.agencyInfo.Id;
            var cardHistory = c;

            var fromDate = new DateTime();
            var toDate = new DateTime();
            if (string.IsNullOrEmpty(c.fd))
            {
                fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                toDate = fromDate.AddMonths(1).AddDays(-1);
                cardHistory.CustAgencyList = new List<SpCustomerAgencyBuy>();
            }
            else
            {
                fromDate = DateTime.Parse(StringUtil.ConvertStringToDate(c.fd));
                toDate = DateTime.Parse(StringUtil.ConvertStringToDate(c.td));

                cardHistory.CustAgencyList = _storeService.SP_CustomerAgencyBuy(c.AgencyId ?? 0, c.SeriNumber ?? "", c.FullName ?? "", c.PhoneNo ?? "", c.Email ?? "", c.LevelCode ?? "", fromDate, toDate);
            }
            cardHistory.FromDate = fromDate;
            cardHistory.ToDate = toDate;
            cardHistory.LevelList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("LV")).ToSelectListItems(c.LevelCode ?? "");
            cardHistory.AgencyDDL = _storeService.AgencyParentFindAll("FINDALL", GetUser().agencyInfo.Id).ToSelectListItems(c.AgencyId ?? 0);
            foreach (var item in cardHistory.CustAgencyList)
            {
                item.Seri = $"{item.SerialNumber}\nGiá: {string.Format("{0:#,##0}", item.Price)}\nNgày bán: {item.DateCreate}";
                item.NguoiBan = $"{item.SellBy}\nĐT: {item.AgencyPhone}\nEmail:{item.AgencyEmail}\nĐịa chỉ: {@item.Address} {@item.DistrictName}, {item.CityName}";
                item.NguoiMua = $"{item.FulName}\nĐT: {item.PhoneNo}\nEmail: {item.Email}";
                item.LoaiThe = $"{item.LevelName}\nThời hạn: {item.DurationName}\nTrạng thái: {item.StatusName}\nKích hoạt: {item.TrangThaiThe}";
            }
            var res = cardHistory.CustAgencyList;
            var dataModel = res.ToList().ConvertToDataTable();
            dataModel.TableName = "Model";
            var stream = Excel.ExportReport(dataModel, HttpContext.Server.MapPath("/Areas/Agency/Report/CustomerAgencyBuy.xlsx"), false, OfficeType.XLSX);
            return File(stream, OfficeContentType.XLSX, "CustomerAgencyBuy.xlsx");
        }

        public Dictionary<string, string> CboStatus()
        {
            Dictionary<string, string> dics = new Dictionary<string, string>();
            dics["BUY"] = "Ngày Bán";
            dics["ACTIVE"] = "Ngày Khách Kích Hoạt";
            return dics;
        }
        public ActionResult PopupRecallCard(int agencyId, string seriNumber)
        {
            var model = new M_HistoryRecall();
            model.Serinumber = seriNumber;
            model.AgencyId = agencyId;
            model.AgencyName = _agencyService.GetBy(agencyId).FullName;
            model.CardNo = CryptorEngine.Decrypt(_agencyAndCardService.GetBy(agencyId, seriNumber).CardNo, true, "TTLSTNHD@CARD");
            model.DateRecall = DateTime.Now.Date;
            return PartialView("_PopupRecallCard", model);
        }
        [HttpPost]
        public ActionResult PopupRecallCard(M_HistoryRecall model)
        {
            if (ModelState.IsValid)
            {
                if (RecallCard(model.AgencyId, model.Serinumber))
                {
                    model.DateRecall = DateTime.Now.Date;
                    _historyRecall.Insert(model);
                    _vuViecService.Commit();
                }
            }
            return Json("oke");
        }
        public bool RecallCard(int ai, string s)
        {
            string hasValue = string.Empty;
            try
            {
                var userInfo = GetUser();
                var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("CARD", "900")); //Cập nhật trạng thái hủy mã kích hoạt
                var agencyAndCard = Mapper.Map<AgencyAndCard>(_agencyAndCardService.GetByStatus(s, "450"));
                agencyAndCard.CardNo = null;
                //agencyAndCard.PhoneNo = null;
                //agencyAndCard.FulName = null;
                //agencyAndCard.VerifyInfo = null;
                agencyAndCard.UserNameActive = userInfo.Username;
                agencyAndCard.UserActiveFullName = userInfo.FullName;
                agencyAndCard.LastDateUpdate = DateTime.Now;
                agencyAndCard.StatusId = statusInfo.LineNo;
                agencyAndCard.StatusCode = statusInfo.Code;
                agencyAndCard.StatusName = statusInfo.Name;

                //statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("CARD", "250")); //Cập nhật trạng thái thu hồi
                var cardLine = Mapper.Map<CardLine>(_cardLineService.GetBySerialNumber(s));
                cardLine.LastStatusId = statusInfo.LineNo;
                cardLine.LastStatusCode = statusInfo.Code;
                cardLine.LastStatusName = statusInfo.Name;
                cardLine.LastDateUpdate = DateTime.Now;
                var cardEntry = new CardEntry()
                {
                    LotNumber = cardLine.LotNumber,
                    CardLineId = cardLine.Id,
                    SerialNumber = cardLine.SerialNumber,
                    CardNo = cardLine.CardNo,
                    Price = cardLine.Price,
                    UserName = cardLine.UserName,
                    UserFullName = cardLine.UserFullName,
                    DateCreate = cardLine.DateCreate,
                    UserType = UserType.Agency,
                    UserNameActive = userInfo.Username,
                    UserActiveFullName = userInfo.FullName,
                    Date = agencyAndCard.LastDateUpdate,
                    StatusId = statusInfo.LineNo,
                    StatusCode = statusInfo.Code,
                    StatusName = statusInfo.Name,
                    Source = CardSource.AgencyBuyActivation,
                    SourceNo = cardLine.SerialNumber,
                    AgencyId = userInfo.agencyInfo.Id,
                    AgencyName = userInfo.agencyInfo.FullName
                };
                hasValue = _cardLineService.Update(Mapper.Map<M_CardLine>(cardLine));
                hasValue = _agencyAndCardService.Update(Mapper.Map<M_AgencyAndCard>(agencyAndCard));
                hasValue = _cardEntryService.Insert(Mapper.Map<M_CardEntry>(cardEntry));
                hasValue = _vuViecService.Commit();
                var _status = string.IsNullOrEmpty(hasValue) ? "Oke" : "Error";
                var _msg = string.IsNullOrEmpty(hasValue) ? "" : hasValue;
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}