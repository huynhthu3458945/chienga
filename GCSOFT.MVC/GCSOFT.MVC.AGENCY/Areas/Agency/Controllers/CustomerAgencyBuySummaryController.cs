﻿using AutoMapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Areas.Agency.Data;
using GCSOFT.MVC.AGENCY.Areas.Transaction.Data;
using GCSOFT.MVC.AGENCY.Helper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.StoreProcedue;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.ExeclOfficeEngine;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.StoreProcedure.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.Agency.Controllers
{
    [CompressResponseAttribute]
    public class CustomerAgencyBuySummaryController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IStoreProcedureService _storeService;
        private readonly ICardLineService _cardLineService;
        private readonly ICardEntryService _cardEntryService;
        private readonly IAgencyCardService _agencyAndCardService;
        private readonly IOptionLineService _optionLineService;
        private readonly IAgencyService _agencyService;
        private readonly IHistoryRecall _historyRecall;
        private string sysCategory = "AGENCYCUSTBUYTOTAL";
        private bool? permission = false;
        private string hasValue;
        #endregion

        #region -- Contructor --
        public CustomerAgencyBuySummaryController
            (
                IVuViecService vuViecService
                , IStoreProcedureService storeService
                , ICardLineService cardLineService
                , ICardEntryService cardEntryService
                , IAgencyCardService agencyAndCardService
                , IOptionLineService optionLineService
                , IAgencyService agencyService
                , IHistoryRecall historyRecall
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _storeService = storeService;
            _cardLineService = cardLineService;
            _cardEntryService = cardEntryService;
            _agencyAndCardService = agencyAndCardService;
            _optionLineService = optionLineService;
            _agencyService = agencyService;
            _historyRecall = historyRecall;
        }
        #endregion
        public ActionResult Index(CustomerAgencyBuySummaryPage c)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var userInfo = GetUser();
            c.AgencyId = c.AgencyId ?? userInfo.agencyInfo.Id;
            var cardHistory = c;

            var fromDate = new DateTime();
            var toDate = new DateTime();
            if (string.IsNullOrEmpty(c.fd))
            {
                fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                toDate = fromDate.AddMonths(1).AddDays(-1);
                cardHistory.CustAgencySumList = new List<SpCustomerAgencyBuySumary>();
            }
            else
            {
                fromDate = DateTime.Parse(StringUtil.ConvertStringToDate(c.fd));
                toDate = DateTime.Parse(StringUtil.ConvertStringToDate(c.td));

                cardHistory.CustAgencySumList = _storeService.sp_CustomerAgencyBuySumary(c.AgencyId ?? 0, c.SeriNumber ?? "", c.FullName ?? "", c.PhoneNo ?? "", c.Email ?? "", c.LevelCode ?? "", fromDate, toDate);
            }
            cardHistory.FromDate = fromDate;
            cardHistory.ToDate = toDate;
            cardHistory.LevelList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("LV")).ToSelectListItems(c.LevelCode ?? "");
            cardHistory.AgencyDDL = _storeService.AgencyParentFindAll("FINDALL", GetUser().agencyInfo.Id).ToSelectListItems(c.AgencyId ?? 0);
            return View(cardHistory);
        }
        public ActionResult Download(CustomerAgencyBuySummaryPage c)
        {
            var userInfo = GetUser();
            c.AgencyId = c.AgencyId ?? userInfo.agencyInfo.Id;
            var cardHistory = c;
            var fromDate = new DateTime();
            var toDate = new DateTime();
            if (string.IsNullOrEmpty(c.fd))
            {
                fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                toDate = fromDate.AddMonths(1).AddDays(-1);
                cardHistory.CustAgencySumList = new List<SpCustomerAgencyBuySumary>();
            }
            else
            {
                fromDate = DateTime.Parse(StringUtil.ConvertStringToDate(c.fd));
                toDate = DateTime.Parse(StringUtil.ConvertStringToDate(c.td));

                cardHistory.CustAgencySumList = _storeService.sp_CustomerAgencyBuySumary(c.AgencyId ?? 0, c.SeriNumber ?? "", c.FullName ?? "", c.PhoneNo ?? "", c.Email ?? "", c.LevelCode ?? "", fromDate, toDate);
            }
            var agencyInfo = Mapper.Map<AgencyCard>(_agencyService.GetBy2(c.AgencyId ?? 0));
            var res = cardHistory.CustAgencySumList;
            var dataModel = res.ToList().ConvertToDataTable();
            dataModel.TableName = "Model";

            List<KeyValuePair<string, object>> excelItems = new List<KeyValuePair<string, object>>();
            excelItems.Add(new KeyValuePair<string, object>("AgencyName", string.Format("{0} {1}", agencyInfo.AgencyType == AgencyType.Staff ? "Nhân Viên " : "Bưu điện ", string.IsNullOrEmpty(agencyInfo.CompanyName) ? agencyInfo.FullName : agencyInfo.CompanyName)));
            excelItems.Add(new KeyValuePair<string, object>("MonthYear", string.Format("BIÊN BẢN ĐỐI SOÁT THÁNG {0}/{1}", fromDate.Month, fromDate.Year)));
            excelItems.Add(new KeyValuePair<string, object>("FromToDate", string.Format("Từ ngày {0} đến ngày {1}", c.fd, c.td)));
            Excel.ExcelItems = excelItems;

            var stream = Excel.ExportReport(dataModel, HttpContext.Server.MapPath("/Areas/Agency/Report/BaoCaoDoiSoat02.xlsx"), false, OfficeType.XLSX);
            return File(stream, OfficeContentType.XLSX, "BaoCaoDoiSoat.xlsx");
        }
        public Dictionary<string, string> CboStatus()
        {
            Dictionary<string, string> dics = new Dictionary<string, string>();
            dics["BUY"] = "Ngày Bán";
            dics["ACTIVE"] = "Ngày Khách Kích Hoạt";
            return dics;
        }
    }
}