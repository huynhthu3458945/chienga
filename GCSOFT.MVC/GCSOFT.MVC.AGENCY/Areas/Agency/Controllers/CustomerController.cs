﻿using AutoMapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Areas.Agency.Data;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GCSOFT.MVC.AGENCY.Helper;
namespace GCSOFT.MVC.AGENCY.Areas.Agency.Controllers
{
    [CompressResponseAttribute]
    public class CustomerController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IAgencyService _agencyService;
        private readonly IPackageService _packageService;
        private readonly IProvinceService _provinceService;
        private readonly IDistrictService _districtService;
        private string sysCategory = "AGEN_CUST";
        private bool? permission = false;
        private AgencyCard agencyCard;
        private M_Agency mAgency;
        private string hasValue;
        #endregion

        #region -- Contructor --
        public CustomerController
            (
                IVuViecService vuViecService
                , IAgencyService agencyService
                , IPackageService packageService
                , IProvinceService provinceService
                , IDistrictService districtService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _agencyService = agencyService;
            _packageService = packageService;
            _provinceService = provinceService;
            _districtService = districtService;
        }
        #endregion
        public ActionResult Index()
        {
            try
            {
                #region -- Role user --
                permission = GetPermission(sysCategory, Authorities.View);
                if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
                if (!permission.Value) return View("AccessDenied");
                #endregion
                var agencyList = Mapper.Map<IEnumerable<AgencyList>>(_agencyService.FindAll(AgencyType.RetailCustomers, GetUser().agencyInfo.Id));
                return View(agencyList);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
            
        }
        public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            agencyCard = new AgencyCard();
            agencyCard.ParentAgencyId = GetUser().agencyInfo.Id;
            agencyCard.DateCreate = DateTime.Now;
            agencyCard.CityList = Mapper.Map<IEnumerable<ProvinceViewModel>>(_provinceService.FindAll()).ToSelectListItems(0);
            agencyCard.DistrictList = Mapper.Map<IEnumerable<DistrictViewModel>>(_districtService.FindAll(0)).ToSelectListItems(0);
            agencyCard.Legal = "Cá Nhân";
            return View(agencyCard);
        }

        [HttpPost]
        public ActionResult Create(AgencyCard _agencyCard)
        {
            try
            {
                SetData(_agencyCard, true);
                hasValue = _agencyService.Insert(mAgency);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = mAgency.Id, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        public ActionResult Edit(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            agencyCard = Mapper.Map<AgencyCard>(_agencyService.GetBy(id));
            if (agencyCard == null)
                return HttpNotFound();
            ViewBag.hasAccount = agencyCard.HasAccount;
            //agencyCard.PackageList = Mapper.Map<IEnumerable<PackageList>>(_packageService.FindAll()).ToSelectListItems(agencyCard.PackageId);
            agencyCard.CityList = Mapper.Map<IEnumerable<ProvinceViewModel>>(_provinceService.FindAll()).ToSelectListItems(agencyCard.CityId ?? 0);
            agencyCard.DistrictList = Mapper.Map<IEnumerable<DistrictViewModel>>(_districtService.FindAll(agencyCard.CityId ?? 0)).ToSelectListItems(agencyCard.DistrictId ?? 0);
            agencyCard.RegistrationDateStr = string.Format("{0:dd/MM/yyyy}", agencyCard.RegistrationDate);
            agencyCard.DateBlockStr = string.Format("{0:dd/MM/yyyy}", agencyCard.DateBlock);
            ViewBag.IsAccounting = "";
            return View(agencyCard);
        }

        [HttpPost]
        public ActionResult Edit(AgencyCard _agencyCard)
        {
            try
            {
                SetData(_agencyCard, false);
                hasValue = _agencyService.Update(mAgency);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = mAgency.Id, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        public ActionResult Deletes(string id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                int[] _codes = id.Split(',').Select(item => int.Parse(item)).ToArray();
                var teacherImgs = _agencyService.FindAll(_codes).Where(d => !string.IsNullOrEmpty(d.Image)).ToList();

                hasValue = _agencyService.Delete(_codes);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                {
                    UploadHelper _upload = new UploadHelper();
                    string pathImgTeacher = HttpContext.Server.MapPath("~/Files/Agencry/");
                    teacherImgs.ForEach(item =>
                    {
                        _upload.DeleteFileOnServer(pathImgTeacher + item.Id.ToString() + "/", item.Image);
                    });
                    //-- Delete Foloder
                    if (Directory.Exists(pathImgTeacher + teacherImgs.Select(d => d.Id).FirstOrDefault()))
                        Directory.Delete(pathImgTeacher + teacherImgs.Select(d => d.Id).FirstOrDefault(), true);
                    return Json(new { v = true, count = _codes.Count() });
                }
                return Json("Xóa dữ liệu không thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }
        public JsonResult RemoveImg(int id, string img)
        {
            hasValue = _agencyService.DeleteImg(id);
            hasValue = _vuViecService.Commit();
            if (string.IsNullOrEmpty(hasValue))
            {
                UploadHelper _upload = new UploadHelper();
                string PathSave = HttpContext.Server.MapPath("~/Files/Agencry/" + id.ToString() + "/");
                _upload.DeleteFileOnServer(PathSave, img);
            }
            return Json(string.IsNullOrEmpty(hasValue));
        }
        private void SetData(AgencyCard _agencyCard, bool isCreate)
        {
            agencyCard = _agencyCard;
            agencyCard.AgencyType = AgencyType.RetailCustomers;
            if (isCreate)
                agencyCard.Id = _agencyService.GetID();
            if (!string.IsNullOrEmpty(agencyCard.RegistrationDateStr))
                agencyCard.RegistrationDate = DateTime.Parse(StringUtil.ConvertStringToDate(agencyCard.RegistrationDateStr));
            else
                agencyCard.RegistrationDate = null;
            if (!string.IsNullOrEmpty(agencyCard.DateBlockStr))
                agencyCard.DateBlock = DateTime.Parse(StringUtil.ConvertStringToDate(agencyCard.DateBlockStr));
            else
                agencyCard.DateBlock = null;
            var provinceInfo = Mapper.Map<ProvinceViewModel>(_provinceService.GetBy(agencyCard.CityId ?? 0)) ?? new ProvinceViewModel();
            agencyCard.CityName = provinceInfo.name;
            var districtInfo = Mapper.Map<DistrictViewModel>(_districtService.GetBy(agencyCard.DistrictId ?? 0)) ?? new DistrictViewModel();
            agencyCard.DistrictName = districtInfo.name;
            UploadHelper _uploadHelper = new UploadHelper();
            _uploadHelper.PathSave = HttpContext.Server.MapPath("~/Files/Agencry/" + agencyCard.Id.ToString() + "/");
            _uploadHelper.UploadFile(Request.Files["fileUpload"]);
            if (!string.IsNullOrEmpty(_uploadHelper.FileSave))
                agencyCard.Image = _uploadHelper.FileSave;

            mAgency = Mapper.Map<M_Agency>(agencyCard);
        }
        public ActionResult LegalForm(int id, string type)
        {
            var _agencyInfo = Mapper.Map<AgencyCard>(_agencyService.GetBy(id)) ?? new AgencyCard();
            _agencyInfo.CityList = Mapper.Map<IEnumerable<ProvinceViewModel>>(_provinceService.FindAll()).ToSelectListItems(0);
            _agencyInfo.DistrictList = Mapper.Map<IEnumerable<DistrictViewModel>>(_districtService.FindAll(0)).ToSelectListItems(0);
            if (type.Equals("Cá Nhân"))
                return PartialView("_personal", _agencyInfo);
            else
                return PartialView("_Company", _agencyInfo);
        }
    }
}