﻿using AutoMapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Areas.Agency.Data;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GCSOFT.MVC.AGENCY.Helper;
using OfficeOpenXml;
using GCSOFT.MVC.AGENCY.Areas.Transaction.Data;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using GCSOFT.MVC.Model.SystemModels;
using GCSOFT.MVC.AGENCY.Areas.Transaction.Controllers;
using GCSOFT.MVC.Service.StoreProcedure.Interface;

namespace GCSOFT.MVC.AGENCY.Areas.Agency.Controllers
{
    [CompressResponseAttribute]
    public class FindAgencyController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IAgencyService _agencyService;
        private readonly IPackageService _packageService;
        private readonly IProvinceService _provinceService;
        private readonly IDistrictService _districtService;
        private readonly IAgencyCardService _agencyCardService;
        private readonly IUserService _userService;
        private readonly IUserThuocNhomService _userThuocNhomService;
        private readonly IMailSetupService _mailSetupService;
        private readonly ITemplateService _templateService;
        private readonly IStoreProcedureService _storeService;
        private string sysCategory = "AGEN_FINDBRANCH";
        private bool? permission = false;
        private AgencyCard agencyCard;
        private M_Agency mAgency;
        private string hasValue;
        #endregion

        #region -- Contructor --
        public FindAgencyController
            (
                IVuViecService vuViecService
                , IAgencyService agencyService
                , IPackageService packageService
                , IProvinceService provinceService
                , IDistrictService districtService
                , IAgencyCardService agencyCardService
                , IUserService userService
                , IUserThuocNhomService userThuocNhomService
                , IMailSetupService mailSetupService
                , ITemplateService templateService
                , IStoreProcedureService storeService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _agencyService = agencyService;
            _packageService = packageService;
            _provinceService = provinceService;
            _districtService = districtService;
            _agencyCardService = agencyCardService;
            _userService = userService;
            _userThuocNhomService = userThuocNhomService;
            _mailSetupService = mailSetupService;
            _templateService = templateService;
            _storeService = storeService;
        }
        #endregion
       
        public ActionResult Index(int? id)
        {
            try
            {
                #region -- Role user --
                permission = GetPermission(sysCategory, Authorities.View);
                if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
                if (!permission.Value) return View("AccessDenied");
                #endregion
                var userInfo = GetUser();
                id = id ?? userInfo.agencyInfo.Id;
                var agencyPage = new FindAgencyPage();
                agencyPage.AgencyDDL = _storeService.AgencyParentFindAll("FINDALL", GetUser().agencyInfo.Id).ToSelectListItems(id ?? 0);
                agencyPage.AgencyList = Mapper.Map<IEnumerable<AgencyList>>(_agencyService.FindAllByAgency(id ?? 0));
                return View(agencyPage);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
            
        }
    }
}