﻿using AutoMapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Areas.Agency.Data;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Data.Repositories.StoreProcedure;
using GCSOFT.MVC.Model.StoreProcedue;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.MasterDataService.Service;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.Agency.Controllers
{
    [CompressResponseAttribute]
    public class HistoryRecallController : BaseController
    {
        private string sysCategory = "AGENCY_HISTORYRECALL";
        private bool? permission = false;
        private readonly IHistoryRecall _historyRecall;
        private readonly IVuViecService _vuViecService;
        private readonly IStoreProcedureRepository _storeProcedureRepository;

        /// <summary>
        ///     Contractor
        /// </summary>
        /// <param name="vuViecService"></param>
        /// <param name="historyRecall"></param>
        /// <param name="storeProcedureRepository"></param>
        public HistoryRecallController(IVuViecService vuViecService, IHistoryRecall historyRecall, IStoreProcedureRepository storeProcedureRepository) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _historyRecall = historyRecall;
            _storeProcedureRepository = storeProcedureRepository;
        }

        /// <summary>
        ///  Load Danh Sách Lịch Sử Thu Hồi
        /// </summary>
        /// <param name="p"></param>
        /// <param name="Serinumber"></param>
        /// <param name="CardNo"></param>
        /// <param name="DateRecallStr"></param>
        /// <param name="Reason"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [16/09/2021]
        /// </history>
        public ActionResult Index(int? p, string Serinumber, string CardNo, string DateRecallStr, string Reason)
        {
            try
            {
                #region -- Role user --
                permission = GetPermission(sysCategory, Authorities.View);
                if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
                if (!permission.Value) return View("AccessDenied");
                #endregion
                var historyRecalls = new VM_HistoryRecall();
                if (p == null)
                {
                    historyRecalls.ListHistoryRecall = new List<HistoryRecall>();
                    historyRecalls.Paging = new Paging();
                }
                else
                {
                    int pageString = p ?? 0;
                    int page = pageString == 0 ? 1 : pageString;
                    int totalRecord = 0;
                    totalRecord = _historyRecall.GetAll(GetUser().agencyInfo.Id).Count;
                    historyRecalls.Paging = new Paging(totalRecord, page);
                    DateTime dateRecall = DateTime.Now;
                    bool isDate = false;
                    if (!string.IsNullOrEmpty(DateRecallStr))
                    {
                        dateRecall = DateTime.Parse(StringUtil.ConvertStringToDate(DateRecallStr));
                        isDate = true;
                    }
                    
                    var res = _storeProcedureRepository.SP_HistoryRecall(GetUser().agencyInfo.Id, CardNo, Serinumber, isDate, dateRecall, Reason, historyRecalls.Paging.start, historyRecalls.Paging.offset);
                    // mã hóa những đong đã lưu trước khi chưa mã hóa
                    foreach(var item in res.Where(z=> z.CardNo!= null))
                    {
                        try
                        {
                            item.CardNo02 = CryptorEngine.Decrypt(item.CardNo, true, "TTLSTNHD@CARD");
                        }
                        catch (Exception ex)
                        {
                            item.CardNo02 = string.Empty;
                        }
                    }
                    historyRecalls.ListHistoryRecall = Mapper.Map<IEnumerable<HistoryRecall>>(res).ToList();

                }

                return View(historyRecalls);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
    }
}