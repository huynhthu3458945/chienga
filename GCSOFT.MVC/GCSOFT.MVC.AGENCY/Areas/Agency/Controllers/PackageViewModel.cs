﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.AGENCY.Areas.Agency.Controllers
{
    public class PackageList
    {
        public int Id { get; set; }
        [DisplayName("Cấp")]
        public string LeavelName { get; set; }
        [DisplayName("Gói")]
        public string Name { get; set; }
        [DisplayName("Số Thẻ Nhận Được")]
        public int NoOfCardsReceived { get; set; }
    }
    public class PackageCard
    {
        public int Id { get; set; }
        [DisplayName("Cấp")]
        public string LeavelName { get; set; }
        [DisplayName("Gói")]
        public string Name { get; set; }
        [DisplayName("Giá Trị")]
        public decimal ValueAmt { get; set; }
        [DisplayName("Chiết Khấu")]
        public decimal PaymentDiscount { get; set; }
        [DisplayName("Số Thẻ Nhận Được")]
        public int NoOfCardsReceived { get; set; }
        [DisplayName("Doanh Thu Của Đại Lý (Bằng Số)")]
        public decimal RevenueAmt { get; set; }
        [DisplayName("Doanh Thu Của Đại Lý (Bằng Chữ)")]
        public string RevenueStr { get; set; }
        [DisplayName("Lợi Nhuận")]
        public decimal ProfitAmt { get; set; }
        [DisplayName("Thứ Tự")]
        public int SortOrder { get; set; }
    }
}