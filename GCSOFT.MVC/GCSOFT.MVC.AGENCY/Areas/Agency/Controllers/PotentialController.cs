﻿using AutoMapper;
using Dapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Areas.Agency.Data;
using GCSOFT.MVC.AGENCY.ViewModels.HienTai;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Service.Transaction.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GCSOFT.MVC.Service.ExeclOfficeEngine;
using System.IO;
using System.Data.OleDb;
using System.Data;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Data.Common;
using System.Text;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using GCSOFT.MVC.AGENCY.Helper;

namespace GCSOFT.MVC.AGENCY.Areas.Agency.Controllers
{
    [CompressResponseAttribute]
    public class PotentialController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IPotentialCustomerService _potenticalCustService;
        private readonly IClassService _classService;
        private readonly IHistoryCall _historyCall;
        private readonly IUserInfoService _userInfoService;
        private readonly IOptionLineService _optionLineService;
        private readonly ICardHeaderService _cardHeaderService;
        private readonly ICardLineService _cardLineService;
        private readonly ITemplateService _templateService;
        private readonly IMailSetupService _mailSetupService;
        private string sysCategory = "AGEN_POTENTICAL";
        private bool? permission = false;
        private string hasValue;
        private bool hasError;
        private string _phoneExist = "";
        private string _emailExist = "";
        private string _phoneGranted = "";
        private string _emailGranted = "";
        private string ptbConnection = ConfigurationManager.ConnectionStrings["GCConnection"].ConnectionString;
        #endregion

        #region -- Contructor --
        public PotentialController
            (
                IVuViecService vuViecService
                , IPotentialCustomerService potenticalCustService
                , IClassService classService
                , IHistoryCall historyCall
                , IUserInfoService userInfoService
                , IOptionLineService optionLineService
                , ICardHeaderService cardHeaderService
                , ICardLineService cardLineService
                , ITemplateService templateService
                , IMailSetupService mailSetupService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _potenticalCustService = potenticalCustService;
            _classService = classService;
            _historyCall = historyCall;
            _userInfoService = userInfoService;
            _optionLineService = optionLineService;
            _cardHeaderService = cardHeaderService;
            _cardLineService = cardLineService;
            _templateService = templateService;
            _mailSetupService = mailSetupService;
        }
        #endregion
        // GET: Agency/Potential
        public ActionResult Index(int? p, string fullName, string phone, string email, string fromDate, string toDate)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var userInfo = GetUser();
            int pageString = p ?? 0;
            int _currPage = pageString == 0 ? 1 : pageString;
            var totalRecord = TotalRecord(fullName, phone, email, fromDate, toDate);

            var potentical = new PotenticalPage();
            if (!string.IsNullOrEmpty(fromDate))
                potentical.FromDate = DateTime.Parse(StringUtil.ConvertStringToDate(fromDate));
            else
                potentical.FromDate = DateTime.Now.AddMonths(-7);
            if (!string.IsNullOrEmpty(toDate))
                potentical.ToDate = DateTime.Parse(StringUtil.ConvertStringToDate(toDate));
            else
                potentical.ToDate = DateTime.Now;
            fromDate = potentical.FromDateStr;
            toDate = potentical.ToDateStr;
            potentical.paging = new Paging(totalRecord, _currPage);
            potentical.potenticals = PotentialList(potentical.paging.start, potentical.paging.offset, fullName, phone, email, fromDate, toDate);
            foreach (var item in potentical.potenticals)
            {
                item.TinhTrang = "";
                var isActiveByPhone = _userInfoService.CheckByPhone(item.Phone, STNHDType.Active);
                if (isActiveByPhone)
                    item.TinhTrang += "Người dùng đã mua tài khoản";
                var isExperienceBySerinumber = _userInfoService.CheckBySerinumber(item.Serinumber, STNHDType.ExperienceAccount);
                if (isExperienceBySerinumber)
                    item.TinhTrang += "<BR />Đã kích hoạt trải nghiệm";
            }
            return View(potentical);
        }
        public ActionResult PopupAddHistoryCall(int agencyId, int id)
        {
            var model = new M_HistoryCall();
            model.AgencyId = agencyId;
            model.PotentialId = id;
            model.DateInput = DateTime.Now.Date;
            model.NextDate = DateTime.Now.Date;
            return PartialView("_PopupAddHistoryCall", model);
        }
        [HttpPost]
        public ActionResult PopupAddHistoryCall(M_HistoryCall model)
        {
            if (ModelState.IsValid)
            {
                model.DateInput = DateTime.Now.Date;
                model.NextDate = DateTime.Now.Date;
                _historyCall.Insert(model);
                _vuViecService.Commit();
            }
            return Json("oke");
        }
        public ActionResult PopupViewHistoryCall(int agencyId, int id)
        {
            var model = _historyCall.GetAll(agencyId, id);
            return PartialView("_PopupViewHistoryCall", model);
        }
        public void Download(int? p, string fullName, string phone, string email, string fromDate, string toDate)
        {

            var userInfo = GetUser();
            var datas = SqlDownloadFile(fullName, phone, email, fromDate, toDate);
            ExcelPackage Ep = new ExcelPackage();
            ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("CustomerInfo");
            Sheet.Cells["A1:O1"].Merge = true;
            Sheet.Cells["A1:O1"].Value = "Danh Sách Khách Hàng";
            Sheet.Cells["A1:O1"].Style.Font.Size = 14;
            Sheet.Cells["A1:O1"].Style.Font.Bold = true;

            Sheet.Cells["A3"].Value = "STT";
            Sheet.Cells["B3"].Value = "Tên Phụ Huynh";
            Sheet.Cells["C3"].Value = "Email";
            Sheet.Cells["D3"].Value = "Điện Thoại";
            Sheet.Cells["E3"].Value = "Tên Con 1";
            Sheet.Cells["F3"].Value = "Lớp Con 1";
            Sheet.Cells["G3"].Value = "Trường Học Con 1";
            Sheet.Cells["H3"].Value = "Tỉnh Thành Phố";
            Sheet.Cells["I3"].Value = "Quận Huyện";
            Sheet.Cells["J3"].Value = "Mã Kích Hoạt";
            Sheet.Cells["A3:J3"].Style.Font.Bold = true;
            int row = 3;
            int stt = 0;
            foreach (var item in datas)
            {
                ++row;
                Sheet.Cells[string.Format("A{0}", row)].Value = ++stt;
                Sheet.Cells[string.Format("B{0}", row)].Value = item.fullName;
                Sheet.Cells[string.Format("C{0}", row)].Value = item.Email;
                Sheet.Cells[string.Format("D{0}", row)].Value = item.Phone;
                Sheet.Cells[string.Format("E{0}", row)].Value = item.ChildFullName1;
                Sheet.Cells[string.Format("F{0}", row)].Value = item.ClassName1;
                Sheet.Cells[string.Format("G{0}", row)].Value = item.Child1_SchoolName;
                Sheet.Cells[string.Format("H{0}", row)].Value = item.CityName;
                Sheet.Cells[string.Format("I{0}", row)].Value = item.DistrictName;
                Sheet.Cells[string.Format("J{0}", row)].Value = item.ActionCode;
            }
            Sheet.Cells["A3:J" + row].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            Sheet.Cells["A3:J" + row].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            Sheet.Cells["A3:J" + row].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            Sheet.Cells["A3:J" + row].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            Sheet.Cells["A:AZ"].AutoFitColumns();

            this.Response.Clear();
            this.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            this.Response.AddHeader("content-disposition", string.Format("attachment;  filename={0}", "CustomerInfo.xlsx"));
            this.Response.BinaryWrite(Ep.GetAsByteArray());
            Ep.Dispose();
        }
        private int TotalRecord(string fullName, string phone, string email, string fromDate, string toDate)
        {
            try
            {
                using (var conn = new SqlConnection(ptbConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "TOTAL_ROW");
                    paramaters.Add("@FullName", fullName);
                    paramaters.Add("@Phone", phone);
                    paramaters.Add("@Email", email);
                    if (!string.IsNullOrEmpty(fromDate))
                    {
                        paramaters.Add("@fromDate", DateTime.Parse(StringUtil.ConvertStringToDate(fromDate)));
                    }
                    if (!string.IsNullOrEmpty(toDate))
                    {
                        paramaters.Add("@toDate", DateTime.Parse(StringUtil.ConvertStringToDate(toDate)));
                    }
                    paramaters.Add("@AgencyId", GetUser().agencyInfo.Id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var recordOfPageResults = conn.Query<RecordOfPage>("SP_PotentialCustomer", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return recordOfPageResults.FirstOrDefault().TotalRow;
                }
            }
            catch { return 0; }
        }
        private IEnumerable<SqlPotentialList> PotentialList(int start, int offset, string fullName, string phone, string email, string fromDate, string toDate)
        {
            try
            {
                using (var conn = new SqlConnection(ptbConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "FINDALLBY");
                    paramaters.Add("@FullName", fullName);
                    paramaters.Add("@Phone", phone);
                    paramaters.Add("@Email", email);
                    paramaters.Add("@AgencyId", GetUser().agencyInfo.Id);
                    if (!string.IsNullOrEmpty(fromDate))
                    {
                        paramaters.Add("@fromDate", DateTime.Parse(StringUtil.ConvertStringToDate(fromDate)));
                    }
                    if (!string.IsNullOrEmpty(toDate))
                    {
                        paramaters.Add("@toDate", DateTime.Parse(StringUtil.ConvertStringToDate(toDate)));
                    }
                    paramaters.Add("@Start", start);
                    paramaters.Add("@Offset", offset);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<SqlPotentialList>("SP_PotentialCustomer", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }
            }
            catch { return new List<SqlPotentialList>(); }
        }
        private IEnumerable<SqlPotentialList> SqlDownloadFile(string fullName, string phone, string email, string fromDate, string toDate)
        {
            try
            {
                using (var conn = new SqlConnection(ptbConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "DOWNLOADLIST");
                    paramaters.Add("@FullName", fullName);
                    paramaters.Add("@Phone", phone);
                    paramaters.Add("@Email", email);
                    paramaters.Add("@AgencyId", GetUser().agencyInfo.Id);
                    if (!string.IsNullOrEmpty(fromDate))
                    {
                        paramaters.Add("@fromDate", DateTime.Parse(StringUtil.ConvertStringToDate(fromDate)));
                    }
                    if (!string.IsNullOrEmpty(toDate))
                    {
                        paramaters.Add("@toDate", DateTime.Parse(StringUtil.ConvertStringToDate(toDate)));
                    }
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<SqlPotentialList>("SP_PotentialCustomer", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }
            }
            catch { return new List<SqlPotentialList>(); }
        }
        public ActionResult DownloadExcel()
        {
            var res = File(HttpContext.Server.MapPath("/Areas/Agency/Report/CustomerLeadTemplate.xlsx"), OfficeContentType.XLSX, "CustomerLeadTemplate.xlsx");
            return res;
        }
        [HttpPost]
        public JsonResult ImportDataLead()
        {
            var PostedFile = Request.Files[0];
            string filename = PostedFile.FileName;
            string targetpath = Server.MapPath("/Areas/Agency/Files/");
            if (!Directory.Exists(targetpath))
                Directory.CreateDirectory(targetpath);
            PostedFile.SaveAs(targetpath + filename);
            string pathToExcelFile = targetpath + filename;
            var connectionString = "";
            if (filename.EndsWith(".xls"))
            {
                connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=Excel 8.0;", pathToExcelFile);
            }
            else if (filename.EndsWith(".xlsx"))
            {
                connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";", pathToExcelFile);
            }
            var adapter = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", connectionString);
            var ds = new DataSet();
            adapter.Fill(ds, "ExcelTable");
            DataTable dtable = ds.Tables["ExcelTable"];

            var idCard = _cardLineService.GetID() - 1;
            var cardHeader = Mapper.Map<CardHeaderCard>(_cardHeaderService.GetBy("LO2203-011"));

            string strHtml = string.Empty;
            string fullName = "";
            string phone = "";
            string email = "";
            string _carNoDecrypt = "";
            string _carNoEncrypt = "";
            var hasCardNo = true;
            var cardLines = new List<string>();
            var customerLeadList = new List<M_PotentialCustomer>();
            var customerLead = new M_PotentialCustomer();
            var agencyInfo = GetUser().agencyInfo;
            hasError = false;
            CheckError(dtable);
            if (hasError)
            {
                if(!string.IsNullOrEmpty(_phoneExist))
                    strHtml = $"Điện thoại đã có tài khoản:{_phoneExist.Substring(1, _phoneExist.Length-1)}<BR/>";
                if (!string.IsNullOrEmpty(_emailExist))
                    strHtml += $"Email đã có tài khoản:{_emailExist.Substring(1, _emailExist.Length - 1)}<BR/>";
                if (!string.IsNullOrEmpty(_phoneGranted))
                    strHtml += $"Điện thoại đã cấp tài khoản 1 Lớp:{_phoneGranted?.Substring(1, _phoneGranted.Length - 1)}<BR/>";
                if (!string.IsNullOrEmpty(_emailGranted))
                    strHtml += $"Email đã cấp tài khoản 1 Lớp:{_emailGranted.Substring(1, _emailGranted.Length - 1)}<BR/>";
                return Json(new { success = false, html = strHtml });
            }    
            foreach (DataRow item in dtable.Rows)
            {
                _carNoEncrypt = "";
                _carNoDecrypt = "";
                fullName = item["HoTenPhuHuynh"]?.ToString();
                phone = item["SoDienThoai"]?.ToString();
                email = item["Email"]?.ToString();

                idCard += 1;
                var _serinumner = StringUtil.GetProductID("1L", (idCard - 1).ToString(), 9);
                customerLead.fullName = fullName;
                customerLead.Phone = phone;
                customerLead.Email = email;
                customerLead.AgencyId = agencyInfo.Id;
                customerLead.DateCreate = DateTime.Now;
                do
                {
                    _carNoDecrypt = GetBuildCardNo().ToString().Replace("I", "L").Replace("0", "A").Replace("O", "B");
                    hasCardNo = cardLines.Where(xCard => xCard == _carNoDecrypt).Any();
                } while (_potenticalCustService.CheckExist(_carNoDecrypt) || hasCardNo);
                _carNoEncrypt = CryptorEngine.Encrypt(_carNoDecrypt, true, "TTLSTNHD@CARD");
                cardLines.Add(_carNoDecrypt);
                customerLead.Serinumber = _serinumner;
                customerLead.ActionCode = _carNoDecrypt;
                hasValue = _potenticalCustService.Insert(customerLead);
                hasValue = AddToCardLine(_serinumner, _carNoEncrypt, cardHeader);
                SendEmail(customerLead.fullName, _carNoDecrypt, customerLead.Email);
                hasValue = _vuViecService.Commit();
            }
            return Json(new { success = true, html = strHtml });
        }

        private void CheckError(DataTable dtable)
        {
            string strHtml = string.Empty;
            string fullName = "";
            string phone = "";
            string email = "";
            _phoneExist = "";
            _emailExist = "";
            _phoneGranted = "";
            _emailGranted = "";
            var agencyInfo = GetUser().agencyInfo;
            foreach (DataRow item in dtable.Rows)
            {

                fullName = item["HoTenPhuHuynh"]?.ToString();
                phone = item["SoDienThoai"]?.ToString();
                email = item["Email"]?.ToString();

                if (_userInfoService.CheckByPhone(phone, STNHDType.Active))
                {
                    _phoneExist += $", {phone}";
                    hasError = true;
                }
                if (_userInfoService.CheckByEmail(email, STNHDType.Active))
                {
                    _emailExist += $", {email}";
                    hasError = true;
                }
                if (_potenticalCustService.CheckByPhone(phone))
                {
                    _phoneGranted += $", {phone}";
                    hasError = true;
                }
                if (_potenticalCustService.checkByEmail(email))
                {
                    _emailGranted += $", {email}";
                    hasError = true;
                }

            }
        }
        private StringBuilder GetBuildCardNo()
        {
            var builder = new StringBuilder();
            var randomNumber = new RandomNumberGenerator();
            builder.Append("L1-");
            builder.Append(randomNumber.RandomString(3, true).ToUpper());
            builder.Append(randomNumber.RandomNumber(100, 999));
            builder.Append(randomNumber.RandomString(1, false).ToUpper());
            return builder;
        }
        private string AddToCardLine(string seriNumber, string cardNo, CardHeaderCard cardHeader)
        {
            try
            {
                var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("TTT", "100"));

                var cardLine = new CardLine();
                var cardLines = new List<CardLine>();
                var idCard = _cardLineService.GetID();

                statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("CARD", "450"));

                cardLine = new CardLine()
                {
                    Id = idCard,
                    LotNumber = cardHeader.LotNumber,
                    SerialNumber = seriNumber,
                    CardNo = cardNo,
                    Price = 0,
                    UserName = "",
                    UserFullName = cardHeader.UserFullName,
                    DateCreate = DateTime.Now,

                    UserType = UserType.TTL,
                    UserNameActive = "",
                    UserActiveFullName = "",
                    LastDateUpdate = DateTime.Now,
                    StatusId = statusInfo.LineNo,
                    StatusCode = statusInfo.Code,
                    StatusName = statusInfo.Name,
                    LastStatusId = statusInfo.LineNo,
                    LastStatusCode = statusInfo.Code,
                    LastStatusName = statusInfo.Name,
                    DurationId = cardHeader.DurationId,
                    DurationCode = cardHeader.DurationCode,
                    DurationName = cardHeader.DurationName,
                    ReasonId = cardHeader.ReasonId,
                    ReasonCode = cardHeader.ReasonCode,
                    ReasonName = cardHeader.ReasonName,
                    LevelId = cardHeader.LevelId,
                    LevelCode = cardHeader.LevelCode,
                    LevelName = cardHeader.LevelName
                };
                return _cardLineService.Insert(Mapper.Map<M_CardLine>(cardLine));
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        private string SendEmail(string fullName, string _carNoDecrypt, string emailTo)
        {
            try
            {
                var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
                var mailTemplate = Mapper.Map<Template>(_templateService.Get("EMAILACTIVELOP1"));
                var mailTitle = mailTemplate.Title;
                var mailContent = mailTemplate.Content.Replace("{{FULLNAME}}", fullName);
                mailContent = mailContent.Replace("{{ACTIVECODE}}", _carNoDecrypt);
                var emailTos = new List<string>();
                emailTos.Add(emailTo);
                var _mailHelper = new EmailHelper()
                {
                    EmailTos = emailTos,
                    DisplayName = emailInfo.DisplayName,
                    Title = mailTitle,
                    Body = mailContent,
                    MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
                };
                return _mailHelper.SendEmailsThread();
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
    }
}