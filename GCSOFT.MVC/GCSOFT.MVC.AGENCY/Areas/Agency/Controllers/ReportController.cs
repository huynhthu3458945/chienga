﻿using AutoMapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Areas.Agency.Data;
using GCSOFT.MVC.AGENCY.Areas.Transaction.Data;
using GCSOFT.MVC.AGENCY.Extension;
using GCSOFT.MVC.AGENCY.Helper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.StoreProcedue;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.ExeclOfficeEngine;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.StoreProcedure.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.Agency.Controllers
{
    [CompressResponseAttribute]
    public class ReportController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IStoreProcedureService _storeService;
        private readonly IAgencyService _agencyService;
        private readonly IOptionLineService _optionLineService;
        private readonly IAgencyCardService _agencyAndCardService;
        private readonly ICardLineService _cardLineService;
        private readonly ICardEntryService _cardEntryService;
        private readonly IHistoryRecall _historyRecall;
        private readonly IMailSetupService _mailSetupService;
        private bool? permission = false;
        private string sysCategory = "RPT_AGENCY_GENE_02";
        private string sysCategory_DetailCustomer = "RPT_DETAILCUSTOMER";
        private string sysCategory_DataPost = "RPT_DATAPOST";
        #endregion

        #region -- Contructor --
        public ReportController
            (
                IVuViecService vuViecService
                , IStoreProcedureService storeService
                , IAgencyService agencyService
                , IOptionLineService optionLineService
                , IAgencyCardService agencyAndCardService
                , ICardLineService cardLineService
                , ICardEntryService cardEntryService
                , IHistoryRecall historyRecall
                , IMailSetupService mailSetupService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _storeService = storeService;
            _agencyService = agencyService;
            _optionLineService = optionLineService;
            _agencyAndCardService = agencyAndCardService;
            _cardLineService = cardLineService;
            _cardEntryService = cardEntryService;
            _historyRecall = historyRecall;
            _mailSetupService = mailSetupService;
        }
        #endregion
        public ActionResult AgencyInfo()
        {
            #region -- Role user --
            permission = GetPermission("RPTAGENCYINFO", Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var _fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.FromDate = string.Format("{0:dd/MM/yyyy}", _fromDate);
            ViewBag.ToDate = string.Format("{0:dd/MM/yyyy}", _fromDate.AddMonths(1).AddDays(-1));
            return View();
        }
        public PartialViewResult RptAgencyInfo(string fd, string td)
        {
            var agencyInfo = GetUser().agencyInfo;
            var _fromDate = DateTime.Parse(StringUtil.ConvertStringToDate(fd));
            var _toDate = DateTime.Parse(StringUtil.ConvertStringToDate(td));
            return PartialView("_RptAgencyInfo", _storeService.sp_AgencyDetail(_fromDate, _toDate, agencyInfo.Id));
        }

        public ActionResult AgencyBuy2Child()
        {
            #region -- Role user --
            permission = GetPermission("RPTABUY2CHILD", Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var _fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.FromDate = string.Format("{0:dd/MM/yyyy}", _fromDate);
            ViewBag.ToDate = string.Format("{0:dd/MM/yyyy}", _fromDate.AddMonths(1).AddDays(-1));
            return View();
        }
        public PartialViewResult RptAgencyBuy2Child(string fd, string td)
        {
            var agencyInfo = GetUser().agencyInfo;
            var _fromDate = DateTime.Parse(StringUtil.ConvertStringToDate(fd));
            var _toDate = DateTime.Parse(StringUtil.ConvertStringToDate(td));
            return PartialView("_RptABuy2Child", _storeService.sp_AgencyBuy2Child(_fromDate, _toDate, agencyInfo.Id));
        }
        public ActionResult Download(string fd, string td)
        {
            var agencyInfo = GetUser().agencyInfo;
            var _fromDate = DateTime.Parse(StringUtil.ConvertStringToDate(fd));
            var _toDate = DateTime.Parse(StringUtil.ConvertStringToDate(td));
            var res = _storeService.sp_AgencyDetail(_fromDate, _toDate, agencyInfo.Id);
            var dataModel = res.ToList().ConvertToDataTable();
            dataModel.TableName = "Model";
            var stream = Excel.ExportReport(dataModel, HttpContext.Server.MapPath("/Areas/Agency/Report/AgencyInfo.xlsx"), false, OfficeType.XLSX);
            return File(stream, OfficeContentType.XLSX, "AgencyInfo.xlsx");
        }

        /// <summary>
        ///     Báo cáo đại lý kinh doanh
        /// </summary>
        /// <param name="p"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [10/09/2021]
        /// </history>
        public ActionResult Index(int? p, AgencyBuyGeneralData c)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            try
            {
                var agencyInfo = c;

                var fromDateBuy = new DateTime();
                var toDateBuy = new DateTime();
                var fromDateActive = new DateTime();
                var toDateActive = new DateTime();
                if (string.IsNullOrEmpty(c.fdBuy))
                {
                    fromDateBuy = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    toDateBuy = fromDateBuy.AddMonths(1).AddDays(-1);

                    fromDateActive = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    toDateActive = fromDateActive.AddMonths(1).AddDays(-1);

                    agencyInfo.Paging = new Paging();
                    agencyInfo.agencyBuyGeneral = new List<AgencyBuyGeneral>();
                }
                else
                {
                    fromDateBuy = DateTime.Parse(StringUtil.ConvertStringToDate(c.fdBuy));
                    toDateBuy = DateTime.Parse(StringUtil.ConvertStringToDate(c.tdBuy));

                    fromDateActive = DateTime.Parse(StringUtil.ConvertStringToDate(c.fdActive));
                    toDateActive = DateTime.Parse(StringUtil.ConvertStringToDate(c.tdActive));

                    int pageString = p ?? 0;
                    int page = pageString == 0 ? 1 : pageString;

                    // totalRecord = _storeService.SP_RPT_AgencyBuyGeneralCount(0, DateTime.Now, DateTime.Now, c.searchAgencyId ?? 0, c.searchPhone ?? "", c.searchEmail ?? "", c.GroupBy ?? "", c.SortBy ?? "").FirstOrDefault().TotalRecord;

                    var agencyBuyInfoList = _storeService.SP_RPT_AgencyBuyGeneralDiffDate(GetUser().agencyInfo.Id, fromDateBuy, toDateBuy, fromDateActive, toDateActive, c.searchAgencyId ?? 0, c.searchPhone ?? "", c.searchEmail ?? "", c.GroupBy ?? "", c.SortBy ?? "", 0, 0);
                    int totalRecord = agencyBuyInfoList.Count();
                    agencyInfo.Paging = new Paging(totalRecord, page);
                    switch (c.SortBy)
                    {
                        case "100":
                            agencyInfo.agencyBuyGeneral = agencyBuyInfoList.OrderByDescending(d => d.Total_VIP).Skip(agencyInfo.Paging.start).Take(agencyInfo.Paging.offset);
                            break;
                        case "200":
                            agencyInfo.agencyBuyGeneral = agencyBuyInfoList.OrderByDescending(d => d.Total_LEVEL).Skip(agencyInfo.Paging.start).Take(agencyInfo.Paging.offset);
                            break;
                        case "300":
                            agencyInfo.agencyBuyGeneral = agencyBuyInfoList.OrderByDescending(d => d.Total_GRADE).Skip(agencyInfo.Paging.start).Take(agencyInfo.Paging.offset);
                            break;
                        case "400":
                            agencyInfo.agencyBuyGeneral = agencyBuyInfoList.OrderByDescending(d => d.CardBuy_VIP).Skip(agencyInfo.Paging.start).Take(agencyInfo.Paging.offset);
                            break;
                        case "500":
                            agencyInfo.agencyBuyGeneral = agencyBuyInfoList.OrderByDescending(d => d.CardBuy_LEVEL).Skip(agencyInfo.Paging.start).Take(agencyInfo.Paging.offset);
                            break;
                        case "600":
                            agencyInfo.agencyBuyGeneral = agencyBuyInfoList.OrderByDescending(d => d.CardBuy_GRADE).Skip(agencyInfo.Paging.start).Take(agencyInfo.Paging.offset);
                            break;
                        case "700":
                            agencyInfo.agencyBuyGeneral = agencyBuyInfoList.OrderByDescending(d => d.CardActive_VIP).Skip(agencyInfo.Paging.start).Take(agencyInfo.Paging.offset);
                            break;
                        case "800":
                            agencyInfo.agencyBuyGeneral = agencyBuyInfoList.OrderByDescending(d => d.CardActive_LEVEL).Skip(agencyInfo.Paging.start).Take(agencyInfo.Paging.offset);
                            break;
                        case "900":
                            agencyInfo.agencyBuyGeneral = agencyBuyInfoList.OrderByDescending(d => d.CardActive_GRADE).Skip(agencyInfo.Paging.start).Take(agencyInfo.Paging.offset);
                            break;
                        default:
                            agencyInfo.agencyBuyGeneral = agencyBuyInfoList.Skip(agencyInfo.Paging.start).Take(agencyInfo.Paging.offset);
                            break;
                    }


                    agencyInfo.TotalVIP = agencyBuyInfoList.Sum(d => d.Total_VIP);
                    agencyInfo.TotalLevel = agencyBuyInfoList.Sum(d => d.Total_LEVEL);
                    agencyInfo.TotalGrade = agencyBuyInfoList.Sum(d => d.Total_GRADE);

                    agencyInfo.BuyVIP = agencyBuyInfoList.Sum(d => d.CardBuy_VIP);
                    agencyInfo.BuyLevel = agencyBuyInfoList.Sum(d => d.CardBuy_LEVEL);
                    agencyInfo.BuyGrade = agencyBuyInfoList.Sum(d => d.CardBuy_GRADE);

                    agencyInfo.ActiveVIP = agencyBuyInfoList.Sum(d => d.CardActive_VIP);
                    agencyInfo.ActiveLevel = agencyBuyInfoList.Sum(d => d.CardActive_LEVEL);
                    agencyInfo.ActiveGrade = agencyBuyInfoList.Sum(d => d.CardActive_GRADE);
                }
                agencyInfo.FromDateBuy = fromDateBuy;
                agencyInfo.ToDateBuy = toDateBuy;
                agencyInfo.FromDateActive = fromDateActive;
                agencyInfo.ToDateActive = toDateActive;
                //agencyInfo.AgencyList = Mapper.Map<IEnumerable<AgencyList>>(_agencyService.FindAllByAgency(GetUser().agencyInfo.Id)).ToSelectListItems(c.searchAgencyId ?? 0);
                agencyInfo.AgencyList = _storeService.AgencyParentFindAll("FINDALL", GetUser().agencyInfo.Id).ToSelectListItems(c.searchAgencyId ?? 0);
                agencyInfo.SortByList = CboSortByList().ToSelectListItems(c.SortBy);
                return View(agencyInfo);
            }
            catch (Exception ex)
            {
                var error = ex.ToString();
                ViewBag.Error = ex;
                return View("Error");
            }

        }

        /// <summary>
        ///     CboSortByList
        /// </summary>
        /// <param name="p"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [10/09/2021]
        /// </history>
        public Dictionary<string, string> CboSortByList()
        {
            Dictionary<string, string> dics = new Dictionary<string, string>();
            dics["100"] = "Thẻ VIP - Nhiều Nhất";
            dics["200"] = "Thẻ Cấp - Nhiều Nhất";
            dics["300"] = "Thẻ Lớp - Nhiều Nhất";

            dics["400"] = "Thẻ VIP Bán - Nhiều Nhất";
            dics["500"] = "Thẻ Cấp Bán - Nhiều Nhất";
            dics["600"] = "Thẻ Lớp Bán - Nhiều Nhất";

            dics["700"] = "Thẻ VIP Kích Hoạt - Nhiều Nhất";
            dics["800"] = "Thẻ Cấp Kích Hoạt - Nhiều Nhất";
            dics["900"] = "Thẻ Lớp Kích Hoạt - Nhiều Nhất";
            return dics;
        }

        /// <summary>
        ///     Download báo cáo kinh doanh đại lý
        /// </summary>
        /// <param name="p"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [10/09/2021]
        /// </history>
        public ActionResult DownloadAgencyBuyGeneral(int? p, AgencyBuyGeneralData c)
        {
            var agencyInfo = c;

            var fromDateBuy = new DateTime();
            var toDateBuy = new DateTime();
            var fromDateActive = new DateTime();
            var toDateActive = new DateTime();
            if (string.IsNullOrEmpty(c.fdBuy))
            {
                fromDateBuy = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                toDateBuy = fromDateBuy.AddMonths(1).AddDays(-1);

                fromDateActive = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                toDateActive = fromDateActive.AddMonths(1).AddDays(-1);

                agencyInfo.Paging = new Paging();
                agencyInfo.agencyBuyGeneral = new List<AgencyBuyGeneral>();
            }
            else
            {
                fromDateBuy = DateTime.Parse(StringUtil.ConvertStringToDate(c.fdBuy));
                toDateBuy = DateTime.Parse(StringUtil.ConvertStringToDate(c.tdBuy));

                fromDateActive = DateTime.Parse(StringUtil.ConvertStringToDate(c.fdActive));
                toDateActive = DateTime.Parse(StringUtil.ConvertStringToDate(c.tdActive));

                int pageString = p ?? 0;
                int page = pageString == 0 ? 1 : pageString;
                var agencyBuyInfoList = _storeService.SP_RPT_AgencyBuyGeneralDiffDate(GetUser().agencyInfo.Id, fromDateBuy, toDateBuy, fromDateActive, toDateActive, c.searchAgencyId ?? 0, c.searchPhone ?? "", c.searchEmail ?? "", c.GroupBy ?? "", c.SortBy ?? "", 0, 0);
                switch (c.SortBy)
                {
                    case "100":
                        agencyInfo.agencyBuyGeneral = agencyBuyInfoList.OrderByDescending(d => d.Total_VIP);
                        break;
                    case "200":
                        agencyInfo.agencyBuyGeneral = agencyBuyInfoList.OrderByDescending(d => d.Total_LEVEL);
                        break;
                    case "300":
                        agencyInfo.agencyBuyGeneral = agencyBuyInfoList.OrderByDescending(d => d.Total_GRADE);
                        break;
                    case "400":
                        agencyInfo.agencyBuyGeneral = agencyBuyInfoList.OrderByDescending(d => d.CardBuy_VIP);
                        break;
                    case "500":
                        agencyInfo.agencyBuyGeneral = agencyBuyInfoList.OrderByDescending(d => d.CardBuy_LEVEL);
                        break;
                    case "600":
                        agencyInfo.agencyBuyGeneral = agencyBuyInfoList.OrderByDescending(d => d.CardBuy_GRADE);
                        break;
                    case "700":
                        agencyInfo.agencyBuyGeneral = agencyBuyInfoList.OrderByDescending(d => d.CardActive_VIP);
                        break;
                    case "800":
                        agencyInfo.agencyBuyGeneral = agencyBuyInfoList.OrderByDescending(d => d.CardActive_LEVEL);
                        break;
                    case "900":
                        agencyInfo.agencyBuyGeneral = agencyBuyInfoList.OrderByDescending(d => d.CardActive_GRADE);
                        break;
                    default:
                        agencyInfo.agencyBuyGeneral = agencyBuyInfoList;
                        break;
                }
                foreach (var item in agencyInfo.agencyBuyGeneral)
                {
                    item.FullName = $"{item.FullName}\n- ĐT:{item.Phone}\n- Quận/Huyện:{item.DistrictName}\n- Tỉnh/Thành Phố:{item.CityName}";
                }
                agencyInfo.TotalVIP = agencyBuyInfoList.Sum(d => d.Total_VIP);
                agencyInfo.TotalLevel = agencyBuyInfoList.Sum(d => d.Total_LEVEL);
                agencyInfo.TotalGrade = agencyBuyInfoList.Sum(d => d.Total_GRADE);

                agencyInfo.BuyVIP = agencyBuyInfoList.Sum(d => d.CardBuy_VIP);
                agencyInfo.BuyLevel = agencyBuyInfoList.Sum(d => d.CardBuy_LEVEL);
                agencyInfo.BuyGrade = agencyBuyInfoList.Sum(d => d.CardBuy_GRADE);

                agencyInfo.ActiveVIP = agencyBuyInfoList.Sum(d => d.CardActive_VIP);
                agencyInfo.ActiveLevel = agencyBuyInfoList.Sum(d => d.CardActive_LEVEL);
                agencyInfo.ActiveGrade = agencyBuyInfoList.Sum(d => d.CardActive_GRADE);
            }

            var res = agencyInfo.agencyBuyGeneral;
            var dataModel = res.ToList().ConvertToDataTable();
            dataModel.TableName = "Model";
            List<KeyValuePair<string, object>> excelItems = new List<KeyValuePair<string, object>>();
            excelItems.Add(new KeyValuePair<string, object>("TotalVIP", agencyInfo.TotalVIP));
            excelItems.Add(new KeyValuePair<string, object>("TotalLevel", agencyInfo.TotalLevel));
            excelItems.Add(new KeyValuePair<string, object>("TotalGrade", agencyInfo.TotalGrade));
            excelItems.Add(new KeyValuePair<string, object>("BuyVIP", agencyInfo.BuyVIP));
            excelItems.Add(new KeyValuePair<string, object>("BuyLevel", agencyInfo.BuyLevel));
            excelItems.Add(new KeyValuePair<string, object>("BuyGrade", agencyInfo.BuyGrade));
            excelItems.Add(new KeyValuePair<string, object>("ActiveVIP", agencyInfo.ActiveVIP));
            excelItems.Add(new KeyValuePair<string, object>("ActiveLevel", agencyInfo.ActiveLevel));
            excelItems.Add(new KeyValuePair<string, object>("ActiveGrade", agencyInfo.ActiveGrade));
            Excel.ExcelItems = excelItems;
            var stream = Excel.ExportReport(dataModel, HttpContext.Server.MapPath("/Areas/Agency/Report/AgencyBuyGeneral.xlsx"), false, OfficeType.XLSX);
            return File(stream, OfficeContentType.XLSX, "AgencyBuyGeneral.xlsx");
        }

        /// <summary>
        ///     Chi tiết khách hàng
        /// </summary>
        /// <param name="p"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [13/09/2021]
        /// </history>
        public ActionResult DetailCustomer(int? p, DetailCustomerAgency c)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory_DetailCustomer, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            try
            {
                var userInfo = GetUser();
                c.AgencyId = c.AgencyId ?? 0;
                var cardHistory = c;

                var fromDate = new DateTime();
                var toDate = new DateTime();
                if (string.IsNullOrEmpty(c.fd))
                {
                    fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    toDate = fromDate.AddMonths(1).AddDays(-1);
                    cardHistory.CustAgencyList = new List<SpAgencyDetailSales>();
                    c.Paging = new Paging();
                }
                else
                {
                    int pageString = p ?? 0;
                    int page = pageString == 0 ? 1 : pageString;
                    int totalRecord = 0;
                    fromDate = DateTime.Parse(StringUtil.ConvertStringToDate(c.fd));
                    toDate = DateTime.Parse(StringUtil.ConvertStringToDate(c.td));
                    totalRecord = _storeService.SP_RPT_AgencyDetailSalesCount("COUNT.RECORD", userInfo.agencyInfo.Id, c.ViewBy ?? "", fromDate, toDate, c.AgencyId ?? 0, c.CardNo ?? "", c.LevelCode ?? "", c.SeriNumber ?? "", c.FullName ?? "", c.PhoneNo ?? "", c.Email ?? "", c.CardStatus ?? "", 0, 0).FirstOrDefault().NoOfNumber;
                    cardHistory.Paging = new Paging(totalRecord, page);
                    cardHistory.CustAgencyList = _storeService.SP_RPT_AgencyDetailSales("PAGING", userInfo.agencyInfo.Id, c.ViewBy ?? "", fromDate, toDate, c.AgencyId ?? 0, c.CardNo ?? "", c.LevelCode ?? "", c.SeriNumber ?? "", c.FullName ?? "", c.PhoneNo ?? "", c.Email ?? "", c.CardStatus ?? "", cardHistory.Paging.start, cardHistory.Paging.offset);
                }
                cardHistory.FromDate = fromDate;
                cardHistory.ToDate = toDate;
                cardHistory.LevelList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("LV")).ToSelectListItems(c.LevelCode ?? "");
                //cardHistory.AgencyDDL = Mapper.Map<IEnumerable<AgencyList>>(_agencyService.FindAllByAgency(GetUser().agencyInfo.Id)).ToSelectListItems(c.AgencyId ?? 0);
                cardHistory.AgencyDDL = _storeService.AgencyParentFindAll("FINDALL", GetUser().agencyInfo.Id).ToSelectListItems(c.AgencyId ?? 0);

                cardHistory.ViewBy = c.ViewBy;
                cardHistory.ViewByList = CboStatus().ToSelectListItems(c.ViewBy);
                cardHistory.CardStatus = c.CardStatus;
                cardHistory.CardStatusList = CboCardStatus().ToSelectListItems(c.CardStatus);
                return View(cardHistory);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public ActionResult PopupRecallCard(int agencyId, string seriNumber)
        {
            var agencyAndCard = _agencyAndCardService.GetBy(agencyId, seriNumber);
            var cardInfo = _cardLineService.GetBySerialNumber(agencyAndCard.SerialNumber);
            var agencyInfo = _agencyService.GetBy(agencyId);
            var model = new M_HistoryRecall();
            if (cardInfo.LastStatusCode.Equals("450") && !cardInfo.IsActive)
            {
                model.Serinumber = seriNumber;
                model.AgencyId = agencyId;
                model.AgencyName = agencyInfo.FullName;
                model.AgencyEmail = agencyInfo.Email;
                model.AgencyPhone = agencyInfo.Phone;
                model.ParentAgencyId = agencyInfo.ParentAgencyId ?? 0;

                model.CardNo = agencyAndCard.CardNo;
                model.FullName = agencyAndCard.FulName ?? cardInfo.FulName;
                model.Phone = agencyAndCard.PhoneNo ?? cardInfo.PhoneNo;
                model.Email = agencyAndCard.VerifyInfo ?? cardInfo.VerifyInfo;
                model.DateBuy = agencyAndCard.LastDateUpdate;

                model.DateRecall = DateTime.Now.Date;
            }
            
            return PartialView("_PopupRecallCard", model);
        }
        [HttpPost]
        public ActionResult PopupRecallCard(M_HistoryRecall model)
        {
            if (ModelState.IsValid)
            {
                if (RecallCard(model))
                {
                    model.DateRecall = DateTime.Now.Date;
                    model.SMS = $"Ma kich hoat STNHD {CryptorEngine.Decrypt(model.CardNo, true, "TTLSTNHD@CARD")} cua A/C da HUY boi doi tac [{model.AgencyId} - {model.AgencyName}], AC se khong su dung duoc Ma Kich Hoat nay nua, hotline: 0939 279 868 neu can ho tro".NonUnicode();
                    _historyRecall.Insert(model);
                    _vuViecService.Commit();
                }
            }
            return Json("oke");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s">Seri Number</param>
        /// <param name="ai">Agency Id</param>
        /// <param name="cardNo">cardNo</param>
        /// <returns></returns>
        public bool RecallCard(M_HistoryRecall model)
        {
            string hasValue = string.Empty;
            try
            {
                var userInfo = GetUser();
                var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("CARD", "100")); //Cập nhật trạng thái đã bán mã kích hoạt
                //var agencyAndCard = Mapper.Map<AgencyAndCard>(_agencyAndCardService.GetByStatus(ai, s));
                var agencyAndCard = Mapper.Map<AgencyAndCard>(_agencyAndCardService.GetByStatus(model.Serinumber, "450"));
                agencyAndCard.CardNo = null;
                agencyAndCard.PhoneNo = null;
                agencyAndCard.FulName = null;
                agencyAndCard.VerifyInfo = null;
                agencyAndCard.UserNameActive = userInfo.Username;
                agencyAndCard.UserActiveFullName = userInfo.FullName;
                agencyAndCard.LastDateUpdate = DateTime.Now;
                agencyAndCard.StatusId = statusInfo.LineNo;
                agencyAndCard.StatusCode = statusInfo.Code;
                agencyAndCard.StatusName = statusInfo.Name;

                statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("CARD", "250")); //Cập nhật trạng thái thu hồi
                var cardLine = Mapper.Map<CardLine>(_cardLineService.GetBySerialNumber(model.Serinumber));
                cardLine.LastStatusId = statusInfo.LineNo;
                cardLine.LastStatusCode = statusInfo.Code;
                cardLine.LastStatusName = statusInfo.Name;

                var cardEntry = new CardEntry()
                {
                    LotNumber = cardLine.LotNumber,
                    CardLineId = cardLine.Id,
                    SerialNumber = cardLine.SerialNumber,
                    CardNo = cardLine.CardNo,
                    Price = cardLine.Price,
                    UserName = cardLine.UserName,
                    UserFullName = cardLine.UserFullName,
                    DateCreate = cardLine.DateCreate,

                    UserType = UserType.Agency,
                    UserNameActive = userInfo.Username,
                    UserActiveFullName = userInfo.FullName,
                    Date = agencyAndCard.LastDateUpdate,
                    StatusId = statusInfo.LineNo,
                    StatusCode = statusInfo.Code,
                    StatusName = statusInfo.Name,
                    Source = CardSource.AgencyBuyActivation,
                    SourceNo = cardLine.SerialNumber,
                    AgencyId = userInfo.agencyInfo.Id,
                    AgencyName = userInfo.agencyInfo.FullName
                };
                hasValue = _cardLineService.Update(Mapper.Map<M_CardLine>(cardLine));
                hasValue = _agencyAndCardService.Update(Mapper.Map<M_AgencyAndCard>(agencyAndCard));
                hasValue = _cardEntryService.Insert(Mapper.Map<M_CardEntry>(cardEntry));
                hasValue = _vuViecService.Commit();
                var _status = string.IsNullOrEmpty(hasValue) ? "Oke" : "Error";
                var _msg = string.IsNullOrEmpty(hasValue) ? "" : hasValue;
                return ResendRecallForAgency(model);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        ///     Download báo cáo kinh doanh đại lý
        /// </summary>
        /// <param name="p"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [10/09/2021]
        /// </history>
        public ActionResult DownloadDetailCustomer(DetailCustomerAgency c)
        {
            var userInfo = GetUser();
            c.AgencyId = c.AgencyId ?? 0;
            var cardHistory = c;

            var fromDate = new DateTime();
            var toDate = new DateTime();
            if (string.IsNullOrEmpty(c.fd))
            {
                fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                toDate = fromDate.AddMonths(1).AddDays(-1);
                cardHistory.CustAgencyList = new List<SpAgencyDetailSales>();
            }
            else
            {
                fromDate = DateTime.Parse(StringUtil.ConvertStringToDate(c.fd));
                toDate = DateTime.Parse(StringUtil.ConvertStringToDate(c.td));
                cardHistory.CustAgencyList = _storeService.SP_RPT_AgencyDetailSales("FINDALL", userInfo.agencyInfo.Id, c.ViewBy ?? "", fromDate, toDate, c.AgencyId ?? 0, c.CardNo ?? "", c.LevelCode ?? "", c.SeriNumber ?? "", c.FullName ?? "", c.PhoneNo ?? "", c.Email ?? "", c.CardStatus ?? "", 0, 0);
            }
            foreach (var item in cardHistory.CustAgencyList)
            {
                item.DoiTacTuyenTren = $"- {item.ParentAgencyName}\n- {item.AgencyTree}";
                item.DoiTacBanHang = $"- {item.AgencyName}\n- {item.AgencyPhone}\n- {item.AgencyEmail}\n- {item.AgencyDistrictName}, {item.AgencyCityName}";
                item.NguoiMua = $"- {item.FulName}\n- {item.PhoneNo}\n- {item.VerifyInfo}";
                item.KichHoatBoi = $"- {item.ApplyUser}\n- {item.DateActive}";
                item.LoaiThe = $"- {item.LevelName}\n- {item.DurationName}\n- {item.HinhThucBan}\n- {item.TrangThaiThe}";
            }
            var res = cardHistory.CustAgencyList;
            var dataModel = res.ToList().ConvertToDataTable();
            dataModel.TableName = "Model";
            var stream = Excel.ExportReport(dataModel, HttpContext.Server.MapPath("/Areas/Agency/Report/DetailCustomer.xlsx"), false, OfficeType.XLSX);
            return File(stream, OfficeContentType.XLSX, "DetailCustomer.xlsx");
        }

        /// <summary>
        ///     CboStatus
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [13/09/2021]
        /// </history>
        public Dictionary<string, string> CboStatus()
        {
            Dictionary<string, string> dics = new Dictionary<string, string>();
            dics["BUY"] = "Ngày Bán";
            dics["ACTIVE"] = "Ngày Khách Kích Hoạt";
            return dics;
        }

        /// <summary>
        ///     CboCardStatus
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [13/09/2021]
        /// </history>
        public Dictionary<string, string> CboCardStatus()
        {
            Dictionary<string, string> dics = new Dictionary<string, string>();
            dics["Activated"] = "Đã Kích Hoạt";
            dics["NotActivated"] = "Chưa Kích Hoạt";
            return dics;
        }
        /// <summary>
        ///     CboCardStatus
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [21/09/2021]
        /// </history>
        public bool ResendRecallForAgency(M_HistoryRecall model)
        {
            try
            {
                var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
                var emailTos = new List<string>();
                var emailCCs = new List<string>();
                var agency = _agencyService.GetBy(model.AgencyId);
                emailTos.Add(model.Email);
                emailCCs.Add(model.AgencyEmail);
                var _mailHelper = new EmailHelper()
                {
                    EmailTos = emailTos,
                    DisplayName = emailInfo.DisplayName,
                    Title = "[ STNHD ] Thu Hồi Thẻ",
                    Body = $"Mã kích hoạt STNHĐ[{model.CardNo}] của anh chị đã Hủy bởi đối tác[{model.AgencyId} - {model.AgencyName}]",
                    MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
                };
                string hasValue = _mailHelper.DoSendMail();
                //-- Send SMS
                var content = $"Ma kich hoat STNHD {CryptorEngine.Decrypt(model.CardNo, true, "TTLSTNHD@CARD")} cua A/C da HUY boi doi tac [{model.AgencyId} - {model.AgencyName}], AC se khong su dung duoc Ma Kich Hoat nay nua, hotline: 0939 279 868 neu can ho tro".NonUnicode();
                var smsHelper = new SMSHelper()
                {
                    PhoneNo = model.Phone,
                    Content = content
                };
                if (smsHelper.IsMobiphone())
                {
                    smsHelper.Content = content;
                    hasValue = smsHelper.SendMobiphone();
                }
                else
                    hasValue = smsHelper.Send();
                //-- Send SMS +
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        ///     Chi tiết khách hàng
        /// </summary>
        /// <param name="p"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [13/09/2021]
        /// </history>
        public ActionResult DataPostBD(DataPostBD c)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory_DataPost, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var userInfo = GetUser();
            c.AgencyId = c.AgencyId ?? 0;
            var dataPostBD = c;

            var fromDate = new DateTime();
            var toDate = new DateTime();
            if (string.IsNullOrEmpty(c.fd))
            {
                fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                toDate = fromDate.AddMonths(1).AddDays(-1);
                dataPostBD.DataPostBDList = new List<SpDataPostBD>();
            }
            else
            {
                fromDate = DateTime.Parse(StringUtil.ConvertStringToDate(c.fd));
                toDate = DateTime.Parse(StringUtil.ConvertStringToDate(c.td));
                dataPostBD.DataPostBDList = _storeService.SP_DataPostBD(c.SearchAgencyId != 0 ? c.SearchAgencyId : userInfo.agencyInfo.Id, fromDate, toDate, c.SearchAgencyId, c.PhoneNo ?? "", c.Email ?? "");
            }
            dataPostBD.FromDate = fromDate;
            dataPostBD.ToDate = toDate;
            c.AgencyId = c.SearchAgencyId;
            //dataPostBD.AgencyDDL = Mapper.Map<IEnumerable<AgencyList>>(_agencyService.FindAllByAgency(GetUser().agencyInfo.Id)).ToSelectListItems(c.AgencyId ?? 0);
            dataPostBD.AgencyDDL = _storeService.AgencyParentFindAll("FINDALL", userInfo.agencyInfo.Id).ToSelectListItems(c.SearchAgencyId);
            return View(dataPostBD);
        }

        /// <summary>
        ///     Download báo cáo kinh doanh đại lý
        /// </summary>
        /// <param name="p"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [25/10/2021]
        /// </history>
        public ActionResult DownloadDataPostBD(DataPostBD c)
        {
            var userInfo = GetUser();
            c.AgencyId = c.AgencyId ?? 0;
            var cardHistory = c;

            var fromDate = new DateTime();
            var toDate = new DateTime();
            if (string.IsNullOrEmpty(c.fd))
            {
                fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                toDate = fromDate.AddMonths(1).AddDays(-1);
                cardHistory.DataPostBDList = new List<SpDataPostBD>();
            }
            else
            {
                fromDate = DateTime.Parse(StringUtil.ConvertStringToDate(c.fd));
                toDate = DateTime.Parse(StringUtil.ConvertStringToDate(c.td));
                cardHistory.DataPostBDList = _storeService.SP_DataPostBD(c.SearchAgencyId != 0 ? c.SearchAgencyId : userInfo.agencyInfo.Id, fromDate, toDate, c.SearchAgencyId, c.PhoneNo ?? "", c.Email ?? "");
            }
            
            var res = cardHistory.DataPostBDList;
            var dataModel = res.ToList().ConvertToDataTable();
            dataModel.TableName = "Model";
            var stream = Excel.ExportReport(dataModel, HttpContext.Server.MapPath("/Areas/Agency/Report/DataPostBD.xlsx"), false, OfficeType.XLSX);
            return File(stream, OfficeContentType.XLSX, "DataPostBD.xlsx");
        }
    }
}