﻿using AutoMapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Areas.Agency.Data;
using GCSOFT.MVC.AGENCY.Areas.Transaction.Data;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.Agency.Controllers
{
    public class UserInfoController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IUserInfoService _userInfoService;
        private readonly IIntroduceStudentService _introduceStudentService;
        private M_IntroduceStudent _m_IntroduceStudent = new M_IntroduceStudent();
        private string hasValue;
        #endregion
        #region -- Contructor --
        public UserInfoController(IVuViecService vuViecService
            , IUserInfoService userInfoService
            , IIntroduceStudentService introduceStudentService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _userInfoService = userInfoService;
            _introduceStudentService = introduceStudentService;
        }
        #endregion
        // GET: Agency/UserInfo
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetBySBD(string id)
        {
            var user = _userInfoService.GetBy(id);

            if (user.Id > 0)
            {
                return Json(new { status = true, SBD = user.Email }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = false, SBD = "" }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult CheckUserName(string id)
        {
            var user = _userInfoService.GetBy(id);

            if (user.Id > 0)
            {
                return Json(new { status = true }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = false }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index2(IntroduceStudenViewModel search)
        {
            IntroduceStudenIndex model = new IntroduceStudenIndex();
            if (!string.IsNullOrEmpty(search.UserName) || !string.IsNullOrEmpty(search.FullName))
                model.IntroduceStudenList = Mapper.Map<IEnumerable<IntroduceStudenViewModel>>(_introduceStudentService.FindUserNameOrFullName(search.UserName, search.FullName));
            else
                model.IntroduceStudenList = Mapper.Map<IEnumerable<IntroduceStudenViewModel>>(_introduceStudentService.FindAll());

            return View(model);
        }

        public ActionResult Create()
        {
            #region -- Role user --
            //permission = GetPermission(sysCategory, Authorities.Add);
            //if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            //if (!permission.Value) return View("AccessDenied");
            #endregion
            IntroduceStudenViewModel model = new IntroduceStudenViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(IntroduceStudenViewModel model)
        {
            try
            {
                SetData(model);
                hasValue = _introduceStudentService.Insert(_m_IntroduceStudent);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = _m_IntroduceStudent.Id });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public ActionResult Edit(int id)
        {
            #region -- Role user --
            //permission = GetPermission(sysCategory, Authorities.Add);
            //if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            //if (!permission.Value) return View("AccessDenied");
            #endregion
            IntroduceStudenViewModel model = Mapper.Map<IntroduceStudenViewModel>(_introduceStudentService.GetById(id));
            model.IsEdit = true;
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            #region -- Role user --
            //permission = GetPermission(sysCategory, Authorities.Add);
            //if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            //if (!permission.Value) return View("AccessDenied");
            #endregion
            try
            {
                hasValue = _introduceStudentService.Delete(id);
                hasValue = _vuViecService.Commit();
                return Json("Xóa dữ liệu không thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }

        [HttpPost]
        public ActionResult Edit(IntroduceStudenViewModel model)
        {
            try
            {
                SetData(model);
                _m_IntroduceStudent.UserName = _introduceStudentService.GetById(model.Id).UserName;
                hasValue = _introduceStudentService.Update(_m_IntroduceStudent);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = _m_IntroduceStudent.Id });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        #region -- Functions --
        private void SetData(IntroduceStudenViewModel model)
        {
            _m_IntroduceStudent = Mapper.Map<M_IntroduceStudent>(model);
        }
        #endregion
    }
}