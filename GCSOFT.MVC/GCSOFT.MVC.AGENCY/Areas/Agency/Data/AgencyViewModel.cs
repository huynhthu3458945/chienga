﻿using GCSOFT.MVC.Model.AgencyModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.Agency.Data
{
    public class AgencyList
    {
        public int Id { get; set; }
        public string Code { get; set; }
        [DisplayName("Họ và Tên")]
        public string FullName { get; set; }
        [DisplayName("Điện Thoại")]
        public string Phone { get; set; }
        [DisplayName("Email")]
        public string Email { get; set; }
        [DisplayName("Ngày Đăng Ký")]
        public DateTime RegistrationDate { get; set; }
        [DisplayName("Ngày Đăng Ký")]
        public string RegistrationDateStr { get { return string.Format("{0:dd/MM/yyyy}", RegistrationDate); } }
        [DisplayName("Cấp")]
        public string LeavelName { get; set; }
        public bool HasAccount { get; set; }
        [DisplayName("Trạng Thái")]
        public string AccountStatus { get { return HasAccount ? "Đã Cấp Tài Khoản" : "Chưa Cấp Tài Khoản"; } }
        public string CityName { get; set; }
        public string DistrictName { get; set; }
        public AgencyType AgencyType { get; set; }
        public int? ParentAgencyId { get; set; }
        [DisplayName("API Key")]
        public string AgencyNo { get; set; }
    }
    public class AgencyCard
    {
        public int Id { get; set; }
        public string Code { get; set; }
        [DisplayName("Họ")]
        public string FirstName { get; set; }
        [DisplayName("Tên")]
        public string LastName { get; set; }
        [DisplayName("Họ và Tên")]
        public string FullName { get; set; }
        [DisplayName("Điện Thoại")]
        public string Phone { get; set; }
        [DisplayName("Email")]
        public string Email { get; set; }
        [DisplayName("Địa Chỉ")]
        public string Address { get; set; }
        [DisplayName("Ngày Đăng Ký")]
        public DateTime? RegistrationDate { get; set; }
        [DisplayName("Ngày Đăng Ký")]
        public string RegistrationDateStr { get; set; }
        [DisplayName("Gói Đại Lý")]
        public int PackageId { get; set; }
        [DisplayName("Khóa")]
        public bool Block { get; set; }
        [DisplayName("Ngày Khóa")]
        public DateTime? DateBlock { get; set; }
        [DisplayName("Ngày Khóa")]
        public string DateBlockStr { get; set; }
        [DisplayName("Lý Do Khóa")]
        public string ReasonBlock { get; set; }
        [DisplayName("User Quản Trị")]
        public string UserName { get; set; }
        public string xUserName { get; set; }
        [DisplayName("Hình Ảnh (500 x 500)")]
        public string Image { get; set; }
        [DisplayName("Ghi Chú")]
        public string Remark { get; set; }
        public bool HasAccount { get; set; }
        public bool IsFirstLogin { get; set; }
        public IEnumerable<SelectListItem> PackageList { get; set; }
        public string PackageName { get; set; }
        public string LeavelName { get; set; }
        public AgencyType AgencyType { get; set; }
        public int? ParentAgencyId { get; set; }
        [DisplayName("Tỉnh / Thành Phố")]
        public int? CityId { get; set; }
        public string CityName { get; set; }
        public IEnumerable<SelectListItem> CityList { get; set; }
        [DisplayName("Quận/Huyện")]
        public int? DistrictId { get; set; }
        public string DistrictName { get; set; }
        public IEnumerable<SelectListItem> DistrictList { get; set; }
        public int? WardsId { get; set; }
        public string WardsName { get; set; }
        public string Legal { get; set; }
        [DisplayName("Số CMND/ Passport")]
        public string CMND { get; set; }
        [DisplayName("MST")]
        public string MST { get; set; }
        [DisplayName("Tên Công Ty")]
        public string CompanyName { get; set; }
        [DisplayName("Hiển Thị Lên Website ")]
        public bool ShowWebsite { get; set; }
        public DateTime? DateCreate { get; set; }
        public int NoOfOutlier { get; set; }
    }
}