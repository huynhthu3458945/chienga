﻿using GCSOFT.MVC.Model.StoreProcedue;
using GCSOFT.MVC.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.Agency.Data
{
    public class DetailCustomerAgency
    {
        public int? AgencyId { get; set; }
        public string SeriNumber { get; set; }
        public string FullName { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public DateTime FromDate { get; set; }
        public string fd { get; set; }
        public string FromDateStr { get { return string.Format("{0:dd/MM/yyyy}", FromDate); } }
        public DateTime ToDate { get; set; }
        public string td { get; set; }
        public string ToDateStr { get { return string.Format("{0:dd/MM/yyyy}", ToDate); } }

        public string CardNo { get; set; }
        public string ViewBy { get; set; }
        public Paging Paging { get; set; }
        public IEnumerable<SelectListItem> ViewByList { get; set; }
        public string CardStatus { get; set; }
        public IEnumerable<SelectListItem> CardStatusList { get; set; }
        public string LevelCode { get; set; }
        public IEnumerable<SelectListItem> LevelList { get; set; }
        public IEnumerable<SelectListItem> AgencyDDL { get; set; }
        public IEnumerable<SpAgencyDetailSales> CustAgencyList { get; set; }
    }
}