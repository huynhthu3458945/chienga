﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.AGENCY.Areas.Agency.Data
{
    public class DistrictViewModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public string prefix { get; set; }
        public int province_id { get; set; }
    }
}