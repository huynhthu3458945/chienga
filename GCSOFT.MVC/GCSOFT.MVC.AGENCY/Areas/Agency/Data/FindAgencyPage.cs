﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.Agency.Data
{
    public class FindAgencyPage
    {
        public string AgencyId { get; set; }
        public IEnumerable<SelectListItem> AgencyDDL { get; set; }
        public IEnumerable<AgencyList> AgencyList { get; set; }
    }
}