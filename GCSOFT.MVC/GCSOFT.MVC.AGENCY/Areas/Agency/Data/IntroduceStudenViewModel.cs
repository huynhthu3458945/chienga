﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.AGENCY.Areas.Agency.Data
{
    public class IntroduceStudenViewModel
    {
        public int Id { get; set; }
        [DisplayName("Tài Khoản")]
        public string UserName { get; set; }
        [DisplayName("Họ Tên")]
        public string FullName { get; set; }
        [DisplayName("Link")]
        public string Link { get; set; }
        public bool IsEdit { get; set; }
    }
    public class IntroduceStudenIndex
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Link { get; set; }
        public virtual IEnumerable<IntroduceStudenViewModel> IntroduceStudenList { get; set; }

        public IntroduceStudenIndex()
        {
            IntroduceStudenList = new List<IntroduceStudenViewModel>();
        }

    }

}