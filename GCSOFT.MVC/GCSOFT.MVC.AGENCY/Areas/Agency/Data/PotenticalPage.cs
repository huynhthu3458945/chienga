﻿using GCSOFT.MVC.AGENCY.ViewModels.HienTai;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.AGENCY.Areas.Agency.Data
{
    public class PotenticalPage
    {
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public DateTime FromDate { get; set; }
        public string FromDateStr { get { return string.Format("{0:dd/MM/yyyy}", FromDate); } }
        public DateTime ToDate { get; set; }
        public string ToDateStr { get { return string.Format("{0:dd/MM/yyyy}", ToDate); } }
        public Paging paging { get; set; }
        public IEnumerable<SqlPotentialList> potenticals { get; set; }
    }
    public class SqlPotentialList
    {
        public int id { get; set; }
        public string fullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string ChildFullName1 { get; set; }
        public string ClassName1 { get; set; }
        public string Child1_SchoolName { get; set; }
        public string ChildFullName2 { get; set; }
        public string ClassName2 { get; set; }
        public string Child2_SchoolName { get; set; }
        public string DistrictName { get; set; }
        public string CityName { get; set; }
        public string TinhTrang { get; set; }
        public int AgencyId { get; set; }
        public string userName { get; set; }
        public string ActionCode { get; set; }
        public string RenderActionCode { get; set; }
        public string Serinumber { get; set; }
        public string DateCreateStr { get; set; }
    }
}