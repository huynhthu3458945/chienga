﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.AGENCY.Areas.Agency.Data
{
    public class ProvinceViewModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
    }
}