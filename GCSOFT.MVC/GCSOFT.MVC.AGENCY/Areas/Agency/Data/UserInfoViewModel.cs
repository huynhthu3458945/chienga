﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.Agency.Data
{
    public class UserInfoViewModel
    {
        public int Id { get; set; }
        [DisplayName("Tên Đăng Nhập")]
        public string userName { get; set; }
        [DisplayName("Họ và Tên Con")]
        public string fullName { get; set; }
        [DisplayName("Mật Khẩu")]
        public string Password { get; set; }
        public string RePassword { get; set; }
        [DisplayName("Email Của Ba Mẹ")]
        public string Email { get; set; }
        [DisplayName("Điện Thoại Của Ba Mẹ")]
        public string Phone { get; set; }
        public string OTP { get; set; }
        public string Phone2 { get; set; }
        [DisplayName("Cho Phép Sử Dụng")]
        public bool ActivePhone { get; set; }
        [DisplayName("Địa Chỉ")]
        public string Address { get; set; }
        [DisplayName("Đã Chọn Lớp")]
        public bool HasClass { get; set; }
        [DisplayName("Facebook")]
        public string facebook { get; set; }
        [DisplayName("Zalo")]
        public string zalo { get; set; }
        [DisplayName("Viber")]
        public string viber { get; set; }
        [DisplayName("Serinumber")]
        public string Serinumber { get; set; }
        [DisplayName("Lớp Học")]
        public int ClassId { get; set; }
        public IEnumerable<VMClassList> VMClassList { get; set; }
        public IEnumerable<SelectListItem> ClassList { get; set; }
        public string token { get; set; }
        public DateTime? DateActive { get; set; }
        public DateTime? DateExpired { get; set; }
        public bool HasToken { get; set; }
        public STNHDType? STNHDType { get; set; }
        public string LevelCodePoup { get; set; }
        public UserInfoViewModel()
        {
            Id = 0;
        }
    }

    public class VMClassList : ClassList
    {
        public bool Check  { get; set; }
    }
}