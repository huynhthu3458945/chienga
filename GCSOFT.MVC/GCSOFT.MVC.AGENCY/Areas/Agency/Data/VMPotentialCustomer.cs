﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.AGENCY.Areas.Agency.Data
{
    public class VMPotentialCustomer
    {
        public int Id { get; set; }
        public string PotentialCode { get; set; }
        public int AgencyId { get; set; }
        public string userName { get; set; }
        public string fullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public bool ActivePhone { get; set; }
        public bool HasEbook { get; set; }
        public string ChildFullName1 { get; set; }
        public int? Child1_ClassId { get; set; }
        public string ChildFullName2 { get; set; }
        public int? Child2_ClassId { get; set; }
        public string ClassName1 { get; set; }
        public string ClassName2 { get; set; }
        public int? CityId { get; set; }
        public string CityName { get; set; }
        public int? DistrictId { get; set; }
        public string DistrictName { get; set; }
        public DateTime? DateActive { get; set; }
        public DateTime? DateExpired { get; set; }
        public int StatusId { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }
        public string Child1_SchoolName { get; set; }
        public string Child2_SchoolName { get; set; }
        public string TinhTrang { get; set; }
        public string Serinumber { get; set; }
        public string ActionCode { get; set; }
    }
}