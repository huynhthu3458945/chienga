﻿using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.Agency.Data
{
    public class CardHeaderList
    {
        [DisplayName("Số Lô")]
        public string LotNumber { get; set; }
        [DisplayName("Ngày Tạo")]
        public DateTime DateOrder { get; set; }
        [DisplayName("Số Lượng Thẻ")]
        public int NoOfCard { get; set; }
        public string NoOfCardStr { get { return string.Format("{0:#,##}", NoOfCard); } }
        [DisplayName("Ngày Tạo")]
        public string DateOrderStr { get { return string.Format("{0:dd/MM/yyyy}", DateOrder); } }
        [DisplayName("Người Tạo")]
        public string UserFullName { get; set; }
        [DisplayName("Trạng Thái")]
        public string StatusName { get; set; }
    }
    public class CardHeaderCard
    {
        public CardType CardType { get; set; }
        [DisplayName("Số Lô")]
        public string LotNumber { get; set; }
        [DisplayName("Số Lượng Thẻ")]
        public int NoOfCard { get; set; }
        [DisplayName("Giá")]
        public decimal Price { get; set; }
        [DisplayName("Trạng Thái")]
        public string StatusName { get; set; }
        [DisplayName("Người Tạo")]
        public string UserFullName { get; set; }

        [DisplayName("Ngày Tạo")]
        public DateTime DateOrder { get; set; }
        [DisplayName("Ngày Tạo")]
        public string DateOrderStr { get { return string.Format("{0:dd/MM/yyyy}", DateOrder); } }
        [DisplayName("Trạng Thái")]
        public int StatusId { get; set; }
        [DisplayName("Trạng Thái")]
        public string StatusCode { get; set; }
        [DisplayName("Người Tạo")]
        public string UserName { get; set; }
        [DisplayName("Ngày Tạo")]
        public DateTime DateCreate { get; set; }
        public Paging paging { get; set; }
        public int DurationId { get; set; }
        [DisplayName("Thời Hạn")]
        public string DurationCode { get; set; }
        public string DurationName { get; set; }
        public IEnumerable<SelectListItem> DurationList { get; set; }
        public int ReasonId { get; set; }
        [DisplayName("Lý Do Xuất Kho")]
        public string ReasonCode { get; set; }
        public string ReasonName { get; set; }
        [DisplayName("Gói Học")]
        public int LevelId { get; set; }
        public string LevelCode { get; set; }
        public string LevelName { get; set; }
        [DisplayName("Khuyến mãi")]
        public string PromotionCodeId { get; set; }
        public string PromotionCode { get; set; }
        public virtual IEnumerable<CardLine> CardLines { get; set; }
        public virtual IEnumerable<CardEntry> CardEntries { get; set; }
        public CardHeaderCard()
        {
            paging = new Paging();
            this.CardLines = new List<CardLine>();
        }
    }
    public class CardLine
    {
        public long Id { get; set; }
        [DisplayName("Số Lô")]
        public string LotNumber { get; set; }
        [DisplayName("Số Serial")]
        public string SerialNumber { get; set; }
        [DisplayName("Số Kích Hoạt")]
        public string CardNo { get; set; }
        [DisplayName("Giá")]
        public decimal Price { get; set; }
        public string PriceStr { get { return string.Format("{0:#,##}", Price); } }
        [DisplayName("Người Tạo")]
        public string UserName { get; set; }
        public string UserFullName { get; set; }
        [DisplayName("Ngày Tạo")]
        public DateTime DateCreate { get; set; }
        public UserType UserType { get; set; }
        public string UserNameActive { get; set; }
        public string UserActiveFullName { get; set; }
        public DateTime LastDateUpdate { get; set; }
        public int StatusId { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }
        public string PhoneNo { get; set; }
        public string FulName { get; set; }
        public bool IsActive { get; set; }
        public int LastStatusId { get; set; }
        public string LastStatusCode { get; set; }
        public string LastStatusName { get; set; }
        public int DurationId { get; set; }
        public string DurationCode { get; set; }
        public string DurationName { get; set; }
        public int ReasonId { get; set; }
        public string ReasonCode { get; set; }
        public string ReasonName { get; set; }
        public int LevelId { get; set; }
        public string LevelCode { get; set; }
        public string LevelName { get; set; }
        public string VerifyInfo { get; set; } 
        public string ApplyUser { get; set; }
        public DateTime? ApplyDate { get; set; }
        public DateTime? DateBuy { get; set; }
    }
    public class CardEntry
    {
        //public long Id { get; set; }
        [DisplayName("Số Lô")]
        public string LotNumber { get; set; }
        [DisplayName("Card Line Id")]
        public long CardLineId { get; set; }
        [DisplayName("Số Serial")]
        public string SerialNumber { get; set; }
        [DisplayName("Số Kích Hoạt")]
        public string CardNo { get; set; }
        [DisplayName("Giá")]
        public decimal Price { get; set; }
        [DisplayName("Người Tạo")]
        public string UserName { get; set; }
        [DisplayName("Người Tạo")]
        public string UserFullName { get; set; }
        [DisplayName("Ngày Tạo")]
        public DateTime DateCreate { get; set; }
        [DisplayName("Nguồn")]
        public UserType UserType { get; set; }
        [DisplayName("Người Tạo")]
        public string UserNameActive { get; set; }
        public string UserActiveFullName { get; set; }
        [DisplayName("Ngày")]
        public DateTime Date { get; set; }
        [DisplayName("Trạng Thái")]
        public int StatusId { get; set; }
        [DisplayName("Trạng Thái")]
        public string StatusCode { get; set; }
        [DisplayName("Trạng Thái")]
        public string StatusName { get; set; }
        public CardSource Source { get; set; }
        public string SourceNo { get; set; }
        public int? AgencyId { get; set; }
        public string AgencyName { get; set; }
        public int LevelId { get; set; }
        public string LevelCode { get; set; }
        public string LevelName { get; set; }
    }
}