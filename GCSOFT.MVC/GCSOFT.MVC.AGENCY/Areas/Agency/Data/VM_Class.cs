﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.Agency.Data
{
    public class ClassList
    {
        public ClassType ClassType { get; set; }
        public int Id { get; set; }
        [DisplayName("Mã Lớp")]
        public string Code { get; set; }
        [DisplayName("Tên Lớp")]
        public string Name { get; set; }
        [DisplayName("Ghi Chú")]
        public string Remark { get; set; }
        [DisplayName("Thứ Tự")]
        public int SortOrder { get; set; }
        [DisplayName("Trạng Thái")]
        public int StatusId { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }
        public IEnumerable<SelectListItem> StatusList { get; set; }
        [DisplayName("Cấp")]
        public int LevelId { get; set; }
        public string LevelName { get; set; }
        public IEnumerable<SelectListItem> LevelList { get; set; }
        public ClassList()
        {
            SortOrder = 0;
        }
    }
}