﻿using GCSOFT.MVC.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.AGENCY.Areas.Agency.Data
{
    public class VM_HistoryRecall
    {
        public int Id { get; set; }
        public int AgencyId { get; set; }
        public string AgencyName { get; set; }
        public string Serinumber { get; set; }
        public string CardNo { get; set; }
        public string CardNo02 { get; set; }
        public DateTime? DateRecall { get; set; }
        public string DateRecallStr { get { return string.Format("{0:dd/MM/yyyy}", DateRecall); } }
        public string Reason { get; set; }
        public Paging Paging { get; set; }
        public List<HistoryRecall> ListHistoryRecall { get; set; }
    }

    public class HistoryRecall
    {
        public int Id { get; set; }
        public int AgencyId { get; set; }
        public string AgencyName { get; set; }
        public string Serinumber { get; set; }
        public string CardNo { get; set; }
        public string CardNo02 { get; set; }
        public DateTime? DateRecall { get; set; }
        public string DateRecallStr { get { return string.Format("{0:dd/MM/yyyy}", DateRecall); } }
        public string Reason { get; set; }
        public Paging Paging { get; set; }
    }
}