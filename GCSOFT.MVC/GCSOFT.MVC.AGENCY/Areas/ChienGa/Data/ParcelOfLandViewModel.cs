﻿using GCSOFT.MVC.AGENCY.ViewModels.HienTai;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.ChienGa.Data
{
    public class ParcelOfLandViewModel
    {

        public int Id { get; set; }
        public string Code { get; set; }

        public string Vocative1 { get; set; }
        public IEnumerable<SelectListItem> VocativeList1 { get; set; }
        public string FullNameMale { get; set; }
        public string CMND1 { get; set; }
        public string CMND2 { get; set; }
        public string Vocative2 { get; set; }
        public IEnumerable<SelectListItem> VocativeList2 { get; set; }
        public string FullNameFemale { get; set; }
        public int? YearBirthMale { get; set; }
        public int? YearBirthFemale { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Ward { get; set; }
        public DateTime DateCreate { get; set; }
        public string DateCreateStr { get; set; }
        public IEnumerable<SelectListItem> WardList { get; set; }
        public string SoilType { get; set; }
        public IEnumerable<SelectListItem> SoilTypeList { get; set; }
        public string Volatility { get; set; }
        public IEnumerable<SelectListItem> VolatilityList { get; set; }
        public int Orders { get; set; }
        public int? MapOld { get; set; }
        public int? MapNew { get; set; }
        public int? ParcelBig { get; set; }
        public int? ParcelOld { get; set; }
        public int? ParcelNew { get; set; }
        public decimal? AreaOld { get; set; }
        public decimal? AreaNew { get; set; }
        public string Note { get; set; }
        public bool IsCreate { get; set; }
    }

    public class ParcelOfLandListPage
    {
        public string Code { get; set; }
        public string FullNameMale { get; set; }
        public string FullNameFemale { get; set; }
        public int? YearBirthMale { get; set; }
        public int? YearBirthFemale { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public Paging paging { get; set; }
        public List<ParcelOfLandViewModel> ParcelOfLandList { get; set; }
        public string Ward { get; set; }
        public IEnumerable<SelectListItem> WardList { get; set; }

        public string SoilType { get; set; }
        public IEnumerable<SelectListItem> SoilTypeList { get; set; }

        public string Volatility { get; set; }
        public IEnumerable<SelectListItem> VolatilityList { get; set; }

    }

    public class ParcelOfLandViliViewModel
    {
        public int Id { get; set; }
        public int Map { get; set; }
        public int Parcel { get; set; }
        public decimal Area { get; set; }
        public string Ward { get; set; }
    }
}