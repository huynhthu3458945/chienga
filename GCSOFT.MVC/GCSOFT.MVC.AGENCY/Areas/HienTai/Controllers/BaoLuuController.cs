﻿using AutoMapper;
using Dapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Areas.Agency.Data;
using GCSOFT.MVC.AGENCY.Helper;
using GCSOFT.MVC.AGENCY.HienTaiService;
using GCSOFT.MVC.AGENCY.ViewModels.HienTai;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.StoreProcedue;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.HienTai.Controllers
{
    [CompressResponseAttribute]
    public class BaoLuuController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IOptionLineService _optionLineService;
        private readonly IMailSetupService _mailSetupService;
        private readonly ITemplateService _templateService;
        private string sysCategory = "HIENTAI_LOP";
        private bool? permission = false;
        private string hienTaiConnection = ConfigurationManager.ConnectionStrings["HienTaiConnection"].ConnectionString;
        #endregion

        ParentService parentService = new ParentService();
        StudentService studentService = new StudentService();
        ReserveService reserveService = new ReserveService();
        #region -- Contructor --
        public BaoLuuController
            (
                IVuViecService vuViecService,
                IOptionLineService optionLineService,
                IMailSetupService mailSetupService,
                ITemplateService templateService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _optionLineService = optionLineService;
            _mailSetupService = mailSetupService;
            _templateService = templateService;
        }
        #endregion
        public ActionResult Index(int? pageNumber)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion

            int page = 0;
            int pageSize = 10;
            int.TryParse(pageNumber.ToString(), out page);
            if (page == 0) page = 1;
            ViewBag.StartSTT = 1;

            ViewBag.ListReserveCode = _optionLineService.FindAll("RESERVE");
            ViewBag.ListStudent = studentService.GetAllStudentDrop(GetUser().teacherInfo.AgencyId);
            ViewBag.ListApproveStatus = reserveService.GetApproveStatusDrop(GetUser().teacherInfo.AgencyId);

            List<O_Reserve> model = reserveService.GetAllReserve(string.Empty, string.Empty, 0, string.Empty, GetUser().teacherInfo.AgencyId);
            IPagedList<O_Reserve> data = model.ToPagedList(page, pageSize);
            return View(data);
        }

        [HttpPost]
        public ActionResult IndexContent(string code, string reserveCode, string studentId, string statusCode, int? pageNumber)
        {
            int pageSize = 10;
            int page = 0;
            int.TryParse(pageNumber.ToString(), out page);
            if (page == 0)
            {
                page = 1;
                ViewBag.StartSTT = 1;
            }
            else
            {
                ViewBag.StartSTT = ((page - 1) * pageSize) + 1;
            }

            ViewBag.code = code;
            ViewBag.reserveCode = reserveCode;
            ViewBag.studentId = studentId ?? "0";
            ViewBag.statusCode = statusCode;

            int studentIdConvert = 0;
            int.TryParse(studentId == null ? "0" : studentId, out studentIdConvert);

            List<O_Reserve> model = reserveService.GetAllReserve(code, reserveCode, studentIdConvert, statusCode, GetUser().teacherInfo.AgencyId);
            IPagedList<O_Reserve> data = model.ToPagedList(page, pageSize);
            return PartialView("_IndexContent", data);
        }
        
        public ActionResult Details(string code)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.ViewDetail);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View(new ReserveService().GetBy(code, GetUser().teacherInfo.AgencyId));
        }
    }
}