﻿using AutoMapper;
using Dapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Helper;
using GCSOFT.MVC.AGENCY.ViewModels.HienTai;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.HienTai.Controllers
{
    [CompressResponseAttribute]
    public class CustomerCareController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IOptionLineService _optionLineService;
        private string sysCategory = "HIENTAI_CUSTCARE";
        private bool? permission = false;
        private string hasValue;
        private O_CustomerCareInfo customerCareInfo;
        private O_CustomerCareCreateEdit customerCareCreateEdit;
        private string hienTaiConnection = ConfigurationManager.ConnectionStrings["HienTaiConnection"].ConnectionString;
        public CustomerCareController
            (
                IVuViecService vuViecService
                , IOptionLineService optionLineService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _optionLineService = optionLineService;
        }
        #endregion
        public ActionResult Index(int? currPage, int? teacherId, int? studentId)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var sqlHelper = new SqlExecHelper();
            int pageString = currPage ?? 0;
            int _currPage = pageString == 0 ? 1 : pageString;
            var studentPage = new O_CustomerCarePage();
            studentPage.StudentList = SqlStudentList().ToSelectListItems(studentId ?? 0);
            studentPage.TeacherList = sqlHelper.SqlTeacherListList(GetUser().teacherInfo).ToSelectListItems(teacherId ?? 0);
            int totalRecord = TotalRecord(teacherId ?? 0, studentId ?? 0);
            studentPage.paging = new Paging(totalRecord, _currPage);
            studentPage.CustomerCareList = CustomerCareList(studentPage.paging.start, studentPage.paging.offset, teacherId ?? 0, studentId ?? 0).ToList();
            return View(studentPage);
        }
        public ActionResult Create(int? studentId)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            customerCareCreateEdit = new O_CustomerCareCreateEdit();
            customerCareCreateEdit.DateInput = DateTime.Now;
            customerCareCreateEdit.DateInputStr = string.Format("{0:dd/MM/yyyy}", customerCareCreateEdit.DateInput);
            customerCareCreateEdit.Code = StringUtil.GetProductID("C", GetMax(), 4);
            customerCareCreateEdit.StudentList = SqlStudentList().ToSelectListItems(studentId ?? 0);
            if (studentId != null) customerCareCreateEdit.Ref = true;
            return View(customerCareCreateEdit);
        }
        public ActionResult Edit(int Id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            customerCareCreateEdit = GetBy(Id);
            customerCareCreateEdit.StudentList = SqlStudentList().ToSelectListItems(customerCareCreateEdit.StudentId);
            customerCareCreateEdit.DateInputStr = string.Format("{0:dd/MM/yyyy}", customerCareCreateEdit.DateInput);
            return View(customerCareCreateEdit);
        }
        [HttpPost]
        public ActionResult Create(O_CustomerCareCreateEdit _customerCareCreateEdit)
        {
            try
            {
                SetData(_customerCareCreateEdit, true);
                var id = SqlInsert(customerCareInfo);
                return RedirectToAction("Edit", new { id = id, msg = "1" });
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult Edit(O_CustomerCareCreateEdit _customerCareCreateEdit)
        {
            try
            {
                try
                {
                    SetData(_customerCareCreateEdit, false);
                    var id = SqlUpdate(customerCareInfo);
                    return RedirectToAction("Edit", new { id = _customerCareCreateEdit.Id, msg = "1" });
                }
                catch (Exception ex)
                {
                    ViewBag.Error = ex.ToString();
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public ActionResult Deletes(int id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                hasValue = SqlDelete(id);
                return Json("Xóa dữ liệu không thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }
        #region -- Functioin --
        private int TotalRecord(int teacherId, int studentId)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "TOTAL.ROW");
                    paramaters.Add("@AgencyID", GetUser().teacherInfo.AgencyId);
                    paramaters.Add("@StudentId", studentId);
                    paramaters.Add("@TeacherId", teacherId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var recordOfPageResults = conn.Query<RecordOfPage>("SP2_O_CustomerCare", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return recordOfPageResults.FirstOrDefault().TotalRow;
                }
            }
            catch { return 0; }
        }
        private IEnumerable<O_CustomerCareList> CustomerCareList(int start, int offset, int teacherId, int studentId)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "FINDALL");
                    paramaters.Add("@AgencyID", GetUser().teacherInfo.AgencyId);
                    paramaters.Add("@StudentId", studentId);
                    paramaters.Add("@TeacherId", teacherId);
                    paramaters.Add("@Start", start);
                    paramaters.Add("@Offset", offset);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<O_CustomerCareList>("SP2_O_CustomerCare", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }
            }
            catch { return new List<O_CustomerCareList>(); }
        }
        private string GetMax()
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "GETMAX");
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var recordOfPageResults = conn.Query<MaxCode>("SP2_O_CustomerCare", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return recordOfPageResults.FirstOrDefault().Code;
                }
            }
            catch { return ""; }
        }
        private O_CustomerCareCreateEdit GetBy(int id)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "GETBY");
                    paramaters.Add("@Id", id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<O_CustomerCareCreateEdit>("SP2_O_CustomerCare", paramaters, null, true, null, System.Data.CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch { return new O_CustomerCareCreateEdit(); }
        }
        private int SqlInsert(O_CustomerCareInfo CustomerCareInfo)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "INSERT");
                    foreach (PropertyInfo propertyInfo in CustomerCareInfo.GetType().GetProperties())
                    {
                        paramaters.Add("@" + propertyInfo.Name, propertyInfo.GetValue(CustomerCareInfo, null));
                    }
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP2_O_CustomerCare", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return paramaters.Get<Int32>("@RetCode");
                }
            }
            catch { return 0; }
        }
        private int SqlUpdate(O_CustomerCareInfo CustomerCareInfo)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "UPDATE");
                    foreach (PropertyInfo propertyInfo in CustomerCareInfo.GetType().GetProperties())
                    {
                        paramaters.Add("@" + propertyInfo.Name, propertyInfo.GetValue(CustomerCareInfo, null));
                    }
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP2_O_CustomerCare", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return CustomerCareInfo.Id;
                }
            }
            catch { return 0; }
        }
        private string SqlDelete(int id)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "DELETE");
                    paramaters.Add("@Id", id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP2_O_CustomerCare", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return "";
                }
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        private void SetData(O_CustomerCareCreateEdit _customerCareCreateEdit, bool isCreate)
        {
            if (isCreate)
            {
                customerCareInfo = new O_CustomerCareInfo();
                customerCareInfo.Code = StringUtil.GetProductID("C", GetMax(), 4);
            }
            else
            {
                customerCareInfo = Mapper.Map<O_CustomerCareInfo>(GetBy(_customerCareCreateEdit.Id));
            }
            customerCareInfo.StudentId = _customerCareCreateEdit.StudentId;
            customerCareInfo.Title = _customerCareCreateEdit.Title;
            customerCareInfo.Description = _customerCareCreateEdit.Description;
            customerCareInfo.DateInput = DateTime.Parse(StringUtil.ConvertStringToDate(_customerCareCreateEdit.DateInputStr));
            customerCareInfo.AgencyId = GetUser().teacherInfo.AgencyId;
            customerCareInfo.TeacherId = GetUser().teacherInfo.Id;
        }
        private IEnumerable<OptionInt> SqlStudentList()
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "FINDALL2");
                    paramaters.Add("@AgencyId", GetUser().teacherInfo.AgencyId);
                    paramaters.Add("@TeacherId", GetUser().teacherInfo.Id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<OptionInt>("SP2_O_Student", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }
            }
            catch { return new List<OptionInt>(); }
        }
        #endregion
    }
}