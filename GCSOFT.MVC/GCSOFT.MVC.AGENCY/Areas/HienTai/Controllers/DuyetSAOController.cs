﻿using ClosedXML.Excel;
using Dapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Helper;
using GCSOFT.MVC.AGENCY.ViewModels.HienTai;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Service.ExeclOfficeEngine;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.HienTai.Controllers
{
    [CompressResponseAttribute]
    public class DuyetSAOController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IOptionLineService _optionLineService;
        private string sysCategory = "HT_BC_SAONGAY";
        private bool? permission = false;
        private string hasValue;
        private string hienTaiConnection = ConfigurationManager.ConnectionStrings["HienTaiConnection"].ConnectionString;
        #endregion
        public DuyetSAOController
            (
                IVuViecService vuViecService
                , IOptionLineService optionLineService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _optionLineService = optionLineService;
        }
        // GET: HienTai/DuyetSAO
        public ActionResult Index(SP2_RPT_STAR_INPROCESS_Page c)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            try
            {
                var sqlHelper = new SqlExecHelper();
                var teacherInfo = GetUser().teacherInfo;
                var fromDateBuy = new DateTime();
                if (string.IsNullOrEmpty(c.DateRpt2))
                    fromDateBuy = DateTime.Now;
                else
                    fromDateBuy = DateTime.Parse(StringUtil.ConvertStringToDate(c.DateRpt2));
                c.DateRpt = fromDateBuy;

                c.SchoolClassList = sqlHelper.SqlSchoolClassList(teacherInfo).ToSelectListItems(c.SchoolClassId ?? 0);
                c.TeacherId = c.TeacherId ?? GetUser().teacherInfo.Id;
                c.TeacherList = sqlHelper.SqlTeacherListList(teacherInfo).ToSelectListItems(c.TeacherId ?? 0);
                c.StudentList = sqlHelper.SqlStudentList(teacherInfo).ToSelectListItems(c.StudentId ?? 0);
                List<OptionInt> lstStatus = new List<OptionInt> { new OptionInt { Key = -1, Value = "Tất cả"}
                , new OptionInt { Key = 0, Value = "Chưa Báo cáo" }, new OptionInt { Key = 1, Value = "Đã báo cáo"}};
                c.StatusList = lstStatus.ToSelectListItems(-1);
                c.RptStarInProcessList = SqlRptStarInProcess(c);
                c.RptStarInProcessReport = SqlRptStarInProcessReport(c);
                return View(c);
            }
            catch (Exception ex)
            {
                var error = ex.ToString();
                ViewBag.Error = ex;
                return View("Error");
            }
        }

        public ActionResult ExportInProcess(SP2_RPT_STAR_INPROCESS_Page c)
        {
            //xu ly
            var teacherInfo = GetUser().teacherInfo;
            var fromDateBuy = new DateTime();
            if (string.IsNullOrEmpty(c.DateRpt2))
                fromDateBuy = DateTime.Now;
            else
                fromDateBuy = DateTime.Parse(StringUtil.ConvertStringToDate(c.DateRpt2));
            c.DateRpt = fromDateBuy;

            //lay data
            IEnumerable<SP2_RPT_STAR_INPROCESS> data = SqlRptStarInProcess(c);

            //create data
            DataTable dt = new DataTable();
            //Setiing Table Name  
            dt.TableName = "ExportInProcess";
            //Add Columns  
            dt.Columns.Add("STT", typeof(int));
            dt.Columns.Add("SBD", typeof(string));
            dt.Columns.Add("HienTai", typeof(string));
            dt.Columns.Add("PhuHuynh", typeof(string));
            dt.Columns.Add("DienThoai", typeof(string));
            dt.Columns.Add("Sao", typeof(string));
            dt.Columns.Add("LanLamSao", typeof(string));
            dt.Columns.Add("SoNgayDaBaoCao", typeof(string));
            dt.Columns.Add("NgayBatDau", typeof(string));
            dt.Columns.Add("NgayBaoCao", typeof(string));
            dt.Columns.Add("TrangThai", typeof(string));
            if (data.Count() > 0)
            {
                int stt = 1;
                foreach (var item in data)
                {
                    dt.Rows.Add(stt, item.IDNo, item.FullName,item.ParentName,item.Phone,item.StarName,item.Title,item.CountDate, item.StartDate, c.DateRpt.ToString(), item.Status);
                    stt++;
                }
            }
            dt.AcceptChanges();

            //Name of File  
            string fileName = "ExportInProcess.xlsx";
            using (XLWorkbook wb = new XLWorkbook())
            {
                //Add DataTable in worksheet  
                wb.Worksheets.Add(dt);
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    //Return xlsx Excel File  
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
                }
            }
        }

        private IEnumerable<OptionInt> SqlSchoolClassStarList()
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "FINDALL2");
                    paramaters.Add("@AgencyId", GetUser().teacherInfo.AgencyId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<OptionInt>("SP2_O_SchoolClass", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }
            }
            catch { return new List<OptionInt>(); }
        }
        private List<SP2_RPT_STAR_INPROCESS> SqlRptStarInProcess(SP2_RPT_STAR_INPROCESS_Page c)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "SP2_RPT_STAR_INPROCESS");
                    paramaters.Add("@AgencyId", GetUser().teacherInfo.AgencyId);
                    paramaters.Add("@StudentId", c.StudentId);
                    paramaters.Add("@TeacherId", c.TeacherId);
                    paramaters.Add("@SchoolClassId", c.SchoolClassId);
                    paramaters.Add("@DateRpt", c.DateRpt);
                    if (c.StatusId != -1)
                    {
                        paramaters.Add("@Status", c.StatusId);
                    }
                    return conn.Query<SP2_RPT_STAR_INPROCESS>("SP2_RPT_STAR_INPROCESS", paramaters, null, true, null, System.Data.CommandType.StoredProcedure).ToList();
                }
            }
            catch { return new List<SP2_RPT_STAR_INPROCESS>(); }
        }

        private List<SP2_RPT_STAR_INPROCESS_REPORT> SqlRptStarInProcessReport(SP2_RPT_STAR_INPROCESS_Page c)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "SP2_RPT_STAR_INPROCESS_REPORT");
                    paramaters.Add("@AgencyId", GetUser().teacherInfo.AgencyId);
                    paramaters.Add("@StudentId", c.StudentId);
                    paramaters.Add("@TeacherId", c.TeacherId);
                    paramaters.Add("@SchoolClassId", c.SchoolClassId);
                    paramaters.Add("@DateRpt", c.DateRpt);
                    return conn.Query<SP2_RPT_STAR_INPROCESS_REPORT>("SP2_RPT_STAR_INPROCESS", paramaters, null, true, null, System.Data.CommandType.StoredProcedure).ToList();
                }
            }
            catch { return new List<SP2_RPT_STAR_INPROCESS_REPORT>(); }
        }
    }
}