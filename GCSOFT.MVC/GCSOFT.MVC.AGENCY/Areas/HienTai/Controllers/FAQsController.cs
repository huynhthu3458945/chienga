﻿using AutoMapper;
using Dapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Areas.HienTai.Data;
using GCSOFT.MVC.AGENCY.Helper;
using GCSOFT.MVC.AGENCY.ViewModels.HienTai;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.HienTai.Controllers
{
    public class FAQsController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IOptionLineService _optionLineService;
        private string sysCategory = "HIENTAI_FAQS";
        private bool? permission = false;
        private string hasValue;
        private string hienTaiConnection = ConfigurationManager.ConnectionStrings["HienTaiConnection"].ConnectionString;
        #endregion

        #region -- Contructor --
        public FAQsController
            (
                IVuViecService vuViecService
                , IOptionLineService optionLineService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _optionLineService = optionLineService;
        }
        #endregion
        // GET: HienTai/FAQs
        public ActionResult Index(int? currPage,string statusCode, string title, string question, string answer)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion

            var fAQList = new FAQsListPage();
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                var paramaters = new DynamicParameters();

                int pageString = currPage ?? 0;
                int _currPage = pageString == 0 ? 1 : pageString;


                paramaters = new DynamicParameters();
                paramaters.Add("@Action", "TOTAL.ROW");

                paramaters.Add("@Title", title ?? string.Empty);
                paramaters.Add("@Question", question ?? string.Empty);
                paramaters.Add("@Answer", answer ?? string.Empty);
                paramaters.Add("@StatusCode", statusCode ?? string.Empty);

                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var recordOfPageResults = conn.Query<RecordOfPage>("SP_FAQs", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                int totalRecord = recordOfPageResults.FirstOrDefault().TotalRow;
                fAQList.paging = new Paging(totalRecord, _currPage);

                paramaters = new DynamicParameters();
                paramaters.Add("@Action", "FINDALL");
                paramaters.Add("@Title", title ?? string.Empty);
                paramaters.Add("@StatusCode", statusCode ?? string.Empty);
                paramaters.Add("@Question", question ?? string.Empty);
                paramaters.Add("@Answer", answer ?? string.Empty);
                paramaters.Add("@Start", fAQList.paging.start);
                paramaters.Add("@Offset", fAQList.paging.offset);

                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var res = conn.Query<FAQsViewModel>("SP_FAQs", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);

                fAQList.StatusCode = statusCode;
                fAQList.StatusList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("FAQs")).ToSelectListItems(statusCode ?? "");

                fAQList.FAQsList = res.ToList();
            }
            return View(fAQList);
        }
        public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var model = new FAQsViewModel();
            model.StatusList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("FAQs")).ToSelectListItems("");

            return View(model);
        }
        public ActionResult Edit(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion

            var model = new FAQsViewModel();
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                var paramaters = new DynamicParameters();

                paramaters = new DynamicParameters();
                paramaters.Add("@Action", "FINDBYID");
                paramaters.Add("@Id", id);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var res = conn.Query<FAQsViewModel>("SP_FAQs", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                model = res.FirstOrDefault();
                model.StatusList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("FAQs")).ToSelectListItems(model.StatusCode ?? "");
            }
            return View(model);
        }
        [HttpPost]
        public ActionResult Create(FAQsViewModel model)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    var paramaters = new DynamicParameters();
                    paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "ADD");
                    foreach (PropertyInfo propertyInfo in model.GetType().GetProperties())
                    {
                        paramaters.Add("@" + propertyInfo.Name, propertyInfo.GetValue(model, null));
                    }
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP_FAQs", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return RedirectToAction("Edit", new { id = paramaters.Get<Int32>("@RetCode"), msg = "1" });
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult Edit(FAQsViewModel model)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    var paramaters = new DynamicParameters();

                    paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "EDIT");
                    foreach (PropertyInfo propertyInfo in model.GetType().GetProperties())
                    {
                        paramaters.Add("@" + propertyInfo.Name, propertyInfo.GetValue(model, null));
                    }
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP_FAQs", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return RedirectToAction("Edit", new { id = paramaters.Get<Int32>("@RetCode"), msg = "1" });
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public ActionResult Deletes(int id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json(new
            {
                success = false,
                message = "Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại"
            }, JsonRequestBehavior.AllowGet);

            if (!permission.Value)
                return Json(new
                {
                    success = false,
                    message = "Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ."
                }, JsonRequestBehavior.AllowGet);
            #endregion
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "DELETE");
                    paramaters.Add("@Id", id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP_FAQs", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                }
                return Json(new
                {
                    success = true,
                    message = "Xóa dữ liệu không thành công !"
                }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new
                {
                    success = false,
                    message = "Xóa dữ liệu không thành công"
                }, JsonRequestBehavior.AllowGet);

            }
        }

        public ActionResult Detail(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.ViewDetail);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var model = new FAQsViewModel();
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                var paramaters = new DynamicParameters();

                paramaters = new DynamicParameters();
                paramaters.Add("@Action", "FINDBYID");
                paramaters.Add("@Id", id);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var res = conn.Query<FAQsViewModel>("SP_FAQs", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                model = res.FirstOrDefault();
                model.StatusList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("FAQs")).ToSelectListItems(model.StatusCode ?? "");
            }
            return View(model);
        }
    }
}