﻿using AutoMapper;
using Dapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Areas.Agency.Data;
using GCSOFT.MVC.AGENCY.Helper;
using GCSOFT.MVC.AGENCY.ViewModels.HienTai;
using GCSOFT.MVC.AGENCY.ViewModels.MasterData;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.StoreProcedue;
using GCSOFT.MVC.Model.SystemModels;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.HienTai.Controllers
{
    [CompressResponseAttribute]
    public class GiaoVienController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IOptionLineService _optionLineService;
        private readonly IUserService _userService;
        private readonly IUserThuocNhomService _userThuocNhomService;
        private readonly IMailSetupService _mailSetupService;
        private readonly ITemplateService _templateService;
        private readonly IClassService _classService;
        private string sysCategory = "HIENTAI_TEACHER";
        private bool? permission = false;
        private string hasValue;
        private O_TeacherInfo teacherInfo;
        private O_TeacherCreateEdit teacherCreateEdit;
        private O_TeacherDetails teacherDetails;
        private string hienTaiConnection = ConfigurationManager.ConnectionStrings["HienTaiConnection"].ConnectionString;
        #endregion

        #region -- Contructor --
        public GiaoVienController
            (
                IVuViecService vuViecService
                , IOptionLineService optionLineService
                , IUserService userService
                , IUserThuocNhomService userThuocNhomService
                , IMailSetupService mailSetupService
                , ITemplateService templateService
                , IClassService classService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _optionLineService = optionLineService;
            _userService = userService;
            _userThuocNhomService = userThuocNhomService;
            _mailSetupService = mailSetupService;
            _templateService = templateService;
            _classService = classService;
        }
        #endregion

        public ActionResult Index(int? currPage, string code, string fullName, string phone)
        {
            try
            {
                var teacherList = new O_TeacherListPage();
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();

                    //-- Paging
                    int pageString = currPage ?? 0;
                    int _currPage = pageString == 0 ? 1 : pageString;
                    paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "TOTAL.ROW");
                    paramaters.Add("@AgencyID", GetUser().teacherInfo.AgencyId);
                    paramaters.Add("@Id", GetUser().teacherInfo.Id);
                    paramaters.Add("@Code", code ?? string.Empty);
                    paramaters.Add("@Phone", phone ?? string.Empty);
                    paramaters.Add("@FullName", fullName ?? string.Empty);

                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var recordOfPageResults = conn.Query<RecordOfPage>("SP2_O_Teacher", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    int totalRecord = recordOfPageResults.FirstOrDefault().TotalRow;
                    teacherList.paging = new Paging(totalRecord, _currPage);
                    //-- Paging +

                    paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "FINDALL");
                    paramaters.Add("@AgencyID", GetUser().teacherInfo.AgencyId);
                    paramaters.Add("@Id", GetUser().teacherInfo.Id);
                    paramaters.Add("@Code", code ?? string.Empty);
                    paramaters.Add("@Phone", phone ?? string.Empty);
                    paramaters.Add("@FullName", fullName ?? string.Empty);
                    paramaters.Add("@Start", teacherList.paging.start);
                    paramaters.Add("@Offset", teacherList.paging.offset);

                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var results = conn.Query<O_TeacherList>("SP2_O_Teacher", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    teacherList.TeacherList = results.ToList();
                }
                ViewBag.teacherId = GetUser().teacherInfo.Id;
                return View(teacherList);
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.ToString();
                return View("Error");
            }
            
        }
        public ActionResult Details(int id, int? currPage, int? classStar)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.ViewDetail);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@Action", "GETBY");
                paramaters.Add("@Id", id);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var results = conn.Query<O_TeacherDetails>("SP2_O_Teacher", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                teacherDetails = results.FirstOrDefault();
                teacherDetails.xUserName = teacherDetails.Username;
                teacherDetails.xRoleCode = teacherDetails.RoleCode;

                paramaters.Add("@Action", "GETBYTEACHER");
                paramaters.Add("@TeacherId", id);
                paramaters.Add("@AgencyId", GetUser().teacherInfo.AgencyId);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var schoolClassStarList = conn.Query<OptionInt>("SP2_O_SchoolClass", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                teacherDetails.SchoolClassStarList = schoolClassStarList.ToSelectListItems(0);
                int defaultClassStar = schoolClassStarList.Count() > 0 ? schoolClassStarList.First().Key : 0;
                teacherDetails.SchoolClassStarId = classStar ?? defaultClassStar;

                int pageString = currPage ?? 0;
                int _currPage = pageString == 0 ? 1 : pageString;
                int totalRecord = TotalRecord(teacherDetails.SchoolClassStarId);
                teacherDetails.paging = new Paging(totalRecord, _currPage);
                teacherDetails.StudentList = StudentListByClassStar(teacherDetails.paging.start, teacherDetails.paging.offset, teacherDetails.SchoolClassStarId).ToList();
            }
            return View(teacherDetails);
        }
        public ActionResult Create(int? id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            teacherCreateEdit = new O_TeacherCreateEdit();
            teacherCreateEdit.RoleList = SqlRoleTeacherList(0).ToSelectListItems("");
            teacherCreateEdit.GenderList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("GENDER")).ToSelectListItems("");
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@Action", "PARENT.LIST");
                paramaters.Add("@AgencyID", GetUser().teacherInfo.AgencyId);
                paramaters.Add("@Id", GetUser().teacherInfo.Id);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var results = conn.Query<AgencyParentList>("SP2_O_Teacher", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                teacherCreateEdit.ParentTeacherList = results.ToSelectListItems(0);
                if (id != null)
                {
                    teacherCreateEdit.ParentTeacherId = id;
                }

                paramaters = new DynamicParameters();
                paramaters.Add("@Action", "GETMAX");
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var maxCode = conn.Query<MaxCode>("SP2_O_Teacher", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                teacherCreateEdit.Code = StringUtil.GetProductID("GV", maxCode.FirstOrDefault().Code, 4);
                teacherCreateEdit.Username = teacherCreateEdit.Code;
            }
            return View(teacherCreateEdit);
        }
        public ActionResult Edit(int Id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@Action", "GETBY");
                paramaters.Add("@Id", Id);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var results = conn.Query<O_TeacherCreateEdit>("SP2_O_Teacher", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                teacherCreateEdit = results.FirstOrDefault();
                teacherCreateEdit.xUserName = teacherCreateEdit.Username;
                teacherCreateEdit.xRoleCode = teacherCreateEdit.RoleCode;
                teacherCreateEdit.RoleList = SqlRoleTeacherList(Id).ToSelectListItems(teacherCreateEdit.RoleCode);
                teacherCreateEdit.GenderList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("GENDER")).ToSelectListItems(teacherCreateEdit.Gender);

                paramaters.Add("@Action", "PARENT.LIST");
                paramaters.Add("@AgencyID", GetUser().teacherInfo.AgencyId);
                paramaters.Add("@Id", GetUser().teacherInfo.Id);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var parentTeacherResults = conn.Query<AgencyParentList>("SP2_O_Teacher", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                teacherCreateEdit.ParentTeacherList = parentTeacherResults.ToSelectListItems(teacherCreateEdit.ParentTeacherId ?? 0);
            }
            return View(teacherCreateEdit);
        }
        [HttpPost]
        public ActionResult Create(O_TeacherCreateEdit _teacherCreateEdit)
        {
            try
            {
                SetData(_teacherCreateEdit, true);
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "INSERT");
                    teacherInfo.WorkDate = DateTime.Now;
                    foreach (PropertyInfo propertyInfo in teacherInfo.GetType().GetProperties())
                    {
                        paramaters.Add("@" + propertyInfo.Name, propertyInfo.GetValue(teacherInfo, null));
                    }
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP2_O_Teacher", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    CreateUserName(teacherInfo.Username, teacherInfo.RoleCode, teacherInfo);
                    return RedirectToAction("Edit", new { id = paramaters.Get<Int32>("@RetCode"), msg = "1" });
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult Edit(O_TeacherCreateEdit _teacherCreateEdit)
        {
            try
            {
                SetData(_teacherCreateEdit, false);
                try
                {
                    using (var conn = new SqlConnection(hienTaiConnection))
                    {
                        if (conn.State == System.Data.ConnectionState.Closed)
                            conn.Open();
                        var paramaters = new DynamicParameters();
                        paramaters.Add("@Action", "UPDATE");
                        foreach (PropertyInfo propertyInfo in teacherInfo.GetType().GetProperties())
                        {
                            paramaters.Add("@" + propertyInfo.Name, propertyInfo.GetValue(teacherInfo, null));
                        }
                        paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                        paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                        var result = conn.Execute("SP2_O_Teacher", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                        ChangeRole(teacherInfo.Username, teacherInfo.RoleCode, teacherInfo.xRoleCode);
                        return RedirectToAction("Edit", new { id = teacherInfo.Id, msg = "1" });
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.Error = ex.ToString();
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public ActionResult Deletes(int id, string userName)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "DELETE");
                    paramaters.Add("@Id", id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP2_O_Teacher", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    var userInfo = Mapper.Map<UserCard>(_userService.GetBy(userName));
                    var userIds = new List<int>();
                    userIds.Add(userInfo.ID);
                    hasValue = _userService.Delete(userIds.ToArray());
                }
                return Json("Xóa dữ liệu không thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }


        public ActionResult ChangePassword(string userName)
        {
            var userInfo = Mapper.Map<UserCard>(_userService.GetBy(userName));
            var user = Mapper.Map<UserLoginVMs>(_userService.GetBy(userInfo.ID));
            return PartialView("_ChangePassword", user);
        }

        [HttpPost]
        public ActionResult ChangePassword(UserLoginVMs userVM)
        {
            var user = _userService.GetBy(userVM.ID);
            user.Password = CryptorEngine.Encrypt(userVM.Password, true, "Hieu.Le-GC.Soft");
            user.LastDateUpdate = DateTime.Now.Date;
            return Json(_userService.ChangePassword(user));
        }


        private void SetData(O_TeacherCreateEdit _teacherCreateEdit, bool isCreate)
        {
            if (isCreate)
            {
                teacherInfo = new O_TeacherInfo();
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "GETMAX");
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var results = conn.Query<MaxCode>("SP2_O_Teacher", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    teacherInfo.Code = StringUtil.GetProductID("GV", results.FirstOrDefault().Code, 4);
                }
            }
            else
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "GETBY");
                    paramaters.Add("@Id", _teacherCreateEdit.Id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var results = conn.Query<O_TeacherInfo>("SP2_O_Teacher", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    teacherInfo = results.FirstOrDefault();
                }
            }
            teacherInfo.FullName = _teacherCreateEdit.FullName;
            teacherInfo.Code = _teacherCreateEdit.Code;
            teacherInfo.Phone = _teacherCreateEdit.Phone;
            teacherInfo.Email = _teacherCreateEdit.Email;
            teacherInfo.Username = _teacherCreateEdit.Username;
            teacherInfo.ParentTeacherId = _teacherCreateEdit.ParentTeacherId;
            teacherInfo.AgencyId = GetUser().teacherInfo.AgencyId;
            teacherInfo.RoleCode = _teacherCreateEdit.RoleCode;
            teacherInfo.HasAccount = _teacherCreateEdit.HasAccount;
            teacherInfo.xRoleCode = _teacherCreateEdit.RoleCode;
            teacherInfo.xUserName = _teacherCreateEdit.Username;
            teacherInfo.NoOfOutlier = GetUser().teacherInfo.NoOfOutlier;
            teacherInfo.Gender = _teacherCreateEdit.Gender;

        }
        private string CreateUserName(string u, string g, O_TeacherInfo teacherInfo)
        {
            try
            {
                RandomNumberGenerator randomNumber = new RandomNumberGenerator();
                var _pass = randomNumber.RandomNumber(10000, 99999);
                var userCard = new UserCard()
                {
                    ID = _userService.GetID(),
                    Password = CryptorEngine.Encrypt(_pass.ToString(), true, "Hieu.Le-GC.Soft"),
                    Username = u,
                    Email = teacherInfo.Email,
                    Phone = teacherInfo.Phone,
                    Description = teacherInfo.RoleCode,
                    LastName = teacherInfo.FullName,
                    FullName = teacherInfo.FullName
                };
                hasValue = _userService.Insert2(Mapper.Map<HT_Users>(userCard));
                hasValue = _vuViecService.Commit();
                var userInfo = Mapper.Map<UserCard>(_userService.GetBy(userCard.Username));
                var _userThuocNhom = new UserThuocNhomVM()
                {
                    UserID = userInfo.ID,
                    UserGroupCode = g
                };
                hasValue = _userThuocNhomService.Insert2(Mapper.Map<HT_UserThuocNhom>(_userThuocNhom));
                hasValue = _vuViecService.Commit();
                ResendPassForAgency(teacherInfo);
                return hasValue;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        private string ChangeRole(string userName, string role, string xRole)
        {
            try
            {
                if (role.Equals(xRole))
                {
                    var userInfo = Mapper.Map<UserCard>(_userService.GetBy(userName));
                    hasValue = _userThuocNhomService.Delete(xRole, userInfo.ID);
                    var _userThuocNhom = new UserThuocNhomVM()
                    {
                        UserID = userInfo.ID,
                        UserGroupCode = role
                    };
                    hasValue = _userThuocNhomService.Insert2(Mapper.Map<HT_UserThuocNhom>(_userThuocNhom));
                    hasValue = _vuViecService.Commit();
                }
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public string ResendPassForAgency(O_TeacherInfo teacherInfo)
        {
            var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
            var mailTemplate = Mapper.Map<Template>(_templateService.Get("TEACHERINFO"));
            var userInfo = Mapper.Map<UserLoginVMs>(_userService.GetBy(teacherInfo.Username));
            var emailTos = new List<string>();
            emailTos.Add(teacherInfo.Email);
            var _mailHelper = new EmailHelper()
            {
                EmailTos = emailTos,
                DisplayName = emailInfo.DisplayName,
                Title = mailTemplate.Title,
                Body = mailTemplate.Content.Replace("{{Name}}", teacherInfo.FullName).Replace("{{Username}}", teacherInfo.Username).Replace("{{Password}}", CryptorEngine.Decrypt(userInfo.Password, true, "Hieu.Le-GC.Soft")),
                MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
            };
            string hasValue = _mailHelper.DoSendMail();
            //-- Send SMS
            var content = string.Format(mailTemplate.SMSContent, teacherInfo.Username, CryptorEngine.Decrypt(userInfo.Password, true, "Hieu.Le-GC.Soft"));
            var smsHelper = new SMSHelper()
            {
                PhoneNo = teacherInfo.Phone,
                Content = content
            };
            hasValue = smsHelper.Send();
            //-- Send SMS +
            return "";
        }
        private int TotalRecord(int classStarId)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "TOTAL.ROW.CLASSSTAR");
                    paramaters.Add("@SchoolClassStarId", classStarId);
                    paramaters.Add("@AgencyID", GetUser().teacherInfo.AgencyId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var recordOfPageResults = conn.Query<RecordOfPage>("SP2_O_Student", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return recordOfPageResults.FirstOrDefault().TotalRow;
                }
            }
            catch { return 0; }
        }
        private IEnumerable<O_StudentList> StudentListByClassStar(int start, int offset, int classStarId)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "FINDBYCLASSSTAR");
                    paramaters.Add("@SchoolClassStarId", classStarId);
                    paramaters.Add("@AgencyID", GetUser().teacherInfo.AgencyId);
                    paramaters.Add("@Start", start);
                    paramaters.Add("@Offset", offset);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<O_StudentList>("SP2_O_Student", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }
            }
            catch { return new List<O_StudentList>(); }
        }
        private IEnumerable<OptionString> SqlRoleTeacherList(int teacherId)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "ROLE_GIAOVIEN");
                    paramaters.Add("@Id", GetUser().teacherInfo.Id);
                    paramaters.Add("@TeacherId", teacherId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<OptionString>("SP2_O_Teacher", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }
            }
            catch { return new List<OptionString>(); }
        }
    }
}