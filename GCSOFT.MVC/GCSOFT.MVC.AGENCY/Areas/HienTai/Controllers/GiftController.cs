﻿using Dapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.ViewModels.HienTai;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using System;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.HienTai.Controllers
{
    [CompressResponseAttribute]
    public class GiftController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IOptionLineService _optionLineService;
        private string sysCategory = "HIENTAI_GIFT";
        private bool? permission = false;
        private O_GiftInfo giftInfo;
        private O_GiftDetails giftDetails;
        private O_GiftCreateEdit giftCreateEdit;
        private string hienTaiConnection = ConfigurationManager.ConnectionStrings["HienTaiConnection"].ConnectionString;
        #endregion

        #region -- Contructor --
        public GiftController
            (
                IVuViecService vuViecService
                , IOptionLineService optionLineService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _optionLineService = optionLineService;
        }
        #endregion
        public ActionResult Index(int? currPage, string code, string title, string des)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var giftList = new O_GiftListPage();
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();

                //-- Paging
                int pageString = currPage ?? 0;
                int _currPage = pageString == 0 ? 1 : pageString;
                paramaters = new DynamicParameters();
                paramaters.Add("@Action", "TOTAL.ROW");
                paramaters.Add("@AgencyID", GetUser().teacherInfo.AgencyId);
                paramaters.Add("@Code", code ?? string.Empty);
                paramaters.Add("@Title", title ?? string.Empty);
                paramaters.Add("@Description", des ?? string.Empty);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var recordOfPageResults = conn.Query<RecordOfPage>("SP2_O_Gift", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                int totalRecord = recordOfPageResults.FirstOrDefault().TotalRow;
                giftList.paging = new Paging(totalRecord, _currPage);
                //-- Paging +

                paramaters.Add("@Action", "FINDALL");
                paramaters.Add("@AgencyID", GetUser().teacherInfo.AgencyId);
                paramaters.Add("@Start", giftList.paging.start);
                paramaters.Add("@Offset", giftList.paging.offset);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var results = conn.Query<O_GiftList>("SP2_O_Gift", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                giftList.GiftList = results.ToList();
            }
            return View(giftList);
        }
        public ActionResult Details(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.ViewDetail);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@Action", "GETBY");
                paramaters.Add("@Id", id);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var results = conn.Query<O_GiftDetails>("SP2_O_Gift", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                giftDetails = results.FirstOrDefault();
            }
            return View(giftDetails);
        }
        public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            giftCreateEdit = new O_GiftCreateEdit();
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters = new DynamicParameters();
                paramaters.Add("@Action", "GETMAX");
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var maxCode = conn.Query<MaxCode>("SP2_O_Gift", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                giftCreateEdit.Code = StringUtil.GetProductID("G", maxCode.FirstOrDefault().Code, 4);
                giftCreateEdit.DateInput = DateTime.Now;
                giftCreateEdit.DateInputString = string.Format("{0:dd/MM/yyyy}", giftCreateEdit.DateInput);
            }
            return View(giftCreateEdit);
        }
        public ActionResult Edit(int Id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@Action", "GETBY");
                paramaters.Add("@Id", Id);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var results = conn.Query<O_GiftCreateEdit>("SP2_O_Gift", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                giftCreateEdit = results.FirstOrDefault();
                giftCreateEdit.DateInputString = string.Format("{0:dd/MM/yyyy}", giftCreateEdit.DateInput);
            }
            return View(giftCreateEdit);
        }
        [HttpPost]
        public ActionResult Create(O_GiftCreateEdit _giftCreateEdit, HttpPostedFileBase file)
        {
            try
            {
                if (file != null)
                {
                    _giftCreateEdit.ImageName = Path.GetFileName(file.FileName);
                    _giftCreateEdit.ImagePath = string.Concat("~/Content/cms/images/gifts/", Path.GetFileName(file.FileName));
                }
                SetData(_giftCreateEdit, true);
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "INSERT");
                    foreach (PropertyInfo propertyInfo in giftInfo.GetType().GetProperties())
                    {
                        paramaters.Add("@" + propertyInfo.Name, propertyInfo.GetValue(giftInfo, null));
                    }
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP2_O_Gift", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    if (result == 1)
                    {
                        if (file != null)
                        {
                            string pic = Path.GetFileName(file.FileName);
                            string path = Path.Combine(Server.MapPath("~/Content/cms/images/gifts"), pic);
                            file.SaveAs(path);
                        }
                    }
                    return RedirectToAction("Edit", new { id = paramaters.Get<Int32>("@RetCode"), msg = "1" });
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult Edit(O_GiftCreateEdit _giftCreateEdit, HttpPostedFileBase file)
        {
            try
            {
                if (file != null)
                {
                    var filePath = Server.MapPath(_giftCreateEdit.ImagePath);
                    if (System.IO.File.Exists(filePath))
                    {
                        System.IO.File.Delete(filePath);
                    }
                    _giftCreateEdit.ImageName = Path.GetFileName(file.FileName);
                    _giftCreateEdit.ImagePath = string.Concat("~/Content/cms/images/gifts/", Path.GetFileName(file.FileName));
                }
                SetData(_giftCreateEdit, false);
                try
                {
                    using (var conn = new SqlConnection(hienTaiConnection))
                    {
                        if (conn.State == System.Data.ConnectionState.Closed)
                            conn.Open();
                        var paramaters = new DynamicParameters();
                        paramaters.Add("@Action", "UPDATE");
                        foreach (PropertyInfo propertyInfo in giftInfo.GetType().GetProperties())
                        {
                            paramaters.Add("@" + propertyInfo.Name, propertyInfo.GetValue(giftInfo, null));
                        }
                        paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                        paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                        var result = conn.Execute("SP2_O_Gift", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                        if (result == 1)
                        {
                            if (file != null)
                            {
                                string pic = Path.GetFileName(file.FileName);
                                string path = Path.Combine(Server.MapPath("~/Content/cms/images/gifts"), pic);
                                file.SaveAs(path);
                            }
                        }
                        return RedirectToAction("Edit", new { id = giftInfo.Id, msg = "1" });
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.Error = ex.ToString();
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public ActionResult Deletes(int id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();

                    paramaters.Add("@Action", "GETBY");
                    paramaters.Add("@Id", id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var results = conn.Query<O_GiftCreateEdit>("SP2_O_Gift", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    var imagePath = results.FirstOrDefault().ImagePath;

                    paramaters.Add("@Action", "DELETE");
                    paramaters.Add("@Id", id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP2_O_Gift", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    if (result == 1)
                    {
                        var filePath = Server.MapPath(imagePath);
                        if (System.IO.File.Exists(filePath))
                        {
                            System.IO.File.Delete(filePath);
                        }
                    }
                }
                return Json("Xóa dữ liệu không thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }
        private void SetData(O_GiftCreateEdit _giftCreateEdit, bool isCreate)
        {
            if (isCreate)
            {
                giftInfo = new O_GiftInfo();
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "GETMAX");
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var results = conn.Query<MaxCode>("SP2_O_Gift", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    giftInfo.Code = StringUtil.GetProductID("C", results.FirstOrDefault().Code, 4);
                    giftInfo.DateInput = DateTime.Parse(StringUtil.ConvertStringToDate(_giftCreateEdit.DateInputString));
                }
            }
            else
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "GETBY");
                    paramaters.Add("@Id", _giftCreateEdit.Id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var results = conn.Query<O_GiftInfo>("SP2_O_Gift", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    giftInfo = results.FirstOrDefault();
                }
            }

            giftInfo.AgencyId = GetUser().teacherInfo.AgencyId;
            giftInfo.Title = _giftCreateEdit.Title;
            giftInfo.Description = _giftCreateEdit.Description;
            giftInfo.ImageName = _giftCreateEdit.ImageName;
            giftInfo.ImagePath = _giftCreateEdit.ImagePath;
            giftInfo.Quantily = _giftCreateEdit.Quantily;
            giftInfo.IsActive = _giftCreateEdit.IsActive;
            giftInfo.AgencyId = GetUser().teacherInfo.AgencyId;
            giftInfo.DateInput = DateTime.Parse(StringUtil.ConvertStringToDate(_giftCreateEdit.DateInputString));
        }
    }
}