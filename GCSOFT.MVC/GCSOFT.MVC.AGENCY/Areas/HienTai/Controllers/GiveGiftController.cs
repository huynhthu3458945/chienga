﻿using Dapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Helper;
using GCSOFT.MVC.AGENCY.ViewModels.HienTai;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.StoreProcedue;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.HienTai.Controllers
{
    [CompressResponseAttribute]
    public class GiveGiftController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IOptionLineService _optionLineService;
        private string sysCategory = "HIENTAI_GIVEGIFT";
        private bool? permission = false;
        private O_GiveGiftInfo giveGiftInfo;
        private O_GiveGiftDetails giveGiftDetails;
        private O_GiveGiftCreateEdit giveGiftCreateEdit;
        private string hienTaiConnection = ConfigurationManager.ConnectionStrings["HienTaiConnection"].ConnectionString;
        #endregion

        #region -- Contructor --
        public GiveGiftController
            (
                IVuViecService vuViecService
                , IOptionLineService optionLineService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _optionLineService = optionLineService;
        }
        #endregion
        public ActionResult Index(int? currPage, string code, int? student, int? gift)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var giveGiftList = new O_GiveGiftListPage();
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();

                //-- Paging
                int pageString = currPage ?? 0;
                int _currPage = pageString == 0 ? 1 : pageString;
                paramaters = new DynamicParameters();
                paramaters.Add("@Action", "TOTAL.ROW");
                paramaters.Add("@AgencyID", GetUser().teacherInfo.AgencyId);
                paramaters.Add("@Code", code ?? string.Empty);
                paramaters.Add("@GiftId", gift);
                paramaters.Add("@StudentId", student);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var recordOfPageResults = conn.Query<RecordOfPage>("SP2_O_GiveGift", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                int totalRecord = recordOfPageResults.FirstOrDefault().TotalRow;
                giveGiftList.paging = new Paging(totalRecord, _currPage);
                //-- Paging +

                paramaters.Add("@Action", "FINDALL");
                paramaters.Add("@AgencyID", GetUser().teacherInfo.AgencyId);
                paramaters.Add("@Start", giveGiftList.paging.start);
                paramaters.Add("@Offset", giveGiftList.paging.offset);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var results = conn.Query<O_GiveGiftList>("SP2_O_GiveGift", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                giveGiftList.GiveGiftList = results.ToList();

                paramaters.Add("@Action", "STUDENT.LIST");
                paramaters.Add("@AgencyID", GetUser().teacherInfo.AgencyId);
                var teacherList = conn.Query<AgencyParentList>("SP2_O_GiveGift", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                giveGiftList.StudentList = teacherList.ToSelectListItems(0);
                giveGiftList.StudentId = student ?? 0;

                paramaters.Add("@Action", "GIFT.LIST");
                paramaters.Add("@AgencyID", GetUser().teacherInfo.AgencyId);
                var classList = conn.Query<AgencyParentList>("SP2_O_GiveGift", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                giveGiftList.GiftList = classList.ToSelectListItems(0);
                giveGiftList.GiftId = gift ?? 0;
            }
            return View(giveGiftList);
        }
        public ActionResult Details(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.ViewDetail);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@Action", "DETAILS");
                paramaters.Add("@Id", id);
                paramaters.Add("@AgencyID", GetUser().teacherInfo.AgencyId);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var results = conn.Query<O_GiveGiftDetails>("SP2_O_GiveGift", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                giveGiftDetails = results.FirstOrDefault();
                giveGiftDetails.ReceivedString = giveGiftDetails.IsReceived == true ? "Đã Nhận Quà" : "Chưa Nhận Quà";
            }
            return View(giveGiftDetails);
        }
        public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            giveGiftCreateEdit = new O_GiveGiftCreateEdit();
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters = new DynamicParameters();
                paramaters.Add("@Action", "GETMAX");
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var maxCode = conn.Query<MaxCode>("SP2_O_GiveGift", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                giveGiftCreateEdit.Code = StringUtil.GetProductID("G", maxCode.FirstOrDefault().Code, 4);

                paramaters.Add("@Action", "STUDENT.LIST");
                paramaters.Add("@AgencyID", GetUser().teacherInfo.AgencyId);
                var teacherList = conn.Query<AgencyParentList>("SP2_O_GiveGift", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                giveGiftCreateEdit.StudentList = teacherList.ToSelectListItems(0);

                paramaters.Add("@Action", "GIFT.LIST");
                paramaters.Add("@AgencyID", GetUser().teacherInfo.AgencyId);
                var classList = conn.Query<AgencyParentList>("SP2_O_GiveGift", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                giveGiftCreateEdit.GiftList = classList.ToSelectListItems(0);
            }
            return View(giveGiftCreateEdit);
        }
        public ActionResult Edit(int Id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@Action", "GETBY");
                paramaters.Add("@Id", Id);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var results = conn.Query<O_GiveGiftCreateEdit>("SP2_O_GiveGift", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                giveGiftCreateEdit = results.FirstOrDefault();

                paramaters.Add("@Action", "STUDENT.LIST");
                paramaters.Add("@AgencyID", GetUser().teacherInfo.AgencyId);
                var teacherList = conn.Query<AgencyParentList>("SP2_O_GiveGift", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                giveGiftCreateEdit.StudentList = teacherList.ToSelectListItems(0);

                paramaters.Add("@Action", "GIFT.LIST");
                paramaters.Add("@AgencyID", GetUser().teacherInfo.AgencyId);
                var classList = conn.Query<AgencyParentList>("SP2_O_GiveGift", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                giveGiftCreateEdit.GiftList = classList.ToSelectListItems(0);
            }
            return View(giveGiftCreateEdit);
        }
        [HttpPost]
        public ActionResult Create(O_GiveGiftCreateEdit _giftCreateEdit)
        {
            try
            {
                SetData(_giftCreateEdit, true);
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "INSERT");
                    foreach (PropertyInfo propertyInfo in giveGiftInfo.GetType().GetProperties())
                    {
                        paramaters.Add("@" + propertyInfo.Name, propertyInfo.GetValue(giveGiftInfo, null));
                    }
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP2_O_GiveGift", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return RedirectToAction("Edit", new { id = paramaters.Get<Int32>("@RetCode"), msg = "1" });
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult Edit(O_GiveGiftCreateEdit _giftCreateEdit)
        {
            try
            {
                SetData(_giftCreateEdit, false);
                try
                {
                    using (var conn = new SqlConnection(hienTaiConnection))
                    {
                        if (conn.State == System.Data.ConnectionState.Closed)
                            conn.Open();
                        var paramaters = new DynamicParameters();
                        paramaters.Add("@Action", "UPDATE");
                        foreach (PropertyInfo propertyInfo in giveGiftInfo.GetType().GetProperties())
                        {
                            paramaters.Add("@" + propertyInfo.Name, propertyInfo.GetValue(giveGiftInfo, null));
                        }
                        paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                        paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                        var result = conn.Execute("SP2_O_GiveGift", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                        return RedirectToAction("Edit", new { id = giveGiftInfo.Id, msg = "1" });
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.Error = ex.ToString();
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public ActionResult Deletes(int id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "DELETE");
                    paramaters.Add("@Id", id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP2_O_GiveGift", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                }
                return Json("Xóa dữ liệu không thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }
        private void SetData(O_GiveGiftCreateEdit _giftCreateEdit, bool isCreate)
        {
            if (isCreate)
            {
                giveGiftInfo = new O_GiveGiftInfo();
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "GETMAX");
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var results = conn.Query<MaxCode>("SP2_O_GiveGift", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    giveGiftInfo.Code = StringUtil.GetProductID("C", results.FirstOrDefault().Code, 4);
                }
            }
            else
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "GETBY");
                    paramaters.Add("@Id", _giftCreateEdit.Id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var results = conn.Query<O_GiveGiftInfo>("SP2_O_GiveGift", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    giveGiftInfo = results.FirstOrDefault();
                }
            }

            giveGiftInfo.Description = _giftCreateEdit.Description;
            giveGiftInfo.StudentId = _giftCreateEdit.StudentId;
            giveGiftInfo.GiftId = _giftCreateEdit.GiftId;
            giveGiftInfo.Quantity = _giftCreateEdit.Quantity;
            giveGiftInfo.IsReceived = _giftCreateEdit.IsReceived;
            giveGiftInfo.Reason = _giftCreateEdit.Reason;
            giveGiftInfo.AgencyId = GetUser().teacherInfo.AgencyId;
        }
    }
}