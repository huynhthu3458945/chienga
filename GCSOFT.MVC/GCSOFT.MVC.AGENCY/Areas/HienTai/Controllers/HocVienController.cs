﻿using AutoMapper;
using ClosedXML.Excel;
using Dapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Areas.Agency.Data;
using GCSOFT.MVC.AGENCY.Helper;
using GCSOFT.MVC.AGENCY.ViewModels.HienTai;
using GCSOFT.MVC.AGENCY.ViewModels.MasterData;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.HienTai.Controllers
{
    [CompressResponseAttribute]
    public class HocVienController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IOptionLineService _optionLineService;
        private readonly IClassService _classService;
        private readonly IMailSetupService _mailSetupService;
        private readonly ITemplateService _templateService;
        private readonly IAgencyService _agencyService;
        private string sysCategory = "HIENTAI_HOCSINH";
        private bool? permission = false;
        private string hasValue;
        private O_StudentInfo studentInfo;
        private O_StudentDetails studentDetails;
        private O_StudentCreateEdit studentCreateEdit;
        private string hienTaiConnection = ConfigurationManager.ConnectionStrings["HienTaiConnection"].ConnectionString;
        #endregion
        public HocVienController
            (
                IVuViecService vuViecService
                , IOptionLineService optionLineService
                , IClassService classService
                , IMailSetupService mailSetupService
                , ITemplateService templateService
                , IAgencyService agencyService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _optionLineService = optionLineService;
            _classService = classService;
            _mailSetupService = mailSetupService;
            _templateService = templateService;
            _agencyService = agencyService;
        }
        public ActionResult Index(int? currPage, string code, string fullName, int? schoolClassId, int? TeacherId)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var sqlHelper = new SqlExecHelper();
            int pageString = currPage ?? 0;
            int _currPage = pageString == 0 ? 1 : pageString;
            var studentPage = new O_StudentPage();
            studentPage.SchoolClassId = schoolClassId ?? 0;
            studentPage.SchoolClassList = SqlSchoolClassStarList().ToSelectListItems(schoolClassId ?? 0);
            studentPage.TeacherList = sqlHelper.SqlTeacherListList(GetUser().teacherInfo).ToSelectListItems(TeacherId ?? GetUser().teacherInfo.Id);
            int totalRecord = TotalRecord(code, fullName, schoolClassId, TeacherId ?? GetUser().teacherInfo.Id);
            studentPage.paging = new Paging(totalRecord, _currPage);
            studentPage.StudentList = StudentList(studentPage.paging.start, studentPage.paging.offset, code, fullName, schoolClassId, TeacherId ?? GetUser().teacherInfo.Id).ToList();
            
            return View(studentPage);
        }
        public ActionResult Details(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.ViewDetail);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@Action", "GETDETAILS");
                paramaters.Add("@Id", id);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var results = conn.Query<O_StudentDetails>("SP2_O_Student", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                studentDetails = results.FirstOrDefault();
                studentDetails.StudentStarList = SqlStudentStarList(id);
            }
            return View(studentDetails);
        }
        public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion

            //bo
            //var _NoOfLevelAndOutlier = ExecGetNoOfLevelAndOutlier(GetUser().teacherInfo.Id);
            //ViewBag.NoOfLevel = _NoOfLevelAndOutlier.NoOfLevel;
            //var _totalStudent = TotalStudent();
            //if (_totalStudent >= _NoOfLevelAndOutlier.NoOfOutlier)
            //{
            //    ViewBag.NoOfStudentTTL = _NoOfLevelAndOutlier.NoOfOutlier;
            //    ViewBag.NoOfStudentAgency = _totalStudent;
            //    return View("Permission");
            //}

            studentCreateEdit = new O_StudentCreateEdit();
            studentCreateEdit.AgencyCode = GetUser().teacherInfo.Code;
            var sqlHelper = new SqlExecHelper();
            studentCreateEdit.TeacherList = sqlHelper.SqlTeacherListList(GetUser().teacherInfo).ToSelectListItems(0);
            studentCreateEdit.AgencyNo = StringUtil.GetProductID("", GetMax(studentCreateEdit.AgencyCode), 3);
            studentCreateEdit.Code = GetStudentKey();
            studentCreateEdit.IDNo = studentCreateEdit.Code;
            studentCreateEdit.SchoolClassStarList = SqlSchoolClassStarList().ToSelectListItems(0);
            studentCreateEdit.StatusList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("STATUS_STUDENT")).ToSelectListItems("DHD");
            studentCreateEdit.GenderList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("GENDER")).ToSelectListItems("");
            studentCreateEdit.ShirtSizeList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("HT_SizeShirt")).ToSelectListItems("");
            studentCreateEdit.SchoolClassList = Mapper.Map<IEnumerable<VM_Class>>(_classService.FindAll()).ToList().ToSelectListItems(0);
            studentCreateEdit.ParentList = SqlParentList().ToSelectListItems(0);
            studentCreateEdit.LevelList = LevelStartList().ToSelectListItems(0);
            studentCreateEdit.AgencyList = sqlHelper.SqlGetAgency().ToSelectListItems(0);
            return View(studentCreateEdit);
        }
        public ActionResult Edit(int Id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            //var _NoOfLevelAndOutlier = ExecGetNoOfLevelAndOutlier(GetUser().teacherInfo.Id);
            //ViewBag.NoOfLevel = _NoOfLevelAndOutlier.NoOfLevel;
            studentCreateEdit = GetBy(Id);
            studentCreateEdit.SchoolClassStarList = SqlSchoolClassStarList().ToSelectListItems(studentCreateEdit.SchoolClassStarId);
            studentCreateEdit.StatusList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("STATUS_STUDENT")).ToSelectListItems(studentCreateEdit.Status);
            studentCreateEdit.GenderList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("GENDER")).ToSelectListItems(studentCreateEdit.Gender);
            studentCreateEdit.ShirtSizeList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("HT_SizeShirt")).ToSelectListItems(studentCreateEdit.ShirtSize);
            studentCreateEdit.SchoolClassList = Mapper.Map<IEnumerable<VM_Class>>(_classService.FindAll()).ToList().ToSelectListItems(studentCreateEdit.SchoolClassId);
            studentCreateEdit.ParentList = SqlParentList().ToSelectListItems(studentCreateEdit.ParentId);
            studentCreateEdit.StudentStarList = SqlStudentStarList(Id);
            studentCreateEdit.DateOfBirthStr = string.Format("{0:dd/MM/yyyy}", studentCreateEdit.DateOfBirth);
            studentCreateEdit.StartDateStr = string.Format("{0:dd/MM/yyyy}", studentCreateEdit.StartDate);
            studentCreateEdit.EndDateStr = string.Format("{0:dd/MM/yyyy}", studentCreateEdit.EndDate);
            var sqlHelper = new SqlExecHelper();
            studentCreateEdit.TeacherList = sqlHelper.SqlTeacherListList(GetUser().teacherInfo).ToSelectListItems(studentCreateEdit.TeacherId);
            studentCreateEdit.LevelList = LevelStartList().ToSelectListItems(0);
            studentCreateEdit.LevelArrs = StudentLevelList(Id).Select(d => d.LevelId).ToArray();
            studentCreateEdit.AgencyList = sqlHelper.SqlGetAgency().ToSelectListItems(studentCreateEdit.AgencyId);
            return View(studentCreateEdit);
        }
        [HttpPost]
        public ActionResult Create(O_StudentCreateEdit _studentCreateEdit)
        {
            try
            {
                SetData(_studentCreateEdit, true);
                var id = SqlInsert(studentInfo);
                hasValue = SendMKHHienTai(id);
                return RedirectToAction("Edit", new { id = id, msg = "1" });
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        [HttpPost]
        public ActionResult CreateAccountParent(int studentId)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion

            try
            {
                O_StudentCreateEdit _studentCreateEdit = new O_StudentCreateEdit();
                _studentCreateEdit = GetBy(studentId);
                _studentCreateEdit.StartDateStr = _studentCreateEdit.StartDate.ToString("dd/MM/yyyy");
                _studentCreateEdit.EndDateStr = _studentCreateEdit.EndDate.ToString("dd/MM/yyyy");
                _studentCreateEdit.LevelList = LevelStartList().ToSelectListItems(0);
                _studentCreateEdit.LevelArrs = StudentLevelList(studentId).Select(d => d.LevelId).ToArray();
                SetDataPH(_studentCreateEdit, true, studentId);

                //set get lai thong tin phu huynh
                O_ParentCreateEdit data = GetParentBy(_studentCreateEdit.ParentId);
                _studentCreateEdit.Id = 0;
                _studentCreateEdit.FullName = data.FullName;
                _studentCreateEdit.DateOfBirth = null;
                _studentCreateEdit.Gender = data.Gender;
                SetDataPH(_studentCreateEdit, true, studentId);
                var sqlHelper = new SqlExecHelper();
                
                var id = SqlInsertAccountParent(studentInfo);
                hasValue = SendMKHHienTai(id);
                return RedirectToAction("Edit", new { id = id, msg = "1" });
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        [HttpPost]
        public ActionResult Edit(O_StudentCreateEdit _studentCreateEdit)
        {
            try
            {
                try
                {
                    SetData(_studentCreateEdit, false);
                    var id = SqlUpdate(studentInfo);
                    return RedirectToAction("Edit", new { id = _studentCreateEdit.Id, msg = "1" });
                }
                catch (Exception ex)
                {
                    ViewBag.Error = ex.ToString();
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public ActionResult Deletes(int id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                hasValue = SqlDelete(id, "delete");
                return Json("Xóa dữ liệu thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }
        public ActionResult Block(int id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                hasValue = SqlDelete(id, "block");
                return Json("Khóa học viên thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }
        public ActionResult UnBlock(int id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                hasValue = SqlDelete(id, "unblock");
                return Json("Mở khóa học viên thành công !");
            }
            catch { return Json("Mở khóa học viên không thành công"); }
        }
        public JsonResult ReSendMKH(int Id)
        {
            try
            {
                hasValue = SendMKHHienTai(Id);
                //-- Gửi mail cho Tâm Trí Lực +
                var _status = string.IsNullOrEmpty(hasValue) ? "Oke" : "Error";
                var _msg = string.IsNullOrEmpty(hasValue) ? "" : hasValue;
                return Json(new { s = _status, m = _msg }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { s = "Error", m = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SchoolClassInfo(int schoolClassId)
        {
            var _levelArr = LevelStarList(schoolClassId).Select(d => d.LevelId).ToArray();
            return Json(new { schoolClassInfo = ExecSchoolClassInfo(schoolClassId), levelArr = _levelArr });
        }
        public ActionResult AccountInfo(int id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.ViewDetail);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
           List<StudentAccount> data = (StudentAccount(id)).ToList();
            return PartialView("_AccountInfo", data);
        }

        public ActionResult ExportStudent(string code, string fullName, int? schoolClassId, int? TeacherId)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.ExportExcel);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion

            List<O_StudentExport> data = new List<O_StudentExport>();
            data = StudentExport(code, fullName, schoolClassId, TeacherId ?? GetUser().teacherInfo.Id).ToList();
            //create data
            DataTable dt = new DataTable();
            //Setiing Table Name  
            dt.TableName = "ExportStudent";
            //Add Columns  
            dt.Columns.Add("STT", typeof(int));
            dt.Columns.Add("StudentId", typeof(int));
            dt.Columns.Add("SBD", typeof(string));
            dt.Columns.Add("Lop", typeof(string));
            dt.Columns.Add("GiaoVien", typeof(string));
            dt.Columns.Add("HienTai", typeof(string));
            dt.Columns.Add("Level", typeof(string));
            dt.Columns.Add("NgayBatDau", typeof(string));
            dt.Columns.Add("NgayKetThuc", typeof(string));
            dt.Columns.Add("GioiTinh", typeof(string));
            dt.Columns.Add("SinhNhat", typeof(string));
            dt.Columns.Add("HoVaTenPhuHuynh", typeof(string));
            dt.Columns.Add("DienThoai", typeof(string));
            dt.Columns.Add("Email", typeof(string));
            dt.Columns.Add("DiaChi", typeof(string));
            dt.Columns.Add("SizeAo", typeof(string));
            if (data.Count() > 0)
            {
                int stt = 1;
                foreach (var item in data)
                {
                    dt.Rows.Add(stt,item.Id, item.IDNo, item.SchoolClassName, item.TeacherName, item.FullName, item.Lever
                        , item.OpeningDay, item.ClosingDay, item.Gender, item.DateOfBirth, item.ParentName, item.Phone, item.Email, item.Address, item.ShirtSize);
                    stt++;
                }
            }
            dt.AcceptChanges();

            //Name of File  
            string fileName = "ExportStudent.xlsx";
            using (XLWorkbook wb = new XLWorkbook())
            {
                //Add DataTable in worksheet  
                wb.Worksheets.Add(dt);
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    //Return xlsx Excel File  
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
                }
            }
        }

        #region -- Functioin --
        private int TotalRecord(string code, string fullName, int? schoolClassId, int teacherId)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "TOTAL.ROW");
                    paramaters.Add("@AgencyID", GetUser().teacherInfo.AgencyId);
                    paramaters.Add("@Code", code ?? string.Empty);
                    paramaters.Add("@FullName", fullName ?? string.Empty);
                    paramaters.Add("@SchoolClassId", schoolClassId);
                    paramaters.Add("@TeacherId", teacherId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var recordOfPageResults = conn.Query<RecordOfPage>("SP2_O_Student", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return recordOfPageResults.FirstOrDefault().TotalRow;
                }
            }
            catch { return 0; }
        }
        private int TotalStudent()
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "TOTAL.STUDENT");
                    paramaters.Add("@TeacherId", GetUser().teacherInfo.Id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var recordOfPageResults = conn.Query<RecordOfPage>("SP2_O_Student", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return recordOfPageResults.FirstOrDefault().TotalRow;
                }
            }
            catch { return 0; }
        }
        private IEnumerable<O_StudentList> StudentList(int start, int offset, string code, string fullName, int? schoolClassId, int teacherId)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "FINDALL");
                    paramaters.Add("@AgencyID", GetUser().teacherInfo.AgencyId);
                    paramaters.Add("@Code", code ?? string.Empty);
                    paramaters.Add("@FullName", fullName ?? string.Empty);
                    paramaters.Add("@SchoolClassId", schoolClassId ?? 0);
                    paramaters.Add("@TeacherId", teacherId);
                    paramaters.Add("@Start", start);
                    paramaters.Add("@Offset", offset);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<O_StudentList>("SP2_O_Student", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }
            }
            catch { return new List<O_StudentList>(); }
        }
        private IEnumerable<O_StudentExport> StudentExport(string code, string fullName, int? schoolClassId, int teacherId)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "FINEXPORT");
                    paramaters.Add("@AgencyID", GetUser().teacherInfo.AgencyId);
                    paramaters.Add("@Code", code ?? string.Empty);
                    paramaters.Add("@FullName", fullName ?? string.Empty);
                    paramaters.Add("@SchoolClassId", schoolClassId ?? 0);
                    paramaters.Add("@TeacherId", teacherId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<O_StudentExport>("SP2_O_Student", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }
            }
            catch { return new List<O_StudentExport>(); }
        }
        private string GetMax(string AgencyCode)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "GETMAX");
                    paramaters.Add("@AgencyCode", AgencyCode);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var recordOfPageResults = conn.Query<MaxCode>("SP2_O_Student", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return recordOfPageResults.FirstOrDefault().Code;
                }
            }
            catch { return ""; }
        }

        private string GetStudentKey()
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "GETKEYSTUDENT");
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var recordOfPageResults = conn.Query<MaxCode>("SP2_O_Student", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return recordOfPageResults.FirstOrDefault().Code;
                }
            }
            catch { return ""; }
        }

        private O_StudentCreateEdit GetBy(int id)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "GETBY");
                    paramaters.Add("@Id", id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<O_StudentCreateEdit>("SP2_O_Student", paramaters, null, true, null, System.Data.CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch { return new O_StudentCreateEdit(); }
        }
        private int SqlInsert(O_StudentInfo studentInfo)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "INSERT");
                    foreach (PropertyInfo propertyInfo in studentInfo.GetType().GetProperties())
                    {
                        paramaters.Add("@" + propertyInfo.Name, propertyInfo.GetValue(studentInfo, null));
                    }
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP2_O_Student", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return paramaters.Get<Int32>("@RetCode");
                }
            }
            catch { return 0; }
        }
        private int SqlInsertAccountParent(O_StudentInfo studentInfo)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "INSERT_ACCOUNT_PARENT");
                    foreach (PropertyInfo propertyInfo in studentInfo.GetType().GetProperties())
                    {
                        paramaters.Add("@" + propertyInfo.Name, propertyInfo.GetValue(studentInfo, null));
                    }
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP2_O_Student", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return paramaters.Get<Int32>("@RetCode");
                }
            }
            catch { return 0; }
        }
        private int SqlUpdate(O_StudentInfo studentInfo)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "UPDATE");
                    foreach (PropertyInfo propertyInfo in studentInfo.GetType().GetProperties())
                    {
                        paramaters.Add("@" + propertyInfo.Name, propertyInfo.GetValue(studentInfo, null));
                    }
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP2_O_Student", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return studentInfo.Id;
                }
            }
            catch { return 0; }
        }
        private string SqlDelete(int id, string type)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    if (type.Equals("delete"))
                        paramaters.Add("@Action", "DELETE");
                    else if (type.Equals("block"))
                        paramaters.Add("@Action", "BLOCK");
                    else
                        paramaters.Add("@Action", "UNBLOCK");
                    paramaters.Add("@Id", id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP2_O_Student", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return "";
                }
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        private void SetData(O_StudentCreateEdit _studentCreateEdit, bool isCreate)
        {
            if (isCreate)
            {
                studentInfo = new O_StudentInfo();
                studentInfo.AgencyCode = GetUser().teacherInfo.Code;
                studentInfo.AgencyNo = StringUtil.GetProductID("", GetMax(studentInfo.AgencyCode), 3);
                studentInfo.Code = GetStudentKey();
                studentInfo.IDNo = studentInfo.Code;
            }
            else
            {
                studentInfo = Mapper.Map<O_StudentInfo>(GetBy(_studentCreateEdit.Id));
                studentInfo.IDNo = _studentCreateEdit.IDNo;
            }
            if (!string.IsNullOrEmpty(_studentCreateEdit.DateOfBirthStr))
                _studentCreateEdit.DateOfBirth = DateTime.Parse(StringUtil.ConvertStringToDate(_studentCreateEdit.DateOfBirthStr));
            else
                _studentCreateEdit.DateOfBirth = null;

            studentInfo.FullName = _studentCreateEdit.FullName;
            studentInfo.DateOfBirth = _studentCreateEdit.DateOfBirth;
            studentInfo.StartDate = DateTime.Parse(StringUtil.ConvertStringToDate(_studentCreateEdit.StartDateStr));
            studentInfo.EndDate = DateTime.Parse(StringUtil.ConvertStringToDate(_studentCreateEdit.EndDateStr));
            studentInfo.ShirtSize = _studentCreateEdit.ShirtSize;
            studentInfo.SchoolName = _studentCreateEdit.SchoolName;
            studentInfo.SchoolClassId = _studentCreateEdit.SchoolClassId;
            studentInfo.ParentId = _studentCreateEdit.ParentId;
            studentInfo.Gender = _studentCreateEdit.Gender;
            studentInfo.SchoolClassStarId = _studentCreateEdit.SchoolClassStarId;
            studentInfo.Status = _studentCreateEdit.Status;
            studentInfo.AgencyId = _studentCreateEdit.AgencyId;
            studentInfo.TeacherId = _studentCreateEdit.TeacherId;
            studentInfo.LevelArr = string.Join(",", _studentCreateEdit.LevelArrs);
            
        }
        private void SetDataPH(O_StudentCreateEdit _studentCreateEdit, bool isCreate, int studentId)
        {
            if (isCreate)
            {
                studentInfo = new O_StudentInfo();
                studentInfo.AgencyCode = GetUser().teacherInfo.Code;
                studentInfo.AgencyNo = StringUtil.GetProductID("", GetMax(studentInfo.AgencyCode), 3);
                studentInfo.Code = string.Format("PH{0}", _studentCreateEdit.IDNo); // tai khoan phu huynh  = PH + SBD HienTai
                studentInfo.IDNo = studentInfo.Code;
            }
            else
            {
                studentInfo = Mapper.Map<O_StudentInfo>(GetBy(_studentCreateEdit.Id));
                studentInfo.IDNo = _studentCreateEdit.IDNo;
            }
            if (!string.IsNullOrEmpty(_studentCreateEdit.DateOfBirthStr))
                _studentCreateEdit.DateOfBirth = DateTime.Parse(StringUtil.ConvertStringToDate(_studentCreateEdit.DateOfBirthStr));
            else
                _studentCreateEdit.DateOfBirth = null;

            studentInfo.FullName = _studentCreateEdit.FullName;
            studentInfo.DateOfBirth = _studentCreateEdit.DateOfBirth;
            studentInfo.StartDate = DateTime.Parse(StringUtil.ConvertStringToDate(_studentCreateEdit.StartDateStr));
            studentInfo.EndDate = DateTime.Parse(StringUtil.ConvertStringToDate(_studentCreateEdit.EndDateStr));
            studentInfo.ShirtSize = _studentCreateEdit.ShirtSize;
            studentInfo.SchoolName = _studentCreateEdit.SchoolName;
            studentInfo.SchoolClassId = _studentCreateEdit.SchoolClassId;
            studentInfo.ParentId = _studentCreateEdit.ParentId;
            studentInfo.Gender = _studentCreateEdit.Gender;
            studentInfo.SchoolClassStarId = _studentCreateEdit.SchoolClassStarId;
            studentInfo.Status = _studentCreateEdit.Status;
            studentInfo.AgencyId = GetUser().teacherInfo.AgencyId;
            studentInfo.TeacherId = _studentCreateEdit.TeacherId;
            studentInfo.LevelArr = string.Join(",", _studentCreateEdit.LevelArrs);
            studentInfo.StudentOrigin = studentId;

        }
        private IEnumerable<StudentStar> SqlStudentStarList(int studentId)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "STUDENTSTAR");
                    paramaters.Add("@Id", studentId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<StudentStar>("SP2_O_Student", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }
            }
            catch { return new List<StudentStar>(); }
        }
        private StudentActiveInfo SqlStudentActiveInfo(int studentId)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "STUDENT_ACTION_INFO");
                    paramaters.Add("@Id", studentId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<StudentActiveInfo>("SP2_O_Student", paramaters, null, true, null, System.Data.CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch { return new StudentActiveInfo(); }
        }
        private IEnumerable<OptionInt> SqlSchoolClassStarList()
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "FINDALL2");
                    paramaters.Add("@AgencyId", GetUser().teacherInfo.AgencyId);
                    paramaters.Add("@TeacherId", GetUser().teacherInfo.Id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<OptionInt>("SP2_O_SchoolClass", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }
            }
            catch { return new List<OptionInt>(); }
        }
        private IEnumerable<OptionInt> SqlParentList()
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "FINDALL2");
                    paramaters.Add("@AgencyId", GetUser().teacherInfo.AgencyId);
                    paramaters.Add("@TeacherId", GetUser().teacherInfo.Id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<OptionInt>("SP2_O_Parent", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }
            }
            catch { return new List<OptionInt>(); }
        }
        private string SqlUpdateStatusSendSMS(int id)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "UPDATE_STATUS_SENDSMS");
                    paramaters.Add("@Id", id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP2_O_Student", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return "";
                }
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public string SendMKHHienTai(int studentId)
        {
            var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
            var mailTemplate = Mapper.Map<Template>(_templateService.Get("MKH_HIENTAI"));
            var studentActive = SqlStudentActiveInfo(studentId);
            var emailTos = new List<string>();
            emailTos.Add(studentActive.Email);
            var _mailHelper = new EmailHelper()
            {
                EmailTos = emailTos,
                DisplayName = emailInfo.DisplayName,
                Title = mailTemplate.Title,
                Body = mailTemplate.Content.Replace("[FullName]", studentActive.FullName).Replace("[SBD]", studentActive.IDNo).Replace("[MKH]", studentActive.ActivationCode),
                MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
            };
            string hasValue = _mailHelper.DoSendMail();
            //-- Send SMS
            var content = string.Format(mailTemplate.SMSContent, studentActive.IDNo, studentActive.ActivationCode);
            var smsHelper = new SMSHelper()
            {
                PhoneNo = studentActive.PhoneNumber,
                Content = content
            };
            hasValue = smsHelper.Send();
            //-- Send SMS +
            hasValue = SqlUpdateStatusSendSMS(studentId);
            return "";
        }
        private IEnumerable<LevelStarViewModel> LevelStartList()
        {
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@Action", "LEVEL_LIST");
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var results = conn.Query<LevelStarViewModel>("SP2_O_SchoolClass", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                return results.ToList();
            }
        }
        private IEnumerable<LevelStar> ListSchoolClassLevel(string[] levelArrs)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    if (levelArrs.Count() == 1 && levelArrs[0].Equals("1"))
                    {
                        paramaters.Add("@Action", "SCHOOLCLASS_LEVEL1");
                    }
                    else if (levelArrs.Count() == 1 && levelArrs[0].Equals("2"))
                    {
                        paramaters.Add("@Action", "SCHOOLCLASS_LEVEL2");
                    }
                    else
                    {
                        paramaters.Add("@Action", "SCHOOLCLASS_LEVEL1_2");
                    }
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<LevelStar>("SP2_O_SchoolClass", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }
            }
            catch { return new List<LevelStar>(); }
        }
        private IEnumerable<O_TeacherInfo> ListSchoolClassTeacher(int teacherId)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "GETCLASS.TEACHER");
                    paramaters.Add("@TeacherId", teacherId);
                    paramaters.Add("@AgencyId", GetUser().teacherInfo.AgencyId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var schoolClassStarList = conn.Query<O_TeacherInfo>("SP2_O_SchoolClass", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return schoolClassStarList;
                }
            }
            catch { return new List<O_TeacherInfo>(); }
        }
        public ActionResult LoadSchoolStarByLevel(int studentId, string levelArr, string startDate, string endDate)
        {
            return Json(new { status = true, data = ExecSchoolStarByLevel(studentId, levelArr, startDate, endDate) });
        }
        public ActionResult LoadSchoolClassStars(int schoolClassStarId)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();

                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "LEVELLISTBYSCHOOL");
                    paramaters.Add("@SchoolClassId", schoolClassStarId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var levelStarResults = conn.Query<LevelStar>("SP2_O_SchoolClass", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);

                    if (levelStarResults.Count() == 1 && levelStarResults.FirstOrDefault().LevelId == 1)
                    {
                        paramaters.Add("@Action", "SCHOOLSTAR_LEVEL_1");
                    }
                    else if (levelStarResults.Count() == 1 && levelStarResults.FirstOrDefault().LevelId == 2)
                    {
                        paramaters.Add("@Action", "SCHOOLSTAR_LEVEL_2");
                    }
                    else
                    {
                        paramaters.Add("@Action", "SCHOOLSTAR_LEVEL_1_2");
                    }
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var results = conn.Query<SchoolClassStar>("SP2_O_SchoolClass", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return Json(new { status = true, data = results });
                }
            }
            catch (Exception ex) { return Json(new { status = false }); }
        }
        private IEnumerable<StudentLevel> StudentLevelList(int studentId)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "STUDENTLEVEL");
                    paramaters.Add("@Id", studentId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var studentLevelList = conn.Query<StudentLevel>("SP2_O_Student", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return studentLevelList;
                }
            }
            catch { return new List<StudentLevel>(); }
        }
        private SchoolClassInfo ExecSchoolClassInfo(int schoolClassId)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "SCHOOL.CLASS.INFO");
                    paramaters.Add("@Id", schoolClassId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var sqlSchoolClassInfo = conn.Query<SchoolClassInfo>("SP2_O_SchoolClass", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return sqlSchoolClassInfo.FirstOrDefault();
                }
            }
            catch { return new SchoolClassInfo(); }
        }
        private NoOfLevelAndOutlier ExecGetNoOfLevelAndOutlier(int teacherId)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "NO.LEVEL.STUDENT");
                    paramaters.Add("@TeacherId", teacherId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var sqlNoOfLevelAndOutlier = conn.Query<NoOfLevelAndOutlier>("SP2_O_Teacher", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    var noOfResult = sqlNoOfLevelAndOutlier.FirstOrDefault();
                    if (noOfResult == null)
                        noOfResult = new NoOfLevelAndOutlier();
                    return noOfResult;
                }
            }
            catch { return new NoOfLevelAndOutlier(); }
        }
        private IEnumerable<LevelStar> LevelStarList(int schoolClassId)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "LEVELLISTBYSCHOOL");
                    paramaters.Add("@SchoolClassId", schoolClassId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var levelStarResults = conn.Query<LevelStar>("SP2_O_SchoolClass", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return levelStarResults.ToList();
                }
            }
            catch { return new List<LevelStar>(); }
        }
        private IEnumerable<StudentStar> ExecSchoolStarByLevel(int studentId, string levelArr, string startDate, string endDate)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "STAR.BY.LEVEL");
                    paramaters.Add("@Id", studentId);
                    paramaters.Add("@LevelArr", levelArr);
                    paramaters.Add("@StartDateStr", startDate);
                    paramaters.Add("@EndDateStr", endDate);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var levelStarResults = conn.Query<StudentStar>("SP2_O_Student", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return levelStarResults.ToList();
                }
            }
            catch { return new List<StudentStar>(); }
        }
        private IEnumerable<StudentAccount> StudentAccount(int id)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "STUDENT.ACCOUNT");
                    paramaters.Add("@Id", id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var studentAccount = conn.Query<StudentAccount>("SP2_O_Student", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return studentAccount;
                }
            }
            catch { return new List<StudentAccount>(); }
        }
        private O_ParentCreateEdit GetParentBy(int id)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "GETBY");
                    paramaters.Add("@Id", id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<O_ParentCreateEdit>("SP2_O_Parent", paramaters, null, true, null, System.Data.CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch { return new O_ParentCreateEdit(); }
        }
        #endregion
    }
}