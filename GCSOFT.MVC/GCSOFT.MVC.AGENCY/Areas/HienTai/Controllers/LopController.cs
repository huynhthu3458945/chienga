﻿using Dapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.ViewModels.HienTai;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.HienTai.Controllers
{
    [CompressResponseAttribute]
    public class LopController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IOptionLineService _optionLineService;
        private string sysCategory = "HIENTAI_DMLOP";
        private bool? permission = false;
        private O_ClassInfo classInfo;
        private O_ClassDetails classDetails;
        private O_ClassCreateEdit classCreateEdit;
        private string hienTaiConnection = ConfigurationManager.ConnectionStrings["HienTaiConnection"].ConnectionString;
        #endregion

        #region -- Contructor --
        public LopController
            (
                IVuViecService vuViecService
                , IOptionLineService optionLineService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _optionLineService = optionLineService;
        }
        #endregion
        public ActionResult Index(int? currPage, string code, string title, string des)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var teacherList = new O_ClassListPage();
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();

                //-- Paging
                int pageString = currPage ?? 0;
                int _currPage = pageString == 0 ? 1 : pageString;
                paramaters = new DynamicParameters();
                paramaters.Add("@Action", "TOTAL.ROW");
                paramaters.Add("@TeacherId", GetUser().teacherInfo.Id);
                paramaters.Add("@Code", code ?? string.Empty);
                paramaters.Add("@Title", title ?? string.Empty);
                paramaters.Add("@Description", des ?? string.Empty);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var recordOfPageResults = conn.Query<RecordOfPage>("SP2_O_Class", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                int totalRecord = recordOfPageResults.FirstOrDefault().TotalRow;
                teacherList.paging = new Paging(totalRecord, _currPage);
                //-- Paging +

                paramaters.Add("@Action", "FINDALL");
                paramaters.Add("@TeacherId", GetUser().teacherInfo.Id);
                paramaters.Add("@Start", teacherList.paging.start);
                paramaters.Add("@Offset", teacherList.paging.offset);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var results = conn.Query<O_ClassList>("SP2_O_Class", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                teacherList.ClasList = results.ToList();
            }
            return View(teacherList);
        }
        public ActionResult Details(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.ViewDetail);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@Action", "GETBY");
                paramaters.Add("@Id", id);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var results = conn.Query<O_ClassDetails>("SP2_O_Class", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                classDetails = results.FirstOrDefault();
            }
            return View(classDetails);
        }
        public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            classCreateEdit = new O_ClassCreateEdit();
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters = new DynamicParameters();
                paramaters.Add("@Action", "GETMAX");
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var maxCode = conn.Query<MaxCode>("SP2_O_Class", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                classCreateEdit.Code = StringUtil.GetProductID("C", maxCode.FirstOrDefault().Code, 4);
            }
            return View(classCreateEdit);
        }
        public ActionResult Edit(int Id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@Action", "GETBY");
                paramaters.Add("@Id", Id);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var results = conn.Query<O_ClassCreateEdit>("SP2_O_Class", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                classCreateEdit = results.FirstOrDefault();
            }
            return View(classCreateEdit);
        }
        [HttpPost]
        public ActionResult Create(O_ClassCreateEdit _ClassCreateEdit)
        {
            try
            {
                SetData(_ClassCreateEdit, true);
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "INSERT");
                    foreach (PropertyInfo propertyInfo in classInfo.GetType().GetProperties())
                    {
                        paramaters.Add("@" + propertyInfo.Name, propertyInfo.GetValue(classInfo, null));
                    }
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP2_O_Class", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return RedirectToAction("Edit", new { id = paramaters.Get<Int32>("@RetCode"), msg = "1" });
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult Edit(O_ClassCreateEdit _ClassCreateEdit)
        {
            try
            {
                SetData(_ClassCreateEdit, false);
                try
                {
                    using (var conn = new SqlConnection(hienTaiConnection))
                    {
                        if (conn.State == System.Data.ConnectionState.Closed)
                            conn.Open();
                        var paramaters = new DynamicParameters();
                        paramaters.Add("@Action", "UPDATE");
                        foreach (PropertyInfo propertyInfo in classInfo.GetType().GetProperties())
                        {
                            paramaters.Add("@" + propertyInfo.Name, propertyInfo.GetValue(classInfo, null));
                        }
                        paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                        paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                        var result = conn.Execute("SP2_O_Class", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                        return RedirectToAction("Edit", new { id = classInfo.Id, msg = "1" });
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.Error = ex.ToString();
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public ActionResult Deletes(int id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "DELETE");
                    paramaters.Add("@Id", id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP2_O_Class", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                }
                return Json("Xóa dữ liệu không thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }
        private void SetData(O_ClassCreateEdit _ClassCreateEdit, bool isCreate)
        {
            if (isCreate)
            {
                classInfo = new O_ClassInfo();
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "GETMAX");
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var results = conn.Query<MaxCode>("SP2_O_Class", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    classInfo.Code = StringUtil.GetProductID("C", results.FirstOrDefault().Code, 4);
                    classInfo.CreateBy = GetUser().teacherInfo.Id;
                    classInfo.CreatedDate = DateTime.Now;
                    classInfo.ModifyBy = GetUser().teacherInfo.Id;
                    classInfo.ModifyDate = DateTime.Now;
                }
            }
            else
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "GETBY");
                    paramaters.Add("@Id", _ClassCreateEdit.Id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var results = conn.Query<O_ClassInfo>("SP2_O_Class", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    classInfo = results.FirstOrDefault();
                    classInfo.ModifyBy = GetUser().teacherInfo.Id;
                    classInfo.ModifyDate = DateTime.Now;
                    classInfo.CreateBy = _ClassCreateEdit.CreateBy;
                    classInfo.CreatedDate = _ClassCreateEdit.CreatedDate;
                }
            }

            classInfo.Title = _ClassCreateEdit.Title;
            classInfo.Status = _ClassCreateEdit.Status;
            classInfo.Description = _ClassCreateEdit.Description;
            classInfo.AgencyId = GetUser().teacherInfo.AgencyId;
            classInfo.TeacherId = GetUser().teacherInfo.Id;
        }
    }
}