﻿using Dapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Helper;
using GCSOFT.MVC.AGENCY.ViewModels.HienTai;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.StoreProcedue;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.HienTai.Controllers
{
    [CompressResponseAttribute]
    public class LopHocController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IOptionLineService _optionLineService;
        private string sysCategory = "HIENTAI_LOP";
        private bool? permission = false;
        private string hasValue;
        private O_SchoolClassInfo schoolClassInfo;
        private O_SchoolClassDetails schoolClassDetails;
        private O_SchoolClassCreateEdit schoolClassCreateEdit;
        private string hienTaiConnection = ConfigurationManager.ConnectionStrings["HienTaiConnection"].ConnectionString;
        #endregion

        #region -- Contructor --
        public LopHocController
            (
                IVuViecService vuViecService
                , IOptionLineService optionLineService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _optionLineService = optionLineService;
        }
        #endregion

        public ActionResult Index(int? currPage, string code, string name, int? teacher)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var schoolClasList = new O_SchoolClassListPage();
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();


                schoolClasList.TeacherId = teacher ?? GetUser().teacherInfo.Id;
                var sqlHelper = new SqlExecHelper();
                schoolClasList.TeacherList = sqlHelper.SqlTeacherListList(GetUser().teacherInfo).ToSelectListItems(schoolClasList.TeacherId);

                var paramaters = new DynamicParameters();

                //-- Paging
                int pageString = currPage ?? 0;
                int _currPage = pageString == 0 ? 1 : pageString;
                paramaters = new DynamicParameters();
                paramaters.Add("@Action", "TOTAL.ROW");
                paramaters.Add("@AgencyID", GetUser().teacherInfo.AgencyId);
                paramaters.Add("@Code", code ?? string.Empty);
                paramaters.Add("@Name", name ?? string.Empty);
                paramaters.Add("@TeacherId", schoolClasList.TeacherId);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var recordOfPageResults = conn.Query<RecordOfPage>("SP2_O_SchoolClass", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                int totalRecord = recordOfPageResults.FirstOrDefault().TotalRow;
                schoolClasList.paging = new Paging(totalRecord, _currPage);
                //-- Paging +

                paramaters = new DynamicParameters();
                paramaters.Add("@Action", "FINDALL");
                paramaters.Add("@AgencyID", GetUser().teacherInfo.AgencyId);
                paramaters.Add("@Code", code ?? string.Empty);
                paramaters.Add("@Name", name ?? string.Empty);
                paramaters.Add("@TeacherId", schoolClasList.TeacherId);
                paramaters.Add("@Start", schoolClasList.paging.start);
                paramaters.Add("@Offset", schoolClasList.paging.offset);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var results = conn.Query<O_SchoolClassList>("SP2_O_SchoolClass", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                schoolClasList.SchoolClasList = results.ToList();

            }
            return View(schoolClasList);
        }
        public ActionResult Details(int id, int? currPage)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.ViewDetail);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@Action", "GETBY");
                paramaters.Add("@Id", id);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var results = conn.Query<O_SchoolClassDetails>("SP2_O_SchoolClass", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                schoolClassDetails = results.FirstOrDefault();

                paramaters.Add("@Action", "LEVELLISTBYSCHOOL");
                paramaters.Add("@SchoolClassId", id);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var levelStarResults = conn.Query<LevelStar>("SP2_O_SchoolClass", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                schoolClassDetails.LevelArrs = levelStarResults.Select(d => d.LevelId).ToArray();

                int pageString = currPage ?? 0;
                int _currPage = pageString == 0 ? 1 : pageString;
                int totalRecord = TotalRecord(id);
                schoolClassDetails.paging = new Paging(totalRecord, _currPage);
                schoolClassDetails.StudentList = StudentListByClassStar(schoolClassDetails.paging.start, schoolClassDetails.paging.offset, id).ToList();
            }
            return View(schoolClassDetails);
        }
        public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            schoolClassCreateEdit = new O_SchoolClassCreateEdit();
            schoolClassCreateEdit.Code = StringUtil.CheckLetter("K", GetMax(), 4);
            var sqlHelper = new SqlExecHelper();
            schoolClassCreateEdit.TeacherList = sqlHelper.SqlTeacherListList(GetUser().teacherInfo).ToSelectListItems(0);
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@Action", "CLASS.LIST");
                paramaters.Add("@TeacherId", GetUser().teacherInfo.Id);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var classList = conn.Query<AgencyParentList>("SP2_O_Class", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                schoolClassCreateEdit.ClassList = classList.ToSelectListItems(0);

                schoolClassCreateEdit.OpeningDay = DateTime.Now;
                schoolClassCreateEdit.OpeningDayString = string.Format("{0:dd/MM/yyyy}", schoolClassCreateEdit.OpeningDay);
                schoolClassCreateEdit.ClosingDay = DateTime.Now;
                schoolClassCreateEdit.ClosingDayString = string.Format("{0:dd/MM/yyyy}", schoolClassCreateEdit.ClosingDay);
            }
            schoolClassCreateEdit.LevelList = LevelStartList().ToSelectListItems(0);
            return View(schoolClassCreateEdit);
        }
        public ActionResult Edit(int Id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@Action", "GETBY");
                paramaters.Add("@Id", Id);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var results = conn.Query<O_SchoolClassCreateEdit>("SP2_O_SchoolClass", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                schoolClassCreateEdit = results.FirstOrDefault();

                var sqlHelper = new SqlExecHelper();
                schoolClassCreateEdit.TeacherList = sqlHelper.SqlTeacherListList(GetUser().teacherInfo).ToSelectListItems(schoolClassCreateEdit.TeacherId ?? 0);

                paramaters.Add("@Action", "CLASS.LIST");
                paramaters.Add("@TeacherId", GetUser().teacherInfo.Id);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var classList = conn.Query<AgencyParentList>("SP2_O_Class", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                schoolClassCreateEdit.ClassList = classList.ToSelectListItems(schoolClassCreateEdit.ClassId);

                paramaters.Add("@Action", "LEVELLISTBYSCHOOL");
                paramaters.Add("@SchoolClassId", schoolClassCreateEdit.Id);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var levelStarResults = conn.Query<LevelStar>("SP2_O_SchoolClass", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                schoolClassCreateEdit.LevelArrs = levelStarResults.Select(d => d.LevelId).ToArray();
            }
            schoolClassCreateEdit.OpeningDayString = string.Format("{0:dd/MM/yyyy}", schoolClassCreateEdit.OpeningDay);
            schoolClassCreateEdit.ClosingDayString = string.Format("{0:dd/MM/yyyy}", schoolClassCreateEdit.ClosingDay);
            schoolClassCreateEdit.LevelList = LevelStartList().ToSelectListItems(0);
            schoolClassCreateEdit.SchoolClassStars = SchoolClassStars(Id);
            return View(schoolClassCreateEdit);
        }
        [HttpPost]
        public ActionResult Create(O_SchoolClassCreateEdit _schoolClassCreateEdit)
        {
            try
            {
                SetData(_schoolClassCreateEdit, true);
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "INSERT");
                    foreach (PropertyInfo propertyInfo in schoolClassInfo.GetType().GetProperties())
                    {
                        paramaters.Add("@" + propertyInfo.Name, propertyInfo.GetValue(schoolClassInfo, null));
                    }
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP2_O_SchoolClass", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    var schoolClassid = paramaters.Get<Int32>("@RetCode");
                    // Insert level và sao cho Lớp học
                    foreach (var item in _schoolClassCreateEdit.LevelArrs)
                    {
                        InsertLevel(schoolClassid, item);
                    }
                    return RedirectToAction("Edit", new { id = paramaters.Get<Int32>("@RetCode"), msg = "1" });
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult Edit(O_SchoolClassCreateEdit _schoolClassCreateEdit)
        {
            try
            {
                SetData(_schoolClassCreateEdit, false);
                try
                {
                    using (var conn = new SqlConnection(hienTaiConnection))
                    {
                        if (conn.State == System.Data.ConnectionState.Closed)
                            conn.Open();
                        var paramaters = new DynamicParameters();
                        paramaters.Add("@Action", "UPDATE");
                        foreach (PropertyInfo propertyInfo in schoolClassInfo.GetType().GetProperties())
                        {
                            paramaters.Add("@" + propertyInfo.Name, propertyInfo.GetValue(schoolClassInfo, null));
                        }
                        paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                        paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                        var result = conn.Execute("SP2_O_SchoolClass", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                        DeleteLevel(schoolClassInfo.Id);
                        foreach (var item in _schoolClassCreateEdit.LevelArrs)
                        {
                            InsertLevel(schoolClassInfo.Id, item);
                        }
                        return RedirectToAction("Edit", new { id = schoolClassInfo.Id, msg = "1" });
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.Error = ex.ToString();
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public ActionResult Deletes(int id, string userName)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "DELETE");
                    paramaters.Add("@Id", id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP2_O_SchoolClass", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return Json(new { retCode = paramaters.Get<Int32>("@RetCode"), retText = paramaters.Get<string>("@RetText") });
                }
            }
            catch { return Json(new { retCode = 2, retText = "Có lỗi vui lòng liên hệ Admin để hỗ trợ" }); }
        }
        private void SetData(O_SchoolClassCreateEdit _schoolClassCreateEdit, bool isCreate)
        {
            if (isCreate)
            {
                schoolClassInfo = new O_SchoolClassInfo();
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "GETBY");
                    paramaters.Add("@Id", _schoolClassCreateEdit.ClassId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var classInfo = conn.Query<O_ClassInfo>("SP2_O_Class", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    schoolClassInfo.ClassId = classInfo.FirstOrDefault().Id;
                }
                schoolClassInfo.Code = StringUtil.CheckLetter("K", GetMax(), 4);
            }
            else
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "GETBY");
                    paramaters.Add("@Id", _schoolClassCreateEdit.Id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var results = conn.Query<O_SchoolClassInfo>("SP2_O_SchoolClass", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    schoolClassInfo = results.FirstOrDefault();

                    paramaters.Add("@Action", "GETBY");
                    paramaters.Add("@Id", _schoolClassCreateEdit.ClassId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var classInfo = conn.Query<O_ClassInfo>("SP2_O_Class", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    schoolClassInfo.ClassId = classInfo.FirstOrDefault().Id;
                }
            }
            schoolClassInfo.Name = _schoolClassCreateEdit.Name;
            schoolClassInfo.TeacherId = _schoolClassCreateEdit.TeacherId;
            schoolClassInfo.AgencyId = GetUser().teacherInfo.AgencyId;
            schoolClassInfo.Total = _schoolClassCreateEdit.Total;
            schoolClassInfo.OpeningDay = DateTime.Parse(StringUtil.ConvertStringToDate(_schoolClassCreateEdit.OpeningDayString));
            schoolClassInfo.ClosingDay = DateTime.Parse(StringUtil.ConvertStringToDate(_schoolClassCreateEdit.ClosingDayString));
        }
        private string LevelListStr()
        {
            var ddlClassStr = new StringBuilder();
            ddlClassStr.Append("<select id=\"Childrens_{{stt}}__Class_Id\" name=\"Childrens[{{stt}}].Class_Id\" class=\"md-input label-fixed\" accesskey=\"ddkChildrensKeyNewLine\">");
            ddlClassStr.Append("<option value=\"\">[ Chọn ]</option>");
            var classList = LevelStartList();
            foreach (var item in classList)
            {
                ddlClassStr.Append(string.Format("<option value=\"{0}\">{1}</option>", item.Id, item.LevelName));
            }
            ddlClassStr.Append("</select>");
            return ddlClassStr.ToString();
        }
        private IEnumerable<LevelStarViewModel> LevelStartList()
        {
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@Action", "LEVEL_LIST");
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var results = conn.Query<LevelStarViewModel>("SP2_O_SchoolClass", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                return results.ToList();
            }
        }
        private string DeleteLevel(int schoolClassId)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "DELETE_LEVEL");
                    paramaters.Add("@SchoolClassId", schoolClassId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var results = conn.Execute("SP2_O_SchoolClass", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                }
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        private string InsertLevel(int schoolClassId, int levelId)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "INSERT_LEVEL");
                    paramaters.Add("@SchoolClassId", schoolClassId);
                    paramaters.Add("@LevelId", levelId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var results = conn.Execute("SP2_O_SchoolClass", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                }
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        private IEnumerable<SchoolClassStar> SchoolClassStars(int schoolClassId)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "SCHOOL_STAR");
                    paramaters.Add("@SchoolClassId", schoolClassId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var results = conn.Query<SchoolClassStar>("SP2_O_SchoolClass", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return results.ToList();
                }
            }
            catch (Exception ex) { return new List<SchoolClassStar>(); }
        }
        public ActionResult LoadSchoolClassStars(string[] levelArrs)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();

                    var paramaters = new DynamicParameters();
                    if (levelArrs.Count() == 1 && levelArrs[0].Equals("1"))
                    {
                        paramaters.Add("@Action", "SCHOOLSTAR_LEVEL_1");
                    }
                    else if (levelArrs.Count() == 1 && levelArrs[0].Equals("2"))
                    {
                        paramaters.Add("@Action", "SCHOOLSTAR_LEVEL_2");
                    }
                    else
                    {
                        paramaters.Add("@Action", "SCHOOLSTAR_LEVEL_1_2");
                    }
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var results = conn.Query<SchoolClassStar>("SP2_O_SchoolClass", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return Json(new { status = true, data = results });
                }
            }
            catch (Exception ex) { return Json(new { status = false }); }
        }
        private int TotalRecord(int classStarId)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "TOTAL.ROW.CLASSSTAR");
                    paramaters.Add("@SchoolClassStarId", classStarId);
                    paramaters.Add("@AgencyID", GetUser().teacherInfo.AgencyId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var recordOfPageResults = conn.Query<RecordOfPage>("SP2_O_Student", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return recordOfPageResults.FirstOrDefault().TotalRow;
                }
            }
            catch { return 0; }
        }
        private IEnumerable<O_StudentList> StudentListByClassStar(int start, int offset, int classStarId)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "FINDBYCLASSSTAR");
                    paramaters.Add("@SchoolClassStarId", classStarId);
                    paramaters.Add("@AgencyID", GetUser().teacherInfo.AgencyId);
                    paramaters.Add("@Start", start);
                    paramaters.Add("@Offset", offset);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<O_StudentList>("SP2_O_Student", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }
            }
            catch { return new List<O_StudentList>(); }
        }
        public ActionResult ValidateClass(SchoolClassValidate data)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed) { }
                    conn.Open();

                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "LIST.SCHOOLCLASS");
                    paramaters.Add("@ClassId", int.Parse(data.ClassId));
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var results = conn.Query<O_SchoolClassInfo>("SP2_O_SchoolClass", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    bool result = true;
                    if (results.Count() > 0)
                    {
                        DateTime openingDay = DateTime.Parse(StringUtil.ConvertStringToDate(data.OpeningDay));
                        DateTime closingDay = DateTime.Parse(StringUtil.ConvertStringToDate(data.ClosingDay));
                        foreach (var item in results)
                        {
                            if (item.Id != int.Parse(data.Id))
                            {
                                if (openingDay >= item.OpeningDay && openingDay <= item.ClosingDay ||
                                    closingDay >= item.OpeningDay && closingDay <= item.ClosingDay)
                                {
                                    result = false;
                                    break;
                                }
                            }

                        }
                    }
                    return Json(new { status = result });
                }
            }
            catch (Exception ex) { return Json(new { status = false }); }
        }
        private IEnumerable<AgencyParentList> SqlTeacherListList()
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "PARENT.LIST");
                    paramaters.Add("@AgencyID", GetUser().teacherInfo.AgencyId);
                    paramaters.Add("@Id", GetUser().teacherInfo.Id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<AgencyParentList>("SP2_O_Teacher", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }
            }
            catch { return new List<AgencyParentList>(); }
        }
        private string GetMax()
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "GETMAX");
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var recordOfPageResults = conn.Query<MaxCode>("SP2_O_SchoolClass", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return recordOfPageResults.FirstOrDefault().Code;
                }
            }
            catch { return ""; }
        }
    }
}