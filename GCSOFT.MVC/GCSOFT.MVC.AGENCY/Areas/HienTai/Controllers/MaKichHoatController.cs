﻿using AutoMapper;
using Dapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Areas.Agency.Data;
using GCSOFT.MVC.AGENCY.Helper;
using GCSOFT.MVC.AGENCY.HienTaiService;
using GCSOFT.MVC.AGENCY.ViewModels.HienTai;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.StoreProcedue;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.HienTai.Controllers
{
    [CompressResponseAttribute]
    public class MaKichHoatController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IOptionLineService _optionLineService;
        private readonly IMailSetupService _mailSetupService;
        private readonly ITemplateService _templateService;
        private string sysCategory = "HIENTAI_LOP";
        private bool? permission = false;
        private string hienTaiConnection = ConfigurationManager.ConnectionStrings["HienTaiConnection"].ConnectionString;
        #endregion

        ParentService parentService = new ParentService();
        StudentService studentService = new StudentService();
        StudentActiveService studentActiveService = new StudentActiveService();
        #region -- Contructor --
        public MaKichHoatController
            (
                IVuViecService vuViecService,
                IOptionLineService optionLineService,
                IMailSetupService mailSetupService,
                ITemplateService templateService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _optionLineService = optionLineService;
            _mailSetupService = mailSetupService;
            _templateService = templateService;
        }
        #endregion
        public ActionResult Index(int? pageNumber)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion

            int page = 0;
            int pageSize = 10;
            int.TryParse(pageNumber.ToString(), out page);
            if (page == 0) page = 1;
            ViewBag.StartSTT = 1;

            ViewBag.ListParent = parentService.GetAllParentDrop(GetUser().teacherInfo.AgencyId);
            ViewBag.ListStudent = studentService.GetAllStudentDrop(GetUser().teacherInfo.AgencyId);

            List<O_StudentActive> model = studentActiveService.GetAllStudentActive(string.Empty, string.Empty, 0, 0, GetUser().teacherInfo.AgencyId);
            IPagedList<O_StudentActive> data = model.ToPagedList(page, pageSize);
            return View(data);
        }

        [HttpPost]
        public ActionResult IndexContent(string activationCode, string phoneNumber, string studentId, string parentId, int? pageNumber)
        {
            int pageSize = 10;
            int page = 0;
            int.TryParse(pageNumber.ToString(), out page);
            if (page == 0)
            {
                page = 1;
                ViewBag.StartSTT = 1;
            }
            else
            {
                ViewBag.StartSTT = ((page - 1) * pageSize) + 1;
            }

            ViewBag.activationCode = activationCode;
            ViewBag.phoneNumber = phoneNumber;
            ViewBag.studentId = studentId ?? "0";
            ViewBag.parentId = parentId ?? "0";

            int studentIdConvert = 0;
            int.TryParse(studentId == null ? "0" : studentId, out studentIdConvert);

            int parentIdConvert = 0;
            int.TryParse(parentId == null ? "0" : parentId, out parentIdConvert);

            List<O_StudentActive> model = studentActiveService.GetAllStudentActive(activationCode, phoneNumber, studentIdConvert, parentIdConvert, GetUser().teacherInfo.AgencyId);
            IPagedList<O_StudentActive> data = model.ToPagedList(page, pageSize);
            return PartialView("_IndexContent", data);
        }
        
        public ActionResult Details(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.ViewDetail);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View(new StudentActiveService().GetBy(id, GetUser().teacherInfo.AgencyId));
        }
    }
}