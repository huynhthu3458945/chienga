﻿using AutoMapper;
using Dapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Areas.Agency.Data;
using GCSOFT.MVC.AGENCY.Helper;
using GCSOFT.MVC.AGENCY.ViewModels.HienTai;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using Serilog;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.HienTai.Controllers
{
    [CompressResponseAttribute]
    public class NotificationController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IOptionLineService _optionLineService;
        private readonly IClassService _classService;
        private readonly IMailSetupService _mailSetupService;
        private readonly ITemplateService _templateService;
        private readonly IAgencyService _agencyService;
        private string sysCategory = "HIENTAI_THONGBAO";
        private bool? permission = false;
        private string hasValue;
        private string hienTaiConnection = ConfigurationManager.ConnectionStrings["HienTaiConnection"].ConnectionString;

        private string DOMAINAPI = ConfigurationManager.AppSettings["DOMAINAPI"];
        private string TOKEN = ConfigurationManager.AppSettings["TOKEN"];
        private const string PORTAPI = "44348";
        private HttpClientHandler clientHandler = new HttpClientHandler();

        public NotificationController(IVuViecService vuViecService, IOptionLineService optionLineService, IClassService classService, IMailSetupService mailSetupService, ITemplateService templateService, IAgencyService agencyService) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _optionLineService = optionLineService;
            _classService = classService;
            _mailSetupService = mailSetupService;
            _templateService = templateService;
            _agencyService = agencyService;
        }
        #endregion

        public ActionResult Index(int? currPage, string NotificationType)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            //select
            ViewBag.NotificationTypes = SqlNotificationType().ToSelectListItems("APP");

            var notice = new O_NotificationPage();
            var paramaters = new DynamicParameters();
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();

                var sqlHelper = new SqlExecHelper();
                //-- Paging
                int pageString = currPage ?? 0;
                int _currPage = pageString == 0 ? 1 : pageString;
                paramaters = new DynamicParameters();
                paramaters.Add("@Action", "TOTAL.ROW");
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var recordOfPageResults = conn.Query<RecordOfPage>("SP2_O_Notification", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                int totalRecord = recordOfPageResults.FirstOrDefault().TotalRow;
                notice.paging = new Paging(totalRecord, _currPage);
                //-- Paging +

                paramaters = new DynamicParameters();
                paramaters.Add("@Action", "HIENTAI_NOTIFICATION_LIST");
                paramaters.Add("@NotificationType", NotificationType);
                paramaters.Add("@Start", notice.paging.start);
                paramaters.Add("@Offset", notice.paging.offset);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var data = conn.Query<O_Notification>("SP2_O_Notification", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                notice.Notifications = data.ToList();
            }
            return View(notice);
        }

        public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion

            O_Notification model = new O_Notification();
            model.NotificationTypes = SqlNotificationType().ToSelectListItems("APP");
            model.IsAll = true;
            model.ListNotificationReceivers = SqlNotificationReceiver("TTL").ToList();

            var sqlHelper = new SqlExecHelper();
            var teacherInfo = GetUser().teacherInfo;
            model.SchoolClassList = sqlHelper.SqlSchoolClassList(teacherInfo).ToSelectListItems(model.SchoolClassId);
            model.TeacherId = GetUser().teacherInfo.Id;
            model.TeacherList = sqlHelper.SqlTeacherListList(teacherInfo).ToSelectListItems(model.TeacherId);
            model.StudentList = sqlHelper.SqlStudentList(teacherInfo).ToSelectListItems(model.StudentId);

            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> Create(O_Notification model)
        {
            model.CreateBy = GetUser().ID;
            model.CreateDate = DateTime.Now;
            model.NotificationType = string.Join(",", model.NotificationTypeArr);

            //insert Notification
            int notificationId = InsertNotification(model);

            //insert list NotificationReceiver
            ////create list
            List<O_NotificationReceiver> notificationReceivers = new List<O_NotificationReceiver>();
            notificationReceivers = model.IsAll == true ? model.ListNotificationReceivers : model.ListNotificationReceivers.Where(x => x.Select == true).ToList();
            //set date
            notificationReceivers.ForEach(x =>
            {
                x.NotificationId = notificationId;
                x.Subject = model.Name;
                x.CreateDate = DateTime.Now;

                if (model.NotificationType.Contains("APP"))
                {
                    x.ContentMobile = model.ContentMobile.Contains("<<FullName>>") == true ? model.ContentMobile.Replace("<<FullName>>", x.FullName) : model.ContentMobile;
                }
                if (model.NotificationType.Contains("EMAIL"))
                {
                    x.ContentEmail = model.ContentEmail.Contains("&lt;&lt;FullName&gt;&gt;") == true ? model.ContentEmail.Replace("&lt;&lt;FullName&gt;&gt;", x.FullName) : model.ContentEmail;
                }
            });

            //Save
            var result = await BulkInsertNotificationReceiver(notificationReceivers);

            try
            {
                //exec job
                ExecJob(model.NotificationType, model.Name, notificationId);
            }
            catch (Exception ex)
            {
                LogException("/Notification/Create", ex, model);
            }

            return RedirectToAction("Index", "Notification", new { Area = "HienTai" });
        }

        public ActionResult Delete(int id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                int hasValue = SqlDelete(id);
                return Json("Xóa dữ liệu thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }

        public ActionResult NotificationResult(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion

            IEnumerable<NotificationResult> data = SqlNotificationResult(id);
            ViewBag.NotificationId = id;
            return View(data.ToList());
        }

        private IEnumerable<OptionString> SqlNotificationType()
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_NOTIFICATION_TYPE");
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var data = conn.Query<OptionString>("SP2_O_Notification", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return data.Where(x => x.Key != "SMS");
                }
            }
            catch (Exception ex)
            {
                return new List<OptionString>();
            }
        }

        private IEnumerable<O_Notification> SqlNotificationList(int? currPage, string name)
        {
            try
            {
                var data = new O_NotificationPage();

                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();

                    var paramaters = new DynamicParameters();

                    //-- Paging
                    int pageString = currPage ?? 0;
                    int _currPage = pageString == 0 ? 1 : pageString;
                    paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "TOTAL.ROW");
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var recordOfPageResults = conn.Query<RecordOfPage>("SP2_O_Notification", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    int totalRecord = recordOfPageResults.FirstOrDefault().TotalRow;
                    data.paging = new Paging(totalRecord, _currPage);
                    //-- Paging +

                    paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_NOTIFICATION_LIST");
                    paramaters.Add("@Name", name != null ? name : null);
                    paramaters.Add("@Start", data.paging.start);
                    paramaters.Add("@Offset", data.paging.offset);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<O_Notification>("SP2_O_Notification", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                return new List<O_Notification>();
            }
        }

        private IEnumerable<O_NotificationReceiver> SqlNotificationReceiver(string strAgency)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_NOTIFICATION_RECEIVER");
                    paramaters.Add("@AgencyKey", strAgency);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<O_NotificationReceiver>("SP2_O_Notification", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                return new List<O_NotificationReceiver>();
            }
        }

        private IEnumerable<NotificationResult> SqlNotificationResult(int id)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_NOTIFICATION_RESULT");
                    paramaters.Add("@Id", id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<NotificationResult>("SP2_O_Notification", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                return new List<NotificationResult>();
            }
        }

        private int InsertNotification(O_Notification data)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_NOTIFICATION_CREARE");
                    foreach (PropertyInfo propertyInfo in data.GetType().GetProperties())
                    {
                        if (propertyInfo.Name != "NotificationTypeArr" && propertyInfo.Name != "ListNotificationReceivers" && propertyInfo.Name != "NotificationTypes" && propertyInfo.Name != "CreateName")
                        {
                            paramaters.Add("@" + propertyInfo.Name, propertyInfo.GetValue(data, null));
                        }
                    }
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP2_O_Notification", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return paramaters.Get<Int32>("@RetCode");
                }
            }
            catch (Exception ex)
            { return 0; }
        }

        private int SqlDelete(int id)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_NOTIFICATION_DELETE");
                    paramaters.Add("@Id", id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP2_O_Notification", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return paramaters.Get<Int32>("@RetCode");
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        private async Task<bool> BulkInsertNotificationReceiver(List<O_NotificationReceiver> notificationReceiver)
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[16] {
                    new DataColumn("NotificationId", typeof(int)),
                    new DataColumn("UserId", typeof(int)),
                    new DataColumn("StudentId",typeof(int)),
                    new DataColumn("IdNo",typeof(string)),
                    new DataColumn("Email",typeof(string)),
                    new DataColumn("Phone",typeof(string)),
                    new DataColumn("Subject",typeof(string)),
                    new DataColumn("CreateDate",typeof(DateTime)),
                    new DataColumn("ContentSMS",typeof(string)),
                    new DataColumn("ContentMobile",typeof(string)),
                    new DataColumn("ContentEmail",typeof(string)),
                    new DataColumn("FullName",typeof(string)),
                    new DataColumn("ParentName",typeof(string)),
                    new DataColumn("StatusApp",typeof(bool)),
                    new DataColumn("StatusSMS",typeof(bool)),
                    new DataColumn("StatusEMAIL",typeof(bool)),
                });

                foreach (var row in notificationReceiver)
                {
                    dt.Rows.Add(row.NotificationId, row.UserId, row.StudentId, row.IdNo, row.Email, row.Phone, row.Subject
                        , row.CreateDate, row.ContentSMS, row.ContentMobile, row.ContentEmail, row.FullName, row.ParentName, row.StatusApp, row.StatusSMS, row.StatusEMAIL);
                }

                using (SqlConnection conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();

                    using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(conn))
                    {
                        //[OPTIONAL]: Map the DataTable columns with that of the database table
                        sqlBulkCopy.ColumnMappings.Add("NotificationId", "NotificationId");
                        sqlBulkCopy.ColumnMappings.Add("UserId", "UserId");
                        sqlBulkCopy.ColumnMappings.Add("StudentId", "StudentId");
                        sqlBulkCopy.ColumnMappings.Add("IdNo", "IdNo");
                        sqlBulkCopy.ColumnMappings.Add("Email", "Email");
                        sqlBulkCopy.ColumnMappings.Add("Phone", "Phone");
                        sqlBulkCopy.ColumnMappings.Add("Subject", "Subject");
                        sqlBulkCopy.ColumnMappings.Add("CreateDate", "CreateDate");
                        sqlBulkCopy.ColumnMappings.Add("ContentSMS", "ContentSMS");
                        sqlBulkCopy.ColumnMappings.Add("ContentMobile", "ContentMobile");
                        sqlBulkCopy.ColumnMappings.Add("ContentEmail", "ContentEmail");
                        sqlBulkCopy.ColumnMappings.Add("FullName", "FullName");
                        sqlBulkCopy.ColumnMappings.Add("ParentName", "ParentName");
                        sqlBulkCopy.ColumnMappings.Add("StatusApp", "StatusApp");
                        sqlBulkCopy.ColumnMappings.Add("StatusSMS", "StatusSMS");
                        sqlBulkCopy.ColumnMappings.Add("StatusEMAIL", "StatusEMAIL");
                        //Set the database table name
                        sqlBulkCopy.DestinationTableName = "O_NotificationReceiver";
                        await sqlBulkCopy.WriteToServerAsync(dt);
                        conn.Close();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private async void ExecJob(string notificationType, string title, int notificationId)
        {
           
            if (notificationType.Contains("APP"))
            {
                int rlNotification = ExecuteSendNotification(notificationId, title);
            }
            if (notificationType.Contains("EMAIL"))
            {
                int rlEmail = ExecuteSendEmail(notificationId);
            }
        }

        private int GetTotalByType(int notiId, string type)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "TOTAL.ROW.TYPE");
                    paramaters.Add("@Id", notiId);
                    paramaters.Add("@Type", type);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var recordOfPageResults = conn.Query<RecordOfPage>("SP2_O_Notification", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    int totalRecord = recordOfPageResults.FirstOrDefault().TotalRow;
                    return totalRecord;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        private int ExecuteSendEmail(int notiId)
        {
            //tinh tong so goi tin
            int packageSize = 100;
            int totalItem = GetTotalByType(notiId, "email");
            int packageNumber = totalItem / packageSize;
            decimal modItem = totalItem % packageSize;
            if (packageNumber == 0)
                packageNumber = 1;

            if (modItem > 0)
                packageNumber += 1;

            for (int i = 1; i <= packageNumber; i++)
            {
                //gui email
                //lay data theo goi
                List<HienTaiService.O_NotificationReceiver> data = new List<HienTaiService.O_NotificationReceiver>();
                int start = packageSize * (i - 1);
                data = new HienTaiService.NotificationService().GetSendEmailBy(notiId, start, packageSize);

                //tao luong xu ly email
                TheadSendEmail(data);
            }
            return 0;
        }

        private int ExecuteSendNotification(int notiId, string title)
        {
            //tinh tong so goi tin
            int packageSize = 500;
            int totalItem = GetTotalByType(notiId, "app");
            int packageNumber = totalItem / packageSize;
            decimal modItem = totalItem % packageSize;
            if (packageNumber == 0)
                packageNumber = 1;

            if (modItem > 0)
                packageNumber += 1;

            for (int i = 1; i <= packageNumber; i++)
            {
                //gui email
                //lay data theo goi
                List<HienTaiService.O_NotificationReceiver> data = new List<HienTaiService.O_NotificationReceiver>();
                int start = packageSize * (i - 1);
                data = new HienTaiService.NotificationService().GetSendEmailBy(notiId, start, packageSize);

                //tao luong xu ly notification
                TheadSendNotification(data, title);
            }
            return 0;
        }

        private void TheadSendEmail(List<HienTaiService.O_NotificationReceiver> data)
        {
            var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());

            Thread email = new Thread(delegate ()
            {
                for (int i = 0; i < data.Count(); i++)
                {
                    // thuc hien send email
                    HienTaiService.O_NotificationReceiver dataSend = data[i];
                    try
                    {
                        //thuc hien gui
                        List<string> listEmail = new List<string>();
                        if (dataSend != null && dataSend.Email != null)
                        {
                            listEmail.Add(dataSend.Email);
                            var _mailHelper = new EmailHelper()
                            {
                                EmailTos = listEmail,
                                DisplayName = emailInfo.DisplayName,
                                Title = dataSend.Subject != null ? dataSend.Subject : string.Empty,
                                Body = dataSend.ContentEmail != null ? dataSend.ContentEmail : string.Empty,
                                MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
                            };
                            // noti
                            var hasValue = _mailHelper.DoSendMail();
                        }
                    }
                    catch (Exception ex)
                    {
                        new HienTaiService.NotificationService().UpdateEmailStatus(dataSend.NotificationId, dataSend.StudentId, dataSend.UserId, dataSend.IdNo);
                    }
                    //Debug.WriteLine(string.Format("{0}", data[i].StudentId));
                }
            });
            email.IsBackground = true;
            email.Start();
        }

        private void TheadSendNotification(List<HienTaiService.O_NotificationReceiver> data, string title)
        {
            //cau hinh
            string url = "https://apipro.5phutthuocbai.com/api/MstNotifications/push?appName=DTHT";
            var notification = new Noti();
            notification.type = "NOTIFICATION";
            notification.title = title;
            notification.image = "https://5phutthuocbai.com/static/media/voi.ac034b38.png";
            notification.date = DateTime.Now;
            notification.name = "ĐÀO TẠO HIỀN TÀI";

            Thread noti = new Thread(delegate ()
            {
                for (int i = 0; i < data.Count(); i++)
                {
                    // thuc hien send noti
                    HienTaiService.O_NotificationReceiver dataSend = data[i];
                    //thuc hien gui
                    List<int> user = new List<int>();
                    if (dataSend != null)
                    {
                        user.Add(dataSend.UserId);
                    }
                    //set config
                    notification.listUserId = user;
                    notification.description = dataSend.ContentMobile;

                    try
                    {
                        using (var httpClient = new HttpClient())
                        {
                            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                            //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TOKEN);
                            //httpClient.DefaultRequestHeaders.Add("app-secret", "tamtriluc-firebase");
                            httpClient.Timeout = TimeSpan.FromMinutes(10);
                            HttpResponseMessage response = httpClient.PostAsJsonAsync($"{url}", notification).Result;
                            httpClient.DefaultRequestHeaders.ConnectionClose = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        new HienTaiService.NotificationService().UpdateAppStatus(dataSend.NotificationId, dataSend.StudentId, dataSend.UserId, dataSend.IdNo);
                    }

                    //Debug.WriteLine(string.Format("{0}", data[i].StudentId));
                }
            });
            noti.IsBackground = true;
            noti.Start();
        }

        private void LogException(string url, Exception exception, object param)
        {
            var logger = new LoggerConfiguration()
                         .MinimumLevel.Error()
                         .WriteTo.File(System.Web.Hosting.HostingEnvironment.MapPath("~/Areas/HienTai/Logs/.txt"),
                         LogEventLevel.Error, // Minimum Log level
                                 rollingInterval: RollingInterval.Day, // This will append time period to the filename like 20180316.txt
                                 retainedFileCountLimit: null,
                                 fileSizeLimitBytes: null,
                                 outputTemplate: "DateTime: {Timestamp:yyyy-MM-dd HH:mm:ss}{NewLine}{Message}{NewLine}Exception: {Exception}{NewLine}",  // Set custom file format
                                 shared: true // Shared between multi-process shared log files
                                 )
                         .CreateLogger();

            logger.Error(exception, "Url: {url}\r\nParam: {@param}", url, param);
        }
    }
}