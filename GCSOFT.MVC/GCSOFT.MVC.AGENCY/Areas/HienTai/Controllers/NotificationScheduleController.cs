﻿using AutoMapper;
using Dapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Areas.Agency.Data;
using GCSOFT.MVC.AGENCY.Helper;
using GCSOFT.MVC.AGENCY.HienTaiService;
using GCSOFT.MVC.AGENCY.ViewModels.HienTai;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using Hangfire;
using Hangfire.Storage;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.HienTai.Controllers
{
    [CompressResponseAttribute]
    public class NotificationScheduleController : BaseController
    {

        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IOptionLineService _optionLineService;
        private readonly IClassService _classService;
        private readonly IMailSetupService _mailSetupService;
        private readonly ITemplateService _templateService;
        private readonly IAgencyService _agencyService;
        private string sysCategory = "HIENTAI_LENLICH";
        private bool? permission = false;
        private string hasValue;
        private string hienTaiConnection = ConfigurationManager.ConnectionStrings["HienTaiConnection"].ConnectionString;
        private string DOMAINAPI = ConfigurationManager.AppSettings["DOMAINAPI"];
        private string TOKEN = ConfigurationManager.AppSettings["TOKEN"];
        private const string PORTAPI = "44348";
        private HttpClientHandler clientHandler = new HttpClientHandler();

        public NotificationScheduleController(IVuViecService vuViecService, IOptionLineService optionLineService, IClassService classService, IMailSetupService mailSetupService, ITemplateService templateService, IAgencyService agencyService) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _optionLineService = optionLineService;
            _classService = classService;
            _mailSetupService = mailSetupService;
            _templateService = templateService;
            _agencyService = agencyService;
        }
        #endregion

        public ActionResult Index(int? currPage, string NotificationType)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion

            //select
            ViewBag.NotificationTypes = SqlNotificationType().ToSelectListItems("APP");

            var notice = new O_NotificationSchedulePage();
            var paramaters = new DynamicParameters();
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();

                var sqlHelper = new SqlExecHelper();
                //-- Paging
                int pageString = currPage ?? 0;
                int _currPage = pageString == 0 ? 1 : pageString;
                paramaters = new DynamicParameters();
                paramaters.Add("@Action", "TOTAL.ROW");
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var recordOfPageResults = conn.Query<RecordOfPage>("SP2_O_NotificationSchedule", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                int totalRecord = recordOfPageResults.FirstOrDefault().TotalRow;
                notice.paging = new Paging(totalRecord, _currPage);
                //-- Paging +

                paramaters = new DynamicParameters();
                paramaters.Add("@Action", "HIENTAI_SCHEDULE_LIST");
                paramaters.Add("@NotificationType", NotificationType);
                paramaters.Add("@Start", notice.paging.start);
                paramaters.Add("@Offset", notice.paging.offset);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var data = conn.Query<O_NotificationSchedule>("SP2_O_NotificationSchedule", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                notice.NotificationSchedules = data.ToList();
            }
            return View(notice);
        }

        public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion

            NotificationScheduleService notificationScheduleService = new NotificationScheduleService();
            O_NotificationSchedule model = new O_NotificationSchedule();
            model.NotificationTypes = SqlNotificationType().ToSelectListItems("APP");
            model.ReceiverTypeTypes = SqlReceiverType().ToSelectListItems("STUDENT");
            model.LoopTypes = SqlLoopType().ToSelectListItems("NOLOOP");
            model.NotificationCats = notificationScheduleService.GetScheduleCategory().ToSelectListItems("BINH_THUONG");
            model.Status = true;
            model.AllDay = true;
            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> Create(O_NotificationSchedule model)
        {
            model.CreateBy = GetUser().ID;
            model.CreateDate = DateTime.Now;
            model.Status = true;
            model.NotificationType = string.Join(",", model.NotificationTypeArr);
            if (model.AllDay)
                model.TimeSlots = "08:00";
            int result = sqlInsert(model);

            //exec
            if (result > 0)
            {
                ExecuteNotificationSchedule(result);
            }

            return RedirectToAction("Index", "NotificationSchedule", new { Area = "HienTai" });
        }

        public ActionResult Delete(int id)
        {
            return RedirectToAction("Index", "NotificationSchedule", new { Area = "HienTai" });
        }

        public async Task<ActionResult> Stop(int notificationId)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion

            try
            {
             
                //update status
                int result = sqlUpdateStatus(notificationId, false);
                //delete
                O_NotificationSchedule data = sqlDetail(notificationId);
                if (data == null) return RedirectToAction("Index", "NotificationSchedule", new { Area = "HienTai" }); 

                var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
                if (data.TimeSlots.Split(';').Length > 0)
                {
                    int i = 0;
                    foreach (string time in data.TimeSlots.Split(';'))
                    {
                        i++;
                        if (time != string.Empty)
                        {
                            int h = Convert.ToInt32(time.Split(':')[0]);
                            int s = Convert.ToInt32(time.Split(':')[1]);

                            if (data.Loop == "DAILY")
                            {
                                RecurringJob.RemoveIfExists(string.Format("{0}-{1}-{2}", notificationId, "Daily", i));
                            }
                            else
                            {

                                DateTime date1 = DateTime.Now;
                                DateTime date2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, h, s, 0);
                                TimeSpan ts = date2 - date1;

                                //Enqueue => thực hiện ngay
                               string jobid =  BackgroundJob.Schedule<NotificationScheduleService>(x => x.ExecuteNotification(notificationId, time, data, emailInfo), TimeSpan.FromMinutes(ts.TotalMinutes));
                               BackgroundJob.Delete(jobid);
                            }
                        }
                    }
                }

                return RedirectToAction("Index", "NotificationSchedule", new { Area = "HienTai" });
            }
            catch (Exception ex)
            {
                var error = ex.ToString();
                ViewBag.Error = ex;
                return View("Error");
            }
        }

        public async Task<ActionResult> Start(int notificationId)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion

            try
            {
                //update status
                int result = sqlUpdateStatus(notificationId, true);
                ExecuteNotificationSchedule(notificationId);
                return RedirectToAction("Index", "NotificationSchedule", new { Area = "HienTai" });
            }
            catch (Exception ex)
            {
                var error = ex.ToString();
                ViewBag.Error = ex;
                return View("Error");
            }
        }

        public async Task<ActionResult> StopAll()
        {
            try
            {
                using (var connection = JobStorage.Current.GetConnection())
                {
                    foreach (var recurringJob in connection.GetRecurringJobs())
                    {
                        RecurringJob.RemoveIfExists(recurringJob.Id);
                    }
                }

                return RedirectToAction("Index", "NotificationSchedule", new { Area = "HienTai" });
            }
            catch (Exception ex)
            {
                var error = ex.ToString();
                ViewBag.Error = ex;
                return View("Error");
            }
        }

        public ActionResult History(int? currPage, int notificationId, string date)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion

            ViewBag.NotificationId = notificationId;
            ViewBag.Date = date != null ? date : DateTime.Now.ToString("dd/MM/yyyy");
            var notice = new O_NotificationScheduleHistoryPage();
            var paramaters = new DynamicParameters();
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();

                var sqlHelper = new SqlExecHelper();
                //-- Paging
                int pageString = currPage ?? 0;
                int _currPage = pageString == 0 ? 1 : pageString;
                paramaters = new DynamicParameters();
                paramaters.Add("@Action", "HIENTAI_SCHEDULE_HISTORY_TOTAL.ROW");
                paramaters.Add("@Id", notificationId);
                paramaters.Add("@CreateDate", date != null ? new DateTime(Convert.ToInt32(date.Split('/')[2]), Convert.ToInt32(date.Split('/')[1]), Convert.ToInt32(date.Split('/')[0])) : DateTime.Now);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var recordOfPageResults = conn.Query<RecordOfPage>("SP2_O_NotificationSchedule", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                int totalRecord = recordOfPageResults.FirstOrDefault().TotalRow;
                notice.paging = new Paging(totalRecord, _currPage);
                //-- Paging +

                paramaters = new DynamicParameters();
                paramaters.Add("@Action", "HIENTAI_SCHEDULE_HISTORY");
                paramaters.Add("@Id", notificationId);
                paramaters.Add("@CreateDate", date != null ? new DateTime(Convert.ToInt32(date.Split('/')[2]), Convert.ToInt32(date.Split('/')[1]), Convert.ToInt32(date.Split('/')[0])) : DateTime.Now);
                paramaters.Add("@Start", notice.paging.start);
                paramaters.Add("@Offset", notice.paging.offset);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var data = conn.Query<O_NotificationScheduleHistoryView>("SP2_O_NotificationSchedule", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                notice.NotificationSchedules = data.ToList();
            }

            return View(notice);
        }

        private IEnumerable<OptionString> SqlNotificationType()
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_NOTIFICATION_TYPE");
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var data = conn.Query<OptionString>("SP2_O_Notification", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return data.Where(x => x.Key != "SMS");
                }
            }
            catch (Exception ex)
            {
                return new List<OptionString>();
            }
        }

        private IEnumerable<OptionString> SqlReceiverType()
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_SCHEDULE_REECEIVER_TYPE");
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var data = conn.Query<OptionString>("SP2_O_NotificationSchedule", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return data.Where(x => x.Key != "SMS");
                }
            }
            catch (Exception ex)
            {
                return new List<OptionString>();
            }
        }

        private IEnumerable<OptionString> SqlLoopType()
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_SCHEDULE_LOOP");
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var data = conn.Query<OptionString>("SP2_O_NotificationSchedule", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return data;
                }
            }
            catch (Exception ex)
            {
                return new List<OptionString>();
            }
        }

        private int sqlInsert(O_NotificationSchedule data)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_SCHEDULE_CREATE");
                    foreach (PropertyInfo propertyInfo in data.GetType().GetProperties())
                    {
                        if (propertyInfo.Name != "CreateName" && propertyInfo.Name != "NotificationTypeArr"
                            && propertyInfo.Name != "TimeSlotInput" && propertyInfo.Name != "NotificationTypes"
                            && propertyInfo.Name != "ReceiverTypeTypes" && propertyInfo.Name != "LoopTypes" && propertyInfo.Name != "NotificationCats")
                        {
                            paramaters.Add("@" + propertyInfo.Name, propertyInfo.GetValue(data, null));
                        }
                    }
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP2_O_NotificationSchedule", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return paramaters.Get<Int32>("@RetCode");
                }
            }
            catch (Exception ex)
            { return 0; }
        }

        private int sqlUpdateStatus(int id, bool status)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_SCHEDULE_UPDATE_STATUS");
                    paramaters.Add("@Id", id);
                    paramaters.Add("@Status", status);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP2_O_NotificationSchedule", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return paramaters.Get<Int32>("@RetCode");
                }
            }
            catch (Exception ex)
            { return 0; }
        }

        private O_NotificationSchedule sqlDetail(int id)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_SCHEDULE_DETAIL");
                    paramaters.Add("@Id", id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var data = conn.Query<O_NotificationSchedule>("SP2_O_NotificationSchedule", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return data.SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                return new O_NotificationSchedule();
            }
        }

        private bool ExecuteNotificationSchedule(int notificationId)
        {
            // lây thông tin notification
            O_NotificationSchedule data = sqlDetail(notificationId);
            if (data == null) return false;

            var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());

            //gan data
            try
            {
                if (data.TimeSlots.Split(';').Length > 0)
                {
                    int i = 0;
                    foreach (string time in data.TimeSlots.Split(';'))
                    {
                        i++;
                        if (time != string.Empty)
                        {
                            int h = Convert.ToInt32(time.Split(':')[0]);
                            int s = Convert.ToInt32(time.Split(':')[1]);

                            if (data.Loop == "DAILY")
                            {
                                RecurringJob.AddOrUpdate<NotificationScheduleService>(string.Format("{0}-{1}-{2}", notificationId, "Daily", i)
                                    , x => x.ExecuteNotification(notificationId,time,data, emailInfo), Cron.Daily(h, s), TimeZoneInfo.Local);
                            }
                            else
                            {

                                DateTime date1 = DateTime.Now;
                                DateTime date2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, h, s, 0);
                                TimeSpan ts = date2 - date1;

                                //Enqueue => thực hiện ngay
                                BackgroundJob.Schedule<NotificationScheduleService>(x => x.ExecuteNotification(notificationId, time, data, emailInfo), TimeSpan.FromMinutes(ts.TotalMinutes));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                new NotificationScheduleService().LogException("NotificationScheduleJob_Schedule", ex.Message, 0);
            }
            return true;
        }
     
    }
}