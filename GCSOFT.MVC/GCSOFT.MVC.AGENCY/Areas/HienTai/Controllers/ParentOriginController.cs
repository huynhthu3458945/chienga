﻿using ClosedXML.Excel;
using Dapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.ViewModels.HienTai;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.HienTai.Controllers
{
    [CompressResponseAttribute]
    public class ParentOriginController : BaseController
    {
        #region -- Contructor --
        private readonly IVuViecService _vuViecService;
        private readonly IOptionLineService _optionLineService;
        private string sysCategory = "HIENTAI_DULIEUPH";
        private bool? permission = false;
        private string hienTaiConnection = ConfigurationManager.ConnectionStrings["HienTaiConnection"].ConnectionString;
        public ParentOriginController(IVuViecService vuViecService, IOptionLineService optionLineService) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _optionLineService = optionLineService;
        }
        #endregion

        public ActionResult Index(int? currPage, string code, string title)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var parentOriginList = new O_ParentOriginPage();
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();

                //-- Paging
                int pageString = currPage ?? 0;
                int _currPage = pageString == 0 ? 1 : pageString;
                paramaters = new DynamicParameters();
                paramaters.Add("@Action", "TOTAL.ROW");
                paramaters.Add("@Code", code ?? string.Empty);
                paramaters.Add("@Title", title ?? string.Empty);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var recordOfPageResults = conn.Query<RecordOfPage>("SP2_O_ParentOriginLocal", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                int totalRecord = recordOfPageResults.FirstOrDefault().TotalRow;
                parentOriginList.paging = new Paging(totalRecord, _currPage);
                //-- Paging +

                paramaters.Add("@Action", "FINDALL");
                paramaters.Add("@Code", code ?? string.Empty);
                paramaters.Add("@Title", title ?? string.Empty);
                paramaters.Add("@Start", parentOriginList.paging.start);
                paramaters.Add("@Offset", parentOriginList.paging.offset);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var results = conn.Query<O_ParentOrigin>("SP2_O_ParentOriginLocal", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                parentOriginList.ParentOrigins = results.ToList();
            }
            return View(parentOriginList);
        }

        public ActionResult Detail(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.ViewDetail);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            O_ParentOrigin data = GetDetail(id);
            return View(data);
        }

        public ActionResult Report(string StartDate, string EndDate, bool? IsAll)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion

            DateTime sd = DateTime.Now;
            DateTime ed = DateTime.Now;
            if (StartDate != null)
            {
                sd = new DateTime(Convert.ToInt32(StartDate.Substring(6, 4)), Convert.ToInt32(StartDate.Substring(3, 2)), Convert.ToInt32(StartDate.Substring(0, 2)));
            }

            if (EndDate != null) 
            {
                ed = new DateTime(Convert.ToInt32(EndDate.Substring(6, 4)), Convert.ToInt32(EndDate.Substring(3, 2)), Convert.ToInt32(EndDate.Substring(0, 2)));
            }

            O_ParentOriginReport model = new O_ParentOriginReport();
            model.IsAll = IsAll == null ? true : (bool)IsAll;
            model.StartDate = sd.ToString("dd/MM/yyyy");
            model.EndDate = ed.ToString("dd/MM/yyyy");
            model = GetReport(sd, ed, model.IsAll);
            return View(model);
        }

        public ActionResult ExportData()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.ExportExcel);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion

            //xu ly
            var teacherInfo = GetUser().teacherInfo;
            //lay data
            IEnumerable<O_ParentOrigin> data = GetAllData();
            //create data
            DataTable dt = new DataTable();
            try
            {
                //Setiing Table Name  
                dt.TableName = "du_lieu_dau_vao";
                //Add Columns  
                dt.Columns.Add("Id", typeof(int));
                dt.Columns.Add("StudentId", typeof(int));
                dt.Columns.Add("IdNo", typeof(string));
                dt.Columns.Add("ParentId", typeof(int));
                dt.Columns.Add("ParentCode", typeof(string));
                dt.Columns.Add("TenDaiLy", typeof(string));
                dt.Columns.Add("CapDaiLy", typeof(string));
                dt.Columns.Add("DanhXung", typeof(string));
                dt.Columns.Add("IsChaMeHienTai", typeof(bool));
                dt.Columns.Add("DiemChaMeHienTai", typeof(decimal));
                dt.Columns.Add("HoVaTenPH1", typeof(string));
                dt.Columns.Add("MoiQuanHePH1", typeof(string));
                dt.Columns.Add("SDTPH1", typeof(string));
                dt.Columns.Add("NgaySinhPH1", typeof(string));
                dt.Columns.Add("GioiTinhPH1", typeof(string));
                dt.Columns.Add("FacebookPH1", typeof(string));
                dt.Columns.Add("HoTenPH2", typeof(string));
                dt.Columns.Add("MoiQuanHePH2", typeof(string));
                dt.Columns.Add("SDTPH2", typeof(string));
                dt.Columns.Add("EmailPH2", typeof(string));
                dt.Columns.Add("NgaySinhPH2", typeof(string));
                dt.Columns.Add("GioiTinhPH2", typeof(string));
                dt.Columns.Add("FacebookPH2", typeof(string));
                dt.Columns.Add("NamHoc", typeof(int));
                dt.Columns.Add("GhiChu", typeof(string));
                dt.Columns.Add("DiaChi", typeof(string));
                dt.Columns.Add("DiaChiNhanQua", typeof(string));
                dt.Columns.Add("TenNganHang", typeof(string));
                dt.Columns.Add("SoTaiKhoan", typeof(string));
                dt.Columns.Add("ChuTaiKhoan", typeof(string));
                dt.Columns.Add("NgheNghiep", typeof(string));
                dt.Columns.Add("SoThich", typeof(string));
                dt.Columns.Add("TinhTrangHonNhan", typeof(string));
                dt.Columns.Add("SoNguoiCon", typeof(int));
                dt.Columns.Add("HinhAnhGiaDinh", typeof(string));
                dt.Columns.Add("ThoiGianLienHe", typeof(string));
                dt.Columns.Add("DongLucThamGia", typeof(string));
                dt.Columns.Add("MongMuon", typeof(string));
                dt.Columns.Add("KhoKhanCuocSong", typeof(string));
                dt.Columns.Add("PhanMemSuDung", typeof(string));
                dt.Columns.Add("ThoiGianChoCon", typeof(string));
                dt.Columns.Add("GopYKien", typeof(string));
                dt.Columns.Add("SoConThamGiaHienTai", typeof(int));
                dt.Columns.Add("ThoiGianHoc", typeof(int));
                dt.Columns.Add("HoVaTenHienTai", typeof(string));
                dt.Columns.Add("GiayKhaiSinh", typeof(string));
                dt.Columns.Add("NgaySinhHienTai", typeof(string));
                dt.Columns.Add("GioiTinhHienTai", typeof(string));
                dt.Columns.Add("TruongHocHienTai", typeof(string));
                dt.Columns.Add("SizeAo", typeof(string));
                dt.Columns.Add("ThanhTichHocTap", typeof(string));
                dt.Columns.Add("DieuTuHao", typeof(string));
                dt.Columns.Add("DiemYeu", typeof(string));
                dt.Columns.Add("UocMo", typeof(string));
                dt.Columns.Add("SoThichHienTai", typeof(string));
                dt.Columns.Add("ThoiQuen", typeof(string));
                dt.Columns.Add("KhoKhanOTruong", typeof(string));
                dt.Columns.Add("KhoKhanONha", typeof(string));
                dt.Columns.Add("LoLangHienTai", typeof(string));
                dt.Columns.Add("NguoiGiupDo", typeof(string));
                dt.Columns.Add("GiaiQuyetHienTai", typeof(string));
                dt.Columns.Add("NguoiTamSu", typeof(string));
                dt.Columns.Add("QuaHientai", typeof(string));
                dt.Columns.Add("TuMoTa", typeof(string));
                dt.Columns.Add("TuMoTaHienTai", typeof(string));
                dt.Columns.Add("Channel", typeof(string));
                dt.Columns.Add("CreateDate", typeof(string));

                if (data.Count() > 0)
                {
                    int stt = 1;
                    foreach (var item in data)
                    {
                        dt.Rows.Add(item.Id, item.StudentId, item.IdNo, item.ParentId, item.ParentCode, item.TenDaiLy, item.CapDaiLy, item.DanhXung, item.IsChaMeHienTai, item.DiemChaMeHienTai
                            , item.HoVaTenPH1, item.MoiQuanHePH1, item.SDTPH1, item.NgaySinhPH1, item.GioiTinhPH1, item.FacebookPH1, item.HoTenPH2, item.MoiQuanHePH2
                            , item.SDTPH2,item.EmailPH2, item.NgaySinhPH2, item.GioiTinhPH2, item.FacebookPH2, item.NamHoc, item.GhiChu, item.DiaChi, item.DiaChiNhanQua
                            , item.TenNganHang, item.SoTaiKhoan, item.ChuTaiKhoan,item.NgheNghiep,item.SoThich,item.TinhTrangHonNhan,item.SoNguoiCon,item.HinhAnhGiaDinh, item.ThoiGianLienHe
                            , item.DongLucThamGia, item.MongMuon, item.KhoKhanCuocSong, item.PhanMemSuDung, item.ThoiGianChoCon, item.GopYKien, item.SoConThamGiaHienTai, item.ThoiGianHoc, item.HoVaTenHienTai, item.GiayKhaiSinh
                            , item.NgaySinhHienTai, item.GioiTinhHienTai, item.TruongHocHienTai, item.SizeAo, item.ThanhTichHocTap, item.DieuTuHao, item.DiemYeu, item.UocMo, item.SoThichHienTai, item.ThoiQuen
                            , item.KhoKhanOTruong, item.KhoKhanONha, item.LoLangHienTai, item.NguoiGiupDo, item.GiaiQuyetHienTai, item.NguoiTamSu, item.QuaHientai, item.TuMoTa, item.TuMoTaHienTai, item.Channel, item.CreateDate);
                        stt++;
                    }
                }
                dt.AcceptChanges();
            }
            catch (Exception ex)
            {

            }

            //Name of File  
            string fileName = "du_lieu_dau_vao.xlsx";
            using (XLWorkbook wb = new XLWorkbook())
            {
                //Add DataTable in worksheet  
                wb.Worksheets.Add(dt);
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    //Return xlsx Excel File  
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
                }
            }

        }

        private List<O_ParentOrigin> GetAllData()
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "GET_EXPORT_ALL");
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<O_ParentOrigin>("SP2_O_ParentOriginLocal", paramaters, null, true, null, System.Data.CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<O_ParentOrigin>();
            }
        }

        private O_ParentOrigin GetDetail(int id)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "GETBY_DETAIL");
                    paramaters.Add("@StudentId", id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<O_ParentOrigin>("SP2_O_ParentOriginLocal", paramaters, null, true, null, System.Data.CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return new O_ParentOrigin();
            }
        }

        private O_ParentOriginReport GetReport(DateTime StartDate, DateTime EndDate, bool IsAll)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "REPORT");
                    paramaters.Add("@StartDate", StartDate);
                    paramaters.Add("@EndDate", EndDate);
                    paramaters.Add("@IsAll", IsAll);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<O_ParentOriginReport>("SP2_O_ParentOriginLocal", paramaters, null, true, null, System.Data.CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return new O_ParentOriginReport();
            }
        }
    }
}