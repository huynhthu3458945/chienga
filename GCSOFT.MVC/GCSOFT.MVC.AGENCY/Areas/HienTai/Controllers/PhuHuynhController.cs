﻿using AutoMapper;
using ClosedXML.Excel;
using Dapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Areas.Agency.Data;
using GCSOFT.MVC.AGENCY.Helper;
using GCSOFT.MVC.AGENCY.ViewModels.HienTai;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.HienTai.Controllers
{
    [CompressResponseAttribute]
    public class PhuHuynhController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IOptionLineService _optionLineService;
        private readonly IProvinceService _provinceService;
        private readonly IDistrictService _districtService;
        private string sysCategory = "HIENTAI_PHUHUYNH";
        private bool? permission = false;
        private string hasValue;
        private O_ParentInfo parentInfo;
        private O_ParentCreateEdit parentCreateEdit;
        private O_ParentDetails parentDetails;
        private string hienTaiConnection = ConfigurationManager.ConnectionStrings["HienTaiConnection"].ConnectionString;
        #endregion
        public PhuHuynhController
            (
                IVuViecService vuViecService
                , IOptionLineService optionLineService
                , IProvinceService provinceService
                , IDistrictService districtService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _optionLineService = optionLineService;
            _provinceService = provinceService;
            _districtService = districtService;
        }
        // GET: HienTai/PhuHuynh
        public ActionResult Index(int? currPage, string code, string fullName, string phone, string email, int? teacherId)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion

            var sqlHelper = new SqlExecHelper(); 

            int pageString = currPage ?? 0;
            int _currPage = pageString == 0 ? 1 : pageString;
            var parentPage = new O_ParentPage();
            parentPage.TeacherList = sqlHelper.SqlTeacherListList(GetUser().teacherInfo).ToSelectListItems(teacherId ?? GetUser().teacherInfo.Id);
            int totalRecord = TotalRecord(code, fullName, phone, email, teacherId ?? GetUser().teacherInfo.Id);
            parentPage.paging = new Paging(totalRecord, _currPage);
            parentPage.ParentList = ParentList(parentPage.paging.start, parentPage.paging.offset, code, fullName, phone, email, teacherId ?? GetUser().teacherInfo.Id).ToList();
            return View(parentPage);
        }
        public ActionResult Details(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.ViewDetail);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@Action", "GETDETAILS");
                paramaters.Add("@Id", id);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var results = conn.Query<O_ParentDetails>("SP2_O_Parent", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                parentDetails = results.FirstOrDefault();
                parentDetails.ParentStudents = SqlParentStudentList(id);
            }
            return View(parentDetails);
        }
        public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            parentCreateEdit = new O_ParentCreateEdit();
            parentCreateEdit.IsCreate = true;
            parentCreateEdit.Code = StringUtil.GetProductID("P", GetMax(), 4);
            parentCreateEdit.CityList = Mapper.Map<IEnumerable<ProvinceViewModel>>(_provinceService.FindAll()).ToSelectListItems(0);
            parentCreateEdit.DistrictList = Mapper.Map<IEnumerable<DistrictViewModel>>(_districtService.FindAll(0)).ToSelectListItems(0);
            parentCreateEdit.GenderList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("GENDER")).ToSelectListItems("");
            parentCreateEdit.GuardianList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("GUARDIAN")).ToSelectListItems("");
            var sqlHelper = new SqlExecHelper();
            parentCreateEdit.TeacherList = sqlHelper.SqlTeacherListList(GetUser().teacherInfo).ToSelectListItems(0);
            parentCreateEdit.AgencyList = sqlHelper.SqlGetAgency().ToSelectListItems(0);
            return View(parentCreateEdit);
        }
        public ActionResult Edit(int Id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            parentCreateEdit = GetBy(Id);
            parentCreateEdit.CityList = Mapper.Map<IEnumerable<ProvinceViewModel>>(_provinceService.FindAll()).ToSelectListItems(parentCreateEdit.ProvinceId);
            parentCreateEdit.DistrictList = Mapper.Map<IEnumerable<DistrictViewModel>>(_districtService.FindAll(parentCreateEdit.ProvinceId)).ToSelectListItems(parentCreateEdit.DistrictId);
            parentCreateEdit.GenderList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("GENDER")).ToSelectListItems("");
            parentCreateEdit.GuardianList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("GUARDIAN")).ToSelectListItems("");
            parentCreateEdit.ParentStudents = SqlParentStudentList(Id);
            var sqlHelper = new SqlExecHelper();
            parentCreateEdit.TeacherList = sqlHelper.SqlTeacherListList(GetUser().teacherInfo).ToSelectListItems(parentCreateEdit.TeacherId);
            parentCreateEdit.AgencyList = sqlHelper.SqlGetAgency().ToSelectListItems(parentCreateEdit.AgencyId);
            return View(parentCreateEdit);
        }
        [HttpPost]
        public ActionResult Create(O_ParentCreateEdit _parentCreateEdit)
        {
            try
            {
                SetData(_parentCreateEdit, true);
                var id = SqlInsert(parentInfo);
                return RedirectToAction("Edit", new { id = id, msg = "1" });
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult Edit(O_ParentCreateEdit _parentCreateEdit)
        {
            try
            {
                try
                {
                    SetData(_parentCreateEdit, false);
                    var id = SqlUpdate(parentInfo);
                    return RedirectToAction("Edit", new { id = _parentCreateEdit.Id, msg = "1" });
                }
                catch (Exception ex)
                {
                    ViewBag.Error = ex.ToString();
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public ActionResult Deletes(int id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                hasValue = SqlDelete(id);
                return Json("Xóa dữ liệu không thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }

        public ActionResult ExportParent(string code, string fullName, string phone, string email, int? teacherId)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion

            IEnumerable <O_ParentExport> data = ParentExport(code, fullName, phone, email, teacherId ?? GetUser().teacherInfo.Id).ToList();

            //create data
            DataTable dt = new DataTable();
            //Setiing Table Name  
            dt.TableName = "ExportParent";
            //Add Columns  
            dt.Columns.Add("STT", typeof(int));
            dt.Columns.Add("MaPhuHuynh", typeof(string));
            dt.Columns.Add("PhuHuynh", typeof(string));
            dt.Columns.Add("GioiTinh", typeof(string));
            dt.Columns.Add("Phone", typeof(string));
            dt.Columns.Add("Email", typeof(string));
            dt.Columns.Add("VaiTro", typeof(string));
            dt.Columns.Add("DiaChi", typeof(string));
            dt.Columns.Add("SoTaiKhoan", typeof(string));
            dt.Columns.Add("TenNganHang", typeof(string));
            dt.Columns.Add("NguoiThuHuong", typeof(string));
            dt.Columns.Add("TinhThanh", typeof(string));
            dt.Columns.Add("QuanHuyen", typeof(string));
            if (data.Count() > 0)
            {
                int stt = 1;
                foreach (var item in data)
                {
                    dt.Rows.Add(stt, item.Code, item.FullName, item.Gender, item.Phone, item.Email
                        , item.Guardian, item.Address, item.BankAccountNo, item.BankName, item.Beneficiary, item.ProvinceName, item.DistrictName);
                    stt++;
                }
            }
            dt.AcceptChanges();

            //Name of File  
            string fileName = "ExportParent.xlsx";
            using (XLWorkbook wb = new XLWorkbook())
            {
                //Add DataTable in worksheet  
                wb.Worksheets.Add(dt);
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    //Return xlsx Excel File  
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
                }
            }
        }

        #region -- Functioin --
        private int TotalRecord(string code, string fullName, string phone, string email, int teacherId)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "TOTAL.ROW");
                    paramaters.Add("@Code", code ?? string.Empty);
                    paramaters.Add("@Phone", phone ?? string.Empty);
                    paramaters.Add("@FullName", fullName ?? string.Empty);
                    paramaters.Add("@Email", email ?? string.Empty);
                    paramaters.Add("@TeacherId", teacherId);
                    paramaters.Add("@AgencyID", GetUser().teacherInfo.AgencyId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var recordOfPageResults = conn.Query<RecordOfPage>("SP2_O_Parent", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return recordOfPageResults.FirstOrDefault().TotalRow;
                }
            }
            catch { return 0; }
        }
        private IEnumerable<O_ParentList> ParentList(int start, int offset, string code, string fullName, string phone, string email, int teacherId)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "FINDALL");
                    paramaters.Add("@Code", code ?? string.Empty);
                    paramaters.Add("@Phone", phone ?? string.Empty);
                    paramaters.Add("@FullName", fullName ?? string.Empty);
                    paramaters.Add("@Email", email ?? string.Empty);
                    paramaters.Add("@AgencyID", GetUser().teacherInfo.AgencyId);
                    paramaters.Add("@TeacherId", teacherId);
                    paramaters.Add("@Start", start);
                    paramaters.Add("@Offset", offset);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<O_ParentList>("SP2_O_Parent", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }
            }
            catch { return new List<O_ParentList>(); }
        }
        private IEnumerable<O_ParentExport> ParentExport(string code, string fullName, string phone, string email, int teacherId)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "FINDEXPORT");
                    paramaters.Add("@Code", code ?? string.Empty);
                    paramaters.Add("@Phone", phone ?? string.Empty);
                    paramaters.Add("@FullName", fullName ?? string.Empty);
                    paramaters.Add("@Email", email ?? string.Empty);
                    paramaters.Add("@AgencyID", GetUser().teacherInfo.AgencyId);
                    paramaters.Add("@TeacherId", teacherId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<O_ParentExport>("SP2_O_Parent", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }
            }
            catch { return new List<O_ParentExport>(); }
        }
        private string GetMax()
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "GETMAX");
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var recordOfPageResults = conn.Query<MaxCode>("SP2_O_Parent", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return recordOfPageResults.FirstOrDefault().Code;
                }
            }
            catch { return ""; }
        }
        private O_ParentCreateEdit GetBy(int id)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "GETBY");
                    paramaters.Add("@Id", id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<O_ParentCreateEdit>("SP2_O_Parent", paramaters, null, true, null, System.Data.CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch { return new O_ParentCreateEdit(); }
        }
        private int SqlInsert(O_ParentInfo parentInfo)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "INSERT");
                    foreach (PropertyInfo propertyInfo in parentInfo.GetType().GetProperties())
                    {
                        paramaters.Add("@" + propertyInfo.Name, propertyInfo.GetValue(parentInfo, null));
                    }
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP2_O_Parent", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return paramaters.Get<Int32>("@RetCode");
                }
            }
            catch  { return 0; }
        }
        private int SqlUpdate(O_ParentInfo parentInfo)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "UPDATE");
                    foreach (PropertyInfo propertyInfo in parentInfo.GetType().GetProperties())
                    {
                        paramaters.Add("@" + propertyInfo.Name, propertyInfo.GetValue(parentInfo, null));
                    }
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP2_O_Parent", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return parentInfo.Id;
                }
            }
            catch { return 0; }
        }
        private string SqlDelete(int id)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "DELETE");
                    paramaters.Add("@Id", id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP2_O_Parent", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return "";
                }
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        private void SetData(O_ParentCreateEdit _parentCreateEdit, bool isCreate)
        {
            if (isCreate)
            {
                parentInfo = new O_ParentInfo();
                parentInfo.Code = StringUtil.GetProductID("P", GetMax(), 4);
            }
            else
            {
                parentInfo = Mapper.Map<O_ParentInfo>(GetBy(_parentCreateEdit.Id));
            }
            parentInfo.FullName = _parentCreateEdit.FullName;
            parentInfo.Email = _parentCreateEdit.Email;
            parentInfo.Phone = _parentCreateEdit.Phone;
            parentInfo.Facebook = _parentCreateEdit.Facebook;
            parentInfo.ProvinceId = _parentCreateEdit.ProvinceId;
            parentInfo.DistrictId = _parentCreateEdit.DistrictId;
            parentInfo.Beneficiary = _parentCreateEdit.Beneficiary;
            parentInfo.BankAccountNo = _parentCreateEdit.BankAccountNo;
            parentInfo.BankName = _parentCreateEdit.BankName;
            parentInfo.Remark = _parentCreateEdit.Remark;
            parentInfo.Address = _parentCreateEdit.Address;
            parentInfo.AgencyId = _parentCreateEdit.AgencyId;
            parentInfo.GuardianId = _parentCreateEdit.GuardianId;
            parentInfo.Gender = _parentCreateEdit.Gender;
            parentInfo.JobDesc = _parentCreateEdit.JobDesc;
            parentInfo.TeacherId = _parentCreateEdit.TeacherId;
            parentInfo.CodeAcc = _parentCreateEdit.CodeAcc;
        }
        private IEnumerable<O_ParentStudent> SqlParentStudentList(int parentId)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "CHILDREN");
                    paramaters.Add("@Id", parentId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<O_ParentStudent>("SP2_O_Parent", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }
            }
            catch { return new List<O_ParentStudent>(); }
        }
        #endregion
    }
}