﻿using AutoMapper;
using ClosedXML.Excel;
using Dapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Areas.Agency.Data;
using GCSOFT.MVC.AGENCY.Helper;
using GCSOFT.MVC.AGENCY.ViewModels.HienTai;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.HienTai.Controllers
{
    [CompressResponseAttribute]
    public class QuaTrinhLamSAOController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IOptionLineService _optionLineService;
        private string sysCategory = "HT_BC_QUATRINH";
        private bool? permission = false;
        private string hasValue;
        private string hienTaiConnection = ConfigurationManager.ConnectionStrings["HienTaiConnection"].ConnectionString;
        #endregion
        public QuaTrinhLamSAOController
            (
                IVuViecService vuViecService
                , IOptionLineService optionLineService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _optionLineService = optionLineService;
        }
        // GET: HienTai/QuaTrinhLamSAO
        public ActionResult Index(SP2_RPT_STAR_STATUS_SUMMARY_Page c)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            try
            {
                var sqlHelper = new SqlExecHelper();
                var teacherInfo = GetUser().teacherInfo;
                c.SchoolClassList = sqlHelper.SqlSchoolClassList(teacherInfo).ToSelectListItems(c.SchoolClassId ?? 0);
                c.TeacherId = c.TeacherId ?? GetUser().teacherInfo.Id;
                c.TeacherList = sqlHelper.SqlTeacherListList(teacherInfo).ToSelectListItems(c.TeacherId ?? 0);
                c.StudentList = sqlHelper.SqlStudentList(teacherInfo).ToSelectListItems(c.StudentId ?? 0);
                c.RptStarSummary = SqlRptStarSummary(c);
                return View(c);
            }
            catch (Exception ex)
            {
                var error = ex.ToString();
                ViewBag.Error = ex;
                return View("Error");
            }
        }
        public ActionResult Detail(SP2_RPT_STAR_STATUS_DETAIL_Page c)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            try
            {
                ViewBag.StudentId = c.StudentId;
                c.SchoolClassList = SqlSchoolClassStarList().ToSelectListItems(c.SchoolClassStarId ?? 0);
                c.RptStarDetail = SqlRptStarDetail(c);
                if (c.RptStarDetail == null)
                    c.RptStarDetail = new List<SP2_RPT_STAR_STATUS_DETAIL>();
                return View(c);
            }
            catch (Exception ex)
            {
                var error = ex.ToString();
                ViewBag.Error = ex;
                return View("Error");
            }
        }

        public ActionResult ExportInProcessDetail(SP2_RPT_STAR_STATUS_DETAIL_Page c)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion

            //lay data
            IEnumerable<SP2_RPT_STAR_STATUS_DETAIL> data = SqlRptStarDetail(c);

            //create data
            DataTable dt = new DataTable();
            //Setiing Table Name  
            dt.TableName = "ExportInProcessDetail";
            //Add Columns  
            dt.Columns.Add("STT", typeof(int));
            dt.Columns.Add("SBD", typeof(string));
            dt.Columns.Add("HienTai", typeof(string));
            dt.Columns.Add("Lop", typeof(string));
            dt.Columns.Add("GiaoVien", typeof(string));
            dt.Columns.Add("Sao", typeof(string));
            dt.Columns.Add("TrangThai", typeof(string));
            dt.Columns.Add("SoNgayLam", typeof(string));
            dt.Columns.Add("NgayBatDauVaKetThuc", typeof(string));
            if (data.Count() > 0)
            {
                int stt = 1;
                foreach (var item in data)
                {
                    dt.Rows.Add(stt, item.IDNo, item.FullName, item.SchoolName, item.TeacherName,item.StarName, item.StatusName, string.Format("{0}/{1}", item.NoOfDay,item.TotalDay)
                        ,string.Format("{0} - {1}", item.StartProcessDate.ToString("dd/MM/yyyy"), item.EndProcessDate.ToString("dd/MM/yyyy")));
                    stt++;
                }
            }
            dt.AcceptChanges();

            //Name of File  
            string fileName = "ExportInProcessDetail.xlsx";
            using (XLWorkbook wb = new XLWorkbook())
            {
                //Add DataTable in worksheet  
                wb.Worksheets.Add(dt);
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    //Return xlsx Excel File  
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
                }
            }
        }

        public ActionResult ExportInProcessSummary(SP2_RPT_STAR_STATUS_SUMMARY_Page c)
        {
            //xu ly
            var teacherInfo = GetUser().teacherInfo;

            //lay data
            IEnumerable<SP2_RPT_STAR_STATUS_SUMMARY> data = SqlRptStarSummary(c);

            //create data
            DataTable dt = new DataTable();
            //Setiing Table Name  
            dt.TableName = "ExportInProcessSummary";
            //Add Columns  
            dt.Columns.Add("STT", typeof(int));
            dt.Columns.Add("SBD", typeof(string));
            dt.Columns.Add("HienTai", typeof(string));
            dt.Columns.Add("Lop", typeof(string));
            dt.Columns.Add("GiaoVien", typeof(string));
            dt.Columns.Add("TrangThai", typeof(string));
            if (data.Count() > 0)
            {
                int stt = 1;
                foreach (var item in data)
                {
                    dt.Rows.Add(stt, item.IDNo, item.FullName, item.SchoolName, item.TeacherName, item.StatusName);
                    stt++;
                }
            }
            dt.AcceptChanges();

            //Name of File  
            string fileName = "ExportInProcessSummary.xlsx";
            using (XLWorkbook wb = new XLWorkbook())
            {
                //Add DataTable in worksheet  
                wb.Worksheets.Add(dt);
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    //Return xlsx Excel File  
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
                }
            }
        }

        private IEnumerable<OptionInt> SqlSchoolClassStarList()
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "FINDALL2");
                    paramaters.Add("@AgencyId", GetUser().teacherInfo.AgencyId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<OptionInt>("SP2_O_SchoolClass", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }
            }
            catch { return new List<OptionInt>(); }
        }
        private List<SP2_RPT_STAR_STATUS_SUMMARY> SqlRptStarSummary(SP2_RPT_STAR_STATUS_SUMMARY_Page c)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "SUMMARY");
                    paramaters.Add("@AgencyId", GetUser().teacherInfo.AgencyId);
                    paramaters.Add("@StudentId", c.StudentId);
                    paramaters.Add("@TeacherId", c.TeacherId);
                    paramaters.Add("@SchoolClassId", c.SchoolClassId);
                    return conn.Query<SP2_RPT_STAR_STATUS_SUMMARY>("SP2_RPT_STAR_STATUS", paramaters, null, true, null, System.Data.CommandType.StoredProcedure).ToList();
                }
            }
            catch { return new List<SP2_RPT_STAR_STATUS_SUMMARY>(); }
        }
        private List<SP2_RPT_STAR_STATUS_DETAIL> SqlRptStarDetail(SP2_RPT_STAR_STATUS_DETAIL_Page c)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "DETAIL");
                    paramaters.Add("@AgencyId", GetUser().teacherInfo.AgencyId);
                    paramaters.Add("@SBD", c.SBD);
                    paramaters.Add("@StatusCode", c.StatusCode);
                    paramaters.Add("@StudentId", c.StudentId ?? 0);
                    paramaters.Add("@SchoolClassId", c.SchoolClassStarId ?? 0);
                    return conn.Query<SP2_RPT_STAR_STATUS_DETAIL>("SP2_RPT_STAR_STATUS", paramaters, null, true, null, System.Data.CommandType.StoredProcedure).ToList();
                }
            }
            catch { return new List<SP2_RPT_STAR_STATUS_DETAIL>(); }
        }
    }
}