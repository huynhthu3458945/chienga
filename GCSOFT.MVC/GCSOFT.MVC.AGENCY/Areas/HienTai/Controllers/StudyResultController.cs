﻿using Dapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Helper;
using GCSOFT.MVC.AGENCY.ViewModels.HienTai;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.HienTai.Controllers
{
    [CompressResponseAttribute]
    public class StudyResultController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IOptionLineService _optionLineService;
        private string sysCategory = "HIENTAI_KETQUAHOCTAP";
        private bool? permission = false;
        private string hasValue;
        private string hienTaiConnection = ConfigurationManager.ConnectionStrings["HienTaiConnection"].ConnectionString;
        private O_StudentDetails studentDetails;
        #endregion

        public StudyResultController
           (
               IVuViecService vuViecService
               , IOptionLineService optionLineService
           ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _optionLineService = optionLineService;
        }

        public ActionResult Index(O_StudyResultPage c)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            try
            {
                //var notice = new O_StudyResultPage();
                var paramaters = new DynamicParameters();
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();

                    var sqlHelper = new SqlExecHelper();
                    var teacherInfo = GetUser().teacherInfo;
                    c.SchoolClassList = sqlHelper.SqlSchoolClassList(teacherInfo).ToSelectListItems(c.SchoolClassId ?? 0);
                    c.TeacherId = c.TeacherId ?? GetUser().teacherInfo.Id;
                    c.TeacherList = sqlHelper.SqlTeacherListList(teacherInfo).ToSelectListItems(c.TeacherId ?? 0);
                    c.StudentList = sqlHelper.SqlStudentList(teacherInfo).ToSelectListItems(c.StudentId ?? 0);

                    //-- Paging
                    int pageString = c.currPage ?? 0;
                    int _currPage = pageString == 0 ? 1 : pageString;
                    paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "TOTAL.ROW");
                    if (c.SchoolClassId != null)
                    {
                        paramaters.Add("@SchoolClassId", c.SchoolClassId);
                    }
                    if (c.StudentId != null)
                    {
                        paramaters.Add("@StudentId", c.StudentId);
                    }
                    if (c.TeacherId != null)
                    {
                        paramaters.Add("@TeacherId", c.TeacherId);
                    }
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var recordOfPageResults = conn.Query<RecordOfPage>("SP2_O_StudyResult", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    int totalRecord = recordOfPageResults.FirstOrDefault().TotalRow;
                    c.paging = new Paging(totalRecord, _currPage);
                    //-- Paging +

                    paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_KETQUAHOCTAP_LIST");
                    if (c.SchoolClassId != null)
                    {
                        paramaters.Add("@SchoolClassId", c.SchoolClassId);
                    }
                    if (c.StudentId != null)
                    {
                        paramaters.Add("@StudentId", c.StudentId);
                    }
                    if (c.TeacherId != null)
                    {
                        paramaters.Add("@TeacherId", c.TeacherId);
                    }
                    paramaters.Add("@Start", c.paging.start);
                    paramaters.Add("@Offset", c.paging.offset);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var data = conn.Query<O_StudyResult>("SP2_O_StudyResult", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    c.StudyResults = data.ToList();
                    return View(c);
                }
            }
            catch (Exception ex)
            {
                var error = ex.ToString();
                ViewBag.Error = ex;
                return View("Error");
            }
        }

        public ActionResult Detail(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.ViewDetail);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@Action", "GETDETAILS");
                paramaters.Add("@Id", id);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var results = conn.Query<O_StudentDetails>("SP2_O_Student", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                studentDetails = results.FirstOrDefault();
                studentDetails.StudentStarList = SqlStudentStarList(id);
            }
            return View(studentDetails);
        }

        public ActionResult ProcessLine(int? currPage, int id , int studentStarId, int processHeaderId)
        {
            //
            int processId = processHeaderId != 0 ? processHeaderId : sqlProcessHeaderList(id, studentStarId).FirstOrDefault().Key;

            O_ProcessHeaderView o_ProcessHeaderView = sqlProcessHeader(id, studentStarId);
            if (o_ProcessHeaderView != null)
            {
                o_ProcessHeaderView.StudentId = id;
                o_ProcessHeaderView.StudentStarId = studentStarId;
                //o_ProcessHeaderView.ProcessLines = sqlProcessLine(id, studentStarId, processId);
                o_ProcessHeaderView.Id = processId;
                o_ProcessHeaderView.ProcessHeaders = sqlProcessHeaderList(id, studentStarId).ToSelectListItems(processId);
            }

            List<O_ProcessLine> dataProcessLine = new List<O_ProcessLine>();

            try
            {
                var paramaters = new DynamicParameters();
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();

                    var sqlHelper = new SqlExecHelper();
                    //-- Paging
                    int pageString = currPage ?? 0;
                    int _currPage = pageString == 0 ? 1 : pageString;
                    paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_KQHT_TOTAL_ROW");
                    paramaters.Add("@StudentId", id);
                    paramaters.Add("@StudentStarId", studentStarId);
                    paramaters.Add("@ProcessHeaderId", processHeaderId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var recordOfPageResults = conn.Query<RecordOfPage>("SP2_O_StudyResult", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    int totalRecord = recordOfPageResults.FirstOrDefault().TotalRow;
                    o_ProcessHeaderView.paging = new Paging(totalRecord, _currPage);
                    //-- Paging +
     
                    paramaters.Add("@Action", "HIENTAI_KQHT_PROCESSLINE");
                    paramaters.Add("@StudentId", id);
                    paramaters.Add("@StudentStarId", studentStarId);
                    paramaters.Add("@ProcessHeaderId", processHeaderId);
                    paramaters.Add("@Start", o_ProcessHeaderView.paging.start);
                    paramaters.Add("@Offset", o_ProcessHeaderView.paging.offset);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    dataProcessLine = conn.Query<O_ProcessLine>("SP2_O_StudyResult", paramaters, null, true, null, System.Data.CommandType.StoredProcedure).ToList();
                }
            }
            catch {
                dataProcessLine = new List<O_ProcessLine>();
            }
            o_ProcessHeaderView.ProcessLines = dataProcessLine;
            return View(o_ProcessHeaderView);
        }

        public ActionResult HandleStar(int id, int studentStarId, int processHeaderId, string date)
        {
            O_StudyResultChangeHistory model = new O_StudyResultChangeHistory();
            model.ProcessHeaderId = processHeaderId;
            model.ProcessHeaderIdDelete = processHeaderId;
            model.ProcessHeaders = sqlProcessHeaderList(id, studentStarId).ToSelectListItems(processHeaderId);
            model.Tickets = sqlTicket().ToSelectListItems(0);
            model.StudentId = id;
            model.Date = date;
            model.StudentStarId = studentStarId;
            return View(model);
        }

        [HttpPost]
        public ActionResult HandleStar(O_StudyResultChangeHistory model)
        {
            model.CreateBy = GetUser().ID;
            model.CreateDate = DateTime.Now;

            int result = sqlHandleProcessLine(model);
            return RedirectToAction("ProcessLine", "StudyResult", new { Area = "HienTai", id = model.StudentId, studentStarId = model.StudentStarId, processHeaderId = model.ProcessHeaderId });
        }

        private IEnumerable<StudentStar> SqlStudentStarList(int studentId)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "STUDENTSTAR");
                    paramaters.Add("@Id", studentId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<StudentStar>("SP2_O_Student", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }
            }
            catch { return new List<StudentStar>(); }
        }

        private O_ProcessHeaderView sqlProcessHeader(int studentId, int studentStarId)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_KQHT_PROCESSHEADER");
                    paramaters.Add("@StudentId", studentId);
                    paramaters.Add("@StudentStarId", studentStarId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var data = conn.Query<O_ProcessHeaderView>("SP2_O_StudyResult", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return data.SingleOrDefault(); 
                }
            }
            catch (Exception ex)
            { 
                return new O_ProcessHeaderView();
            }
        }

        private IEnumerable<OptionInt> sqlProcessHeaderList(int studentId, int studentStarId)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_KQHT_PROCESSHEADER_LIST");
                    paramaters.Add("@StudentId", studentId);
                    paramaters.Add("@StudentStarId", studentStarId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<OptionInt>("SP2_O_StudyResult", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }
            }
            catch { return new List<OptionInt>(); }
        }

        private List<O_ProcessLine> sqlProcessLine(int studentId, int studentStarId, int processHeaderId)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_KQHT_PROCESSLINE");
                    paramaters.Add("@StudentId", studentId);
                    paramaters.Add("@StudentStarId", studentStarId);
                    paramaters.Add("@ProcessHeaderId", processHeaderId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<O_ProcessLine>("SP2_O_StudyResult", paramaters, null, true, null, System.Data.CommandType.StoredProcedure).ToList();
                }
            }
            catch { return new List<O_ProcessLine>(); }
        }

        private int sqlHandleProcessLine(O_StudyResultChangeHistory data)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_KQHT_PROCESSLINE_HANDLE");
                    foreach (PropertyInfo propertyInfo in data.GetType().GetProperties())
                    {
                        if (propertyInfo.Name != "Tickets" && propertyInfo.Name != "ProcessHeaders")
                        {
                            paramaters.Add("@" + propertyInfo.Name, propertyInfo.GetValue(data, null));
                        }
                    }
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP2_O_StudyResult", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return paramaters.Get<Int32>("@RetCode");
                }
            }
            catch (Exception ex)
            { return 0; }
        }

        private IEnumerable<OptionInt> sqlTicket()
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_KQHT_TICKET");
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<OptionInt>("SP2_O_StudyResult", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }
            }
            catch { return new List<OptionInt>(); }
        }



    }
}