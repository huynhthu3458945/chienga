﻿using Dapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Helper;
using GCSOFT.MVC.AGENCY.ViewModels.HienTai;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.HienTai.Controllers
{
    [CompressResponseAttribute]
    public class SummaryLevelController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IOptionLineService _optionLineService;
        private string sysCategory = "HIENTAI_TONGKETLEVEL";
        private bool? permission = false;
        private string hasValue;
        private string hienTaiConnection = ConfigurationManager.ConnectionStrings["HienTaiConnection"].ConnectionString;

        public SummaryLevelController(IVuViecService vuViecService, IOptionLineService optionLineService) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _optionLineService = optionLineService;
        }
        #endregion

        public ActionResult Index(O_SummaryLevelPage c)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion

            var paramaters = new DynamicParameters();
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();

                var sqlHelper = new SqlExecHelper();
                var teacherInfo = GetUser().teacherInfo;
                c.SchoolClassList = sqlHelper.SqlSchoolClassList(teacherInfo).ToSelectListItems(c.SchoolClassId ?? 0);
                c.TeacherId = c.TeacherId ?? GetUser().teacherInfo.Id;
                c.TeacherList = sqlHelper.SqlTeacherListList(teacherInfo).ToSelectListItems(c.TeacherId ?? 0);
                c.StudentList = sqlHelper.SqlStudentList(teacherInfo).ToSelectListItems(c.StudentId ?? 0);
                c.LevelList = sqlLevelList().ToSelectListItems(c.LevelId ?? 0);

                //-- Paging
                int pageString = c.currPage ?? 0;
                int _currPage = pageString == 0 ? 1 : pageString;
                paramaters = new DynamicParameters();
                paramaters.Add("@Action", "TOTAL.ROW");
                if (c.SchoolClassId != null)
                {
                    paramaters.Add("@SchoolClassId", c.SchoolClassId);
                }
                if (c.StudentId != null)
                {
                    paramaters.Add("@StudentId", c.StudentId);
                }
                if (c.TeacherId != null)
                {
                    paramaters.Add("@TeacherId", c.TeacherId);
                }
                if (c.LevelId != null)
                {
                    paramaters.Add("@LevelId", c.TeacherId);
                }
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var recordOfPageResults = conn.Query<RecordOfPage>("SP2_O_SummaryLevel", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                int totalRecord = recordOfPageResults.FirstOrDefault().TotalRow;
                c.paging = new Paging(totalRecord, _currPage);
                //-- Paging +

                paramaters = new DynamicParameters();
                paramaters.Add("@Action", "HIENTAI_TONGKET_LEVEL_LIST");
                if (c.SchoolClassId != null)
                {
                    paramaters.Add("@SchoolClassId", c.SchoolClassId);
                }
                if (c.StudentId != null)
                {
                    paramaters.Add("@StudentId", c.StudentId);
                }
                if (c.TeacherId != null)
                {
                    paramaters.Add("@TeacherId", c.TeacherId);
                }
                if (c.LevelId != null)
                {
                    paramaters.Add("@LevelId", c.TeacherId);
                }
                paramaters.Add("@Start", c.paging.start);
                paramaters.Add("@Offset", c.paging.offset);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var data = conn.Query<O_SummaryLevelView>("SP2_O_SummaryLevel", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                c.SummaryLevels = data.ToList();
            }
            return View(c);
        }

        public ActionResult UpLevel(int id, int number)
        {
            O_SummaryLevel model = new O_SummaryLevel();
            model.StudentId = id;
            model.NumberOfStarCompleted = number;
            model.LevelList = sqlLevelList().ToSelectListItems(model.LevelId);
            return View(model);
        }

        [HttpPost]
        public ActionResult UpLevel(O_SummaryLevel model)
        {
            model.CreateBy = GetUser().ID;
            model.CreateDate = DateTime.Now;
            var result = sqlInsert(model);
            return RedirectToAction("Index", "SummaryLevel", new { area = "HienTai"});
        }

        public ActionResult ListStar(int id)
        {
            List<O_SummaryLevelStar> data = new List<O_SummaryLevelStar>();
            data = sqlSummaryLevelStar(id);
            return View(data);
        }

        public ActionResult History(int id)
        {
            List<O_SummaryLevelHistory> data = sqlSummaryLevelHistory(id);
            return View(data);
        }

        public IEnumerable<OptionInt> sqlLevelList()
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_TONGKET_LEVEL_TYPE");
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<OptionInt>("SP2_O_SummaryLevel", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }
            }
            catch { return new List<OptionInt>(); }
        }

        private int sqlInsert(O_SummaryLevel data)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_TONGKET_LEVEL_CREATE");
                    foreach (PropertyInfo propertyInfo in data.GetType().GetProperties())
                    {
                        if (propertyInfo.Name != "LevelList")
                        {
                            paramaters.Add("@" + propertyInfo.Name, propertyInfo.GetValue(data, null));
                        }
                    }
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP2_O_SummaryLevel", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return paramaters.Get<Int32>("@RetCode");
                }
            }
            catch (Exception ex)
            { return 0; }
        }

        private List<O_SummaryLevelHistory> sqlSummaryLevelHistory(int studentId)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_TONGKET_LEVEL_HISTORY");
                    paramaters.Add("@StudentId", studentId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var data = conn.Query<O_SummaryLevelHistory>("SP2_O_SummaryLevel", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<O_SummaryLevelHistory>();
            }
        }

        private List<O_SummaryLevelStar> sqlSummaryLevelStar(int studentId)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_TONGKET_STAR_COMPLETED");
                    paramaters.Add("@StudentId", studentId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var data = conn.Query<O_SummaryLevelStar>("SP2_O_SummaryLevel", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<O_SummaryLevelStar>();
            }
        }

    }
}