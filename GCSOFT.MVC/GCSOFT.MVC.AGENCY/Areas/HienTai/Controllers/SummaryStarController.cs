﻿using AutoMapper;
using Dapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Areas.Agency.Data;
using GCSOFT.MVC.AGENCY.Helper;
using GCSOFT.MVC.AGENCY.ViewModels.HienTai;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.HienTai.Controllers
{
    [CompressResponseAttribute]
    public class SummaryStarController : BaseController
    {
        #region -- Properties --

        private readonly IVuViecService _vuViecService;
        private readonly IOptionLineService _optionLineService;
        private readonly IMailSetupService _mailSetupService;
        private string sysCategory = "HIENTAI_TONGKETSAO";
        private bool? permission = false;
        private string hasValue;
        private string hienTaiConnection = ConfigurationManager.ConnectionStrings["HienTaiConnection"].ConnectionString;

        public SummaryStarController(IVuViecService vuViecService, IOptionLineService optionLineService,IMailSetupService mailSetupService) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _optionLineService = optionLineService;
            _mailSetupService = mailSetupService;
        }

        #endregion

        public ActionResult Index(O_SummaryStarPage c)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion

            var paramaters = new DynamicParameters();
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();

                var sqlHelper = new SqlExecHelper();
                var teacherInfo = GetUser().teacherInfo;
                c.SchoolClassList = sqlHelper.SqlSchoolClassList(teacherInfo).ToSelectListItems(c.SchoolClassId ?? 0);
                c.TeacherId = c.TeacherId ?? GetUser().teacherInfo.Id;
                c.TeacherList = sqlHelper.SqlTeacherListList(teacherInfo).ToSelectListItems(c.TeacherId ?? 0);
                c.StudentList = sqlHelper.SqlStudentList(teacherInfo).ToSelectListItems(c.StudentId ?? 0);

                //-- Paging
                int pageString = c.currPage ?? 0;
                int _currPage = pageString == 0 ? 1 : pageString;
                paramaters = new DynamicParameters();
                paramaters.Add("@Action", "TOTAL.ROW");
                if (c.SchoolClassId != null)
                {
                    paramaters.Add("@SchoolClassId", c.SchoolClassId);
                }
                if (c.StudentId != null)
                {
                    paramaters.Add("@StudentId", c.StudentId);
                }
                if (c.TeacherId != null)
                {
                    paramaters.Add("@TeacherId", c.TeacherId);
                }
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var recordOfPageResults = conn.Query<RecordOfPage>("SP2_O_SummaryStar", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                int totalRecord = recordOfPageResults.FirstOrDefault().TotalRow;
                c.paging = new Paging(totalRecord, _currPage);
                //-- Paging +

                paramaters = new DynamicParameters();
                paramaters.Add("@Action", "HIENTAI_TONGKETSAO_LIST");
                if (c.SchoolClassId != null)
                {
                    paramaters.Add("@SchoolClassId", c.SchoolClassId);
                }
                if (c.StudentId != null)
                {
                    paramaters.Add("@StudentId", c.StudentId);
                }
                if (c.TeacherId != null)
                {
                    paramaters.Add("@TeacherId", c.TeacherId);
                }
                paramaters.Add("@Start", c.paging.start);
                paramaters.Add("@Offset", c.paging.offset);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var data = conn.Query<O_SummaryStarView>("SP2_O_SummaryStar", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                c.SummaryStars = data.ToList();
            }
            return View(c);
        }

        public ActionResult Detail(int? currPage, int id, int studentStarId)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.ViewDetail);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion

            O_SummaryStarDetail data = new O_SummaryStarDetail();
            data = sqlSummaryStar(id, studentStarId);
            List<O_ProcessLineSummary> dataSub = new List<O_ProcessLineSummary>();
            try
            {
                var paramaters = new DynamicParameters();
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();

                    var sqlHelper = new SqlExecHelper();
                    //-- Paging
                    int pageString = currPage ?? 0;
                    int _currPage = pageString == 0 ? 1 : pageString;
                    paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_TONGKETSAO_TOTAL_ROW");
                    paramaters.Add("@StudentId", id);
                    paramaters.Add("@StudentStarId", studentStarId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var recordOfPageResults = conn.Query<RecordOfPage>("SP2_O_SummaryStar", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    int totalRecord = recordOfPageResults.FirstOrDefault().TotalRow;
                    data.paging = new Paging(totalRecord, _currPage);
                    
                    //-- Paging +
                    paramaters.Add("@Action", "HIENTAI_TONGKETSAO_PROCESSLINE");
                    paramaters.Add("@StudentId", id);
                    paramaters.Add("@StudentStarId", studentStarId);
                    paramaters.Add("@Start", data.paging.start);
                    paramaters.Add("@Offset", data.paging.offset);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    dataSub = conn.Query<O_ProcessLineSummary>("SP2_O_SummaryStar", paramaters, null, true, null, System.Data.CommandType.StoredProcedure).ToList();
                }
            }
            catch
            {
                dataSub = new List<O_ProcessLineSummary>();
            }
            data.ProcessLines = dataSub;
            return View(data);
        }

        public ActionResult Check(int id, int studentStarId, int processLineId, int studentId, int processHeaderId)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion

            //get data
            O_SummaryStar data = new O_SummaryStar();
            if (id != 0)
            {
                data = sqlSummaryStarDetail(studentId, studentStarId, processLineId);
            }
            data.StudentStarId = studentStarId;
            data.ProcessLineId = processLineId;
            data.StudentId = studentId;
            data.ProcessHeaderId = processHeaderId;
            data.SummaryStarTypes = id != 0 ? SqlSummaryStarType().ToSelectListItems(data.StatusCode) : SqlSummaryStarType().ToSelectListItems("DAT");
            data.StatusProcess = id != 0 ? SqlStatusProcess().ToSelectListItems(data.Status) : SqlStatusProcess().ToSelectListItems("300");
            return View(data);
        }

        [HttpPost]
        public ActionResult Check(O_SummaryStar model)
        {
            if (model.Id != 0) // update
            {
                model.CreateDate = DateTime.Now;
                model.CreateBy = GetUser().ID;
                int r = sqlUpdate(model);
            }
            else // insert
            {
                model.CreateDate = DateTime.Now;
                model.CreateBy = GetUser().ID;
                int r = sqlInsert(model);

                // send email
                SendMail(r);

            }
            return RedirectToAction("Detail", "SummaryStar", new {
                area = "HienTai",
                id = model.StudentId,
                studentStarId = model.StudentStarId
            });
        }

        public ActionResult History(int id, int studentStarId)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion

            List<O_ProcessLineSummary> data = new List<O_ProcessLineSummary>();
            data = sqlSummaryStarList(id, studentStarId);
            return View(data);
        }

        public ActionResult ViewProcessLine(int id, int studentStarId)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion

            List<O_ProcessLineSummary> data = new List<O_ProcessLineSummary>();
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_TONGKETSAO_PROCESSLINE_ALL");
                    paramaters.Add("@StudentId", id);
                    paramaters.Add("@StudentStarId", studentStarId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    data = conn.Query<O_ProcessLineSummary>("SP2_O_SummaryStar", paramaters, null, true, null, System.Data.CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                
            }
            return View(data);
        }

        private O_SummaryStarDetail sqlSummaryStar(int studentId, int studentStarId)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_TONGKETSAO_DETAIL");
                    paramaters.Add("@StudentId", studentId);
                    paramaters.Add("@StudentStarId", studentStarId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var data = conn.Query<O_SummaryStarDetail>("SP2_O_SummaryStar", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return data.SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                return new O_SummaryStarDetail();
            }
        }

        private O_ProcessLineSummary sqlProcessLineSummaryDetail(int studentId, int studentStarId, int processLineId)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_TONGKETSAO_PROCESSLINE");
                    paramaters.Add("@StudentId", studentId);
                    paramaters.Add("@StudentStarId", studentStarId);
                    paramaters.Add("@ProcessLineId", processLineId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var data = conn.Query<O_ProcessLineSummary>("SP2_O_SummaryStar", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return data.SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                return new O_ProcessLineSummary();
            }
        }

        private O_SummaryStar sqlSummaryStarDetail(int studentId, int studentStarId, int processLineId)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_TONGKETSAO_PROCESSLINE_DETAIL");
                    paramaters.Add("@StudentId", studentId);
                    paramaters.Add("@StudentStarId", studentStarId);
                    paramaters.Add("@ProcessLineId", processLineId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var data = conn.Query<O_SummaryStar>("SP2_O_SummaryStar", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return data.SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                return new O_SummaryStar();
            }
        }

        private List<O_ProcessLineSummary> sqlSummaryStarList(int studentId, int studentStarId)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_TONGKETSAO_PROCESSLINE_LIST");
                    paramaters.Add("@StudentId", studentId);
                    paramaters.Add("@StudentStarId", studentStarId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var data = conn.Query<O_ProcessLineSummary>("SP2_O_SummaryStar", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<O_ProcessLineSummary>();
            }
        }

        private IEnumerable<OptionString> SqlSummaryStarType()
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_TONGKETSAO_STARTUS");
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<OptionString>("SP2_O_SummaryStar", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                return new List<OptionString>();
            }
        }

        private IEnumerable<OptionString> SqlStatusProcess()
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_TONGKETSAO_STARTUS_PROCESS");
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<OptionString>("SP2_O_SummaryStar", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                return new List<OptionString>();
            }
        }

        private int sqlInsert(O_SummaryStar data)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_TONGKETSAO_CREATE");
                    foreach (PropertyInfo propertyInfo in data.GetType().GetProperties())
                    {
                        if (propertyInfo.Name != "SummaryStarTypes" && propertyInfo.Name != "StatusProcess")
                        {
                            paramaters.Add("@" + propertyInfo.Name, propertyInfo.GetValue(data, null));
                        }
                    }
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP2_O_SummaryStar", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return paramaters.Get<Int32>("@RetCode");
                }
            }
            catch (Exception ex)
            { return 0; }
        }

        private int sqlUpdate(O_SummaryStar data)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_TONGKETSAO_UPDATE");
                    foreach (PropertyInfo propertyInfo in data.GetType().GetProperties())
                    {
                        if (propertyInfo.Name != "SummaryStarTypes" && propertyInfo.Name != "StatusProcess")
                        {
                            paramaters.Add("@" + propertyInfo.Name, propertyInfo.GetValue(data, null));
                        }
                    }
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP2_O_SummaryStar", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return paramaters.Get<Int32>("@RetCode");
                }
            }
            catch (Exception ex)
            { return 0; }
        }

        public void SendMail(int id)
        {
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@Action", "HIENTAI_TONGKETSAO_GETSEND_DETAIL");
                paramaters.Add("@Id", id);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var results = conn.Query<O_SummaryStarSend>("SP2_O_SummaryStar", paramaters, null, true, null, System.Data.CommandType.StoredProcedure).SingleOrDefault();
                //get tiket


                if (results != null)
                {
                    var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
                    var emailTos = new List<string>();
                    emailTos.Add(results.Email);
                    var _mailHelper = new EmailHelper()
                    {
                        EmailTos = emailTos,
                        DisplayName = "Đào Tạo Hiền Tài",
                        Title = "Thông báo kết quả chấm sao",
                        Body = $"<p>Chào <strong>{results.FullName}!</strong></p>"
                        + $"<p>Sao tổng kết của <strong> {results.FullName} </strong> đã được đánh giá</p>"
                        + $"<p>Người đánh giá : <strong>{results.TeacherName}</strong></p>"
                        + $"<p>Kết quả : <strong>{results.StatusName}</strong></p>"
                        + $"<p>Nội dung đánh giá: <strong>{results.Note}</strong></p>"
                        + $"<p>Ngày đánh giá: <strong>{results.CreateDate}</strong></p>"
                        + $"<p>Hãy truy cập vào ứng dụng để xem chi tiết nhé. Cảm ơn !</p>",
                       
                        MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
                    };

                    _mailHelper.SendEmailsThread();
                    
                }
            }
        }

    }
}