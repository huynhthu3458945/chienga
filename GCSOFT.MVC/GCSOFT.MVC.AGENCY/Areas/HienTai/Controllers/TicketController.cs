﻿using AutoMapper;
using Dapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Areas.Agency.Data;
using GCSOFT.MVC.AGENCY.Areas.HienTai.Data;
using GCSOFT.MVC.AGENCY.Helper;
using GCSOFT.MVC.AGENCY.ViewModels.HienTai;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Model.StoreProcedue;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.HienTai.Controllers
{
    public class TicketController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly ITicketService _ticketService;
        private readonly IOptionLineService _optionLineService;
        private readonly IUserService _userService;
        private readonly IUserThuocNhomService _userThuocNhomService;
        private readonly IMailSetupService _mailSetupService;
        private string sysCategory = "AGENCY_TICKET";
        private bool? permission = false;
        private TicketViewModel ticketViewModel;
        private M_Ticket mTicket;
        private string hasValue;
        private string hienTaiConnection = ConfigurationManager.ConnectionStrings["HienTaiConnection"].ConnectionString;
        #endregion

        #region -- Contructor --
        public TicketController
            (
                IVuViecService vuViecService
                , ITicketService ticketService
                , IOptionLineService optionLineService
                , IUserService userService
                , IUserThuocNhomService userThuocNhomService
                , IMailSetupService mailSetupService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _ticketService = ticketService;
            _optionLineService = optionLineService;
            _userService = userService;
            _userThuocNhomService = userThuocNhomService;
            _mailSetupService = mailSetupService;
        }
        #endregion
        public ActionResult Index(int? currPage, string code, int? sendById, string pIC, string statusCode)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            try
            {
                var ticketList = new TicketListPage();
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();

                    int pageString = currPage ?? 0;
                    int _currPage = pageString == 0 ? 1 : pageString;

                    paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "TOTAL.ROW");
                    paramaters.Add("@Id", GetUser().teacherInfo.Id);
                    paramaters.Add("@Code", code ?? string.Empty);
                    paramaters.Add("@SendById", sendById ?? 0);
                    paramaters.Add("@PIC", pIC ?? string.Empty);
                    paramaters.Add("@StatusCode", statusCode);

                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var recordOfPageResults = conn.Query<RecordOfPage>("SP2_O_Ticket", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    int totalRecord = recordOfPageResults.FirstOrDefault().TotalRow;
                    ticketList.paging = new Paging(totalRecord, _currPage);

                    paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "FINDALL");
                    paramaters.Add("@Id", GetUser().teacherInfo.Id);
                    paramaters.Add("@Code", code ?? string.Empty);
                    paramaters.Add("@SendById", sendById ?? 0);
                    paramaters.Add("@PIC", pIC ?? string.Empty);
                    paramaters.Add("@StatusCode", statusCode);
                    paramaters.Add("@Start", ticketList.paging.start);
                    paramaters.Add("@Offset", ticketList.paging.offset);

                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var results = conn.Query<SP_TickKet>("SP2_O_Ticket", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    ticketList.TicketList = results.ToList();
                }

                var userIds = _userThuocNhomService.GetMany("TICKET").Select(d => d.UserID).ToList() ?? new List<int>();
                ticketList.StatusList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("STA_TICKED")).ToSelectListItems(ticketList.StatusCode);
                ticketList.TicketTypes = SqlTicketType().ToSelectListItems(ticketList.TicketType);
                ticketList.PICList = Mapper.Map<IEnumerable<UserVM>>(_userService.FindAll(userIds)).ToList().ToSelectListItems(ticketList.PIC);


                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "PARENT.LIST");
                    paramaters.Add("@AgencyID", GetUser().teacherInfo.AgencyId);
                    paramaters.Add("@Id", GetUser().teacherInfo.Id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var results = conn.Query<AgencyParentList>("SP2_O_Teacher", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);

                    ticketList.SendList = results.ToSelectListItems(sendById ?? 0);
                    if (sendById > 0)
                    {
                        ticketList.SendById = sendById ?? 0;
                    }
                }

                return View(ticketList);
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.ToString();
                return View("Error");
            }
        }
        public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var userIds = _userThuocNhomService.GetMany("TICKET").Select(d => d.UserID).ToList() ?? new List<int>();
            ticketViewModel = new TicketViewModel();
            ticketViewModel.SendById = GetUser().teacherInfo.Id;
            ticketViewModel.SendBy = GetUser().Username;
            ticketViewModel.SendByName = GetUser().teacherInfo.FullName;
            ticketViewModel.SendByEmail = GetUser().teacherInfo.Email;
            ticketViewModel.Code = StringUtil.CheckLetter("T", _ticketService.GetMax(), 4);
            ticketViewModel.DateCreate = DateTime.Now;
            ticketViewModel.DateCreateStr = string.Format("{0:dd/MM/yyyy}", ticketViewModel.DateCreate);
            ticketViewModel.DateModify = DateTime.Now;
            ticketViewModel.DateModifyStr = string.Format("{0:dd/MM/yyyy}", ticketViewModel.DateModify);
            ticketViewModel.StatusCode = "100";
            ticketViewModel.StatusList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("STA_TICKED")).ToSelectListItems(ticketViewModel.StatusCode);
            ticketViewModel.PICList = Mapper.Map<IEnumerable<UserVM>>(_userService.FindAll(userIds)).ToList().ToSelectListItems("");
            ticketViewModel.TicketTypes = SqlTicketType().ToSelectListItems("");

            ViewBag.Role = GetUser().teacherInfo.RoleCode;
             
            ViewBag.IsCreate = true;
            return View(ticketViewModel);
        }
        public ActionResult Edit(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            ticketViewModel = Mapper.Map<TicketViewModel>(_ticketService.GetBy(id));
            if (ticketViewModel == null)
                return HttpNotFound();
            ticketViewModel.DateCreateStr = string.Format("{0:dd/MM/yyyy}", ticketViewModel.DateCreate);
            ticketViewModel.DateModifyStr = string.Format("{0:dd/MM/yyyy}", ticketViewModel.DateModify);
            var userIds = _userThuocNhomService.GetMany("TICKET").Select(d => d.UserID).ToList() ?? new List<int>();
            ticketViewModel.StatusList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("STA_TICKED")).ToSelectListItems(ticketViewModel.StatusCode);
            ticketViewModel.PICList = Mapper.Map<IEnumerable<UserVM>>(_userService.FindAll(userIds)).ToList().ToSelectListItems(ticketViewModel.PIC);
            ticketViewModel.TicketTypes = SqlTicketType().ToSelectListItems(ticketViewModel.TicketType);
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                var paramaters = new DynamicParameters();

                paramaters = new DynamicParameters();
                paramaters.Add("@Action", "TOTAL.GETCOMMENT");
                paramaters.Add("@Id", GetUser().teacherInfo.Id);
                paramaters.Add("@Code", ticketViewModel.Code ?? string.Empty);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                ticketViewModel.TicketCommentList = conn.Query<TicketComment>("SP2_O_Ticket", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
            }
            ViewBag.IsCreate = false;
            ViewBag.Role = GetUser().teacherInfo.RoleCode;
            return View(ticketViewModel);
        }
        public ActionResult Deletes(int id, string code)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                int[] _codes = new int[] { id };
                hasValue = _ticketService.Delete(_codes);
                hasValue = _vuViecService.Commit();

                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    var paramaters = new DynamicParameters();

                    paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "DELETETICKETCOMMENT");
                    paramaters.Add("@Code", code ?? string.Empty);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var res = conn.Query<ApiResult<AgencyParentList>>("SP2_O_Ticket", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }

                if (string.IsNullOrEmpty(hasValue))
                    return Json(new { v = true, count = _codes.Count() });

                return Json("Xóa dữ liệu không thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }
        private void SetData(TicketViewModel _tickedViewModel, bool isCreate)
        {
            ticketViewModel = _tickedViewModel;
            if (isCreate)
            {
                ticketViewModel.DateCreate = DateTime.Now;
                ticketViewModel.SendBy = GetUser().Username;
                ticketViewModel.SendById = GetUser().teacherInfo.Id;
                ticketViewModel.SendByName = GetUser().teacherInfo.FullName;
                ticketViewModel.SendByEmail = GetUser().teacherInfo.Email;
            }

            ticketViewModel.DateModify = DateTime.Now;

            var picInfo = Mapper.Map<UserVM>(_userService.GetBy(ticketViewModel.PIC));
            ticketViewModel.PICName = picInfo.FullName;

            var systemInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("SYSTEM", "200")) ?? new VM_OptionLine();
            ticketViewModel.SystemId = systemInfo.LineNo;
            ticketViewModel.SystemCode = systemInfo.Code;
            ticketViewModel.SystemName = systemInfo.Name;

            var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("STA_TICKED", ticketViewModel.StatusCode)) ?? new VM_OptionLine();
            ticketViewModel.StatusId = statusInfo.LineNo;
            ticketViewModel.StatusName = statusInfo.Name;

            mTicket = Mapper.Map<M_Ticket>(ticketViewModel);
        }
        [HttpPost]
        public ActionResult Create(TicketViewModel _tickedViewModel)
        {
            try
            {
                SetData(_tickedViewModel, true);
                hasValue = _ticketService.Insert(mTicket);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = mTicket.Id, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult Edit(TicketViewModel _tickedViewModel)
        {
            try
            {
                SetData(_tickedViewModel, false);
                hasValue = _ticketService.Update(mTicket);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = mTicket.Id, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public JsonResult SendComment(string content, string code, int id)
        {
            try
            {
                var dateTimeNow = DateTime.Now;
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    var paramaters = new DynamicParameters();

                    paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "TOTAL.SENDCOMMENT");
                    paramaters.Add("@Id", GetUser().teacherInfo.Id);
                    paramaters.Add("@FullName", GetUser().teacherInfo.FullName);
                    paramaters.Add("@Code", code ?? string.Empty);
                    paramaters.Add("@Content", content ?? string.Empty);
                    paramaters.Add("@DateCreate", dateTimeNow);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    conn.Query("SP2_O_Ticket", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    // Send mail cho người đặt ticket
                    SendMail(code, content, GetUser().teacherInfo.FullName, id);
                }
                return Json(new
                {
                    success = true,
                    fullname = GetUser().teacherInfo.FullName,
                    content = content,
                    datacreateStr = $"{dateTimeNow.Year}.{dateTimeNow.Month}.{dateTimeNow.Day} | {dateTimeNow.Hour}:{dateTimeNow.Minute} phút"
                }
                , JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult LoadPIC(string code)
        {
            try
            {
                IEnumerable<SelectListItem> result = TicketTypeByCode(code);
                return Json(new
                {
                    success = true,
                    content = result
                }
                , JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public void SendMail(string code, string content, string fullName, int id)
        {
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@Action", "GETBYTEACHER");
                paramaters.Add("@Code", code);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var results = conn.Query<O_TeacherList>("SP2_O_Ticket", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                //get tiket

                TicketViewModel data = Mapper.Map<TicketViewModel>(_ticketService.GetBy(id));


                if (results.Count() > 0)
                {
                    if (GetUser().teacherInfo.Id != results.FirstOrDefault().Id) // Người tạo ra ticket và ghi chú cùng 1 người thì không cần gửi mail
                    {
                        var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
                        var emailTos = new List<string>();
                        emailTos.Add(results.FirstOrDefault().Email);
                        //emailTos.Add("Huynhthu3458945@gmail.com");
                        var _mailHelper = new EmailHelper()
                        {
                            EmailTos = emailTos,
                            DisplayName = "Đào Tạo Hiền Tài",
                            Title = "Phản Hồi Ticket",
                            Body = $"<p>Chào <strong>{results.FirstOrDefault().FullName}!</strong></p>"
                            + $"<p>Bạn Vừa được nhập 1 phản hồi của Mã Ticket: <strong> {code} </strong></p>"
                            + $"<p>Người tạo Ticket : <strong>{data.SendByName}</strong></p>"
                            + $"<p>Tiêu đề Ticket : <strong>{data.Title}</strong></p>"
                            + $"<p>Nội Dung Ticket : <strong>{data.Question}</strong></p>"
                            + $"<p>Ngày tạo Ticket : <strong>{data.DateCreateStr}</strong></p>"
                            + $"<p>Họ Tên Người Phản Hồi: <strong>{fullName}</strong></p>"
                            + $"<p>Nội Dung Phản Hồi: <strong>{content}</strong></p>"
                            + $"<p>Ngày Phản Hồi: <strong>{data.DateModifyStr}</strong></p>"
                            + $"<p><strong>Đường dẫn: <a href=\"https://agency.stnhd.com/HienTai/Ticket/Edit/{id}\">https://agency.stnhd.com/HienTai/Ticket/Edit/{id}</a><strong></p>",
                            MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
                        };
                        _mailHelper.SendEmailsThread();
                    }
                }
            }
        }

        private IEnumerable<OptionString> SqlTicketType()
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_TICKET_TYPE");
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<OptionString>("SP_TICKET", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                return new List<OptionString>();
            }
        }

        private IEnumerable<SelectListItem> TicketTypeByCode(string code)
        {
            var userIds = _userThuocNhomService.GetMany("TICKET").Select(d => d.UserID).ToList() ?? new List<int>();
            List<string> it = new List<string> { "001", "002", "003" };
            IEnumerable<SelectListItem> result;
            if (it.Contains(code))
            {
                result = Mapper.Map<IEnumerable<UserVM>>(_userService.FindAll(userIds).Where(x => x.Username == "levinhthanh")).ToList().ToSelectListItems("");
            }
            else
            {
                result = Mapper.Map<IEnumerable<UserVM>>(_userService.FindAll(userIds).Where(x => x.Username != "levinhthanh")).ToList().ToSelectListItems("");
            }
            return result;
        }
    }
}