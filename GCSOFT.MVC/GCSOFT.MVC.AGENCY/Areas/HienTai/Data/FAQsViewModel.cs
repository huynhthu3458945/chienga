﻿using GCSOFT.MVC.AGENCY.ViewModels.HienTai;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.HienTai.Data
{
    public class FAQsViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        [AllowHtml]
        public string Question { get; set; }
        [AllowHtml]
        public string Answer { get; set; }
        public string StatusCode { get; set; }
        public IEnumerable<SelectListItem> StatusList { get; set; }
        public bool IsActive { get; set; }
        public int Orders { get; set; }
    }

    public class FAQsListPage
    {
        public string Title { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public Paging paging { get; set; }
        public List<FAQsViewModel> FAQsList { get; set; }
        public string StatusCode { get; set; }
        public IEnumerable<SelectListItem> StatusList { get; set; }
    }
}