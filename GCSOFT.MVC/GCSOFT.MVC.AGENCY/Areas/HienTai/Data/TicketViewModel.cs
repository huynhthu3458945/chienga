﻿using GCSOFT.MVC.AGENCY.ViewModels.HienTai;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.HienTai.Data
{
    public class TicketListPage
    {
        public string Code { get; set; }
        public int SendById { get; set; }
        public string PIC { get; set; }
        public string StatusCode { get; set; }
        public string TicketType { get; set; }
        public IEnumerable<SelectListItem> PICList { get; set; }
        public IEnumerable<SelectListItem> SendList { get; set; }
        public IEnumerable<SelectListItem> StatusList { get; set; }
        public IEnumerable<SelectListItem> TicketTypes { get; set; }
        public Paging paging { get; set; }
        public List<SP_TickKet> TicketList { get; set; }

    }
    public class TicketComment
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Content { get; set; }
        public DateTime DateCreate { get; set; }
        public string DateCreateStr { get { return $"{DateCreate.Year}.{DateCreate.Month}.{DateCreate.Day} | {DateCreate.Hour}:{DateCreate.Minute} phút"; } }
    }
    public class TicketList
    {
        public int Id { get; set; }
        [DisplayName("Tiêu Đề")]
        public string Title { get; set; }
        [DisplayName("Ngày Tạo")]
        public DateTime DateCreate { get; set; }
        public string DateCreateStr { get { return string.Format("{0:dd/MM/yyyy}", DateCreate); } }
        [DisplayName("Người Gửi Yêu Cầu")]
        public string SendByName { get; set; }
        [DisplayName("Người Tiếp Nhận")]
        public string PICName { get; set; }
        [DisplayName("Trạng Thái")]
        public string StatusName { get; set; }
    }
    public class SP_TickKet
    {
        public int Id { get; set; }
        public int TicketId { get; set; }
        public int ParentTeacherId { get; set; }
        public string Code { get; set; }
        [DisplayName("Tiêu Đề")]
        public string Title { get; set; }
        [DisplayName("Ngày Tạo")]
        public DateTime DateCreate { get; set; }
        public string DateCreateStr { get { return string.Format("{0:dd/MM/yyyy}", DateCreate); } }
        [DisplayName("Ngày cập nhật")]
        public DateTime DateModify { get; set; }
        public string DateModifyStr { get { return string.Format("{0:dd/MM/yyyy}", DateModify); } }
        public string SendBy { get; set; }
        [DisplayName("Người Gửi Yêu Cầu")]
        public string SendByName { get; set; }
        public string PIC { get; set; }
        [DisplayName("Người Tiếp Nhận")]
        public string PICName { get; set; }
        [DisplayName("Trạng Thái")]
        public string StatusName { get; set; }
        public string CodeTeacher { get; set; }
        public string TicketType { get; set; }
    }

    public class TicketViewModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Link { get; set; }

        [DisplayName("Tiêu Đề")]
        public string Title { get; set; }
        [DisplayName("Câu Hỏi")]
        [AllowHtml]
        public string Question { get; set; }
        [DisplayName("Câu Trả Lời")]
        [AllowHtml]
        public string Anser { get; set; }

        public DateTime DateCreate { get; set; }
        [DisplayName("Ngày Tạo")]
        public string DateCreateStr { get; set; }

        public DateTime DateModify { get; set; }
        [DisplayName("Ngày Chỉnh Sửa")]
        public string DateModifyStr { get; set; }

        public int SystemId { get; set; }
        [DisplayName("Hệ Thống")]
        public string SystemCode { get; set; }
        public string SystemName { get; set; }

        public string SendByEmail { get; set; }
        public int SendById { get; set; }
        public string SendBy { get; set; }
        [DisplayName("Người Gửi Yêu Cầu")]
        public string SendByName { get; set; }

        [DisplayName("Người Tiếp Nhận")]
        public string PIC { get; set; }
        public string PICName { get; set; }

        public int StatusId { get; set; }
        [DisplayName("Trạng Thái")]
        public string StatusCode { get; set; }
        public string StatusName { get; set; }
        [DisplayName("Nguyên Nhân")]
        public string Reason { get; set; }
        [DisplayName("Khắc Phục")]
        public string Overcome { get; set; }
        [DisplayName("Hướng Xử Lý Tiếp Theo")]
        public string SolutionNet { get; set; }
        public string Comment { get; set; }
        [DisplayName("Loại Ticket")]
        public string TicketType { get; set; }
        public IEnumerable<SelectListItem> SystemList { get; set; }
        public IEnumerable<SelectListItem> PICList { get; set; }
        public IEnumerable<SelectListItem> TicketTypes { get; set; }
        public IEnumerable<SelectListItem> StatusList { get; set; }

        public IEnumerable<TicketComment> TicketCommentList { get; set; }
    }
}