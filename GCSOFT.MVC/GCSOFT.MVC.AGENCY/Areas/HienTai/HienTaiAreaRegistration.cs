﻿using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.HienTai
{
    public class HienTaiAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "HienTai";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "HienTai_default",
                "HienTai/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}