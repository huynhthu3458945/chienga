﻿using AutoMapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Areas.Agency.Data;
using GCSOFT.MVC.AGENCY.Areas.Transaction.Data;
using GCSOFT.MVC.AGENCY.Helper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.StoreProcedure.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Service.Transaction.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.Transaction.Controllers
{
    [CompressResponseAttribute]
    public class AgencyCallBackController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly ISalesHeaderService _salesHdrService;
        private readonly ISalesLineService _salesLineService;
        private readonly IAgencyService _agencyService;
        private readonly IOptionLineService _optionLineService;
        private readonly ICardLineService _cardLineService;
        private readonly ICardEntryService _cardEntryService;
        private readonly IAgencyCardService _agencyAndCardService;
        private readonly IStoreProcedureService _storeProcService;
        private string sysCategory = "AGENCYPCB2";
        private bool? permission = false;
        private SalesOrderCard salesOrderCard;
        private M_SalesHeader mSalesHeader;
        private List<AgencyAndCard> agencyAndCards;
        private List<CardEntry> cardEntries;
        private List<M_AgencyAndCard> mAgencyAndCards;
        private SalesType salesType = SalesType.CardWithdrawal;
        private AgencyType agencyType = AgencyType.Agency;
        private string hasValue;
        #endregion
        public AgencyCallBackController
            (
                IVuViecService vuViecService
                , ISalesHeaderService salesHdrService
                , ISalesLineService salesLineService
                , IAgencyService agencyService
                , IOptionLineService optionLineService
                , ICardLineService cardLineService
                , ICardEntryService cardEntryService
                , IAgencyCardService agencyAndCardService
                , IStoreProcedureService storeProcService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _salesHdrService = salesHdrService;
            _salesLineService = salesLineService;
            _agencyService = agencyService;
            _optionLineService = optionLineService;
            _cardLineService = cardLineService;
            _cardEntryService = cardEntryService;
            _agencyAndCardService = agencyAndCardService;
            _storeProcService = storeProcService;
        }
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var salesOrderList = Mapper.Map<IEnumerable<SalesOrderList>>(_salesHdrService.FindAllAgency(salesType, agencyType, GetUser().agencyInfo.Id));
            return View(salesOrderList);
        }
        public ActionResult Create()
        {
            try
            {
                #region -- Role user --
                permission = GetPermission(sysCategory, Authorities.Add);
                if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
                if (!permission.Value) return View("AccessDenied");
                #endregion
                var userInfo = GetUser();
                salesOrderCard = new SalesOrderCard();
                salesOrderCard.Code = StringUtil.CheckLetter("CB", _salesHdrService.GetMax(salesType), 4);
                salesOrderCard.DocumentDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.DocumentDate);
                salesOrderCard.AgencyId = userInfo.agencyInfo.Id;
                salesOrderCard.AgencyName = userInfo.agencyInfo.FullName;
                salesOrderCard.CustomerList = Mapper.Map<IEnumerable<AgencyList>>(_agencyService.FindAll(agencyType, GetUser().agencyInfo.Id)).ToSelectListItems(0);
                var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("DH", "500"));
                salesOrderCard.StatusId = statusInfo.LineNo;
                salesOrderCard.StatusCode = statusInfo.Code;
                salesOrderCard.StatusName = statusInfo.Name;
                salesOrderCard.CustomerType = agencyType;
                salesOrderCard.LevelList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("LV")).ToSelectListItems("");
                return View(salesOrderCard);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
            
        }
        public ActionResult Edit(string id, int? p)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            salesOrderCard = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(salesType, id));
            if (salesOrderCard == null)
                return HttpNotFound();
            salesOrderCard.DocumentDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.DocumentDate);
            salesOrderCard.PostingDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.PostingDate);
            salesOrderCard.LevelList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("LV")).ToSelectListItems(salesOrderCard.LevelCode);
            salesOrderCard.CustomerList = Mapper.Map<IEnumerable<AgencyList>>(_agencyService.FindAll(agencyType, GetUser().agencyInfo.Id)).ToSelectListItems(salesOrderCard.CustomerId);
            //-- Paging
            int pageString = p ?? 0;
            int page = pageString == 0 ? 1 : pageString;
            var totalRecord = Mapper.Map<IEnumerable<SalesLine>>(_salesLineService.FindAll(salesType, id)).Count();
            salesOrderCard.paging = new Paging(totalRecord, page);
            salesOrderCard.SalesLines = Mapper.Map<IEnumerable<SalesLine>>(_salesLineService.FindAll(salesType, id, salesOrderCard.paging.start, salesOrderCard.paging.offset));
            //-- Paging +
            return View(salesOrderCard);
        }
        [HttpPost]
        public ActionResult Create(SalesOrderCard _salesOrderCard)
        {
            try
            {
                SetData(_salesOrderCard, true);
                _salesHdrService.Insert(mSalesHeader);
                _salesLineService.Insert(mSalesHeader.SalesLines);
                //-- Delete Sub Agency Card
                var agencyCardIds = agencyAndCards.Select(d => d.Id).ToList();
                _agencyAndCardService.Delete(mSalesHeader.CustomerId, agencyCardIds);
                //-- Delete Sub Agency Card +
                _agencyAndCardService.Update(mAgencyAndCards);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = mSalesHeader.Code, msg = "2" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        private void SetData(SalesOrderCard _salesOrderCard, bool isCreate)
        {
            var userInfo = GetUser();
            salesOrderCard = _salesOrderCard;
            if (isCreate)
            {
                salesOrderCard.Code = StringUtil.CheckLetter("CB", _salesHdrService.GetMax(salesType), 4);
                salesOrderCard.CreateBy = userInfo.Username;
                salesOrderCard.DateCreate = DateTime.Now;
                salesOrderCard.LastModifyBy = userInfo.Username;
                salesOrderCard.LastModifyDate = salesOrderCard.DateCreate;
            } else
            {
                salesOrderCard.LastModifyBy = userInfo.Username;
                salesOrderCard.LastModifyDate = salesOrderCard.DateCreate;
            }
            if (!string.IsNullOrEmpty(salesOrderCard.ReceiptDateStr))
                salesOrderCard.ReceiptDate = DateTime.Parse(StringUtil.ConvertStringToDate(salesOrderCard.ReceiptDateStr));
            else
                salesOrderCard.ReceiptDate = null;
            if (!string.IsNullOrEmpty(salesOrderCard.PostingDateStr))
                salesOrderCard.PostingDate = DateTime.Parse(StringUtil.ConvertStringToDate(salesOrderCard.PostingDateStr));
            else
                salesOrderCard.PostingDate = null;

            if (!string.IsNullOrEmpty(salesOrderCard.DocumentDateStr))
                salesOrderCard.DocumentDate = DateTime.Parse(StringUtil.ConvertStringToDate(salesOrderCard.DocumentDateStr));
            else
                salesOrderCard.DocumentDate = null;
            var customerInfo = Mapper.Map<AgencyCard>(_agencyService.GetBy(salesOrderCard.CustomerId));
            salesOrderCard.CustomerName = customerInfo.FullName;
            salesOrderCard.CustomerAddress = customerInfo.Address;
            salesOrderCard.CustomerEmail = customerInfo.Email;
            salesOrderCard.CustomerPhoneNo = customerInfo.Phone;
            salesOrderCard.SalesType = salesType;
            salesOrderCard.CustomerType = agencyType;
            var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("DH", "600"));
            salesOrderCard.StatusId = statusInfo.LineNo;
            salesOrderCard.StatusCode = statusInfo.Code;
            salesOrderCard.StatusName = statusInfo.Name;
            var LevelInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("LV", salesOrderCard.LevelCode)) ?? new VM_OptionLine();
            salesOrderCard.LevelId = LevelInfo.LineNo;
            salesOrderCard.LevelCode = LevelInfo.Code;
            salesOrderCard.LevelName = LevelInfo.Name;
            //-- Lay the moi
            //agencyAndCards = Mapper.Map<IEnumerable<AgencyAndCard>>(_agencyAndCardService.FindAll(salesOrderCard.CustomerId, "100", salesOrderCard.TotalCard)).ToList();
            agencyAndCards = Mapper.Map<IEnumerable<AgencyAndCard>>(_agencyAndCardService.GetNewCards(salesOrderCard.CustomerId, salesOrderCard.LevelCode, "100", salesOrderCard.TotalCard)).ToList();
            statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("CARD", "100")); //Thẻ Moi
            var agencyCards = agencyAndCards;
            agencyCards.ToList().ForEach(item => {
                item.AgencyId = GetUser().agencyInfo.Id;
                item.StatusId = statusInfo.LineNo;
                item.StatusCode = statusInfo.Code;
                item.StatusName = statusInfo.Name;
            });
            mAgencyAndCards = Mapper.Map<List<M_AgencyAndCard>>(agencyCards);
            var salesLine = from c in agencyAndCards
                            select new SalesLine
                            {
                                SalesType = salesType,
                                DocumentCode = salesOrderCard.Code,
                                LineNo = c.Id,
                                LotNumber = c.LotNumber,
                                CardId = c.Id,
                                SerialNumber = c.SerialNumber,
                                Price = c.Price
                            };
            salesOrderCard.SalesLines = salesLine;
            mSalesHeader = Mapper.Map<M_SalesHeader>(salesOrderCard);
        }
        /// <summary>
        /// Lấy Số Lượng Thẻ Mới của Sub Agency
        /// </summary>
        /// <param name="idc"></param>
        /// <returns></returns>
        public JsonResult NoOfCard(int idc, string lvc)
        {
            var statusCodes = new List<string>();
            statusCodes.Add("100");
            var _noOfCards = _agencyAndCardService.TotalCard(idc, statusCodes, lvc);
            return Json(_noOfCards, JsonRequestBehavior.AllowGet);
        }
    }
}