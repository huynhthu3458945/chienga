﻿using AutoMapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Areas.Transaction.Data;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Service.Transaction.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.Transaction.Controllers
{
    [CompressResponseAttribute]
    public class AgencyShipmentController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly ISalesHeaderService _salesHdrService;
        private readonly ISalesLineService _salesLineService;
        private readonly IAgencyService _agencyService;
        private readonly IOptionLineService _optionLineService;
        private readonly ICardLineService _cardLineService;
        private readonly ICardEntryService _cardEntryService;
        private string sysCategory = "AGENCYSHIPMENT";
        private bool? permission = false;
        private SalesOrderCard salesOrderCard;
        private SalesOrderCard salesOrderSource;
        private M_SalesHeader mSalesHeader;
        private SalesType salesType = SalesType.SalesShipment;
        private AgencyType agencyType = AgencyType.Agency;
        private string hasValue;

        #endregion
        public AgencyShipmentController
            (
                IVuViecService vuViecService
                , ISalesHeaderService salesHdrService
                , ISalesLineService salesLineService
                , IAgencyService agencyService
                , IOptionLineService optionLineService
                , ICardLineService cardLineService
                , ICardEntryService cardEntryService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _salesHdrService = salesHdrService;
            _salesLineService = salesLineService;
            _agencyService = agencyService;
            _optionLineService = optionLineService;
            _cardLineService = cardLineService;
            _cardEntryService = cardEntryService;
        }
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var list = Mapper.Map<IEnumerable<SalesOrderList>>(_salesHdrService.FindAllAgency(salesType, agencyType, GetUser().agencyInfo.Id));
            return View(list);
        }
        public ActionResult Details(string id, int? p)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.ViewDetail);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            salesOrderCard = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(salesType, id));
            if (salesOrderCard == null)
                return HttpNotFound();
            salesOrderCard.DocumentDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.DocumentDate);
            salesOrderCard.ReceiptDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.ReceiptDate);
            //-- Paging
            int pageString = p ?? 0;
            int page = pageString == 0 ? 1 : pageString;
            var totalRecord = Mapper.Map<IEnumerable<SalesLine>>(_salesLineService.FindAll(salesType, id)).Count();
            salesOrderCard.paging = new Paging(totalRecord, page);
            salesOrderCard.SalesLines = Mapper.Map<IEnumerable<SalesLine>>(_salesLineService.FindAll(salesType, id, salesOrderCard.paging.start, salesOrderCard.paging.offset));
            //-- Paging +
            return View(salesOrderCard);
        }
    }
}