﻿using AutoMapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Areas.Agency.Data;
using GCSOFT.MVC.AGENCY.Areas.Transaction.Data;
using GCSOFT.MVC.AGENCY.Areas.Transaction.ViewModel;
using GCSOFT.MVC.AGENCY.Helper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Service.Transaction.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.Transaction.Controllers
{
    [CompressResponseAttribute]
    public class CardLocationController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IAgencyCardService _agencyCardService;
        private readonly ICardLineService _cardLineService;
        private readonly ICardEntryService _cardEntryService;
        private readonly ISMSService _smsService;
        private readonly IOptionLineService _optionLineService;
        private readonly IUserInfoService _userInfoService;
        private readonly IClassService _classService;
        private readonly IStudentClassService _studentClassService;
        private readonly ITemplateService _templateService;
        private readonly IMailSetupService _mailSetupService;
        private string sysCategory = "CARDLOCATION";
        private bool? permission = false;
        private AgencyAndCard agencyAndCard;
        private CardLine cardLine;
        private CardEntry cardEntry;//-- Ghi nhận thẻ đã bán
        private string hasValue;
        #endregion

        #region -- Contructor --
        public CardLocationController
            (
                IVuViecService vuViecService
                , IAgencyCardService agencyCardService
                , ICardLineService cardLineService
                , ICardEntryService cardEntryService
                , ISMSService smsService
                , IOptionLineService optionLineService
                , IUserInfoService userInfoService
                , IClassService classService
                , IStudentClassService studentClassService
                , ITemplateService templateService
                , IMailSetupService mailSetupService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _agencyCardService = agencyCardService;
            _cardLineService = cardLineService;
            _cardEntryService = cardEntryService;
            _smsService = smsService;
            _optionLineService = optionLineService;
            _userInfoService = userInfoService;
            _classService = classService;
            _studentClassService = studentClassService;
            _templateService = templateService;
            _mailSetupService = mailSetupService;
        }
        #endregion
        public void DownloadCard(string s)
        {
            var userInfo = GetUser();
            var cards = Mapper.Map<IEnumerable<AgencyAndCard>>(_agencyCardService.FindAll(userInfo.agencyInfo.Id, s, null));
            ExcelPackage Ep = new ExcelPackage();
            ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("Thong Tin");
            Sheet.Cells["A1"].Value = "STT";
            Sheet.Cells["B1"].Value = "Số Seri";
            Sheet.Cells["C1"].Value = "Mã Kích Hoạt";
            Sheet.Cells["D1"].Value = "Giá";
            Sheet.Cells["E1"].Value = "Trạng Thái";
            Sheet.Cells["F1"].Value = "Ngày Cập Nhật";
            int row = 2;
            int stt = 0;
            string _cardNo = "";
            foreach (var item in cards)
            {
                _cardNo = "";
                stt += 1;
                Sheet.Cells[string.Format("A{0}", row)].Value = stt;
                Sheet.Cells[string.Format("B{0}", row)].Value = item.SerialNumber;
                if (!string.IsNullOrEmpty(item.CardNo))
                {
                    _cardNo = CryptorEngine.Decrypt(item.CardNo, true, "TTLSTNHD@CARD");
                }
                Sheet.Cells[string.Format("C{0}", row)].Value = _cardNo;
                Sheet.Cells[string.Format("D{0}", row)].Value = item.Price;
                Sheet.Cells[string.Format("E{0}", row)].Value = item.StatusName;
                Sheet.Cells[string.Format("F{0}", row)].Value = string.Format("{0:dd/MM/yyyy}", item.LastDateUpdate);
                row++;
            }
            Sheet.Cells["A:AZ"].AutoFitColumns();
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment: filename=" + "CardInfo.xlsx");
            Response.BinaryWrite(Ep.GetAsByteArray());
            Response.End();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <param name="s">Status Code</param>
        /// <param name="si">Seri</param>
        /// <returns></returns>
        public ActionResult Index(int? p, string s, string si)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var userInfo = GetUser();
            int pageString = p ?? 0;
            int page = pageString == 0 ? 1 : pageString;

            var statusCodes = new List<string>();
            statusCodes.Add("100");
            statusCodes.Add("200");
            statusCodes.Add("400");
            statusCodes.Add("600");
            statusCodes.Add("450");
            statusCodes.Add("500");

            var totalRecord = Mapper.Map<IEnumerable<AgencyAndCard>>(_agencyCardService.FindAll(userInfo.agencyInfo.Id, s, si)).Count();
            var agencyAndCard = new CardStoreIndex();

            agencyAndCard.StatusCode = s;
            agencyAndCard.StatusList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("CARD", statusCodes)).ToSelectListItems(s ?? "");
            agencyAndCard.Keyword = si;
            agencyAndCard.paging = new Paging(totalRecord, page);
            agencyAndCard.AgencyAndCards = Mapper.Map<IEnumerable<AgencyAndCard>>(_agencyCardService.FindAll(userInfo.agencyInfo.Id, s, si, agencyAndCard.paging.startIndex, agencyAndCard.paging.offset));
            agencyAndCard.AgencyAndCards.ToList().ForEach(item => {
                if (!string.IsNullOrEmpty(item.CardNo))
                    item.CardNo = CryptorEngine.Decrypt(item.CardNo, true, "TTLSTNHD@CARD");
            });
            agencyAndCard.CardTotal = _agencyCardService.TotalCard(userInfo.agencyInfo.Id, statusCodes);
            statusCodes = new List<string>();
            statusCodes.Add("400");
            statusCodes.Add("600");
            statusCodes.Add("500");
            statusCodes.Add("450");
            agencyAndCard.CardSold = _agencyCardService.TotalCard(userInfo.agencyInfo.Id, statusCodes);
            agencyAndCard.CardRemaining = agencyAndCard.CardTotal - agencyAndCard.CardSold;
            statusCodes = new List<string>();
            statusCodes.Add("400");
            agencyAndCard.CardSoldAgency = _agencyCardService.TotalCard(userInfo.agencyInfo.Id, statusCodes);
            statusCodes = new List<string>();
            statusCodes.Add("450");
            agencyAndCard.CardSoldRegistion = _agencyCardService.TotalCard(userInfo.agencyInfo.Id, statusCodes);
            statusCodes = new List<string>();
            statusCodes.Add("500");
            agencyAndCard.CardSoldRegister = _agencyCardService.TotalCard(userInfo.agencyInfo.Id, statusCodes);
            statusCodes = new List<string>();
            statusCodes.Add("600");
            agencyAndCard.CardSoldPartner = _agencyCardService.TotalCard(userInfo.agencyInfo.Id, statusCodes);
            return View(agencyAndCard);
        }
        /// <summary>
        /// Buy card
        /// </summary>
        /// <param name="id">Seri number</param>
        /// <returns></returns>
        public ActionResult ActivationCard(string id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var agencyAndCard = Mapper.Map<AgencyAndCard>(_agencyCardService.GetBy(GetUser().agencyInfo.Id,id));
            return PartialView("_ActivationCard", agencyAndCard);
        }
        /// <summary>
        /// Đăng ký mói 1 user.
        /// </summary>
        /// <param name="id">Seri number</param>
        /// <returns></returns>
        public  ActionResult Register(string id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var userInfo = new UserInfoViewModel()
            {
                Serinumber = id,
                ClassList = Mapper.Map<IEnumerable<ClassList>>(_classService.FindAll(ClassType.Class, "SHOW")).ToList().ToSelectListItems(0)
            };
            return PartialView("_Register", userInfo);
        }
        public JsonResult GetNewSeri()
        {
            var agencyAndCard = Mapper.Map<AgencyAndCard>(_agencyCardService.GetByStatus(GetUser().agencyInfo.Id, "100"));
            return Json(agencyAndCard.SerialNumber, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ActivationCard(AgencyAndCard _agencyAndCard)
        {
            hasValue = CheckSerinumber(_agencyAndCard.SerialNumber, _agencyAndCard.OTP, _agencyAndCard.PhoneNo);
            if (string.IsNullOrEmpty(hasValue))
            {
                var userInfo = GetUser();
                var statusOriginal = Mapper.Map<VM_OptionLine>(_optionLineService.Get("CARD", "450")); //Cập nhật trạng thái đã bán mã kích hoạt
                cardLine = Mapper.Map<CardLine>(_cardLineService.GetBySerialNumber(_agencyAndCard.SerialNumber));
                cardLine.PhoneNo = _agencyAndCard.PhoneNo;
                cardLine.FulName = _agencyAndCard.FulName;
                
                cardLine.LastStatusId = statusOriginal.LineNo;
                cardLine.LastStatusCode = statusOriginal.Code;
                cardLine.LastStatusName = statusOriginal.Name;
                cardLine.LastDateUpdate = DateTime.Now;
                cardLine.DateBuy = DateTime.Now;

                agencyAndCard = Mapper.Map<AgencyAndCard>(_agencyCardService.GetBy(userInfo.agencyInfo.Id, _agencyAndCard.SerialNumber));
                agencyAndCard.UserNameActive = userInfo.Username;
                agencyAndCard.UserActiveFullName = userInfo.FullName;
                agencyAndCard.LastDateUpdate = DateTime.Now;
                
                agencyAndCard.StatusId = statusOriginal.LineNo;
                agencyAndCard.StatusCode = statusOriginal.Code;
                agencyAndCard.StatusName = statusOriginal.Name;

                agencyAndCard.PhoneNo = _agencyAndCard.PhoneNo;
                agencyAndCard.FulName = _agencyAndCard.FulName;
                agencyAndCard.OTP = _agencyAndCard.OTP;
                agencyAndCard.VerifyInfo = _agencyAndCard.VerifyInfo;

                var idCardEntry = _cardEntryService.GetID();
                cardEntry = new CardEntry()
                {
                    //Id = idCardEntry,
                    LotNumber = cardLine.LotNumber,
                    CardLineId = cardLine.Id,
                    SerialNumber = cardLine.SerialNumber,
                    CardNo = cardLine.CardNo,
                    Price = cardLine.Price,
                    UserName = cardLine.UserName,
                    UserFullName = cardLine.UserFullName,
                    DateCreate = cardLine.DateCreate,

                    UserType = UserType.Agency,
                    UserNameActive = userInfo.Username,
                    UserActiveFullName = userInfo.FullName,
                    Date = agencyAndCard.LastDateUpdate,
                    StatusId = agencyAndCard.StatusId,
                    StatusCode = agencyAndCard.StatusCode,
                    StatusName = agencyAndCard.StatusName,
                    Source = CardSource.AgencyBuyActivation,
                    SourceNo = cardLine.SerialNumber,
                    AgencyId = userInfo.agencyInfo.Id,
                    AgencyName = userInfo.agencyInfo.FullName
                };
                hasValue = _cardLineService.Update(Mapper.Map<M_CardLine>(cardLine));
                hasValue = _agencyCardService.Update(Mapper.Map<M_AgencyAndCard>(agencyAndCard));
                hasValue = _cardEntryService.Insert(Mapper.Map<M_CardEntry>(cardEntry));
                hasValue = _vuViecService.Commit();
                if (!string.IsNullOrEmpty(hasValue))
                    hasValue = "Cấp mã kích hoạt bị lỗi, vui lòng liên hệ với Tâm Trí Lực hổ trợ";
            }
            return Json(string.IsNullOrEmpty(hasValue) ? "" : hasValue, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult Register(UserInfoViewModel _userInfo)
        {
            hasValue = CheckUserRegister(_userInfo.Serinumber, _userInfo.OTP, _userInfo.Phone, _userInfo.userName);
            if (string.IsNullOrEmpty(hasValue))
            {
                var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("CARD", "500")); //Cập nhật trạng thái đã kích hoạt
                var userInfo = GetUser();
                cardLine = Mapper.Map<CardLine>(_cardLineService.GetBySerialNumber(_userInfo.Serinumber));
                cardLine.PhoneNo = _userInfo.Phone;
                cardLine.FulName = _userInfo.fullName;
                cardLine.IsActive = true;
                cardLine.LastStatusId = statusInfo.LineNo;
                cardLine.LastStatusCode = statusInfo.Code;
                cardLine.LastStatusName = statusInfo.Name;
                cardLine.LastDateUpdate = DateTime.Now;
                cardLine.ApplyDate = DateTime.Now;
                cardLine.ApplyUser = _userInfo.userName;
                cardLine.DateBuy = DateTime.Now;
                //-- Insert user info
                var randomNumber = new RandomNumberGenerator();
                _userInfo.Password = randomNumber.RandomNumber(100000, 999999).ToString();
                _userInfo.token = CryptorEngine.Encrypt(_userInfo.Email + "_1", true, "stnhdttl");
                _userInfo.ActivePhone = true;
                _userInfo.HasClass = true;
                _userInfo.DateActive = DateTime.Now;
                _userInfo.STNHDType = STNHDType.Active;
                int noOfDuration = 0;
                try { noOfDuration = int.Parse(StringUtil.RebuildPhoneNo(cardLine.DurationCode)); }
                catch { noOfDuration = 0; }
                if (cardLine.DurationCode.EndsWith("Y"))
                    _userInfo.DateExpired = DateTime.Now.AddYears(noOfDuration);
                else if (cardLine.DurationCode.EndsWith("W"))
                {
                    int noOfWeeks = noOfDuration * 7;
                    _userInfo.DateExpired = DateTime.Now.AddDays(noOfWeeks);
                }
                else if (cardLine.DurationCode.EndsWith("M"))
                    _userInfo.DateExpired = DateTime.Now.AddMonths(noOfDuration);
                else if (cardLine.DurationCode.EndsWith("D"))
                    _userInfo.DateExpired = DateTime.Now.AddDays(noOfDuration);
                else
                    _userInfo.DateExpired = null;
                hasValue = _userInfoService.Insert(Mapper.Map<M_UserInfo>(_userInfo));

                agencyAndCard = Mapper.Map<AgencyAndCard>(_agencyCardService.GetBy(userInfo.agencyInfo.Id, _userInfo.Serinumber));
                agencyAndCard.UserNameActive = userInfo.Username;
                agencyAndCard.UserActiveFullName = userInfo.FullName;
                agencyAndCard.LastDateUpdate = DateTime.Now;
                agencyAndCard.StatusId = statusInfo.LineNo;
                agencyAndCard.StatusCode = statusInfo.Code;
                agencyAndCard.StatusName = statusInfo.Name;
                agencyAndCard.PhoneNo = _userInfo.Phone;
                agencyAndCard.FulName = _userInfo.fullName;
                agencyAndCard.OTP = _userInfo.OTP;
                agencyAndCard.VerifyInfo = _userInfo.Email;
                var idCardEntry = _cardEntryService.GetID();
                cardEntry = new CardEntry()
                {
                    //Id = idCardEntry,
                    LotNumber = cardLine.LotNumber,
                    CardLineId = cardLine.Id,
                    SerialNumber = cardLine.SerialNumber,
                    CardNo = cardLine.CardNo,
                    Price = cardLine.Price,
                    UserName = cardLine.UserName,
                    UserFullName = cardLine.UserFullName,
                    DateCreate = cardLine.DateCreate,

                    UserType = UserType.Agency,
                    UserNameActive = userInfo.Username,
                    UserActiveFullName = userInfo.FullName,
                    Date = agencyAndCard.LastDateUpdate,
                    StatusId = agencyAndCard.StatusId,
                    StatusCode = agencyAndCard.StatusCode,
                    StatusName = agencyAndCard.StatusName,
                    Source = CardSource.AgencyBuyActivation,
                    SourceNo = cardLine.SerialNumber,
                    AgencyId = userInfo.agencyInfo.Id,
                    AgencyName = userInfo.agencyInfo.FullName
                };
                var smsTemplate = Mapper.Map<Template>(_templateService.Get("SMS_TT_DANG_NHAP"));
                var smsInfo = new SMSViewModel()
                {
                    Type = SMSType.SMS,
                    PhoneNo = _userInfo.Phone,
                    OTP = null,
                    Content = StringUtil.StripTagsRegexCompiled(smsTemplate.Content).Replace("{{NAME}}", "5 Phut Thuoc Bai").Replace("{{USERNAME}}", _userInfo.userName).Replace("{{MATKHAU}}", _userInfo.Password),
                    DateInput = DateTime.Now,
                    Username = userInfo.Username,
                    FullName = userInfo.FullName,
                    Serinumber = agencyAndCard.SerialNumber
                };
                var smsHelper = new SMSHelper()
                {
                    PhoneNo = smsInfo.PhoneNo,
                    Content = smsInfo.Content
                };
                if (smsHelper.IsMobiphone())
                {
                    smsTemplate = Mapper.Map<Template>(_templateService.Get("MOBI_TKKH_TC"));
                    smsHelper.Content = string.Format(smsTemplate.SMSContent, _userInfo.userName, _userInfo.Password);
                    hasValue = smsHelper.SendMobiphone();
                }
                else
                    hasValue = smsHelper.Send();
                //-- Gửi qua email
                var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
                var mailTemplate = Mapper.Map<Template>(_templateService.Get("EMAIL_REGISTER"));
                var mailTitle = mailTemplate.Title;
                var mailContent = mailTemplate.Content.Replace("{{FULLNAME}}", _userInfo.fullName);
                mailContent = mailContent.Replace("{{USERNAME}}", _userInfo.userName);
                mailContent = mailContent.Replace("{{PASSWORD}}", _userInfo.Password);
                var emailTos = new List<string>();
                emailTos.Add(_userInfo.Email);
                var _mailHelper = new EmailHelper()
                {
                    EmailTos = emailTos,
                    DisplayName = emailInfo.DisplayName,
                    Title = mailTitle,
                    Body = mailContent,
                    MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
                };
                hasValue = _mailHelper.DoSendMail();
                //-- Gửi qua email +
                hasValue = _cardLineService.Update(Mapper.Map<M_CardLine>(cardLine));
                hasValue = _agencyCardService.Update(Mapper.Map<M_AgencyAndCard>(agencyAndCard));
                hasValue = _cardEntryService.Insert(Mapper.Map<M_CardEntry>(cardEntry));
                hasValue = _smsService.Insert(Mapper.Map<M_SMS>(smsInfo));
                hasValue = _vuViecService.Commit();
                if (!string.IsNullOrEmpty(hasValue))
                    hasValue = "Cấp mã kích hoạt bị lỗi, vui lòng liên hệ với Tâm Trí Lực hổ trợ";
            }
            return Json(string.IsNullOrEmpty(hasValue) ? "" : hasValue, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Gửi OTP
        /// </summary>
        /// <param name="p">Phone No.</param>
        /// <param name="s">Seri Number</param>
        /// <returns></returns>
        public JsonResult SendOTP(string p, string s)
        {
            var userInfo = GetUser();
            RandomNumberGenerator randomNumber = new RandomNumberGenerator();
            var otp = randomNumber.RandomNumber(1000, 9999);
            var smsTemplate = Mapper.Map<Template>(_templateService.Get("SMS_OTP"));
            var _content = StringUtil.StripTagsRegexCompiled(smsTemplate.Content.Replace("{{NAME}}", "5 Phut Thuoc Bai"));
            _content = _content.Replace("{{OTP}}", otp.ToString());
            _content = _content.Replace("{{HIEULUC}}", "hieu luc 5 phut");
            _content = _content.Replace("{{NOIDUNG}}", "xac thuc SDT dang ky tai khoan");
            var smsInfo = new SMSViewModel() {
                Type = SMSType.OTP,
                PhoneNo = p,
                OTP = otp.ToString(),
                Content = _content,
                DateInput = DateTime.Now.AddMinutes(5),
                Username = userInfo.Username,
                FullName = userInfo.FullName,
                Serinumber = s
            };
            var smsHelper = new SMSHelper()
            {
                PhoneNo = smsInfo.PhoneNo,
                Content = smsInfo.Content
            };
            hasValue = smsHelper.Send();
            hasValue = _smsService.Insert(Mapper.Map<M_SMS>(smsInfo));
            hasValue = _vuViecService.Commit();
            return Json("", JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Gửi OTP Cho Đăng Ký Thành Viên
        /// </summary>
        /// <param name="p">Phone No.</param>
        /// <param name="s">Seri Number</param>
        /// <returns></returns>
        public JsonResult SendOTPRegister(string p, string s)
        {
            var userInfo = GetUser();
            RandomNumberGenerator randomNumber = new RandomNumberGenerator();
            var otp = randomNumber.RandomNumber(1000, 9999);
            var smsTemplate = Mapper.Map<Template>(_templateService.Get("SMS_OTP"));
            var _content = StringUtil.StripTagsRegexCompiled(smsTemplate.Content.Replace("{{NAME}}", "5 Phut Thuoc Bai"));
            _content = _content.Replace("{{OTP}}", otp.ToString());
            _content = _content.Replace("{{HIEULUC}}", "hieu luc 5 phut");
            _content = _content.Replace("{{NOIDUNG}}", "xac thuc SDT dang ky tai khoan");
            var smsInfo = new SMSViewModel()
            {
                Type = SMSType.OTP,
                PhoneNo = p,
                OTP = otp.ToString(),
                Content = _content,
                DateInput = DateTime.Now.AddMinutes(5),
                Username = userInfo.Username,
                FullName = userInfo.FullName,
                Serinumber = s
            };
            var smsHelper = new SMSHelper()
            {
                PhoneNo = smsInfo.PhoneNo,
                Content = smsInfo.Content
            };
            hasValue = smsHelper.Send();
            hasValue = _smsService.Insert(Mapper.Map<M_SMS>(smsInfo));
            hasValue = _vuViecService.Commit();
            return Json("", JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Kiểm tra thông tin vào có hợp lệ không
        /// </summary>
        /// <param name="s">Seri Number</param>
        /// <param name="o">OTP</param>
        /// <param name="p">Phone No.</param>
        /// <returns></returns>
        public string CheckSerinumber(string s, string o, string p)
        {
            var userInfo = GetUser();
            var cardInfo = Mapper.Map<AgencyAndCard>(_agencyCardService.GetBy(userInfo.agencyInfo.Id, s, "100")); // Kiem tra theo phai la The Moi hay khong ???
            if (string.IsNullOrEmpty(cardInfo.LotNumber))
            {
                cardInfo = Mapper.Map<AgencyAndCard>(_agencyCardService.GetBy(userInfo.agencyInfo.Id, s));
                return string.Format("{0} {1}", s, cardInfo.StatusName);
            }
            return "";
        }
        /// <summary>
        /// Kiểm tra thông tin vào có hợp lệ không
        /// </summary>
        /// <param name="s">Seri Number</param>
        /// <param name="o">OTP</param>
        /// <param name="p">Phone No.</param>
        /// <param name="u">Username</param>
        /// <returns></returns>
        public string CheckUserRegister(string s, string o, string p, string u)
        {
            var userInfo = GetUser();
            var _userName = Mapper.Map<UserInfoViewModel>(_userInfoService.GetBy(u));
            if (!string.IsNullOrEmpty(_userName.userName))
                return "Tên Đăng Nhập Đã Tồn Tại";
            var cardInfo = Mapper.Map<AgencyAndCard>(_agencyCardService.GetBy(userInfo.agencyInfo.Id, s, "100")); // Kiem tra theo phai la The Moi hay khong ???
            if (string.IsNullOrEmpty(cardInfo.LotNumber))
            {
                cardInfo = Mapper.Map<AgencyAndCard>(_agencyCardService.GetBy(userInfo.agencyInfo.Id, s));
                return string.Format("{0} {1}", s, cardInfo.StatusName);
            }
            var userPhones = _userInfoService.NoOfMemberResiger(p) + 1;
            if (userPhones > 5)
            {
                return $"- Số Điện Thoại: {p} đã được đăng ký hơn 5 Tài Khoản, vui lòng chọn số điện thoại khác";
            }
            return "";
            //var otpStatus = _smsService.CheckOTP(p, o, s);
            //var msg = "";
            //if (otpStatus.Equals("Nothing"))
            //    msg = "Mã OTP không tồn tại";
            //if (otpStatus.Equals("expired"))
            //    msg = "Mã OTP đã hết hạn";
            //return msg;
        }
        /// <summary>
        /// Buy card
        /// </summary>
        /// <param name="id">Seri number</param>
        /// <returns></returns>
        public ActionResult DetailsActivationCard(string id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var agencyAndCard = Mapper.Map<AgencyAndCard>(_agencyCardService.GetBy(GetUser().agencyInfo.Id, id));
            agencyAndCard.CardNo = CryptorEngine.Decrypt(agencyAndCard.CardNo, true, "TTLSTNHD@CARD");
            return PartialView("_DetailsActivationCard", agencyAndCard);
        }
        /// <summary>
        /// Buy card
        /// </summary>
        /// <param name="id">Seri number</param>
        /// <returns></returns>
        public ActionResult DetailsCardInfo(string id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var agencyAndCard = Mapper.Map<AgencyAndCard>(_agencyCardService.GetBy(GetUser().agencyInfo.Id, id));
            var userInfo = Mapper.Map<UserInfoViewModel>(_userInfoService.GetByPhone(agencyAndCard.PhoneNo));
            userInfo.Serinumber = agencyAndCard.SerialNumber;
            userInfo.ClassList = Mapper.Map<IEnumerable<ClassList>>(_classService.FindAll(ClassType.Class, "SHOW")).ToList().ToSelectListItems(userInfo.ClassId);
            return PartialView("_DetailsCardInfo", userInfo);
        }
        public JsonResult ResendViaEmailPhoneActivation(string id, string e, string p)
        {
            var agencyAndCard = Mapper.Map<AgencyAndCard>(_agencyCardService.GetBy(GetUser().agencyInfo.Id, id));
            agencyAndCard.VerifyInfo = e;
            hasValue = _agencyCardService.Update(Mapper.Map<M_AgencyAndCard>(agencyAndCard));
            //-- Send Phone
            var smsTemplate = Mapper.Map<Template>(_templateService.Get("SMS_MAKICHHOAT"));
            var smsHelper = new SMSHelper()
            {
                PhoneNo = agencyAndCard.PhoneNo,
                Content = StringUtil.StripTagsRegexCompiled(smsTemplate.Content).Replace("{{NAME}}", "vao 5 Phut Thuoc Bai").Replace("{{MAKICHHOAT}}", CryptorEngine.Decrypt(agencyAndCard.CardNo, true, "TTLSTNHD@CARD")).Replace("{{LINK}}", "https://sieutrinhohocduong.com/student")
            };
            if (smsHelper.IsMobiphone())
            {
                smsTemplate = Mapper.Map<Template>(_templateService.Get("MOBI_MAKICHHOAT"));
                smsHelper.Content = string.Format(smsTemplate.SMSContent, "5 Phut Thuoc Bai", CryptorEngine.Decrypt(agencyAndCard.CardNo, true, "TTLSTNHD@CARD"));
                hasValue = smsHelper.SendMobiphone();
            }
            else
                hasValue = smsHelper.Send();
            //-- Send Phone +
            //-- Gửi qua email
            var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
            var mailTemplate = Mapper.Map<Template>(_templateService.Get("EMAILACTIVECODE"));
            var mailTitle = mailTemplate.Title;
            var mailContent = mailTemplate.Content.Replace("{{FULLNAME}}", agencyAndCard.FulName);
            mailContent = mailContent.Replace("{{ACTIVECODE}}", CryptorEngine.Decrypt(agencyAndCard.CardNo, true, "TTLSTNHD@CARD"));
            var emailTos = new List<string>();
            emailTos.Add(e);
            var _mailHelper = new EmailHelper()
            {
                EMailFrom = emailInfo.Email,
                EmailTos = emailTos,
                DisplayName = emailInfo.DisplayName,
                Title = mailTitle,
                Body = mailContent,
                MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
            };
            hasValue = _mailHelper.DoSendMail();
            //-- Gửi qua email +
            return Json("", JsonRequestBehavior.AllowGet);
        }
        public JsonResult ResendViaEmailPhoneUserInfo(string id)
        {
            var agencyAndCard = Mapper.Map<AgencyAndCard>(_agencyCardService.GetBy(GetUser().agencyInfo.Id, id));
            var userInfo = Mapper.Map<UserInfoViewModel>(_userInfoService.GetByPhone(agencyAndCard.PhoneNo));
            var smsTemplate = Mapper.Map<Template>(_templateService.Get("SMS_TT_DANG_NHAP"));
            var smsHelper = new SMSHelper()
            {
                PhoneNo = userInfo.Phone,
                Content = StringUtil.StripTagsRegexCompiled(smsTemplate.Content).Replace("{{NAME}}", "5 Phut Thuoc Bai").Replace("{{USERNAME}}", userInfo.Phone).Replace("{{MATKHAU}}", userInfo.Password)
            };
            if (smsHelper.IsMobiphone())
            {
                smsTemplate = Mapper.Map<Template>(_templateService.Get("MOBI_TKKH_TC"));
                smsHelper.Content = string.Format(smsTemplate.SMSContent, userInfo.userName, userInfo.Password);
                hasValue = smsHelper.SendMobiphone();
            }
            else
                hasValue = smsHelper.Send();
            return Json("", JsonRequestBehavior.AllowGet);
        }
        private StringBuilder GetBuildCardNo()
        {
            var builder = new StringBuilder();
            var randomNumber = new RandomNumberGenerator();
            builder.Append(randomNumber.RandomString(4, true).ToUpper());
            builder.Append(randomNumber.RandomNumber(1000, 9999));
            builder.Append(randomNumber.RandomString(2, false).ToUpper());

            return builder;
        }
    }
}