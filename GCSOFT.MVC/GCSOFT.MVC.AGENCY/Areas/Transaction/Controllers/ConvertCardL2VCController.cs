﻿using AutoMapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Areas.Agency.Data;
using GCSOFT.MVC.AGENCY.Areas.Transaction.Data;
using GCSOFT.MVC.AGENCY.Helper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.ApprovalService.Interface;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.StoreProcedure.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Service.Transaction.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.Transaction.Controllers
{
    [CompressResponseAttribute]
    public class ConvertCardL2VCController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly ISalesHeaderService _salesHdrService;
        private readonly ISalesLineService _salesLineService;
        private readonly IAgencyService _agencyService;
        private readonly IOptionLineService _optionLineService;
        private readonly ICardLineService _cardLineService;
        private readonly ICardEntryService _cardEntryService;
        private readonly IUserThuocNhomService _userThuocNhomService;
        private readonly IUserService _userService;
        private readonly IFlowService _flowService;
        private readonly IUserGroupService _userGroupService;
        private readonly ITemplateService _templateService;
        private readonly IMailSetupService _mailSetupService;
        private readonly IStoreProcedureService _storeProcedureService;
        private readonly IAgencyCardService _agencyAndCardService;
        private string sysCategory = "AGEN_CONVERT_L2VC";
        private bool? permission = false;
        private SalesOrderCard salesOrderCard;
        private M_SalesHeader mSalesHeader;
        private SalesType salesType = SalesType.Convert;
        private ConvertType convertType = ConvertType.L2VC;
        private string hasValue;
        #endregion
        public ConvertCardL2VCController
            (
                IVuViecService vuViecService
                , ISalesHeaderService salesHdrService
                , ISalesLineService salesLineService
                , IAgencyService agencyService
                , IOptionLineService optionLineService
                , ICardLineService cardLineService
                , ICardEntryService cardEntryService
                , IUserThuocNhomService userThuocNhomService
                , IUserService userService
                , IFlowService flowService
                , IUserGroupService userGroupService
                , ITemplateService templateService
                , IMailSetupService mailSetupService
                , IStoreProcedureService storeProcedureService
                , IAgencyCardService agencyAndCardService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _salesHdrService = salesHdrService;
            _salesLineService = salesLineService;
            _agencyService = agencyService;
            _optionLineService = optionLineService;
            _cardLineService = cardLineService;
            _cardEntryService = cardEntryService;
            _userThuocNhomService = userThuocNhomService;
            _userService = userService;
            _flowService = flowService;
            _userGroupService = userGroupService;
            _templateService = templateService;
            _mailSetupService = mailSetupService;
            _storeProcedureService = storeProcedureService;
            _agencyAndCardService = agencyAndCardService;
        }
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var salesOrderList = Mapper.Map<IEnumerable<SalesOrderList>>(_salesHdrService.FindAllCustomer(salesType, GetUser().agencyInfo.Id, convertType));
            return View(salesOrderList);
        }
        public ActionResult Create()
        {
            try
            {
                #region -- Role user --
                permission = GetPermission(sysCategory, Authorities.Add);
                if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
                if (!permission.Value) return View("AccessDenied");
                #endregion
                var agencyInfo = GetUser().agencyInfo;
                salesOrderCard = new SalesOrderCard();
                salesOrderCard.Code = StringUtil.CheckLetter("CV", _salesHdrService.GetMax(salesType), 4);
                salesOrderCard.DocumentDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.DocumentDate);
                salesOrderCard.CustomerId = agencyInfo.Id;
                salesOrderCard.CustomerName = agencyInfo.FullName;
                var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("APPSTATUS", "100"));
                salesOrderCard.StatusId = statusInfo.LineNo;
                salesOrderCard.StatusCode = statusInfo.Code;
                salesOrderCard.StatusName = statusInfo.Name;
                // Loại trừ VIP, Lớp 1 tiếng việt
                var listLV = new string[] { "100", "400" };
                var cardtypes = _optionLineService.FindAll("LV").Where(d => !listLV.Contains(d.Code));
                salesOrderCard.FromLevelList = Mapper.Map<IEnumerable<VM_OptionLine>>(cardtypes).ToSelectListItems("");
                return View(salesOrderCard);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }
        public ActionResult Edit(string id, int? p)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var agencyInfo = GetUser().agencyInfo;
            salesOrderCard = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(salesType, id));
            if (salesOrderCard == null)
                return HttpNotFound();
            salesOrderCard.DocumentDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.DocumentDate);
            salesOrderCard.PostingDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.PostingDate);
            // Loại trừ VIP, Lớp 1 tiếng việt
            var listLV = new string[] { "100", "400" };
            var cardtypes = _optionLineService.FindAll("LV").Where(d => !listLV.Contains(d.Code));
            salesOrderCard.FromLevelList = Mapper.Map<IEnumerable<VM_OptionLine>>(cardtypes).ToSelectListItems(salesOrderCard.FromLevelCode);
            return View(salesOrderCard);
        }
        [HttpPost]
        public ActionResult Create(SalesOrderCard _salesOrderCard)
        {
            try
            {
                SetData(_salesOrderCard, true);
                _salesHdrService.Insert(mSalesHeader);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = mSalesHeader.Code, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult Edit(SalesOrderCard _salesOrderCard)
        {
            try
            {
                SetData(_salesOrderCard, false);
                hasValue = _salesHdrService.Update(mSalesHeader);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = mSalesHeader.Code, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        private void SetData(SalesOrderCard _salesOrderCard, bool isCreate)
        {
            var userInfo = GetUser();
            salesOrderCard = _salesOrderCard;
            if (isCreate)
            {
                salesOrderCard.Code = StringUtil.CheckLetter("CV", _salesHdrService.GetMax(salesType), 4);
                salesOrderCard.CreateBy = userInfo.Username;
                salesOrderCard.DateCreate = DateTime.Now;
            }
            else
            {
                salesOrderCard.LastModifyBy = userInfo.Username;
                salesOrderCard.LastModifyDate = _salesOrderCard.DateCreate;
            }
            salesOrderCard.ConvertType = convertType;
            salesOrderCard.LastModifyBy = userInfo.Username;
            salesOrderCard.LastModifyDate = DateTime.Now;
            if (!string.IsNullOrEmpty(_salesOrderCard.ReceiptDateStr))
                salesOrderCard.ReceiptDate = DateTime.Parse(StringUtil.ConvertStringToDate(_salesOrderCard.ReceiptDateStr));
            else
                salesOrderCard.ReceiptDate = null;
            if (!string.IsNullOrEmpty(_salesOrderCard.PostingDateStr))
                salesOrderCard.PostingDate = DateTime.Parse(StringUtil.ConvertStringToDate(_salesOrderCard.PostingDateStr));
            else
                salesOrderCard.PostingDate = null;

            if (!string.IsNullOrEmpty(_salesOrderCard.DocumentDateStr))
                salesOrderCard.DocumentDate = DateTime.Parse(StringUtil.ConvertStringToDate(_salesOrderCard.DocumentDateStr));
            else
                salesOrderCard.DocumentDate = null;
            var customerInfo = Mapper.Map<AgencyCard>(_agencyService.GetBy(salesOrderCard.CustomerId));
            salesOrderCard.CustomerName = customerInfo.FullName;
            salesOrderCard.CustomerAddress = customerInfo.Address;
            salesOrderCard.CustomerEmail = customerInfo.Email;
            salesOrderCard.CustomerPhoneNo = customerInfo.Phone;
            salesOrderCard.SalesType = salesType;
            var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("ORDERSTATUS", "700"));
            salesOrderCard.StatusId = statusInfo.LineNo;
            salesOrderCard.StatusCode = statusInfo.Code;
            salesOrderCard.StatusName = statusInfo.Name;
            salesOrderCard.ToLevelCode = "100";
            mSalesHeader = Mapper.Map<M_SalesHeader>(salesOrderCard);
        }
        /// <summary>
        /// Lấy Số Lượng Thẻ Mới của Sub Agency
        /// </summary>
        /// <param name="idc"></param>
        /// <param name="c">CardType</param>
        /// <returns></returns>
        public JsonResult NoOfCard(int idc, string c)
        {
            var statusCodes = new List<string>();
            statusCodes.Add("100");
            var _noOfCards = _agencyAndCardService.TotalCard(idc, statusCodes, c);
            return Json(_noOfCards, JsonRequestBehavior.AllowGet);
        }
    }
}