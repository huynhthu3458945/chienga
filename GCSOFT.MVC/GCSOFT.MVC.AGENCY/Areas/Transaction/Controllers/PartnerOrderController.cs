﻿using AutoMapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Areas.Agency.Data;
using GCSOFT.MVC.AGENCY.Areas.Transaction.Data;
using GCSOFT.MVC.AGENCY.Helper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.StoreProcedure.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Service.Transaction.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.Transaction.Controllers
{
    [CompressResponseAttribute]
    public class PartnerOrderController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly ISalesHeaderService _salesHdrService;
        private readonly ISalesLineService _salesLineService;
        private readonly IAgencyService _agencyService;
        private readonly IOptionLineService _optionLineService;
        private readonly ICardLineService _cardLineService;
        private readonly ICardEntryService _cardEntryService;
        private readonly IAgencyCardService _agencyAndCardService;
        private readonly IStoreProcedureService _storeProcService;
        private string sysCategory = "AGENCYPARTNERORDER";
        private bool? permission = false;
        private SalesOrderCard salesOrderCard;
        private M_SalesHeader mSalesHeader;
        private List<AgencyAndCard> agencyAndCards;
        private CardLine cardLine = new CardLine();
        private List<CardLine> cardLines = new List<CardLine>();
        private SalesType salesType = SalesType.SalesOrder;
        private AgencyType agencyType = AgencyType.Partner;
        private string hasValue;
        #endregion
        public PartnerOrderController
            (
                IVuViecService vuViecService
                , ISalesHeaderService salesHdrService
                , ISalesLineService salesLineService
                , IAgencyService agencyService
                , IOptionLineService optionLineService
                , ICardLineService cardLineService
                , ICardEntryService cardEntryService
                , IAgencyCardService agencyAndCardService
                , IStoreProcedureService storeProcService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _salesHdrService = salesHdrService;
            _salesLineService = salesLineService;
            _agencyService = agencyService;
            _optionLineService = optionLineService;
            _cardLineService = cardLineService;
            _cardEntryService = cardEntryService;
            _agencyAndCardService = agencyAndCardService;
            _storeProcService = storeProcService;
        }
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var salesOrderList = Mapper.Map<IEnumerable<SalesOrderList>>(_salesHdrService.FindAllAgency(salesType, agencyType, GetUser().agencyInfo.Id));
            return View(salesOrderList);
        }
        public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var userInfo = GetUser();
            salesOrderCard = new SalesOrderCard();
            salesOrderCard.Code = StringUtil.CheckLetter("SO", _salesHdrService.GetMax(salesType), 4);
            salesOrderCard.DocumentDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.DocumentDate);
            salesOrderCard.AgencyId = userInfo.agencyInfo.Id;
            salesOrderCard.AgencyName = userInfo.agencyInfo.FullName;
            salesOrderCard.CustomerList = Mapper.Map<IEnumerable<AgencyList>>(_agencyService.FindAll(agencyType, GetUser().agencyInfo.Id)).ToSelectListItems(0);
            var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("DH", "100"));
            salesOrderCard.StatusId = statusInfo.LineNo;
            salesOrderCard.StatusCode = statusInfo.Code;
            salesOrderCard.StatusName = statusInfo.Name;
            salesOrderCard.CustomerType = agencyType;
            salesOrderCard.LevelList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("LV")).ToSelectListItems("");
            ViewBag.IsReturnOrder = "disabled";
            return View(salesOrderCard);
        }
        public ActionResult Edit(string id, int? p)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            salesOrderCard = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(salesType, id));
            if (salesOrderCard == null)
                return HttpNotFound();
            salesOrderCard.DocumentDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.DocumentDate);
            salesOrderCard.PostingDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.PostingDate);
            salesOrderCard.LevelList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("LV")).ToSelectListItems(salesOrderCard.LevelCode);
            salesOrderCard.CustomerList = Mapper.Map<IEnumerable<AgencyList>>(_agencyService.FindAll(agencyType, GetUser().agencyInfo.Id)).ToSelectListItems(salesOrderCard.CustomerId);
            //-- Paging
            int pageString = p ?? 0;
            int page = pageString == 0 ? 1 : pageString;
            var totalRecord = Mapper.Map<IEnumerable<SalesLine>>(_salesLineService.FindAll(salesType, id)).Count();
            salesOrderCard.paging = new Paging(totalRecord, page);
            salesOrderCard.SalesLines = Mapper.Map<IEnumerable<SalesLine>>(_salesLineService.FindAll(salesType, id, salesOrderCard.paging.start, salesOrderCard.paging.offset));
            //-- Paging +
            var salesShipmentInfo = Mapper.Map<SalesOrderCard>(_salesHdrService.GetByFromSource(SalesType.SalesOrder, id));
            var statusCodes = new List<string>();
            statusCodes.Add("100");
            statusCodes.Add("200");
            var disabled = statusCodes.Contains(salesOrderCard.StatusCode) ? "" : "disabled";
            if (disabled.Equals(""))
                disabled = (salesShipmentInfo.StatusCode ?? "").Equals("300") ? "disabled" : "";
            ViewBag.IsReturnOrder = disabled;
            return View(salesOrderCard);
        }
        [HttpPost]
        public ActionResult Create(SalesOrderCard _salesOrderCard)
        {
            try
            {
                SetData(_salesOrderCard, true);
                _salesHdrService.Insert(mSalesHeader);
                 _salesLineService.Insert(mSalesHeader.SalesLines);
                 _cardLineService.Update(Mapper.Map<IEnumerable<M_CardLine>>(cardLines));
                 _agencyAndCardService.Update(Mapper.Map<IEnumerable<M_AgencyAndCard>>(agencyAndCards));
                 _vuViecService.Commit();
                 TransferCardToAgency(mSalesHeader.Code);
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = mSalesHeader.SourceNo, msg = "2" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        private void SetData(SalesOrderCard _salesOrderCard, bool isCreate)
        {
            var userInfo = GetUser();
            salesOrderCard = _salesOrderCard;
            if (isCreate)
            {
                salesOrderCard.Code = StringUtil.CheckLetter("SO", _salesHdrService.GetMax(salesType), 4);
                salesOrderCard.CreateBy = userInfo.Username;
                salesOrderCard.DateCreate = DateTime.Now;
                salesOrderCard.LastModifyBy = userInfo.Username;
                salesOrderCard.LastModifyDate = salesOrderCard.DateCreate;
            }
            else
            {
                salesOrderCard.LastModifyBy = userInfo.Username;
                salesOrderCard.LastModifyDate = salesOrderCard.DateCreate;
            }
            if (!string.IsNullOrEmpty(salesOrderCard.ReceiptDateStr))
                salesOrderCard.ReceiptDate = DateTime.Parse(StringUtil.ConvertStringToDate(salesOrderCard.ReceiptDateStr));
            else
                salesOrderCard.ReceiptDate = null;
            if (!string.IsNullOrEmpty(salesOrderCard.PostingDateStr))
                salesOrderCard.PostingDate = DateTime.Parse(StringUtil.ConvertStringToDate(salesOrderCard.PostingDateStr));
            else
                salesOrderCard.PostingDate = null;

            if (!string.IsNullOrEmpty(salesOrderCard.DocumentDateStr))
                salesOrderCard.DocumentDate = DateTime.Parse(StringUtil.ConvertStringToDate(salesOrderCard.DocumentDateStr));
            else
                salesOrderCard.DocumentDate = null;
            var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("DH", "200")); //Đã Giao Hàng
            salesOrderCard.StatusId = statusInfo.LineNo;
            salesOrderCard.StatusCode = statusInfo.Code;
            salesOrderCard.StatusName = statusInfo.Name;
            salesOrderCard.PostingDate = DateTime.Now;

            var customerInfo = Mapper.Map<AgencyCard>(_agencyService.GetBy(salesOrderCard.CustomerId));
            salesOrderCard.CustomerName = customerInfo.FullName;
            salesOrderCard.CustomerAddress = customerInfo.Address;
            salesOrderCard.CustomerEmail = customerInfo.Email;
            salesOrderCard.CustomerPhoneNo = customerInfo.Phone;
            salesOrderCard.SalesType = salesType;
            salesOrderCard.CustomerType = agencyType;
            var LevelInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("LV", salesOrderCard.LevelCode)) ?? new VM_OptionLine();
            salesOrderCard.LevelId = LevelInfo.LineNo;
            salesOrderCard.LevelCode = LevelInfo.Code;
            salesOrderCard.LevelName = LevelInfo.Name;
            //-- Lay the moi
            agencyAndCards = Mapper.Map<IEnumerable<AgencyAndCard>>(_agencyAndCardService.GetNewCards(GetUser().agencyInfo.Id, salesOrderCard.LevelCode, "100", salesOrderCard.TotalCard)).ToList();
            var salesLine = from c in agencyAndCards
                            select new SalesLine
                            {
                                SalesType = salesType,
                                DocumentCode = salesOrderCard.Code,
                                LineNo = c.Id,
                                LotNumber = c.LotNumber,
                                CardId = c.Id,
                                SerialNumber = c.SerialNumber,
                                Price = salesOrderCard.Price ?? c.Price
                            };
            salesOrderCard.SalesLines = salesLine;
            //-- Lay the moi +
            //-- Update status the
            cardLines = new List<CardLine>();
            statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("CARD", "200")); //Thẻ Chờ Xác Nhận
            agencyAndCards.ToList().ForEach(item =>
            {
                item.StatusId = statusInfo.LineNo;
                item.StatusCode = statusInfo.Code;
                item.StatusName = statusInfo.Name;
                item.UserType = UserType.Agency;
                item.UserNameActive = GetUser().Username;
                item.UserActiveFullName = GetUser().FullName;
                item.LastDateUpdate = DateTime.Now;

                cardLine = Mapper.Map<CardLine>(_cardLineService.GetBySerialNumber(item.SerialNumber));
                cardLine.LastStatusId = statusInfo.LineNo;
                cardLine.LastStatusCode = statusInfo.Code;
                cardLine.LastStatusName = statusInfo.Name;
                cardLines.Add(cardLine);
            });
            mSalesHeader = Mapper.Map<M_SalesHeader>(salesOrderCard);
        }
        private string TransferCardToAgency(string code)
        {
            try
            {
                salesOrderCard = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(SalesType.SalesOrder, code));
                salesOrderCard.Code = StringUtil.CheckLetter("SM", _salesHdrService.GetMax(SalesType.SalesShipment), 4);
                salesOrderCard.DocumentDate = DateTime.Now;
                var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("DH", "250"));
                salesOrderCard.StatusId = statusInfo.LineNo;
                salesOrderCard.StatusCode = statusInfo.Code;
                salesOrderCard.StatusName = statusInfo.Name;
                salesOrderCard.SalesType = SalesType.SalesShipment;
                salesOrderCard.SourceNo = code;
                salesOrderCard.FromSoure = SalesType.SalesOrder;

                var userInfo = GetUser();
                statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("CARD", "200")); //Thẻ Chờ Xác Nhận.
                var _salesLines = Mapper.Map<IEnumerable<SalesLine>>(_salesLineService.FindAll(SalesType.SalesOrder, code));
                _salesLines.ToList().ForEach(item => {
                    item.SalesType = SalesType.SalesShipment;
                    item.DocumentCode = salesOrderCard.Code;
                });
                salesOrderCard.SalesLines = _salesLines;
                mSalesHeader = Mapper.Map<M_SalesHeader>(salesOrderCard);
                hasValue = _salesHdrService.Insert(mSalesHeader);
                hasValue = _salesLineService.Insert(mSalesHeader.SalesLines);
                hasValue = _vuViecService.Commit();
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        /// <summary>
        /// Lấy Số Lượng Thẻ Còn Lại
        /// </summary>
        /// <param name="ai">Agency Id</param>
        /// <param name="lvc">level Code</param>
        /// <returns></returns>
        public JsonResult NoOfCard(int ai, string lvc)
        {
            var statusCodes = new List<string>();
            statusCodes.Add("100");
            var _noOfCards = _agencyAndCardService.TotalCard(ai, statusCodes, lvc);
            return Json(_noOfCards, JsonRequestBehavior.AllowGet);
        }
    }
}