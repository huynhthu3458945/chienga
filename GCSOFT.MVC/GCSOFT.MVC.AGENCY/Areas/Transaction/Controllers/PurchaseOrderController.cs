﻿// #################################################################
// # Copyright (C) 2021-2022, Tâm Trí Lực JSC.  All Rights Reserved.                       
// #
// # History：                                                                        
// #	Date Time	    Updated		    Content                	
// #    25/08/2021	    Huỳnh Thử 		Update
// ##################################################################

using AutoMapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Areas.Agency.Data;
using GCSOFT.MVC.AGENCY.Areas.Transaction.Data;
using GCSOFT.MVC.AGENCY.Areas.Transaction.ViewModel;
using GCSOFT.MVC.AGENCY.Helper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.ApprovalService.Interface;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Service.Transaction.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.Transaction.Controllers
{
    [CompressResponseAttribute]
    public class PurchaseOrderController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly ISalesHeaderService _salesHdrService;
        private readonly ISalesLineService _salesLineService;
        private readonly IAgencyService _agencyService;
        private readonly IOptionLineService _optionLineService;
        private readonly ICardLineService _cardLineService;
        private readonly ICardEntryService _cardEntryService;
        private readonly IUserThuocNhomService _userThuocNhomService;
        private readonly IUserService _userService;
        private readonly IFlowService _flowService;
        private readonly IUserGroupService _userGroupService;
        private readonly ITemplateService _templateService;
        private readonly IMailSetupService _mailSetupService;
        private readonly IPromotionService _promotionService;
        private string sysCategory = "AGENCYPO";
        private bool? permission = false;
        private SalesOrderCard salesOrderCard;
        private M_SalesHeader mSalesHeader;
        private SalesType salesType = SalesType.PurchaseOrder;
        private string hasValue;
        #endregion
        public PurchaseOrderController
            (
                IVuViecService vuViecService
                , ISalesHeaderService salesHdrService
                , ISalesLineService salesLineService
                , IAgencyService agencyService
                , IOptionLineService optionLineService
                , ICardLineService cardLineService
                , ICardEntryService cardEntryService
                , IUserThuocNhomService userThuocNhomService
                , IUserService userService
                , IFlowService flowService
                , IUserGroupService userGroupService
                , ITemplateService templateService
                , IMailSetupService mailSetupService
                , IPromotionService promotionService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _salesHdrService = salesHdrService;
            _salesLineService = salesLineService;
            _agencyService = agencyService;
            _optionLineService = optionLineService;
            _cardLineService = cardLineService;
            _cardEntryService = cardEntryService;
            _userThuocNhomService = userThuocNhomService;
            _userService = userService;
            _flowService = flowService;
            _userGroupService = userGroupService;
            _templateService = templateService;
            _mailSetupService = mailSetupService;
            _promotionService = promotionService;
        }
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var salesOrderList = Mapper.Map<IEnumerable<SalesOrderList>>(_salesHdrService.FindAllCustomer(salesType, GetUser().agencyInfo.Id));
            return View(salesOrderList);
        }
        /// <summary>
        ///     Create
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Update [25/08/2021]
        /// </history>
        public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var agencyInfo = GetUser().agencyInfo;
            salesOrderCard = new SalesOrderCard();
            salesOrderCard.Code = StringUtil.CheckLetter("PO", _salesHdrService.GetMax(salesType), 4);
            salesOrderCard.DocumentDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.DocumentDate);
            salesOrderCard.CustomerId = agencyInfo.Id;
            salesOrderCard.CustomerName = agencyInfo.FullName;
            var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("ORDERSTATUS", "100"));
            salesOrderCard.LevelList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("LV")).ToSelectListItems("");
            var promotionList = _promotionService.GetAll();
            salesOrderCard.PromotionList = Mapper.Map<IEnumerable<VM_Promotion>>(promotionList.Where(z=>z.FromDate.Value.Date <= DateTime.Now.Date && DateTime.Now.Date <= z.ToDate.Value.Date)).ToSelectListItems(string.Empty);
            salesOrderCard.StatusId = statusInfo.LineNo;
            salesOrderCard.StatusCode = statusInfo.Code;
            salesOrderCard.StatusName = statusInfo.Name;
            ViewBag.IsAllow = "disabled";
            return View(salesOrderCard);
        }

        /// <summary>
        ///     Edit
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Update [25/08/2021]
        /// </history>
        public ActionResult Edit(string id, int? p)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            salesOrderCard = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(salesType, id));
            if (salesOrderCard == null)
                return HttpNotFound();
            salesOrderCard.DocumentDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.DocumentDate);
            salesOrderCard.PostingDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.PostingDate);
            salesOrderCard.LevelList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("LV")).ToSelectListItems(salesOrderCard.LevelCode);
            var promotionList = _promotionService.GetAll();
            salesOrderCard.PromotionList = Mapper.Map<IEnumerable<VM_Promotion>>(promotionList).ToSelectListItems(salesOrderCard.PromotionCode);
            ViewBag.IsAllow = salesOrderCard.StatusCode.Equals("100") ? "" : "disabled";
            return View(salesOrderCard);
        }
        [HttpPost]
        public ActionResult Create(SalesOrderCard _salesOrderCard)
        {
            try
            {
                SetData(_salesOrderCard, true);
                hasValue = _salesHdrService.Insert(mSalesHeader);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = mSalesHeader.Code, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult Edit(SalesOrderCard _salesOrderCard)
        {
            try
            {
                SetData(_salesOrderCard, false);
                _salesHdrService.Update(mSalesHeader);
                _flowService.Insert(mSalesHeader.Flows);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = mSalesHeader.Code, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public ActionResult Delete(string id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                hasValue = _salesHdrService.Delete(salesType, id);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return Json(new { v = true, count = 1 });
                return Json("Xóa dữ liệu không thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }
        public JsonResult ChangeStatus(string c)
        {
            try
            {
                salesOrderCard = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(salesType, c));
                var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("ORDERSTATUS", "700"));
                salesOrderCard.StatusId = statusInfo.LineNo;
                salesOrderCard.StatusCode = statusInfo.Code;
                salesOrderCard.StatusName = statusInfo.Name;
                hasValue = _salesHdrService.Update(Mapper.Map<M_SalesHeader>(salesOrderCard));
                hasValue = _vuViecService.Commit();
                //-- Gửi mail cho Tâm Trí Lực, cc đại lý
                var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
                var mailTemplate = Mapper.Map<Template>(_templateService.Get("POAGENCYSEND"));
                var mailTitle = mailTemplate.Title.Replace("{{PO_NO}}", salesOrderCard.Code);
                mailTitle = mailTitle.Replace("{{AGENCY}}", salesOrderCard.CustomerName);
                mailTitle = mailTitle.Replace("{{NOOFCARD}}", salesOrderCard.NoOfCardStr);
                mailTitle = mailTitle.Replace("{{USER}}", salesOrderCard.CustomerName);
                mailTitle = mailTitle.Replace("{{STATUS_NAME}}", "Chờ Xác Nhận");
                var content = mailTemplate.Content.Replace("{{FULLNAME}}", "ACE bộ phận bán hàng");
                content = content.Replace("{{PO_NO}}", salesOrderCard.Code);
                content = content.Replace("{{DOCUMENT_DATE}}", salesOrderCard.DocumentDateStr);
                content = content.Replace("{{AGENCY_NAME}}", salesOrderCard.CustomerName);
                content = content.Replace("{{AGENCY_ADD}}", salesOrderCard.CustomerAddress);
                content = content.Replace("{{AGENCY_EMAIL}}", salesOrderCard.CustomerEmail);
                content = content.Replace("{{AGENCY_PHONE}}", salesOrderCard.CustomerPhoneNo);
                content = content.Replace("{{NOOFCARD}}", string.Format("{0:#,##0}", salesOrderCard.NoOfCard));
                content = content.Replace("{{GIFT_CARD}}", string.Format("{0:#,##0}", salesOrderCard.GiftCard));
                content = content.Replace("{{AMOUNT}}", string.Format("{0:#,##0}", salesOrderCard.Amount));
                content = content.Replace("{{DISCOUNTPERCENT}}", string.Format("{0:#,##0}%", salesOrderCard.DiscountPercent));
                content = content.Replace("{{DISCOUNT_AMT}}", string.Format("{0:#,##0}", salesOrderCard.DiscountAmt));
                content = content.Replace("{{TOTALAMOUNT}}", string.Format("{0:#,##0}", salesOrderCard.TotalAmount));
                content = content.Replace("{{REMARK}}", salesOrderCard.Remark);

                var userIds = _userThuocNhomService.GetMany("SALES").Select(d => d.UserID).ToList() ?? new List<int>();
                var emailTos = Mapper.Map<IEnumerable<UserVM>>(_userService.FindAll(userIds)).Where(d => !string.IsNullOrEmpty(d.Email)).Select(d => d.Email).ToList();
                var _mailHelper = new EmailHelper()
                {
                    EmailTos = emailTos,
                    DisplayName = emailInfo.DisplayName,
                    Title = mailTitle,
                    Body = content,
                    MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
                };
                hasValue = _mailHelper.DoSendMail();
                //-- Gửi mail cho Tâm Trí Lực +
                var _status = string.IsNullOrEmpty(hasValue) ? "Oke" : "Error";
                var _msg = string.IsNullOrEmpty(hasValue) ? "" : hasValue;
                return Json(new { s = _status, m = _msg }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex) 
            {
                return Json(new { s = "Error", m = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        private void SetData(SalesOrderCard _salesOrderCard, bool isCreate)
        {
            var userInfo = GetUser();
            salesOrderCard = _salesOrderCard;
            if (isCreate)
            {
                salesOrderCard.Code = StringUtil.CheckLetter("PO", _salesHdrService.GetMax(salesType), 4);
                salesOrderCard.CreateBy = userInfo.Username;
                salesOrderCard.DateCreate = DateTime.Now;
                salesOrderCard.LastModifyBy = userInfo.Username;
                salesOrderCard.LastModifyDate = salesOrderCard.DateCreate;
            }
            else
            {
                salesOrderCard.LastModifyBy = userInfo.Username;
                salesOrderCard.LastModifyDate = DateTime.Now;
            }
            if (!string.IsNullOrEmpty(salesOrderCard.PostingDateStr))
                salesOrderCard.PostingDate = DateTime.Parse(StringUtil.ConvertStringToDate(salesOrderCard.PostingDateStr));
            else
                salesOrderCard.PostingDate = null;

            if (!string.IsNullOrEmpty(salesOrderCard.DocumentDateStr))
                salesOrderCard.DocumentDate = DateTime.Parse(StringUtil.ConvertStringToDate(salesOrderCard.DocumentDateStr));
            else
                salesOrderCard.DocumentDate = null;
            salesOrderCard.NoOfCard = salesOrderCard.TotalCard;
            var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("ORDERSTATUS", "700"));
            salesOrderCard.StatusId = statusInfo.LineNo;
            salesOrderCard.StatusCode = statusInfo.Code;
            salesOrderCard.StatusName = statusInfo.Name;
            salesOrderCard.ReceiptDate = null;
            var customerInfo = Mapper.Map<AgencyCard>(_agencyService.GetBy(salesOrderCard.CustomerId));
            salesOrderCard.CustomerName = customerInfo.FullName;
            salesOrderCard.CustomerAddress = customerInfo.Address;
            salesOrderCard.CustomerEmail = customerInfo.Email;
            salesOrderCard.CustomerPhoneNo = customerInfo.Phone;
            salesOrderCard.SalesType = salesType;
            var LevelInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("LV", salesOrderCard.LevelCode));
            salesOrderCard.LevelId = LevelInfo.LineNo;
            salesOrderCard.LevelCode = LevelInfo.Code;
            salesOrderCard.LevelName = LevelInfo.Name;
            mSalesHeader = Mapper.Map<M_SalesHeader>(salesOrderCard);
        }
    }
}