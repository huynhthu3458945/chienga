﻿using AutoMapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Areas.Agency.Data;
using GCSOFT.MVC.AGENCY.Areas.Transaction.Data;
using GCSOFT.MVC.AGENCY.Helper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.StoreProcedure.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Service.Transaction.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.Transaction.Controllers
{
    [CompressResponseAttribute]
    public class RetailOrderController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly ISalesHeaderService _salesHdrService;
        private readonly ISalesLineService _salesLineService;
        private readonly IAgencyService _agencyService;
        private readonly IOptionLineService _optionLineService;
        private readonly ICardLineService _cardLineService;
        private readonly ICardEntryService _cardEntryService;
        private readonly IAgencyCardService _agencyAndCardService;
        private readonly IStoreProcedureService _storeProcService;
        private readonly IAgencyCardService _agencyCardService;
        private readonly ITemplateService _templateService;
        private readonly IMailSetupService _mailSetupService;
        private string sysCategory = "AGEN_R_ORDER";
        private bool? permission = false;
        private SalesOrderCard salesOrderCard;
        private M_SalesHeader mSalesHeader;
        private List<AgencyAndCard> agencyAndCards;
        private List<CardEntry> cardEntries;
        private SalesType salesType = SalesType.Retail;
        private AgencyType agencyType = AgencyType.Branch;
        private CardLine cardLine = new CardLine();
        private AgencyAndCard agencyAndCard = new AgencyAndCard();
        private List<CardLine> cardLines = new List<CardLine>();
        private string hasValue;
        #endregion
        public RetailOrderController
            (
                IVuViecService vuViecService
                , ISalesHeaderService salesHdrService
                , ISalesLineService salesLineService
                , IAgencyService agencyService
                , IOptionLineService optionLineService
                , ICardLineService cardLineService
                , ICardEntryService cardEntryService
                , IAgencyCardService agencyAndCardService
                , IStoreProcedureService storeProcService
                , IAgencyCardService agencyCardService
                , ITemplateService templateService
                , IMailSetupService mailSetupService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _salesHdrService = salesHdrService;
            _salesLineService = salesLineService;
            _agencyService = agencyService;
            _optionLineService = optionLineService;
            _cardLineService = cardLineService;
            _cardEntryService = cardEntryService;
            _agencyAndCardService = agencyAndCardService;
            _storeProcService = storeProcService;
            _agencyCardService = agencyCardService;
            _templateService = templateService;
            _mailSetupService = mailSetupService;
        }
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var salesOrderList = Mapper.Map<IEnumerable<SalesOrderList>>(_salesHdrService.FindAllAgency(salesType, agencyType, GetUser().agencyInfo.Id));
            return View(salesOrderList);
        }
        public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var userInfo = GetUser();
            salesOrderCard = new SalesOrderCard();
            salesOrderCard.Code = StringUtil.CheckLetter("R", _salesHdrService.GetMax(salesType), 5);
            salesOrderCard.DocumentDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.DocumentDate);
            salesOrderCard.AgencyId = userInfo.agencyInfo.Id;
            salesOrderCard.AgencyName = userInfo.agencyInfo.FullName;
            //salesOrderCard.CustomerList = Mapper.Map<IEnumerable<AgencyList>>(_agencyService.FindAll(agencyType, GetUser().agencyInfo.Id)).ToSelectListItems(0);
            var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("DH", "100"));
            salesOrderCard.StatusId = statusInfo.LineNo;
            salesOrderCard.StatusCode = statusInfo.Code;
            salesOrderCard.StatusName = statusInfo.Name;
            salesOrderCard.CustomerType = agencyType;
            salesOrderCard.LevelList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("LV").Where(d => d.Code.Equals("100") || d.Code.Equals("300"))).ToSelectListItems("");
            salesOrderCard.TotalCard = 1;
            return View(salesOrderCard);
        }
        public ActionResult Edit(string id, int? p)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            salesOrderCard = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(salesType, id));
            if (salesOrderCard == null)
                return HttpNotFound();
            salesOrderCard.DocumentDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.DocumentDate);
            salesOrderCard.PostingDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.PostingDate);
            salesOrderCard.LevelList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("LV").Where(d => d.Code.Equals("100") || d.Code.Equals("300"))).ToSelectListItems(salesOrderCard.LevelCode);
            salesOrderCard.SalesLines = Mapper.Map<IEnumerable<SalesLine>>(_salesLineService.FindAll(salesType, id));
            return View(salesOrderCard);
        }
        [HttpPost]
        public ActionResult Create(SalesOrderCard _salesOrderCard)
        {
            try
            {
                SetData(_salesOrderCard, true);
                hasValue = _salesHdrService.Insert(mSalesHeader);
                cardLines = new List<CardLine>();
                agencyAndCards = new List<AgencyAndCard>();
                var idCard = _cardLineService.GetID() - 1;
                for (int i = 0; i < _salesOrderCard.TotalCard; i++)
                {
                    idCard += 1;
                    GetActionCode(_salesOrderCard, idCard);
                    cardLines.Add(cardLine);
                    agencyAndCards.Add(agencyAndCard);
                }
                var salesLine = new SalesLine();
                var salesLines = new List<SalesLine>();
                var lineNo = 0;
                foreach (var item in cardLines)
                {
                    lineNo += 100;
                    salesLine = new SalesLine();
                    salesLine.SalesType = SalesType.Retail;
                    salesLine.DocumentCode = _salesOrderCard.Code;
                    salesLine.LineNo = lineNo;
                    salesLine.LotNumber = item.LotNumber;
                    salesLine.CardId = item.Id;
                    salesLine.SerialNumber = item.SerialNumber;
                    salesLine.Price = item.Price;
                    salesLine.CardNo = item.CardNo;
                    salesLines.Add(salesLine);
                }
                mSalesHeader.SalesLines = Mapper.Map<IEnumerable<M_SalesLine>>(salesLines);
                hasValue = _salesLineService.Insert(mSalesHeader.SalesLines);
                hasValue = _cardLineService.Insert(Mapper.Map<IEnumerable<M_CardLine>>(cardLines));
                hasValue = _agencyAndCardService.Insert(Mapper.Map<IEnumerable<M_AgencyAndCard>>(agencyAndCards));
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                {
                    SendMail(cardLines, mSalesHeader);
                   return RedirectToAction("Edit", new { id = mSalesHeader.Code, msg = "1" });
                }
                    
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        private void SetData(SalesOrderCard _salesOrderCard, bool isCreate)
        {
            var userInfo = GetUser();
            salesOrderCard = _salesOrderCard;
            if (isCreate)
            {
                salesOrderCard.Code = StringUtil.CheckLetter("R", _salesHdrService.GetMax(salesType), 5);
                salesOrderCard.CreateBy = userInfo.Username;
                salesOrderCard.DateCreate = DateTime.Now;
                salesOrderCard.LastModifyBy = userInfo.Username;
                salesOrderCard.LastModifyDate = salesOrderCard.DateCreate;
            }
            else
            {
                salesOrderCard.LastModifyBy = userInfo.Username;
                salesOrderCard.LastModifyDate = salesOrderCard.DateCreate;
            }
            if (!string.IsNullOrEmpty(salesOrderCard.ReceiptDateStr))
                salesOrderCard.ReceiptDate = DateTime.Parse(StringUtil.ConvertStringToDate(salesOrderCard.ReceiptDateStr));
            else
                salesOrderCard.ReceiptDate = null;
            if (!string.IsNullOrEmpty(salesOrderCard.PostingDateStr))
                salesOrderCard.PostingDate = DateTime.Parse(StringUtil.ConvertStringToDate(salesOrderCard.PostingDateStr));
            else
                salesOrderCard.PostingDate = null;

            if (!string.IsNullOrEmpty(salesOrderCard.DocumentDateStr))
                salesOrderCard.DocumentDate = DateTime.Parse(StringUtil.ConvertStringToDate(salesOrderCard.DocumentDateStr));
            else
                salesOrderCard.DocumentDate = null;
            var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("DH", "200")); //Đã Giao Hàng
            salesOrderCard.StatusId = statusInfo.LineNo;
            salesOrderCard.StatusCode = statusInfo.Code;
            salesOrderCard.StatusName = statusInfo.Name;
            salesOrderCard.PostingDate = DateTime.Now;

            //salesOrderCard.CustomerName = customerInfo.FullName;
            //salesOrderCard.CustomerAddress = customerInfo.Address;
            //salesOrderCard.CustomerEmail = customerInfo.Email;
            //salesOrderCard.CustomerPhoneNo = customerInfo.Phone;

            salesOrderCard.SalesType = salesType;
            salesOrderCard.CustomerType = agencyType;
            var LevelInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("LV", salesOrderCard.LevelCode)) ?? new VM_OptionLine();
            salesOrderCard.LevelId = LevelInfo.LineNo;
            salesOrderCard.LevelCode = LevelInfo.Code;
            salesOrderCard.LevelName = LevelInfo.Name;
            
            mSalesHeader = Mapper.Map<M_SalesHeader>(salesOrderCard);
        }
        private string GetActionCode(SalesOrderCard _salesOrderCard, long idCard)
        {
            var _userInfo = GetUser();
            
            //-- Build Ma Kich Hoat
            var builder = new StringBuilder();
            Guid g = Guid.NewGuid();
            builder.Append(g.ToString().Split('-')[0].ToString().ToUpper());
            var randomNumber = new RandomNumberGenerator();
            builder.Append(randomNumber.RandomNumber(10, 99));
            var _cardNo = builder.ToString();
            //-- Build Ma Kich Hoat +
            var cryptorEngine = new CryptorEngine2();
            var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("CARD", "450"));
            var durationInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("CARDINFO", "ALL"));
            var LotNumber = "LO2107-003";
            string prefex = "S";
            if (_salesOrderCard.LevelCode.Equals("300"))
            {
                LotNumber = "LO2107-004";
                prefex = "L";
                durationInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("CARDINFO", "1Y"));
            }

            cardLine = new CardLine()
            {
                Id = idCard,
                LotNumber = LotNumber,
                SerialNumber = StringUtil.GetProductID(prefex, (idCard - 1).ToString(), 9),
                CardNo = cryptorEngine.Encrypt(_cardNo, true, "TTLSTNHD@CARD"),
                Price = _salesOrderCard.Price ?? 0,
                UserName = _userInfo.Username,
                UserFullName = _userInfo.FullName,
                DateCreate = DateTime.Now,
                FulName = salesOrderCard.CustomerName,
                PhoneNo = salesOrderCard.CustomerPhoneNo,
                UserType = UserType.TTL,
                UserNameActive = _userInfo.Username,
                UserActiveFullName = _userInfo.FullName,
                LastDateUpdate = DateTime.Now,
                StatusId = statusInfo.LineNo,
                StatusCode = statusInfo.Code,
                StatusName = statusInfo.Name,
                LastStatusId = statusInfo.LineNo,
                LastStatusCode = statusInfo.Code,
                LastStatusName = statusInfo.Name,

                DurationId = durationInfo.LineNo,
                DurationCode = durationInfo.Code,
                DurationName = durationInfo.Name,

                ReasonId = 45,
                ReasonCode = "BH",
                ReasonName = "Bán Hàng"
            };
            agencyAndCard = new AgencyAndCard()
            {
                AgencyId = salesOrderCard.AgencyId ?? 0,
                Id = cardLine.Id,
                LotNumber = cardLine.LotNumber,
                SerialNumber = cardLine.SerialNumber,
                CardNo = cardLine.CardNo,
                Price = cardLine.Price,
                UserName = cardLine.UserName,
                UserFullName = cardLine.UserFullName,
                DateCreate = cardLine.DateCreate,
                UserType = cardLine.UserType,
                UserNameActive = cardLine.UserNameActive,
                UserActiveFullName = cardLine.UserActiveFullName,
                LastDateUpdate = cardLine.LastDateUpdate,
                StatusId = statusInfo.LineNo,
                StatusCode = statusInfo.Code,
                StatusName = statusInfo.Name,
                DurationId = cardLine.DurationId,
                DurationCode = cardLine.DurationCode,
                DurationName = cardLine.DurationName,
                ReasonId = cardLine.ReasonId,
                ReasonCode = cardLine.ReasonCode,
                ReasonName = cardLine.ReasonName
            };

            return null;
        }
        public string SendMail(List<CardLine> cardLines, M_SalesHeader salesOrderCard)
        {
            try
            {
                foreach (var item in cardLines)
                {
                    //-- Gửi qua email
                    var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
                    var mailTemplate = Mapper.Map<Template>(_templateService.Get("EMAILACTIVECODE"));
                    var mailTitle = mailTemplate.Title;
                    var mailContent = mailTemplate.Content.Replace("{{FULLNAME}}", salesOrderCard.CustomerName);
                    mailContent = mailContent.Replace("{{ACTIVECODE}}", CryptorEngine.Decrypt(item.CardNo, true, "TTLSTNHD@CARD"));
                    var emailTos = new List<string>();
                    emailTos.Add(salesOrderCard.CustomerEmail);
                    var _mailHelper = new EmailHelper()
                    {
                        EmailTos = emailTos,
                        DisplayName = emailInfo.DisplayName,
                        Title = mailTitle,
                        Body = mailContent,
                        MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
                    };
                    string hasValue = _mailHelper.SendEmailsThread();

                    if (!string.IsNullOrEmpty(salesOrderCard.CustomerPhoneNo))
                    {
                        var newPhoneNo = salesOrderCard.CustomerPhoneNo.Trim();
                        var firstNumber = int.Parse(newPhoneNo[0].ToString());
                        if (firstNumber != 0)
                        {
                            newPhoneNo = "0" + newPhoneNo;
                        }
                        //-- Gui qua SMS
                        var smsTemplate = Mapper.Map<Template>(_templateService.Get("SMS_MAKICHHOAT"));
                        var smsHelper = new SMSHelper()
                        {
                            PhoneNo = newPhoneNo,
                            Content = StringUtil.StripTagsRegexCompiled(smsTemplate.Content).Replace("{{NAME}}", "vao 5 Phut Thuoc Bai").Replace("{{MAKICHHOAT}}", CryptorEngine.Decrypt(item.CardNo, true, "TTLSTNHD@CARD")).Replace("{{LINK}}", "https://sieutrinhohocduong.com/student")
                        };
                        if (smsHelper.IsMobiphone())
                        {
                            smsTemplate = Mapper.Map<Template>(_templateService.Get("MOBI_TKKH_KH"));
                            smsHelper.Content = string.Format(smsTemplate.SMSContent, "5 Phut Thuoc Bai", CryptorEngine.Decrypt(item.CardNo, true, "TTLSTNHD@CARD"));
                            hasValue = smsHelper.SendMobiphone();
                        }
                        else
                            hasValue = smsHelper.Send();
                        //-- Gui qua SMS +
                    }
                }
                //-- Gửi qua email +
                return string.Empty;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
    }
}