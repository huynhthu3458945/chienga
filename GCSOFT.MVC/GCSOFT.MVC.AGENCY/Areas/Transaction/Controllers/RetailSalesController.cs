﻿//####################################################################
//# Copyright (C) 2021-2022, TTL JSC.  All Rights Reserved. 
//#
//# History:
//#     Date Time       Updater         Comment
//#     20/08/2021      Huỳnh Thử        Create
//####################################################################

using AutoMapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Areas.Agency.Data;
using GCSOFT.MVC.AGENCY.Areas.Transaction.Data;
using GCSOFT.MVC.AGENCY.Helper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.ExeclOfficeEngine;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.StoreProcedure.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Service.Transaction.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace GCSOFT.MVC.AGENCY.Areas.Transaction.Controllers
{
    [CompressResponseAttribute]
    public class RetailSalesController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly ISalesHeaderService _salesHdrService;
        private readonly ISalesLineService _salesLineService;
        private readonly IAgencyService _agencyService;
        private readonly IOptionLineService _optionLineService;
        private readonly ICardLineService _cardLineService;
        private readonly ICardEntryService _cardEntryService;
        private readonly IAgencyCardService _agencyAndCardService;
        private readonly IStoreProcedureService _storeProcService;
        private readonly IAgencyCardService _agencyCardService;
        private readonly ITemplateService _templateService;
        private readonly IMailSetupService _mailSetupService;
        private readonly ICustomerService _customerService;
        private readonly ISalesPriceService _salesPriceService;
        private readonly IUserInfoService _userInfoService;
        private readonly IClassService _classService;
        private readonly IStudentClassService _studentClassService;
        private readonly ISMSService _smsService;
        private string sysCategory = "AGEN_RS_SALE";
        private bool? permission = false;
        private SalesOrderCard salesOrderCard;
        private M_SalesHeader mSalesHeader;
        private List<AgencyAndCard> agencyAndCards;
        private List<CardEntry> cardEntries;
        private SalesType salesType = SalesType.Retail;
        private AgencyType agencyType = AgencyType.Branch;
        private CardLine cardLine = new CardLine();
        private AgencyAndCard agencyAndCard = new AgencyAndCard();
        private List<CardLine> cardLines = new List<CardLine>();
        private List<UserInfoViewModel> UserInfos = new List<UserInfoViewModel>();
        private UserInfoViewModel userInfo = new UserInfoViewModel();
        private CardEntry cardEntry;//-- Ghi nhận thẻ đã bán
        private string hasValue;
        private string _serinumber;
        private UserLoginVMs _userInfo;
        #endregion
        public RetailSalesController
            (
                IVuViecService vuViecService
                , ISalesHeaderService salesHdrService
                , ISalesLineService salesLineService
                , IAgencyService agencyService
                , IOptionLineService optionLineService
                , ICardLineService cardLineService
                , ICardEntryService cardEntryService
                , IAgencyCardService agencyAndCardService
                , IStoreProcedureService storeProcService
                , IAgencyCardService agencyCardService
                , ITemplateService templateService
                , IMailSetupService mailSetupService
                , ICustomerService customerService
                , ISalesPriceService salesPriceService
                , IUserInfoService userInfoService
                , IClassService classService
                , IStudentClassService studentClassService
                , ISMSService sMSService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _salesHdrService = salesHdrService;
            _salesLineService = salesLineService;
            _agencyService = agencyService;
            _optionLineService = optionLineService;
            _cardLineService = cardLineService;
            _cardEntryService = cardEntryService;
            _agencyAndCardService = agencyAndCardService;
            _storeProcService = storeProcService;
            _agencyCardService = agencyCardService;
            _templateService = templateService;
            _mailSetupService = mailSetupService;
            _customerService = customerService;
            _salesPriceService = salesPriceService;
            _userInfoService = userInfoService;
            _classService = classService;
            _studentClassService = studentClassService;
            _smsService = sMSService;
        }

        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [20/08/2021]
        /// </history>
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var salesOrderList = Mapper.Map<IEnumerable<SalesOrderList>>(_salesHdrService.FindAllAgency(salesType, agencyType, GetUser().agencyInfo.Id));
            return View(salesOrderList);
        }
        public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var userInfo = GetUser();
            salesOrderCard = new SalesOrderCard();
            salesOrderCard.Code = StringUtil.CheckLetter("RS", _salesHdrService.GetMax(salesType), 5);
            salesOrderCard.DocumentDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.DocumentDate);
            salesOrderCard.AgencyId = userInfo.agencyInfo.Id;
            salesOrderCard.AgencyName = userInfo.agencyInfo.FullName;
            var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("DH", "100"));
            salesOrderCard.StatusId = statusInfo.LineNo;
            salesOrderCard.StatusCode = statusInfo.Code;
            salesOrderCard.StatusName = statusInfo.Name;
            salesOrderCard.CustomerType = agencyType;
            salesOrderCard.LevelList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("LV")).ToSelectListItems("");
            //salesOrderCard.ClassList = Mapper.Map<IEnumerable<ClassList>>(_classService.FindAll(ClassType.Class, "SHOW")).ToList().ToSelectListItems(0);
            ViewBag.ClassListStr = ClassListStr(0, 0);
            salesOrderCard.TypeSaleList = new List<SelectListItem>() {
            new SelectListItem()
                {
                    Text = "Bán Mã Kích Hoạt",
                    Value = "1",
                },
            new SelectListItem()
                {
                    Text = "Đăng Ký Tài Khoản",
                    Value = "2",
                }
            };

            salesOrderCard.TotalCard = 1;

            var customer = new CustomerViewModel();
            customer.ClassList = Mapper.Map<IEnumerable<ClassList>>(_classService.FindAll(ClassType.Class, "SHOW")).ToList().ToSelectListItems(0);
            salesOrderCard.CustomerViewModelList.Add(customer);
            return View(salesOrderCard);
        }
        public ActionResult Edit(string id, int? p)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            salesOrderCard = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(salesType, id));
            if (salesOrderCard == null)
                return HttpNotFound();
            salesOrderCard.DocumentDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.DocumentDate);
            salesOrderCard.PostingDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.PostingDate);
            salesOrderCard.LevelList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("LV").Where(d => d.Code.Equals("100") || d.Code.Equals("300"))).ToSelectListItems(salesOrderCard.LevelCode);
            salesOrderCard.SalesLines = Mapper.Map<IEnumerable<SalesLine>>(_salesLineService.FindAll(salesType, id));
            salesOrderCard.CustomerViewModelList = Mapper.Map<List<CustomerViewModel>>(_customerService.FindAll(salesOrderCard.Code));
            salesOrderCard.TypeSaleList = new List<SelectListItem>() {
            new SelectListItem()
                {
                    Selected = salesOrderCard.TypeSale.Equals("1")? true : false,
                    Text = "Bán Mã Kích Hoạt",
                    Value = "1",
                },
            new SelectListItem()
                {
                    Selected = salesOrderCard.TypeSale.Equals("2")? true : false,
                    Text = "Đăng Ký Tài Khoản",
                    Value = "2",
                }
            };
            foreach (var item in salesOrderCard.CustomerViewModelList)
            {
                item.ClassList = Mapper.Map<IEnumerable<ClassList>>(_classService.FindAll(ClassType.Class, "SHOW")).ToList().ToSelectListItems(item.GradeId ?? 0);
            }
            return View(salesOrderCard);
        }
        public JsonResult GetLevelInfo(string l)
        {

            try
            {
                if (string.IsNullOrEmpty(l))
                    return Json(new { price = 0, durationCode = "", durationName = "" }, JsonRequestBehavior.AllowGet);
                var priceInfo = _salesPriceService.GetBy(l, DateTime.Now);
                return Json(new { price = priceInfo.Price, durationCode = priceInfo.DurationCode, durationName = priceInfo.DurationName }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { price = 0, durationCode = "", durationName = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult CheckEndQuantity(int agencyId, string lvCode)
        {
            var idCards = _agencyAndCardService.FindAll(agencyId, "100", string.Empty, lvCode).OrderBy(z => z.Id);
            // Note we are returning a filename as well as the handle
            return Json(idCards != null ? idCards.Count() : 0, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckUserNameAndPhoneActive(List<UserPhone> userNames)
        {
            try
            {
                bool status = false;
                string res = string.Empty;
                if (userNames[0].TypeSale == null)
                    return Json(new { status = status, res = res }, JsonRequestBehavior.AllowGet);
                if (userNames[0].TypeSale.Equals("1")) // Bán kích hoạt
                    return Json(new { status = status, res = res }, JsonRequestBehavior.AllowGet);
                int currentPhone = 0;
                int currentEmail = 0;
                foreach (var userName in userNames)
                {
                    if (string.IsNullOrEmpty(userName.UserName))
                    {
                        status = true;
                        res += $"- Chưa nhập đủ tên đăng nhập<BR />";
                    }
                    if (string.IsNullOrEmpty(userName.Phone))
                    {
                        status = true;
                        res += $"- Chưa nhập đủ số điện thoại<BR />";
                    }
                    if (string.IsNullOrEmpty(userName.Email))
                    {
                        status = true;
                        res += $"- Chưa nhập đủ Email khách hàng<BR />";
                    }
                    if (status)
                        break;
                    if (userNames.Where(d => d.UserName.Equals(userName.UserName)).Count() > 1 )
                    {
                        status = true;
                        res += $"- Tài Khoản: {userName.UserName} Trùng nhau trong form đăng ký<BR />";
                    }
                    // check user name
                    var user = _userInfoService.GetBy(userName.UserName);
                    if (user != null && user.Id != 0)
                    {
                        status = true;
                        res += $"- Tài Khoản: {userName.UserName} Đã tồn tại!<BR />";
                    }
                    // Phone
                    //var newPhoneNo = userName.Phone.Trim();
                    //var firstNumber = int.Parse(newPhoneNo[0].ToString());
                    //if (firstNumber != 0)
                    //{
                    //    newPhoneNo = "0" + newPhoneNo;
                    //}
                    currentPhone = userNames.Where(d => d.Phone == userName.Phone.Trim()).Count();
                    var userPhones = _userInfoService.NoOfMemberResiger(userName.Phone.Trim()) + currentPhone;
                    if (userPhones > 5)
                    {
                        status = true;
                        res += $"- Số Điện Thoại: {userName.Phone} đã được đăng ký hơn 5 Tài Khoản, vui lòng chọn số điện thoại khác<BR />";
                    }
                    if (status)
                        break;
                    currentEmail = userNames.Where(d => d.Email == userName.Email.Trim()).Count();
                    if (userPhones > 5)
                    {
                        status = true;
                        res += $"- Email: {userName.Email} đã được đăng ký hơn 5 Tài Khoản, vui lòng chọn Email khác<BR />";
                    }
                    if (status)
                        break;
                }
                return Json(new { status = status, res = res }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = false, res = "Có lỗi trong quá trình xử lý. Vui lòng kiểm tra lại!" }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult Create(SalesOrderCard _salesOrderCard)
        {
            try
            {
                SetData(_salesOrderCard, true);
                hasValue = _salesHdrService.Insert(mSalesHeader);
                hasValue = _customerService.Delete(salesOrderCard.Code);

                cardLines = new List<CardLine>();
                agencyAndCards = new List<AgencyAndCard>();
                var serinumber = string.Empty;
                var cardToAssigns = Mapper.Map<IEnumerable<AgencyAndCard>>(_agencyAndCardService.GetNewCards(GetUser().agencyInfo.Id, _salesOrderCard.LevelCode, "100", salesOrderCard.CustomerViewModelList.Count())).ToList();
                int index = 0;
                _userInfo = GetUser();
                foreach (var customer in salesOrderCard.CustomerViewModelList)
                {
                    serinumber = cardToAssigns.ToList()[index].SerialNumber;
                    customer.SerialNumber = serinumber;
                    if (_salesOrderCard.TypeSale.Equals("2")) // Đăng ký tài khoản
                        RegisterUser(customer, (decimal)salesOrderCard.Price, serinumber);
                    else
                        GetActionCode(customer, (decimal)salesOrderCard.Price, serinumber);
                    index++;
                }
                hasValue = _customerService.Insert(Mapper.Map<IEnumerable<M_Customer>>(salesOrderCard.CustomerViewModelList));
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = mSalesHeader.Code, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        private void SetData(SalesOrderCard _salesOrderCard, bool isCreate)
        {
            var userInfo = GetUser();
            salesOrderCard = _salesOrderCard;
            if (isCreate)
            {
                salesOrderCard.Code = StringUtil.CheckLetter("RS", _salesHdrService.GetMax(salesType), 5);
                salesOrderCard.CreateBy = userInfo.Username;
                salesOrderCard.DateCreate = DateTime.Now;
                salesOrderCard.LastModifyBy = userInfo.Username;
                salesOrderCard.LastModifyDate = salesOrderCard.DateCreate;
            }
            else
            {
                salesOrderCard.LastModifyBy = userInfo.Username;
                salesOrderCard.LastModifyDate = salesOrderCard.DateCreate;
            }
            if (!string.IsNullOrEmpty(salesOrderCard.ReceiptDateStr))
                salesOrderCard.ReceiptDate = DateTime.Parse(StringUtil.ConvertStringToDate(salesOrderCard.ReceiptDateStr));
            else
                salesOrderCard.ReceiptDate = null;
            if (!string.IsNullOrEmpty(salesOrderCard.PostingDateStr))
                salesOrderCard.PostingDate = DateTime.Parse(StringUtil.ConvertStringToDate(salesOrderCard.PostingDateStr));
            else
                salesOrderCard.PostingDate = null;

            if (!string.IsNullOrEmpty(salesOrderCard.DocumentDateStr))
                salesOrderCard.DocumentDate = DateTime.Parse(StringUtil.ConvertStringToDate(salesOrderCard.DocumentDateStr));
            else
                salesOrderCard.DocumentDate = null;

            var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("DH", "200")); //Đã Giao Hàng
            salesOrderCard.StatusId = statusInfo.LineNo;
            salesOrderCard.StatusCode = statusInfo.Code;
            salesOrderCard.StatusName = statusInfo.Name;
            salesOrderCard.PostingDate = DateTime.Now;

            salesOrderCard.SalesType = salesType;
            salesOrderCard.CustomerType = agencyType;
            var LevelInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("LV", salesOrderCard.LevelCode)) ?? new VM_OptionLine();
            salesOrderCard.LevelId = LevelInfo.LineNo;
            salesOrderCard.LevelCode = LevelInfo.Code;
            salesOrderCard.LevelName = LevelInfo.Name;
            var durationInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("CARDINFO", salesOrderCard.DurationCode)) ?? new VM_OptionLine();
            salesOrderCard.DurationId = durationInfo.LineNo;
            salesOrderCard.DurationCode = durationInfo.Code;
            salesOrderCard.DurationName = durationInfo.Name;
            foreach (var item in salesOrderCard.CustomerViewModelList)
            {
                item.Code = salesOrderCard.Code;
                item.LotNumber = salesOrderCard.Code;
                item.IsEmail = StringUtil.Validate(item.Email) ? "Đúng" : "Sai Email";
                item.RegisterDate = DateTime.Now;
                item.Owner = GetUser().agencyInfo.FullName;
                item.UserOwner = GetUser().Username;
                item.DateCard = DateTime.Now;
                item.LevelId = salesOrderCard.LevelId;
                item.LevelCode = salesOrderCard.LevelCode;
                item.LevelName = salesOrderCard.LevelName;
                item.DurationId = salesOrderCard.DurationId;
                item.DurationCode = salesOrderCard.DurationCode;
                item.DurationName = salesOrderCard.DurationName;
            }
            mSalesHeader = Mapper.Map<M_SalesHeader>(salesOrderCard);
        }
        private string GetActionCode(CustomerViewModel customer, decimal price, string serinumber)
        {
            var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("CARD", "450"));
            //-- Check ma trung
            var hasCardNo = true;
            var _cardNo = string.Empty;
            do
            {
                _cardNo = CryptorEngine.Encrypt(GetBuildCardNo().ToString(), true, "TTLSTNHD@CARD");
                hasCardNo = cardLines.Where(d => d.CardNo == _cardNo).Any();
            } while (_cardLineService.CheckExist(_cardNo) || hasCardNo);
            //-- Check ma trung +
            cardLine = Mapper.Map<CardLine>(_cardLineService.GetBySerialNumber(serinumber));
            cardLine.CardNo = _cardNo;
            cardLine.FulName = customer.FullName;
            cardLine.PhoneNo = customer.PhoneNo;
            cardLine.VerifyInfo = customer.Email;
            cardLine.LastStatusId = statusInfo.LineNo;
            cardLine.LastStatusCode = statusInfo.Code;
            cardLine.LastStatusName = statusInfo.Name;
            cardLine.LastDateUpdate = DateTime.Now;
            cardLine.DateBuy = DateTime.Now;
            cardLine.Price = price;
            //agencyAndCard = Mapper.Map<AgencyAndCard>(_agencyAndCardService.GetBy(salesOrderCard.AgencyId.Value, idCard));
            agencyAndCard = Mapper.Map<AgencyAndCard>(_agencyCardService.GetBy(salesOrderCard.AgencyId.Value, serinumber));
            agencyAndCard.CardNo = _cardNo;
            agencyAndCard.StatusId = statusInfo.LineNo;
            agencyAndCard.StatusCode = statusInfo.Code;
            agencyAndCard.StatusName = statusInfo.Name;

            agencyAndCard.UserNameActive = _userInfo.Username;
            agencyAndCard.UserActiveFullName = _userInfo.FullName;
            agencyAndCard.LastDateUpdate = DateTime.Now;

            agencyAndCard.VerifyInfo = customer.Email;
            agencyAndCard.FulName = customer.FullName;
            agencyAndCard.PhoneNo = customer.PhoneNo;
            agencyAndCard.CardNo = cardLine.CardNo;
            agencyAndCard.LastDateUpdate = cardLine.LastDateUpdate;
            //-- Send Email, SMS
            var smsTemplate = Mapper.Map<Template>(_templateService.Get("SMS_MAKICHHOAT"));
            var smsInfo = new SMSViewModel()
            {
                Type = SMSType.SMS,
                PhoneNo = agencyAndCard.PhoneNo,
                OTP = null,
                Content = StringUtil.StripTagsRegexCompiled(smsTemplate.Content).Replace("{{NAME}}", "vao 5 Phut Thuoc Bai").Replace("{{MAKICHHOAT}}", CryptorEngine.Decrypt(_cardNo, true, "TTLSTNHD@CARD")).Replace("{{LINK}}", "https://sieutrinhohocduong.com/student"),
                DateInput = DateTime.Now,
                Username = _userInfo.Username,
                FullName = _userInfo.FullName,
                Serinumber = agencyAndCard.SerialNumber
            };
            var smsHelper = new SMSHelper()
            {
                PhoneNo = smsInfo.PhoneNo,
                Content = smsInfo.Content
            };
            if (smsHelper.IsMobiphone())
            {
                smsTemplate = Mapper.Map<Template>(_templateService.Get("MOBI_TKKH_KH"));
                smsHelper.Content = string.Format(smsTemplate.SMSContent, "5 Phut Thuoc Bai", CryptorEngine.Decrypt(_cardNo, true, "TTLSTNHD@CARD"));
                hasValue = smsHelper.SendMobiphone();
            }
            else
                hasValue = smsHelper.Send();
            //-- Gửi qua email
            var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
            var mailTemplate = Mapper.Map<Template>(_templateService.Get("EMAILACTIVECODE"));
            var mailTitle = mailTemplate.Title;
            var mailContent = mailTemplate.Content.Replace("{{FULLNAME}}", _userInfo.FullName);
            mailContent = mailContent.Replace("{{ACTIVECODE}}", CryptorEngine.Decrypt(_cardNo, true, "TTLSTNHD@CARD"));
            var emailTos = new List<string>();
            emailTos.Add(customer.Email);
            var _mailHelper = new EmailHelper()
            {
                EmailTos = emailTos,
                DisplayName = emailInfo.DisplayName,
                Title = mailTitle,
                Body = mailContent,
                MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
            };
            hasValue = _mailHelper.SendEmailsThread();
            //-- SEnd Email, SMS +
            _cardLineService.Update(Mapper.Map<M_CardLine>(cardLine));
            _agencyCardService.Update(Mapper.Map<M_AgencyAndCard>(agencyAndCard));
            return null;
        }
        private string RegisterUser(CustomerViewModel customer, decimal price, string serinumber)
        {
            var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("CARD", "500")); //Cập nhật trạng thái đã kích hoạt
            cardLine = Mapper.Map<CardLine>(_cardLineService.GetBySerialNumber(serinumber));
            cardLine.PhoneNo = customer.PhoneNo;
            cardLine.FulName = customer.FullName;
            cardLine.IsActive = true;
            cardLine.LastStatusId = statusInfo.LineNo;
            cardLine.LastStatusCode = statusInfo.Code;
            cardLine.LastStatusName = statusInfo.Name;
            cardLine.LastDateUpdate = DateTime.Now;
            cardLine.ApplyDate = DateTime.Now;
            cardLine.ApplyUser = customer.UserName;
            cardLine.DateBuy = DateTime.Now;
            cardLine.Price = price;

            agencyAndCard = Mapper.Map<AgencyAndCard>(_agencyCardService.GetBy(salesOrderCard.AgencyId.Value, serinumber));
            agencyAndCard.UserNameActive = _userInfo.Username;
            agencyAndCard.UserActiveFullName = _userInfo.FullName;
            agencyAndCard.LastDateUpdate = DateTime.Now;
            agencyAndCard.StatusId = statusInfo.LineNo;
            agencyAndCard.StatusCode = statusInfo.Code;
            agencyAndCard.StatusName = statusInfo.Name;
            agencyAndCard.PhoneNo = customer.PhoneNo;
            agencyAndCard.FulName = customer.FullName;
            agencyAndCard.VerifyInfo = customer.Email;

            //-- Insert user info
            userInfo = new UserInfoViewModel();
            var randomNumber = new RandomNumberGenerator();
            userInfo.userName = customer.UserName;
            userInfo.fullName = customer.FullName;
            userInfo.Phone = customer.PhoneNo;
            userInfo.Email = customer.Email;
            userInfo.Address = customer.Address;
            userInfo.ClassId = customer.GradeId ?? 0;
            userInfo.Password = randomNumber.RandomNumber(100000, 999999).ToString();
            userInfo.ActivePhone = true;
            userInfo.HasClass = true;
            userInfo.DateActive = DateTime.Now;
            userInfo.STNHDType = STNHDType.Active;
            userInfo.Serinumber = cardLine.SerialNumber;
            int noOfDuration = 0;
            try { noOfDuration = int.Parse(StringUtil.RebuildPhoneNo(cardLine.DurationCode)); }
            catch { noOfDuration = 0; }
            if (cardLine.DurationCode.EndsWith("Y"))
                userInfo.DateExpired = DateTime.Now.AddYears(noOfDuration);
            else if (cardLine.DurationCode.EndsWith("W"))
            {
                int noOfWeeks = noOfDuration * 7;
                userInfo.DateExpired = DateTime.Now.AddDays(noOfWeeks);
            }
            else if (cardLine.DurationCode.EndsWith("M"))
                userInfo.DateExpired = DateTime.Now.AddMonths(noOfDuration);
            else if (cardLine.DurationCode.EndsWith("D"))
                userInfo.DateExpired = DateTime.Now.AddDays(noOfDuration);
            else
                userInfo.DateExpired = null;
            _userInfoService.Insert(Mapper.Map<M_UserInfo>(userInfo));
            if (!salesOrderCard.LevelCode.Equals("100"))
            {
                //-- By Level
                bool isTieuHoc = false; bool isTHCS = false; bool isTHPT = false;
                var idLevelTieuHocs = new List<int>(); var idLevelTHCS = new List<int>(); var idLevelTHPT = new List<int>();
                if (salesOrderCard.LevelCode.Equals("200"))
                {
                    idLevelTieuHocs.Add(4); idLevelTieuHocs.Add(5); idLevelTieuHocs.Add(6); idLevelTieuHocs.Add(7); idLevelTieuHocs.Add(8);
                    idLevelTHCS.Add(9); idLevelTHCS.Add(13); idLevelTHCS.Add(14); idLevelTHCS.Add(15);
                    idLevelTHPT.Add(16); idLevelTHPT.Add(17); idLevelTHPT.Add(18);

                    if (idLevelTieuHocs.Contains(userInfo.ClassId))
                        isTieuHoc = true;
                    if (idLevelTHCS.Contains(userInfo.ClassId))
                        isTHCS = true;
                    if (idLevelTHPT.Contains(userInfo.ClassId))
                        isTHPT = true;
                }
                //-- By Level +
                var _studentClasses = new List<StudentClassViewModel>();
                var _studentClass = new StudentClassViewModel();
                var classes = Mapper.Map<IEnumerable<ClassList>>(_classService.FindAll(ClassType.Class, "SHOW")).ToList();
                foreach (var item in classes)
                {
                    _studentClass = new StudentClassViewModel();
                    _studentClass.userName = userInfo.userName;
                    _studentClass.FromDate = DateTime.Now;
                    _studentClass.ToDate = userInfo.DateExpired ?? DateTime.Now;
                    _studentClass.ClassId = item.Id;
                    _studentClass.ClassName = item.Name;
                    _studentClass.IsActive = false;
                    if (salesOrderCard.LevelCode.Equals("200"))
                    {
                        if (isTieuHoc && idLevelTieuHocs.Contains(item.Id))
                            _studentClass.IsActive = true;
                        if (isTHCS && idLevelTHCS.Contains(item.Id))
                            _studentClass.IsActive = true;
                        if (isTHPT && idLevelTHPT.Contains(item.Id))
                            _studentClass.IsActive = true;
                    }
                    else
                    {
                        if (item.Id == userInfo.ClassId)
                        {
                            if (userInfo.DateExpired != null)
                                _studentClass.ToDate = userInfo.DateExpired ?? DateTime.Now;
                            _studentClass.IsActive = true;
                        }
                    }
                    _studentClasses.Add(_studentClass);
                }
                _studentClassService.Delete(userInfo.userName);
                _studentClassService.Insert(Mapper.Map<IEnumerable<M_StudentClass>>(_studentClasses));
            }
            //-- Insert user info +
            //-- Send Email, SMS
            var smsTemplate = Mapper.Map<Template>(_templateService.Get("SMS_TT_DANG_NHAP"));
            var smsInfo = new SMSViewModel()
            {
                Type = SMSType.SMS,
                PhoneNo = userInfo.Phone,
                OTP = null,
                Content = StringUtil.StripTagsRegexCompiled(smsTemplate.Content).Replace("{{NAME}}", "5 Phut Thuoc Bai").Replace("{{USERNAME}}", userInfo.userName).Replace("{{MATKHAU}}", userInfo.Password),
                DateInput = DateTime.Now,
                Username = userInfo.userName,
                FullName = userInfo.fullName,
                Serinumber = agencyAndCard.SerialNumber
            };
            var smsHelper = new SMSHelper()
            {
                PhoneNo = smsInfo.PhoneNo,
                Content = smsInfo.Content
            };
            if (smsHelper.IsMobiphone())
            {
                smsTemplate = Mapper.Map<Template>(_templateService.Get("MOBI_TKKH_TC"));
                smsHelper.Content = string.Format(smsTemplate.SMSContent, userInfo.userName, userInfo.Password);
                hasValue = smsHelper.SendMobiphone();
            }
            else
                hasValue = smsHelper.Send();
            //hasValue = smsHelper.Send();
            //-- Gửi qua email
            var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
            var mailTemplate = Mapper.Map<Template>(_templateService.Get("EMAIL_REGISTER"));
            var mailTitle = mailTemplate.Title;
            var mailContent = mailTemplate.Content.Replace("{{FULLNAME}}", userInfo.fullName);
            mailContent = mailContent.Replace("{{USERNAME}}", userInfo.userName);
            mailContent = mailContent.Replace("{{PASSWORD}}", userInfo.Password);
            var emailTos = new List<string>();
            emailTos.Add(userInfo.Email);
            var _mailHelper = new EmailHelper()
            {
                EmailTos = emailTos,
                DisplayName = emailInfo.DisplayName,
                Title = mailTitle,
                Body = mailContent,
                MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
            };
            hasValue = _mailHelper.SendEmailsThread();
            //-- Send Email, SMS +
            _cardLineService.Update(Mapper.Map<M_CardLine>(cardLine));
            _agencyCardService.Update(Mapper.Map<M_AgencyAndCard>(agencyAndCard));
            return null;
        }
        /// <summary>
        /// GetBuildCardNo
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [26/08/2021]
        /// </history>
        private StringBuilder GetBuildCardNo()
        {
            var builder = new StringBuilder();
            var randomNumber = new RandomNumberGenerator();
            builder.Append(randomNumber.RandomString(4, true).ToUpper());
            builder.Append(randomNumber.RandomNumber(1000, 9999));
            builder.Append(randomNumber.RandomString(2, false).ToUpper());

            return builder;
        }

        public string SendMail(List<CardLine> cardLines)
        {
            try
            {
                foreach (var item in cardLines)
                {
                    //-- Gửi qua email
                    var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
                    var mailTemplate = Mapper.Map<Template>(_templateService.Get("EMAILACTIVECODE"));
                    var mailTitle = mailTemplate.Title;
                    var mailContent = mailTemplate.Content.Replace("{{FULLNAME}}", item.FulName);
                    mailContent = mailContent.Replace("{{ACTIVECODE}}", CryptorEngine.Decrypt(item.CardNo, true, "TTLSTNHD@CARD"));
                    var emailTos = new List<string>();
                    emailTos.Add(item.VerifyInfo);
                    var _mailHelper = new EmailHelper()
                    {
                        EmailTos = emailTos,
                        DisplayName = emailInfo.DisplayName,
                        Title = mailTitle,
                        Body = mailContent,
                        MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
                    };
                    string hasValue = _mailHelper.SendEmailsThread();

                    if (!string.IsNullOrEmpty(item.PhoneNo))
                    {
                        var newPhoneNo = item.PhoneNo.Trim();
                        var firstNumber = int.Parse(newPhoneNo[0].ToString());
                        if (firstNumber != 0)
                        {
                            newPhoneNo = "0" + newPhoneNo;
                        }
                        //-- Gui qua SMS
                        var smsTemplate = Mapper.Map<Template>(_templateService.Get("SMS_MAKICHHOAT"));
                        var smsHelper = new SMSHelper()
                        {
                            PhoneNo = newPhoneNo,
                            Content = StringUtil.StripTagsRegexCompiled(smsTemplate.Content).Replace("{{NAME}}", "vao 5 Phut Thuoc Bai").Replace("{{MAKICHHOAT}}", CryptorEngine.Decrypt(item.CardNo, true, "TTLSTNHD@CARD")).Replace("{{LINK}}", "https://sieutrinhohocduong.com/student")
                        };
                        if (smsHelper.IsMobiphone())
                        {
                            smsTemplate = Mapper.Map<Template>(_templateService.Get("MOBI_TKKH_KH"));
                            smsHelper.Content = string.Format(smsTemplate.SMSContent, "5 Phut Thuoc Bai", CryptorEngine.Decrypt(item.CardNo, true, "TTLSTNHD@CARD"));
                            hasValue = smsHelper.SendMobiphone();
                        }
                        else
                            hasValue = smsHelper.Send();
                        //-- Gui qua SMS +
                    }
                }
                //-- Gửi qua email +
                return string.Empty;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public ActionResult DownloadExcel()
        {
            var res = File(HttpContext.Server.MapPath("/Areas/Transaction/Report/RetailSales.xlsx"), OfficeContentType.XLSX, "RetailSales.xlsx");
            return res;
        }

        /// <summary>
        ///     RenderHtml
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [26/08/2021]
        /// </history>
        [HttpPost]
        public JsonResult RenderHtml(string typeSale)
        {
            try
            {
                var gradeList = Mapper.Map<IEnumerable<ClassList>>(_classService.FindAll(ClassType.Class, "SHOW")).ToList();
                var gradeId = 0;
                var PostedFile = Request.Files[0];
                string filename = PostedFile.FileName;
                string targetpath = Server.MapPath("/Areas/Transaction/Files/");
                if (!Directory.Exists(targetpath))
                    Directory.CreateDirectory(targetpath);
                PostedFile.SaveAs(targetpath + filename);
                string pathToExcelFile = targetpath + filename;
                var connectionString = "";
                if (filename.EndsWith(".xls"))
                {
                    connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=Excel 8.0;", pathToExcelFile);
                }
                else if (filename.EndsWith(".xlsx"))
                {
                    connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";", pathToExcelFile);
                }
                var adapter = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", connectionString);
                var ds = new DataSet();
                adapter.Fill(ds, "ExcelTable");
                DataTable dtable = ds.Tables["ExcelTable"];
                string strHtml = string.Empty;
                int index = 0;
                foreach (DataRow item in dtable.Rows)
                {
                    gradeId = 0;
                    if (!string.IsNullOrEmpty(item["Grade"]?.ToString()))
                    {
                        try { gradeId = gradeList.Where(d => d.SortOrder == int.Parse(item["Grade"].ToString())).Select(d => d.Id).FirstOrDefault(); }
                        catch { gradeId = 0; }
                    }
                    strHtml += $"<div class=\"uk-grid tr\" data-uk-grid-margin>"
                            + $"<div class=\"uk-width-medium-1-10 uk-text-center\">"
                            + $"<p class=\"uk-text-large\">{index + 1}</p>"
                            + $"</div>"
                            + $"<div class=\"uk-width-medium-8-10\">"
                            + $"<div class=\"uk-grid uk-grid-small\" data-uk-grid-margin>"
                            + $"<div class=\"uk-width-medium-2-10\">"
                            + $"<label for=\"CustomerViewModelList__{index}__FullName\">Họ tên người mua</label>"
                            + $"<input class=\"md-input validate[required]\" accesskey=\"KeyFocus\" id=\"CustomerViewModelList_{index}__FullName\" name=\"CustomerViewModelList[{index}].FullName\" type=\"text\" value=\"{item["FullName"]?.ToString() ?? ""}\" />"
                            + $"</div>"
                            + $"<div class=\"uk-width-medium-2-10\">"
                            + $"<label for=\"CustomerViewModelList__{index}__PhoneNo\">Số điện thoại</label>"
                            + $"<input class=\"md-input validate[required]\" accesskey=\"KeyFocus\" id=\"CustomerViewModelList_{index}__PhoneNo\" name=\"CustomerViewModelList[{index}].PhoneNo\" type=\"text\" value=\"{item["PhoneNo"]?.ToString() ?? ""}\" />"
                            + $"</div>"
                            + $"<div class=\"uk-width-medium-2-10\">"
                            + $"<label for=\"CustomerViewModelList__{index}__Email\">Email</label>"
                            + $"<input class=\"md-input validate[required]\" accesskey=\"KeyFocus\" id=\"CustomerViewModelList_{index}__Email\" name=\"CustomerViewModelList[{index}].Email\" type=\"text\" value=\"{item["Email"]?.ToString() ?? ""}\" />"
                            + $"</div>"
                            + $"<div class=\"uk-width-medium-4-10\">"
                            + $"<label for=\"CustomerViewModelList__{index}__Address\">Địa chỉ</label>"
                            + $"<input class=\"md-input validate[required]\" accesskey=\"KeyFocus\" id=\"CustomerViewModelList_{index}__Address\" name=\"CustomerViewModelList[{index}].Address\" type=\"text\" value=\"{item["Address"]?.ToString() ?? ""}\" />"
                            + $"</div>"
                            + $"</div>"
                            + $"<div class=\"uk-grid uk-grid-small\" data-uk-grid-margin>"
                            + $"<div class=\"uk-width-medium-5-10\">"
                            + $"<label for=\"CustomerViewModelList__{index}__UserName\">Tài Khoản</label>"
                            + $"<input class=\"md-input {(typeSale.Equals("2") ? "validate[required]" : "")}\" id=\"CustomerViewModelList_{index}__UserName\" name=\"CustomerViewModelList[{index}].UserName\" type=\"text\" value=\"{(typeSale.Equals("2") ? item["UserName"]?.ToString() ?? "" : "") }\" />"
                            + $"</div>"
                            + $"<div class=\"uk-width-medium-5-10\">"
                            + $"<label for=\"CustomerViewModelList__{index}__GradeId\">Lớp</label>"
                            + ClassListStr(gradeId, index)
                            + $"</div>"
                            + $"</div>"
                            + $"</div>"
                            + $"<div class=\"uk-width-medium-1-10 uk-text-center\">"
                            + $"<a href=\"javascript:void(0);\" name=\"btnRemoveContactLine\"><i class=\"material-icons md-24\">&#xE872;</i></a>"
                            + $"</div>"
                            + $"</div>";
                    index++;
                }
                return Json(new { success = true, html = strHtml });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, html = ex.ToString() }); ;
            }
            
        }
        private string ClassListStr(int gradeId, int index)
        {
            var ddlClassStr = new StringBuilder();
            ddlClassStr.Append("<select id=\"CustomerViewModelList_" + index.ToString() + "__GradeId\" name=\"CustomerViewModelList[" + index.ToString() + "].GradeId\" class=\"md-input label-fixed\">");
            ddlClassStr.Append("<option value=\"\">[ Chọn ]</option>");
            var classList = Mapper.Map<IEnumerable<ClassList>>(_classService.FindAll(ClassType.Class, "SHOW")).ToList();
            foreach (var item in classList)
            {
                ddlClassStr.Append(string.Format("<option {2} value=\"{0}\">{1}</option>", item.Id, item.Name, gradeId == item.Id ? "selected" : ""));
            }
            ddlClassStr.Append("</select>");
            return ddlClassStr.ToString();
        }
        public class UserPhone
        {
            public string UserName { get; set; }
            public string Phone { get; set; }
            public string Email { get; set; }
            public string TypeSale { get; set; }
        }
    }
}