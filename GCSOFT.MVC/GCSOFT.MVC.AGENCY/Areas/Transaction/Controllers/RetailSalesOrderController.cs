﻿using AutoMapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Areas.Agency.Data;
using GCSOFT.MVC.AGENCY.Areas.Transaction.Data;
using GCSOFT.MVC.AGENCY.Helper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.ExeclOfficeEngine;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.StoreProcedure.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Service.Transaction.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.Transaction.Controllers
{
    [CompressResponseAttribute]
    public class RetailSalesOrderController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly ISalesHeaderService _salesHdrService;
        private readonly ISalesLineService _salesLineService;
        private readonly IAgencyService _agencyService;
        private readonly IOptionLineService _optionLineService;
        private readonly ICardLineService _cardLineService;
        private readonly ICardEntryService _cardEntryService;
        private readonly IAgencyCardService _agencyAndCardService;
        private readonly IStoreProcedureService _storeProcService;
        private readonly IAgencyCardService _agencyCardService;
        private readonly ITemplateService _templateService;
        private readonly IMailSetupService _mailSetupService;
        private readonly ICustomerService _customerService;
        private readonly ISalesPriceService _salesPriceService;
        private readonly IClassService _classService;
        private readonly IStudentClassService _studentClassService;
        private readonly IUserInfoService _userInfoService;
        private string sysCategory = "AGEN_RS_ORDER";
        private bool? permission = false;
        private SalesOrderCard salesOrderCard;
        private M_SalesHeader mSalesHeader;
        private List<AgencyAndCard> agencyAndCards;
        private List<CardEntry> cardEntries;
        private SalesType salesType = SalesType.Retail;
        private AgencyType agencyType = AgencyType.Branch;
        private CardLine cardLine = new CardLine();
        private AgencyAndCard agencyAndCard = new AgencyAndCard();
        private List<CardLine> cardLines = new List<CardLine>();
        private UserInfoViewModel userInfo = new UserInfoViewModel();
        private string hasValue;
        #endregion
        public RetailSalesOrderController
            (
                IVuViecService vuViecService
                , ISalesHeaderService salesHdrService
                , ISalesLineService salesLineService
                , IAgencyService agencyService
                , IOptionLineService optionLineService
                , ICardLineService cardLineService
                , ICardEntryService cardEntryService
                , IAgencyCardService agencyAndCardService
                , IStoreProcedureService storeProcService
                , IAgencyCardService agencyCardService
                , ITemplateService templateService
                , IMailSetupService mailSetupService
                , ICustomerService customerService
                , ISalesPriceService salesPriceService
                , IUserInfoService userInfoService
                , IClassService classService
                , IStudentClassService studentClassService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _salesHdrService = salesHdrService;
            _salesLineService = salesLineService;
            _agencyService = agencyService;
            _optionLineService = optionLineService;
            _cardLineService = cardLineService;
            _cardEntryService = cardEntryService;
            _agencyAndCardService = agencyAndCardService;
            _storeProcService = storeProcService;
            _agencyCardService = agencyCardService;
            _templateService = templateService;
            _mailSetupService = mailSetupService;
            _customerService = customerService;
            _salesPriceService = salesPriceService;
            _userInfoService = userInfoService;
            _classService = classService;
            _studentClassService = studentClassService;
        }
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var salesOrderList = Mapper.Map<IEnumerable<SalesOrderList>>(_salesHdrService.FindAllAgency(salesType, agencyType, GetUser().agencyInfo.Id));
            return View(salesOrderList);
        }
        public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var userInfo = GetUser();
            salesOrderCard = new SalesOrderCard();
            salesOrderCard.Code = StringUtil.CheckLetter("RS", _salesHdrService.GetMax(salesType), 5);
            salesOrderCard.DocumentDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.DocumentDate);
            salesOrderCard.AgencyId = userInfo.agencyInfo.Id;
            salesOrderCard.AgencyName = userInfo.agencyInfo.FullName;
            var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("DH", "100"));
            salesOrderCard.StatusId = statusInfo.LineNo;
            salesOrderCard.StatusCode = statusInfo.Code;
            salesOrderCard.StatusName = statusInfo.Name;
            salesOrderCard.CustomerType = agencyType;
            salesOrderCard.LevelList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("LV")).ToSelectListItems("");
            
            salesOrderCard.TypeSaleList = new List<SelectListItem>() {
            new SelectListItem()
                {
                    Text = "Bán Mã Kích Hoạt",
                    Value = "1",
                },
            new SelectListItem()
                {
                    Text = "Đăng Ký Tài Khoản",
                    Value = "2",
                }
            };
            salesOrderCard.TotalCard = 1;
            ViewBag.ClassListStr = ClassListStr(0, 0);
            var customer = new CustomerViewModel();
            customer.ClassList = Mapper.Map<IEnumerable<ClassList>>(_classService.FindAll(ClassType.Class, "SHOW")).ToList().ToSelectListItems(0);
            salesOrderCard.CustomerViewModelList.Add(customer);
            return View(salesOrderCard);
        }
        public ActionResult Edit(string id, int? p)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            salesOrderCard = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(salesType, id));
            if (salesOrderCard == null)
                return HttpNotFound();
            salesOrderCard.DocumentDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.DocumentDate);
            salesOrderCard.PostingDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.PostingDate);
            salesOrderCard.LevelList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("LV")).ToSelectListItems(salesOrderCard.LevelCode);
            salesOrderCard.SalesLines = Mapper.Map<IEnumerable<SalesLine>>(_salesLineService.FindAll(salesType, id));
            salesOrderCard.CustomerViewModelList = Mapper.Map<List<CustomerViewModel>>(_customerService.FindAll(salesOrderCard.Code));
            salesOrderCard.TypeSaleList = new List<SelectListItem>() {
            new SelectListItem()
                {
                    Selected = salesOrderCard.TypeSale.Equals("1")? true : false,
                    Text = "Bán Mã Kích Hoạt",
                    Value = "1",
                },
            new SelectListItem()
                {
                    Selected = salesOrderCard.TypeSale.Equals("2")? true : false,
                    Text = "Đăng Ký Tài Khoản",
                    Value = "2",
                }
            };
            foreach (var item in salesOrderCard.CustomerViewModelList)
            {
                item.ClassList = Mapper.Map<IEnumerable<ClassList>>(_classService.FindAll(ClassType.Class, "SHOW")).ToList().ToSelectListItems(item.GradeId ?? 0);
            }
            return View(salesOrderCard);
        }
        public JsonResult GetLevelInfo(string l)
        {
            
            try
            {
                if (string.IsNullOrEmpty(l))
                    return Json(new { price = 0, durationCode = "", durationName = "" }, JsonRequestBehavior.AllowGet);
                var priceInfo = _salesPriceService.GetBy(l, DateTime.Now);
                return Json(new { price = priceInfo.Price, durationCode = priceInfo.DurationCode, durationName = priceInfo.DurationName }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                    return Json(new { price = 0, durationCode = "", durationName = "" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult Create(SalesOrderCard _salesOrderCard)
        {
            try
            {
                SetData(_salesOrderCard, true);
                hasValue = _salesHdrService.Insert(mSalesHeader);
                hasValue = _customerService.Delete(salesOrderCard.Code);
                cardLines = new List<CardLine>();
                agencyAndCards = new List<AgencyAndCard>();
                var idCard = _cardLineService.GetID() - 1;
                foreach (var customer in salesOrderCard.CustomerViewModelList)
                {
                    idCard += 1;
                    if (_salesOrderCard.TypeSale.Equals("2")) // Đăng ký tài khoản
                        RegisterUser(customer, (decimal)salesOrderCard.Price, idCard);
                    else
                        GetActionCode(customer, (decimal)salesOrderCard.Price, idCard);
                    customer.SerialNumber = cardLine.SerialNumber;
                    cardLines.Add(cardLine);
                    agencyAndCards.Add(agencyAndCard);
                }
                hasValue = _cardLineService.Insert(Mapper.Map<IEnumerable<M_CardLine>>(cardLines));
                hasValue = _agencyAndCardService.Insert(Mapper.Map<IEnumerable<M_AgencyAndCard>>(agencyAndCards));
                hasValue = _customerService.Insert(Mapper.Map<IEnumerable<M_Customer>>(salesOrderCard.CustomerViewModelList));
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                {
                    if (_salesOrderCard.TypeSale.Equals("2")) // Đăng ký tài khoản
                        SendMailRegisterUser(cardLines);
                    else
                        SendMail(cardLines);
                    return RedirectToAction("Edit", new { id = mSalesHeader.Code, msg = "1" });
                }
                    
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        private void SetData(SalesOrderCard _salesOrderCard, bool isCreate)
        {
            var userInfo = GetUser();
            salesOrderCard = _salesOrderCard;
            if (isCreate)
            {
                salesOrderCard.Code = StringUtil.CheckLetter("RS", _salesHdrService.GetMax(salesType), 5);
                salesOrderCard.CreateBy = userInfo.Username;
                salesOrderCard.DateCreate = DateTime.Now;
                salesOrderCard.LastModifyBy = userInfo.Username;
                salesOrderCard.LastModifyDate = salesOrderCard.DateCreate;
            }
            else
            {
                salesOrderCard.LastModifyBy = userInfo.Username;
                salesOrderCard.LastModifyDate = salesOrderCard.DateCreate;
            }
            if (!string.IsNullOrEmpty(salesOrderCard.ReceiptDateStr))
                salesOrderCard.ReceiptDate = DateTime.Parse(StringUtil.ConvertStringToDate(salesOrderCard.ReceiptDateStr));
            else
                salesOrderCard.ReceiptDate = null;
            if (!string.IsNullOrEmpty(salesOrderCard.PostingDateStr))
                salesOrderCard.PostingDate = DateTime.Parse(StringUtil.ConvertStringToDate(salesOrderCard.PostingDateStr));
            else
                salesOrderCard.PostingDate = null;

            if (!string.IsNullOrEmpty(salesOrderCard.DocumentDateStr))
                salesOrderCard.DocumentDate = DateTime.Parse(StringUtil.ConvertStringToDate(salesOrderCard.DocumentDateStr));
            else
                salesOrderCard.DocumentDate = null;

            var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("DH", "200")); //Đã Giao Hàng
            salesOrderCard.StatusId = statusInfo.LineNo;
            salesOrderCard.StatusCode = statusInfo.Code;
            salesOrderCard.StatusName = statusInfo.Name;
            salesOrderCard.PostingDate = DateTime.Now;

            salesOrderCard.SalesType = salesType;
            salesOrderCard.CustomerType = agencyType;
            var LevelInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("LV", salesOrderCard.LevelCode)) ?? new VM_OptionLine();
            salesOrderCard.LevelId = LevelInfo.LineNo;
            salesOrderCard.LevelCode = LevelInfo.Code;
            salesOrderCard.LevelName = LevelInfo.Name;
            var durationInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("CARDINFO", salesOrderCard.DurationCode)) ?? new VM_OptionLine();
            salesOrderCard.DurationId = durationInfo.LineNo;
            salesOrderCard.DurationCode = durationInfo.Code;
            salesOrderCard.DurationName = durationInfo.Name;
            foreach (var item in salesOrderCard.CustomerViewModelList)
            {
                item.Code = salesOrderCard.Code;
                item.LotNumber = salesOrderCard.Code;
                item.IsEmail = StringUtil.Validate(item.Email) ? "Đúng" : "Sai Email";
                item.RegisterDate = DateTime.Now;
                item.Owner = GetUser().agencyInfo.FullName;
                item.UserOwner = GetUser().Username;
                item.DateCard = DateTime.Now;
                item.LevelId = salesOrderCard.LevelId;
                item.LevelCode = salesOrderCard.LevelCode;
                item.LevelName = salesOrderCard.LevelName;
                item.DurationId = salesOrderCard.DurationId;
                item.DurationCode = salesOrderCard.DurationCode;
                item.DurationName = salesOrderCard.DurationName;
            }
            mSalesHeader = Mapper.Map<M_SalesHeader>(salesOrderCard);
        }
        private string GetActionCode(CustomerViewModel customer, decimal price, long idCard)
        {
            var _userInfo = GetUser();
            var hasCardNo = true;
            //-- Build Ma Kich Hoat
            var _cardNo = string.Empty;
            do
            {
                _cardNo = CryptorEngine.Encrypt(GetBuildCardNo().ToString(), true, "TTLSTNHD@CARD");
                hasCardNo = cardLines.Where(d => d.CardNo == _cardNo).Any();
            } while (_cardLineService.CheckExist(_cardNo) || hasCardNo);
            //-- Build Ma Kich Hoat +
            var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("CARD", "450"));
            var LotNumber = customer.LotNumber;
            string prefex = "S";
            if (customer.LevelCode.Equals("300"))
                prefex = "L";
            if (customer.LevelCode.Equals("200"))
                prefex = "C";
            cardLine = new CardLine()
            {
                Id = idCard,
                LotNumber = LotNumber,
                SerialNumber = StringUtil.GetProductID(prefex, (idCard - 1).ToString(), 9),
                CardNo = _cardNo,
                Price = price,
                UserName = _userInfo.Username,
                UserFullName = _userInfo.FullName,
                DateCreate = DateTime.Now,
                FulName = customer.FullName,
                PhoneNo = customer.PhoneNo,
                VerifyInfo = customer.Email,
                UserType = UserType.TTL,
                UserNameActive = _userInfo.Username,
                UserActiveFullName = _userInfo.FullName,
                LastDateUpdate = DateTime.Now,
                StatusId = statusInfo.LineNo,
                StatusCode = statusInfo.Code,
                StatusName = statusInfo.Name,
                LastStatusId = statusInfo.LineNo,
                LastStatusCode = statusInfo.Code,
                LastStatusName = statusInfo.Name,

                DurationId = customer.DurationId,
                DurationCode = customer.DurationCode,
                DurationName = customer.DurationName,

                ReasonId = 45,
                ReasonCode = "BH",
                ReasonName = "Bán Hàng",

                LevelId = customer.LevelId,
                LevelCode = customer.LevelCode,
                LevelName = customer.LevelName,
                DateBuy = DateTime.Now
            };
            agencyAndCard = new AgencyAndCard()
            {
                AgencyId = salesOrderCard.AgencyId ?? 0,
                Id = cardLine.Id,
                LotNumber = cardLine.LotNumber,
                SerialNumber = cardLine.SerialNumber,
                CardNo = cardLine.CardNo,
                Price = cardLine.Price,
                UserName = cardLine.UserName,
                UserFullName = cardLine.UserFullName,
                DateCreate = cardLine.DateCreate,
                UserType = cardLine.UserType,
                UserNameActive = cardLine.UserNameActive,
                UserActiveFullName = cardLine.UserActiveFullName,
                LastDateUpdate = cardLine.LastDateUpdate,
                StatusId = statusInfo.LineNo,
                StatusCode = statusInfo.Code,
                StatusName = statusInfo.Name,
                DurationId = cardLine.DurationId,
                DurationCode = cardLine.DurationCode,
                DurationName = cardLine.DurationName,
                ReasonId = cardLine.ReasonId,
                ReasonCode = cardLine.ReasonCode,
                ReasonName = cardLine.ReasonName,
                LevelId = cardLine.LevelId,
                LevelCode = cardLine.LevelCode,
                LevelName = cardLine.LevelName,
                FulName = customer.FullName,
                PhoneNo = customer.PhoneNo,
                VerifyInfo = customer.Email
            };
            return null;
        }
        private string RegisterUser(CustomerViewModel customer, decimal price, long idCard)
        {
            var _userInfo = GetUser();
            var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("CARD", "500"));
            var LotNumber = customer.LotNumber;
            string prefex = "S";
            if (customer.LevelCode.Equals("300"))
                prefex = "L";
            if (customer.LevelCode.Equals("200"))
                prefex = "C";
            cardLine = new CardLine()
            {
                Id = idCard,
                LotNumber = LotNumber,
                SerialNumber = StringUtil.GetProductID(prefex, (idCard - 1).ToString(), 9),
                Price = price,
                UserName = _userInfo.Username,
                UserFullName = _userInfo.FullName,
                DateCreate = DateTime.Now,
                FulName = customer.FullName,
                PhoneNo = customer.PhoneNo,
                VerifyInfo = customer.Email,
                UserType = UserType.TTL,
                UserNameActive = _userInfo.Username,
                UserActiveFullName = _userInfo.FullName,
                LastDateUpdate = DateTime.Now,
                StatusId = statusInfo.LineNo,
                StatusCode = statusInfo.Code,
                StatusName = statusInfo.Name,
                LastStatusId = statusInfo.LineNo,
                LastStatusCode = statusInfo.Code,
                LastStatusName = statusInfo.Name,
                DurationId = customer.DurationId,
                DurationCode = customer.DurationCode,
                DurationName = customer.DurationName,

                ReasonId = 45,
                ReasonCode = "BH",
                ReasonName = "Bán Hàng",

                LevelId = customer.LevelId,
                LevelCode = customer.LevelCode,
                LevelName = customer.LevelName,
                DateBuy = DateTime.Now,
                ApplyDate = DateTime.Now,
                ApplyUser = customer.UserName,
                IsActive = true
            };
            agencyAndCard = new AgencyAndCard()
            {
                AgencyId = salesOrderCard.AgencyId ?? 0,
                Id = cardLine.Id,
                LotNumber = cardLine.LotNumber,
                SerialNumber = cardLine.SerialNumber,
                CardNo = cardLine.CardNo,
                Price = cardLine.Price,
                UserName = cardLine.UserName,
                UserFullName = cardLine.UserFullName,
                DateCreate = cardLine.DateCreate,
                UserType = cardLine.UserType,
                UserNameActive = cardLine.UserNameActive,
                UserActiveFullName = cardLine.UserActiveFullName,
                LastDateUpdate = cardLine.LastDateUpdate,
                StatusId = statusInfo.LineNo,
                StatusCode = statusInfo.Code,
                StatusName = statusInfo.Name,
                DurationId = cardLine.DurationId,
                DurationCode = cardLine.DurationCode,
                DurationName = cardLine.DurationName,
                ReasonId = cardLine.ReasonId,
                ReasonCode = cardLine.ReasonCode,
                ReasonName = cardLine.ReasonName,
                LevelId = cardLine.LevelId,
                LevelCode = cardLine.LevelCode,
                LevelName = cardLine.LevelName,
                FulName = customer.FullName,
                PhoneNo = customer.PhoneNo,
                VerifyInfo = customer.Email,
            };
            //-- Insert user info
            userInfo = new UserInfoViewModel();
            var randomNumber = new RandomNumberGenerator();
            userInfo.userName = customer.UserName;
            userInfo.fullName = customer.FullName;
            userInfo.Phone = customer.PhoneNo;
            userInfo.Email = customer.Email;
            userInfo.Address = customer.Address;
            userInfo.ClassId = customer.GradeId ?? 4;
            userInfo.Password = randomNumber.RandomNumber(100000, 999999).ToString();
            userInfo.ActivePhone = true;
            userInfo.HasClass = true;
            userInfo.DateActive = DateTime.Now;
            userInfo.STNHDType = STNHDType.Active;
            userInfo.Serinumber = cardLine.SerialNumber;
            int noOfDuration = 0;
            try { noOfDuration = int.Parse(StringUtil.RebuildPhoneNo(cardLine.DurationCode)); }
            catch { noOfDuration = 0; }
            if (cardLine.DurationCode.EndsWith("Y"))
                userInfo.DateExpired = DateTime.Now.AddYears(noOfDuration);
            else if (cardLine.DurationCode.EndsWith("W"))
            {
                int noOfWeeks = noOfDuration * 7;
                userInfo.DateExpired = DateTime.Now.AddDays(noOfWeeks);
            }
            else if (cardLine.DurationCode.EndsWith("M"))
                userInfo.DateExpired = DateTime.Now.AddMonths(noOfDuration);
            else if (cardLine.DurationCode.EndsWith("D"))
                userInfo.DateExpired = DateTime.Now.AddDays(noOfDuration);
            else
                userInfo.DateExpired = null;
            _userInfoService.Insert(Mapper.Map<M_UserInfo>(userInfo));
            if (!salesOrderCard.LevelCode.Equals("100"))
            {
                //-- By Level
                bool isTieuHoc = false; bool isTHCS = false; bool isTHPT = false;
                var idLevelTieuHocs = new List<int>(); var idLevelTHCS = new List<int>(); var idLevelTHPT = new List<int>();
                if (salesOrderCard.LevelCode.Equals("200"))
                {
                    idLevelTieuHocs.Add(4); idLevelTieuHocs.Add(5); idLevelTieuHocs.Add(6); idLevelTieuHocs.Add(7); idLevelTieuHocs.Add(8);
                    idLevelTHCS.Add(9); idLevelTHCS.Add(13); idLevelTHCS.Add(14); idLevelTHCS.Add(15);
                    idLevelTHPT.Add(16); idLevelTHPT.Add(17); idLevelTHPT.Add(18);

                    if (idLevelTieuHocs.Contains(userInfo.ClassId))
                        isTieuHoc = true;
                    if (idLevelTHCS.Contains(userInfo.ClassId))
                        isTHCS = true;
                    if (idLevelTHPT.Contains(userInfo.ClassId))
                        isTHPT = true;
                }
                //-- By Level +
                var _studentClasses = new List<StudentClassViewModel>();
                var _studentClass = new StudentClassViewModel();
                var classes = Mapper.Map<IEnumerable<ClassList>>(_classService.FindAll(ClassType.Class, "SHOW")).ToList();
                foreach (var item in classes)
                {
                    _studentClass = new StudentClassViewModel();
                    _studentClass.userName = userInfo.userName;
                    _studentClass.FromDate = DateTime.Now;
                    _studentClass.ToDate = userInfo.DateExpired ?? DateTime.Now;
                    _studentClass.ClassId = item.Id;
                    _studentClass.ClassName = item.Name;
                    _studentClass.IsActive = false;
                    if (salesOrderCard.LevelCode.Equals("200"))
                    {
                        if (isTieuHoc && idLevelTieuHocs.Contains(item.Id))
                            _studentClass.IsActive = true;
                        if (isTHCS && idLevelTHCS.Contains(item.Id))
                            _studentClass.IsActive = true;
                        if (isTHPT && idLevelTHPT.Contains(item.Id))
                            _studentClass.IsActive = true;
                    }
                    else
                    {
                        if (item.Id == userInfo.ClassId)
                        {
                            if (userInfo.DateExpired != null)
                                _studentClass.ToDate = userInfo.DateExpired ?? DateTime.Now;
                            _studentClass.IsActive = true;
                        }
                    }
                    _studentClasses.Add(_studentClass);
                }
                _studentClassService.Delete(userInfo.userName);
                _studentClassService.Insert(Mapper.Map<IEnumerable<M_StudentClass>>(_studentClasses));
            }
            //-- Insert user info +
            return null;
        }
        public string SendMail(List<CardLine> cardLines)
        {
            try
            {
                foreach (var item in cardLines)
                {
                    //-- Gửi qua email
                    var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
                    var mailTemplate = Mapper.Map<Template>(_templateService.Get("EMAILACTIVECODE"));
                    var mailTitle = mailTemplate.Title;
                    var mailContent = mailTemplate.Content.Replace("{{FULLNAME}}", item.FulName);
                    mailContent = mailContent.Replace("{{ACTIVECODE}}", CryptorEngine.Decrypt(item.CardNo, true, "TTLSTNHD@CARD"));
                    var emailTos = new List<string>();
                    emailTos.Add(item.VerifyInfo);
                    var _mailHelper = new EmailHelper()
                    {
                        EmailTos = emailTos,
                        DisplayName = emailInfo.DisplayName,
                        Title = mailTitle,
                        Body = mailContent,
                        MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
                    };
                    string hasValue = _mailHelper.SendEmailsThread();

                    if (!string.IsNullOrEmpty(item.PhoneNo))
                    {
                        var newPhoneNo = item.PhoneNo.Trim();
                        var firstNumber = int.Parse(newPhoneNo[0].ToString());
                        if (firstNumber != 0)
                        {
                            newPhoneNo = "0" + newPhoneNo;
                        }
                        //-- Gui qua SMS
                        var smsTemplate = Mapper.Map<Template>(_templateService.Get("SMS_MAKICHHOAT"));
                        var smsHelper = new SMSHelper()
                        {
                            PhoneNo = newPhoneNo,
                            Content = StringUtil.StripTagsRegexCompiled(smsTemplate.Content).Replace("{{NAME}}", "vao 5 Phut Thuoc Bai").Replace("{{MAKICHHOAT}}", CryptorEngine.Decrypt(item.CardNo, true, "TTLSTNHD@CARD")).Replace("{{LINK}}", "https://sieutrinhohocduong.com/student")
                        };
                        if (smsHelper.IsMobiphone())
                        {
                            smsTemplate = Mapper.Map<Template>(_templateService.Get("MOBI_TKKH_KH"));
                            smsHelper.Content = string.Format(smsTemplate.SMSContent, "5 Phut Thuoc Bai", CryptorEngine.Decrypt(item.CardNo, true, "TTLSTNHD@CARD"));
                            hasValue = smsHelper.SendMobiphone();
                        }
                        else
                            hasValue = smsHelper.Send();
                        //-- Gui qua SMS +
                    }
                }
                //-- Gửi qua email +
                return string.Empty;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public string SendMailRegisterUser(List<CardLine> cardLines)
        {
            try
            {
                var emailInfo = new MailSetup();
                var mailTemplate = new Template();
                foreach (var item in cardLines)
                {
                    userInfo = Mapper.Map<UserInfoViewModel>(_userInfoService.GetBy(item.ApplyUser));
                    emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
                    mailTemplate = Mapper.Map<Template>(_templateService.Get("EMAIL_REGISTER"));
                    //-- Gửi qua email
                    var mailTitle = mailTemplate.Title;
                    var mailContent = mailTemplate.Content.Replace("{{FULLNAME}}", userInfo.fullName);
                    mailContent = mailContent.Replace("{{USERNAME}}", userInfo.userName);
                    mailContent = mailContent.Replace("{{PASSWORD}}", userInfo.Password);
                    var emailTos = new List<string>();
                    emailTos.Add(userInfo.Email);
                    var _mailHelper = new EmailHelper()
                    {
                        EmailTos = emailTos,
                        DisplayName = emailInfo.DisplayName,
                        Title = mailTitle,
                        Body = mailContent,
                        MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
                    };
                    hasValue = _mailHelper.SendEmailsThread();

                    if (!string.IsNullOrEmpty(item.PhoneNo))
                    {
                        var newPhoneNo = item.PhoneNo.Trim();
                        var firstNumber = int.Parse(newPhoneNo[0].ToString());
                        if (firstNumber != 0)
                        {
                            newPhoneNo = "0" + newPhoneNo;
                        }
                        //-- Gui qua SMS
                        var smsHelper = new SMSHelper()
                        {
                            PhoneNo = newPhoneNo,
                            Content = string.Format(mailTemplate.SMSContent, userInfo.userName, userInfo.Password)
                        };
                        if (smsHelper.IsMobiphone())
                            hasValue = smsHelper.SendMobiphone();
                        else
                            hasValue = smsHelper.Send();
                        //-- Gui qua SMS +
                    }
                }
                //-- Gửi qua email +
                return string.Empty;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        private StringBuilder GetBuildCardNo()
        {
            var builder = new StringBuilder();
            var randomNumber = new RandomNumberGenerator();
            builder.Append(randomNumber.RandomString(4, true).ToUpper());
            builder.Append(randomNumber.RandomNumber(1000, 9999));
            builder.Append(randomNumber.RandomString(2, false).ToUpper());

            return builder;
        }
        /// <summary>
        ///     DownloadExcel
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [26/08/2021]
        /// </history>
        public ActionResult DownloadExcel()
        {
            var res = File(HttpContext.Server.MapPath("/Areas/Transaction/Report/RetailSalesMau2.xlsx"), OfficeContentType.XLSX, "RetailSalesMau2.xlsx");
            return res;
        }

        /// <summary>
        ///     RenderHtml
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [26/08/2021]
        /// </history>
        [HttpPost]
        public JsonResult RenderHtml(string typeSale)
        {
            var gradeList = Mapper.Map<IEnumerable<ClassList>>(_classService.FindAll(ClassType.Class, "SHOW")).ToList();
            var gradeId = 0;
            var PostedFile = Request.Files[0];
            string filename = PostedFile.FileName;
            string targetpath = Server.MapPath("/Areas/Transaction/Files/");
            if (!Directory.Exists(targetpath))
                Directory.CreateDirectory(targetpath);
            PostedFile.SaveAs(targetpath + filename);
            string pathToExcelFile = targetpath + filename;
            var connectionString = "";
            if (filename.EndsWith(".xls"))
            {
                connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=Excel 8.0;", pathToExcelFile);
            }
            else if (filename.EndsWith(".xlsx"))
            {
                connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";", pathToExcelFile);
            }
            var adapter = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", connectionString);
            var ds = new DataSet();
            adapter.Fill(ds, "ExcelTable");
            DataTable dtable = ds.Tables["ExcelTable"];
            string strHtml = string.Empty;
            int index = 0;
            foreach (DataRow item in dtable.Rows)
            {
                gradeId = 0;
                if (!string.IsNullOrEmpty(item["LopHocSTNHD"]?.ToString()))
                {
                    try { gradeId = gradeList.Where(d => d.SortOrder == int.Parse(item["LopHocSTNHD"].ToString())).Select(d => d.Id).FirstOrDefault(); }
                    catch { gradeId = 0; }
                }
                strHtml += $"<div class=\"uk-grid tr\" data-uk-grid-margin>"
                        + $"<div class=\"uk-width-medium-1-10 uk-text-center\">"
                        + $"<p class=\"uk-text-large\">{index + 1}</p>"
                        + $"</div>"
                        + $"<div class=\"uk-width-medium-8-10\">"
                        + $"<div class=\"uk-grid uk-grid-small\" data-uk-grid-margin>"
                        + $"<div class=\"uk-width-medium-2-10\">"
                        + $"<label for=\"CustomerViewModelList__{index}__FullName\">Họ tên người mua</label>"
                        + $"<input class=\"md-input validate[required]\" accesskey=\"KeyFocus\" id=\"CustomerViewModelList_{index}__FullName\" name=\"CustomerViewModelList[{index}].FullName\" type=\"text\" value=\"{item["HoVaTenCon"]?.ToString() ?? ""}\" />"
                        + $"</div>"
                        + $"<div class=\"uk-width-medium-2-10\">"
                        + $"<label for=\"CustomerViewModelList__{index}__PhoneNo\">Số điện thoại</label>"
                        + $"<input class=\"md-input validate[required]\" accesskey=\"KeyFocus\" id=\"CustomerViewModelList_{index}__PhoneNo\" name=\"CustomerViewModelList[{index}].PhoneNo\" type=\"text\" value=\"{item["SoDienThoai"]?.ToString() ?? ""}\" />"
                        + $"</div>"
                        + $"<div class=\"uk-width-medium-2-10\">"
                        + $"<label for=\"CustomerViewModelList__{index}__Email\">Email</label>"
                        + $"<input class=\"md-input validate[required]\" accesskey=\"KeyFocus\" id=\"CustomerViewModelList_{index}__Email\" name=\"CustomerViewModelList[{index}].Email\" type=\"text\" value=\"{item["Email"]?.ToString() ?? ""}\" />"
                        + $"</div>"
                        + $"<div class=\"uk-width-medium-4-10\">"
                        + $"<label for=\"CustomerViewModelList__{index}__Address\">Địa chỉ</label>"
                        + $"<input class=\"md-input validate[required]\" accesskey=\"KeyFocus\" id=\"CustomerViewModelList_{index}__Address\" name=\"CustomerViewModelList[{index}].Address\" type=\"text\" value=\"{item["DiaChi"]?.ToString() ?? ""}\" />"
                        + $"</div>"
                        + $"</div>"
                        + $"<div class=\"uk-grid uk-grid-small\" data-uk-grid-margin>"
                        + $"<div class=\"uk-width-medium-5-10\">"
                        + $"<label for=\"CustomerViewModelList__{index}__UserName\">Tài Khoản</label>"
                        + $"<input class=\"md-input {(typeSale.Equals("2") ? "validate[required]" : "")}\" id=\"CustomerViewModelList_{index}__UserName\" name=\"CustomerViewModelList[{index}].UserName\" type=\"text\" value=\"{(typeSale.Equals("2") ? item["TenDangNhap"]?.ToString() ?? "" : "") }\" />"
                        + $"</div>"
                        + $"<div class=\"uk-width-medium-5-10\">"
                        + $"<label for=\"CustomerViewModelList__{index}__GradeId\">Lớp</label>"
                        + ClassListStr(gradeId, index)
                        + $"</div>"
                        + $"</div>"
                        + $"</div>"
                        + $"<div class=\"uk-width-medium-1-10 uk-text-center\">"
                        + $"<a href=\"javascript:void(0);\" name=\"btnRemoveContactLine\"><i class=\"material-icons md-24\">&#xE872;</i></a>"
                        + $"</div>"
                        + $"</div>";
                index++;
            }
            return Json(new { success = true, html = strHtml });
        }
        private string ClassListStr(int gradeId, int index)
        {
            var ddlClassStr = new StringBuilder();
            ddlClassStr.Append("<select id=\"CustomerViewModelList_" + index.ToString() + "__GradeId\" name=\"CustomerViewModelList[" + index.ToString() + "].GradeId\" class=\"md-input label-fixed\">");
            ddlClassStr.Append("<option value=\"\">[ Chọn ]</option>");
            var classList = Mapper.Map<IEnumerable<ClassList>>(_classService.FindAll(ClassType.Class, "SHOW")).ToList();
            foreach (var item in classList)
            {
                ddlClassStr.Append(string.Format("<option {2} value=\"{0}\">{1}</option>", item.Id, item.Name, gradeId == item.Id ? "selected" : ""));
            }
            ddlClassStr.Append("</select>");
            return ddlClassStr.ToString();
        }
        public JsonResult CheckUserNameAndPhoneActive(List<UserPhone> userNames)
        {
            try
            {
                bool status = false;
                string res = string.Empty;
                if (userNames[0].TypeSale == null)
                    return Json(new { status = status, res = res }, JsonRequestBehavior.AllowGet);
                if (userNames[0].TypeSale.Equals("1")) // Bán kích hoạt
                    return Json(new { status = status, res = res }, JsonRequestBehavior.AllowGet);
                int currentPhone = 0;
                foreach (var userName in userNames)
                {
                    if (string.IsNullOrEmpty(userName.UserName))
                    {
                        status = true;
                        res += $"- Chưa nhập đủ tên đăng nhập<BR />";
                    }
                    if (string.IsNullOrEmpty(userName.Phone))
                    {
                        status = true;
                        res += $"- Chưa nhập đủ số điện thoại<BR />";
                    }
                    if (status)
                        break;
                    if (userNames.Where(d => d.UserName.Equals(userName.UserName)).Count() > 1)
                    {
                        status = true;
                        res += $"- Tài Khoản: {userName.UserName} Trùng nhau trong form đăng ký<BR />";
                    }
                    // check user name
                    var user = _userInfoService.GetBy(userName.UserName);
                    if (user != null && user.Id != 0)
                    {
                        status = true;
                        res += $"- Tài Khoản: {userName.UserName} Đã tồn tại!<BR />";
                    }
                    currentPhone = userNames.Where(d => d.Phone == userName.Phone.Trim()).Count();
                    var userPhones = _userInfoService.NoOfMemberResiger(userName.Phone.Trim()) + currentPhone;
                    if (userPhones > 5)
                    {
                        status = true;
                        res += $"- Số Điện Thoại: {userName.Phone} đã được đăng ký hơn 5 Tài Khoản, vui lòng chọn số điện thoại khác<BR />";
                    }
                    if (status)
                        break;
                }
                return Json(new { status = status, res = res }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = false, res = "Có lỗi trong quá trình xử lý. Vui lòng kiểm tra lại!" }, JsonRequestBehavior.AllowGet);
            }

        }
        public class UserPhone
        {
            public string UserName { get; set; }
            public string Phone { get; set; }
            public string TypeSale { get; set; }
        }
    }
}