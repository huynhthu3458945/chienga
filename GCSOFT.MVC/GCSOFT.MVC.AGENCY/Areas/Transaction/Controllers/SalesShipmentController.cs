﻿using AutoMapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Areas.Agency.Data;
using GCSOFT.MVC.AGENCY.Areas.Transaction.Data;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.StoreProcedure.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Service.Transaction.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.Transaction.Controllers
{
    [CompressResponseAttribute]
    public class SalesShipmentController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly ISalesHeaderService _salesHdrService;
        private readonly ISalesLineService _salesLineService;
        private readonly IAgencyService _agencyService;
        private readonly IOptionLineService _optionLineService;
        private readonly ICardLineService _cardLineService;
        private readonly ICardEntryService _cardEntryService;
        private readonly IAgencyCardService _agencyCardService;
        private readonly IStoreProcedureService _storeProcService;
        private string sysCategory = "RECEIPT";
        private bool? permission = false;
        private SalesOrderCard salesOrderCard;
        private SalesOrderCard SalesOrderInfo;
        private SalesOrderCard SalesShipmentInfo;
        private M_SalesHeader mSalesHeader;
        private List<AgencyAndCard> agencyAndCards;
        private List<CardEntry> cardEntries;
        private List<CardLine> cardLines;
        private SalesOrderCard salesOrderSource;
        private SalesType salesType = SalesType.SalesShipment;
        private string hasValue;

        #endregion
        public SalesShipmentController
            (
                IVuViecService vuViecService
                , ISalesHeaderService salesHdrService
                , ISalesLineService salesLineService
                , IAgencyService agencyService
                , IOptionLineService optionLineService
                , ICardLineService cardLineService
                , ICardEntryService cardEntryService
                , IAgencyCardService agencyCardService
                , IStoreProcedureService storeProcService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _salesHdrService = salesHdrService;
            _salesLineService = salesLineService;
            _agencyService = agencyService;
            _optionLineService = optionLineService;
            _cardLineService = cardLineService;
            _cardEntryService = cardEntryService;
            _agencyCardService = agencyCardService;
            _storeProcService = storeProcService;
        }
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            try
            {
                var salesShipmentPage = new SalesShipmentPage();
                salesShipmentPage.SalesOrderList = Mapper.Map<IEnumerable<SalesOrderList>>(_salesHdrService.FindAll(salesType, GetUser().agencyInfo.Id));
                return View(salesShipmentPage);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
            
        }
        public ActionResult Details(string id, int? p)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            salesOrderCard = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(salesType, id, GetUser().agencyInfo.Id));
            if (salesOrderCard == null)
                return HttpNotFound();
            salesOrderCard.DocumentDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.DocumentDate);
            salesOrderCard.ReceiptDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.ReceiptDate);
            //-- Paging
            int pageString = p ?? 0;
            int page = pageString == 0 ? 1 : pageString;
            var totalRecord = Mapper.Map<IEnumerable<SalesLine>>(_salesLineService.FindAll(salesType, id)).Count();
            salesOrderCard.paging = new Paging(totalRecord, page);
            salesOrderCard.SalesLines = Mapper.Map<IEnumerable<SalesLine>>(_salesLineService.FindAll(salesType, id, salesOrderCard.paging.start, salesOrderCard.paging.offset));
            //-- Paging +
            return View(salesOrderCard);
        }
        [HttpPost]
        public ActionResult ChangeStatus(SalesOrderCard _salesOrderCard)
        {
            try
            {
                #region -- Role user --
                permission = GetPermission(sysCategory, Authorities.Edit);
                if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
                if (!permission.Value) return View("AccessDenied");
                #endregion
                SetData(_salesOrderCard);
                hasValue = _salesHdrService.Update(Mapper.Map<M_SalesHeader>(SalesOrderInfo));
                hasValue = _salesHdrService.Update(Mapper.Map<M_SalesHeader>(SalesShipmentInfo));
                hasValue = _vuViecService.Commit();
                if (!string.IsNullOrEmpty(hasValue))
                {
                    ViewBag.Error = hasValue;
                    return View("Error");
                }
                var userInfo = GetUser() ?? new UserLoginVMs();
                var sprocResult = new ProcResult();
                if (string.IsNullOrEmpty(SalesShipmentInfo.AgencyName)) // Đây là đại lý của TTL
                {
                    sprocResult = _storeProcService.SP_AgencyAcceptOrder((int)SalesShipmentInfo.SalesType, SalesShipmentInfo.Code, SalesShipmentInfo.LotNumber, userInfo.Username ?? SalesShipmentInfo.CustomerPhoneNo, userInfo.FullName ?? SalesShipmentInfo.CustomerName, (int)SalesShipmentInfo.FromSoure, SalesShipmentInfo.SourceNo);
                }
                else // Chạy cho Đại Lý Con của Đại Lý Nhận hàng
                {
                    string statusCode = "400";
                    if (SalesShipmentInfo.CustomerType == AgencyType.Partner)
                        statusCode = "600";
                    sprocResult = _storeProcService.SP_SubAgencyAcceptOrder((int)SalesShipmentInfo.SalesType, SalesShipmentInfo.Code, userInfo.Username ?? SalesShipmentInfo.CustomerPhoneNo, userInfo.FullName ?? SalesShipmentInfo.CustomerName, (int)SalesShipmentInfo.FromSoure, SalesShipmentInfo.SourceNo, statusCode);
                }
                
                if (string.IsNullOrEmpty(sprocResult.HasValue))
                    return Json("", JsonRequestBehavior.AllowGet);
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        private void SetData(SalesOrderCard _salesOrderCard)
        {
            SalesShipmentInfo = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(SalesType.SalesShipment, _salesOrderCard.Code));
            var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("DH", "300")); //Đã Nhận Hàng
            SalesShipmentInfo.StatusId = statusInfo.LineNo;
            SalesShipmentInfo.StatusCode = statusInfo.Code;
            SalesShipmentInfo.StatusName = statusInfo.Name;
            SalesShipmentInfo.ReceiptDate = DateTime.Now;
            SalesShipmentInfo.Remark = _salesOrderCard.Remark;
            //-- Còn Sales Shipment Source
            SalesOrderInfo = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(_salesOrderCard.FromSoure, _salesOrderCard.SourceNo));
            SalesOrderInfo.PostingDate = SalesShipmentInfo.ReceiptDate;
            SalesOrderInfo.ReceiptDate = SalesShipmentInfo.ReceiptDate;
            //var cardLine = new CardLine();
            //cardLines = new List<CardLine>();

            //var cardEntry = new CardEntry();
            //cardEntries = new List<CardEntry>();

            //var agencyAndCard = new AgencyAndCard();
            //agencyAndCards = new List<AgencyAndCard>();

            //var idCardEntry = _cardEntryService.GetID() - 1;
            //statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("CARD", "100")); //Thẻ mới đối với Đại Lý.
            //var statusTTL = Mapper.Map<VM_OptionLine>(_optionLineService.Get("CARD", "400")); //Đã Bán
            //var userInfo = GetUser();

            //var _salesLines = Mapper.Map<IEnumerable<SalesLine>>(_salesLineService.FindAll(SalesType.SalesShipment, SalesShipmentInfo.Code));
            //_salesLines.ToList().ForEach(item => {
            //    cardLine = Mapper.Map<CardLine>(_cardLineService.GetBySerialNumber(item.SerialNumber));
            //    cardLine.UserType = UserType.Agency;
            //    cardLine.UserNameActive = userInfo.Username;
            //    cardLine.UserActiveFullName = userInfo.FullName;

            //    cardLine.LastDateUpdate = SalesShipmentInfo.ReceiptDate ?? DateTime.Now;

            //    cardLine.StatusId = statusTTL.LineNo;
            //    cardLine.StatusCode = statusTTL.Code;
            //    cardLine.StatusName = statusTTL.Name;

            //    cardLine.LastStatusId = statusInfo.LineNo;
            //    cardLine.LastStatusCode = statusInfo.Code;
            //    cardLine.LastStatusName = statusInfo.Name;

            //    cardLines.Add(cardLine);

            //    idCardEntry += 1;
            //    cardEntry = new CardEntry()
            //    {
            //        //Id = idCardEntry,
            //        LotNumber = cardLine.LotNumber,
            //        CardLineId = cardLine.Id,
            //        SerialNumber = cardLine.SerialNumber,
            //        CardNo = cardLine.CardNo,
            //        Price = cardLine.Price,
            //        UserName = cardLine.UserName,
            //        UserFullName = cardLine.UserFullName,
            //        DateCreate = cardLine.DateCreate,
            //        UserType = UserType.Agency,
            //        UserNameActive = cardLine.UserNameActive,
            //        UserActiveFullName = cardLine.UserActiveFullName,
            //        Date = cardLine.LastDateUpdate,
            //        StatusId = cardLine.StatusId,
            //        StatusCode = cardLine.StatusCode,
            //        StatusName = cardLine.StatusName,
            //        Source = CardSource.SalesShipment,
            //        SourceNo = SalesShipmentInfo.Code,
            //        AgencyId = SalesShipmentInfo.CustomerId,
            //        AgencyName = SalesShipmentInfo.CustomerName
            //    };
            //    //idCardEntry += 1;
            //    //cardEntry = new CardEntry()
            //    //{
            //    //    Id = idCardEntry,
            //    //    LotNumber = cardLine.LotNumber,
            //    //    CardLineId = cardLine.Id,
            //    //    SerialNumber = cardLine.SerialNumber,
            //    //    CardNo = cardLine.CardNo,
            //    //    Price = cardLine.Price,
            //    //    UserName = cardLine.UserName,
            //    //    UserFullName = cardLine.UserFullName,
            //    //    DateCreate = cardLine.DateCreate,
            //    //    UserType = UserType.Agency,
            //    //    UserNameActive = cardLine.UserNameActive,
            //    //    UserActiveFullName = cardLine.UserActiveFullName,
            //    //    Date = cardLine.LastDateUpdate,
            //    //    StatusId = cardLine.LastStatusId,
            //    //    StatusCode = cardLine.LastStatusCode,
            //    //    StatusName = cardLine.LastStatusName,
            //    //    Source = CardSource.SalesShipment,
            //    //    SourceNo = SalesShipmentInfo.Code,
            //    //    AgencyId = SalesShipmentInfo.CustomerId,
            //    //    AgencyName = SalesShipmentInfo.CustomerName
            //    //};
            //    //cardEntries.Add(cardEntry);

            //    agencyAndCard = new AgencyAndCard() { 
            //        AgencyId =SalesShipmentInfo.CustomerId,
            //        Id = cardLine.Id,
            //        LotNumber=cardLine.LotNumber,
            //        SerialNumber = cardLine.SerialNumber,
            //        CardNo = cardLine.CardNo,
            //        Price = cardLine.Price,
            //        UserName=cardLine.UserName,
            //        UserFullName = cardLine.UserFullName,
            //        DateCreate = cardLine.DateCreate,
            //        UserType = cardLine.UserType,
            //        UserNameActive=cardLine.UserNameActive,
            //        UserActiveFullName=cardLine.UserActiveFullName,
            //        LastDateUpdate=cardLine.LastDateUpdate,
            //        StatusId= statusInfo.LineNo,
            //        StatusCode= statusInfo.Code,
            //        StatusName= statusInfo.Name,
            //        DurationId = cardLine.DurationId,
            //        DurationCode = cardLine.DurationCode,
            //        DurationName = cardLine.DurationName,
            //        ReasonId = cardLine.ReasonId,
            //        ReasonCode = cardLine.ReasonCode,
            //        ReasonName = cardLine.ReasonName
            //    };
            //    agencyAndCards.Add(agencyAndCard);
            //});
        }
        #region -- function for agency --
        /// <summary>
        /// Tạo phiếu giao hàng
        /// </summary>
        /// <param name="id">Code of Sales Order</param>
        /// <returns></returns>
        public ActionResult Create(string id)
        {
            //#region -- Role user --
            //permission = GetPermission(sysCategory, Authorities.Add);
            //if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            //if (!permission.Value) return View("AccessDenied");
            //#endregion
            salesOrderCard = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(SalesType.SalesOrder, id));
            salesOrderCard.Code = StringUtil.CheckLetter("SM", _salesHdrService.GetMax(salesType), 4);
            salesOrderCard.DocumentDate = DateTime.Now;
            salesOrderCard.DocumentDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.DocumentDate);
            salesOrderCard.FromSoure = SalesType.SalesShipment;
            salesOrderCard.SourceNo = id;

            var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("DH", "250"));
            salesOrderCard.StatusId = statusInfo.LineNo;
            salesOrderCard.StatusCode = statusInfo.Code;
            salesOrderCard.StatusName = statusInfo.Name;
            return PartialView("_Create", salesOrderCard);
        }
        [HttpPost]
        public ActionResult Create(SalesOrderCard _salesOrderCard)
        {
            try
            {
                SetDataAgency(_salesOrderCard, true);
                hasValue = _salesHdrService.Insert(mSalesHeader);
                hasValue = _salesHdrService.Update(Mapper.Map<M_SalesHeader>(salesOrderSource));
                hasValue = _salesLineService.Insert(mSalesHeader.SalesLines);
                hasValue = _agencyCardService.Update(Mapper.Map<IEnumerable<M_AgencyAndCard>>(agencyAndCards));
                hasValue = _cardLineService.Update(Mapper.Map<IEnumerable<M_CardLine>>(cardLines));
                //hasValue = _cardEntryService.Insert(Mapper.Map<IEnumerable<M_CardEntry>>(cardEntries));
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                {
                    string url = string.Empty;
                    if (mSalesHeader.CustomerType == AgencyType.Agency)
                    {
                        url = Url.Action("Edit", "SalesOrder", new { area = "Transaction", id = salesOrderSource.Code, msg = "2" });
                        return Json(url, JsonRequestBehavior.AllowGet);
                    } 
                    else
                    {
                        url = Url.Action("Edit", "PartnerOrder", new { area = "Transaction", id = salesOrderSource.Code, msg = "2" });
                        return Json(url, JsonRequestBehavior.AllowGet);
                    }
                }
                    
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        private void SetDataAgency(SalesOrderCard _salesOrderCard, bool isCreate)
        {
            salesOrderSource = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(SalesType.SalesOrder, _salesOrderCard.SourceNo));
            var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("DH", "200")); //Đã Giao Hàng
            salesOrderSource.StatusId = statusInfo.LineNo;
            salesOrderSource.StatusCode = statusInfo.Code;
            salesOrderSource.StatusName = statusInfo.Name;
            salesOrderSource.PostingDate = DateTime.Now;

            salesOrderCard = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(SalesType.SalesOrder, _salesOrderCard.SourceNo));
            salesOrderCard.Code = StringUtil.CheckLetter("SM", _salesHdrService.GetMax(SalesType.SalesShipment), 4);
            salesOrderCard.DocumentDate = salesOrderSource.PostingDate;
            statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("DH", "250"));
            salesOrderCard.StatusId = statusInfo.LineNo;
            salesOrderCard.StatusCode = statusInfo.Code;
            salesOrderCard.StatusName = statusInfo.Name;
            salesOrderCard.SalesType = salesType;
            salesOrderCard.SourceNo = _salesOrderCard.SourceNo;
            salesOrderCard.FromSoure = SalesType.SalesOrder;
            salesOrderCard.Remark = _salesOrderCard.Remark;

            var cardLine = new CardLine();
            cardLines = new List<CardLine>();

            var agencyAndCard = new AgencyAndCard();
            agencyAndCards = new List<AgencyAndCard>();

            var cardEntry = new CardEntry();
            cardEntries = new List<CardEntry>();
            var idCardEntry = _cardEntryService.GetID() - 1;

            statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("CARD", "200")); //Thẻ Chờ Xác Nhận.
            var userInfo = GetUser();

            var _salesLines = Mapper.Map<IEnumerable<SalesLine>>(_salesLineService.FindAll(salesOrderCard.FromSoure, salesOrderCard.SourceNo));
            _salesLines.ToList().ForEach(item => {
                item.SalesType = SalesType.SalesShipment;
                item.DocumentCode = salesOrderCard.Code;
                agencyAndCard = Mapper.Map<AgencyAndCard>(_agencyCardService.GetBy(userInfo.agencyInfo.Id, item.SerialNumber));
                agencyAndCard.UserType = UserType.Agency;
                agencyAndCard.UserNameActive = userInfo.Username;
                agencyAndCard.UserActiveFullName = userInfo.FullName;
                agencyAndCard.LastDateUpdate = salesOrderCard.DocumentDate ?? DateTime.Now;
                agencyAndCard.StatusId = statusInfo.LineNo;
                agencyAndCard.StatusCode = statusInfo.Code;
                agencyAndCard.StatusName = statusInfo.Name;
                agencyAndCards.Add(agencyAndCard);

                cardLine = Mapper.Map<CardLine>(_cardLineService.GetBySerialNumber(item.SerialNumber));
                cardLine.LastStatusId = statusInfo.LineNo;
                cardLine.LastStatusCode = statusInfo.Code;
                cardLine.LastStatusName = statusInfo.Name;
                cardLines.Add(cardLine);

                idCardEntry += 1;
                cardEntry = new CardEntry()
                {
                    //Id = idCardEntry,
                    LotNumber = cardLine.LotNumber,
                    CardLineId = cardLine.Id,
                    SerialNumber = cardLine.SerialNumber,
                    CardNo = cardLine.CardNo,
                    Price = cardLine.Price,
                    UserName = cardLine.UserName,
                    UserFullName = cardLine.UserFullName,
                    DateCreate = cardLine.DateCreate,
                    UserType = UserType.Agency,
                    UserNameActive = agencyAndCard.UserNameActive,
                    UserActiveFullName = agencyAndCard.UserActiveFullName,
                    Date = agencyAndCard.LastDateUpdate,
                    StatusId = agencyAndCard.StatusId,
                    StatusCode = agencyAndCard.StatusCode,
                    StatusName = agencyAndCard.StatusName,
                    Source = CardSource.SalesShipment,
                    SourceNo = salesOrderCard.Code
                };
                cardEntries.Add(cardEntry);
            });
            salesOrderCard.SalesLines = _salesLines;
            mSalesHeader = Mapper.Map<M_SalesHeader>(salesOrderCard);
        }
        #endregion
    }
}