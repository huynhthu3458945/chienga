﻿using AutoMapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Areas.Transaction.Data;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.StoreProcedure.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.Transaction.Controllers
{
    public class ViHatDichVuController : BaseController
    {
        private readonly ICongViecService _congViecService;
        private readonly IViHatService _ViHatService;
        private readonly IViHatDetailService _ViHatDetailService;
        private readonly IVuViecService _vuViecService;
        private readonly IStoreProcedureService _storeService;
        private string sysCategory = "ViHatDichVu";
        private bool? permission = false;
        private string hasValue;
        private M_ViHat _m_ViHat = new M_ViHat();
        private IEnumerable<M_ViHatDetail> _m_ViHatDetail = new List<M_ViHatDetail>();
        public ViHatDichVuController
            (
                IVuViecService vuViecService
                , IViHatService ViHatService
                , IViHatDetailService ViHatDetailService
                , ICongViecService congViecService
                , IStoreProcedureService storeService
            ) : base(vuViecService)
        {
            _congViecService = congViecService;
            _ViHatService = ViHatService;
            _ViHatDetailService = ViHatDetailService;
            _vuViecService = vuViecService;
            _storeService = storeService;
        }

        // GET: Community/SysCategory
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            ViHatIndex model = new ViHatIndex();
            model.ViHatList = Mapper.Map<IEnumerable<ViHatViewModel>>(_ViHatService.FindAll());
            //model.StatusList = new List<SelectListItem>() {
            //new SelectListItem()
            //    {
            //        Selected =  false,
            //        Text = "Bán Mã Kích Hoạt",
            //        Value = "1",
            //    },
            //new SelectListItem()
            //    {
            //        Selected = false,
            //        Text = "Đăng Ký Tài Khoản",
            //        Value = "2",
            //    }
            //};
            return View(model);
        }

        public ActionResult Success()
        {
            return View();
        }

        public ActionResult Create()
        {
            #region -- Role user --
            //permission = GetPermission(sysCategory, Authorities.Add);
            //if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            //if (!permission.Value) return View("AccessDenied");
            #endregion
            ViHatViewModel model = new ViHatViewModel();
            model.StatusList = new List<SelectListItem>()
            {
            new SelectListItem()
                {
                   
                    Text = "Mới",
                    Value = "New",
                },
            new SelectListItem()
                {
                    
                    Text = "Đã gửi Vihat",
                    Value = "Send",
                },
            new SelectListItem()
                {
                   
                    Text = "Đã tạo tài khoản",
                    Value = "Account",
                },
            new SelectListItem()
                {
                    
                    Text = "Đã nhận được API Key",
                    Value = "API",
                }
            };
            model.ViHatDetails = GetViHaDetails();
            return View(model);
        }

        public List<ViHatDetailViewModel> GetViHaDetails()
        {
           return new List<ViHatDetailViewModel>()
            {
                new ViHatDetailViewModel()
                {
                    Service = "Omicall",
                    Description = "https://omicall.com/ve-omicall"
                },
                 new ViHatDetailViewModel()
                {
                    Service = "Voice Brandname",
                    Description = "https://evoicebrand.com"
                },
                 new ViHatDetailViewModel()
                {
                    Service = "Đăng ký đầu số 1800",
                    Description = "Khách gọi vào miễn phí, doanh nghiệp chịu phí"
                },
                 new ViHatDetailViewModel()
                {
                    Service = "Đăng ký đầu số 1900",
                    Description = "Khách gọi vào chịu phí, doanh nghiệp không chịu phí"
                },
                 new ViHatDetailViewModel()
                {
                    Service = "SMS Brandname",
                    Description = "Gửi tin nhắn Brandname cho khách hàng"
                },
                 new ViHatDetailViewModel()
                {
                    Service = "Zalo Zns",
                    Description = "https://zalo.cloud/zns"
                },
                 new ViHatDetailViewModel()
                {
                    Service = "Viber Marketing",
                    Description = "https://vibermarketing.vn/gioi-thieu"
                },
                 new ViHatDetailViewModel()
                {
                    Service = "Svoucher",
                    Description = "https://svoucher.vn/about"
                }
            };
        }
        [HttpPost]
        public ActionResult Create(ViHatViewModel model)
        {
            try
            {
                model.APK = Guid.NewGuid().ToString();
                SetData(model);
                hasValue = _ViHatService.Insert(_m_ViHat);
                hasValue = _ViHatDetailService.Insert(_m_ViHatDetail, model.APK);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Success");
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public ActionResult Edit(string id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            ViHatViewModel model = Mapper.Map<ViHatViewModel>(_ViHatService.GetByAPK(id));
            var resReponViHatDetails = Mapper.Map<IEnumerable<ViHatDetailViewModel>>(_ViHatDetailService.FindAll().Where(z => z.APKMaster == id));
            string[] codeArray = resReponViHatDetails.Select(z=>z.Service).ToArray();
            var resViHatDetails = GetViHaDetails();
            foreach (var item in resViHatDetails)
            {
                if (codeArray.Contains(item.Service))
                    item.Check = true;
            }
            model.ViHatDetails = resViHatDetails;
            model.StatusList = new List<SelectListItem>()
            {
            new SelectListItem()
                {
                    Selected = model.StatusID.Equals("New") ? true : false,
                    Text = "Mới",
                    Value = "New",
                },
            new SelectListItem()
                {
                    Selected = model.StatusID.Equals("Send") ? true : false,
                    Text = "Đã gửi Vihat",
                    Value = "Send",
                },
            new SelectListItem()
                {
                    Selected = model.StatusID.Equals("Account") ? true : false,
                    Text = "Đã tạo tài khoản",
                    Value = "Account",
                },
            new SelectListItem()
                {
                    Selected = model.StatusID.Equals("API") ? true : false,
                    Text = "Đã nhận được API Key",
                    Value = "API",
                }
            };
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(ViHatViewModel model)
        {
            try
            {
                SetData(model);
                hasValue = _ViHatService.Update(_m_ViHat);
                hasValue = _ViHatDetailService.Insert(_m_ViHatDetail, model.APK);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = model.APK });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        #region -- Functions --
        private void SetData(ViHatViewModel model)
        {
            _m_ViHat = Mapper.Map<M_ViHat>(model);
            model.StatusList = new List<SelectListItem>()
            {
            new SelectListItem()
                {
                    Selected = model.StatusID.Equals("New") ? true : false,
                    Text = "Mới",
                    Value = "New",
                },
            new SelectListItem()
                {
                    Selected = model.StatusID.Equals("Send") ? true : false,
                    Text = "Đã gửi Vihat",
                    Value = "Send",
                },
            new SelectListItem()
                {
                    Selected = model.StatusID.Equals("Account") ? true : false,
                    Text = "Đã tạo tài khoản",
                    Value = "Account",
                },
            new SelectListItem()
                {
                    Selected = model.StatusID.Equals("API") ? true : false,
                    Text = "Đã nhận được API Key",
                    Value = "API",
                }
            };
            _m_ViHat.StatusName = model.StatusList.Where(z => z.Value == _m_ViHat.StatusID).FirstOrDefault().Text;
            _m_ViHatDetail = Mapper.Map<IEnumerable<M_ViHatDetail>>(model.ViHatDetails.Where(z=>z.Check));
           foreach(var item in _m_ViHatDetail)
            {
                item.APKMaster = _m_ViHat.APK;
            }
        }
        #endregion
    }
}