﻿using AutoMapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.Areas.Agency.Data;
using GCSOFT.MVC.AGENCY.Areas.Transaction.Data;
using GCSOFT.MVC.AGENCY.Helper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.StoreProcedure.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Service.Transaction.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.Transaction.Controllers
{
    [CompressResponseAttribute]
    public class WholesaleController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly ISalesHeaderService _salesHdrService;
        private readonly ISalesLineService _salesLineService;
        private readonly IAgencyService _agencyService;
        private readonly IOptionLineService _optionLineService;
        private readonly ICardLineService _cardLineService;
        private readonly ICardEntryService _cardEntryService;
        private readonly IAgencyCardService _agencyAndCardService;
        private readonly IStoreProcedureService _storeProcService;
        private readonly IMailSetupService _mailSetupService;
        private readonly ITemplateService _templateService;
        private string sysCategory = "WHOLESALE";
        private bool? permission = false;
        private SalesOrderCard salesOrderCard;
        private M_SalesHeader mSalesHeader;
        private List<AgencyAndCard> agencyAndCards;
        private List<CardEntry> cardEntries;
        private SalesType salesType = SalesType.Wholesale;
        private AgencyType agencyType = AgencyType.RetailCustomers;
        private string hasValue;
        #endregion
        public WholesaleController
            (
                IVuViecService vuViecService
                , ISalesHeaderService salesHdrService
                , ISalesLineService salesLineService
                , IAgencyService agencyService
                , IOptionLineService optionLineService
                , ICardLineService cardLineService
                , ICardEntryService cardEntryService
                , IAgencyCardService agencyAndCardService
                , IStoreProcedureService storeProcService
                , IMailSetupService mailSetupService
                , ITemplateService templateService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _salesHdrService = salesHdrService;
            _salesLineService = salesLineService;
            _agencyService = agencyService;
            _optionLineService = optionLineService;
            _cardLineService = cardLineService;
            _cardEntryService = cardEntryService;
            _agencyAndCardService = agencyAndCardService;
            _storeProcService = storeProcService;
            _mailSetupService = mailSetupService;
            _templateService = templateService;
        }
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var salesOrderList = Mapper.Map<IEnumerable<SalesOrderList>>(_salesHdrService.FindAllAgency(salesType, agencyType, GetUser().agencyInfo.Id));
            return View(salesOrderList);
        }
        public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var userInfo = GetUser();
            salesOrderCard = new SalesOrderCard();
            salesOrderCard.Code = StringUtil.CheckLetter("WS", _salesHdrService.GetMax(salesType), 4);
            salesOrderCard.DocumentDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.DocumentDate);
            salesOrderCard.AgencyId = userInfo.agencyInfo.Id;
            salesOrderCard.AgencyName = userInfo.agencyInfo.FullName;
            salesOrderCard.CustomerList = Mapper.Map<IEnumerable<AgencyList>>(_agencyService.FindAll(agencyType, GetUser().agencyInfo.Id)).ToSelectListItems(0);
            var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("DH", "100"));
            salesOrderCard.StatusId = statusInfo.LineNo;
            salesOrderCard.StatusCode = statusInfo.Code;
            salesOrderCard.StatusName = statusInfo.Name;
            salesOrderCard.CustomerType = agencyType;
            salesOrderCard.LevelList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("LV")).ToSelectListItems("");
            return View(salesOrderCard);
        }
        public ActionResult Edit(string id, int? p)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            salesOrderCard = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(salesType, id));
            if (salesOrderCard == null)
                return HttpNotFound();
            salesOrderCard.DocumentDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.DocumentDate);
            salesOrderCard.PostingDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.PostingDate);
            salesOrderCard.LevelList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("LV")).ToSelectListItems(salesOrderCard.LevelCode);
            salesOrderCard.CustomerList = Mapper.Map<IEnumerable<AgencyList>>(_agencyService.FindAll(agencyType, GetUser().agencyInfo.Id)).ToSelectListItems(salesOrderCard.CustomerId);
            //-- Paging
            int pageString = p ?? 0;
            int page = pageString == 0 ? 1 : pageString;
            var totalRecord = Mapper.Map<IEnumerable<SalesLine>>(_salesLineService.FindAll(salesType, id)).Count();
            salesOrderCard.paging = new Paging(totalRecord, page);
            salesOrderCard.SalesLines = Mapper.Map<IEnumerable<SalesLine>>(_salesLineService.FindAll(salesType, id, salesOrderCard.paging.start, salesOrderCard.paging.offset));
            //-- Paging +
            return View(salesOrderCard);
        }
        public ActionResult Card(string id)
        {
            if (id == null)
                return HttpNotFound();
            salesOrderCard = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(Guid.Parse(id)));
            if (salesOrderCard == null)
                return HttpNotFound();
             
            return View();
        }
        public ActionResult Receipt(string id)
        {
            if (id == null)
                return HttpNotFound();
            salesOrderCard = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(Guid.Parse(id)));
            if (salesOrderCard == null)
                return HttpNotFound();
            salesOrderCard.DocumentDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.DocumentDate);
            salesOrderCard.PostingDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.PostingDate);
            var cryptorEngine = new CryptorEngine2();
            var cardLine = new CardLine();
            if (salesOrderCard.StatusCode.Equals("300"))
            {
                salesOrderCard.SalesLines = Mapper.Map<IEnumerable<SalesLine>>(_salesLineService.FindAll(salesType, salesOrderCard.Code));
                salesOrderCard.SalesLines.ToList().ForEach(item => {
                    cardLine = Mapper.Map<CardLine>(_cardLineService.GetBySerialNumber(item.SerialNumber));
                    item.CardNo = cryptorEngine.Decrypt(cardLine.CardNo, true, "TTLSTNHD@CARD");
                });
                ViewBag.Status = "Receipted";
                return View(salesOrderCard);
            }
            ViewBag.Status = "Receipt";
            var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("DH", "300"));

            salesOrderCard.StatusId = statusInfo.LineNo;
            salesOrderCard.StatusCode = statusInfo.Code;
            salesOrderCard.StatusName = statusInfo.Name;
            salesOrderCard.LastModifyDate = DateTime.Now;
            salesOrderCard.PostingDate = DateTime.Now;
            salesOrderCard.ReceiptDate = DateTime.Now;
            //-- Lay the moi
            //agencyAndCards = Mapper.Map<IEnumerable<AgencyAndCard>>(_agencyAndCardService.FindAll(salesOrderCard.AgencyId ?? 0, "100", salesOrderCard.TotalCard)).ToList();
            agencyAndCards = Mapper.Map<IEnumerable<AgencyAndCard>>(_agencyAndCardService.GetNewCards(salesOrderCard.AgencyId ?? 0, salesOrderCard.LevelCode, "100", salesOrderCard.TotalCard)).ToList();
            var salesLine = from c in agencyAndCards
                            select new SalesLine
                            {
                                SalesType = salesType,
                                DocumentCode = salesOrderCard.Code,
                                LineNo = c.Id,
                                LotNumber = c.LotNumber,
                                CardId = c.Id,
                                SerialNumber = c.SerialNumber,
                                Price = salesOrderCard.Price ?? c.Price
                            };
            salesOrderCard.SalesLines = salesLine;
            //-- Lay the moi +
            cardLine = new CardLine();
            var cardLines = new List<CardLine>();
            var cardEntry = new CardEntry();
            var cardEntries = new List<CardEntry>();
            var idCardEntry = _cardEntryService.GetID() - 1;
            var activeCode = "";
            var builder = new StringBuilder();
            var randomNumber = new RandomNumberGenerator();
            cryptorEngine = new CryptorEngine2();
            var statusOriginal = Mapper.Map<VM_OptionLine>(_optionLineService.Get("CARD", "450")); //Đã Bán Mã Kích Hoạt
            agencyAndCards.ToList().ForEach(item =>
            {
                cryptorEngine = new CryptorEngine2();
                builder = new StringBuilder();
                builder.Append("A");
                Guid g = Guid.NewGuid();
                builder.Append(g.ToString().Split('-')[0].ToString().ToUpper());
                randomNumber = new RandomNumberGenerator();
                builder.Append(randomNumber.RandomNumber(10, 99));
                activeCode = builder.ToString();

                item.StatusId = statusOriginal.LineNo;
                item.StatusCode = statusOriginal.Code;
                item.StatusName = statusOriginal.Name;
                item.UserType = UserType.Agency;
                item.UserNameActive = salesOrderCard.AgencyId.ToString();
                item.UserActiveFullName = salesOrderCard.AgencyName;
                item.LastDateUpdate = DateTime.Now;
                item.CardNo = cryptorEngine.Encrypt(activeCode, true, "TTLSTNHD@CARD");

                cardLine = Mapper.Map<CardLine>(_cardLineService.GetBySerialNumber(item.SerialNumber));
                cardLine.CardNo = item.CardNo;
                cardLine.PhoneNo = salesOrderCard.CustomerPhoneNo;
                cardLine.FulName = salesOrderCard.CustomerName;
                cardLine.LastStatusId = statusOriginal.LineNo;
                cardLine.LastStatusCode = statusOriginal.Code;
                cardLine.LastStatusName = statusOriginal.Name;
                cardLine.LastDateUpdate = item.LastDateUpdate;
                cardLines.Add(cardLine);

                idCardEntry += 1;
                cardEntry = new CardEntry() 
                {
                    LotNumber = item.LotNumber,
                    CardLineId = item.Id,
                    SerialNumber = item.SerialNumber,
                    CardNo = item.CardNo,
                    Price = item.Price,
                    UserName = item.UserName,
                    UserFullName = item.UserFullName,
                    DateCreate = item.DateCreate,
                    UserType = UserType.TTL,
                    UserNameActive = item.UserNameActive,
                    UserActiveFullName = item.UserActiveFullName,
                    Date = item.LastDateUpdate,
                    StatusId = item.StatusId,
                    StatusCode = item.StatusCode,
                    StatusName = item.StatusName,
                    Source = CardSource.SalesOrder,
                    SourceNo = salesOrderCard.Code
                };
                cardEntries.Add(cardEntry);
            });
            mSalesHeader = Mapper.Map<M_SalesHeader>(salesOrderCard);
            hasValue = _salesHdrService.Update(mSalesHeader);
            hasValue = _salesLineService.Insert(mSalesHeader.SalesLines);
            hasValue = _agencyAndCardService.Update(Mapper.Map<IEnumerable<M_AgencyAndCard>>(agencyAndCards));
            hasValue = _cardEntryService.Insert(Mapper.Map<IEnumerable<M_CardEntry>>(cardEntries));
            hasValue = _cardLineService.Update(Mapper.Map<IEnumerable<M_CardLine>>(cardLines));
            hasValue = _vuViecService.Commit();
            hasValue = SendEmailToSuccess(salesOrderCard);
            return RedirectToAction("Receipt", new { id = mSalesHeader.PrivateKey, msg = "2" });
        }
        [HttpPost]
        public ActionResult Create(SalesOrderCard _salesOrderCard)
        {
            try
            {
                SetData(_salesOrderCard, true);
                _salesHdrService.Insert(mSalesHeader);
                _salesLineService.Insert(mSalesHeader.SalesLines);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                {
                    SendEmailToReceipt(salesOrderCard);
                    return RedirectToAction("Edit", new { id = mSalesHeader.Code, msg = "1" });
                }
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        private void SetData(SalesOrderCard _salesOrderCard, bool isCreate)
        {
            var userInfo = GetUser();
            salesOrderCard = _salesOrderCard;
            if (isCreate)
            {
                salesOrderCard.Code = StringUtil.CheckLetter("WS", _salesHdrService.GetMax(salesType), 4);
                salesOrderCard.CreateBy = userInfo.Username;
                salesOrderCard.DateCreate = DateTime.Now;
                salesOrderCard.LastModifyBy = userInfo.Username;
                salesOrderCard.LastModifyDate = salesOrderCard.DateCreate;
                salesOrderCard.PrivateKey = Guid.NewGuid();
                var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("DH", "250"));
                salesOrderCard.StatusId = statusInfo.LineNo;
                salesOrderCard.StatusCode = statusInfo.Code;
                salesOrderCard.StatusName = statusInfo.Name;
            }
            else
            {
                salesOrderCard.LastModifyBy = userInfo.Username;
                salesOrderCard.LastModifyDate = salesOrderCard.DateCreate;
            }
            if (!string.IsNullOrEmpty(salesOrderCard.ReceiptDateStr))
                salesOrderCard.ReceiptDate = DateTime.Parse(StringUtil.ConvertStringToDate(salesOrderCard.ReceiptDateStr));
            else
                salesOrderCard.ReceiptDate = null;
            if (!string.IsNullOrEmpty(salesOrderCard.PostingDateStr))
                salesOrderCard.PostingDate = DateTime.Parse(StringUtil.ConvertStringToDate(salesOrderCard.PostingDateStr));
            else
                salesOrderCard.PostingDate = null;

            if (!string.IsNullOrEmpty(salesOrderCard.DocumentDateStr))
                salesOrderCard.DocumentDate = DateTime.Parse(StringUtil.ConvertStringToDate(salesOrderCard.DocumentDateStr));
            else
                salesOrderCard.DocumentDate = null;
            var customerInfo = Mapper.Map<AgencyCard>(_agencyService.GetBy(salesOrderCard.CustomerId));
            salesOrderCard.CustomerName = customerInfo.FullName;
            salesOrderCard.CustomerAddress = customerInfo.Address;
            salesOrderCard.CustomerEmail = customerInfo.Email;
            salesOrderCard.CustomerPhoneNo = customerInfo.Phone;
            salesOrderCard.SalesType = salesType;
            salesOrderCard.CustomerType = agencyType;
            var LevelInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("LV", salesOrderCard.LevelCode)) ?? new VM_OptionLine();
            salesOrderCard.LevelId = LevelInfo.LineNo;
            salesOrderCard.LevelCode = LevelInfo.Code;
            salesOrderCard.LevelName = LevelInfo.Name;
            mSalesHeader = Mapper.Map<M_SalesHeader>(salesOrderCard);
        }
        private string SendEmailToReceipt(SalesOrderCard salesOrder)
        {
            try
            {
                var customerInfo = Mapper.Map<AgencyCard>(_agencyService.GetBy(salesOrder.CustomerId));
                var mailTemplate = Mapper.Map<Template>(_templateService.Get("WHOLESALE"));
                //-- Send Email
                var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
                var mailTitle = mailTemplate.Title;
                var mailContent = mailTemplate.Content.Replace("{{FULLNAME}}", customerInfo.FullName);
                mailContent = mailContent.Replace("{{AGENCYNAME}}", salesOrder.AgencyName);
                mailContent = mailContent.Replace("{{NOOFCARD}}", salesOrder.TotalCardStr);
                mailContent = mailContent.Replace("{{STATUSNAME}}", salesOrder.StatusName);
                mailContent = mailContent.Replace("{{Id}}", salesOrder.PrivateKey.ToString());
                var emailTos = new List<string>();
                emailTos.Add(customerInfo.Email);
                var _mailHelper = new EmailHelper()
                {
                    EmailTos = emailTos,
                    DisplayName = emailInfo.DisplayName,
                    Title = mailTitle,
                    Body = mailContent,
                    MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
                };
                hasValue = _mailHelper.DoSendMail();
                //-- Send Email +
                //-- Send SMS
                var smsHelper = new SMSHelper()
                {
                    PhoneNo = customerInfo.Phone,
                    Content = mailTemplate.SMSContent.Replace("{{Email}}", customerInfo.Email)
                };
                hasValue = smsHelper.Send();
                //-- Send SMS +
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public JsonResult ResendReceipt(string id)
        {
            try
            {
                salesOrderCard = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(salesType, id));
                hasValue = SendEmailToReceipt(salesOrderCard) ?? string.Empty;
                return Json(new { msg = hasValue }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { msg = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        private string SendEmailToSuccess(SalesOrderCard salesOrder)
        {
            try
            {
                var customerInfo = Mapper.Map<AgencyCard>(_agencyService.GetBy(salesOrder.CustomerId));
                var mailTemplate = Mapper.Map<Template>(_templateService.Get("WHOLESALE_RECEIPT"));
                //-- Send Email
                var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
                var mailTitle = mailTemplate.Title;
                var mailContent = mailTemplate.Content.Replace("{{FULLNAME}}", customerInfo.FullName);
                mailContent = mailContent.Replace("{{NOOFCARD}}", salesOrder.TotalCardStr);
                mailContent = mailContent.Replace("{{STATUSNAME}}", salesOrder.StatusName);
                mailContent = mailContent.Replace("{{Id}}", salesOrder.PrivateKey.ToString());
                var emailTos = new List<string>();
                emailTos.Add(customerInfo.Email);
                var _mailHelper = new EmailHelper()
                {
                    EmailTos = emailTos,
                    DisplayName = emailInfo.DisplayName,
                    Title = mailTitle,
                    Body = mailContent,
                    MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
                };
                hasValue = _mailHelper.DoSendMail();
                //-- Send Email +
                //-- Send SMS
                var smsHelper = new SMSHelper()
                {
                    PhoneNo = customerInfo.Phone,
                    Content = mailTemplate.SMSContent.Replace("{{Email}}", customerInfo.Email)
                };
                hasValue = smsHelper.Send();
                //-- Send SMS +
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

    }
}