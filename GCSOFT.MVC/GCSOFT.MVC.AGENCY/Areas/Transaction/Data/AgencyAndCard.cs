﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.AGENCY.Areas.Transaction.Data
{
    public class AgencyAndCard
    {
        public int AgencyId { get; set; }
        public long Id { get; set; }
        public string LotNumber { get; set; }
        public string SerialNumber { get; set; }
        public string PhoneNo { get; set; }
        public string FulName { get; set; }
        public string OTP { get; set; }
        public string CardNo { get; set; }
        public decimal Price { get; set; }
        public string UserName { get; set; }
        public string UserFullName { get; set; }
        public DateTime DateCreate { get; set; }
        public UserType UserType { get; set; }
        public string UserNameActive { get; set; }
        public string UserActiveFullName { get; set; }
        public DateTime LastDateUpdate { get; set; }
        public int StatusId { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }
        public VerifyType VerifyType { get; set; }
        public string VerifyInfo { get; set; }
        public int DurationId { get; set; }
        public string DurationCode { get; set; }
        public string DurationName { get; set; }
        public int ReasonId { get; set; }
        public string ReasonCode { get; set; }
        public string ReasonName { get; set; }
        public int LevelId { get; set; }
        public string LevelCode { get; set; }
        public string LevelName { get; set; }
    }
}