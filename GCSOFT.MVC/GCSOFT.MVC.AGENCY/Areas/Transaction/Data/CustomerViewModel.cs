﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.Transaction.Data
{
    public class CustomerViewModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        [DisplayName("Họ và Tên")]
        public string FullName { get; set; }
        [DisplayName("Điện Thoại")]
        public string PhoneNo { get; set; }
        public string IsEmail { get; set; }
        [DisplayName("Email")]
        public string Email { get; set; }
        public DateTime? RegisterDate { get; set; }
        public string RegistrationDate { get; set; }
        [DisplayName("Ngày Đăng Ký")]
        public string RegisterDateStr { get { return string.Format("{0:dd/MM/yyyy}", RegisterDate); } }
        public string Owner { get; set; }
        public int? NoOfCard { get; set; }
        public string LotNumber { get; set; }
        public string SerialNumber { get; set; }
        public string CardNo { get; set; }
        public string UserOwner { get; set; }
        public DateTime? DateCard { get; set; }
        public int StatusId { get; set; }
        public string StatusCode { get; set; }
        [DisplayName("Trạng Thái")]
        public string StatusName { get; set; }
        public int DurationId { get; set; }
        public string DurationCode { get; set; }
        public string DurationName { get; set; }
        public int LevelId { get; set; }
        public string LevelCode { get; set; }
        public string LevelName { get; set; }
        public string Address { get; set; }
        public int? CityId { get; set; }
        public string CityName { get; set; }
        public int? DistrictId { get; set; }
        public string DistrictName { get; set; }
        [DisplayName("Tài Khoản")]
        public string UserName { get; set; }
        [DisplayName("Lớp")]
        public int? GradeId { get; set; }
        public IEnumerable<SelectListItem> ClassList { get; set; }
    }
}