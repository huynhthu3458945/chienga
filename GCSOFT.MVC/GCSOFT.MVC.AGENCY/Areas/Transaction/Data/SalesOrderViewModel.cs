﻿using GCSOFT.MVC.AGENCY.Areas.Agency.Data;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.Transaction.Data
{
    public class SalesShipmentPage
    {
        public IEnumerable<SalesOrderList> SalesOrderList { get; set; }
    }
    public class SalesOrderList
    {
        [DisplayName("Mã Đơn Hàng")]
        public string Code { get; set; }
        [DisplayName("Đại Lý")]
        public string CustomerName { get; set; }
        [DisplayName("Ngày")]
        public DateTime DocumentDate { get; set; }
        [DisplayName("Ngày")]
        public string DocumentDateStr { get { return string.Format("{0:dd/MM/yyyy}", DocumentDate); } }
        public DateTime? PostingDate { get; set; }
        [DisplayName("Ngày Giao Hàng")]
        public string PostingDateStr { get { return string.Format("{0:dd/MM/yyyy}", PostingDate); } }
        public DateTime? ReceiptDate { get; set; }
        [DisplayName("Ngày Nhận Hàng")]
        public string ReceiptDateStr { get { return string.Format("{0:dd/MM/yyyy}", ReceiptDate); } }
        [DisplayName("Số Lượng Thẻ")]
        public int NoOfCard { get; set; }
        [DisplayName("Số Lượng Thẻ")]
        public string NoOfCardStr { get { return string.Format("{0:#,##0}", NoOfCard); } }
        [DisplayName("Số Thẻ Tặng")]
        public int GiftCard { get; set; }
        public string GiftCardStr { get { return string.Format("{0:#,##0}", GiftCard); } }
        [DisplayName("Tổng Số Lượng")]
        public int TotalCard { get; set; }
        [DisplayName("Tổng Số Lượng")]
        public string TotalCardStr { get { return string.Format("{0:#,##0}", TotalCard); } }
        [DisplayName("Trạng Thái")]
        public string StatusName { get; set; }
        [DisplayName("Giá Trị")]
        public Decimal TotalAmount { get; set; }
        [DisplayName("Tổng Giá Trị")]
        public string TotalAmountStr { get { return string.Format("{0:#,##0}", TotalAmount); } }
        public string LevelName { get; set; }
        public string FromLevelCode { get; set; }
        public string FromLevelCodeStr { 
            get 
            {
                if (string.IsNullOrEmpty(FromLevelCode))
                    FromLevelCode = "";
                if (FromLevelCode.Equals("100"))
                    return "Thẻ VIP 12 Năm";
                else if (FromLevelCode.Equals("200"))
                    return "Thẻ 1 Cấp";
                else
                    return "Thẻ Lớp";
                return "";
            } 
        }
        public string ToLevelCode { get; set; }
        public string ToLevelCodeStr
        {
            get
            {
                if (string.IsNullOrEmpty(ToLevelCode))
                    ToLevelCode = "300";
                if (ToLevelCode.Equals("100"))
                    return "Thẻ VIP 12 Năm";
                else if (ToLevelCode.Equals("200"))
                    return "Thẻ 1 Cấp";
                else
                    return "Thẻ Lớp";
                return "";
            }
        } 
        public string DurationName { get; set; }
    }
    public class SalesOrderCard
    {
        public SalesType SalesType { get; set; }
        public AgencyType CustomerType { get; set; }
        [DisplayName("Mã Đơn Hàng")]
        public string Code { get; set; }

        public DateTime? DocumentDate { get; set; }
        [DisplayName("Ngày (*)")]
        public string DocumentDateStr { get; set; }

        public DateTime? PostingDate { get; set; }
        [DisplayName("Ngày Giao Hàng")]
        public string PostingDateStr { get; set; }

        public DateTime? ReceiptDate { get; set; }
        [DisplayName("Ngày Nhận Hàng")]
        public string ReceiptDateStr { get; set; }

        [DisplayName("Đại Lý (*)")]
        public int CustomerId { get; set; }
        [DisplayName("Họ và Tên Khách Hàng (*)")]
        public string CustomerName { get; set; }
        [DisplayName("Địa Chỉ")]
        public string CustomerAddress { get; set; }
        [DisplayName("Email")]
        public string CustomerEmail { get; set; }
        [DisplayName("Điện Thoại")]
        public string CustomerPhoneNo { get; set; }
        public IEnumerable<SelectListItem> CustomerList { get; set; }
        [DisplayName("Số Lượng Thẻ (*)")]
        public int NoOfCard { get; set; }
        public string NoOfCardStr { get { return string.Format("{0:#,##0}", NoOfCard); } }
        [DisplayName("Số Thẻ Tặng")]
        public int GiftCard { get; set; }
        public string GiftCardStr { get { return string.Format("{0:#,##0}", GiftCard); } }
        [DisplayName("Tổng Thẻ Nhận")]
        public int TotalCard { get; set; }
        public string TotalCardStr { get { return string.Format("{0:#,##0}", TotalCard); } }
        [DisplayName("Số Lô Thẻ")]
        public string LotNumber { get; set; }
        [DisplayName("Từ Số")]
        public string FromSeriesNumber { get; set; }
        [DisplayName("Đến Số")]
        public string ToSeriesNumber { get; set; }
        [DisplayName("Tổng Tiền")]
        public decimal Amount { get; set; }
        public string AmountStr { get { return string.Format("{0:#,##0}", Amount); } }
        [DisplayName("Chiết Khấu (%)")]
        public decimal DiscountPercent { get; set; } 
        [DisplayName("Tiền Chiết Khấu")]
        public decimal DiscountAmt { get; set; }
        public string DiscountAmtStr { get { return string.Format("{0:#,##0}", DiscountAmt); } }
        [DisplayName("Tổng Tiền")]
        public decimal TotalAmount { get; set; }
        public string TotalAmountStr { get { return string.Format("{0:#,##0}", TotalAmount); } }
        [DisplayName("Ghi Chú")]
        public string Remark { get; set; }

        [DisplayName("Trạng Thái")]
        public int StatusId { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }

        public SalesType FromSoure { get; set; }
        public string SourceNo { get; set; }
        public int? AgencyId { get; set; }
        public string AgencyName { get; set; }
        public string CreateBy { get; set; }
        public DateTime DateCreate { get; set; }
        public string LastModifyBy { get; set; }
        public DateTime LastModifyDate { get; set; }
        public Paging paging { get; set; }
        public int NoOfCardAgency { get; set; }
        public Guid? PrivateKey { get; set; }
        [DisplayName("Đơn Giá (*)")]
        public decimal? Price { get; set; }
        [DisplayName("Gói (*)")]
        public int LevelId { get; set; }
        [DisplayName("Hình Thức Bán (*)")]
        public string TypeSale { get; set; }
        public string LevelCode { get; set; }
        public string LevelName { get; set; }
        [DisplayName("Loại Thẻ (*)")]
        public string FromLevelCode { get; set; }
        public string ToLevelCode { get; set; }
        [DisplayName("Số Lượng Giao Thực Tế")]
        public int? SuggestedNoOfCard { get; set; }
        [DisplayName("Ghi Chú Đơn Hàng")]
        public string TTLRemark { get; set; }
        public int DurationId { get; set; }
        public string DurationCode { get; set; }
        [DisplayName("Thời Hạn")]
        public string DurationName { get; set; }
        public int ReasonId { get; set; }
        public string ReasonCode { get; set; }
        public string ReasonName { get; set; }
        [DisplayName("Khuyến mãi")]
        public string PromotionCodeId { get; set; }
        public string PromotionCode { get; set; }
        public ConvertType ConvertType { get; set; }
        public IEnumerable<SelectListItem> PromotionList { get; set; }
        public IEnumerable<SelectListItem> LevelList { get; set; }
        public IEnumerable<SelectListItem> TypeSaleList { get; set; }
        public IEnumerable<SelectListItem> FromLevelList { get; set; }
        public virtual IEnumerable<SalesLine> SalesLines { get; set; }
        public virtual IEnumerable<CardLine> CardLines { get; set; }
        public virtual IEnumerable<CardEntry> CardEntries { get; set; }
        public virtual List<CustomerViewModel> CustomerViewModelList { get; set; }
        public SalesOrderCard()
        {
            DocumentDate = DateTime.Now;
            paging = new Paging();
            this.SalesLines = new List<SalesLine>();
            CustomerViewModelList = new List<CustomerViewModel>();
        }
    }
    public class SalesLine
    {
        public SalesType SalesType { get; set; }
        public string DocumentCode { get; set; }
        public long LineNo { get; set; }
        public string LotNumber { get; set; }
        public long CardId { get; set; }
        public string SerialNumber { get; set; }
        public decimal Price { get; set; }
        public string CardNo { get; set; }
        public string PriceStr { get { return string.Format("{0:#,##0}", Price); } }
    }
}