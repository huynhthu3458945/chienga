﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.Transaction.Data
{
    public class ViHatViewModel
    {
        public int Id { get; set; }
        [DisplayName("Họ Tên")]
        public string FullName { get; set; }
        [DisplayName("Số Điện Thoại")]
        public string Phone { get; set; }
        [DisplayName("Email")]
        public string Email { get; set; }
        [DisplayName("Số Lượng Nhân Viên")]
        public int? Quantity { get; set; }
        [DisplayName("Trạng Thái")]
        public string StatusID { get; set; }
        public string StatusName { get; set; }
        public string APK { get; set; }
        public IEnumerable<SelectListItem> StatusList { get; set; }
        public IEnumerable<ViHatDetailViewModel> ViHatDetails { get; set; }

        public ViHatViewModel()
        {
            StatusList = new List<SelectListItem>();
            ViHatDetails = new List<ViHatDetailViewModel>();
        }
    }

    public class ViHatIndex
    {
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string StatusID { get; set; }
        public IEnumerable<SelectListItem> StatusList { get; set; }
        public virtual IEnumerable<ViHatViewModel> ViHatList { get; set; }

        public ViHatIndex()
        {
            ViHatList = new List<ViHatViewModel>();
        }
       
    }
    public class ViHatDetailViewModel
    {
        public string Service { get; set; }
        public string Description { get; set; }
        public bool Check { get; set; }
    }


}