﻿using GCSOFT.MVC.AGENCY.Areas.Transaction.Data;
using GCSOFT.MVC.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Areas.Transaction.ViewModel
{
    public class CardStoreIndex
    {
        public Paging paging { get; set; }
        public int CardTotal { get; set; }
        public int CardSold { get; set; }
        public int CardRemaining { get; set; }
        public int CardSoldAgency { get; set; }
        public int CardSoldRegistion { get; set; }
        public int CardSoldRegister { get; set; }
        public int CardSoldPartner { get; set; }
        public string StatusCode { get; set; }
        public IEnumerable<SelectListItem> StatusList { get; set; }
        public string LevelCode { get; set; }
        public IEnumerable<SelectListItem> LevelCodeList { get; set; }
        public string Keyword { get; set; }
        public IEnumerable<AgencyAndCard> AgencyAndCards { get; set; }
    }
}