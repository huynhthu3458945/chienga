﻿using GCSOFT.MVC.Data.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
           //var _activeCode = CryptorEngine.Encrypt("KROK4753KR", true, "TTLSTNHD@CARD");
            return RedirectToAction("Index", "Dashboard", new { area = "administrator" });
        }
    }
}