﻿using AutoMapper;
using Dapper;
using GCSOFT.MVC.AGENCY.Helper;
using GCSOFT.MVC.AGENCY.ViewModels.dtht;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Controllers
{
    public class ParentController : Controller
    {
        private readonly IOptionLineService _optionLineService;
        private string hienTaiConnection = ConfigurationManager.ConnectionStrings["HienTaiConnection"].ConnectionString;
        public ParentController(IOptionLineService optionLineService)
        {
            _optionLineService = optionLineService;

            //khai bao map
            Mapper.CreateMap<ParentInfo, O_ParentOrigin>().ReverseMap();
            Mapper.CreateMap<O_ParentOrigin, ParentInfo>().ReverseMap();
        }

        public ActionResult Info(string r ,string c, int st = 0 )
        {
            //email, sms, app
            //8981
            //st = 8981;
            //c = "email";

            if (c == null || c==string.Empty)
                return RedirectToAction("Error", "Parent");

            if (st == 0)
                return RedirectToAction("Error", "Parent");

            //check exist
            int studentId = GetBy(st);
            if (studentId == 0)
                return RedirectToAction("Error", "Parent");

            //insert parent click
            int rlchhannel = SqlInsertParentClick(c, st);

            ParentInfo parentInfo = new ParentInfo();
            parentInfo.Channel = c;
            parentInfo.StudentId = st;
            O_ParentOrigin data = GetDetail(st);
            if (data.Id > 0) //update
            {
                parentInfo = Mapper.Map<ParentInfo>(data);//gan data
                parentInfo.DanhXungs = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("DX")).ToSelectListItems(parentInfo.DanhXung);
                //3 gioi tinh
                var gioitinhs = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("GENDER"));
                parentInfo.GioiTinhPH1s = gioitinhs.ToSelectListItems(parentInfo.GioiTinhPH1);
                parentInfo.GioiTinhPH2s = gioitinhs.ToSelectListItems(parentInfo.GioiTinhPH2);
                parentInfo.GioiTinhHTs = gioitinhs.ToSelectListItems(parentInfo.GioiTinhHienTai);
                //2 moi quan he
                var moiQuanHes = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("GUARDIAN"));
                parentInfo.MoiQuanHePH1s = moiQuanHes.ToSelectListItems(parentInfo.MoiQuanHePH1);
                parentInfo.MoiQuanHePH2s = moiQuanHes.ToSelectListItems(parentInfo.MoiQuanHePH2);

                parentInfo.PhanMemSuDungs= Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("SOFTWARE")).ToSelectListItems("100");
                parentInfo.PhanMemSuDungArr = parentInfo.PhanMemSuDung.Split(',').ToArray();

                parentInfo.SizeAos = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("HT_SizeShirt")).ToSelectListItems(parentInfo.SizeAo);

                parentInfo.ChaMeHienTai = data.IsChaMeHienTai == true ? 1 : 0;
            }
            else //insert
            {
                parentInfo.HoVaTenPH1 = data.HoVaTenPH1;
                parentInfo.HoVaTenHienTai = data.HoVaTenHienTai;
                parentInfo.DanhXungs = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("DX")).ToSelectListItems("");
                //3 gioi tinh
                var gioitinhs = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("GENDER")).ToSelectListItems("");
                parentInfo.GioiTinhPH1s = gioitinhs;
                parentInfo.GioiTinhPH2s = gioitinhs;
                parentInfo.GioiTinhHTs = gioitinhs;
                //2 moi quan he
                var moiQuanHes = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("GUARDIAN")).ToSelectListItems("");
                parentInfo.MoiQuanHePH1s = moiQuanHes;
                parentInfo.MoiQuanHePH2s = moiQuanHes;

                parentInfo.PhanMemSuDungs = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("SOFTWARE")).ToSelectListItems("");
                parentInfo.SizeAos = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("HT_SizeShirt")).ToSelectListItems("");
                parentInfo.ChaMeHienTai = 1;
            }
            if (r == null)
            {
                ViewBag.Result = 0;
            }
            else
            {
                ViewBag.Result = (r != null && r != "0") ? 1 : 0;
            }
            return View(parentInfo);
        }

        [HttpPost]
        public ActionResult Info(ParentInfo model)
        {
            if (model.PhanMemSuDungArr != null && model.PhanMemSuDungArr.Count() > 0)
            {
                model.PhanMemSuDung = string.Join(",", model.PhanMemSuDungArr);
            }
            model.IsChaMeHienTai = model.ChaMeHienTai == 1 ? true : false;

            //upload file
            if(model.HinhAnhGiaDinhF != null && model.HinhAnhGiaDinhF.ContentLength > 0)
            {
                var dir = Path.Combine(Server.MapPath("~/Files/Parent/HinhAnhGiaDinh"));
                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }
                string _FileName = Path.GetFileName(model.HinhAnhGiaDinhF.FileName);
                string _path = Path.Combine(Server.MapPath("~/Files/Parent/HinhAnhGiaDinh"), _FileName);
                model.HinhAnhGiaDinhF.SaveAs(_path);
                model.HinhAnhGiaDinh = string.Format("/Files/Parent/HinhAnhGiaDinh/{0}", _FileName);
            }

            //upload file
            if (model.GiayKhaiSinhF != null && model.GiayKhaiSinhF.ContentLength > 0)
            {
                var dir = Path.Combine(Server.MapPath("~/Files/Parent/GiayKhaiSinh"));
                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }
                string _FileName = Path.GetFileName(model.HinhAnhGiaDinhF.FileName);
                string _path = Path.Combine(Server.MapPath("~/Files/Parent/GiayKhaiSinh"), _FileName);
                model.GiayKhaiSinhF.SaveAs(_path);
                model.GiayKhaiSinh = string.Format("/Files/Parent/GiayKhaiSinh/{0}", _FileName);
            }

            //result
            int result = 0;
            if (model.Id > 0) // update
            {
                O_ParentOrigin dataUpdate = Mapper.Map<O_ParentOrigin>(model);
                result = SqlUpdateParentOrigin(dataUpdate);
            }
            else // insert
            {
                O_ParentOrigin dataInsert = Mapper.Map<O_ParentOrigin>(model);
                result = SqlInsertParentOrigin(dataInsert);
            }
            return RedirectToAction("Info", "Parent", new {c = model.Channel, st = model.StudentId,r = result });
        }

        public ActionResult Error()
        {
            return View();
        }

        private int GetBy(int id)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "GETBY");
                    paramaters.Add("@StudentId", id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP2_O_ParentOrigin", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return paramaters.Get<Int32>("@RetCode");
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        private O_ParentOrigin GetDetail(int id)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "GETBY_DETAIL");
                    paramaters.Add("@StudentId", id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<O_ParentOrigin>("SP2_O_ParentOrigin", paramaters, null, true, null, System.Data.CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch(Exception ex)
            { 
                return new O_ParentOrigin();
            }
        }

        private int SqlInsertParentClick(string channel, int id)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "PARENT_CLICK_INSERT");
                    paramaters.Add("@StudentId", id);
                    paramaters.Add("@Channel", channel);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Query<O_ParentOrigin>("SP2_O_ParentOrigin", paramaters, null, true, null, System.Data.CommandType.StoredProcedure).FirstOrDefault();
                    return paramaters.Get<Int32>("@RetCode");
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        private int SqlInsertParentOrigin(O_ParentOrigin data)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "INSERT");
                    foreach (PropertyInfo propertyInfo in data.GetType().GetProperties())
                    {
                        paramaters.Add("@" + propertyInfo.Name, propertyInfo.GetValue(data, null));
                    }
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP2_O_ParentOrigin", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return paramaters.Get<Int32>("@RetCode");
                }
            }
            catch (Exception ex)
            { 
                return 0; 
            }
        }

        private int SqlUpdateParentOrigin(O_ParentOrigin data)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "UPDATE");
                    foreach (PropertyInfo propertyInfo in data.GetType().GetProperties())
                    {
                        paramaters.Add("@" + propertyInfo.Name, propertyInfo.GetValue(data, null));
                    }
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP2_O_ParentOrigin", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return data.Id;
                }
            }
            catch (Exception ex)
            { 
                return 0;
            }
        }

        public static string GetUniqueFileName(string fileName)
        {
            fileName = Path.GetFileName(fileName);
            return Path.GetFileNameWithoutExtension(fileName)
                   + "_"
                   + Guid.NewGuid().ToString().Substring(0, 4)
                   + Path.GetExtension(fileName);
        }

    }
}