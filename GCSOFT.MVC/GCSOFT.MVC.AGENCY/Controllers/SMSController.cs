﻿using AutoMapper;
using GCSOFT.MVC.AGENCY.Helper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace GCSOFT.MVC.AGENCY.Controllers
{
    public class SMSController : ApiController
    {
        // GET: api/SMS3
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/SMS3/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/SMS3
        //[BasicAuthentication]
        public HttpResponseMessage Post([FromBody] JToken postData)
        {
            try
            {
                string json = string.Empty;
                HttpResponseMessage response = null;
                var hasValue = string.Empty;
                var smsInfo = JsonConvert.DeserializeObject<SMSHelper>(postData.ToString());
                var smsHelper = new SMSHelper() { 
                    PhoneNo = smsInfo.PhoneNo,
                    Content = smsInfo.Content
                };
                if (smsHelper.IsMobiphone())
                {
                    hasValue = smsHelper.SendMobiphone();
                }
                else
                    hasValue = smsHelper.Send();
                response = Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(hasValue, Encoding.UTF8, "application/json");
                return response;
                //var apiKey = ConfigurationManager.AppSettings["SMSApiKey"];
                //var secretKey = ConfigurationManager.AppSettings["SMSSecretKey"];
                //var brandName = ConfigurationManager.AppSettings["BRANDNAME"];
                //var smsParam = new SMSParam()
                //{
                //    username = "tamtriluc_api",
                //    password = "P@ss885!@#098",
                //    phonenumber = smsInfo.PhoneNo,
                //    message = smsInfo.Content,
                //    brandname = "TAM TRI LUC",
                //    type = "0"
                //};
                //var client = new RestClient("http://221.132.39.104:8088/api/sendsms");
                //client.Timeout = -1;
                //var request = new RestRequest(Method.POST);
                //request.AddHeader("Content-Type", "application/json");
                //request.AddParameter("application/json", new JavaScriptSerializer().Serialize(smsParam), ParameterType.RequestBody);
                //IRestResponse responseSMS = client.Execute(request);
                //var smsResult = JsonConvert.DeserializeObject<SMSResult>(responseSMS.Content);

                //json = JsonConvert.SerializeObject(smsResult);
                //response = Request.CreateResponse(HttpStatusCode.OK);
                //response.Content = new StringContent(json, Encoding.UTF8, "application/json");
                //return response;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
