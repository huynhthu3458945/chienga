﻿using AutoMapper;
using GCSOFT.MVC.AGENCY.Helper;
using GCSOFT.MVC.AGENCY.ViewModels.dtht;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Controllers
{
    public class StudentLeadController : Controller
    {
        private readonly IOptionLineService _optionLineService;
        private readonly IProvinceService _provinceService;
        private readonly IDistrictService _districtService;
        private string apiLink = ConfigurationManager.AppSettings["ApiLink"];
        private string domain = ConfigurationManager.AppSettings["domain"];
        private string hasValue;
        public StudentLeadController
            (
                IOptionLineService optionLineService
                , IProvinceService provinceService
                , IDistrictService districtService
            )
        {
            _optionLineService = optionLineService;
            _provinceService = provinceService;
            _districtService = districtService;
        }
        public ActionResult CreateStudent(string c)
        {
            var studentInfo = new StudentLeadCard()
            {
                ParentIDno = c,
                CourseList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("HT_Course")).ToSelectListItems(""),
                TimeOfCourseList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("HT_TimeOfCourse")).ToSelectListItems(""),
                DateOfBirthList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("DAYOFBIRTH")).ToSelectListItems(""),
                MonthOfBirthList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("MONTH")).ToSelectListItems(""),
                SizeShirtList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("HT_SizeShirt")).ToSelectListItems(""),
                TalkTimeList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("HT_TalkTime")).ToSelectListItems(""),
                GenderList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("GENDER")).ToSelectListItems("")
            };
            return View(studentInfo);
        }
        public ActionResult EditStudent(int id)
        {
            var client = new RestClient(string.Format("{0}/api/Outlier_StudentLead/GetBy?id={1}", apiLink, id));
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);
            var apiResult = JsonConvert.DeserializeObject<ApiResult<StudentLeadCard>>(response.Content);
            var studentInfo = apiResult.Data;

            studentInfo.CourseList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("HT_Course")).ToSelectListItems(studentInfo.CourseCode);
            studentInfo.TimeOfCourseList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("HT_TimeOfCourse")).ToSelectListItems(studentInfo.TimeOfCourseCode);
            studentInfo.DateOfBirthList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("DAYOFBIRTH")).ToSelectListItems(studentInfo.DateOfBirth.ToString());
            studentInfo.MonthOfBirthList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("MONTH")).ToSelectListItems(studentInfo.MonthOfBirth.ToString());
            studentInfo.SizeShirtList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("HT_SizeShirt")).ToSelectListItems(studentInfo.SizeShirtCode);
            studentInfo.TalkTimeList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("HT_TalkTime")).ToSelectListItems(studentInfo.TalkTimeCode);
            studentInfo.GenderList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("GENDER")).ToSelectListItems(studentInfo.GenderCode);

            return View(studentInfo);
        }
        [HttpPost]
        public ActionResult CreateStudent(StudentLeadCard _studentLeadCard)
        {
            try
            {
                if (Request.Files["fileUpload"] != null)
                {
                    if (Request.Files["fileUpload"].ContentLength == 0 && string.IsNullOrEmpty(_studentLeadCard.BirthCertificate))
                    {
                        _studentLeadCard.CourseList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("HT_Course")).ToSelectListItems(_studentLeadCard.CourseCode);
                        _studentLeadCard.TimeOfCourseList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("HT_TimeOfCourse")).ToSelectListItems(_studentLeadCard.TimeOfCourseCode);
                        _studentLeadCard.DateOfBirthList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("DAYOFBIRTH")).ToSelectListItems(_studentLeadCard.DateOfBirth.ToString());
                        _studentLeadCard.MonthOfBirthList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("MONTH")).ToSelectListItems(_studentLeadCard.MonthOfBirth.ToString());
                        _studentLeadCard.SizeShirtList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("HT_SizeShirt")).ToSelectListItems(_studentLeadCard.SizeShirtCode);
                        _studentLeadCard.TalkTimeList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("HT_TalkTime")).ToSelectListItems(_studentLeadCard.TalkTimeCode);
                        _studentLeadCard.GenderList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("GENDER")).ToSelectListItems(_studentLeadCard.GenderCode);
                        ViewBag.isBirth = "error";
                        return View(_studentLeadCard);
                    }
                }
                SetData(ref _studentLeadCard);
                var jsonStudent = JsonConvert.SerializeObject(_studentLeadCard);
                var client = new RestClient(string.Format("{0}/api/Outlier_StudentLead/Insert", apiLink));
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", jsonStudent, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                Console.WriteLine(response.Content);
                return RedirectToAction("Index", "dtht", new { Id = _studentLeadCard.ParentIDno, msg = "1" });
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult EditStudent(StudentLeadCard _studentLeadCard)
        {
            try
            {
                if (Request.Files["fileUpload"] != null)
                {
                    if (Request.Files["fileUpload"].ContentLength == 0 && string.IsNullOrEmpty(_studentLeadCard.BirthCertificate))
                    {
                        _studentLeadCard.CourseList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("HT_Course")).ToSelectListItems(_studentLeadCard.CourseCode);
                        _studentLeadCard.TimeOfCourseList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("HT_TimeOfCourse")).ToSelectListItems(_studentLeadCard.TimeOfCourseCode);
                        _studentLeadCard.DateOfBirthList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("DAYOFBIRTH")).ToSelectListItems(_studentLeadCard.DateOfBirth.ToString());
                        _studentLeadCard.MonthOfBirthList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("MONTH")).ToSelectListItems(_studentLeadCard.MonthOfBirth.ToString());
                        _studentLeadCard.SizeShirtList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("HT_SizeShirt")).ToSelectListItems(_studentLeadCard.SizeShirtCode);
                        _studentLeadCard.TalkTimeList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("HT_TalkTime")).ToSelectListItems(_studentLeadCard.TalkTimeCode);
                        _studentLeadCard.GenderList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("GENDER")).ToSelectListItems(_studentLeadCard.GenderCode);
                        ViewBag.isBirth = "error";
                        return View(_studentLeadCard);
                    }
                }
                SetData(ref _studentLeadCard);
                var jsonStudent = JsonConvert.SerializeObject(_studentLeadCard);
                var client = new RestClient(string.Format("{0}/api/Outlier_StudentLead/Update", apiLink));
                client.Timeout = -1;
                var request = new RestRequest(Method.PUT);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", jsonStudent, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                Console.WriteLine(response.Content);
                return RedirectToAction("Index", "dtht", new { Id = _studentLeadCard.ParentIDno, msg = "1" });
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        private void SetData(ref StudentLeadCard _studentLeadCard)
        {
            
            var _courseInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("HT_Course", _studentLeadCard.CourseCode));
            _studentLeadCard.CourseName = _courseInfo.Name;

            var _timeOfCourse = Mapper.Map<VM_OptionLine>(_optionLineService.Get("HT_TimeOfCourse", _studentLeadCard.TimeOfCourseCode));
            _studentLeadCard.TimeOfCourseName = _timeOfCourse.Name;

            var _sizeShirt = Mapper.Map<VM_OptionLine>(_optionLineService.Get("HT_SizeShirt", _studentLeadCard.SizeShirtCode));
            _studentLeadCard.SizeShirtName = _sizeShirt.Name;

            var _talkTime = Mapper.Map<VM_OptionLine>(_optionLineService.Get("HT_TalkTime", _studentLeadCard.TalkTimeCode));
            _studentLeadCard.TalTimeName = _talkTime.Name;

            var _gender = Mapper.Map<VM_OptionLine>(_optionLineService.Get("GENDER", _studentLeadCard.GenderCode));
            _studentLeadCard.GenderName = _gender.Name;

            UploadHelper _uploadHelper = new UploadHelper();
            _uploadHelper.PathSave = HttpContext.Server.MapPath("~/Files/dtht/");
            _uploadHelper.UploadFile(Request.Files["fileUpload"]);
            if (!string.IsNullOrEmpty(_uploadHelper.FileSave))
                _studentLeadCard.BirthCertificate = string.Format("{0}/Files/dtht/{1}", domain, _uploadHelper.FileSave);
        }
    }
}