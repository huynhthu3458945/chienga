﻿using AutoMapper;
using GCSOFT.MVC.AGENCY.Areas.Agency.Data;
using GCSOFT.MVC.AGENCY.Helper;
using GCSOFT.MVC.AGENCY.ViewModels.dtht;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Controllers
{
    public class dthtController : Controller
    {
        private readonly IOptionLineService _optionLineService;
        private readonly IProvinceService _provinceService;
        private readonly IDistrictService _districtService;
        private string apiLink = ConfigurationManager.AppSettings["ApiLink"];
        private string hasValue;
        public dthtController
            (
                IOptionLineService optionLineService
                , IProvinceService provinceService
                , IDistrictService districtService
            )
        {
            _optionLineService = optionLineService;
            _provinceService = provinceService;
            _districtService = districtService;
        }
        // GET: dtht
        public ActionResult Index(string Id)
        {
            try
            {
                var client = new RestClient(string.Format("{0}/api/Outlier_ParentLead/GetList?IDNo={1}", apiLink, Id));
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                IRestResponse response = client.Execute(request);
                var apiResult = JsonConvert.DeserializeObject<ApiResults<ParentLeadList>>(response.Content);
                var dtnhdPage = new DTHTPage()
                {
                    IDNo = Id,
                    ParentList = apiResult.Data
                };
                client = new RestClient(string.Format("{0}/api/Outlier_StudentLead/GetList?IDNo={1}", apiLink, Id));
                client.Timeout = -1;
                request = new RestRequest(Method.GET);
                response = client.Execute(request);
                var apiStudentResult = JsonConvert.DeserializeObject<ApiResults<StudentLeadList>>(response.Content);
                dtnhdPage.StudentList = apiStudentResult.Data;

                client = new RestClient(string.Format("{0}/api/Outlier_ParentLead/LoadDocument?IDNo={1}", apiLink, Id));
                client.Timeout = -1;
                request = new RestRequest(Method.GET);
                response = client.Execute(request);
                var apiDocumentResult = JsonConvert.DeserializeObject<ApiResults<DocumentList>>(response.Content);
                dtnhdPage.DocumentList = apiDocumentResult.Data;
                return View(dtnhdPage);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
           
        }
        [HttpGet]
        public ActionResult Sign(string c)
        {
            ViewBag.IDno = c;
            return View();
        }
        [HttpPost]
        public ActionResult SignDoc(string IDno)
        {
            try
            {
                var document = new DocumentList();
                document.IDNo = IDno;
                var jsonParent = JsonConvert.SerializeObject(document);
                var client = new RestClient(string.Format("{0}/api/Outlier_ParentLead/SignDoc", apiLink));
                client.Timeout = -1;
                var request = new RestRequest(Method.PUT);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", jsonParent, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                Console.WriteLine(response.Content);
                return RedirectToAction("Index", new { Id = IDno, msg = "1" });
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public ActionResult CreateParent(string c)
        {
            var parentInfo = new ParentLeadCard()
            {
                IDNo = c,
                MainContactList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("CT")).ToSelectListItems(""),
                PrefexCodeList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("DX")).ToSelectListItems(""),
                DateOfBirthList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("DAYOFBIRTH")).ToSelectListItems(""),
                MonthOfBirthList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("MONTH")).ToSelectListItems(""),
                CityList = Mapper.Map<IEnumerable<ProvinceViewModel>>(_provinceService.FindAll()).ToSelectListItems(0),
                DistrictList = Mapper.Map<IEnumerable<DistrictViewModel>>(_districtService.FindAll(0)).ToSelectListItems(0),
                JobCodeList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("JOB")).ToSelectListItems(""),
                MaritalStatusCodeList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("TTHN")).ToSelectListItems(""),
                DifficultCodesList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("KK")).ToSelectListItems(""),
                SoftwareCodesList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("SOFTWARE")).ToSelectListItems(""),
                TimeForChildrenCodeList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("TIMECHIRLD")).ToSelectListItems(""),
                BankList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("BANK")).ToSelectListItems(""),
                RegionList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("QG")).ToSelectListItems("1")
            };
            return View(parentInfo);
        }
        public ActionResult EditParent(int id)
        {
            var client = new RestClient(string.Format("{0}/api/Outlier_ParentLead/GetBy?id={1}", apiLink, id));
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);
            var apiResult = JsonConvert.DeserializeObject<ApiResult<ParentLeadCard>>(response.Content);
            var parentInfo = apiResult.Data;

            parentInfo.MainContactList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("CT")).ToSelectListItems(parentInfo.MainContactCode);
            parentInfo.PrefexCodeList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("DX")).ToSelectListItems(parentInfo.PrefexCode);
            parentInfo.DateOfBirthList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("DAYOFBIRTH")).ToSelectListItems(parentInfo.DateOfBirth.ToString());
            parentInfo.MonthOfBirthList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("MONTH")).ToSelectListItems(parentInfo.MonthOfBirth.ToString());
            parentInfo.CityList = Mapper.Map<IEnumerable<ProvinceViewModel>>(_provinceService.FindAll()).ToSelectListItems(parentInfo.CityId ?? 0);
            parentInfo.DistrictList = Mapper.Map<IEnumerable<DistrictViewModel>>(_districtService.FindAll(parentInfo.CityId ?? 0)).ToSelectListItems(parentInfo.DistrictId ?? 0);
            parentInfo.JobCodeList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("JOB")).ToSelectListItems(parentInfo.JobCode);
            parentInfo.MaritalStatusCodeList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("TTHN")).ToSelectListItems(parentInfo.MaritalStatusName);
            parentInfo.DifficultCodesList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("KK")).ToSelectListItems(""); //parentInfo.DifficultCodes
            parentInfo.SoftwareCodesList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("SOFTWARE")).ToSelectListItems("");//parentInfo.SoftwareCodes
            parentInfo.TimeForChildrenCodeList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("TIMECHIRLD")).ToSelectListItems(parentInfo.TimeForChildrenCode);
            parentInfo.BankList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("BANK")).ToSelectListItems(parentInfo.BankCode);
            parentInfo.RegionList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("QG")).ToSelectListItems(parentInfo.RegionCode);
            parentInfo.DifficultCodeArrs = parentInfo.DifficultCodes.Split(',').ToArray();
            parentInfo.SoftwareCodeArrs = parentInfo.SoftwareCodes.Split(',').ToArray();
            return View(parentInfo);
        }
        [HttpPost]
        public ActionResult CreateParent(ParentLeadCard _parentLeadCard)
        {
            try
            {
                SetData(ref _parentLeadCard);
                var jsonParent = JsonConvert.SerializeObject(_parentLeadCard);
                var client = new RestClient(string.Format("{0}/api/Outlier_ParentLead/Insert", apiLink));
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", jsonParent, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                Console.WriteLine(response.Content);
                return RedirectToAction("Index", new { Id = _parentLeadCard.IDNo, msg = "1" });
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult EditParent(ParentLeadCard _parentLeadCard)
        {
            try
            {
                SetData(ref _parentLeadCard);
                var jsonParent = JsonConvert.SerializeObject(_parentLeadCard);
                var client = new RestClient(string.Format("{0}/api/Outlier_ParentLead/Update", apiLink));
                client.Timeout = -1;
                var request = new RestRequest(Method.PUT);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", jsonParent, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                Console.WriteLine(response.Content);
                return RedirectToAction("Index", new { Id = _parentLeadCard.IDNo, msg = "1" });
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        private void SetData(ref ParentLeadCard _parentLeadInfo)
        {
            var provinceInfo = Mapper.Map<ProvinceViewModel>(_provinceService.GetBy(_parentLeadInfo.CityId ?? 0)) ?? new ProvinceViewModel();
            _parentLeadInfo.CityName = provinceInfo.name;
            var districtInfo = Mapper.Map<DistrictViewModel>(_districtService.GetBy(_parentLeadInfo.DistrictId ?? 0)) ?? new DistrictViewModel();
            _parentLeadInfo.DistrictName = districtInfo.name;

            var _mainContact = Mapper.Map<VM_OptionLine>(_optionLineService.Get("CT", _parentLeadInfo.MainContactCode));
            _parentLeadInfo.MainContactName = _mainContact.Name;

            var _prefex = Mapper.Map<VM_OptionLine>(_optionLineService.Get("DX", _parentLeadInfo.PrefexCode));
            _parentLeadInfo.PrefexName = _prefex.Name;

            var _jobCode = Mapper.Map<VM_OptionLine>(_optionLineService.Get("JOB", _parentLeadInfo.JobCode));
            _parentLeadInfo.JobName = _jobCode.Name;

            var _bank = Mapper.Map<VM_OptionLine>(_optionLineService.Get("BANK", _parentLeadInfo.BankCode));
            _parentLeadInfo.BankName = _bank.Name;

            var _maritalStatus = Mapper.Map<VM_OptionLine>(_optionLineService.Get("TTHN", _parentLeadInfo.MaritalStatusCode));
            _parentLeadInfo.MaritalStatusName = _maritalStatus.Name;

            var _softwareCodes = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.Gets("SOFTWARE", _parentLeadInfo.SoftwareCodeArrs));
            _parentLeadInfo.SoftwareCodes = string.Join(",", _parentLeadInfo.SoftwareCodeArrs);
            _parentLeadInfo.SoftwareName = string.Join(",", _softwareCodes.Select(d => d.Name).ToArray() );

            var _difficultCodes = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.Gets("KK", _parentLeadInfo.DifficultCodeArrs));
            _parentLeadInfo.DifficultCodes = string.Join(",", _parentLeadInfo.DifficultCodeArrs);
            _parentLeadInfo.DifficultName = string.Join(",", _difficultCodes.Select(d => d.Name).ToArray());

            _parentLeadInfo.BirthDay = new DateTime(_parentLeadInfo.YearOfBirth ?? 2020, _parentLeadInfo.MonthOfBirth ?? 1, _parentLeadInfo.DateOfBirth ?? 1);

            var _region = Mapper.Map<VM_OptionLine>(_optionLineService.Get("QG", _parentLeadInfo.RegionCode));
            _parentLeadInfo.RegionName = _region.Name;
        }
    }
}