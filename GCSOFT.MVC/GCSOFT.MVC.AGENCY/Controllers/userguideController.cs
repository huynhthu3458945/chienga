﻿using AutoMapper;
using GCSOFT.MVC.AGENCY.Areas.Administrator.Controllers;
using GCSOFT.MVC.AGENCY.ViewModels;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Controllers
{
    [CompressResponseAttribute]
    public class userguideController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IECourseService _eCouseService;
        
        private string hasValue;
        #endregion

        #region -- Contructor --
        public userguideController
            (
                IVuViecService vuViecService
                , IECourseService eCouseService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _eCouseService = eCouseService;
        }
        #endregion
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">Id Course</param>
        /// <param name="c">Id Category</param>
        /// <returns></returns>
        public ActionResult Details(int? id)
        {
            try
            {
                if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                ECouseCard eCourseCard = new ECouseCard();
                eCourseCard.Course = Mapper.Map<CourseCard>(_eCouseService.GetCourse(id ?? 0));
                return View(eCourseCard);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
            
        }
    }
}