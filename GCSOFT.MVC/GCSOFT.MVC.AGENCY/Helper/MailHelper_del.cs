﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Web;

namespace GCSOFT.MVC.AGENCY.Helper
{
    public class MailHelper_del
    {
        public string EMailFrom { get; set; }
        public List<string> EmailTos { get; set; }
        public List<string> EmailCCs { get; set; }
        public string DisplayName { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public string Password { get; set; }
        public string MailReply { get; set; }
        public string FileAttach { get; set; }
        public bool EnableSsl { get; set; }
        public bool UseDefaultCredentials { get; set; }
        public Stream fileSteam { get; set; }
        public MailHelper_del()
        {
            EnableSsl = true;
            UseDefaultCredentials = false;
        }
        public string DoSendMail()
        {
            try
            {
                MailMessage message = new MailMessage();
                message.From = new MailAddress(this.EMailFrom, this.DisplayName);
                message.SubjectEncoding = System.Text.Encoding.UTF8;
                message.BodyEncoding = System.Text.Encoding.UTF8;
                message.Subject = this.Title;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.Normal;
                foreach (var item in EmailTos)
                {
                    message.To.Add(new MailAddress(item));
                }
                EmailCCs = EmailCCs ?? new List<string>();
                foreach (var item in EmailCCs)
                {
                    message.CC.Add(new MailAddress(item));
                }
                message.Body = this.Body;
                if (!string.IsNullOrEmpty(FileAttach))
                    message.Attachments.Add(new Attachment(fileSteam, FileAttach));
                message.ReplyTo = new MailAddress(string.IsNullOrEmpty(this.MailReply) ? this.EMailFrom : this.MailReply, this.DisplayName);
                SmtpClient client = new SmtpClient();
                client.Host = this.Host;
                client.Port = this.Port;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = UseDefaultCredentials;
                client.Credentials = new NetworkCredential(this.EMailFrom, this.Password);
                client.EnableSsl = EnableSsl;
                client.Send(message);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public string SendEmailsThread()
        {
            string result = "";
            Thread thread = new Thread(
                delegate ()
                {
                    result = DoSendMail();
                }
            );
            thread.Start();
            return result;
        }
    }
}