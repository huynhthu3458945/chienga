﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace GCSOFT.MVC.AGENCY.Helper
{
    public class SMSHelper
    {
        public string PhoneNo { get; set; }
        public string Content { get; set; }
        public string APIKey { get; set; }
        public string Send()
        {
            try
            {
                var apiKey = ConfigurationManager.AppSettings["SMSApiKey"];
                var secretKey = ConfigurationManager.AppSettings["SMSSecretKey"];
                var brandName = ConfigurationManager.AppSettings["BRANDNAME"];
                var smsParam = new SMSParam()
                {
                    username = "tamtriluc_api",
                    password = "P@ss885!@#098",
                    phonenumber = PhoneNo,
                    message = Content,
                    brandname = "TAM TRI LUC",
                    type = "0"
                };
                var client = new RestClient("http://221.132.39.104:8088/api/sendsms");
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", new JavaScriptSerializer().Serialize(smsParam), ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                var smsResult = JsonConvert.DeserializeObject<SMSResult>(response.Content);
                return "";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public string SendMobiphone()
        {
            try
            {
                var client = new RestClient(string.Format("http://rest.esms.vn/MainService.svc/json/SendMultipleMessage_V4_get?Phone={1}&ApiKey=34D616780B2EC90484F7DB8FEF89C7&SecretKey=81350B8E88AFF4D222C2BD903638FF&Brandname=TAM-TRI-LUC&SmsType=2&Content={0}", Content, PhoneNo));
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                request.AddHeader("Cookie", "ASP.NET_SessionId=0s2dblo5xpstfvfjf1j1mdr0");
                IRestResponse response = client.Execute(request);
                return response.Content;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public bool IsMobiphone()
        {
            //return false;
            try
            {
                var listPhone = new List<string>();
                listPhone.Add("090");
                listPhone.Add("093");
                listPhone.Add("089");
                listPhone.Add("070");
                listPhone.Add("079");
                listPhone.Add("078");
                listPhone.Add("077");
                listPhone.Add("076");

                listPhone.Add("09 0");
                listPhone.Add("09 3");
                listPhone.Add("08 9");
                listPhone.Add("07 0");
                listPhone.Add("07 9");
                listPhone.Add("07 8");
                listPhone.Add("07 7");
                listPhone.Add("07 6");

                listPhone.Add("+8490");
                listPhone.Add("+8493");
                listPhone.Add("+8489");
                listPhone.Add("+8470");
                listPhone.Add("+8479");
                listPhone.Add("+8478");
                listPhone.Add("+8477");
                listPhone.Add("+8476");

                listPhone.Add("+84 90");
                listPhone.Add("+84 93");
                listPhone.Add("+84 89");
                listPhone.Add("+84 70");
                listPhone.Add("+84 79");
                listPhone.Add("+84 78");
                listPhone.Add("+84 77");
                listPhone.Add("+84 76");

                var firstNumber = PhoneNo.Substring(0, 3);
                if (listPhone.Contains(firstNumber))
                    return true;
                return false;
            }
            catch { return false; }
        }
    }
    public class SMSParam
    {
        public string username { get; set; }
        public string password { get; set; }
        public string phonenumber { get; set; }
        public string message { get; set; }
        public string brandname { get; set; }
        public string type { get; set; }
    }
    public class SMSResult
    {
        public string id { get; set; }
        public string description { get; set; }
        public string messageid { get; set; }
    }
}