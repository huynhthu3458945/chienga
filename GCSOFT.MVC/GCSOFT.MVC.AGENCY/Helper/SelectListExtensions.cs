﻿// #################################################################
// # Copyright (C) 2021-2022, Tâm Trí Lực JSC.  All Rights Reserved.                       
// #
// # History：                                                                        
// #	Date Time	    Updated		    Content                	
// #    25/08/2021	    Huỳnh Thử 		Update
// ##################################################################

using GCSOFT.MVC.AGENCY.Areas.Agency.Data;
using GCSOFT.MVC.AGENCY.Areas.Transaction.ViewModel;
using GCSOFT.MVC.AGENCY.ViewModels.HienTai;
using GCSOFT.MVC.AGENCY.ViewModels.MasterData;
using GCSOFT.MVC.Model.StoreProcedue;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.Helper
{
    public static class SelectListExtensions
    {
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<ProvinceViewModel> provinces, int selectedId)
        {
            return provinces.Select(item => new SelectListItem
            {
                Selected = (item.id == selectedId),
                Text = item.name,
                Value = item.id.ToString()
            });
        }
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<DistrictViewModel> districts, int selectedId)
        {
            return districts.Select(item => new SelectListItem
            {
                Selected = (item.id == selectedId),
                Text = item.name,
                Value = item.id.ToString()
            });
        }
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<ClassList> classes, int selectedId)
        {
            return classes.Select(item => new SelectListItem
            {
                Selected = (item.Id == selectedId),
                Text = item.Name,
                Value = item.Id.ToString()
            });
        }
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<AgencyList> agencies, int selectedId)
        {
            return agencies.Select(item => new SelectListItem
            {
                Selected = (item.Id == selectedId),
                Text = string.Format("{0} - {1}", item.FullName, item.Phone),
                Value = item.Id.ToString()
            });
        }
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<VM_OptionLine> parents, string selectedId)
        {
            return parents.Select(item => new SelectListItem
            {
                Selected = (item.Code == selectedId),
                Text = item.Name,
                Value = item.Code.ToString()
            });
        }
        public static IEnumerable<SelectListItem> ToSelectListItems(this Dictionary<string, string> parents, string selectedId)
        {
            return parents.Select(item => new SelectListItem
            {
                Selected = (item.Key == selectedId),
                Text = item.Value,
                Value = item.Key.ToString()
            });
        }
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<AgencyParentList> agencyInfo, int selectedId)
        {
            return agencyInfo.Select(item => new SelectListItem
            {
                Selected = (item.Id == selectedId),
                Text = item.FullName,
                Value = item.Id.ToString()
            });
        }
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<LevelStarViewModel> levelStarts, int selectedId)
        {
            return levelStarts.Select(item => new SelectListItem
            {
                Selected = (item.Id == selectedId),
                Text = item.LevelName,
                Value = item.Id.ToString()
            });
        }
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<VM_Class> classes, int selectedId)
        {
            return classes.Select(item => new SelectListItem
            {
                Selected = (item.Id == selectedId),
                Text = item.Name,
                Value = item.Id.ToString()
            });
        }

        /// <summary>
        ///     ToSelectListItems
        /// </summary>
        /// <param name="parents"></param>
        /// <param name="selectedId"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [25/08/2021]
        /// </history>
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<VM_Promotion> parents, string selectedId)
        {
            return parents.Select(item => new SelectListItem
            {
                Selected = (item.Code == selectedId),
                Text = item.Title,
                Value = item.Code.ToString()
            });
        }
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<OptionInt> optionInfos, int selectedId)
        {
            return optionInfos.Select(item => new SelectListItem
            {
                Selected = (item.Key == selectedId),
                Text = item.Value,
                Value = item.Key.ToString()
            });
        }
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<OptionString> optionInfos, string selectedId)
        {
            return optionInfos.Select(item => new SelectListItem
            {
                Selected = (item.Key == selectedId),
                Text = item.Value,
                Value = item.Key.ToString()
            });
        }
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<UserVM> userVMs, string userName)
        {
            return userVMs.Select(item => new SelectListItem
            {
                Selected = (item.Username == userName),
                Text = item.FullName,
                Value = item.Username.ToString()
            });
        }
    }
}