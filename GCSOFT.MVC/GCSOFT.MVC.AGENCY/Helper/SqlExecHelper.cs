﻿using Dapper;
using GCSOFT.MVC.AGENCY.ViewModels.HienTai;
using GCSOFT.MVC.Model.StoreProcedue;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.AGENCY.Helper
{
    public class SqlExecHelper
    {
        public string hienTaiConnection { get; set; }
        public SqlExecHelper()
        {
            hienTaiConnection = ConfigurationManager.ConnectionStrings["HienTaiConnection"].ConnectionString;
        }
        public IEnumerable<AgencyParentList> SqlTeacherListList(O_TeacherInfo teacherInfo)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "PARENT.LIST");
                    paramaters.Add("@AgencyID", teacherInfo.AgencyId);
                    paramaters.Add("@Id", teacherInfo.Id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<AgencyParentList>("SP2_O_Teacher", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }
            }
            catch { return new List<AgencyParentList>(); }
        }
        public IEnumerable<OptionInt> SqlStudentList(O_TeacherInfo teacherInfo)
        {
            try
            {
                //string hienTaiConnection = ConfigurationManager.ConnectionStrings["HienTaiConnection"].ConnectionString;
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "FINDALL2");
                    paramaters.Add("@AgencyId", teacherInfo.AgencyId);
                    paramaters.Add("@TeacherId", teacherInfo.Id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<OptionInt>("SP2_O_Student", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }
            }
            catch { return new List<OptionInt>(); }
        }
        public IEnumerable<OptionInt> SqlSchoolClassList(O_TeacherInfo teacherInfo)
        {
            try
            {
                
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "FINDALL2");
                    paramaters.Add("@AgencyId", teacherInfo.AgencyId);
                    paramaters.Add("@TeacherId", teacherInfo.Id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<OptionInt>("SP2_O_SchoolClass", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }
            }
            catch { return new List<OptionInt>(); }
        }
        public IEnumerable<OptionInt> SqlGetAgency()
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "GETALL");
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<OptionInt>("SP2_O_Agency", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }
            }
            catch { return new List<OptionInt>(); }
        }
    }
}