﻿using Dapper;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.HienTaiService
{
    #region Obj

    public class O_Class
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }
        public int AgencyId { get; set; }
        public int Position { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ModifiBy { get; set; }
        public DateTime ModifyDate { get; set; }
    }

    #endregion End Obj

    public class ClassService
    {
        public IEnumerable<SelectListItem> ToSelectListItems(IEnumerable<O_Class> obj, int selectedId)
        {
            return obj.Select(item => new SelectListItem
            {
                Selected = (item.Id == selectedId),
                Text = item.Title,
                Value = item.Id.ToString()
            });
        }

        public List<O_Class> GetAllClass(int agencyId)
        {
            using (var conn = new SqlConnection(ConnectData.ConnectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();

                paramaters.Add("@Action", EnumAction.All);
                paramaters.Add("@AgencyID", agencyId);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var result = conn.Query<O_Class>("SP_O_Class", paramaters,null,true,null, System.Data.CommandType.StoredProcedure);
               return result.ToList();
            }
        }

        public IPagedList<O_Class> GetAllClassByCondition(string code, string title, string des, int agencyId, int pageNumber, int pageSize)
        {
            using (var conn = new SqlConnection(ConnectData.ConnectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();

                paramaters.Add("@Action", EnumAction.FinAll);
                paramaters.Add("@AgencyID", agencyId);
                paramaters.Add("@Code", code ?? string.Empty);
                paramaters.Add("@Title", title ?? string.Empty);
                paramaters.Add("@Description", des ?? string.Empty);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var result = conn.Query<O_Class>("SP_O_Class", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                return result.ToList().ToPagedList(pageNumber, pageSize);
            }
        }

        public O_Class GetClassById(int agencyId, int id)
        {
            using (var conn = new SqlConnection(ConnectData.ConnectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();

                paramaters.Add("@Action", EnumAction.Detail);
                paramaters.Add("@AgencyID", agencyId);
                paramaters.Add("@Id", id);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var result = conn.Query<O_Class>("SP_O_Class", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
        }

        public int Insert(O_Class model)
        {
            try
            {
                using (var conn = new SqlConnection(ConnectData.ConnectionString))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "INSERT");
                    foreach (PropertyInfo propertyInfo in model.GetType().GetProperties())
                    {
                        paramaters.Add("@" + propertyInfo.Name, propertyInfo.GetValue(model, null));
                    }
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP_O_InsertUpdateDeleteClass", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return paramaters.Get<Int32>("@RetCode");
                }
            }
            catch { return 0; }
        }

        public int Update(O_Class model)
        {
            try
            {
                using (var conn = new SqlConnection(ConnectData.ConnectionString))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "UPDATE");
                    foreach (PropertyInfo propertyInfo in model.GetType().GetProperties())
                    {
                        paramaters.Add("@" + propertyInfo.Name, propertyInfo.GetValue(model, null));
                    }
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP_O_InsertUpdateDeleteClass", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return paramaters.Get<Int32>("@RetCode");
                }
            }
            catch { return 0; }
        }

        public int Delete(O_Class model)
        {
            try
            {
                using (var conn = new SqlConnection(ConnectData.ConnectionString))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "DELETE");
                    foreach (PropertyInfo propertyInfo in model.GetType().GetProperties())
                    {
                        paramaters.Add("@" + propertyInfo.Name, propertyInfo.GetValue(model, null));
                    }
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP_O_InsertUpdateDeleteClass", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return paramaters.Get<Int32>("@RetCode");
                }
            }
            catch { return 0; }
        }
    }
}