﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;


namespace GCSOFT.MVC.AGENCY.HienTaiService
{
    public class ConnectData
    {
        public static string ConnectionString => ConfigurationManager.ConnectionStrings["HienTaiConnection"].ConnectionString;
    }
}