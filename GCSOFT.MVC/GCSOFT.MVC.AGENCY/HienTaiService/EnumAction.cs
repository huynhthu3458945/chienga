﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.AGENCY.HienTaiService
{
    public static class EnumAction
    {
        public static string FinAll = "FINALL";
        public static string FinAllDrop = "FINALLDROP";
        public static string All = "ALL";
        public static string AllByList = "ALLBYLIST";
        public static string Insert = "INSERT";
        public static string Update = "UPDATE";
        public static string Delete = "DELETE";
        public static string Detail = "DETAIL";
        public static string DetailLevel = "DETAILLEVEL";
        public static string GetMax  = "GETMAX";
        
    }
}