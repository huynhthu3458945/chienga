﻿using Dapper;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.HienTaiService
{
    #region Obj

    public class O_Lever
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string LevelName { get; set; }
    }

    #endregion End Obj
    public class LevelService
    {
        public IEnumerable<SelectListItem> ToSelectListItems(IEnumerable<O_Lever> obj, int selectedId)
        {
            return obj.Select(item => new SelectListItem
            {
                Selected = (item.Id == selectedId),
                Text = item.LevelName,
                Value = item.Id.ToString()
            });
        }

        public List<O_Lever> GetAllLever()
        {
            using (var conn = new SqlConnection(ConnectData.ConnectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();

                paramaters.Add("@Action", EnumAction.All);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var result = conn.Query<O_Lever>("SP_O_LEVEL", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                return result.ToList();
            }
        }

    }
}