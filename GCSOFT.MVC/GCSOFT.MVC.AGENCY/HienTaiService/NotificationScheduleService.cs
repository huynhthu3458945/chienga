﻿using AutoMapper;
using Dapper;
using GCSOFT.MVC.AGENCY.Areas.Agency.Data;
using GCSOFT.MVC.AGENCY.Helper;
using GCSOFT.MVC.AGENCY.ViewModels.HienTai;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using Serilog;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace GCSOFT.MVC.AGENCY.HienTaiService
{

    public class NotificationScheduleService
    {
        public void ExecuteNotification(int notificationid, string time, O_NotificationSchedule item, MailSetup emailInfo)
        {
            try
            {
                NotificationScheduleService notificationScheduleService = new NotificationScheduleService();
                O_NotificationNumberSend o_NotificationNumberSend = notificationScheduleService.GetNumberSend(notificationid);

                if (item.NotificationType.Contains("APP") && o_NotificationNumberSend.NumberSendApp > 0)
                {
                    new NotificationScheduleService().ExecuteSendNotification(notificationid, time, item.Name, o_NotificationNumberSend.NumberSendApp);
                }

                if (item.NotificationType.Contains("EMAIL") && o_NotificationNumberSend.NumberSendEmail > 0)
                {
                    new NotificationScheduleService().ExecuteSendEmail(notificationid, time, item.Name, emailInfo, o_NotificationNumberSend.NumberSendEmail);
                }
            }
            catch (Exception ex)
            {
                new NotificationScheduleService().LogException("NotificationScheduleJob_Exec", ex.Message, 0);
            }
        }

        public IEnumerable<OptionString> GetScheduleCategory()
        {
            try
            {
                using (var conn = new SqlConnection(ConnectData.ConnectionString))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_SCHEDULE_CATEGORY");
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var data = conn.Query<OptionString>("SP2_O_NotificationSchedule", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return data;
                }
            }
            catch (Exception ex)
            {
                return new List<OptionString>();
            }
        }

        public O_NotificationNumberSend GetNumberSend(int notificationId)
        {
            try
            {
                using (var conn = new SqlConnection(ConnectData.ConnectionString))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_SCHEDULE_NUMBERSEND");
                    paramaters.Add("@Id", notificationId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<O_NotificationNumberSend>("SP2_O_NotificationSchedule", paramaters, null, true, null, System.Data.CommandType.StoredProcedure).SingleOrDefault();
                }
            }
            catch { return new O_NotificationNumberSend(); }
        }

        public List<O_NotificationScheduleHistory> GetSendBy(int notificationId, string time, string notificationType, int start, int offset)
        {
            try
            {
                using (var conn = new SqlConnection(ConnectData.ConnectionString))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_SCHEDULE_GETSEND_LIST");
                    paramaters.Add("@TimeSlots", time);
                    paramaters.Add("@Id", notificationId);
                    paramaters.Add("@NotificationType", notificationType);
                    paramaters.Add("@Start", start);
                    paramaters.Add("@Offset", offset);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<O_NotificationScheduleHistory>("SP2_O_NotificationSchedule", paramaters, null, true, 600, System.Data.CommandType.StoredProcedure).ToList();
                }
            }
            catch { return new List<O_NotificationScheduleHistory>(); }
        }

        public int ExecuteSendEmail(int notiId, string time, string title, MailSetup mailSetup, int countSendEmail)
        {
            //tinh tong so goi tin
            int packageSize = 30;
            int totalItem = countSendEmail;
            int packageNumber = totalItem / packageSize;
            decimal modItem = totalItem % packageSize;
            if (packageNumber == 0)
                packageNumber = 1;

            if (modItem > 0)
                packageNumber += 1;

            for (int i = 1; i <= packageNumber; i++)
            {
                int start = packageSize * (i - 1);
                //gui email
                //lay data theo goi
                List<O_NotificationScheduleHistory> data = new List<O_NotificationScheduleHistory>();
                data = new NotificationScheduleService().GetSendBy(notiId, time, "MAIL", start, packageSize);

                //tao luong xu ly email
                TheadSendEmail(data, title, mailSetup);
            }
            return 0;
        }

        private void TheadSendEmail(List<O_NotificationScheduleHistory> data, string title , MailSetup mailSetup )
        {
            Thread email = new Thread(delegate ()
            {
                for (int i = 0; i < data.Count(); i++)
                {
                    // thuc hien send email
                    O_NotificationScheduleHistory dataSend = data[i];
                    try
                    {
                        //thuc hien gui
                        List<string> listEmail = new List<string>();
                        if (dataSend != null && dataSend.Email != null)
                        {
                            listEmail.Add(dataSend.Email);
                            var _mailHelper = new EmailHelper()
                            {
                                EmailTos = listEmail,
                                DisplayName = mailSetup.DisplayName,
                                Title = title != null ? title : string.Empty,
                                Body = dataSend.ContentEmail != null ? dataSend.ContentEmail : string.Empty,
                                MailReply = string.IsNullOrEmpty(mailSetup.MailReply) ? mailSetup.Email : mailSetup.MailReply
                            };
                            // noti
                            var hasValue = _mailHelper.DoSendMail();
                        }
                    }
                    catch (Exception ex)
                    {
                        LogException("/NotificationSchedule", ex.Message, dataSend);
                    }
                    //Debug.WriteLine(string.Format("{0}", data[i].StudentId));
                }
            });
            email.IsBackground = true;
            email.Start();
        }

        public int ExecuteSendNotification(int notiId, string time, string title, int countSendNotification)
        {
            //tinh tong so goi tin
            int packageSize = 30;
            int totalItem = countSendNotification;
            int packageNumber = totalItem / packageSize;
            decimal modItem = totalItem % packageSize;
            if (packageNumber == 0)
                packageNumber = 1;

            if (modItem > 0)
                packageNumber += 1;

            for (int i = 1; i <= packageNumber; i++)
            {
                int start = packageSize * (i - 1);
                //gui email
                //lay data theo goi
                List<O_NotificationScheduleHistory> data = new List<O_NotificationScheduleHistory>();
                data = new NotificationScheduleService().GetSendBy(notiId, time, "APP", start, packageSize);

                //tao luong xu ly notification
                TheadSendNotification(data, title);
            }
            return 0;
        }

        private void TheadSendNotification(List<O_NotificationScheduleHistory> data, string title)
        {
            //cau hinh
            string url = "https://apipro.5phutthuocbai.com/api/MstNotifications/push?appName=DTHT";
            var notification = new Noti();
            notification.type = "NOTIFICATION";
            notification.title = title;
            notification.image = "https://5phutthuocbai.com/static/media/voi.ac034b38.png";
            notification.date = DateTime.Now;
            notification.name = "ĐÀO TẠO HIỀN TÀI";

            Thread noti = new Thread(delegate ()
            {
                for (int i = 0; i < data.Count(); i++)
                {
                    // thuc hien send noti
                    O_NotificationScheduleHistory dataSend = data[i];
                    //thuc hien gui
                    List<int> user = new List<int>();
                    if (dataSend != null)
                    {
                        user.Add(dataSend.UserId);
                    }
                    //set config
                    notification.listUserId = user;
                    notification.description = dataSend.Content;

                    try
                    {
                        using (var httpClient = new HttpClient())
                        {
                            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                            //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TOKEN);
                            //httpClient.DefaultRequestHeaders.Add("app-secret", "tamtriluc-firebase");
                            httpClient.Timeout = TimeSpan.FromMinutes(10);
                            HttpResponseMessage response = httpClient.PostAsJsonAsync($"{url}", notification).Result;
                            httpClient.DefaultRequestHeaders.ConnectionClose = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        LogException("/NotificationSchedule", ex.Message, dataSend);
                    }
                }
            });
            noti.IsBackground = true;
            noti.Start();
        }

        public void LogException(string url, string title, object param)
        {
            var logger = new LoggerConfiguration()
                         .MinimumLevel.Information()
                         .WriteTo.File(System.Web.Hosting.HostingEnvironment.MapPath("~/Areas/HienTai/Logs/.txt"),
                         LogEventLevel.Information, // Minimum Log level
                                 rollingInterval: RollingInterval.Day, // This will append time period to the filename like 20180316.txt
                                 retainedFileCountLimit: null,
                                 fileSizeLimitBytes: null,
                                 outputTemplate: "DateTime: {Timestamp:yyyy-MM-dd HH:mm:ss}{NewLine}{Message}{NewLine}Exception: {Exception}{NewLine}",  // Set custom file format
                                 shared: true // Shared between multi-process shared log files
                                 )
                         .CreateLogger();

            logger.Information(title, "Url: {url}\r\nParam: {@param}", url, param);
        }

    }
}