﻿using Dapper;
using GCSOFT.MVC.AGENCY.ViewModels.HienTai;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Web;

namespace GCSOFT.MVC.AGENCY.HienTaiService
{

    #region Obj

    public class O_Notification
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ContentSMS { get; set; }
        public string ContentMobile { get; set; }
        public string ContentEmail { get; set; }
        public string NotificationType { get; set; }
        public string ReceiverType { get; set; }
        public bool IsAll { get; set; }
        public DateTime CreateDate { get; set; }
        public int CreateBy { get; set; }
    }

    public class O_NotificationReceiver
    {
        public int NotificationId { get; set; }
        public int UserId { get; set; }
        public int StudentId { get; set; }
        public string IdNo { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Subject { get; set; }
        public DateTime CreateDate { get; set; }
        public string ContentSMS { get; set; }
        public string ContentMobile { get; set; }
        public string ContentEmail { get; set; }
        public bool Status { get; set; }
    }

    #endregion End Obj

    public class NotificationService
    {
      
        public O_NotificationReceiver GetSendSMSBy(int notiId)
        {
            try
            {
                using (var conn = new SqlConnection(ConnectData.ConnectionString))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_NOTIFICATION_GETSENDSMS");
                    paramaters.Add("@Id", notiId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<O_NotificationReceiver>("SP2_O_Notification", paramaters, null, true, null, System.Data.CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return new O_NotificationReceiver();
            }
        }

        public int UpdateSMSStatus(int notiId, int studentId, int userId, string IdNo)
        {
            try
            {
                using (var conn = new SqlConnection(ConnectData.ConnectionString))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_NOTIFICATION_UPDATESENDSMS");
                    paramaters.Add("@Id", notiId);
                    paramaters.Add("@UserId", userId);
                    paramaters.Add("@IdNo", IdNo);
                    paramaters.Add("@StudentId", studentId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP2_O_Notification", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return 0; //paramaters.Get<Int32>("@RetCode");
                }
            }
            catch
            {
                return 0;
            }
        }

        //-------------- new 23062022
        public List<O_NotificationReceiver> GetSendEmailBy(int notiId, int start, int offset)
        {
            try
            {
                using (var conn = new SqlConnection(ConnectData.ConnectionString))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_NOTIFICATION_GETSENDEMAIL_LIST");
                    paramaters.Add("@Id", notiId);
                    paramaters.Add("@Start", start);
                    paramaters.Add("@Offset", offset);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<O_NotificationReceiver>("SP2_O_Notification", paramaters, null, true, null, System.Data.CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<O_NotificationReceiver>();
            }
        }

        public int UpdateEmailStatus(int notiId, int studentId, int userId, string IdNo)
        {
            try
            {
                using (var conn = new SqlConnection(ConnectData.ConnectionString))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_NOTIFICATION_UPDATESENDEMAIL");
                    paramaters.Add("@Id", notiId);
                    paramaters.Add("@UserId", userId);
                    paramaters.Add("@IdNo", IdNo);
                    paramaters.Add("@StudentId", studentId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP2_O_Notification", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return paramaters.Get<Int32>("@RetCode");
                }
            }
            catch
            {
                return 0;
            }
        }

        public List<O_NotificationReceiver> GetSendAppBy(int notiId, int start, int offset)
        {
            try
            {
                using (var conn = new SqlConnection(ConnectData.ConnectionString))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_NOTIFICATION_GETSENDAPP_LIST");
                    paramaters.Add("@Id", notiId);
                    paramaters.Add("@Start", start);
                    paramaters.Add("@Offset", offset);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<O_NotificationReceiver>("SP2_O_Notification", paramaters, null, true, null, System.Data.CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<O_NotificationReceiver>();
            }
        }

        public int UpdateAppStatus(int notiId, int studentId, int userId, string IdNo)
        {
            try
            {
                using (var conn = new SqlConnection(ConnectData.ConnectionString))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "HIENTAI_NOTIFICATION_UPDATESENDAPP");
                    paramaters.Add("@Id", notiId);
                    paramaters.Add("@UserId", userId);
                    paramaters.Add("@IdNo", IdNo);
                    paramaters.Add("@StudentId", studentId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP2_O_Notification", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return paramaters.Get<Int32>("@RetCode");
                }
            }
            catch
            {
                return 0;
            }
        }

    }
}