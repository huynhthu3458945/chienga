﻿using Dapper;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.HienTaiService
{
    #region Obj
    public class O_Parent
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Facebook { get; set; }
        public int ProvinceId { get; set; }
        public int DistrictId { get; set; }
        public string Beneficiary { get; set; }
        public string BankAccountNo { get; set; }
        public string BankName { get; set; }
        public string Remark { get; set; }
        public string Address { get; set; }
        public string GuardianId { get; set; }
        public string Gender { get; set; }
        public string JobDesc { get; set; }
        public int AgencyId { get; set; }
        public bool IsCreate { get; set; }
    } 
    #endregion

    public class ParentService
    {
        public IEnumerable<O_Parent> GetAllParent(string code, string fullName, string phone, string email, int agencyId)
        {
            try
            {
                using (var conn = new SqlConnection(ConnectData.ConnectionString))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", EnumAction.FinAll);
                    paramaters.Add("@Code", code ?? string.Empty);
                    paramaters.Add("@Phone", phone ?? string.Empty);
                    paramaters.Add("@FullName", fullName ?? string.Empty);
                    paramaters.Add("@Email", email ?? string.Empty);
                    paramaters.Add("@AgencyID", agencyId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<O_Parent>("SP_O_Parent", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }
            }
            catch { return new List<O_Parent>(); }
        }

        public IEnumerable<O_Parent> GetAllParentDrop(int agencyId)
        {
            try
            {
                using (var conn = new SqlConnection(ConnectData.ConnectionString))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", EnumAction.FinAllDrop);
                    paramaters.Add("@AgencyID", agencyId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<O_Parent>("SP_O_Parent", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }
            }
            catch { return new List<O_Parent>(); }
        }

    }
}