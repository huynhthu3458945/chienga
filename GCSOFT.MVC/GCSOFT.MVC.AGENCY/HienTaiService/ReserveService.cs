﻿using Dapper;
using GCSOFT.MVC.AGENCY.ViewModels.HienTai;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;

namespace GCSOFT.MVC.AGENCY.HienTaiService
{
    public class ReserveService
    {
        private string hienTaiConnection = ConfigurationManager.ConnectionStrings["HienTaiConnection"].ConnectionString;

        public List<O_Reserve> GetAllReserve(string code, string reserveCode, int studentId, string approveStatus, int agencyId)
        {
            try
            {
                using (var conn = new SqlConnection(ConnectData.ConnectionString))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "FINDALL");
                    paramaters.Add("@AgencyID", agencyId);
                    paramaters.Add("@Code", code ?? string.Empty);
                    paramaters.Add("@ReserveCode", reserveCode ?? string.Empty);
                    paramaters.Add("@StudentId", studentId);
                    paramaters.Add("@StatusCode", approveStatus ?? string.Empty);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Query<O_Reserve>("SP_O_Reserve", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return result.ToList();
                }
            }
            catch { return new List<O_Reserve>(); }
        }

        public O_ReserveDetails GetBy(string code, int agencyId)
        {
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "GETBY");
                    paramaters.Add("@Code", code);
                    paramaters.Add("@AgencyID", agencyId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    return conn.Query<O_ReserveDetails>("SP_O_Reserve", paramaters, null, true, null, System.Data.CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch { return new O_ReserveDetails(); }
        }

        public List<O_ReserveStatus> GetApproveStatusDrop(int agencyId)
        {
            try
            {
                using (var conn = new SqlConnection(ConnectData.ConnectionString))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "APPROVE.STATUS");
                    paramaters.Add("@AgencyID", agencyId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Query<O_ReserveStatus>("SP_O_Reserve", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return result.ToList();
                }
            }
            catch { return new List<O_ReserveStatus>(); }
        }
    }
}