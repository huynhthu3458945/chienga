﻿using Dapper;
using PagedList;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.HienTaiService
{
    #region Obj
    public class O_SchoolClass
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public int? TeacherId { get; set; }
        public string TeacherName { get; set; }
        public int AgencyId { get; set; }
        public DateTime OpeningDay { get; set; }
        public string OpeningDayString { get; set; }
        public DateTime ClosingDay { get; set; }
        public string ClosingDayString { get; set; }
        public int Total { get; set; }
        public int ClassId { get; set; }
        public int[] LevelArrs { get; set; }
        public bool IsActive { get; set; }
        public string AliasName { get { return string.Format("{0} - {1} - {2}", Code, Title, OpeningDay.ToString("yyyy-MM-dd")); } }
    }

    public class O_SchoolClassInsertUpdate
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int? TeacherId { get; set; }
        public int AgencyId { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime OpeningDay { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime ClosingDay { get; set; }
        public int Total { get; set; }
        public int ClassId { get; set; }
        public string ListLevel { get; set; }
        public List<string> LevelStr { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
    }
    #endregion

    public class SchoolClassService
    {
        //SP_O_SchoolClass
        public IEnumerable<SelectListItem> ToSelectListItems(IEnumerable<O_SchoolClass> obj, int selectedId)
        {
            return obj.Select(item => new SelectListItem
            {
                Selected = (item.Id == selectedId),
                Text = item.Title,
                Value = item.Id.ToString()
            });
        }

        public List<O_SchoolClass> GetAllSchoolClass(int teacherId, int agencyId)
        {
            using (var conn = new SqlConnection(ConnectData.ConnectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();

                paramaters.Add("@Action", EnumAction.All);
                paramaters.Add("@AgencyID", agencyId);
                paramaters.Add("@TeacherId", teacherId);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var result = conn.Query<O_SchoolClass>("SP_O_SchoolClass", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                return result.ToList();
            }
        }

        public List<O_SchoolClass> GetAllSchoolClass(string code, string name, int teacherId, int agencyId)
        {
            using (var conn = new SqlConnection(ConnectData.ConnectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();

                paramaters.Add("@Action", EnumAction.FinAll);
                paramaters.Add("@AgencyID", agencyId);
                paramaters.Add("@Code", code ?? string.Empty);
                paramaters.Add("@Title", name ?? string.Empty);
                paramaters.Add("@TeacherId", teacherId);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var result = conn.Query<O_SchoolClass>("SP_O_SchoolClass", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                return result.ToList();
            }
        }

        public O_SchoolClass GetById(int id, int agencyId)
        {
            using (var conn = new SqlConnection(ConnectData.ConnectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();

                paramaters.Add("@Action", EnumAction.Detail);
                paramaters.Add("@AgencyID", agencyId);
                paramaters.Add("@Id", id);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var result = conn.Query<O_SchoolClass>("SP_O_SchoolClass", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
        }

        public List<O_SchoolClass> GetSchoolClassLevelById(int id, int agencyId)
        {
            using (var conn = new SqlConnection(ConnectData.ConnectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();

                paramaters.Add("@Action", EnumAction.DetailLevel);
                paramaters.Add("@AgencyID", agencyId);
                paramaters.Add("@Id", id);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var result = conn.Query<O_SchoolClass>("SP_O_SchoolClass", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                return result.ToList();
            }
        }

        public int Insert(O_SchoolClassInsertUpdate data)
        {
            try
            {
                using (var conn = new SqlConnection(ConnectData.ConnectionString))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "INSERT");
                    foreach (PropertyInfo propertyInfo in data.GetType().GetProperties().Where(x => x.Name != "LevelStr"))
                    {
                        paramaters.Add("@" + propertyInfo.Name, propertyInfo.GetValue(data, null));
                    }
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP_O_InsertUpdateDeleteSchoolClass", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return paramaters.Get<Int32>("@RetCode");
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int Update(O_SchoolClassInsertUpdate data)
        {
            try
            {
                using (var conn = new SqlConnection(ConnectData.ConnectionString))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "UPDATE");
                    foreach (PropertyInfo propertyInfo in data.GetType().GetProperties().Where(x=> x.Name != "LevelStr"))
                    {
                        paramaters.Add("@" + propertyInfo.Name, propertyInfo.GetValue(data, null));
                    }
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP_O_InsertUpdateDeleteSchoolClass", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return paramaters.Get<Int32>("@RetCode");
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public int Delete(O_SchoolClassInsertUpdate data)
        {
            try
            {
                using (var conn = new SqlConnection(ConnectData.ConnectionString))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "DELETE");
                    paramaters.Add("@Id", data.Id);
                    paramaters.Add("@AgencyId", data.AgencyId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP_O_InsertUpdateDeleteSchoolClass", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return paramaters.Get<Int32>("@RetCode");
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
}