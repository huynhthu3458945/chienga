﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.AGENCY.HienTaiService
{
    #region Obj
    public class StarLevel
    {
        public int Id { get; set; }
        public string BasicName { get; set; }
        public string QualityName { get; set; }
        public string StarName { get; set; }
    }
    #endregion

    public class StarService
    {
        public List<StarLevel> GetAll(int level)
        {
            try
            {
                using (var conn = new SqlConnection(ConnectData.ConnectionString))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", EnumAction.All);
                    paramaters.Add("@Level", level);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var resurt = conn.Query<StarLevel>("SP_O_Star", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return resurt.ToList();
                }
            }
            catch { return new List<StarLevel>(); }
        }

        public List<StarLevel> GetAllByListLevel(string level)
        {
            try
            {
                using (var conn = new SqlConnection(ConnectData.ConnectionString))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", EnumAction.AllByList);
                    paramaters.Add("@LevelStr", level);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var resurt = conn.Query<StarLevel>("SP_O_Star", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return resurt.ToList();
                }
            }
            catch (Exception ex)
            { return new List<StarLevel>(); }
        }
    }
}