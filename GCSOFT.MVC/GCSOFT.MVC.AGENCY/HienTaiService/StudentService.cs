﻿using Dapper;
using GCSOFT.MVC.AGENCY.ViewModels.HienTai;
using GCSOFT.MVC.Model.StoreProcedue;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;

namespace GCSOFT.MVC.AGENCY.HienTaiService
{
    #region Obj
    public class O_Student
    {
        public int Id { get; set; }
        public string IdNo { get; set; }
        public string FullName { get; set; }
        public string Age { get; set; }
        public int SchoolClassStarId { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string DateOfBirthStr { get; set; }
        public DateTime StartDate { get; set; }
        public string StartDateStr { get; set; }
        public DateTime EndDate { get; set; }
        public string EndDateStr { get; set; }
        public string ShirtSize { get; set; }
        public string SchoolClassName { get; set; }
        public int SchoolClassId { get; set; }
        public int ParentId { get; set; }
        public string ParentName { get; set; }
        public string Gender { get; set; }
        public string GenderName { get; set; }
        public string Status { get; set; }
        public string AgencyId { get; set; }
        public string HasSendActiveCode { get; set; }
    }

    public class O_StudentInsertUpdate
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string IdNo { get; set; }
        public string FullName { get; set; }
        public int SchoolClassStarId { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DateOfBirth { get; set; }
        public string DateOfBirthStr { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime StartDate { get; set; }
        public string StartDateStr { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime EndDate { get; set; }
        public string EndDateStr { get; set; }
        public string ShirtSize { get; set; }
        public string SchoolName { get; set; }
        public int SchoolClassId { get; set; }
        public int ParentId { get; set; }
        public string Gender { get; set; }
        public string Status { get; set; }
        public string AgencyId { get; set; }
        public string HasSendActiveCode { get; set; }
        public string StudentComfirmInApp { get; set; }
        public int LevelId { get; set; }
    }

    public class StudentStarLevel
    {
        public int StudentId { get; set; }
        public int StarId { get; set; }
        public int ProcessHeaderID { get; set; }
        public string BasicName { get; set; }
        public string QualityName { get; set; }
        public string StarName { get; set; }
        public int NoOfDay { get; set; }
        public string StartProcessDate { get; set; }
        public string EndProcessDate { get; set; }
    }

    #endregion

    public class StudentService
    {
        public List<O_Student> GetAllStudent(string idNo, string fullName, int? levelId, int? schoolClassId, int agencyId)
        {
            try
            {
                using (var conn = new SqlConnection(ConnectData.ConnectionString))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", EnumAction.FinAll);
                    paramaters.Add("@AgencyID", agencyId);
                    paramaters.Add("@LevelId", levelId);
                    paramaters.Add("@IDNo", idNo ?? string.Empty);
                    paramaters.Add("@FullName", fullName ?? string.Empty);
                    paramaters.Add("@SchoolClassId", schoolClassId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Query<O_Student>("SP_O_Student", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return result.ToList();
                }
            }
            catch { return new List<O_Student>(); }
        }

        public string GetMax()
        {
            try
            {
                using (var conn = new SqlConnection(ConnectData.ConnectionString))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters = new DynamicParameters();
                    paramaters.Add("@Action", EnumAction.GetMax);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var recordOfPageResults = conn.Query<MaxCode>("SP_O_Student", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return recordOfPageResults.FirstOrDefault().Code;
                }
            }
            catch { return ""; }
        }

        public List<StudentStarLevel> GetStudentStars(int studentID, int Level, int agencyId)
        {
            try
            {
                using (var conn = new SqlConnection(ConnectData.ConnectionString))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", EnumAction.FinAll);
                    paramaters.Add("@Id", studentID);
                    paramaters.Add("@AgencyId", agencyId);
                    paramaters.Add("@Level", Level);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var resurt = conn.Query<StudentStarLevel>("SP_O_StudentStar", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return resurt.ToList();
                }
            }
            catch { return new List<StudentStarLevel>(); }
        }

        public int Insert(O_StudentInfo studentInfo)
        {
            try
            {
                using (var conn = new SqlConnection(ConnectData.ConnectionString))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "INSERT");
                    foreach (PropertyInfo propertyInfo in studentInfo.GetType().GetProperties())
                    {
                        paramaters.Add("@" + propertyInfo.Name, propertyInfo.GetValue(studentInfo, null));
                    }
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP_O_Student_Insert", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return paramaters.Get<Int32>("@RetCode");
                }
            }
            catch (Exception ex)
            { return 0; }
        }

        public List<O_Student> GetAllStudentDrop(int agencyId)
        {
            try
            {
                using (var conn = new SqlConnection(ConnectData.ConnectionString))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "STUDENT.LIST");
                    paramaters.Add("@AgencyID", agencyId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Query<O_Student>("SP2_O_GiveGift", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    return result.ToList();
                }
            }
            catch { return new List<O_Student>(); }
        }
    }
}