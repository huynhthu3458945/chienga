﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.AGENCY.HienTaiService
{
    #region Obj
    public class O_Teacher
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string RoleCode { get; set; }
        public string RoleName { get; set; }
        public string ParentName { get; set; }
        public int? ParentTeacherId { get; set; }
        public string GenderName { get; set; }
        public DateTime WorkDate { get; set; }
        public int? AgencyId { get; set; }
        public int? UltimateParent { get; set; }
        public string AliasName { get { return string.Format("{0} - {1}", Code, FullName); } }
    }
    #endregion

    public class TeacherService
    {
        public List<O_Teacher> GetAllTeacher(string code, string name, int teacherId, int agencyId)
        {
            using (var conn = new SqlConnection(ConnectData.ConnectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();

                paramaters.Add("@Action", EnumAction.All);
                paramaters.Add("@AgencyId", agencyId);
                paramaters.Add("@Code", code ?? string.Empty);
                paramaters.Add("@FullName", name ?? string.Empty);
                paramaters.Add("@TeacherId", teacherId);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var result = conn.Query<O_Teacher>("SP_O_Teacher", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                return result.ToList();
            }
        }

        public List<O_Teacher> GetAllTeacher(int teacherId, int agencyId)
        {
            using (var conn = new SqlConnection(ConnectData.ConnectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();

                paramaters.Add("@Action", EnumAction.All);
                paramaters.Add("@AgencyId", agencyId);
                paramaters.Add("@Code", string.Empty);
                paramaters.Add("@FullName", string.Empty);
                paramaters.Add("@TeacherId", teacherId);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var result = conn.Query<O_Teacher>("SP_O_Teacher", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                return result.ToList();
            }
        }
    }
}