﻿using AutoMapper;
using GCSOFT.MVC.AGENCY.Areas.Agency.Controllers;
using GCSOFT.MVC.AGENCY.Areas.Agency.Data;
using GCSOFT.MVC.AGENCY.Areas.ChienGa.Data;
using GCSOFT.MVC.AGENCY.Areas.HienTai.Data;
using GCSOFT.MVC.AGENCY.Areas.Transaction.Data;
using GCSOFT.MVC.AGENCY.Areas.Transaction.ViewModel;
using GCSOFT.MVC.AGENCY.ViewModels.HienTai;
using GCSOFT.MVC.AGENCY.ViewModels.MasterData;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.StoreProcedue;
using GCSOFT.MVC.Model.SystemModels;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.AGENCY.Mappings
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "DomainToViewModelMappings"; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<HT_Users, UserLoginVMs>();
            Mapper.CreateMap<HT_Users, UserVM>();
            
            Mapper.CreateMap<HT_VuViec, VuViecVMs>();
            Mapper.CreateMap<HT_CongViec, CategoryUser>();
            Mapper.CreateMap<HT_CongViec, CategoryListVMs>();
            Mapper.CreateMap<HT_CongViec, CategoryCard>();
            Mapper.CreateMap<HT_VuViecCuaCongViec, CategoryFunctionVMs>();
            //-- Nhom User
            Mapper.CreateMap<HT_NhomUser, NhomUsers>();
            Mapper.CreateMap<HT_NhomUser, NhomUserCard>();
            Mapper.CreateMap<HT_UserThuocNhom, UserThuocNhomVM>();
            //-- Nhom User End
            //-- User
            Mapper.CreateMap<HT_Users, UserList>();
            Mapper.CreateMap<HT_Users, UserCard>();
            //-- User End
            Mapper.CreateMap<M_OptionType, VM_OptionType>();
            Mapper.CreateMap<M_OptionLine, VM_OptionLine>();
            Mapper.CreateMap<M_Agency, AgencyList>();
            Mapper.CreateMap<M_Agency, AgencyCard>();
            Mapper.CreateMap<M_CardHeader, CardHeaderList>();
            Mapper.CreateMap<M_CardHeader, CardHeaderCard>();
            Mapper.CreateMap<M_CardLine, CardLine>();
            Mapper.CreateMap<M_CardEntry, CardEntry>();
            Mapper.CreateMap<M_SalesHeader, SalesOrderList>();
            Mapper.CreateMap<M_SalesHeader, SalesOrderCard>();
            Mapper.CreateMap<M_SalesLine, SalesLine>();
            Mapper.CreateMap<M_AgencyAndCard, AgencyAndCard>();
            Mapper.CreateMap<M_SMS, SMSViewModel>();
            Mapper.CreateMap<M_UserInfo, UserInfoViewModel>();
            Mapper.CreateMap<M_StudentClass, StudentClassViewModel>();
            Mapper.CreateMap<E_Class, ClassList>();
            Mapper.CreateMap<E_Class, VMClassList>();
            Mapper.CreateMap<M_Package, PackageList>();
            Mapper.CreateMap<M_Package, PackageCard>();
            Mapper.CreateMap<M_MailSetup, MailSetup>();
            Mapper.CreateMap<M_Template, Template>();
            Mapper.CreateMap<M_District, DistrictViewModel>();
            Mapper.CreateMap<M_Province, ProvinceViewModel>();
            Mapper.CreateMap<E_Course, CourseCard>();
            Mapper.CreateMap<E_CourseContent, CourseContentLine>();
            Mapper.CreateMap<M_PotentialCustomer, VMPotentialCustomer>();
            Mapper.CreateMap<M_Customer, CustomerViewModel>();
            Mapper.CreateMap<M_Promotion,VM_Promotion>();
            Mapper.CreateMap<SpHistoryRecall, HistoryRecall>();
            Mapper.CreateMap<O_ParentInfo, O_ParentCreateEdit>();
            Mapper.CreateMap<O_CustomerCareInfo, O_CustomerCareCreateEdit>();
            Mapper.CreateMap<E_Class, VM_Class>();
            Mapper.CreateMap<E_Class, VM_Class>();
            Mapper.CreateMap<M_ViHat, ViHatViewModel>();
            Mapper.CreateMap<M_ViHatDetail, ViHatDetailViewModel>();
            Mapper.CreateMap<M_IntroduceStudent, IntroduceStudenViewModel>();
            Mapper.CreateMap<M_Ticket, TicketList>();
            Mapper.CreateMap<M_Ticket, TicketViewModel>();
            Mapper.CreateMap<M_ParcelOfLand, ParcelOfLandViewModel>();
        }
    }
}