﻿using AutoMapper;
using GCSOFT.MVC.AGENCY.Areas.Agency.Controllers;
using GCSOFT.MVC.AGENCY.Areas.Agency.Data;
using GCSOFT.MVC.AGENCY.Areas.ChienGa.Data;
using GCSOFT.MVC.AGENCY.Areas.HienTai.Data;
using GCSOFT.MVC.AGENCY.Areas.Transaction.Data;
using GCSOFT.MVC.AGENCY.ViewModels.HienTai;
using GCSOFT.MVC.AGENCY.ViewModels.MasterData;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.SystemModels;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.AGENCY.Mappings
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "ViewModelToDomainMappings"; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<UserLoginVMs, HT_Users>();
            Mapper.CreateMap<UserVM, HT_Users>();
            Mapper.CreateMap<VuViecVMs, HT_VuViec>();
            Mapper.CreateMap<CategoryUser, HT_CongViec>();
            Mapper.CreateMap<CategoryListVMs, HT_CongViec>();
            Mapper.CreateMap<CategoryCard, HT_CongViec>();
            Mapper.CreateMap<CategoryFunctionVMs, HT_VuViecCuaCongViec>();
            Mapper.CreateMap<NhomUsers, HT_NhomUser>();
            Mapper.CreateMap<NhomUserCard, HT_NhomUser>();
            Mapper.CreateMap<UserThuocNhomVM, HT_UserThuocNhom>();
            Mapper.CreateMap<UserList, HT_Users>();
            Mapper.CreateMap<UserCard, HT_Users>();
            Mapper.CreateMap<VM_OptionType, M_OptionType>();
            Mapper.CreateMap<VM_OptionLine, M_OptionLine>();

            Mapper.CreateMap<AgencyList, M_Agency>();
            Mapper.CreateMap<AgencyCard,M_Agency>();
            Mapper.CreateMap<CardHeaderList, M_CardHeader>();
            Mapper.CreateMap<CardHeaderCard, M_CardHeader>();
            Mapper.CreateMap<CardLine, M_CardLine>();
            Mapper.CreateMap<CardEntry, M_CardEntry>();
            Mapper.CreateMap<SalesOrderList, M_SalesHeader>();
            Mapper.CreateMap<SalesOrderCard, M_SalesHeader>();
            Mapper.CreateMap<SalesLine, M_SalesLine>();
            Mapper.CreateMap<AgencyAndCard, M_AgencyAndCard>();
            Mapper.CreateMap<SMSViewModel, M_SMS>();
            Mapper.CreateMap<UserInfoViewModel, M_UserInfo>();
            Mapper.CreateMap<StudentClassViewModel, M_StudentClass>();
            Mapper.CreateMap<ClassList, E_Class>();
            Mapper.CreateMap<PackageList, M_Package>();
            Mapper.CreateMap<PackageCard, M_Package>();
            Mapper.CreateMap<MailSetup, M_MailSetup>();
            Mapper.CreateMap<Template, M_Template>();
            Mapper.CreateMap<DistrictViewModel, M_District>();
            Mapper.CreateMap<ProvinceViewModel, M_Province>();
            Mapper.CreateMap<CourseCard, E_Course>();
            Mapper.CreateMap<CourseContentLine, E_CourseContent>();
            Mapper.CreateMap<VMPotentialCustomer, M_PotentialCustomer>();
            Mapper.CreateMap<CustomerViewModel, M_Customer>();
            Mapper.CreateMap<O_ParentCreateEdit, O_ParentInfo>();
            Mapper.CreateMap<O_StudentCreateEdit, O_StudentInfo>();
            Mapper.CreateMap<O_CustomerCareCreateEdit, O_CustomerCareInfo>();
            Mapper.CreateMap<VM_Class, E_Class>();
            Mapper.CreateMap<ViHatViewModel, M_ViHat>();
            Mapper.CreateMap<ViHatDetailViewModel, M_ViHatDetail>();
            Mapper.CreateMap<IntroduceStudenViewModel, M_IntroduceStudent>();
            Mapper.CreateMap<TicketList, M_Ticket>();
            Mapper.CreateMap<TicketViewModel, M_Ticket>();
            Mapper.CreateMap<ParcelOfLandViewModel, M_ParcelOfLand>();
        }
    }
}