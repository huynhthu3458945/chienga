$(document).ready(function () {
    ddsmoothmenu.init({
        mainmenuid: "smoothmenu1", //menu DIV id
        orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
        classname: 'ddsmoothmenu', //class added to menu's outer DIV
        //customtheme: ["#424242", "#18374a"],
        contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
    });
    $('#smoothmenu1').show();
    $('.UpperCase').bind('change', function () {
        $(this).val($(this).val().toUpperCase());
    });
	$('.select2').select2();
	var $maskedInput = $('.masked_input');
	if($maskedInput.length) {
		$maskedInput.inputmask();
	}
});

function getNumber(value) {
	return value.replace(/,/g, '').replace(' vnđ','');
}
/*Object Table*/
$.fn.FindMaxIndex = function () {
    var $tableBody = this;
    var $trLast = $tableBody.find("tr:last");
    if ($trLast.length == 0) return 0;
    return parseInt($trLast.find(':input:first').attr('name').match(/\d+/)) + 1;
};

$.fn.RemoveRow = function (objTbl) {
    this.on("click", function (e) {
        e.preventDefault();
        $(this).parent().parent().remove();
        objTbl.RebuildIndex();
        objTbl.RebuildSortOrder();
    });
}
$.fn.RebuildIndex = function () {
    var $tableBody = this;
    for (var i = 0; i < $tableBody.find("tr").length; i++) {
        var $trNew = $tableBody.find("tr:eq(" + i + ")");
        var suffix = $trNew.find(':input:first').attr('name').match(/\d+/);
        $.each($trNew.find(':input'), function (j, val) {
            var oldN = $(this).attr('name');
            var oldId = $(this).attr('id');
            if (typeof (oldN) != "undefined") {
                var newN = oldN.replace('[' + suffix + ']', '[' + i + ']');
                $(this).attr('name', newN);
            }
            if (typeof (oldId) != "undefined") {
                var newId = oldId.replace('_' + suffix + '__', '_' + i + '__');
                $(this).attr('id', newId);
            }
        });
    }
};
$.fn.RebuildSortOrder = function () {
    var $tableBody = this;
    for (var i = 0; i < $tableBody.find('tr').length; i++) {
        $tableBody.find('tr:eq(' + i + ')').find("td:first").html(i + 1);
    }
};
$.fn.AddNewRow = function (objTbl) {
    this.click(function (e) {
        e.preventDefault();
        var $tableBody = objTbl;
        var $trLast = $tableBody.find("tr:last");
        var $trNew = $trLast.clone();
        var suffix = $trNew.find(':input:first').attr('name').match(/\d+/);
        $trNew.find("td:last").html('<a href="javascript:void(0);" name="btnRemoveLine"><i class="material-icons md-24">&#xE872;</i></a>');
        $.each($trNew.find(':input'), function (i, val) {
            // Replaced Name
            var oldN = $(this).attr('name');
            var oldId = $(this).attr('id'); //???
            var newN = oldN.replace('[' + suffix + ']', '[' + (parseInt(suffix) + 1) + ']');
            var newId = oldId.replace('_' + suffix + '__', '_' + (parseInt(suffix) + 1) + '__'); //???
            $(this).attr('name', newN).attr('id', newId);
            //Replaced value
            var type = $(this).attr('type');
            if (typeof (type) != "undefined") {
                if (type.toLowerCase() == "text" || type.toLowerCase() == "hidden") {
                    $(this).attr('value', '');
                    $(this).val('');
                }
            }
            if ($(this).hasClass('auto')) {
                $(this).val(0);
            }
        });
        $trLast.after($trNew);
        InitValue();
        $tableBody.RebuildIndex();
        $tableBody.RebuildSortOrder();
    });
};
/*Objec Table End*/
$.urlParam = function (name) {
    var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results == null) {
        return '';
    } else {
        return results[1] || 0;
    }
}
$.validation = function (obj) {
    return obj.validationEngine('validate');
}
function CreateSuccess(url) {
    if ($.urlParam('msg') == '1') {
        var stringBuiltder = '';
        stringBuiltder += 'Cập nhật dữ liệu thành công<BR />';
        stringBuiltder += ' <a name="loadContent" href="javascript:void(0);" accesskey="' + url + '">Tiếp tục thêm mới ...</a>';
        $().toastmessage('showToast', {
            text: stringBuiltder,
            sticky: false,
            position: 'top-center',
            type: 'success'
        });
        $('[name="loadContent"]').click(function () {
            $(location).attr('href', $(this).attr('accesskey'));
        });
    }
}

function AddNewLine(tbody, btnNewLine, btnRemoveLine, keyNewLine, keyFocus, templateHtml, contextJson) {
    var passDataToTemplate = Handlebars.compile(templateHtml.html())(contextJson);
    if (String.format('{0} .tr")', tbody).length > 0) {
        tbody.find(".tr").end().append(passDataToTemplate);
    } else {
        tbody.html(passDataToTemplate);
    }
    altair_md.inputs();
    altair_uikit.reinitialize_grid_margin();
    tbody.Uk_RebuildIndex();
    tbody.Uk_RebuildSortOrder();
    InitLine(tbody, btnNewLine, btnRemoveLine, keyNewLine, keyFocus);
}
function InitLine(tbody, btnNewLine, btnRemoveLine, keyNewLine, keyFocus) {
    btnRemoveLine.Uk_RemoveRow(tbody);
    keyNewLine.keydown(function (e) {
        var code = e.keyCode || e.which;
        if (code === 9) {
            e.preventDefault();
            var idxTbl = tbody.find('.tr').length;
            var idx = keyNewLine.index($(this)) + 1;

            if (idxTbl == idx) {
                btnNewLine.click();
                keyFocus.last().focus();
            }
            return false;
        }
    });
}
String.format = function () {
    // The string containing the format items (e.g. "{0}")
    // will and always has to be the first argument.
    var theString = arguments[0];

    // start with the second argument (i = 1)
    for (var i = 1; i < arguments.length; i++) {
        // "gm" = RegEx options for Global search (more than one instance)
        // and for Multiline search
        var regEx = new RegExp("\\{" + (i - 1) + "\\}", "gm");
        theString = theString.replace(regEx, arguments[i]);
    }

    return theString;
}
function BlockUI() {
    (function (modal) {
        modal = UIkit.modal.blockUI('<div class=\'uk-text-center\'>Đang tải dữ liệu ...');
    })();
}
function AjaxSave(objForm, flag2) {
    var options = {
        beforeSend: function () {
            BlockUI();
        },
        error: function () {
            $().toastmessage('showErrorToast', "Lỗi, Vui lòng liên hệ với quản trị viên để được hổ trợ");
        }
    }

    var flag = $.validation(objForm);
    if (flag && flag2) {
        objForm.ajaxForm(options).submit();
    }
}

function Save(objForm, flag2)
{
    var flag = $.validation(objForm);
    if (flag && flag2) {
        $('.masked_input').inputmask('remove');
        BlockUI();
        objForm.submit();
    }
}

gcHotkey = {
    _add: function () {
        $(document).bind('keydown.Alt_t', function (evt) {
            $(location).attr('href', $('[name="btnAdd"]').attr('href'));
            return false;
        });
    },
    _edit: function () {
        $(document).bind('keydown.Alt_s', function (evt) {
            $('[name="btnEdit"]').click();
            return false;
        });
    },
    _del: function () {
        $(document).bind('keydown.Alt_x', function (evt) {
            $('[name="btnDel"]').click();
            return false;
        });
    },
    _view: function () {
        $(document).bind('keydown.Alt_v', function (evt) {
            $('[name="Details"]').click();
            return false;
        });
    },
    _return: function () {
        $(document).bind('keydown.Alt_q', function (evt) {
            $(location).attr('href', $('#btnReturn').attr('href'));
            return false;
        });
    },
    _save: function () {
        $(document).bind('keydown.Alt_l', function (evt) {
            $('#btnSave').click();
            return false;
        });
    }
}
/*JqGrid*/
function OpenDialog(obj, url, title) {
    obj.fancybox({
        openEffect: 'elastic',
        closeEffect: 'elastic',
        href: url,
        title: title,
        type: 'ajax',
        fitToView: true
    });
}
// delete one row in tbody    
function deleteRow(id_tbody, index, sTT) {
    var table = document.getElementById(id_tbody);
    table.deleteRow(index);
    for (var i = 0; i < table.rows.length; i++) {
        if (sTT == null) {
            $('#' + id_tbody + ' tr:eq(' + i + ')').find("td:last").html("<a href='javascript:void(0);' onclick='deleteRow(\"" + id_tbody + "\"," + i + ")'><i class=\"material-icons\">&#xE872;</i></a>");
            $("#" + id_tbody).RebuildIndex();
        } else {
            $('#' + id_tbody + ' tr:eq(' + i + ')').find("td:first").html(i + 1);
            $('#' + id_tbody + ' tr:eq(' + i + ')').find("td:last").html("<a href='javascript:void(0);' onclick='deleteRow(\"" + id_tbody + "\"," + i + ", true)'><i class=\"material-icons\">&#xE872;</i></a>");
            $("#" + id_tbody).RebuildIndex();
        }
    }
}

function checkAllTbl(tblName, chkRoot) {
    var x = document.getElementById(tblName);
    var rowAll = x.rows.length - 1;
    var check = document.getElementById(chkRoot);
    if (check.checked) {
        $("[name='chk']").each(function () {
            if ($(this).attr('disabled') != true)
                $(this).attr('checked', true);
        });
    }
    else
        $("[name='chk']").removeAttr('checked');
}

function checkOneTable(checkOne, tblName, chkRoot) {
    if (checkOne.checked == false) {
        document.getElementById(chkRoot).checked = false;
    }
    else if (getCheckInTbl(tblName)) {
        document.getElementById(chkRoot).checked = true;
    }
}

$.fn.Uk_RemoveRow = function (objTbl) {
    this.on("click", function (e) {
        e.preventDefault();
        $(this).parent().parent().remove();
        objTbl.Uk_RebuildIndex();
        objTbl.Uk_RebuildSortOrder();
    });
}
$.fn.Uk_RebuildIndex = function () {
    var $tableBody = this;
    for (var i = 0; i < $tableBody.find(".tr").length; i++) {
        var $trNew = $tableBody.find(".tr:eq(" + i + ")");
        var suffix = $trNew.find(':input:first').attr('name').match(/\d+/);
        $.each($trNew.find(':input'), function (j, val) {
            var oldN = $(this).attr('name');
            var oldId = $(this).attr('id');
            if (typeof (oldN) != "undefined") {
                var newN = oldN.replace('[' + suffix + ']', '[' + i + ']');
                $(this).attr('name', newN);
            }
            if (typeof (oldId) != "undefined") {
                var newId = oldId.replace('_' + suffix + '__', '_' + i + '__');
                $(this).attr('id', newId);
            }
        });
    }
};
$.fn.Uk_RebuildSortOrder = function () {
    var $tableBody = this;
    for (var i = 0; i < $tableBody.find('.tr').length; i++) {
        $tableBody.find('.tr:eq(' + i + ')').find("div:first").html('<p class="uk-text-large">' + (i + 1) +'</p>');
    }
};
function change_alias(alias) {
    var str = alias;
    // Gộp nhiều dấu space thành 1 space
    str = str.replace(/\s+/g, ' ');
    // loại bỏ toàn bộ dấu space (nếu có) ở 2 đầu của xâu
    str.trim();
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g, " ");
    str = str.replace(/ + /g, " ");
    str = str.trim();
    return str;
}
$.fn.Uk_RemoveRow2 = function (objTbl) {
    this.on("click", function (e) {
        e.preventDefault();
        $(this).parent().parent().parent().parent().parent().remove();
        altair_uikit.reinitialize_grid_margin();
        objTbl.Uk_RebuildIndex();
        objTbl.Uk_RebuildSortOrder2();
    });
}
$.fn.Uk_RebuildSortOrder2 = function () {
    var $tableBody = this;
    for (var i = 0; i < $tableBody.find('.tr').length; i++) {
        $tableBody.find('.tr:eq(' + i + ')').find("span:first").html(i + 1);
    }
};