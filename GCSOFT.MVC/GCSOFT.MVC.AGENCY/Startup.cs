﻿
using Autofac;
using GCSOFT.MVC.AGENCY.HienTaiService;
using Hangfire;
using Hangfire.Dashboard;
using Microsoft.Owin;
using Owin;
using System;
using System.Threading.Tasks;

[assembly: OwinStartup(typeof(GCSOFT.MVC.AGENCY.Startup))]

namespace GCSOFT.MVC.AGENCY
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {

            var options = new DashboardOptions
            {
                Authorization = new[]{new DashboardNoAuthorizationFilter()}
            };

            app.UseHangfireDashboard("/hangfire", options);
            app.UseHangfireServer();
        }
    }
}
