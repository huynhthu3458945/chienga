﻿using GCSOFT.MVC.Model.SystemModels;

namespace GCSOFT.MVC.Web.ViewModels
{
    public class AuthenticationVM
    {
        public string CategoryTree { get; set; }
        public bool chkTatCa { get; set; }
        public HT_NhomUser userGroups { get; set; }
    }
}