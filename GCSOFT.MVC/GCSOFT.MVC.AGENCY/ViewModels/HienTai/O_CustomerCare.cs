﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.ViewModels.HienTai
{
    public class O_CustomerCarePage
    {
        public int TeacherId { get; set; }
        public IEnumerable<SelectListItem> TeacherList { get; set; }
        public int StudentId { get; set; }
        public IEnumerable<SelectListItem> StudentList { get; set; }
        public Paging paging { get; set; }
        public List<O_CustomerCareList> CustomerCareList { get; set; }
    }
    public class O_CustomerCareList
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string StudentName { get; set; }
        public string TecherName { get; set; }
        public string Title { get; set; }
        public DateTime DateInput { get; set; }
    }
    public class O_CustomerCareCreateEdit
    {
        public int Id { get; set; }
        [DisplayName("Mã Cuộc Gọi")]
        public string Code { get; set; }
        [DisplayName("Hiền Tài")]
        public int StudentId { get; set; }
        [DisplayName("Phụ Huynh")]
        public int ParentId { get; set; }
        [DisplayName("Tiêu Đề")]
        public string Title { get; set; }
        [DisplayName("Nội Dung Cuộc Gọi")]
        public string Description { get; set; }
        [DisplayName("Ngày Gọi")]
        public DateTime DateInput { get; set; }
        public string DateInputStr { get; set; }
        public int AgencyId { get; set; }
        public int TeacherId { get; set; }
        public string PhoneNumber { get; set; }
        public bool Ref { get; set; }
        public IEnumerable<SelectListItem> StudentList { get; set; }
    }
    public class O_CustomerCareInfo
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int StudentId { get; set; }
        public int ParentId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime DateInput { get; set; }
        public int AgencyId { get; set; }
        public int TeacherId { get; set; }
        public string PhoneNumber { get; set; }
    }
}