﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace GCSOFT.MVC.AGENCY.ViewModels.HienTai
{
    public class O_GiftListPage
    {
        public string Code { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Paging paging { get; set; }
        public List<O_GiftList> GiftList { get; set; }
    }
    public class O_GiftList
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public int Quantily { get; set; }
        public DateTime DateInput { get; set; }
    }
    public class O_GiftCreateEdit
    {
        public int Id { get; set; }
        [DisplayName("Mã Quà Tặng")]
        public string Code {  get; set; }
        [DisplayName("Tên Quà Tặng")]
        public string Title { get; set; }
        [DisplayName("Mô Tà Quà Tặng")]
        public string Description { get; set; }
        [DisplayName("Hình Ảnh Quà Tặng")]
        public string ImageName { get; set; }
        public string ImagePath { get; set; }
        [DisplayName("Tổng Số Lượng")]
        public int Quantily { get; set; }
        [DisplayName("Kích Hoạt Quà Tặng")]
        public bool IsActive { get; set; }
        [DisplayName("Ngày Nhập Quà Tặng")]
        public DateTime DateInput { get; set; }
        public string DateInputString { get; set; }
        public int AgencyId { get; set; }

    }
    public class O_GiftInfo
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageName { get; set; }
        public string ImagePath { get; set; }
        public int Quantily { get; set; }
        public bool IsActive { get; set; }
        public DateTime DateInput { get; set; }
        public int AgencyId { get; set; }
    }

    public class O_GiftDetails
    {
        [DisplayName("Mã Quà Tặng")]
        public string Code { get; set; }
        [DisplayName("Tên Quà Tặng")]
        public string Title { get; set; }
        [DisplayName("Mô Tả Quà Tặng")]
        public string Description { get; set; }
        [DisplayName("Hình Ảnh Quà Tặng")]
        public string ImageName { get; set; }
        public string ImagePath { get; set; }
        [DisplayName("Tổng Số Lượng")]
        public int Quantily { get; set; }
        [DisplayName("Kích Hoạt Quà Tặng")]
        public bool IsActive { get; set; }
        [DisplayName("Ngày Nhập Quà Tặng")]
        public DateTime DateInput { get; set; }
    }
}