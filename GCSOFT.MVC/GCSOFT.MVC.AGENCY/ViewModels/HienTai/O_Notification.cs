﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.ViewModels.HienTai
{
	public class PushNoti {
		public List<int> user_id { get; set; }
		public string image { get; set; }
		public bool seen { get; set; }
		public string title { get; set; }
		public string type { get; set; }
		public string date { get; set; }
		public string name { get; set; }
		public string description { get; set; }
	}

	public class Noti
	{
		public List<int> listUserId { get; set; }
		public string image { get; set; }
		public bool seen { get; set; }
		public string title { get; set; }
		public string type { get; set; }
		public DateTime date { get; set; }
		public string name { get; set; }
		public string description { get; set; }
	}


	public class O_Notification
    {
		public int Id { get; set; }
		[DisplayName("Tiêu đề (*)")]
		public string Name { get; set; }
		[DisplayName("Nội dung thông báo sms")]
		public string ContentSMS { get; set; }
		[DisplayName("Nội dung thông báo mobile")]
		public string ContentMobile { get; set; }
		[AllowHtml]
		[DisplayName("Nội dung gửi email")]
		public string ContentEmail { get; set; }
		[DisplayName("Kiểu thông báo (*)")]
		public string NotificationType { get; set; }
		[DisplayName("Kiểu thông báo (*)")]
		public List<string> NotificationTypeArr { get; set; }
		[DisplayName("Đối tượng nhận thông báo")]
		public string ReceiverType { get; set; }
		[DisplayName("Gửi cho tất cả hiền tài")]
		public bool IsAll { get; set; }
		public DateTime CreateDate { get; set; }
		public int CreateBy { get; set; }
		public string CreateName { get; set; }

		public int TeacherId { get; set; }
		public int SchoolClassId { get; set; }
		public int StudentId { get; set; }

		public IEnumerable<SelectListItem> SchoolClassList { get; set; }
		public IEnumerable<SelectListItem> TeacherList { get; set; }
		public IEnumerable<SelectListItem> StudentList { get; set; }
		
		public List<O_NotificationReceiver> ListNotificationReceivers { get; set; }

		public IEnumerable<SelectListItem> NotificationTypes { get; set; }
	}

	public class O_NotificationPage
	{
		public Paging paging { get; set; }
		public List<O_Notification> Notifications { get; set; }
	}

	public class O_NotificationReceiver
	{ 
		public int NotificationId { get; set; }
		public int UserId { get; set; }
		public int StudentId { get; set; }
		public string IdNo { get; set; }
		public string Email { get; set; }
		public string Phone { get; set; }
		public string Subject { get; set; }
		public DateTime CreateDate { get; set; }
		[AllowHtml]
		public string ContentSMS { get; set; }
		[AllowHtml]
		public string ContentMobile { get; set; }
		[AllowHtml]
		public string ContentEmail { get; set; }
		public bool StatusApp { get; set; }
		public bool StatusSMS { get; set; }
		public bool StatusEMAIL { get; set; }
		public string FullName { get; set; }
		public string ParentName { get; set; }
		public int TeacherId { get; set; }
		public int SchoolClassId { get; set; }
		public bool Select { get; set; }
	}

	public class NotificationResult
	{
		public int NotificationId { get; set; }
		public int UserId { get; set; }
		public int StudentId { get; set; }
		public string IdNo { get; set; }
		public string Email { get; set; }
		public string Phone { get; set; }
		public string Subject { get; set; }
		public string NotificationType { get; set; }
		public DateTime CreateDate { get; set; }
		public string CreateName { get; set; }
		public bool StatusApp { get; set; }
		public bool StatusSMS { get; set; }
		public bool StatusEMAIL { get; set; }
		public string FullName { get; set; }
	}

}