﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.ViewModels.HienTai
{
    public class O_NotificationNumberSend
    {
        public int NumberSendEmail { get; set; }
        public int NumberSendApp { get; set; }
    }

    public class O_NotificationScheduleHistory
    {
        public int NotificationScheduleId { get; set; }
        public string TimeSlotId { get; set; }
        public int StudentId { get; set; }
        public int UserId { get; set; }
        public int TeacherId { get; set; }
        public string Content { get; set; }
        public DateTime CreateDate { get; set; }
        public int CreateBy { get; set; }
        public string Email { get; set; }
        public string NotificationType {get;set;}
        public string ContentEmail { get; set; }
        public string NotificationCategory { get; set; }
    }

    public class O_NotificationScheduleHistoryView
    {
        public int NotificationScheduleId { get; set; }
        public string TimeSlotId { get; set; }
        public int StudentId { get; set; }
        public int UserId { get; set; }
        public int TeacherId { get; set; }
        public string Content { get; set; }
        public DateTime CreateDate { get; set; }
        public int CreateBy { get; set; }
        public string Email { get; set; }
        public string NotificationType { get; set; }
        public string ContentEmail { get; set; }
        public string FullName { get; set; }
        public string Title { get; set; }
        public string ReceiverType { get; set; }
        public string NotificationCategory { get; set; }
    }

    public class O_NotificationSchedule
    {
        public int Id { get; set; }
        [DisplayName("Tiêu đề (*)")]
        public string Name { get; set; }
        [DisplayName("Nội dung Email (*)")]
        [AllowHtml]
        public string ContentEmail { get; set; }
        [DisplayName("Nội dung APP (*)")]
        public string ContentApp { get; set; }
        public string NotificationType { get; set; }
        [DisplayName("Đối tượng nhận thông báo (*)")]
        public string ReceiverType { get; set; }
        public DateTime CreateDate { get; set; }
        public int CreateBy { get; set; }
        public string CreateName { get; set; }
        [DisplayName("Hình thức lặp (*)")]
        public string Loop { get; set; }
        public bool Status { get; set; }
        [DisplayName("Cả ngày (Mặc định là 08:00)")]
        public bool AllDay { get; set; }
        [DisplayName("Kiểu thông báo (*)")]
        public List<string> NotificationTypeArr { get; set; }
        public string TimeSlotInput { get; set; }
        public string TimeSlots { get; set; }
        [DisplayName("Loại thông báo (*)")]
        public string NotificationCategory { get; set; }

        public IEnumerable<SelectListItem> NotificationTypes { get; set; }
        public IEnumerable<SelectListItem> ReceiverTypeTypes { get; set; }
        public IEnumerable<SelectListItem> LoopTypes { get; set; }
        public IEnumerable<SelectListItem> NotificationCats { get; set; }
    }

    public class O_NotificationSchedulePage
    {
        public Paging paging { get; set; }
        public List<O_NotificationSchedule> NotificationSchedules { get; set; }
    }

    public class O_NotificationScheduleHistoryPage
    {
        public Paging paging { get; set; }
        public List<O_NotificationScheduleHistoryView> NotificationSchedules { get; set; }
    }
}