﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.ViewModels.HienTai
{
    public class O_ParentPage
    {
        public string Code { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int TeacherId { get; set; }
        public IEnumerable<SelectListItem> TeacherList { get; set; }
        public Paging paging { get; set; }
        public List<O_ParentList> ParentList { get; set; }
    }
    public class O_ParentList
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public int TeacherId { get; set; }
        public string TeacherName { get; set; }
        public string CodeAcc { get; set; }
    }
    public class O_ParentCreateEdit
    {
        public int Id { get; set; }
        [DisplayName("Mã Phụ Huynh")]
        public string Code { get; set; }
        [DisplayName("Mã Phụ Huynh (Kế Toán)")]
        public string CodeAcc { get; set; }
        [DisplayName("Họ Và Tên Phụ Huynh")]
        public string FullName { get; set; }
        [DisplayName("Email")]
        public string Email { get; set; }
        [DisplayName("Điện thoại")]
        public string Phone { get; set; }
        [DisplayName("Facebook")]
        public string Facebook { get; set; }
        [DisplayName("Tỉnh Thành Phố")]
        public int ProvinceId { get; set; }
        [DisplayName("Quận Huyện")]
        public int DistrictId { get; set; }
        [DisplayName("Tên Người Thụ Hướng")]
        public string Beneficiary { get; set; }
        [DisplayName("Số Tài Khoản")]
        public string BankAccountNo { get; set; }
        [DisplayName("Tên Ngân Hàng")]
        public string BankName { get; set; }
        [DisplayName("Ghi Chú")]
        public string Remark { get; set; }
        [DisplayName("Địa Chỉ")]
        public string Address { get; set; }
        [DisplayName("Vai Trò Giám Hộ")]
        public string GuardianId { get; set; }
        [DisplayName("Giới tính")]
        public string Gender { get; set; }
        [DisplayName("Mô Tả Công Việc")]
        public string JobDesc { get; set; }
        [DisplayName("Đối tác")]
        public int AgencyId { get; set; }
        public bool IsCreate { get; set; }
        [DisplayName("Giáo Viên Đồng Hành")]
        public int TeacherId { get; set; }
        public IEnumerable<SelectListItem> DistrictList { get; set; }
        public IEnumerable<SelectListItem> CityList { get; set; }
        public IEnumerable<O_ParentStudent> ParentStudents { get; set; }
        public IEnumerable<SelectListItem> GuardianList { get; set; }
        public IEnumerable<SelectListItem> GenderList { get; set; }
        public IEnumerable<SelectListItem> TeacherList { get; set; }
        public IEnumerable<SelectListItem> AgencyList { get; set; }
        public O_ParentCreateEdit()
        {
            ParentStudents = new List<O_ParentStudent>();
        }
    }
    public class O_ParentInfo
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string CodeAcc { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Facebook { get; set; }
        public string Gender { get; set; }
        public string JobDesc { get; set; }
        public int ProvinceId { get; set; }
        public int DistrictId { get; set; }
        public string Beneficiary { get; set; }
        public string BankAccountNo { get; set; }
        public string BankName { get; set; }
        public string Remark { get; set; }
        public string Address { get; set; }
        public int AgencyId { get; set; }
        public string GuardianId { get; set; }
        public int TeacherId { get; set; }
    }
    public class O_ParentExport
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string CodeAcc { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Facebook { get; set; }
        public string Gender { get; set; }
        public string JobDesc { get; set; }
        public string DistrictName { get; set; }
        public string ProvinceName { get; set; }
        public string Beneficiary { get; set; }
        public string BankAccountNo { get; set; }
        public string BankName { get; set; }
        public string Remark { get; set; }
        public string Address { get; set; }
        public int AgencyId { get; set; }
        public string Guardian { get; set; }
        public int TeacherId { get; set; }
    }
    public class O_ParentStudent
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string FullName { get; set; }
        public string DateOfBirth { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string GradeName { get; set; }
    }
    public class O_ParentDetails
    {
        public int Id { get; set; }
        [DisplayName("Mã Phụ Huynh")]
        public string Code { get; set; }
        [DisplayName("Họ Và Tên Phụ Huynh")]
        public string FullName { get; set; }
        [DisplayName("Email")]
        public string Email { get; set; }
        [DisplayName("Điện thoại")]
        public string Phone { get; set; }
        [DisplayName("Facebook")]
        public string Facebook { get; set; }
        [DisplayName("Tỉnh Thành Phố")]
        public string ProvinceName { get; set; }
        [DisplayName("Quận Huyện")]
        public string DistrictName { get; set; }
        [DisplayName("Tên Người Thụ Hướng")]
        public string Beneficiary { get; set; }
        [DisplayName("Số Tài Khoản")]
        public string BankAccountNo { get; set; }
        [DisplayName("Tên Ngân Hàng")]
        public string BankName { get; set; }
        [DisplayName("Ghi Chú")]
        public string Remark { get; set; }
        [DisplayName("Địa Chỉ")]
        public string Address { get; set; }
        [DisplayName("Vai Trò Giám Hộ")]
        public string GuardianName { get; set; }
        [DisplayName("Giới tính")]
        public string GenderName { get; set; }
        [DisplayName("Mô Tả Công Việc")]
        public string JobDesc { get; set; }
        public int AgencyId { get; set; }
        public IEnumerable<O_ParentStudent> ParentStudents { get; set; }
        public O_ParentDetails()
        {
            ParentStudents = new List<O_ParentStudent>();
        }
    }
}