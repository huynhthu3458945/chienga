﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.ViewModels.HienTai
{
    public class O_ProcessHeader
    {
        public int Id { get; set; }
        public int StudentStarId { get; set; }
        public int StudentId { get; set; }
        public int StarId { get; set; }
        public int ProgramCode { get; set; }
        public int ProgramName { get; set; }
        public int Title { get; set; }
        public int StartDateProcess { get; set; }
        public int EndDateProcess { get; set; }
        public int NoOfTimes { get; set; }
    }

    public class O_ProcessHeaderView
    {
        public int Id { get; set; }
        public int StudentStarId { get; set; }
        public int StudentId { get; set; }
        [DisplayName("Tên hiền tài")]
        public string StudentName { get; set; }
        public int StarId { get; set; }
        [DisplayName("Tên sao")]
        public string StarName { get; set; }
        public string ProgramCode { get; set; }
        [DisplayName("Chương trình")]
        public string ProgramName { get; set; }
        [DisplayName("Lần thực hành hiện tại ")]
        public string Title { get; set; }
        [DisplayName("Ngày bắt đầu")]
        public DateTime StartDateProcess { get; set; }
        [DisplayName("Ngày kết thúc")]
        public DateTime EndDateProcess { get; set; }
        public int NoOfTimes { get; set; }
        public IEnumerable<SelectListItem> ProcessHeaders { get; set; }

        public Paging paging { get; set; }
        public List<O_ProcessLine> ProcessLines {get; set;}
    }
}