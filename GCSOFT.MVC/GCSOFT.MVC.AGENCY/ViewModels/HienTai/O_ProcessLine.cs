﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.AGENCY.ViewModels.HienTai
{
    public class O_ProcessLine
    {
        public int Id { get; set; }
        public int ProcessHeaderId { get; set; }
        public int StudentStarId { get; set; }
        public int StudentId { get; set; }
        public int StarId { get; set; }
        public string ProgramCode { get; set; }
        public string StudentName { get; set; }
        public string StudentAvartar { get; set; }
        public string ProcessContent { get; set; }
        public DateTime DateInput { get; set; }
        public string Files { get; set; }
        public int StarType { get; set; }
    }

}