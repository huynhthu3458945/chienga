﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.ViewModels.HienTai
{
    public class O_Reserve
    {
        public string Code { get; set; }
        public string ReserveCode { get; set; }
        public string ReserveText { get; set; }
        public DateTime DateRequest { get; set; }
        public DateTime DateReserveStart { get; set; }
        public DateTime DateReserveEnd { get; set; }
        public string Content { get; set; }
        public string StatusName { get; set; }
        public string StudentName { get; set; }
    }
    public class O_ReserveCreateEdit
    {
        public int Id { get; set; }

        [DisplayName("Mã Kích Hoạt")]
        public int ActivationCode { get; set; }

        [DisplayName("IDNo")]
        public string IDNo { get; set; }

        [DisplayName("Họ Và Tên Học Viên")]
        public int StudentId { get; set; }

        [DisplayName("Họ Và Tên Phụ Huynh")]
        public int ParentId { get; set; }

        [DisplayName("Số Điện Thoại")]
        public string PhoneNumber { get; set; }

        [DisplayName("Đã Kích Hoạt Tài Khoản")]
        public bool IsActive { get; set; }

        [DisplayName("Trạng Thái Gửi Mã Kích Hoạt")]
        public bool NoOfSendSMS { get; set; }

        [DisplayName("Người Kích Hoạt")]
        public int ActiveBy { get; set; }

        [DisplayName("Ngày Kích Hoạt")]
        public DateTime DateActive { get; set; }
    }
    public class O_ReserveInfo
    {
        public string Code { get; set; }
        public string ReserveCode { get; set; }
        public string ReserveText { get; set; }
        public DateTime DateRequest { get; set; }
        public DateTime DateReserveStart { get; set; }
        public DateTime DateReserveEnd { get; set; }
        public DateTime DateJourney { get; set; }
        public DateTime DateExtend { get; set; }
        public string Content { get; set; }
        public string Answer { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }
        public string FilesRequest { get; set; }
        public string FilesResponse { get; set; }
        public int StudentId { get; set; }
    }

    public class O_ReserveDetails
    {
        [DisplayName("Mã Bảo Lưu")]
        public string Code { get; set; }

        [DisplayName("Trạng Thái Bảo Lưu")]
        public string ReserveText { get; set; }

        [DisplayName("Thời Gian Yêu Cầu")]
        public DateTime DateRequest { get; set; }

        [DisplayName("Thời Gian Bắt Đầu")]
        public DateTime DateReserveStart { get; set; }

        [DisplayName("Thời Gian Kết Thúc")]
        public DateTime DateReserveEnd { get; set; }

        [DisplayName("Thời Gian Hành Trình")]
        public DateTime DateJourney { get; set; }

        [DisplayName("Thời Gian Gia Hạn")]
        public DateTime DateExtend { get; set; }

        [DisplayName("Lý Do Bảo Lưu")]
        public string Content { get; set; }

        [DisplayName("Phản Hồi Lý Do")]
        public string Answer { get; set; }

        [DisplayName("Trạng Thái Duyệt")]
        public string StatusName { get; set; }

        [DisplayName("File Yêu Cầu")]
        public string FilesRequest { get; set; }

        [DisplayName("File Phản Hồi")]
        public string FilesResponse { get; set; }

        [DisplayName("Học Viên Bảo Lưu")]
        public string StudentName { get; set; }
    }
    public class O_ReserveStatus
    {
        public string StatusCode { get; set; }
        public string StatusName { get; set; }
    }
}