﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.ViewModels.HienTai
{
    public class O_SchoolClassList
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string TeacherName { get; set; }
        public DateTime OpeningDay { get; set; }
        public DateTime ClosingDay { get; set; }
        public int Total{ get; set; }
    }
    public class O_SchoolClassListPage
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int TeacherId { get; set; }
        public IEnumerable<SelectListItem> TeacherList { get; set; }
        public Paging paging { get; set; }
        public List<O_SchoolClassList> SchoolClasList { get; set; }
    }
    public class O_SchoolClassCreateEdit
    {
        public int Id { get; set; }
        [DisplayName("Mã Lớp")]
        public string Code { get; set; }
        [DisplayName("Khóa Lớp")]
        public string Name { get; set; }
        [DisplayName("Giáo Viên Đồng Hành")]
        public int? TeacherId { get; set; }
        public int AgencyId { get; set; }
        [DisplayName("Ngày Mở Lớp")]
        public DateTime OpeningDay { get; set; }
        public string OpeningDayString { get; set; }
        [DisplayName("Ngày Kết Thúc")]
        public DateTime ClosingDay { get; set; }
        public string ClosingDayString { get; set; }
        [DisplayName("Số Lượng Học Viên")]
        public int Total { get; set; }
        public IEnumerable<SelectListItem> TeacherList { get; set; }
        [DisplayName("Lớp Học")]
        public int ClassId { get; set; }
        public IEnumerable<SelectListItem> ClassList { get; set; }
        [DisplayName("Cấp làm SAO (*)")]
        public int[] LevelArrs { get; set; }
        public IEnumerable<SelectListItem> LevelList { get; set; }
        public IEnumerable<SchoolClassStar> SchoolClassStars { get; set; }
        public O_SchoolClassCreateEdit()
        {
            SchoolClassStars = new List<SchoolClassStar>();
        }
    }
    public class O_SchoolClassLevel
    {
        public int LevelId { get; set; }
        public IEnumerable<SelectListItem> LevelList { get; set; }
    }
    public class O_SchoolClassInfo
    {
        public int Id { get; set; }
        [DisplayName("Mã Lớp")]
        public string Code { get; set; }
        [DisplayName("Tên Lớp")]
        public string Name { get; set; }
        [DisplayName("Giáo Viên")]
        public int? TeacherId { get; set; }
        public int AgencyId { get; set; }
        public DateTime OpeningDay { get; set; }
        public DateTime ClosingDay { get; set; }
        public int Total { get; set; }
        public int ClassId { get; set; }
    }
    public class O_SchoolClassDetails
    {
        public int Id { get; set; }
        [DisplayName("Mã Lớp")]
        public string Code { get; set; }
        [DisplayName("Tên Lớp")]
        public string Name { get; set; }
        [DisplayName("Mã Giáo Viên")]
        public int TeacherId { get; set; }
        [DisplayName("Tên Giáo Viên")]
        public string TeacherName { get; set; }
        public int AgencyId { get; set; }
        [DisplayName("Ngày Mở Lớp")]
        public DateTime OpeningDay { get; set; }
        public string OpeningDayString { get; set; }
        [DisplayName("Ngày Kết Thúc")]
        public DateTime ClosingDay { get; set; }
        public string ClosingDayString { get; set; }
        [DisplayName("Số Lượng Học Viên")]
        public int Total { get; set; }
        [DisplayName("Cấp làm SAO")]
        public int[] LevelArrs { get; set; }
        public Paging paging { get; set; }
        public List<O_StudentList> StudentList { get; set; }
    }
    public class LevelStar
    {
        public int LevelId { get; set; }
        public int SchoolClassId { get; set; }
        public string LevelName { get; set; }
    }
    public class LevelStarViewModel
    {
        public int Id { get; set; }
        public string LevelName { get; set; }
    }
    public class SchoolClassStar
    {
        public int SchoolClassId { get; set; }
        public int StarId { get; set; }
        public string BasicName { get; set; }
        public string QualityName { get; set; }
        public string StartName { get; set; }
        public int LevelId { get; set; }
        public string levelName { get; set; }
    }
    public class SchoolClassValidate
    {
        public string Id { get; set; }
        public string ClassId { get; set; }
        public string OpeningDay { get; set; }
        public string ClosingDay { get; set; }
    }
}