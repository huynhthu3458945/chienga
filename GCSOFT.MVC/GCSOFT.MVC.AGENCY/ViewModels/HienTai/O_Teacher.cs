﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.AGENCY.ViewModels.HienTai
{
    public class O_TeacherListPage
    {
        public string Code { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public Paging paging { get; set; }
        public List<O_TeacherList> TeacherList { get; set; }
    }
    public class O_TeacherList
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int Level { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string RoleCode { get; set; }
        public string RoleName { get; set; }
        public string ParentName { get; set; }
        public int? ParentTeacherId { get; set; }
        public string GenderName { get; set; }
        public DateTime WorkDate { get; set; }
    }
    public class O_TeacherCreateEdit
    {
        public int Id { get; set; }
        [DisplayName("Mã Giáo Viên")]
        public string Code { get; set; }
        public int? TeacherType { get; set; }
        [DisplayName("Họ Và Tên")]
        public string FullName { get; set; }
        [DisplayName("Điện Thoại")]
        public string Phone { get; set; }
        [DisplayName("Email")]
        public string Email { get; set; }
        [DisplayName("Tên Đăng Nhập")]
        public string Username { get; set; }
        public string xUserName { get; set; }
        public DateTime WorkDate { get; set; }
        [DisplayName("Quản Lý")]
        public int? ParentTeacherId { get; set; }
        public int AgencyId { get; set; }
        [DisplayName("Chức Vụ")]
        public string RoleCode { get; set; }
        public string xRoleCode { get; set; }
        public bool HasAccount { get; set; }
        public IEnumerable<SelectListItem> RoleList { get; set; }
        public IEnumerable<SelectListItem> ParentTeacherList { get; set; }
        [DisplayName("Giới Tính")]
        public string Gender { get; set; }
        public IEnumerable<SelectListItem> GenderList { get; set; }
    }
    public class O_TeacherInfo
    {
        public int Id { get; set; }
        [DisplayName("Mã Giáo Viên")]
        public string Code { get; set; }
        public int? TeacherType { get; set; }
        [DisplayName("Họ Và Tên")]
        public string FullName { get; set; }
        [DisplayName("Điện Thoại")]
        public string Phone { get; set; }
        [DisplayName("Email")]
        public string Email { get; set; }
        [DisplayName("Tên Đăng Nhập")]
        public string Username { get; set; }
        public string xUserName { get; set; }
        public DateTime WorkDate { get; set; }
        [DisplayName("Quản Lý")]
        public int? ParentTeacherId { get; set; }
        public int AgencyId { get; set; }
        [DisplayName("Chức Vụ")]
        public string RoleCode { get; set; }
        public string xRoleCode { get; set; }
        public bool HasAccount { get; set; }
        public int NoOfOutlier { get; set; }
        [DisplayName("Giới Tính")]
        public string Gender { get; set; }
    }

    public class O_TeacherDetails
    {
        public int Id { get; set; }
        [DisplayName("Mã Giáo Viên")]
        public string Code { get; set; }
        public int? TeacherType { get; set; }
        [DisplayName("Họ Và Tên")]
        public string FullName { get; set; }
        [DisplayName("Điện Thoại")]
        public string Phone { get; set; }
        [DisplayName("Email")]
        public string Email { get; set; }
        [DisplayName("Tên Đăng Nhập")]
        public string Username { get; set; }
        public string xUserName { get; set; }
        public DateTime WorkDate { get; set; }
        [DisplayName("Quản Lý")]
        public int? ParentTeacherId { get; set; }
        public int AgencyId { get; set; }
        [DisplayName("Chức Vụ")]
        public string RoleCode { get; set; }
        public string xRoleCode { get; set; }
        public bool HasAccount { get; set; }
        public int NoOfOutlier { get; set; }
        public string Gender { get; set; }
        [DisplayName("Giới Tính")]
        public string GenderName { get; set; }
        [DisplayName("Chức vụ")]
        public string RoleName { get; set; }
        [DisplayName("Quản Lý")]
        public string ParentName { get; set; }
        [DisplayName("Lớp Học")]
        public int SchoolClassStarId { get; set; }
        public string SchoolClassStarName { get; set; }
        public IEnumerable<SelectListItem> SchoolClassStarList { get; set; }
        public Paging paging { get; set; }
        public List<O_StudentList> StudentList { get; set; }
    }
    public class NoOfLevelAndOutlier
    {
        public int NoOfOutlier { get; set; }
        public int NoOfLevel { get; set; }
    }
}