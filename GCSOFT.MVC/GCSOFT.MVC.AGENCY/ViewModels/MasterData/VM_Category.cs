﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.ViewModels.MasterData
{
    public class VM_Category_Index
    {
        public int Id { get; set; }
        [DisplayName("Tên Danh Mục")]
        public string SearchName { get; set; }
        [DisplayName("Liên Kết")]
        public string LinkWeb { get; set; }
        [DisplayName("Cấp")]
        public int LevelCategory { get; set; }
        public string LevelStr { get; set; }
        [DisplayName("Thứ Tự")]
        public int SortOrder { get; set; }
        [DisplayName("Trang Chủ")]
        public bool Status { get; set; }
        [DisplayName("Trang Chủ")]
        public string StatusStr
        {
            get
            {
                if (!Status)
                    return "<a name=\"btnShow\" href=\"javascript: void(0);\" data-uk-tooltip=\"{ pos: 'left'}\" title=\"Chưa hiển Trị Trên Web\"><i class=\"md-icon material-icons uk-text-danger\">&#xE7F3;</i></a>";
                return "<a name=\"btnShow\" href=\"javascript: void(0);\" data-uk-tooltip=\"{ pos: 'left'}\" title=\"Đã Hiển Thị Trên Web\"><i class=\"md-icon material-icons uk-text-primary\">&#xE7F2;</i></a>";
            }
        }
    }
    public class CategoryMenu
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? ParentNo { get; set; }
        public int SortOrder { get; set; }
    }
    public class VM_Category
    {
        public int Id { get; set; }
        [DisplayName("Tên Danh Mục")]
        public string Name { get; set; }
        [DisplayName("Liên Kết")]
        public string LinkWeb { get; set; }
        [DisplayName("Danh Mục Cha")]
        public int? ParentNo { get; set; }
        public int? xParentNo { get; set; }
        [DisplayName("Diễn Giải")]
        public string Description { get; set; }
        [DisplayName("Thứ Tự")]
        public int SortOrder { get; set; }
        [DisplayName("Trang Chủ")]
        public bool Status { get; set; }
        
        public int LevelCategory { get; set; }
        public int Level { get; set; }
        public string LevelStr { get; set; }
        [DisplayName("Đường Dẫn")]
        public string SearchName { get; set; }
        public string BreadCrumb { get; set; }
        public IEnumerable<SelectListItem> ParentCategorys { get; set; }
    }
}