﻿using System.ComponentModel;

namespace GCSOFT.MVC.Web.ViewModels.MasterData
{
    public class VM_OptionLine
    {
        public int OptionHeaderID { get; set; }
        public int LineNo { get; set; }
        public string OptionHeaderCode { get; set; }
        public string OptionHeaderName { get; set; }
        [DisplayName("Mã")]
        public string Code { get; set; }
        [DisplayName("Tên")]
        public string Name { get; set; }
        [DisplayName("Thứ Tự")]
        public int Order { get; set; }
        [DisplayName("Ẩn/Hiện")]
        public bool IsHidden { get; set; }
    }
    public class OptionInt
    {
        public int Key { get; set; }
        public string Value { get; set; }
    }
    public class OptionString
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}