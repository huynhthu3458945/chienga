﻿using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.ViewModels.MasterData
{
    public class TeacherList
    {
        public int Id { get; set; }
        [DisplayName("Họ Tên")]
        public string FullName { get; set; }
        [DisplayName("Email")]
        public string Email { get; set; }
        [DisplayName("Điện Thoại")]
        public string PhoneNo { get; set; }
    }
    public class TeacherCard
    {
        public int Id { get; set; }
        [DisplayName("Tên")]
        public string FirstName { get; set; }
        [DisplayName("Họ")]
        public string LastName { get; set; }
        [DisplayName("Họ Tên")]
        public string FullName { get; set; }
        [DisplayName("Điện Thoại")]
        public string PhoneNo { get; set; }
        [DisplayName("Email")]
        public string Email { get; set; }
        [DisplayName("Chức Danh")]
        [AllowHtml]
        public string Remark { get; set; }
        [DisplayName("Hình Ảnh (*.jpg W:450 x H:450)px")]
        public string Image { get; set; }
        public string NewPhoneNo
        {
            get { return RebuildPhoneNo(PhoneNo); }
        }
        private string RebuildPhoneNo(string phoneNo)
        {
            string newNumber = string.Empty;
            string[] numbers = Regex.Split(phoneNo, @"\D+");
            foreach (string value in numbers)
            {
                if (!string.IsNullOrEmpty(value))
                {
                    newNumber += value;
                }
            }
            return newNumber;
        }
    }
}