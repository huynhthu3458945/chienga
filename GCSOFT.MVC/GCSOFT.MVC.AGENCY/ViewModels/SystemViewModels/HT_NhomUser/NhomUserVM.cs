﻿using System.Collections.Generic;
using System.ComponentModel;

namespace GCSOFT.MVC.Web.ViewModels.SystemViewModels
{
    public class NhomUsers
    {
        [DisplayName("Mã Nhóm")]
        public string Code { get; set; }
        [DisplayName("Tên Nhóm")]
        public string Name { get; set; }
        [DisplayName("Diễn Giải")]
        public string Description { get; set; }
    }

    public class NhomUserCard : NhomUsers
    {
        public virtual IEnumerable<UserThuocNhomVM> UserOfGroups { get; set; }
        public NhomUserCard()
        {
            UserOfGroups = new List<UserThuocNhomVM>();
        }
    }

    public class UserThuocNhomVM : UserVM
    {
        public string UserGroupCode { get; set; }
        public int UserID { get; set; }
        public NhomUsers NhomUserInfo { get; set; }
    }
}