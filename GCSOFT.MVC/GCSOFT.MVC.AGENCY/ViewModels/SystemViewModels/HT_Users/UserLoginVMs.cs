﻿using GCSOFT.MVC.AGENCY.Areas.Agency.Data;
using GCSOFT.MVC.AGENCY.ViewModels.HienTai;
using System.ComponentModel;

namespace GCSOFT.MVC.Web.ViewModels.SystemViewModels
{
    public class UserLoginVMs
    {
        public int ID { get; set; }
        [DisplayName("Tên đăng nhập")]
        public string Username { get; set; }
        [DisplayName("Tên người sử dụng")]
        public string FullName { get; set; }
        [DisplayName("Mật khẩu")]
        public string Password { get; set; }
        [DisplayName("Nhập lại mật khẩu")]
        public string RePassword { get; set; }
        public AgencyList agencyInfo { get; set; }
        public AgencyList staffInfo { get; set; }
        public O_TeacherInfo teacherInfo { get; set; }
    }
}