﻿using System.Collections.Generic;
using System.ComponentModel;

namespace GCSOFT.MVC.Web.ViewModels.SystemViewModels
{
    public class UserVM
    {
        public int ID { get; set; }
        [DisplayName("Username (*)")]
        public string Username { get; set; }
        [DisplayName("Mật Khẩu (*)")]
        public string Password { get; set; }
        [DisplayName("Email (*)")]
        public string Email { get; set; }
        [DisplayName("Điện Thoại (*)")]
        public string Phone { get; set; }
        [DisplayName("Ghi Chú")]
        public string Description { get; set; }
        [DisplayName("Chức Vụ")]
        public string Position { get; set; }
        [DisplayName("Họ (*)")]
        public string FirstName { get; set; }
        [DisplayName("Tên (*)")]
        public string LastName { get; set; }
        [DisplayName("Họ Tên")]
        public string FullName { get; set; }
        [DisplayName("Địa Chỉ (*)")]
        public string Address { get; set; }
    }

    public class UserList : UserVM
    {
        [DisplayName("Sử Dụng")]
        public bool Blocked { get; set; }
        public string BlockedStr
        {
            get
            {
                if (!Blocked)
                    return "<a name=\"btnShow\" href=\"javascript: void(0);\" data-uk-tooltip=\"{ pos: 'left'}\" title=\"Đang Sử Dụng\"><i class=\"md-icon material-icons uk-text-primary\">&#xE8F4;</i></a>";
                return "<a name=\"btnShow\" href=\"javascript: void(0);\" data-uk-tooltip=\"{ pos: 'left'}\" title=\"Đã Khóa\"><i class=\"md-icon material-icons uk-text-danger\">&#xE8F5;</i></a>";
            }
        }
    }

    public class UserCard : UserVM
    {
        [DisplayName("Khóa")]
        public bool Blocked { get; set; }
        [DisplayName("Phân Quyền")]
        public string[] userGroupCodes { get; set; }
        public IEnumerable<NhomUsers> userGroups { get; set; }
        [DisplayName("Nhập Lại Mật Khẩu (*)")]
        public string RePassword { get; set; }
        public IEnumerable<UserThuocNhomVM> UserOfGroups { get; set; }
        public int MyProperty { get; set; }
        public UserCard()
        {
            ID = 0;
            UserOfGroups = new List<UserThuocNhomVM>();
        }
    }
}