﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.AGENCY.ViewModels.dtht
{
    public class ApiResults<T>
    {
        public int retCode { get; set; }
        public string retText { get; set; }
        public List<T> Data { get; set; }
    }
    public class ApiResult<T>
    {
        public int retCode { get; set; }
        public string retText { get; set; }
        public T Data { get; set; }
    }
}