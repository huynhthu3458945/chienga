﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.AGENCY.ViewModels.dtht
{
    public class DTHTPage
    {
        public string IDNo { get; set; }
        public IEnumerable<DocumentList> DocumentList { get; set; }
        public IEnumerable<ParentLeadList> ParentList { get; set; }
        public IEnumerable<StudentLeadList> StudentList { get; set; }
    }
}