﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.AGENCY.ViewModels.dtht
{
    public class O_ParentOrigin
    {
        public int Id { get; set; }
        public int StudentId { get; set; }
        public string IdNo { get; set; }
        public int ParentId { get; set; }
        public string ParentCode { get; set; }
        public string TenDaiLy { get; set; }
        public string CapDaiLy { get; set; }
        public string DanhXung { get; set; }
        public bool IsChaMeHienTai { get; set; }
        public decimal? DiemChaMeHienTai { get; set; }
        public string HoVaTenPH1 { get; set; }
        public string MoiQuanHePH1 { get; set; }
        public string SDTPH1 { get; set; }
        public string EmailPH1 { get; set; }
        public string NgaySinhPH1 { get; set; }
        public string GioiTinhPH1 { get; set; }
        public string FacebookPH1 { get; set; }

        public string HoTenPH2 { get; set; }
        public string MoiQuanHePH2 { get; set; }
        public string SDTPH2 { get; set; }
        public string EmailPH2 { get; set; }
        public string NgaySinhPH2 { get; set; }
        public string GioiTinhPH2 { get; set; }
        public string FacebookPH2 { get; set; }

        public int NamHoc { get; set; }
        public string GhiChu { get; set; }
        public string DiaChi { get; set; }
        public string DiaChiNhanQua { get; set; }
        public string TenNganHang { get; set; }
        public string SoTaiKhoan { get; set; }
        public string ChuTaiKhoan { get; set; }
        public string NgheNghiep { get; set; }
        public string SoThich { get; set; }
        public string TinhTrangHonNhan { get; set; }
        public int SoNguoiCon { get; set; }
        public string HinhAnhGiaDinh { get; set; }
        public string ThoiGianLienHe { get; set; }
        public string DongLucThamGia { get; set; }
        public string MongMuon { get; set; }
        public string KhoKhanCuocSong { get; set; }
        public string TuMoTa { get; set; }
        public string PhanMemSuDung { get; set; }
        public string ThoiGianChoCon { get; set; }
        public string GopYKien { get; set; }
        public int SoConThamGiaHienTai { get; set; }
        public int ThoiGianHoc { get; set; }
        public string HoVaTenHienTai { get; set; }
        public string GiayKhaiSinh { get; set; }
        public string NgaySinhHienTai { get; set; }
        public string GioiTinhHienTai { get; set; }
        public string TruongHocHienTai { get; set; }
        public string SizeAo { get; set; }
        public string ThanhTichHocTap { get; set; }
        public string DieuTuHao { get; set; }
        public string DiemYeu { get; set; }
        public string UocMo { get; set; }
        public string SoThichHienTai { get; set; }
        public string ThoiQuen { get; set; }
        public string KhoKhanOTruong { get; set; }
        public string KhoKhanONha { get; set; }
        public string LoLangHienTai { get; set; }
        public string NguoiGiupDo { get; set; }
        public string GiaiQuyetHienTai { get; set; }
        public string NguoiTamSu { get; set; }
        public string TuMoTaHienTai { get; set; }
        public string QuaHientai { get; set; }
        public string Channel { get; set; }
        
    }
}