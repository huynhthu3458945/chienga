﻿function gridSearch(gridId, buttonClass, isMultipleSearch) {
    isMultipleSearch = isMultipleSearch == null ? true : isMultipleSearch;
    $("." + buttonClass).click(function () {
        jQuery("#" + gridId).jqGrid('searchGrid', {
            closeOnEscape: true,
            multipleSearch: isMultipleSearch,
            closeAfterSearch: true,
            showQuery: false,
            searchOnEnter: true,
            sopt: ['cn', 'eq', 'ne', 'lt', 'le', 'gt', 'ge']
        });
    });
}

function jqGrid_resize(gridID) {
    $(window).bind('resize', function () {
        $('#' + gridID).setGridWidth($(window).width() - 83, true); //Resized to new width as per window
    }).trigger('resize');
}

function JqEdit(path, gridId) {
    var value = getSelectedValue(gridId);
    if (value) {
        location.href = path + '/' + value;
    }
    else { $().toastmessage('showWarningToast', 'Không có dòng nào được chọn'); }
}

function JqDel(parth, objGridId) {
    var seletedValue = objGridId.jqGrid('getGridParam', 'selarrrow');
    if (seletedValue != '') {
        UIkit.modal.confirm('Bạn có chắc chắn muốn xóa?', function () {
            $.post(parth, { id: seletedValue.toString() }, function (data) {
                if (data.v) {
                    $().toastmessage('showSuccessToast', "Đã xóa <strong>" + data.count + "</strong> dòng dữ liệu");
                    objGridId.trigger("reloadGrid");
                }
                else {
                    $().toastmessage('showErrorToast', data);
                    objGridId.trigger("reloadGrid");
                }
            }, 'json');
        });
    } else { $().toastmessage('showWarningToast', 'Không có dòng nào được chọn'); }
}

function JqDelValidate(parthCheck, parth, objGridId) {
    var seletedValue = objGridId.jqGrid('getGridParam', 'selarrrow');
    if (seletedValue != '') {
        UIkit.modal.confirm('Bạn có chắc chắn muốn xóa?', function () {
            $.post(parthCheck, { ids: seletedValue.toString() }, function (data) {
                var jData = jQuery.parseJSON(JSON.stringify(data));
                if (jData.c > 0) {
                    $.each(jData.d, function (index, obj) {
                        $().toastmessage('showToast', {
                            text: obj,
                            sticky: true,
                            position: 'top-center',
                            type: 'warning'
                        });
                    });
                } else {
                    $.post(parth, { id: seletedValue.toString() }, function (data) {
                        var jData = jQuery.parseJSON(JSON.stringify(data));
                        if (jData.v) {
                            $().toastmessage('showSuccessToast', "Đã xóa <strong>" + jData.count + "</strong> dòng dữ liệu");
                            objGridId.trigger("reloadGrid");
                        }
                        else {
                            $().toastmessage('showErrorToast', jData);
                            objGridId.trigger("reloadGrid");
                        }
                    }, 'json');
                }
            }, 'json');
        });
    } else { $().toastmessage('showWarningToast', 'Không có dòng nào được chọn'); }
}

//LẤY ID ĐƯỢC CHỌN TRÊN GRID
function getSelectedValue(gridId) {
    return $("#" + gridId).jqGrid('getGridParam', 'selrow');
}