﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Data.Common
{
    public static class SqlHelper
    {
        public static IEnumerable<TReturn> CallStoredProcedure<TParameters, TReturn>(this GcEntities context, string storedProcedure, TParameters parameters) where TParameters : class where TReturn : class, new()
        {
            IDictionary<string, object> procedureParameters = new Dictionary<string, object>();
            PropertyInfo[] properties = parameters.GetType().GetProperties();

            var ps = new List<object>();

            foreach (var property in properties)
            {
                object value = property.GetValue(parameters);
                string name = property.Name;

                procedureParameters.Add(name, value);

                ps.Add(new SqlParameter(name, value));
            }

            var keys = procedureParameters.Select(p => string.Format("@{0}", p.Key)).ToList();
            var parms = string.Join(", ", keys.ToArray());
            return context.Database.SqlQuery<TReturn>(storedProcedure + " " + parms, ps.ToArray()).ToList();
        }
    }
}
