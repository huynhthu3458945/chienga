﻿using GCSOFT.MVC.Model.SystemModels;
using System.Data.Entity.ModelConfiguration;

namespace GCSOFT.MVC.Data.Configuration.SystemConfig
{
    public class CongViecVaVuViecConfig : EntityTypeConfiguration<HT_CongViecVaVuViec>
    {
        public CongViecVaVuViecConfig()
        {
            ToTable("Sys_Role");
            Property(p => p.CategoryCode).HasMaxLength(20).HasColumnType("varchar");
            Property(p => p.FunctionCode).HasMaxLength(20).HasColumnType("varchar");
            Property(p => p.UserGroupCode).HasMaxLength(20).HasColumnType("varchar");
        }
    }
}