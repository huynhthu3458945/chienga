﻿using GCSOFT.MVC.Model.SystemModels;
using System.Data.Entity.ModelConfiguration;

namespace GCSOFT.MVC.Data.Configuration.SystemConfig
{
    public class NhomUserConfig : EntityTypeConfiguration<HT_NhomUser>
    {
        public NhomUserConfig()
        {
            ToTable("Sys_UserGroup");
            Property(p => p.Code).HasMaxLength(20).HasColumnType("varchar");
            Property(p => p.Name).HasMaxLength(100);
            Property(p => p.Description).HasMaxLength(100);
        }
    }
}
