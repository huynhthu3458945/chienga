﻿using GCSOFT.MVC.Model;
using GCSOFT.MVC.Model.SystemModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace GCSOFT.MVC.Data.Configuration
{
    public class VuViecConfig : EntityTypeConfiguration<HT_VuViec>
    {
        public VuViecConfig()
        {
            ToTable("Sys_Function");
            Property(p => p.Code).HasMaxLength(20).HasColumnType("varchar");
            Property(p => p.Name).HasMaxLength(50);
            Property(p => p.Description).HasMaxLength(100);
        }
    }
}