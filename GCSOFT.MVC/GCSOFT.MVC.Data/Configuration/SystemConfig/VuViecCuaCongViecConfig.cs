﻿using GCSOFT.MVC.Model.SystemModels;
using System.Data.Entity.ModelConfiguration;

namespace GCSOFT.MVC.Data.Configuration.SystemConfig
{
    public class VuViecCuaCongViecConfig : EntityTypeConfiguration<HT_VuViecCuaCongViec>
    {
        public VuViecCuaCongViecConfig()
        {
            ToTable("Sys_CategoryFunction");
            Property(p => p.CategoryCode).HasMaxLength(20).HasColumnType("varchar");
            Property(p => p.FunctionCode).HasMaxLength(20).HasColumnType("varchar");
        }
    }
}
