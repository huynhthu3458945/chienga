﻿using GCSOFT.MVC.Data.Configuration;
using GCSOFT.MVC.Data.Configuration.MasterDataConfig;
using GCSOFT.MVC.Data.Configuration.SystemConfig;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Model.App;
using GCSOFT.MVC.Model.Approval;
using GCSOFT.MVC.Model.FrontEnd;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.NamPhutThuocBai;
using GCSOFT.MVC.Model.NewSocial;
using GCSOFT.MVC.Model.QuickQuiz;
using GCSOFT.MVC.Model.Report;
using GCSOFT.MVC.Model.SystemModels;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Model.Videos;
using System.Data.Entity;
namespace GCSOFT.MVC.Data
{
    public class GcEntities : DbContext
    {
        //public GcEntities() : base("GCConnection") {
        //    Database.SetInitializer(new MigrateDatabaseToLatestVersion<GcEntities, Migrations.Configuration>("GCConnection"));
        //}
        public GcEntities() : base("GCConnection")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<GcEntities, GCSOFT.MVC.Data.Migrations.Configuration>());
        }
        public DbSet<HT_CongViec> CongViecs { get; set; }
        public DbSet<HT_VuViec> VuViecs { get; set; }
        public DbSet<HT_VuViecCuaCongViec> VuViecCuaCongViecs { get; set; }
        public DbSet<HT_NhomUser> NhomUsers { get; set; }
        public DbSet<HT_CongViecVaVuViec> CongViecVaVuViecs { get; set; }
        public DbSet<HT_Users> Users { get; set; }
        public DbSet<HT_UserThuocNhom> UserThuocNhom { get; set; }
        public DbSet<M_OptionType> OptionTypes { get; set; }
        public DbSet<M_OptionLine> OptionLines { get; set; }
        public DbSet<E_Category> Categories { get; set; }
        public DbSet<E_Teacher> Teachers { get; set; }
        public DbSet<E_Class> Classes { get; set; }
        public DbSet<E_Course> Courses { get; set; }
        public DbSet<E_CourseContent> CourseContents { get; set; }
        public DbSet<E_CourseSupport> CourseSupports { get; set; }
        public DbSet<E_CourseCategory> CourseCategories { get; set; }
        public DbSet<E_CourseResource> CourseResources { get; set; }
        public DbSet<M_Parents> Parentses { get; set; }
        public DbSet<M_Contact> Contacts { get; set; }
        public DbSet<M_Children> Childrens { get; set; }
        public DbSet<M_Question> Questions { get; set; }
        public DbSet<M_ResultEntry> ResultEntries { get; set; }
        public DbSet<M_Vote> Votes { get; set; }
        public DbSet<M_Comment> Comments { get; set; }
        public DbSet<M_UserInfo> UserInfos { get; set; }
        public DbSet<M_UserInfoActive> UserInfoActives { get; set; }
        public DbSet<M_FileResource> FileResources { get; set; }
        public DbSet<M_Setup> Setups { get; set; }
        public DbSet<M_Supporter> Supporters { get; set; }
        public DbSet<E_ClassRefer> ClassRefers { get; set; }
        public DbSet<M_StudentClass> StudentClasses { get; set; }
        public DbSet<M_PurchaseHeader> PurchaseHeaders { get; set; }
        public DbSet<M_Package> Packages { get; set; }
        public DbSet<M_Agency> Agencies { get; set; }
        public DbSet<M_CardHeader> CardHeaders { get; set; }
        public DbSet<M_CardLine> CardLines { get; set; }
        public DbSet<M_CardEntry> CardEntries { get; set; }
        public DbSet<M_SalesHeader> SalesHeaders { get; set; }
        public DbSet<M_SalesLine> SalesLines { get; set; }
        public DbSet<M_AgencyAndCard> AgencyAndCards { get; set; }
        public DbSet<M_SMS> SMSes { get; set; }
        public DbSet<M_District> Districts { get; set; }
        public DbSet<M_Province> Provinces { get; set; }
        public DbSet<M_MailSetup> MailSetups { get; set; }
        public DbSet<M_Template> Templates { get; set; }
        public DbSet<M_ApprovalInfo> ApprovalInfos { get; set; }
        public DbSet<M_Flow> Flows { get; set; }
        public DbSet<M_Customer> Customers { get; set; }
        public DbSet<M_ResetPassword> ResetPasswords { get; set; }
        public DbSet<M_LoginEntry> LoginEntries { get; set; }
        public DbSet<R_UploadVideo> UploadVideos { get; set; }
        public DbSet<M_PaymentResult> PaymentResults { get; set; }
        public DbSet<M_EbookAccount> EbookAccounts { get; set; }
        public DbSet<M_Ticket> Tickets { get; set; }
        public DbSet<M_PotentialCard> PotentialCards { get; set; }
        public DbSet<M_PotentialCustomer> PotentialCustomers { get; set; }
        public DbSet<M_Language> Languages { get; set; }
        public DbSet<M_MemoVideos> MemoVideoses { get; set; }
        public DbSet<M_Banner> Banners { get; set; }
        public DbSet<M_Tool> Tools { get; set; }
        public DbSet<M_Item> Items { get; set; }
        public DbSet<M_RetailSalesOrder> RetailSalesOrders { get; set; }
        public DbSet<TB_Contestants> Contestantses { get; set; }
        public DbSet<TB_DeThi> DeThis { get; set; }
        public DbSet<TB_NopBai> NopBais { get; set; }
        public DbSet<TB_TrongTai> TrongTais { get; set; }
        public DbSet<M_Feedback> Feedbacks { get; set; }
        public DbSet<M_Mindmap> Mindmaps { get; set; }
        public DbSet<M_ResponeOnepay> ResponeOnepays { get; set; }
        public DbSet<M_Like> Likes { get; set; }
        public DbSet<M_SalesPrice> SalesPrices { get; set; }
        public DbSet<TB_MindMapPoint> MindMapPoints { get; set; }
        public DbSet<M_HistoryChangeClass> HistoryChangeClass { get; set; }
        public DbSet<M_Promotion> Promotion { get; set; }
        public DbSet<M_HistoryRecall> HistoryRecall { get; set; }
        public DbSet<M_HistoryCall> HistoryCall { get; set; }
        public DbSet<M_HistoryInvoice> HistoryInvoice { get; set; }
        public DbSet<TB_Gift> Gifts { get; set; }
        public DbSet<TB_PointEntry> PointEntries { get; set; }
        public DbSet<TB_TransactionGift> TransactionGifts { get; set; }
        public DbSet<M_UserInfoOfGroup> UserInfoOfGroups { get; set; }
        public DbSet<M_AgencyAndCardHistory> AgencyAndCardHistorys { get; set; }
        public DbSet<QR_Question> QRQuestions { get; set; }
        public DbSet<QR_Question_Answer> QRQuestion_Answers { get; set; }
        public DbSet<QR_Question_Result> QRQuestion_Results { get; set; }
        public DbSet<QR_Question_User> QRQuestionUser { get; set; }
        public DbSet<M_ViHat> ViHats { get; set; }
        public DbSet<M_ViHatDetail> ViHatDetails { get; set; }
        public DbSet<E_SubEcourceContent> SubEcourceContents { get; set; }
        public DbSet<QR_HistoryStudy_User> QRHistoryStudyUsers { get; set; }
        public DbSet<QR_Honor_Week> QRHonorWeeks { get; set; }
        public DbSet<M_Firebase> Firebase { get; set; }
       public DbSet<ASO_User> ASOUsers { get; set; }
        public DbSet<M_UploadImage> UploadImages { get; set; }
        public DbSet<M_IntroduceStudent> IntroduceStudents { get; set; }
        public DbSet<info_exams_review> info_exams_reviews { get; set; }
        public DbSet<info_exams_review_level> info_exams_review_levels { get; set; }
        public DbSet<info_exams_review_question> info_exams_review_questions { get; set; }
        public DbSet<info_exams_review_question_result> info_exams_review_question_results { get; set; }
        public DbSet<info_exams_review_question_user> info_exams_review_question_users { get; set; }
        public DbSet<info_review_question_answer> info_review_question_answers { get; set; }
        public DbSet<info_review_question_result> info_review_question_results { get; set; }
        public DbSet<info_review_question_user> info_review_question_users { get; set; }
        public DbSet<mst_groupcode> mst_groupcodes { get; set; }
        public DbSet<mst_knowledge_topic> mst_knowledge_topics { get; set; }
        public DbSet<mst_options> mst_optionss { get; set; }
        public DbSet<mst_review_question> mst_review_questions { get; set; }
        public DbSet<mst_topics> mst_topicss { get; set; }

        public DbSet<M_MessageMaster> MessageMasters { get; set; }
        public DbSet<M_MessageDetail> MessageDetails { get; set; }
        public DbSet<M_TicketComment> TicketComment { get; set; }
        public DbSet<M_ParcelOfLand> ParcelOfLand { get; set; }
        public DbSet<M_OldParcelOfLand> OldParcelOfLand { get; set; }
        public DbSet<M_NewParcelOfLand> NewParcelOfLand { get; set; }
        public DbSet<M_ParcelOfLandVilis> ParcelOfLandVilis { get; set; }
        public virtual void Commit()
        {
            base.SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<M_ParcelOfLandVilis>().Property(e => e.Area).HasPrecision(28, 8);
            modelBuilder.Entity<M_OldParcelOfLand>().Property(e => e.Area).HasPrecision(28, 8);
            modelBuilder.Entity<M_NewParcelOfLand>().Property(e => e.Area).HasPrecision(28, 8);
        }
    }
}