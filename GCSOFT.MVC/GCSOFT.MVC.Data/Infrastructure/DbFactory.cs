﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Data.Infrastructure
{
    public class DbFactory : Disposable, IDbFactory
    {
        GcEntities dbContext;

        public GcEntities Init()
        {
            return dbContext ?? (dbContext = new GcEntities());
        }

        protected override void DisposeCore()
        {
            if (dbContext != null)
                dbContext.Dispose();
        }
    }
}
