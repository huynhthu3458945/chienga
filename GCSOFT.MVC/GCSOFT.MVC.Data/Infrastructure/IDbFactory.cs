﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Data.Infrastructure
{
    public interface IDbFactory : IDisposable
    {
        GcEntities Init();
    }
}
