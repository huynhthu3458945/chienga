﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Data.Infrastructure
{
    public abstract class RepositoryBase<T> where T : class
    {
        #region Properties
        private GcEntities dataContext;
        private readonly IDbSet<T> dbSet;

        protected IDbFactory DbFactory
        {
            get;
            private set;
        }

        protected GcEntities DbContext
        {
            get { return dataContext ?? (dataContext = DbFactory.Init()); }
        }
        #endregion

        protected RepositoryBase(IDbFactory dbFactory)
        {
            DbFactory = dbFactory;
            dbSet = DbContext.Set<T>();
        }

        #region Implementation
        public virtual void Add(T entity)
        {
            if (entity == null)
                return;
            dbSet.Add(entity);
        }
        public virtual void Add(IEnumerable<T> objects)
        {
            if (objects == null)
                return;
            foreach (T obj in objects)
                dbSet.Add(obj);
        }
        public virtual void Update(T entity)
        {
            if (entity == null)
                return;
            dbSet.Attach(entity);
            dataContext.Entry(entity).State = EntityState.Modified;
        }
        public virtual void Update(IEnumerable<T> objects)
        {
            if (objects == null)
                return;
            foreach (T obj in objects)
            {
                dbSet.Attach(obj);
                dataContext.Entry(obj).State = EntityState.Modified;
            }
        }
        public virtual void Delete(T entity)
        {
            if (entity == null)
                return;
            dbSet.Remove(entity);
        }
        public virtual void Delete(Expression<Func<T, bool>> where)
        {
            IEnumerable<T> objects = dbSet.Where<T>(where).AsEnumerable();
            if (objects == null)
                return;
            foreach (T obj in objects)
                dbSet.Remove(obj);
        }
        public virtual T GetById(int id)
        {
            return dbSet.Find(id);
        }
        public virtual T GetById(long id)
        {
            return dbSet.Find(id);
        }
        public virtual T GetById(string id)
        {
            return dbSet.Find(id);
        }
        public virtual T GetById(Guid id)
        {
            return dbSet.Find(id);
        }
        public virtual IEnumerable<T> GetAll()
        {
            return dbSet.ToList();
        }
        public virtual IEnumerable<T> GetMany(Expression<Func<T, bool>> where)
        {
            return dbSet.AsNoTracking().Where(where).ToList();
        }
        public T Get(Expression<Func<T, bool>> where)
        {
            return dbSet.AsNoTracking().Where(where).FirstOrDefault<T>();
        }

        public virtual int GetKey(Expression<Func<T, int?>> key)
        {
            try
            {
                int? _id = 0;
                _id = dbSet.Max(key);
                if (_id == 0 || _id == null)
                    return 1;
                return (int)_id + 1;
            }
            catch { return 1; }
        }
        public long GetKey(Expression<Func<T, long?>> key)
        {
            try
            {
                long? _id = 0;
                _id = dbSet.Max(key);
                if (_id == 0 || _id == null)
                    return 1;
                return (long)_id + 1;
            }
            catch { return 1; }
        }
        public virtual int GetKey(Expression<Func<T, bool>> where, Expression<Func<T, int?>> key)
        {
            int? _id = 0;
            _id = dbSet.Where(where).Max(key);
            if (_id == 0 || _id == null)
                return 100;
            return (int)_id + 100;
        }
        #endregion

    }
}
