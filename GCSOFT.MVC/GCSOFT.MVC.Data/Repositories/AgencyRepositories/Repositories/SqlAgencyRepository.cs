﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.AgencyRepositories.Interface;
using GCSOFT.MVC.Model.AgencyModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Data.Repositories.AgencyRepositories.Repositories
{
    public class SqlAgencyRepository : RepositoryBase<M_Agency>, IAgencyRepository
    {
        public SqlAgencyRepository(IDbFactory dbFactory)
            : base(dbFactory) { }
    }
}
