﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Model.Approval;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Data.Repositories.ApprovalRepositories.Interface
{
    public interface IApprovalInfoRepository : IRepository<M_ApprovalInfo>
    {
    }
}
