﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Model.MasterData;

namespace GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface
{
    public interface ICourseCategoryRepository : IRepository<E_CourseCategory>
    {

    }
}
