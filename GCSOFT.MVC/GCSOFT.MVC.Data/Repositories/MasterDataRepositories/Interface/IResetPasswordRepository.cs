﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface
{
    public interface IResetPasswordRepository : IRepository<M_ResetPassword>
    {

    }
}
