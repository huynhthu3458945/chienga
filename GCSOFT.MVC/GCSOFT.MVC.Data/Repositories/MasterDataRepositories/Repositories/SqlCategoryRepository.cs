﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;

namespace GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Repositories
{
    public class SqlCategoryRepository : RepositoryBase<E_Category>, ICategoryRepository
    {
        public SqlCategoryRepository(IDbFactory databaseFactory)
            : base(databaseFactory)
        { }
    }
}
