﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;

namespace GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Repositories
{
    public class SqlCourseCategoryRepository : RepositoryBase<E_CourseCategory>, ICourseCategoryRepository
    {
        public SqlCourseCategoryRepository(IDbFactory dbFactory)
            : base(dbFactory) { }
    }
}