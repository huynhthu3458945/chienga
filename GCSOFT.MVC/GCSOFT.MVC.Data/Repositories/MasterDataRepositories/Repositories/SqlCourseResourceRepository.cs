﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;

namespace GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Repositories
{
    public class SqlCourseResourceRepository : RepositoryBase<E_CourseResource>, ICourseResourceRepository
    {
        public SqlCourseResourceRepository(IDbFactory dbFactory)
            : base(dbFactory) { }
    }
}
