﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;

namespace GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Repositories
{
    public class SqlOptionTypeRepository : RepositoryBase<M_OptionType>, IOptionTypeRepository
    {
        public SqlOptionTypeRepository(IDbFactory dbFactory)
            : base(dbFactory) { }
    }
}