﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Repositories
{
    public class SqlResultEntryRepository : RepositoryBase<M_ResultEntry>, IResultEntryRepository
    {
        public SqlResultEntryRepository(IDbFactory databaseFactory)
            : base(databaseFactory)
        { }
    }
}
