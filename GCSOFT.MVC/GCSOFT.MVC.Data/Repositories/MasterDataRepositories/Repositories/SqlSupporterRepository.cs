﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Repositories
{
    public class SqlSupporterRepository : RepositoryBase<M_Supporter>, ISupporterRepository
    {
        public SqlSupporterRepository(IDbFactory databaseFactory)
            : base(databaseFactory)
        { }
    }
}
