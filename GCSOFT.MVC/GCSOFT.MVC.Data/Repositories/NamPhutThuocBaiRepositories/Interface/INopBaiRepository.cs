﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.NamPhutThuocBai;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Model.Videos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface
{
    public interface INopBaiRepository : IRepository<TB_NopBai>
    {

    }
}
