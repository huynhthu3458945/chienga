﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Model.NamPhutThuocBai;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Data.Repositories.NamPhutThuocBaiRepositories.Interface
{
    public interface IPointEntryRepository : IRepository<TB_PointEntry>
    {

    }
}
