﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Model.QuickQuiz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Data.Repositories.QuickQuizRepositories.Interface
{
    public interface IQRQuestionRepository : IRepository<QR_Question>
    {

    }
    public interface IQRQuestionAnswerRepository : IRepository<QR_Question_Answer>
    {

    }
}