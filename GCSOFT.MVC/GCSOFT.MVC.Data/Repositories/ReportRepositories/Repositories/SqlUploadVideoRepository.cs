﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.AgencyRepositories.Interface;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Model.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Data.Repositories.AgencyRepositories.Repositories
{
    public class SqlUploadVideoRepository : RepositoryBase<R_UploadVideo>, IUploadVideoRepository
    {
        public SqlUploadVideoRepository(IDbFactory dbFactory)
            : base(dbFactory) { }
    }
}
