﻿using GCSOFT.MVC.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model;
using GCSOFT.MVC.Model.StoreProcedue;

namespace GCSOFT.MVC.Data.Repositories.StoreProcedure
{
    public class SqlStoreProcedureRepository : RepositoryBase<string>, IStoreProcedureRepository
    {
        public SqlStoreProcedureRepository(IDbFactory databaseFactory)
            : base(databaseFactory)
        { }

        public IEnumerable<ProcResult> InsertSO(string DocumentNo, string LotNumber, string UserName, string FullName, int FromSource, string SourceNo)
        {
            return DbContext.CallStoredProcedure<object, ProcResult>("SP_CreateSO", new { DocumentNo = DocumentNo, LotNumber = LotNumber, UserName = UserName, FullName = FullName, FromSource = FromSource, SourceNo = SourceNo });
        }
        public IEnumerable<ProcResult> SP_AgencyAcceptOrder(int SalesType, string DocumentNo, string LotNumber, string UserName, string FullName, int FromSource, string SourceNo)
        {
            return DbContext.CallStoredProcedure<object, ProcResult>("SP_AgencyAcceptOrder", new { SalesType = SalesType, DocumentNo = DocumentNo, LotNumber = LotNumber, UserName = UserName, FullName = FullName, FromSource = FromSource, SourceNo = SourceNo });
        }
        public IEnumerable<ProcResult> SP_SubAgencyAcceptOrder(int SalesType, string DocumentNo, string UserName, string FullName, int FromSource, string SourceNo, string StatusCardCode)
        {
            return DbContext.CallStoredProcedure<object, ProcResult>("SP_SubAgencyAcceptOrder", new { SalesType = SalesType, DocumentNo = DocumentNo, UserName = UserName, FullName = FullName, FromSource = FromSource, SourceNo = SourceNo, StatusCardCode = StatusCardCode });
        }
        public IEnumerable<ProcResult> SP_ReturnOrder(int SalesType, string DocumentNo, string UserName, string FullName)
        {
            return DbContext.CallStoredProcedure<object, ProcResult>("SP_ReturnOrder", new { SalesType = SalesType, DocumentNo = DocumentNo, UserName = UserName, FullName = FullName });
        }
        public IEnumerable<SpCustomerInfo> SP_CustomerInfo(string CardNo, string PhoneNo, string Email, string FullName, string SerialNumber, string Username, string DurationCode)
        {
            return DbContext.CallStoredProcedure<object, SpCustomerInfo>("SP_CustomerInfo", new { CardNo = CardNo, PhoneNo = PhoneNo, Email = Email, FullName = FullName, SerialNumber = SerialNumber, Username = Username, DurationCode = DurationCode });
        }
        public IEnumerable<SpUserInfo> SP_UserInfo(string Username, string FullName, string Email, string Phone, string Serinumber, string CardNo)
        {
            return DbContext.CallStoredProcedure<object, SpUserInfo>("SP_UserInfo", new { Username = Username, FullName = FullName, Email = Email, Phone = Phone, Serinumber = Serinumber, CardNo = CardNo });
        }
        public IEnumerable<SpGeneralInfo> SP_GeneralInfo(DateTime FromDate, DateTime ToDate, DateTime LastFromDate, DateTime LastToDate)
        {
            return DbContext.CallStoredProcedure<object, SpGeneralInfo>("sp_rpt_general", new { FromDate = FromDate, ToDate = ToDate, LastFromDate = LastFromDate, LastToDate = LastToDate });
        }
        public IEnumerable<SpTTLBuyCustomerDtl> sp_rpt_TTLBuyCustomerDtl(DateTime FromDate, DateTime ToDate, DateTime LastFromDate, DateTime LastToDate)
        {
            return DbContext.CallStoredProcedure<object, SpTTLBuyCustomerDtl>("sp_rpt_TTLBuyCustomerDtl", new { FromDate = FromDate, ToDate = ToDate, LastFromDate = LastFromDate, LastToDate = LastToDate });
        }
        public IEnumerable<SpAgencyLevelDtl> sp_rpt_AgencyLevelDtl(DateTime FromDate, DateTime ToDate, DateTime LastFromDate, DateTime LastToDate)
        {
            return DbContext.CallStoredProcedure<object, SpAgencyLevelDtl>("sp_rpt_AgencyLevelDtl", new { FromDate = FromDate, ToDate = ToDate, LastFromDate = LastFromDate, LastToDate = LastToDate });
        }
        public IEnumerable<SpAgencyinfo> sp_rpt_agencyinfo(DateTime FromDate, DateTime ToDate)
        {
            return DbContext.CallStoredProcedure<object, SpAgencyinfo>("sp_rpt_agencyinfo", new { FromDate = FromDate, ToDate = ToDate });
        }
        public IEnumerable<ProcResult> sp_ChangeCard(string ConvertCardNo, int AgencyId, string FromStatusCode, string ToStatusCode, int NoOfConvert)
        {
            return DbContext.CallStoredProcedure<object, ProcResult>("sp_ChangeCard", new { ConvertCardNo = ConvertCardNo, AgencyId = AgencyId, FromStatusCode = FromStatusCode, ToStatusCode = ToStatusCode, NoOfConvert = NoOfConvert });
        }
        public IEnumerable<SPApprovalList> SP_LoadApprovalList(int ApprovalType, string StatusCode, string ApprovalNo)
        {
            return DbContext.CallStoredProcedure<object, SPApprovalList>("SP_LoadApprovalList", new { ApprovalType = ApprovalType, StatusCode = StatusCode, ApprovalNo = ApprovalNo });
        }
        public IEnumerable<SpCardSoldByAgency> SP_CardSoldByAgency(int AgencyId, string FullName, string PhoneNo, string Email, string CardNo, string SeriNumber, int Start, int Offset, DateTime FromDate, DateTime ToDate, string Action)
        {
            return DbContext.CallStoredProcedure<object, SpCardSoldByAgency>("SP_CardSoldByAgency", new { AgencyId = AgencyId, FullName = FullName, PhoneNo = PhoneNo, Email = Email, CardNo = CardNo, SeriNumber = SeriNumber, Start = Start, Offset = Offset, FromDate = FromDate, ToDate = ToDate, Action = Action });
        }
        public IEnumerable<ProcResultInt> SP_CountCardSoldByAgency(int AgencyId, string FullName, string PhoneNo, string Email, string CardNo, string SeriNumber)
        {
            return DbContext.CallStoredProcedure<object, ProcResultInt>("SP_CountCardSoldByAgency", new { AgencyId = AgencyId, FullName = FullName, PhoneNo = PhoneNo, Email = Email, CardNo = CardNo, SeriNumber = SeriNumber });
        }
        public IEnumerable<SpAgencyDetail> sp_AgencyDetail(DateTime FromDate, DateTime ToDate, int AgencyId)
        {
            return DbContext.CallStoredProcedure<object, SpAgencyDetail>("sp_AgencyDetail", new { FromDate = FromDate, ToDate = ToDate, AgencyId = AgencyId });
        }
        public IEnumerable<SpAgencyBuy2Child> sp_AgencyBuy2Child(DateTime FromDate, DateTime ToDate, int AgencyId)
        {
            return DbContext.CallStoredProcedure<object, SpAgencyBuy2Child>("sp_AgencyBuy2Child", new { FromDate = FromDate, ToDate = ToDate, AgencyId = AgencyId });
        }
        public IEnumerable<EbookUserByCity> sp_rpt_EbookUserByCity(int cityId, int districtId)
        {
            return DbContext.CallStoredProcedure<object, EbookUserByCity>("sp_rpt_EbookUserByCity", new { CityId = cityId, DistrictId = districtId });
        }
        public IEnumerable<EbookUserDetailByCity> sp_rpt_EbookUserDetailByCity(int cityId, int districtId)
        {
            return DbContext.CallStoredProcedure<object, EbookUserDetailByCity>("sp_rpt_EbookUserDetailByCity", new { CityId = cityId, DistrictId = districtId });
        }

        public IEnumerable<SpCardSoldByAgency> SP_CardSoldByAgencyAcitve(int AgencyId, string FullName, string PhoneNo, string Email, string CardNo, string SeriNumber, int Start, int Offset, DateTime FromDate, DateTime ToDate, string Action)
        {
            return DbContext.CallStoredProcedure<object, SpCardSoldByAgency>("SP_CardSoldByAgencyAcitve", new { AgencyId = AgencyId, FullName = FullName, PhoneNo = PhoneNo, Email = Email, CardNo = CardNo, SeriNumber = SeriNumber, Start = Start, Offset = Offset, FromDate = FromDate, ToDate = ToDate, Action = Action });
        }

        public IEnumerable<ProcResultInt> SP_CountCardSoldByAgencyActive(int AgencyId, string FullName, string PhoneNo, string Email, string CardNo, string SeriNumber)
        {
            return DbContext.CallStoredProcedure<object, ProcResultInt>("SP_CountCardSoldByAgencyActive", new { AgencyId = AgencyId, FullName = FullName, PhoneNo = PhoneNo, Email = Email, CardNo = CardNo, SeriNumber = SeriNumber });
        }
        public IEnumerable<QuanLyTrongTai> sp_QuanLyTrongTai(string userName)
        {
            return DbContext.CallStoredProcedure<object, QuanLyTrongTai>("sp_QuanLyTrongTai", new { userName = userName });
        }
        public IEnumerable<SPBaiThi> SP_BaiThi(string userName)
        {
            return DbContext.CallStoredProcedure<object, SPBaiThi>("SP_BaiThi", new { userName = userName });
        }
        public IEnumerable<SpCustomerAgencyBuy> SP_CustomerAgencyBuy(int AgencyId, string SeriNumber, string FullName, string PhoneNo, string Email, string LevelCode, DateTime FromDate, DateTime ToDate)
        {
            return DbContext.CallStoredProcedure<object, SpCustomerAgencyBuy>("sp_CustomerAgencyBuy", new { AgencyId = AgencyId, SeriNumber = SeriNumber, FullName = FullName, PhoneNo = PhoneNo, Email = Email, LevelCode = LevelCode, FromDate = FromDate, ToDate = ToDate });
        }
        public IEnumerable<AgencyParentList> AgencyParentFindAll(string Action, int AgencyId)
        {
            return DbContext.CallStoredProcedure<object, AgencyParentList>("SP_STNHD_AgencyParentList", new { Action = Action, AgencyId = AgencyId });
        }
        public IEnumerable<AgencyParentInfo> AgencyParentGetInfo(string Action, int AgencyId)
        {
            return DbContext.CallStoredProcedure<object, AgencyParentInfo>("SP_STNHD_AgencyParentList", new { Action = Action, AgencyId = AgencyId });
        }
        public IEnumerable<AgencyBuyGeneralCount> SP_RPT_AgencyBuyGeneralCount(int AgencyId, DateTime FromDate, DateTime ToDate, int searchAgencyId, string searchPhone, string searchEmail, string GroupBy, string SortBy)
        {
            return DbContext.CallStoredProcedure<object, AgencyBuyGeneralCount>("SP_RPT_AgencyBuyGeneralCount", new { AgencyId = AgencyId, FromDate = FromDate, ToDate = ToDate, searchAgencyId = searchAgencyId, searchPhone = searchPhone, searchEmail  = searchEmail , GroupBy = GroupBy, SortBy = SortBy });
        }
        public IEnumerable<AgencyBuyGeneral> SP_RPT_AgencyBuyGeneral(int AgencyId, DateTime FromDate, DateTime ToDate, int searchAgencyId, string searchPhone, string searchEmail, string GroupBy, string SortBy, int Start, int Offset)
        {
            return DbContext.CallStoredProcedure<object, AgencyBuyGeneral>("SP_RPT_AgencyBuyGeneral", new { AgencyId = AgencyId, FromDate = FromDate, ToDate = ToDate, searchAgencyId = searchAgencyId, searchPhone = searchPhone, searchEmail = searchEmail, GroupBy = GroupBy, SortBy = SortBy, Start = Start, Offset = Offset });
        }

        public IEnumerable<ReNewCardNo> SP_CardNoDuplicate()
        {
            return DbContext.CallStoredProcedure<object, ReNewCardNo>("SP_CardNoDuplicate", new { CardNo = string.Empty});
        }

        public IEnumerable<SpAgencyDetailSales> SP_RPT_AgencyDetailSales(string Action, int? AgencyId, string ViewBy, DateTime FromDate, DateTime ToDate, int searchAgencyId, string cardNo, string levelcode, string seri, string fullName, string phone, string email, string carStatus, int Start, int Offset)
        {
            return DbContext.CallStoredProcedure<object, SpAgencyDetailSales>("SP_RPT_AgencyDetailSales", new { Action = Action, AgencyId = AgencyId, ViewBy = ViewBy, FromDate = FromDate, ToDate = ToDate, searchAgencyId = searchAgencyId, CardNo = cardNo, LLevelCode = levelcode, SeriNumber = seri, FullName = fullName, searchPhone = phone, searchEmail = email, CardStatus = carStatus, Start = Start, Offset = Offset });  ;
        }
        public IEnumerable<ProcResultInt> SP_RPT_AgencyDetailSalesCount(string Action, int? AgencyId, string ViewBy, DateTime FromDate, DateTime ToDate, int searchAgencyId, string cardNo, string levelcode, string seri, string fullName, string phone, string email, string carStatus, int Start, int Offset)
        {
            return DbContext.CallStoredProcedure<object, ProcResultInt>("SP_RPT_AgencyDetailSales", new { Action = Action, AgencyId = AgencyId, ViewBy = ViewBy, FromDate = FromDate, ToDate = ToDate, searchAgencyId = searchAgencyId, CardNo = cardNo, LLevelCode = levelcode, SeriNumber = seri, FullName = fullName, searchPhone = phone, searchEmail = email, CardStatus = carStatus, Start = Start, Offset = Offset });
        }

        public IEnumerable<SpHistoryRecall> SP_HistoryRecall(int AgencyId, string CardNo, string Seri, bool IsDate, DateTime DateRecall, string Reason, int Start, int Offset)
        {
            return DbContext.CallStoredProcedure<object, SpHistoryRecall>("SP_HistoryRecall", new { AgencyId = AgencyId, CardNo = CardNo, SeriNumber = Seri, IsDate = IsDate, DateRecall = DateRecall, Reason = Reason, Start = Start, Offset = Offset }); 
        }
        public IEnumerable<SpCustomerAgencyBuySumary> sp_CustomerAgencyBuySumary(int AgencyId, string SeriNumber, string FullName, string PhoneNo, string Email, string LevelCode, DateTime FromDate, DateTime ToDate)
        {
            return DbContext.CallStoredProcedure<object, SpCustomerAgencyBuySumary>("sp_CustomerAgencyBuySumary", new { AgencyId = AgencyId, SeriNumber = SeriNumber, FullName = FullName, PhoneNo = PhoneNo, Email = Email, LevelCode = LevelCode, FromDate = FromDate, ToDate = ToDate });
        }
        public IEnumerable<AgencyBuyGeneral> SP_RPT_AgencyBuyGeneralDiffDate(int AgencyId, DateTime FromDateBuy, DateTime ToDateBuy, DateTime FromDateActive, DateTime ToDateActive, int searchAgencyId, string searchPhone, string searchEmail, string GroupBy, string SortBy, int Start, int Offset)
        {
            return DbContext.CallStoredProcedure<object, AgencyBuyGeneral>("SP_RPT_AgencyBuyGeneralDiffDate", new { AgencyId = AgencyId, FromDateBuy = FromDateBuy, ToDateBuy = ToDateBuy, FromDateActive = FromDateActive, ToDateActive = ToDateActive, searchAgencyId = searchAgencyId, searchPhone = searchPhone, searchEmail = searchEmail, GroupBy = GroupBy, SortBy = SortBy, Start = Start, Offset = Offset });
        }
        public IEnumerable<SP_SYS_Categroy_Tree> SP_SYS_Categroy_Tree(string Code)
        {
            return DbContext.CallStoredProcedure<object, SP_SYS_Categroy_Tree>("SP_SYS_Categroy_Tree", new { Code = Code });
        }
        public IEnumerable<SpDataPostBD> SP_DataPostBD(int AgencyId, DateTime FromDate, DateTime ToDate, int searchAgencyId, string searchPhone, string searchEmail)
        {
            return DbContext.CallStoredProcedure<object, SpDataPostBD>("SP_DataPostBD", new { AgencyId = AgencyId, FromDateBuy = FromDate, ToDateBuy = ToDate, searchAgencyId = searchAgencyId, searchPhone = searchPhone, searchEmail = searchEmail });
        }
        public void SP_RecallLotnumber(string action, string username, string lotNumber)
        {
             DbContext.CallStoredProcedure<object, SpDataPostBD>("SP_RecallLotnumber", new { Action = action, UserName = username, LotNumber = lotNumber});
        }
        public IEnumerable<SP_ListTop> SP_ListTop(string Action, string CodeDethi, int? Top)
        {
            return DbContext.CallStoredProcedure<object, SP_ListTop>("SP_ListTop", new { Action = Action, CodeDethi = CodeDethi, Top = Top });
        }

        public IEnumerable<SpRetailCustomer> SP_RPT_RetailCustomer(string Action, string phone, string email, int Start, int Offset)
        {
            return DbContext.CallStoredProcedure<object, SpRetailCustomer>("SP_RPT_RetailCustomer", new { Action = Action, searchPhone = phone, searchEmail = email, Start = Start, Offset = Offset });
        }
        public IEnumerable<AgencyChildBuyGeneral> SP_RPT_AgencyChildBuyGeneral(int AgencyId, DateTime FromDateBuy, DateTime ToDateBuy, DateTime FromDateActive, DateTime ToDateActive, int searchAgencyId, int Start, int Offset)
        {
            return DbContext.CallStoredProcedure<object, AgencyChildBuyGeneral>("SP_RPT_AgencyChildBuyGeneral", new { AgencyId = AgencyId, FromDateBuy = FromDateBuy, ToDateBuy = ToDateBuy, FromDateActive = FromDateActive, ToDateActive = ToDateActive, searchAgencyId = searchAgencyId, Start = Start, Offset = Offset });
        }
    }
}
