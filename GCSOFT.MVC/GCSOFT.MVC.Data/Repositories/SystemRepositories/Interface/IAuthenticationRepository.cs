﻿using GCSOFT.MVC.Model.SystemModels;
using System.Collections.Generic;

namespace GCSOFT.MVC.Data.Repositories.SystemRepositories.Interface
{
    public interface IAuthenticationRepository
    {
        IEnumerable<Permission> FindAll();
        IEnumerable<PermissionOnSystemFunc> GetAllPermissionOnSystemFunc(string categoryCode, string groupCode);
    }
}
