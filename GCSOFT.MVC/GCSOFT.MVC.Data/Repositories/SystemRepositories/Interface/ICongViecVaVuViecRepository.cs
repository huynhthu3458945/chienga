﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Model.SystemModels;

namespace GCSOFT.MVC.Data.Repositories.SystemRepositories.Interface
{
    public interface ICongViecVaVuViecRepository : IRepository<HT_CongViecVaVuViec>
    {

    }
}
