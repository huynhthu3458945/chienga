﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Model;
using GCSOFT.MVC.Model.SystemModels;
using System.Collections.Generic;

namespace GCSOFT.MVC.Data.Repositories.SystemRepositories.Interface
{
    public interface IVuViecRepository : IRepository<HT_VuViec>
    {
        IEnumerable<HT_VuViec> GetRoleBy(string categoryCode, string userName);
    }
}