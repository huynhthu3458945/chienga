﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.SystemRepositories.Interface;
using GCSOFT.MVC.Model.SystemModels;

namespace GCSOFT.MVC.Data.Repositories.SystemRepositories.Repositories
{
    public class SqlUserGroupRepository : RepositoryBase<HT_NhomUser>, IUserGroupRepository
    {
        public SqlUserGroupRepository(IDbFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }
}