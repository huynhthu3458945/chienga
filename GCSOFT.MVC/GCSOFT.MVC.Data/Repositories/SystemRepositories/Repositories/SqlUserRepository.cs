﻿using System.Linq;
using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.SystemRepositories.Interface;
using GCSOFT.MVC.Model.SystemModels;

namespace GCSOFT.MVC.Data.Repositories.SystemRepositories.Repositories
{
    public class SqlUserRepository : RepositoryBase<HT_Users>, IUserRepository
    {
        public SqlUserRepository(IDbFactory dbFactory)
            : base(dbFactory) { }

        public int GetID()
        {
            try{ return DbContext.Users.Max(d => d.ID); }
            catch { return 0; }
        }
    }
}
