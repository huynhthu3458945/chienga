﻿using System.Linq;
using System.Collections.Generic;
using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.SystemRepositories.Interface;
using GCSOFT.MVC.Model;
using GCSOFT.MVC.Model.SystemModels;

namespace GCSOFT.MVC.Data.Repositories.SystemRepositories.Repositories
{
    public class SqlVuViecRepository : RepositoryBase<HT_VuViec>, IVuViecRepository
    {
        public SqlVuViecRepository(IDbFactory dbFactory)
            : base(dbFactory) { }

        public IEnumerable<HT_VuViec> GetRoleBy(string categoryCode, string userName)
        {
            var vuViecs = from v in DbContext.VuViecs
                          join cv_vv in DbContext.CongViecVaVuViecs on v.Code equals cv_vv.FunctionCode
                          join nu in DbContext.NhomUsers on cv_vv.UserGroupCode equals nu.Code
                          join utn in DbContext.UserThuocNhom on nu.Code equals utn.UserGroupCode
                          join u in DbContext.Users on utn.UserID equals u.ID
                          where cv_vv.CategoryCode.Equals(categoryCode) && u.Username.Equals(userName)
                          select v;
            return vuViecs;
        }
    }
}