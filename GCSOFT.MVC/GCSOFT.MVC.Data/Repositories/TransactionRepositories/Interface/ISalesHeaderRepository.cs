﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Model.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Data.Repositories.TransactionRepositories.Interface
{
    public interface ISalesHeaderRepository : IRepository<M_SalesHeader>
    {

    }
}
