﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.Transaction;

namespace GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Repositories
{
    public class SqlFeedbackRepository : RepositoryBase<M_Feedback>, IFeedbackRepository
    {
        public SqlFeedbackRepository(IDbFactory databaseFactory)
            : base(databaseFactory)
        { }
    }
}