﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.Transaction;

namespace GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Repositories
{
    public class SqlPotentialCardRepository : RepositoryBase<M_PotentialCard>, IPotentialCardRepository
    {
        public SqlPotentialCardRepository(IDbFactory databaseFactory)
            : base(databaseFactory)
        { }
    }
}
