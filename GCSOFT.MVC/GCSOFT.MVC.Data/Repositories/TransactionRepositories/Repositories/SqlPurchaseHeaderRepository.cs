﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.Transaction;

namespace GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Repositories
{
    public class SqlPurchaseHeaderRepository : RepositoryBase<M_PurchaseHeader>, IPurchaseHeaderRepository
    {
        public SqlPurchaseHeaderRepository(IDbFactory databaseFactory)
            : base(databaseFactory)
        { }
    }
}
