﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.TransactionRepositories.Interface;
using GCSOFT.MVC.Model.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Data.Repositories.TransactionRepositories.Repositories
{
    public class SqlSMSRepository : RepositoryBase<M_SMS>, ISMSRepository
    {
        public SqlSMSRepository(IDbFactory databaseFactory)
            : base(databaseFactory)
        { }
    }
}
