﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.TransactionRepositories.Interface;
using GCSOFT.MVC.Model.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Data.Repositories.TransactionRepositories.Repositories
{
    public class SqlSalesLineRepository : RepositoryBase<M_SalesLine>, ISalesLineRepository
    {
        public SqlSalesLineRepository(IDbFactory databaseFactory)
            : base(databaseFactory)
        { }
    }
}
