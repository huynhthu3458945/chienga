﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.AgencyModel
{
    public class M_Agency
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        [MaxLength(80)]
        public string Code { get; set; }
        [MaxLength(80)]
        public string FirstName { get; set; }
        [MaxLength(80)]
        public string LastName { get; set; }
        [MaxLength(160)]
        public string FullName { get; set; }
        [MaxLength(40)]
        public string Phone { get; set; }
        [MaxLength(80)]
        public string Email { get; set; }
        [MaxLength(250)]
        public string Address { get; set; }
        [Column(TypeName = "ntext")]
        public string Remark { get; set; }
        public DateTime? RegistrationDate { get; set; }
        public bool Block { get; set; }
        public DateTime? DateBlock { get; set; }
        public string ReasonBlock { get; set; }
        public bool HasAccount { get; set; }
        public bool IsFirstLogin { get; set; }
        public string UserName { get; set; }
        [MaxLength(500)]
        public string Image { get; set; }
        public int PackageId { get; set; }
        public string PackageName { get; set; }
        public string LeavelName { get; set; }
        public AgencyType AgencyType { get; set; }
        public int? ParentAgencyId { get; set; }
        public int? CityId { get; set; }
        [MaxLength(250)]
        public string CityName { get; set; }
        public int? DistrictId { get; set; }
        [MaxLength(250)]
        public string DistrictName { get; set; }
        public int? WardsId { get; set; }
        [MaxLength(250)]
        public string WardsName { get; set; }
        [MaxLength(20)]
        public string Legal { get; set; }
        [MaxLength(20)]
        public string CMND { get; set; }
        [MaxLength(20)]
        public string MST { get; set; }
        [MaxLength(250)]
        public string CompanyName { get; set; }
        public bool ShowWebsite { get; set; }
        [MaxLength(250)]
        public string ContractNo { get; set; }
        [MaxLength(40)]
        public string AgencyNo { get; set; }
        [MaxLength(500)]
        public string ImgCert { get; set; }
        [MaxLength(40)]
        public string BankAccNo { get; set; }
        [MaxLength(150)]
        public string BankAccName { get; set; }
        [MaxLength(150)]
        public string beneficiary { get; set; }
        public int RatingId { get; set; }
        [MaxLength(30)]
        public string RatingCode { get; set; }
        [MaxLength(80)]
        public string RatingName { get; set; }
        public DateTime? DateCreate { get; set; }
        public bool IsOutlierAdmin { get; set; }
        public int NoOfOutlier { get; set; }
        [MaxLength(30)]
        public string Symbol { get; set; }
    }
    public enum AgencyType : byte
    {
        [Description("Đại Tác")]
        Agency,
        [Description("Nhân Viên")]
        Staff,
        [Description("Cộng Tác Viên")]
        Partner,
        [Description("Khách Lẻ")]
        RetailCustomers,
        [Description("Chi Nhánh")]
        Branch,
        [Description("Kế Toán")]
        Accounting
    }
}
