﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.AgencyModel
{
    public class M_Package
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public string LeavelName { get; set; }
        [MaxLength(250)]
        public string Name { get; set; }
        public decimal ValueAmt { get; set; }
        public decimal PaymentDiscount { get; set; }
        public int NoOfCardsReceived { get; set; }
        public decimal RevenueAmt { get; set; }
        [MaxLength(250)]
        public string RevenueStr { get; set; }
        public decimal ProfitAmt { get; set; }
        public int SortOrder { get; set; }
    }
}
