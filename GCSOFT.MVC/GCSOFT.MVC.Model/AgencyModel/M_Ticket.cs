﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.AgencyModel
{
    public class M_Ticket
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Code { get; set; }
        [MaxLength(2000)]
        public string Title { get; set; }
        public string Link { get; set; }
        [Column(TypeName = "ntext")]
        public string Question { get; set; }
        [Column(TypeName = "ntext")]
        public string Reason { get; set; }
        [Column(TypeName = "ntext")]
        public string Overcome { get; set; }
        [Column(TypeName = "ntext")]
        public string SolutionNet { get; set; }
        public DateTime DateCreate { get; set; }
        public DateTime DateModify { get; set; }
        public int SystemId { get; set; }
        [MaxLength(20)]
        public string SystemCode { get; set; }
        [MaxLength(120)]
        public string SystemName { get; set; }
        public int? SendById { get; set; }
        [MaxLength(40)]
        public string SendBy { get; set; }
        [MaxLength(120)]
        public string SendByName { get; set; }
        public string SendByEmail { get; set; }

        [MaxLength(40)]
        public string PIC { get; set; }
        [MaxLength(120)]
        public string PICName { get; set; }
        
        public int StatusId { get; set; }
        [MaxLength(20)]
        public string StatusCode { get; set; }
        [MaxLength(120)]
        public string StatusName { get; set; }
        [MaxLength(120)]
        public string TicketType { get; set; }
    }
}
