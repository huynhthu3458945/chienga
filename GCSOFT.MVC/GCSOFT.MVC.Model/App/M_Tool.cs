﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.App
{
    public class M_Tool
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [MaxLength(20)]
        public string Code { get; set; }
        [MaxLength(2000)]
        public string Image { get; set; }
        [MaxLength(2000)]
        public string ImageIpad { get; set; }
        [MaxLength(2000)]
        public string ImageDesktop { get; set; }
        [MaxLength(500)]
        public string Url { get; set; }
        [MaxLength(20)]
        public string fontColor { get; set; }
        [MaxLength(200)]
        public string Title { get; set; }
        [MaxLength(20)]
        public string Rule { get; set; }
        public int SortOrder { get; set; }

    }
}
