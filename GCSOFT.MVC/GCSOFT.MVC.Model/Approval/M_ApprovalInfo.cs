﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.Approval
{
    public class M_ApprovalInfo
    {
        [Key, Column(Order = 0)]
        public ApprovalType Type { get; set; }
        [Key, Column(Order = 1, TypeName = "varchar")]
        [MaxLength(20)]
        public string Code { get; set; }
        [MaxLength(20)]
        public string SendBy { get; set; }
        [MaxLength(150)]
        public string SendByName { get; set; }
        public DateTime SendDate { get; set; }
        public int StatusId { get; set; }
        [MaxLength(20)]
        public string StatusCode { get; set; }
        [MaxLength(80)]
        public string StatusName { get; set; }
        [MaxLength(20)]
        public string ApprovalNo { get; set; }
        [MaxLength(150)]
        public string ApprovalName { get; set; }
        [MaxLength(150)]
        public string ApprovalEmail { get; set; }
        [MaxLength(250)]
        public string Description { get; set; }
        public int NextPriority { get; set; }
        public string GroupCode { get; set; }
    }
    public class M_Flow
    {
        [Key, Column(Order = 0)]
        public ApprovalType Type { get; set; }
        [Key, Column(Order = 1, TypeName = "varchar")]
        [MaxLength(20)]
        public string Code { get; set; }
        [Key, Column(Order = 2)]
        public int LineNo { get; set; }
        [MaxLength(40)]
        public string GroupCode { get; set; }
        [MaxLength(150)]
        public string GroupName { get; set; }
        [MaxLength(40)]
        public string ApprovalNo { get; set; }
        [MaxLength(150)]
        public string ApprovalName { get; set; }
        [MaxLength(150)]
        public string ApprovalEmail { get; set; }
        public DateTime? DateCfm { get; set; }
        public int StatusId { get; set; }
        [MaxLength(20)]
        public string StatusCode { get; set; }
        [MaxLength(80)]
        public string StatusName { get; set; }
        public int Priority { get; set; }
        public string Remark { get; set; }
        public int IsActive { get; set; }
    }
    public enum ApprovalType : byte
    {
        [Description("Đơn Đặt Hàng")]
        PurchaseOrder,
        [Description("Đại Lý")]
        Agency,
        [Description("Chuyển Thẻ")]
        Convert
    }
}
