﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GCSOFT.MVC.Model.MasterData
{
    public class E_Category
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        [MaxLength(80)]
        public string Name { get; set; }
        [MaxLength(250)]
        public string LinkWeb { get; set; }
        public int? ParentNo { get; set; }
        public string Description { get; set; }
        public int SortOrder { get; set; }
        public bool Status { get; set; }
        public int LevelCategory { get; set; }
        public int Level { get; set; }
        [MaxLength(250)]
        public string LevelStr { get; set; }
        [MaxLength(500)]
        public string SearchName { get; set; }
        [MaxLength(500)]
        public string BreadCrumb { get; set; }
    }
    public class BreadCrumb
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string URL { get; set; }
        public bool IsMain { get; set; }
    }
}
