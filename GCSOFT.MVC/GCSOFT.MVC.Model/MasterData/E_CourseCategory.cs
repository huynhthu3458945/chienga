﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GCSOFT.MVC.Model.MasterData
{
    public class E_CourseCategory
    {
        [Key, Column(Order = 0)]
        public int IdCourse { get; set; }
        [Key, Column(Order = 1)]
        public int IdCategory { get; set; }
        [MaxLength(500)]
        public string CategroySearchName { get; set; }
        public string Remark { get; set; }
    }
}
