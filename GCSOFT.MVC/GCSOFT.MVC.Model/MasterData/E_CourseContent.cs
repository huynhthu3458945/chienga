﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GCSOFT.MVC.Model.MasterData
{
    public class E_CourseContent
    {
        [Key, Column(Order = 0)]
        public int IdCourse { get; set; }
        [Key, Column(Order = 1)]
        public int LineNo { get; set; }
        public CourseType CourseType { get; set; }
        [MaxLength(80)]
        public string Times { get; set; }
        [MaxLength(300)]
        public string Title { get; set; }
        public bool Bold { get; set; }
        public bool Italic { get; set; }
        public bool IsHidden { get; set; }
        [MaxLength(500)]
        public string LinkYoutube { get; set; }
        [Column(TypeName = "ntext")]
        public string FullDescription { get; set; }
        public int SortOrder { get; set; }
        public int? ParentSortOrder { get; set; }
        public string LinkVideoVN { get; set; }
        public string LinkVideoEN { get; set; }
        public bool IsHiddenHK2 { get; set; }
        public bool IsExperience { get; set; }
        [MaxLength(40)]
        public string LessonTypeCode { get; set; }
        [MaxLength(250)]
        public string LessonTypeName { get; set; }
    }
    public enum CourseType : byte
    {
        [Description("Sơ Đồ Tư Duy")]
        Mindmap,
        [Description("Videos")]
        Videos
    }
    public class E_SubEcourceContent
    {
        [Key, Column(Order = 0)]
        public int IdCourse { get; set; }
        [Key, Column(Order = 1)]
        public int LineNo { get; set; }
        [Key, Column(Order = 2)]
        public int SubLineNo { get; set; }
        [MaxLength(300)]
        public string Title { get; set; }
    }
}
