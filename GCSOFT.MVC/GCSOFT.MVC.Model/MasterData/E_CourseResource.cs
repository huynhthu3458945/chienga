﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GCSOFT.MVC.Model.MasterData
{
    public class E_CourseResource
    {
        [Key, Column(Order = 0)]
        public int IdCourse { get; set; }
        [Key, Column(Order = 1)]
        public int LineNo { get; set; }
        [MaxLength(300)]
        public string Title { get; set; }
        [MaxLength(300)]
        public string Image { get; set; }
    }
}
