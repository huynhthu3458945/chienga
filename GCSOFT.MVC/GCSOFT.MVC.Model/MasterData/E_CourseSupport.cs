﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GCSOFT.MVC.Model.MasterData
{
    public class E_CourseSupport
    {
        [Key, Column(Order = 0)]
        public int IdCourse { get; set; }
        [Key, Column(Order = 1)]
        public int IdTeachcher { get; set; }
        [MaxLength(160)]
        public string TeachcherName { get; set; }
        public string Remark { get; set; }
    }
}