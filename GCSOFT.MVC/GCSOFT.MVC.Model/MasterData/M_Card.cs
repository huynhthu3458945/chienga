﻿using GCSOFT.MVC.Model.Transaction;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.MasterData
{
    public class M_CardHeader
    {
        [MaxLength(80)]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string LotNumber { get; set; }
        public CardType CardType { get; set; }
        public DateTime DateOrder { get; set; }
        public int NoOfCard { get; set; }
        public decimal Price { get; set; }
        public int StatusId { get; set; }
        [MaxLength(20)]
        public string StatusCode { get; set; }
        [MaxLength(250)]
        public string StatusName { get; set; }
        [MaxLength(250)]
        public string UserName { get; set; }
        public string UserFullName { get; set; }
        public DateTime DateCreate { get; set; }
        public int DurationId { get; set; }
        [MaxLength(20)]
        public string DurationCode { get; set; }
        [MaxLength(250)]
        public string DurationName { get; set; }
        public int ReasonId { get; set; }
        [MaxLength(20)]
        public string ReasonCode { get; set; }
        [MaxLength(250)]
        public string ReasonName { get; set; }
        public int LevelId { get; set; }
        [MaxLength(20)]
        public string LevelCode { get; set; }
        [MaxLength(250)]
        public string LevelName { get; set; }
        [MaxLength(50)]
        public string UserOwner { get; set; }
        public int NumberSendMail { get; set; }

        [MaxLength(20)]
        public string PromotionCode { get; set; }
        public virtual IEnumerable<M_CardLine> CardLines { get; set; }
        public virtual IEnumerable<M_CardEntry> CardEntries { get; set; }
        public virtual IEnumerable<M_Customer> Customers { get; set; }
    }
    public class M_CardLine
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }
        [MaxLength(80)]
        public string LotNumber { get; set; }
        [Index("IX_SerialNumber", 1)]
        [Index("IX_SerialStatusDate", 1)]
        [Index("IX_SerialStatusBuy", 1)]
        [MaxLength(50)]
        public string SerialNumber { get; set; }
        [Index("IX_CardNo", 1)]
        [MaxLength(50)]
        public string CardNo { get; set; }
        [MaxLength(50)]
        public string PhoneNo { get; set; }
        [MaxLength(80)]
        public string FulName { get; set; }
        public decimal Price { get; set; }
        [MaxLength(250)]
        public string UserName { get; set; }
        [MaxLength(250)]
        public string UserFullName { get; set; }
        public DateTime DateCreate { get; set; }
        public UserType UserType { get; set; }
        public string UserNameActive { get; set; }
        public string UserActiveFullName { get; set; }
        public DateTime LastDateUpdate { get; set; }
        public int StatusId { get; set; }
        [MaxLength(20)]
        public string StatusCode { get; set; }
        [MaxLength(250)]
        public string StatusName { get; set; }
        public bool IsActive { get; set; }
        public int LastStatusId { get; set; }
        [Index("IX_SerialStatusDate", 2)]
        [Index("IX_SerialStatusBuy", 2)]
        [MaxLength(20)]
        public string LastStatusCode { get; set; }
        [MaxLength(250)]
        public string LastStatusName { get; set; }
        public VerifyType VerifyType { get; set; }
        [MaxLength(80)]
        public string VerifyInfo { get; set; }
        public int DurationId { get; set; }
        [MaxLength(20)]
        public string DurationCode { get; set; }
        [MaxLength(250)]
        public string DurationName { get; set; }
        public int ReasonId { get; set; }
        [MaxLength(20)]
        public string ReasonCode { get; set; }
        [MaxLength(250)]
        public string ReasonName { get; set; }
        public int LevelId { get; set; }
        [MaxLength(20)]
        public string LevelCode { get; set; }
        [MaxLength(250)]
        public string LevelName { get; set; }
        [MaxLength(100)]
        public string ApplyUser { get; set; }
        [Index("IX_SerialStatusDate", 3)]
        public DateTime? ApplyDate { get; set; }
        [Index("IX_SerialStatusBuy", 3)]
        public DateTime? DateBuy { get; set; }
    }
    public class M_AgencyAndCard
    {
        [Key, Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AgencyId { get; set; }
        [Key, Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }
        [MaxLength(80)]
        public string LotNumber { get; set; }
        [MaxLength(50)]
        public string SerialNumber { get; set; }
        [MaxLength(50)]
        public string CardNo { get; set; }
        [MaxLength(50)]
        public string PhoneNo { get; set; }
        [MaxLength(80)]
        public string FulName { get; set; }
        [MaxLength(10)]
        public string OTP { get; set; }
        public decimal Price { get; set; }
        [MaxLength(250)]
        public string UserName { get; set; }
        [MaxLength(250)]
        public string UserFullName { get; set; }
        public DateTime DateCreate { get; set; }
        public UserType UserType { get; set; }
        public string UserNameActive { get; set; }
        public string UserActiveFullName { get; set; }
        public DateTime LastDateUpdate { get; set; }
        public int StatusId { get; set; }
        [MaxLength(20)]
        public string StatusCode { get; set; }
        [MaxLength(250)]
        public string StatusName { get; set; }
        public VerifyType VerifyType { get; set; }
        [MaxLength(80)]
        public string VerifyInfo { get; set; }
        public int DurationId { get; set; }
        [MaxLength(20)]
        public string DurationCode { get; set; }
        [MaxLength(250)]
        public string DurationName { get; set; }
        public int ReasonId { get; set; }
        [MaxLength(20)]
        public string ReasonCode { get; set; }
        [MaxLength(250)]
        public string ReasonName { get; set; }
        public int LevelId { get; set; }
        [MaxLength(20)]
        public string LevelCode { get; set; }
        [MaxLength(250)]
        public string LevelName { get; set; }
    }
    public class M_CardEntry
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [MaxLength(80)]
        public string LotNumber { get; set; }
        public long CardLineId { get; set; }
        [MaxLength(500)]
        public string SerialNumber { get; set; }
        [MaxLength(500)]
        public string CardNo { get; set; }
        public decimal Price { get; set; }
        [MaxLength(250)]
        public string UserName { get; set; }
        [MaxLength(250)]
        public string UserFullName { get; set; }
        public DateTime DateCreate { get; set; }
        public UserType UserType { get; set; }
        public string UserNameActive { get; set; }
        public string UserActiveFullName { get; set; }
        public DateTime Date { get; set; }
        public int StatusId { get; set; }
        [MaxLength(20)]
        public string StatusCode { get; set; }
        [MaxLength(250)]
        public string StatusName { get; set; }
        public CardSource Source { get; set; }
        public string SourceNo { get; set; }
        public int? AgencyId { get; set; }
        [MaxLength(250)]
        public string AgencyName { get; set; }
        public int DurationId { get; set; }
        [MaxLength(20)]
        public string DurationCode { get; set; }
        [MaxLength(250)]
        public string DurationName { get; set; }
        public int ReasonId { get; set; }
        [MaxLength(20)]
        public string ReasonCode { get; set; }
        [MaxLength(250)]
        public string ReasonName { get; set; }
        public int LevelId { get; set; }
        [MaxLength(20)]
        public string LevelCode { get; set; }
        [MaxLength(250)]
        public string LevelName { get; set; }
    }
    public enum UserType : byte
    {
        [Description("Tâm Trí Lực")]
        TTL,
        [Description("Đại Lý")]
        Agency,
        [Description("Website")]
        Website
    }
    public enum VerifyType : byte
    {
        [Description("Email")]
        Email,
        [Description("SMS")]
        SMS
    }
    public enum CardSource : byte
    {
        [Description("Tạo Mới Thẻ STNHD")]
        CardSTNHD,
        [Description("Đơn Hàng Bán")]
        SalesOrder,
        [Description("Phiếu Nhận Hàng")]
        SalesShipment,
        [Description("Đại Lý Bán Mã Kích Hoạt")]
        AgencyBuyActivation
    }
    public enum CardType : byte
    {
        [Description("AgencyCard")]
        AgencyCard,
        [Description("CustomerCard")]
        CustomerCard,
        [Description("Khách Lẻ")]
        Retail
    }
    public class M_AgencyAndCardHistory
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdHistory { get; set; }
        public int AgencyId { get; set; }
        public long Id { get; set; }
        [MaxLength(80)]
        public string LotNumber { get; set; }
        [MaxLength(50)]
        public string SerialNumber { get; set; }
        [MaxLength(50)]
        public string CardNo { get; set; }
        [MaxLength(50)]
        public string PhoneNo { get; set; }
        [MaxLength(80)]
        public string FulName { get; set; }
        [MaxLength(10)]
        public string OTP { get; set; }
        public decimal Price { get; set; }
        [MaxLength(250)]
        public string UserName { get; set; }
        [MaxLength(250)]
        public string UserFullName { get; set; }
        public DateTime DateCreate { get; set; }
        public UserType UserType { get; set; }
        public string UserNameActive { get; set; }
        public string UserActiveFullName { get; set; }
        public DateTime LastDateUpdate { get; set; }
        public int StatusId { get; set; }
        [MaxLength(20)]
        public string StatusCode { get; set; }
        [MaxLength(250)]
        public string StatusName { get; set; }
        public VerifyType VerifyType { get; set; }
        [MaxLength(80)]
        public string VerifyInfo { get; set; }
        public int DurationId { get; set; }
        [MaxLength(20)]
        public string DurationCode { get; set; }
        [MaxLength(250)]
        public string DurationName { get; set; }
        public int ReasonId { get; set; }
        [MaxLength(20)]
        public string ReasonCode { get; set; }
        [MaxLength(250)]
        public string ReasonName { get; set; }
        public int LevelId { get; set; }
        [MaxLength(20)]
        public string LevelCode { get; set; }
        [MaxLength(250)]
        public string LevelName { get; set; }
        public SalesType SalesType { get; set; }
        public string Code { get; set; }
    }
}
