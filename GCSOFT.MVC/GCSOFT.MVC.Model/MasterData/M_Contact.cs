﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.MasterData
{
    public class M_Contact
    {
        [Key, Column(Order = 0)]
        public int Id { get; set; }
        [Key, Column(Order = 1)]
        public int Type { get; set; }
        [Key, Column(Order = 2)]
        public int ReferId { get; set; }
        public string Phone { get; set; }
        public string PhoneSystem { get; set; }
        public string Email { get; set; }
    }
}
