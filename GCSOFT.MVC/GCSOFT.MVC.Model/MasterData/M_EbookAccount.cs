﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.MasterData
{
    public class M_EbookAccount
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [MaxLength(250)]
        public string FullName { get; set; }
        [MaxLength(250)]
        public string Email { get; set; }
        [MaxLength(80)]
        public string PhoneNo { get; set; }
        public bool hasEbook { get; set; }
    }
}
