﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.MasterData
{
    public class M_FileResource
    {
        [Key, Column(Order = 0)]
        public ResultType Type { get; set; }
        [Key, Column(Order = 1)]
        public int ReferId { get; set; }
        [Key, Column(Order = 2)]
        public int LineNo { get; set; }
        public int CommentId { get; set; }

        [MaxLength(300)]
        public string Title { get; set; }
        [MaxLength(300)]
        public string Title2 { get; set; }
        [MaxLength(300)]
        public string Title3 { get; set; }
        [MaxLength(300)]
        public string Title4 { get; set; }
        [MaxLength(300)]
        public string ImageName { get; set; }
        public string ImageBase64 { get; set; }
        public string ImagePath { get; set; }
        public FileType FileType { get; set; }
        public Source Source { get; set; }
    }
    public enum FileType : byte
    {
        [Description("Ảnh")]
        ImagePath,
        [Description("Base64")]
        Base64
    }
    public enum Source : byte
    {
        [Description("BackEnd")]
        BackEnd,
        [Description("FrontEnd")]
        FrontEnd
    }
}
