﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.MasterData
{
    public class M_HistoryCall
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int? PotentialId { get; set; }
        public int AgencyId { get; set; }
        public string UserName { get; set; }
        public string Title { get; set; }
        public string FullDeskcription { get; set; }
        public string NextAction { get; set; }
        public string Serinumber { get; set; }
        public string Relationship { get; set; }
        public string FullName { get; set; }
        public string Note { get; set; }
        public DateTime? DateInput { get; set; }
        public string DateInputStr { get { return string.Format("{0:dd/MM/yyyy}", DateInput); } }
        public DateTime? NextDate { get; set; }
        public string NextDateStr { get { return string.Format("{0:dd/MM/yyyy}", NextDate); } }
    }
}
