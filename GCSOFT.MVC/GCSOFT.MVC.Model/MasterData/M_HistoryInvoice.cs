﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.MasterData
{
    public class M_HistoryInvoice
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Code { get; set; }
        public int UserId_Change { get; set; }
        public string UserName_Change { get; set; }
        public string ReasonName { get; set; }
        public DateTime? DateChange { get; set; }
        public string DateChangeStr { get { return string.Format("{0:dd/MM/yyyy}", DateChange); } }
       
    }
}
