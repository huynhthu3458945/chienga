﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.MasterData
{
    public enum ItemType : byte
    {
        [Description("Siêu Trí Nhớ Học Đường")]
        STNHD
    }
    public class M_Item
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public ItemType ItemType { get; set; }
        [MaxLength(20)]
        public string Code { get; set; }
        [MaxLength(250)]
        public string Title { get; set; }
        [MaxLength(2000)]
        public string Description { get; set; }
        public decimal Price { get; set; }
        [MaxLength(500)]
        public string Book { get; set; }
        [MaxLength(500)]
        public string VideosKTGN { get; set; }
        [MaxLength(500)]
        public string VideosSGK { get; set; }
        [MaxLength(500)]
        public string MindmapSGK { get; set; }
        [MaxLength(500)]
        public string Social { get; set; }
        [MaxLength(20)]
        public string FontColor { get; set; }
        [MaxLength(20)]
        public string bgColor { get; set; }
        [MaxLength(20)]
        public string FontColorActive { get; set; }
        [MaxLength(20)]
        public string bgColorActive { get; set; }
        public int LevelId { get; set; }
        [MaxLength(20)]
        public string LevelCode { get; set; }
        [MaxLength(250)]
        public string LevelName { get; set; }
        public int DurationId { get; set; }
        [MaxLength(20)]
        public string DurationCode { get; set; }
        [MaxLength(250)]
        public string DurationName { get; set; }
        [MaxLength(2000)]
        public string Remark { get; set; }
        [MaxLength(500)]
        public string ItemNameV2 { get; set; }
        [MaxLength(200)]
        public string CompareTitle1 { get; set; }
        [MaxLength(200)]
        public string CompareTitle2 { get; set; }
        [MaxLength(200)]
        public string CompareTitle3 { get; set; }
        [MaxLength(200)]
        public string ComparePrice1 { get; set; }
        [MaxLength(200)]
        public string ComparePrice2 { get; set; }
        [MaxLength(200)]
        public string ComparePrice3 { get; set; }
        [MaxLength(500)]
        public string TitleV2 { get; set; }
        [MaxLength(500)]
        public string Save { get; set; }
        [MaxLength(500)]
        public string Label { get; set; }
    }
}
