﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.MasterData
{
    public class M_Language
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        [MaxLength(20)]
        [Column(TypeName = "varchar")]
        public string Code { get; set; }
        [MaxLength(20)]
        [Column(TypeName = "varchar")]
        public string RegionCode { get; set; }
        [MaxLength(80)]
        public string Name { get; set; }
        public bool IsDefault { get; set; }
        public int? IdStatus { get; set; }
        [MaxLength(150)]
        public string StatusName { get; set; }
        [Column(TypeName = "varchar")]
        [MaxLength(250)]
        public string ImagePathForWeb { get; set; }
    }
}
