﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.MasterData
{
    public class M_LoginEntry
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Index("IX_GetUserInfo", 1)]
        [Index("IX_CheckUserLogin", 1)]
        public bool LoggedIn { get; set; }
        [Index("IX_GetUserInfo", 2)]
        [Index("IX_CheckUserLogin", 2)]
        [MaxLength(80)]
        public string userName { get; set; }
        [Index("IX_CheckUserLogin", 3)]
        [MaxLength(40)]
        public string SessionId { get; set; }
        [Index("IX_GetUserInfo", 3)]
        public DateTime DateLogin { get; set; }
        [MaxLength(20)]
        public string IP { get; set; }
        [MaxLength(20)]
        public string IpType { get; set; }
        [MaxLength(20)]
        public string continent_code { get; set; }
        [MaxLength(40)]
        public string continent_name { get; set; }
        [MaxLength(20)]
        public string country_code { get; set; }
        [MaxLength(40)]
        public string country_name { get; set; }
        [MaxLength(20)]
        public string region_code { get; set; }
        [MaxLength(60)]
        public string region_name { get; set; }
        [MaxLength(80)]
        public string city { get; set; }
        [MaxLength(20)]
        public string zip { get; set; }
        [MaxLength(20)]
        public string latitude { get; set; }
        [MaxLength(20)]
        public string longitude { get; set; }
    }
}
