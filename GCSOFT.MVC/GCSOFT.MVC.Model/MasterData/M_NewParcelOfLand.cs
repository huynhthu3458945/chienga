﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.MasterData
{
    public class M_NewParcelOfLand
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int Id_Parce { get; set; }
        public string Code { get; set; }
        public int Map { get; set; }
        public int Parcel { get; set; }
        public decimal Area { get; set; }
        public string Ward { get; set; }
        public string SoilType { get; set; }
        public string Volatility { get; set; }
        public string Note { get; set; }
    }
}
