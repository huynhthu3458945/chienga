﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GCSOFT.MVC.Model.MasterData
{
    public class M_OptionLine
    {
        [Key]
        [Column(Order = 1)]
        public int OptionHeaderID { get; set; }
        [Key]
        [Column(Order = 2)]
        public int LineNo { get; set; }
        [MaxLength(80)]
        public string OptionHeaderCode { get; set; }
        [MaxLength(20)]
        public string Code { get; set; }
        [MaxLength(80)]
        public string OptionHeaderName { get; set; }
        [MaxLength(250)]
        public string Name { get; set; }
        public int Order { get; set; }
        public bool IsHidden { get; set; }
    }
}
