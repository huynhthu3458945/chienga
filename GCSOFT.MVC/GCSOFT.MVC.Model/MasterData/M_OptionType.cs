﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GCSOFT.MVC.Model.MasterData
{
    public class M_OptionType
    {
        [Key]
        public int ID { get; set; }
        public string Code { get; set; }
        [MaxLength(80)]
        public string Name { get; set; }
        public bool IsSystem { get; set; }
        public string Description { get; set; }
        public virtual IEnumerable<M_OptionLine> Optionlines { get; set; }
    }
}
