﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.MasterData
{
    public class M_ParcelOfLand
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Code { get; set; }
        public string Vocative1 { get; set; }
        public string Vocative2 { get; set; }
        public string FullNameMale { get; set; }
        public string FullNameFemale { get; set; }
        public int YearBirthMale { get; set; }
        public int YearBirthFemale { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Ward { get; set; }
        public string SoilType { get; set; }
        public string Volatility { get; set; }
        public DateTime DateCreate { get; set; }
        public int Orders { get; set; }
        public string CMND1 { get; set; }
        public string CMND2 { get; set; }
    }
}
