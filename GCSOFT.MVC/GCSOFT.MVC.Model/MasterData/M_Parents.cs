﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.MasterData
{
    public class M_Parents
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        [MaxLength(80)]
        public string FirstName { get; set; }
        [MaxLength(80)]
        public string LastName { get; set; }
        [MaxLength(160)]
        public string FullName { get; set; }
        [MaxLength(500)]
        public string FB_Link { get; set; }
        [MaxLength(160)]
        public string FB_Name { get; set; }
        public virtual IEnumerable<M_Contact> Contacts { get; set; }
        public virtual IEnumerable<M_Children> Childrens { get; set; }
    }
    public class M_Children
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public int ParentsId { get; set; }
        [MaxLength(80)]
        public string FirstName { get; set; }
        [MaxLength(80)]
        public string LastName { get; set; }
        [MaxLength(160)]
        public string FullName { get; set; }
        public int Class_Id { get; set; }
        [MaxLength(20)]
        public string Class_Code { get; set; }
        [MaxLength(250)]
        public string Class_Name { get; set; }
    }
}
