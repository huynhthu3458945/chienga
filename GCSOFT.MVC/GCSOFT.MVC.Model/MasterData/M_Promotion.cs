﻿// #################################################################
// # Copyright (C) 2021-2022, Tâm Trí Lực JSC.  All Rights Reserved.                       
// #
// # History：                                                                        
// #	Date Time	    Updated		    Content                	
// #    25/08/2021	    Huỳnh Thử 		Update
// ##################################################################

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.MasterData
{
    public class M_Promotion
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [MaxLength(20)]
        [Index("IX_Code_M_Promotion", 1, IsUnique = true)]
        public string Code { get; set; }
        [MaxLength(250)]
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string Prefix { get; set; }
        public string Remark { get; set; }
    }
}
