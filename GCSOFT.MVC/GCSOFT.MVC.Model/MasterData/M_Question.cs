﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.MasterData
{
    public class M_Question
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public QuestionType Type { get; set; }
        [Column(TypeName = "ntext")]
        public string Question { get; set; }
        [Column(TypeName = "ntext")]
        public string Answer { get; set; }
        public int GroupId { get; set; }
        [MaxLength(250)]
        public string GroupName { get; set; }
        public int QuestionerId { get; set; }
        [MaxLength(250)]
        public string QuestionerFB { get; set; }
        [MaxLength(250)]
        public string QuestionerName { get; set; }
        public DateTime? DateQuestion { get; set; }
        public DateTime? DateAnswer { get; set; }
        public int LevelId { get; set; }
        [MaxLength(80)]
        public string LevelName { get; set; }
        public int ClassId { get; set; }
        [MaxLength(20)]
        public string ClassCode { get; set; }
        [MaxLength(120)]
        public string ClassName { get; set; }
        public int CourseId { get; set; }
        [MaxLength(250)]
        public string CourseName { get; set; }
        public int CourseContentLineNo { get; set; }
        [MaxLength(250)]
        public string CourseContentName { get; set; }
        public int ParentsId { get; set; }
        [MaxLength(250)]
        public string ParentsName { get; set; }
        public int OwnerId { get; set; }
        [MaxLength(250)]
        public string OwnerName { get; set; }
        public int MethodId { get; set; }
        [MaxLength(20)]
        public string MethodCode { get; set; }
        [MaxLength(250)]
        public string MethodName { get; set; }
        public int StatusId { get; set; }
        [MaxLength(20)]
        public string StatusCode { get; set; }
        [MaxLength(250)]
        public string StatusName { get; set; }
        public IEnumerable<M_FileResource> FileResources { get; set; }
        [MaxLength(80)]
        public string userName { get; set; }
        [MaxLength(250)]
        public string fullName { get; set; }
        public int CommentId { get; set; }
        public int MyProperty { get; set; }
        public string QuestionFiles { get; set; }
        public string AnswerFiles { get; set; }
    }
    public enum QuestionType : byte
    {
        [Description("Câu Hỏi")]
        Question,
        [Description("Mẫu Tham Khảo")]
        Sample,
        [Description("Góp Ý")]
        Suggestions
    }
    public class M_QuestionFilter
    {
        public string Questioner { get; set; }
        public string QuestionTitle { get; set; }
        public string TeacherName { get; set; }
        public string StatusCode { get; set; }
    }
}
