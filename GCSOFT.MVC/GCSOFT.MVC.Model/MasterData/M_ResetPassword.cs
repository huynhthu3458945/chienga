﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.MasterData
{
    public class M_ResetPassword
    {
        [Key]
        public Guid Id { get; set; }
        [MaxLength(80)]
        public string UserName { get; set; }
        [MaxLength(250)]
        public string Email { get; set; }
        [MaxLength(120)]
        public string PhoneNo { get; set; }
        public DateTime DocumentDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string Password { get; set; }
        public ResetType Type { get; set; }
        public string Description { get; set; }
        [MaxLength(120)]
        public string VerifyInfo { get; set; }
    }
    public enum ResetType : byte
    {
        [Description("Mật Khẩu")]
        Password,
        [Description("Tên Đăng Nhập")]
        Username
    }
}
