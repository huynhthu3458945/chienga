﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.MasterData
{
    public class M_ResultEntry
    {
        [Key, Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Key, Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public ResultType Type { get; set; }
        [Key, Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ReferId { get; set; }
        public int? LineNo { get; set; }
        [Column(TypeName = "ntext")]
        public string Question { get; set; }
        [Column(TypeName = "ntext")]
        public string Answer { get; set; }
        [Column(TypeName = "ntext")]
        public string QuestionUnsigned { get; set; }
        [Column(TypeName = "ntext")]
        public string AnswerUnsigned { get; set; }
        public DateTime? QuestionDate { get; set; }
        public DateTime? AnswerDate { get; set; }
        public int? Questioner_Id { get; set; }
        [MaxLength(250)]
        public string Questioner_FB_Link { get; set; }
        [MaxLength(250)]
        public string Questioner_FB_Name { get; set; }
        public int? Supporter_Id { get; set; }
        [MaxLength(250)]
        public string Supporter_Name { get; set; }
        public int? Parent_Id { get; set; }
        [MaxLength(250)]
        public string parent_Name { get; set; }
        public int? Level_Id { get; set; }
        [MaxLength(250)]
        public string Level_Name { get; set; }
        public int? Class_Id { get; set; }
        [MaxLength(250)]
        public string Class_Name { get; set; }
        public int? Course_Id { get; set; }
        [MaxLength(250)]
        public string Course_Name { get; set; }
        public int? Method_Id { get; set; }
        [MaxLength(250)]
        public string method_Name { get; set; }
        public int? Group_Id { get; set; }
        [MaxLength(250)]
        public string GroupName { get; set; }
        public int? Status_Id { get; set; }
        [MaxLength(250)]
        public string Status_Name { get; set; }
        [MaxLength(80)] 
        public string userName { get; set; }
        [MaxLength(250)]
        public string fullName { get; set; }
    }
    public enum ResultType : byte
    {
        [Description("Câu Hỏi")]
        Question,
        [Description("Bài Mẫu")]
        Sample,
        [Description("Môn Học")]
        Cource,
        [Description("Góp Ý")]
        Suggestions,
        [Description("Người Hổ Trợ")]
        Supporter,
        [Description("Thảo Luận")]
        Commment,
        [Description("Ảnh Đại Diện Môn Học")]
        AvatarCourse
    }
}
