﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.MasterData
{
    public class M_UserInfo
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [MaxLength(80)]
        public string userName { get; set; }
        [MaxLength(250)]
        public string fullName { get; set; }
        public string Password { get; set; }
        [MaxLength(250)]
        public string Email { get; set; }
        [MaxLength(120)]
        public string Phone { get; set; }
        [MaxLength(120)]
        public string Phone2 { get; set; }
        public bool ActivePhone { get; set; }
        [MaxLength(500)]
        public string Address { get; set; }
        public string token { get; set; }
        //public int? specialId { get; set; }
        public bool HasClass { get; set; }
        //[MaxLength(250)]
        //public string facebook { get; set; }
        //[MaxLength(250)]
        //public string zalo { get; set; }
        //[MaxLength(250)]
        //public string viber { get; set; }
        [MaxLength(50)]
        public string Serinumber { get; set; }
        public int ClassId { get; set; }
        public bool HasToken { get; set; }
        public bool HasVideoToken { get; set; }
        public bool HasEbook { get; set; }
        [MaxLength(80)]
        public string ChildFullName1 { get; set; }
        public int Child1_ClassId { get; set; }
        [MaxLength(250)]
        public string Child1_SchoolName { get; set; }
        [MaxLength(250)]
        public string Child2_SchoolName { get; set; }
        [MaxLength(80)]
        public string ChildFullName2 { get; set; }
        public int Child2_ClassId { get; set; }
        
        public int? CityId { get; set; }
        [MaxLength(250)]
        public string CityName { get; set; }
        public int? DistrictId { get; set; }
        [MaxLength(250)]
        public string DistrictName { get; set; }
        public DateTime? DateActive { get; set; }
        public DateTime? DateExpired { get; set; }
        public int? AgencyId { get; set; }
        public STNHDType? STNHDType { get; set; }
        [MaxLength(1000)]
        public string Avartar { get; set; }
        public bool IsUpdateLevelCode300 { get; set; }
        public DateTime? DateCreate { get; set; }
        public DateTime? Dayofbirth { get; set; }
        [MaxLength(40)]
        public string GenderCode { get; set; }
        [MaxLength(80)]
        public string GenderName { get; set; }
        [MaxLength(250)]
        public string FromSource { get; set; }
        [MaxLength(40)]
        public string IsDaiSu { get; set; }
        [MaxLength(80)]
        public string ReferralCode { get; set; }
        [MaxLength(40)]
        public string RoleCode { get; set; }
        [MaxLength(80)]
        public string RoleName { get; set; }
        public int maxdevicelogin { get; set; }
        public string BackgroundUrl { get; set; }
        [MaxLength(30)]
        public string Symbol { get; set; }
        [MaxLength(30)]
        public string SBD { get; set; }
        public bool IsActiveGrade1 { get; set; }
        //[MaxLength(40)]
        //public string AppID { get; set; }
        public bool IsActive { get; set; }
    }
    public enum STNHDType : byte
    {
        [Description("Chưa đăng ký trải nghiệm")]
        NotExperienced,
        [Description("Tài khoản trải nghiệm")]
        ExperienceAccount,
        [Description("Tài khoản đã kích hoạt")]
        Active,
        [Description("Tài khoản hết hạn")]
        Expired,
        [Description("Tài khoản hết hạn trải nghiệm")]
        ExpiredExperience
    }

    public class M_UserInfoOfGroup
    {
        //-- group: UIG
        [MaxLength(40)]
        [Key, Column(Order = 0)]
        public string GroupCode { get; set; }
        [MaxLength(250)]
        public string GroupName { get; set; }
        [Key, Column(Order = 1)]
        public int userId { get; set; }
        [MaxLength(250)]
        public string userName { get; set; }
        public DateTime DateInput { get; set; }
    }
    public class M_UserInfoFilter
    {
        public string fullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
}
