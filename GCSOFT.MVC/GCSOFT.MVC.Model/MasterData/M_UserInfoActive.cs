﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.MasterData
{
    public class M_UserInfoActive
    {
        [Key]
        public Guid Id { get; set; }
        public int UserId{ get; set; }
        [MaxLength(250)]
        public string Username { get; set; }
        [MaxLength(250)]
        public string AppID { get; set; }
        public DateTime DateActive { get; set; }
        public DateTime DateExpired { get; set; }
        public bool IsActive { get; set; }
        public string Serinumber{ get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }
        [Column(TypeName = "ntext")]
        public string Remark { get; set; }
    }
}
