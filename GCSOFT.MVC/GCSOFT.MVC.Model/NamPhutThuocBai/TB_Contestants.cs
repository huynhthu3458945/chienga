﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.NamPhutThuocBai
{
    public class TB_Contestants
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public bool IsAccountSTNHD { get; set; }
        [MaxLength(40)]
        public string Username { get; set; }
        public string Password { get; set; }
        [MaxLength(20)]
        public string IdentificationNumber { get; set; }
        [MaxLength(250)]
        public string ParentName { get; set; }
        [MaxLength(250)]
        public string Email { get; set; }
        [MaxLength(80)]
        public string Phone { get; set; }
        [MaxLength(250)]
        public string ChildName { get; set; }
        [MaxLength(250)]
        public string SchoolName { get; set; }
        public int GradeId { get; set; }
        [MaxLength(250)]
        public string GradeName { get; set; }
        public int CityId { get; set; }
        [MaxLength(250)]
        public string CityName { get; set; }
        public int DistrictId { get; set; }
        [MaxLength(250)]
        public string DistrictName { get; set; }
        [MaxLength(800)]
        public string Address { get; set; }
        public DateTime DateCreate { get; set; }
    }
}
