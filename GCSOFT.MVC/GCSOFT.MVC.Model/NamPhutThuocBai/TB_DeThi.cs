﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.NamPhutThuocBai
{
    public class TB_DeThi
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int ClassId { get; set; }
        [MaxLength(40)]
        public string Code { get; set; }
        public string Title { get; set; }
        [MaxLength(2000)]
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        [MaxLength(50)]
        public string NgayBatDauStr { get; set; }
        [MaxLength(50)]
        public string NgayKetThucStr { get; set; }
        public bool IsActive { get; set; }
    }
}
