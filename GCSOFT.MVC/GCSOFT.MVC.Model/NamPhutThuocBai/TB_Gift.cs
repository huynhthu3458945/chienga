﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.NamPhutThuocBai
{
    public class TB_Gift
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [MaxLength(40)]
        public string Code { get; set; }
        public int Point { get; set; }
        [MaxLength(250)]
        public string GiftName { get; set; }
        public bool IsShow { get; set; }
    }
}
