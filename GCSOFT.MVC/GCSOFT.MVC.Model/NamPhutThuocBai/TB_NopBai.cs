﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.NamPhutThuocBai
{
    public class TB_NopBai
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [MaxLength(40)]
        public string userName { get; set; }
        [MaxLength(40)]
        public string SBD { get; set; }
        [MaxLength(250)]
        public string FullName { get; set; }
        [MaxLength(40)]
        public string CodeDethi { get; set; }
        public int IdContestants { get; set; }
        [MaxLength(2000)]
        public string LinkFacebok { get; set; }
        public int LuotView { get; set; }
        public int? LuotInteract { get; set; }
        public int LuotShare { get; set; }
        public int LuotComment { get; set; }
        public int IdTrongTaiVong1 { get; set; }
        public int IdTrongTaiChuyenMon1 { get; set; }
        public int IdTrongTaiChuyenMon2 { get; set; }
        public DateTime DateInput { get; set; }
        public int TieuChi1 { get; set; }
        public int TieuChi2 { get; set; }
        public int TieuChi3 { get; set; }
        public int TieuChi4 { get; set; }
        public int TieuChi5 { get; set; }
        public int ChuyenMon1_TrinhBay1 { get; set; }
        public int ChuyenMon1_TrinhBay2 { get; set; }
        public int ChuyenMon1_TrinhBay3 { get; set; }
        public int ChuyenMon1_NoiDung { get; set; }
        public int ChuyenMon1_Mindmap { get; set; }
        public int ChuyenMon2_TrinhBay1 { get; set; }
        public int ChuyenMon2_TrinhBay2 { get; set; }
        public int ChuyenMon2_TrinhBay3 { get; set; }
        public int ChuyenMon2_NoiDung { get; set; }
        public int ChuyenMon2_Mindmap { get; set; }
        public int TongDiem { get; set; }
        public int Point { get; set; }
        [MaxLength(40)]
        public string Vong1StatusCode { get; set; }
        [MaxLength(250)]
        public string Vong1StatusName { get; set; }
        [MaxLength(40)]
        public string StatusCode { get; set; }
        [MaxLength(250)]
        public string StatusName { get; set; }
        [MaxLength(40)]
        public string ChuyenMon1_StatusCode { get; set; }
        [MaxLength(250)]
        public string ChuyenMon1_StatusName { get; set; }
        [MaxLength(40)]
        public string ChuyenMon2_StatusCode { get; set; }
        [MaxLength(250)]
        public string ChuyenMon2_StatusName { get; set; }
        public bool IsChuyenMon { get; set; }
        public bool IsChuyenMonVong1 { get; set; }
        [MaxLength(40)]
        public string UserTrongTaiVong1 { get; set; }
        
        [MaxLength(40)]
        public string UserTrongTaiChuyenMon1 { get; set; }
        [MaxLength(40)]
        public string UserTrongTaiChuyenMon2 { get; set; }
        public int TongDiemVong1 { get; set; }
        public string Remark1 { get; set; }
        public string Remark2 { get; set; }
        public string Remark3 { get; set; }
        public int ClassId { get; set; }
        [MaxLength(40)]
        public string ClassName { get; set; }

        public string FullNameTrongTaiVong1 { get; set; }
        public string FullNameTrongTaiChuyenMon1 { get; set; }
        public string FullNameTrongTaiChuyenMon2 { get; set; }
        public string NhaDtXuatSacCode { get; set; }
        public string NhaDtXuatSacName { get; set; }
        public string NhaDTTruyenCamHungCode { get; set; }
        public string NhaDTTruyenCamHungName { get; set; }
        public virtual IEnumerable<TB_MindMapPoint> MindMapPoints { get; set; }
    }
    public class TB_MindMapPoint
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int IdNopBai { get; set; }
        [MaxLength(40)]
        public string PointCode { get; set; }
        [MaxLength(2000)]
        public string PointDescription { get; set; }
        public int Point { get; set; }
        public string Remark { get; set; }
    }
}
