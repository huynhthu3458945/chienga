﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.QuickQuiz
{
    public class QR_HistoryStudy_User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }
        public int class_id { get; set; }
        public int course_id { get; set; }
        public int lession_Id { get; set; }
        [Index("IX_Learn", 1)]
        public int user_Id { get; set; }
        public DateTime start_date { get; set; }
        public DateTime end_date { get; set; }
        public int times { get; set; }
        public int study_time { get; set; }
        public int result { get; set; }
        [Index("IX_Learn", 2)]
        [MaxLength(250)]
        public string function_name { get; set; }
        [Index("IX_Learn", 3)]
        [MaxLength(20)]
        public string language { get; set; }
        public int percent { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int user_created { get; set; }
        public int user_updated { get; set; }
        public bool delete_flag { get; set; }
        public int point_system { get; set; }
        public int point_success { get; set; }
    }
}
