﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.QuickQuiz
{
    public class QR_Question
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }
        public int class_id { get; set; }
        public int course_id { get; set; }
        public int lession_Id { get; set; }
        public string question { get; set; }
        [Column(TypeName = "ntext")]
        public string description { get; set; }
        public int status { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int user_created { get; set; }
        public int user_updated { get; set; }
        public bool delete_flag { get; set; }
        public int point { get; set; }
        public string QuestionId { get; set; }
        public string Level { get; set; }
        public string ExplainTheAnswer { get; set; }
        public string JammingScheme { get; set; }
        public int time_second { get; set; }
        public bool top { get; set; }
        public string linkImages { get; set; }
    }
    public class QR_Question_Answer
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }
        public Guid question_id { get; set; }
        public string title { get; set; }
        public string answer { get; set; }
        public string description { get; set; }
        public string links { get; set; }
        public bool answer_flag { get; set; }
        public int status { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int user_created { get; set; }
        public int user_updated { get; set; }
        public bool delete_flag { get; set; }
        public int sortOrder { get; set; }
    }
    public class QR_Question_Result
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public Guid question_user_id { get; set; }
        public int user_id { get; set; }
        public Guid question_id { get; set; }
        public Guid answer_id { get; set; }
        public string answer_content { get; set; }
        public string links { get; set; }
        public bool answer_flag { get; set; }
        public int status { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int user_created { get; set; }
        public int user_updated { get; set; }
        public bool delete_flag { get; set; }
        
    }
    public class QR_Question_User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }
        public int class_id { get; set; }
        public int course_id { get; set; }
        public int lession_Id { get; set; }
        public int user_Id { get; set; }
        public int total_question { get; set; }
        public int total_quesstion_sucess { get; set; }
        public int point_system { get; set; }
        public int point_success { get; set; }
        public int time_system { get; set; }
        public int time_success { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int user_created { get; set; }
        public int user_updated { get; set; }
        public bool delete_flag { get; set; }
        public int top { get; set; }
    }
    public class QR_Honor_Week
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public int user_id { get; set; }
        public int week { get; set; }
        public int month { get; set; }
        public int year { get; set; }
        [MaxLength(250)]
        public string point_desc { get; set; }
        [MaxLength(50)]
        public string time_desc { get; set; }
        public int class_id { get; set; }
        public int course_id { get; set; }
        public int lession_Id { get; set; }
        public int level { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int user_created { get; set; }
        public int user_updated { get; set; }
    }
}
