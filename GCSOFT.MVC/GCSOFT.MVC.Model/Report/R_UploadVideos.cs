﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.Report
{
    public class R_UploadVideo
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public UploadType Type { get; set; }
        public int OpeningCalance { get; set; }
        public int QtyOfDay { get; set; }
        public int QtyUploadedOfDay { get; set; }
        public int QtyCumulativeOfDay { get; set; }
        public int PercentComplete { get; set; }
        [MaxLength(500)]
        public string LinkGoogle { get; set; }
        public string Remark { get; set; }
        public DateTime DateCreate { get; set; }
    }
    public enum UploadType : byte
    {
        [Description("Video")]
        Video,
        [Description("Mindmap")]
        Mindmap
    }
}