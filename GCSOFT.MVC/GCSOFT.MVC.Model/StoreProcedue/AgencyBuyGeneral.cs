﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.StoreProcedue
{
    public class AgencyBuyGeneralCount
    {
        public int TotalRecord { get; set; }
    }
    public class AgencyBuyGeneral
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string CityName { get; set; }
        public string DistrictName { get; set; }
        public string Address { get; set; }
        public int Total_VIP { get; set; }
        public int Total_LEVEL { get; set; }
        public int Total_GRADE { get; set; }
        public int CardBuy_VIP { get; set; }
        public int CardBuy_LEVEL { get; set; }
        public int CardBuy_GRADE { get; set; }
        public int CardActive_VIP { get; set; }
        public int CardActive_LEVEL { get; set; }
        public int CardActive_GRADE { get; set; }
        public int CardBuyAll_VIP { get; set; }
        public int CardBuyAll_LEVEL { get; set; }
        public int CardBuyAll_GRADE { get; set; }
        public int TotalCardBuyAll_VIP { get { return Total_VIP - CardBuyAll_VIP; } }
        public int TotalCardBuyAll_LEVEL { get { return Total_LEVEL - CardBuyAll_LEVEL; } }
        public int TotalCardBuyAll_GRADE { get { return Total_GRADE - CardBuyAll_GRADE; } }
        public string Code { get; set; }
    }
}
