﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.StoreProcedue
{
    public class QuanLyTrongTai
    {
        public string userName { get; set; }
        public string FullName { get; set; }
        public string ParentUserName { get; set; }
        public int alevel { get; set; }
    }
}
