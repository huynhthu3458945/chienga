﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.StoreProcedue
{
    public class SPApprovalList
    {
        public int ApprovalType { get; set; }
        public string ApprovalTypeName { get; set; }
        public string ApprovalNo { get; set; }
        public string ApprovalName { get; set; }
        public string Code { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhoneNo { get; set; }
        public DateTime DocumentDate { get; set; }
        public string DocumentDateStr { get { return string.Format("{0:dd/MM/yyyy}", DocumentDate); } }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }
        public int TotalCard { get; set; }
        public decimal TotalAmount { get; set; }
        public string Desc { get; set; }
        public string Key { get; set; }
    }
}
