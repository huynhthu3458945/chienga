﻿using GCSOFT.MVC.Model.AgencyModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.StoreProcedue
{
    public class AgencyParentList
    {
        public int Id { get; set; }
        public string FullName { get; set; }
    }
    public class AgencyParentInfo
    {
        public string Code { get; set; }
        public int Id { get; set; }
        public string AgencyName { get; set; }
        public AgencyType AgencyType { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string CityName { get; set; }
        public string DistrictName { get; set; }
        public string Address { get; set; }
        public int ParentAgencyId { get; set; }
        public string ParentAgency { get; set; }
        public string ParentPhone { get; set; }
        public string ParentEmail { get; set; }
        public string ParentCityName { get; set; }
        public string ParentDistrictName { get; set; }
        public string ParentAddress { get; set; }
        public string CompanyName { get; set; }
    }
}
