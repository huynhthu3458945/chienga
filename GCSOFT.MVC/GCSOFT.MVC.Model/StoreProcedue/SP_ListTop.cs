﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.StoreProcedue
{
    public class SP_ListTop
    {
        public string SBD { get; set; }
        public string FullName { get; set; }
        public string ClassName { get; set; }
        public string IsChuyenMonStr { get; set; }
        public int Tongdiem { get; set; }
        public int TongView { get; set; }
        public int TongVideo { get; set; }
        public int TongTuongTac { get; set; }
        public int TongShare { get; set; }
        public int TongComment { get; set; }
    }
}
