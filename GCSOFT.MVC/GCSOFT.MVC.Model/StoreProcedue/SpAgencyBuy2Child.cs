﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.StoreProcedue
{
    public class SpAgencyBuy2Child
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public int SoTheDangCo { get; set; }
        public int TheDaBan { get; set; }
        public int TheDaBanDaiLy_CTV { get; set; }
        public int TheConLai { get; set; }
        public int TheActiveTrongThang { get; set; }
    }
}
