﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.StoreProcedue
{
    public class SpAgencyDetail
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Level { get; set; }
        public string AgencyType { get; set; }
        public int ParentAgencyId { get; set; }
        public string ParentName { get; set; }
        public int SoTheDangCo { get; set; }
        public int TheDaban { get; set; }
        public int TheDaBanDaiLyCTV { get; set; }
        public int TheConLai { get; set; }
        public int TheActiveTrongThang { get; set; }
    }
}
