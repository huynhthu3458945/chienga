﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.StoreProcedue
{
    public class SpAgencyDetailSales
    {
        public int AgencyId { get; set; }
        public string ParentAgencyName { get; set; }
        public string AgencyTree { get; set; }
        public string AgencyName { get; set; }
        public string AgencyPhone { get; set; }
        public string AgencyEmail { get; set; }
        public string AgencyDistrictName { get; set; }
        public string AgencyCityName { get; set; }
        public string FulName { get; set; }
        public string PhoneNo { get; set; }
        public string SerialNumber { get; set; }
        public string DateBuy { get; set; }
        public string ApplyUser { get; set; }
        public string DateActive { get; set; }
        public string LevelName { get; set; }
        public string DurationName { get; set; }
        public string HinhThucBan { get; set; }
        public string TrangThaiThe { get; set; }
        public bool AllowRecall { get; set; }
        public string DoiTacTuyenTren { get; set; }
        public string DoiTacBanHang { get; set; }
        public string NguoiMua { get; set; }
        public string KichHoatBoi { get; set; }
        public string LoaiThe { get; set; }
        public string MaKichHoat { get; set; }
        public decimal Price { get; set; }
        public string PriceStr { get; set; }
        public string VerifyInfo { get; set; }
    }
}
