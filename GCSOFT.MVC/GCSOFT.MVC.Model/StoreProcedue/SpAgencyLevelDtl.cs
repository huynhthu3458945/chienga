﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.StoreProcedue
{
    public class SpAgencyLevelDtl
    {
        public string LeavelName { get; set; }
        public int LastNoOfMember { get; set; }
        public int NoOfMember { get; set; }
        public int ToDayNoOfMember { get; set; }
    }
}
