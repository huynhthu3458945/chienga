﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.StoreProcedue
{
    public class SpCustomerAgencyBuy
    {
        public int AgencyId { get; set; }
        public string SerialNumber { get; set; }
        public Decimal Price { get; set; }
        public string DateCreate { get; set; }
        public string SellBy { get; set; }
        public string AgencyPhone { get; set; }
        public string AgencyEmail { get; set; }
        public string Address { get; set; }
        public string DistrictName { get; set; }
        public string CityName { get; set; }
        public string FulName { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public string DurationName { get; set; }
        public string LevelName { get; set; }
        public string TrangThaiThe { get; set; }
        public bool AllowRecall { get; set; }
        public string StatusName { get; set; }
        public string Seri { get; set; }
        public string NguoiBan { get; set; }
        public string NguoiMua { get; set; }
        public string LoaiThe { get; set; }
    }
}
