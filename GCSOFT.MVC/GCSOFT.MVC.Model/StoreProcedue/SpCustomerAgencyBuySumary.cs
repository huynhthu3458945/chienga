﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.StoreProcedue
{
    public class SpCustomerAgencyBuySumary
    {
        public int AgencyId { get; set; }
        public string UserActiveFullName { get; set; }
        public string LevelName { get; set; }
        public string DurationName { get; set; }
        public decimal TotalAmount { get; set; }
        public int NoOfSerinumber { get; set; }
        public string UOM { get; set; }
    }
}
