﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.StoreProcedue
{
    public class SpDataPostBD
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string ItemName { get; set; }
        public string Duration { get; set; }
        public string Unit { get; set; }
        public int Actual { get; set; }
        public int TotalAmount { get; set; }
    }
}
