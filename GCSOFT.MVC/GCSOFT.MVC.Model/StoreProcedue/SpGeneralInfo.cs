﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.StoreProcedue
{
    public class SpGeneralInfo
    {
        public string Code { get; set; }
        public string RptName { get; set; }
        public string Bold { get; set; }
        public int QtyPreriod { get; set; }
        public int QtySamePeriod { get; set; }
        public int QtyToday { get; set; }
        public int? QtyPreriodVIP { get; set; }
        public int? QtyPreriodCAP { get; set; }
        public int? QtyPreriodLOP { get; set; }
    }
}
