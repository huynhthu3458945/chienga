﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.StoreProcedue
{
    public class SpTTLBuyCustomerDtl
    {
        public string ReasonName { get; set; }
        public int LastNoOfCard { get; set; }
        public int NoOfCard { get; set; }
        public int ToDayNoOfCard { get; set; }
        public int NoOfCardVIP { get; set; }
        public int NoOfCardCAP { get; set; }
        public int NoOfCardLOP { get; set; }

    }
}
