﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.StoreProcedue
{
    public class SpUserInfo
    {
        public int Id { get; set; }
        public string userName { get; set; }
        public string fullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Addr { get; set; }
        public string ActivePhone { get; set; }
        public string Serinumber { get; set; }
        public string DateActive { get; set; }
        public string DateExpired { get; set; }
        public string Password { get; set; }
    }
}
