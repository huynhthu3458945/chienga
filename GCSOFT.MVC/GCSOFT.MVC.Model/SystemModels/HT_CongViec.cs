﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GCSOFT.MVC.Model.SystemModels
{
    [Table("Sys_Category")]
    public class HT_CongViec
    {
        [Key]
        [MaxLength(20)]
        [Column(TypeName = "varchar")]
        public string Code { get; set; }
        [MaxLength(250)]
        public string Name { get; set; }
        [MaxLength(500)]
        public string Link { get; set; }
        [MaxLength(40)]
        [Column(TypeName = "varchar")]
        public string AreaName { get; set; }
        [MaxLength(40)]
        [Column(TypeName = "varchar")]
        public string ControllerName { get; set; }
        [MaxLength(40)]
        [Column(TypeName = "varchar")]
        public string ActionName { get; set; }
        public bool OnMenu { get; set; }
        public int Priority { get; set; }
        public string Description { get; set; }
        [MaxLength(20)]
        [Column(TypeName = "varchar")]
        public string ParentCode { get; set; }
        [MaxLength(100)]
        public string ParentName { get; set; }
        [MaxLength(20)]
        [Column(TypeName = "varchar")]
        public string ParentPriority { get; set; }
        [MaxLength(20)]
        [Column(TypeName = "varchar")]
        public string ParentPriorityLink { get; set; }
        [NotMapped]
        public virtual IEnumerable<HT_VuViecCuaCongViec> VuViecCuaCongViecs { get; set; }
    }

    [Table("Sys_CategoryFunction")]
    public class HT_VuViecCuaCongViec
    {
        [Key, Column(Order = 0, TypeName = "varchar")]
        [MaxLength(20)]
        public string CategoryCode { get; set; }
        [Key, Column(Order = 1, TypeName = "varchar")]
        [MaxLength(20)]
        public string FunctionCode { get; set; }
    }
}