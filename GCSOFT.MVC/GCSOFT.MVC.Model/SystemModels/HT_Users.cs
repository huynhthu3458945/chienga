﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GCSOFT.MVC.Model.SystemModels
{
    //[Table("Sys_User")]
    public class HT_Users
    {
        [Key]
        public int ID { get; set; }
        [MaxLength(50)]
        public string Username { get; set; }
        [MaxLength(200)]
        public string Password { get; set; }
        [MaxLength(100)]
        public string Email { get; set; }
        [MaxLength(20)]
        [Column(TypeName = "varchar")]
        public string Phone { get; set; }
        [MaxLength(250)]
        public string Description { get; set; }
        public bool Blocked { get; set; }
        [MaxLength(50)]
        public string Position { get; set; }
        [MaxLength(30)]
        public string FirstName { get; set; }
        [MaxLength(30)]
        public string LastName { get; set; }
        [MaxLength(60)]
        public string FullName { get; set; }
        [MaxLength(250)]
        public string Address { get; set; }
        public bool IsUpdatePass { get; set; }
        public bool IsCTV { get; set; } 
        public DateTime? LastDateUpdate { get; set; }
        public virtual IEnumerable<HT_UserThuocNhom> UserOfGroups { get; set; }
    }
}