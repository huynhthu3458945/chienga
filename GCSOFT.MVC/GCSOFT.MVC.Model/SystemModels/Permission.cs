﻿namespace GCSOFT.MVC.Model.SystemModels
{
    public class Permission
    {
        public string CategoryNo { get; set; }
        public string CategoryName { get; set; }
        public string FunctionNo { get; set; }
        public string ParentCategory { get; set; }
        public bool Status { get; set; }
        public string GroupCode { get; set; }
    }

    public class PermissionOnSystemFunc
    {
        public string FunctionNo { get; set; }
        public string FunctionName { get; set; }
        public string CategoryNo { get; set; }
        public bool Permission { get; set; }
    }
}
