﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.Transaction
{
    public class M_Feedback
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int IdCourse { get; set; }
        public int IdLesson { get; set; }
        public FeedbackType FeekbackType { get; set; }
        
        public string Question { get; set; }
        public string Answer { get; set; }

        public int UserId { get; set; }
        [MaxLength(250)]
        public string QuestionName { get; set; }
        [MaxLength(40)]
        public string QuestionPhone { get; set; }
        public DateTime QuestionDate { get; set; }

        [MaxLength(250)]
        public string AnswerName { get; set; }
        [MaxLength(40)]
        public string AnswerPhone { get; set; }
        public DateTime AnswerDate { get; set; }

        [MaxLength(20)]
        public string StatusCode { get; set; }
        [MaxLength(80)]
        public string StatusName { get; set; }
    }
    public enum FeedbackType : byte
    {
        [Description("Videos Kỹ Thuật Ghi Nhớ")]
        VKyThuatGhiNho,
        [Description("Videos Bài Giảng SGK")]
        VBaiGiangSGK
    }
}
