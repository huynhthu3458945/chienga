﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.Transaction
{
    public class M_PaymentResult
    {
        [Key, Column(Order = 0)]
        public PaymentType PaymentType { get; set; }
        [Key, Column(Order = 1)]
        [MaxLength(50)]
        public string TransactionID { get; set; }
        [MaxLength(80)]
        public string TransactionDateTime { get; set; }
        public decimal TotalAmount { get; set; }
        [MaxLength(40)]
        public string CardNumber { get; set; }
        public string ExpiryDate { get; set; }
        [MaxLength(20)]
        public string SubscriptionType { get; set; }
        [MaxLength(10)]
        public string CardType { get; set; }
        [MaxLength(20)]
        public string CardName { get; set; }
        [MaxLength(40)]
        public string AccountNo { get; set; }
        [MaxLength(10)]
        public string SubscriptionSource { get; set; }
        [MaxLength(250)]
        public string Token { get; set; }
        [MaxLength(10)]
        public string ResponseCode { get; set; }
        [MaxLength(80)]
        public string ResponseName { get; set; }
        [MaxLength(2000)]
        public string Description { get; set; }
        public DateTime DatePayment { get; set; }
        public int UserId { get; set; }
        [MaxLength(40)]
        public string Username { get; set; }
    }
    public enum PaymentType : byte
    {
        [Description("Đơn Hàng Bán")]
        Order,
        [Description("Đơn Hàng Trả")]
        ReturnOrder
    }
}
