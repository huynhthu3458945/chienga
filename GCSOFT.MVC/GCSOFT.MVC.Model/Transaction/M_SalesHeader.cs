﻿using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Model.Approval;
using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.Transaction
{
    public class M_SalesHeader
    {
        [Key, Column(Order = 0)]
        public SalesType SalesType { get; set; }
        [Key, Column(Order = 1, TypeName = "varchar")]
        [MaxLength(20)]
        public string Code { get; set; }
        public AgencyType CustomerType { get; set; }
        public int CustomerId { get; set; }
        [MaxLength(250)]
        public string CustomerName { get; set; }
        [MaxLength(250)]
        public string CustomerAddress { get; set; }
        [MaxLength(150)]
        public string CustomerEmail { get; set; }
        [MaxLength(80)]
        public string CustomerPhoneNo { get; set; }

        public DateTime? DocumentDate { get; set; }
        public DateTime? PostingDate { get; set; }
        public DateTime? ReceiptDate { get; set; }

        public int NoOfCard { get; set; }
        public int GiftCard { get; set; }
        public int TotalCard { get; set; }
        [MaxLength(20)]
        public string LotNumber { get; set; }
        public decimal Amount { get; set; }
        public decimal DiscountPercent { get; set; }
        public decimal DiscountAmt { get; set; }
        public int NoOfCardAgency { get; set; }
        public decimal TotalAmount { get; set; }

        public string Remark { get; set; }
        
        public int StatusId { get; set; }
        [MaxLength(20)]
        public string StatusCode { get; set; }
        [MaxLength(80)]
        public string StatusName { get; set; }
        
        public SalesType FromSoure { get; set; }
        [MaxLength(80)]
        public string SourceNo { get; set; }
        
        public int? AgencyId { get; set; }
        [MaxLength(150)]
        public string AgencyName { get; set; }
        [MaxLength(80)]
        public string CreateBy { get; set; }
        public DateTime DateCreate { get; set; }
        [MaxLength(80)]
        public string LastModifyBy { get; set; }
        public DateTime LastModifyDate { get; set; }
        [MaxLength(20)]
        public string ReceiveBy { get; set; }
        [MaxLength(150)]
        public string ReceiveName { get; set; }
        public DateTime? ReceiveDate { get; set; }
        public int DurationId { get; set; }
        [MaxLength(20)]
        public string DurationCode { get; set; }
        [MaxLength(250)]
        public string DurationName { get; set; }
        public int ReasonId { get; set; }
        [MaxLength(20)]
        public string ReasonCode { get; set; }
        [MaxLength(250)]
        public string ReasonName { get; set; }
        public int LevelId { get; set; }
        [MaxLength(20)]
        public string LevelCode { get; set; }
        [MaxLength(250)]
        public string LevelName { get; set; }
        public Guid? PrivateKey { get; set; }
        public decimal? Price { get; set; }
        public string FromLevelCode { get; set; }
        public int? SuggestedNoOfCard { get; set; }
        public string TTLRemark { get; set; }
        public string PromotionCode { get; set; }
        public string TypeSale { get; set; }
        public string ToLevelCode { get; set; }
        public ConvertType ConvertType { get; set; }
        public virtual M_ApprovalInfo ApprovalInfo { get; set; }
        public virtual IEnumerable<M_Flow> Flows { get; set; }
        public virtual IEnumerable<M_SalesLine> SalesLines { get; set; }
        public virtual IEnumerable<M_CardLine> CardLines { get; set; }
        public virtual IEnumerable<M_CardEntry> CardEntries { get; set; }
    }
    public class M_SalesLine
    {
        [Key, Column(Order = 0)]
        public SalesType SalesType { get; set; }
        [Key, Column(Order = 1, TypeName = "varchar")]
        [MaxLength(20)]
        public string DocumentCode { get; set; }
        [Key, Column(Order = 2)]
        public long LineNo { get; set; }
        [MaxLength(20)]
        public string LotNumber { get; set; }
        public long CardId { get; set; }
        public string SerialNumber { get; set; }
        public decimal Price { get; set; }
    }
    public enum SalesType : byte
    {
        [Description("Đơn Hàng Bán")]
        SalesOrder,
        [Description("Đơn Hàng Trả")]
        SalesReturnOrder,
        [Description("Hóa Đơn Bán Hàng")]
        SalesInvoice,
        [Description("Đơn Hàng Đã Giao")]
        SalesShipment,
        [Description("Phiếu Nhập Kho")]
        PurchaseReceipt,
        [Description("Đơn Đặt Hàng")]
        PurchaseOrder,
        [Description("Thu Hồi Thẻ")]
        CardWithdrawal,
        [Description("Bán Thẻ Số Lượng Lớn")]
        Wholesale,
        [Description("Chuyển Thẻ")]
        Convert,
        [Description("Bán Lẻ")]
        Retail
    }
    public enum ConvertType : byte
    {
        [Description("Thẻ VIP sang Lớp, Cấp")]
        V2LC,
        [Description("Thẻ Cấp sang Lớp")]
        C2L,
        [Description("Thẻ Lớp sang VIP, Cấp")]
        L2VC
    }
}
