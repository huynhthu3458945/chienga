﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Model.Videos
{
    public class M_MemoVideos
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        [MaxLength(250)]
        public string TitleVN { get; set; }
        [MaxLength(250)]
        public string TitleEN { get; set; }
        [MaxLength(500)]
        public string LinkVN { get; set; }
        [MaxLength(500)]
        public string LinkEN { get; set; }
        public int SortOrder { get; set; }
        public int ParentSortOrder { get; set; }
        [MaxLength(80)]
        public string Time { get; set; }
        public bool IsExperience { get; set; }
        public string NO { get; set; }
    }
}
