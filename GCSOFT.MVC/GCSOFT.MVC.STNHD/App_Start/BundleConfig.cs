﻿using System.Web;
using System.Web.Optimization;

namespace GCSOFT.MVC.STNHD
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css"
                      , "~/Content/slick.css"
                      , "~/Content/slicktheme.css"
                      , "~/Scripts/jqueryui/jquery-ui.css"
                      , "~/Content/box-comments.css"
                      , "~/Content/index.css"
                      , "~/Content/payment.css"
                      , "~/Content/style.css"
                      , "~/Scripts/process/nprogress.css"
                      , "~/Scripts/fancybox/fancybox.css"
                      , "~/Scripts/dropify/css/dropify.css"
                      , "~/Scripts/ddmenu/ddsmoothmenu.css"
                      ));
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jqueryui/jquery.js"
                        , "~/Scripts/bootstrap/bootstrap.js"
                        , "~/Scripts/jqueryui/jquery-ui.js"
                        , "~/Scripts/slick.js"
                        , "~/Scripts/fontawesome.js"
                        , "~/Scripts/JSubmit.js"
                        , "~/Scripts/process/nprogress.js"
                        , "~/Scripts/fancybox/fancybox.js"
                        , "~/Scripts/handlebars.js"
                        , "~/Scripts/dropify/js/dropify.js"
                        , "~/Scripts/ddmenu/ddsmoothmenu.js"
                        , "~/Scripts/index.js"
                        ));
            BundleTable.EnableOptimizations = false;
        }
    }
}
