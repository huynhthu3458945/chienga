﻿using AutoMapper;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.STNHDService.Interface;
using GCSOFT.MVC.STNHD.Helper;
using GCSOFT.MVC.STNHD.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace GCSOFT.MVC.STNHD.Controllers
{
    public class BaseController : Controller
    {
        private readonly IShareInfoService _shareInfoService;
        private List<LanguageViewModel> languages;
        private string lang = null;
        public BaseController(IShareInfoService shareInfoService)
        {
            _shareInfoService = shareInfoService;
        }
        //public ShareInfo GetShareInfo()
        //{
        //    var shareInfo = new ShareInfo();
        //    if (Session["ShareInfo"] == null)
        //    {
        //        var userInfo = GetUserInfo() ?? new LoginResult();
        //        var _studentClassInit = new List<StudentClassViewModel>();
        //        var _studentClass = new StudentClassViewModel();
        //        if (string.IsNullOrEmpty(userInfo.userName))
        //        {
        //            var classList = Mapper.Map<IEnumerable<ClassList>>(_shareInfoService.ClassList(ClassType.Class, "SHOW"));
        //            foreach (var item in classList)
        //            {
        //                _studentClass = new StudentClassViewModel();
        //                _studentClass.ClassId = item.Id;
        //                _studentClass.ClassName = item.Name;
        //                _studentClassInit.Add(_studentClass);
        //            }
        //            shareInfo.StudentClasses = _studentClassInit;
        //        }
        //        else
        //        {
        //            shareInfo.StudentClasses = Mapper.Map<IEnumerable<StudentClassViewModel>>(_shareInfoService.FindAllByDate(userInfo.email));
        //        }

        //        var idClasses = shareInfo.StudentClasses.Select(d => d.ClassId).ToList();
        //        shareInfo.courseList = Mapper.Map<IEnumerable<CourseList>>(_shareInfoService.CourseList(false, idClasses));
        //        Session["ShareInfo"] = shareInfo;
        //    }

        //    shareInfo = (ShareInfo)Session["ShareInfo"];
        //    return shareInfo;
        //}
        public LoginResult GetUserInfo()
        {
            InitLanguage();
            //var cookieValue = Server.UrlDecode(CookieHelper.GetFromCookie("UserInfo", "LoginResult"));
            string cookieValue = string.Empty;
            if (System.Web.HttpContext.Current.Request.Cookies["UserSTNHD"] != null)
            {
                cookieValue = Server.UrlDecode(System.Web.HttpContext.Current.Request.Cookies["UserSTNHD"].Value);
            }
            if (string.IsNullOrEmpty(cookieValue))
                return null;
            var userInfo = JsonConvert.DeserializeObject<LoginResult>(cookieValue);
            return userInfo;
            //if ((userInfo ?? new LoginResult()).userName.ToLower().Equals("ngocthuystnhd"))
            //    return userInfo;
            //if (string.IsNullOrEmpty(userInfo.SessionId))
            //    userInfo.SessionId = "empty";
            //// check to see if your ID in the Logins table has 
            //// LoggedIn = true - if so, continue, otherwise, redirect to Login page.
            //if (_shareInfoService.IsYourLoginStillTrue(userInfo.userName, userInfo.SessionId))
            //{
            //    // check to see if your user ID is being used elsewhere under a different session ID
            //    if (!_shareInfoService.IsUserLoggedOnElsewhere(userInfo.userName, userInfo.SessionId))
            //    {
            //        return userInfo;
            //    }
            //    else
            //    {
            //        // if it is being used elsewhere, update all their 
            //        // Logins records to LoggedIn = false, except for your session ID
            //        _shareInfoService.LogEveryoneElseOut(userInfo.userName, userInfo.SessionId);
            //        return JsonConvert.DeserializeObject<LoginResult>(cookieValue);
            //    }
            //}
            //else
            //{
            //    //FormsAuthentication.SignOut();
            //    //CookieHelper.RemoveCookie("UserInfo", "LoginResult");
            //    if (System.Web.HttpContext.Current.Request.Cookies["UserSTNHD"] != null)
            //    {
            //        System.Web.HttpCookie cookie = this.ControllerContext.HttpContext.Request.Cookies["UserSTNHD"];
            //        cookie.Expires = DateTime.Now.AddDays(-1);
            //        this.ControllerContext.HttpContext.Response.Cookies.Add(cookie);
            //    }
            //    return null;
            //}
        }
        public string CurrentUrl()
        {
            return HttpUtility.HtmlEncode(System.Web.HttpContext.Current.Request.Url.AbsoluteUri);
        }
        private string RootUrl()
        {
            return string.Format("{0}://{1}{2}",
                                    Request.Url.Scheme,
                                    Request.Url.Host,
                                    Request.Url.Port == 80
                                        ? string.Empty
                                        : ":" + Request.Url.Port);
        }
        /// <summary>
        /// Load image byte64
        /// </summary>
        /// <param name="r">Result Type</param>
        /// <param name="rid">Refer Id</param>
        /// <param name="l">Line No.</param>
        /// <returns></returns>
        public ActionResult Image(ResultType r, int rid, int l)
        {
            string imagePath = string.Format("http://localhost:8080{0}", _shareInfoService.GetFileInfo(r, rid, l));
            //string path = Server.MapPath(imagePath);
            //byte[] imageByteData = System.IO.File.ReadAllBytes(imagePath);

            byte[] imageByteData;
            using (var webClient = new WebClient())
            {
                imageByteData = webClient.DownloadData(imagePath);
            }
            return File(imageByteData, "image/png");
        }
        #region -- Override Function --
        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            if (Session["Languages"] == null)
            {
                languages = Mapper.Map<IEnumerable<LanguageViewModel>>(_shareInfoService.GetAllLanguage()).ToList();
                Session["Languages"] = languages;
            }
            else
                languages = (List<LanguageViewModel>)Session["Languages"];
            HttpCookie langCookie = Request.Cookies["culture"];
            if (langCookie != null)
                lang = langCookie.Value;
            else
                lang = "en";
            new LanguageHelper().SetLanguage(lang, languages);
            return base.BeginExecuteCore(callback, state);
        }
        public void InitLanguage()
        {
            
            string lc = Request.Params["lang"];
            if (string.IsNullOrEmpty(lc))
                lc = Getlanguage();
            if (!string.IsNullOrEmpty(lc))
            {
                new LanguageHelper().SetLanguage(lc, ((IEnumerable<LanguageViewModel>)Session["Languages"]).ToList());
                lang = lc;
            }

            var englishFlat = "https://firebasestorage.googleapis.com/v0/b/tamtriluc-1a443.appspot.com/o/STNHD%2FEnglist.png?alt=media&token=14df94a4-16d8-4204-b833-e7485b91abe2";
            var vietnamFlat = "https://firebasestorage.googleapis.com/v0/b/tamtriluc-1a443.appspot.com/o/STNHD%2Fvietnam.png?alt=media&token=e86289a1-4070-4911-8e83-019f5c3383b6";

            var currentUrl = CurrentUrl();
            currentUrl = (string.IsNullOrEmpty(currentUrl) ? RootUrl() : currentUrl).Replace("?lang=vi-VN", "").Replace("?lang=en-US", "").Replace("&lang=vi-VN", "").Replace("&lang=en-US", "");
            
            ViewBag.MainLangUrl = (currentUrl.Contains("?") ? currentUrl + "&lang=" : "?lang=") + lang;
            ViewBag.MainLangIcon = lang.Equals("vi-VN") ? vietnamFlat : englishFlat;
            ViewBag.MainLang = lang.Equals("vi-VN") ? "Tiếng Việt" : "English";

            ViewBag.OtherLangUrl = (currentUrl.Contains("?") ? currentUrl + "&lang=" : "?lang=") + (lang.Equals("en-US") ? "vi-VN" : "en-US");
            ViewBag.OtherLangIcon = lang.Equals("vi-VN") ? englishFlat : vietnamFlat;
            ViewBag.OtherLang = lang.Equals("vi-VN") ? "English" : "Tiếng Việt";
        }
        public string Getlanguage()
        {
            var lang2 = Request.Params["lang"];
            if (string.IsNullOrEmpty(lang2))
            {
                lang2 = "vi-VN";
                if (Request.Cookies["culture"] != null)
                {
                    lang2 = Request.Cookies["culture"].Value;
                }
            }
            return lang2;
        }
        #endregion
    }
}