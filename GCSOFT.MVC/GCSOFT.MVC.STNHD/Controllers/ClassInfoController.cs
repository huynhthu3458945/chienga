﻿using AutoMapper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.STNHDService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.STNHD.Helper;
using GCSOFT.MVC.STNHD.Models;
using GCSOFT.MVC.STNHD.Models.PageViewModels;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Security;
using System.IO;

namespace GCSOFT.MVC.STNHD.Controllers
{
    public class ClassInfoController : BaseController
    {
        private readonly IShareInfoService _shareInfoService;
        private readonly IStudentClassService _studentClassService;
        private readonly IClassReferService _classReferService;
        private readonly IClassService _classService;
        private readonly IVuViecService _vuViecRepo;
        private readonly IUserInfoService _userInfoService;
        private readonly ISetupService _setupService;
        private readonly ICardLineService _cardLineService;
        private readonly ICustomerService _customerService;
        private string hasValue;
        public ClassInfoController
            (
                IShareInfoService shareInfoService
                , IStudentClassService studentClassService
                , IClassReferService classReferService
                , IClassService classService
                , IVuViecService vuViecRepo
                , IUserInfoService userInfoService
                , ISetupService setupService
                , ICardLineService cardLineService
                , ICustomerService customerService
            ) : base(shareInfoService)
        {
            _shareInfoService = shareInfoService;
            _studentClassService = studentClassService;
            _classReferService = classReferService;
            _classService = classService;
            _vuViecRepo = vuViecRepo;
            _userInfoService = userInfoService;
            _setupService = setupService;
            _cardLineService = cardLineService;
            _customerService = customerService;
        }
        public ActionResult Index()
        {
            var _userInfo = GetUserInfo();
            if (_userInfo == null)
                return RedirectToAction("Index", "Student", new { url = CurrentUrl() });
            var transactionID = StringUtil.ToShortGuid(Guid.NewGuid());
            var classInfos = new ClassInfoViewModel();
            classInfos.UserInfo = GetUserInfo();
            var fristLastNames = StringUtil.getFirstNameLastNameArr(_userInfo.fullName);
            classInfos.PayInfo = new SCBPayment()
            {
                ProfileID = "TAMTRILUC2ciqlm",
                AccessKey = "rpqhlxfppbsoyxsdfjltghal",
                TransactionID = string.Format("{0}-{1}", _userInfo.Id.ToString(), transactionID),
                TransactionDateTime = TransactionDate(),//"2021-03-31T07:42:14Z",
                Language = "vi",
                IsTokenRequest = false,
                SubscribeOnly = false,
                SubscribeWithMin = false,
                Description = string.Format("don hang {0} khoa {1} {2}", string.Format("{0}-{1}", _userInfo.Id.ToString(), transactionID), "tron doi", "1468000"),
                TotalAmount = 10000,
                InternationalFee = 0,
                DomesticFee = 0,
                Currency = "VND",
                SSN = "025240307",
                FirstName = string.IsNullOrEmpty(fristLastNames[0]) ? "." : fristLastNames[0],
                LastName = string.IsNullOrEmpty(fristLastNames[1]) ? "." : fristLastNames[1],
                Gender = "M",
                Address = StringUtil.UppercaseWords(StringUtil.ConvertToUnsign(_userInfo.Address ?? "").Replace('-', ' ')),
                District = ".",
                City = ".",
                PostalCode = "700000",
                Country = "VN",
                Mobile = _userInfo.Phone,
                Email = _userInfo.email,
                CancelUrl = "https://localhost:44357/ClassInfo",
                ReturnUrl = "https://localhost:44357/ActiveAccount/CheckoutResult"
            };
            return View(classInfos);
        }
        private string TransactionDate()
        {
            var today = DateTime.Now;
            return string.Format("{0}-{1}-{2}T00:00:00Z", today.Year, today.Month < 9 ? "0" + today.Month.ToString() : today.Month.ToString(), today.Day < 9 ? "0" + today.Day.ToString() : today.Day.ToString());
        }
        public ActionResult Active()
        {
            var _userInfo = GetUserInfo();
            if (_userInfo == null)
                return RedirectToAction("Index", "Student", new { url = CurrentUrl() });

            var classInfos = new ClassInfoViewModel();
            classInfos.UserInfo = GetUserInfo();
            return View(classInfos);
        }
        public JsonResult LoadLevelByCode(string activeCode)
        {
            activeCode = activeCode.Trim();
            if (string.IsNullOrEmpty(activeCode))
                return Json(new { s = "error", url = "Mã kích hoạt không tồn tại" }, JsonRequestBehavior.AllowGet);
            var valueCheckCode = _cardLineService.CheckActiveCode(activeCode);
            if (valueCheckCode.Equals("Nothing"))
                return Json(new { s = "error", url = "Mã kích hoạt không tồn tại" }, JsonRequestBehavior.AllowGet);
            if (valueCheckCode.Equals("IsActive"))
                return Json(new { s = "error", url = "Mã kích hoạt đã được kích hoạt" }, JsonRequestBehavior.AllowGet);
            var cardInfo = _cardLineService.GetBySerialNumber(valueCheckCode);
            if ((cardInfo.LevelCode ?? string.Empty).Equals("200") || (cardInfo.LevelCode ?? string.Empty).Equals("300"))
                return Json(new { s = "oke", url = "hasClass" }, JsonRequestBehavior.AllowGet);
            return Json(new { s = "oke", url = "Unlimited" }, JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult LoadClassByLevel(string lvCode)
        {
            if (lvCode.Equals("cap1"))
                return PartialView("_Level1");
            else if (lvCode.Equals("cap2"))
                return PartialView("_Level2");
            else if (lvCode.Equals("cap3"))
                return PartialView("_Level3");
            else
                return PartialView("Level");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="activeCode"></param>
        /// <param name="lc">Level Code</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ActiveCode(string activeCode, string levelCode, int classId)
        {
            try
            {
                activeCode = activeCode.Trim();
                if (string.IsNullOrEmpty(activeCode))
                    return Json(new { s = "error", url = "Mã kích hoạt không tồn tại" }, JsonRequestBehavior.AllowGet);
                var valueCheckCode = _cardLineService.CheckActiveCode(activeCode);
                if (valueCheckCode.Equals("Nothing"))
                    return Json(new { s = "error", url = "Mã kích hoạt không tồn tại" }, JsonRequestBehavior.AllowGet);
                if (valueCheckCode.Equals("IsActive"))
                    return Json(new { s = "error", url = "Mã kích hoạt đã được kích hoạt" }, JsonRequestBehavior.AllowGet);
                var cardInfo = _cardLineService.GetBySerialNumber(valueCheckCode);
                var userInfo = GetUserInfo();

                var _userInfo = Mapper.Map<UserInfoViewModel>(_userInfoService.CheckByUsername(userInfo.userName, userInfo.password));
                _userInfo.ActivePhone = true;
                _userInfo.HasEbook = true;
                _userInfo.Serinumber = cardInfo.SerialNumber;
                _userInfo.DateActive = DateTime.Now;
                _userInfo.ClassId = classId;
                _userInfo.STNHDType = STNHDType.Active;
                int noOfDuration = 0;
                try { noOfDuration = int.Parse(StringUtil.RebuildPhoneNo(cardInfo.DurationCode)); }
                catch { noOfDuration = 0; }

                if (cardInfo.DurationCode.EndsWith("Y"))
                    _userInfo.DateExpired = DateTime.Now.AddYears(noOfDuration);
                else if (cardInfo.DurationCode.EndsWith("W")) { 
                    int noOfWeeks = noOfDuration * 7;
                    _userInfo.DateExpired = DateTime.Now.AddDays(noOfWeeks);
                }
                else if (cardInfo.DurationCode.EndsWith("M"))
                    _userInfo.DateExpired = DateTime.Now.AddMonths(noOfDuration);
                else if (cardInfo.DurationCode.EndsWith("D"))
                    _userInfo.DateExpired = DateTime.Now.AddDays(noOfDuration);
                else
                    _userInfo.DateExpired = null;
                hasValue = _userInfoService.Update(Mapper.Map<M_UserInfo>(_userInfo));

                cardInfo.ApplyUser = _userInfo.userName;
                cardInfo.ApplyDate = _userInfo.DateActive;
                cardInfo.IsActive = true;
                hasValue = _cardLineService.Update(cardInfo);
                //-- Add class
                //if (!string.IsNullOrEmpty(levelCode))
                if (cardInfo.LevelCode.Equals("200"))
                    AddStudent2Class(levelCode, _userInfo.userName);
                if (cardInfo.LevelCode.Equals("300"))
                    AddStudent2Class2(classId, _userInfo.userName, _userInfo.DateExpired);
                //-- Add class
                hasValue = _vuViecRepo.Commit();

                var currentUserResult = GetUserInfo();
                var loginResult = Mapper.Map<LoginResult>(_userInfoService.CheckByUsername(userInfo.userName, userInfo.password));
                loginResult.SessionId = currentUserResult.SessionId;
                CookieHelper.RemoveCookie("UserInfo", "LoginResult");
                CookieHelper.StoreInCookie("UserInfo", "", "LoginResult", Server.UrlEncode(JsonConvert.SerializeObject(loginResult)), null);
                if (!loginResult.HasToken)
                    UploadUsertoCloud(loginResult.userName);
                return Json(new { s = "oke", url = Url.Action("Index", "Home") }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { s = "error", url = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult Create(string classIds, string activeCode)
        {
            try
            {
                var userInfo = GetUserInfo();
                var result = string.Empty;
                if (string.IsNullOrEmpty(result))
                {
                    var setup = _setupService.Get();
                    var _studentClasses = new List<StudentClassViewModel>();
                    var _studentClass = new StudentClassViewModel();
                    var _idClasses = classIds.Split(',').Select(item => int.Parse(item)).ToList();
                    var classes = Mapper.Map<IEnumerable<ClassList>>(_shareInfoService.ClassList(ClassType.Class, "SHOW"));
                    foreach (var item in classes)
                    {
                        _studentClass = new StudentClassViewModel();
                        _studentClass.userName = userInfo.userName;
                        _studentClass.FromDate = DateTime.Now;
                        _studentClass.ToDate = DateTime.Now.AddYears((int)setup.Expired);
                        _studentClass.ClassId = item.Id;
                        _studentClass.ClassName = item.Name;
                        _studentClass.IsActive = _idClasses.Contains(item.Id) ? true : false;
                        _studentClasses.Add(_studentClass);
                    }

                    var _userInfo = Mapper.Map<UserInfoViewModel>(_userInfoService.GetByEmail(userInfo.email));
                    _userInfo.HasClass = true;
                    _userInfo.token = CryptorEngine.Encrypt(_userInfo.Password, true, "@stnhdttl.com");
                    
                    hasValue = _studentClassService.Delete(userInfo.email);
                    hasValue = _studentClassService.Insert(Mapper.Map<IEnumerable<M_StudentClass>>(_studentClasses));
                    hasValue = _userInfoService.Update(Mapper.Map<M_UserInfo>(_userInfo));
                    hasValue = _vuViecRepo.Commit();

                    var loginResult = Mapper.Map<LoginResult>(_userInfoService.GetBy(userInfo.email));
                    CookieHelper.RemoveCookie("UserInfo", "LoginResult");
                    CookieHelper.StoreInCookie("UserInfo", "", "LoginResult", Server.UrlEncode(JsonConvert.SerializeObject(userInfo)), null);

                    return Json(new { s = "oke", url = Url.Action("Index", "Home") }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { s = "error", url = result }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                return Json(new { s = "error", url = "B100_1" }, JsonRequestBehavior.AllowGet);
            }
        }
        protected string UploadUsertoCloud(string userName)
        {
            try
            {
                var _userInfo = Mapper.Map<UserInfoViewModel>(_userInfoService.GetBy(userName));
                if (!_userInfo.HasToken)
                {
                    var client = new RestClient("https://cdn.stnhd.com/video/push-userinfo.php");
                    client.Timeout = -1;
                    var request = new RestRequest(Method.POST);
                    request.AlwaysMultipartFormData = true;
                    request.AddParameter("Id", _userInfo.Id);
                    request.AddParameter("hovaten", _userInfo.fullName);
                    request.AddParameter("username", _userInfo.userName);
                    request.AddParameter("ho", "");
                    request.AddParameter("ten", "");
                    request.AddParameter("email", _userInfo.Email);
                    request.AddParameter("sdt", _userInfo.Phone);
                    request.AddParameter("diachi", _userInfo.Address);
                    IRestResponse response = client.Execute(request);
                    _userInfo.HasToken = true;
                    hasValue = _userInfoService.Update(Mapper.Map<M_UserInfo>(_userInfo));
                    hasValue = _vuViecRepo.Commit();
                }
                return null;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        [HttpPost]
        public ActionResult CreateCustomer(ClassInfoViewModel clssInfo)
        {
            var customerInfo = new CustomerViewModel()
            {
                Code = "STNHD",
                FullName = clssInfo.UserInfo.fullName,
                PhoneNo = clssInfo.UserInfo.Phone,
                Email = clssInfo.UserInfo.email,
                RegisterDate = DateTime.Now,
                LotNumber = "LO2103-013",
                UserOwner = GetUserInfo().userName,
                StatusId = 62,
                StatusCode = "100",
                StatusName = "Thẻ Chờ Giao"
            };
            hasValue = _customerService.Insert(Mapper.Map<M_Customer>(customerInfo));
            hasValue = _vuViecRepo.Commit();
            var status = string.IsNullOrEmpty(hasValue) ? "oke" : "error";
            return Json(new { s = status, result = status.Equals("oke") ? "Đã gửi thông tin đăng ký thành công" : hasValue }, JsonRequestBehavior.AllowGet);
        }
        public string AddStudent2Class(string lc, string userName)
        {
            try
            {
                var idLevels = new List<int>();
                if (lc.Equals("cap1"))
                {
                    idLevels.Add(4); idLevels.Add(5); idLevels.Add(6); idLevels.Add(7); idLevels.Add(8);
                }
                if (lc.Equals("cap2"))
                {
                    idLevels.Add(9); idLevels.Add(13); idLevels.Add(14); idLevels.Add(15);
                }
                if (lc.Equals("cap3"))
                {
                    idLevels.Add(16); idLevels.Add(17); idLevels.Add(18);
                }
                var _studentClasses = new List<StudentClassViewModel>();
                var _studentClass = new StudentClassViewModel();
                var classes = Mapper.Map<IEnumerable<ClassList>>(_classService.FindAll(ClassType.Class, "SHOW")).ToList();
                foreach (var item in classes)
                {
                    _studentClass = new StudentClassViewModel();
                    _studentClass.userName = userName;
                    _studentClass.ClassId = item.Id;
                    _studentClass.ClassName = item.Name;
                    _studentClass.IsActive = false;
                    _studentClass.FromDate = DateTime.Now;
                    _studentClass.ToDate = DateTime.Now;
                    if (idLevels.Contains(item.Id))
                        _studentClass.IsActive = true;
                    _studentClasses.Add(_studentClass);
                }
                hasValue = _studentClassService.Delete(userName);
                hasValue = _studentClassService.Insert(Mapper.Map<IEnumerable<M_StudentClass>>(_studentClasses));
                return string.Empty;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public string AddStudent2Class2(int classId, string userName, DateTime? dateExpired)
        {
            try
            {
                var idLevels = new List<int>();
                idLevels.Add(classId);
                //var _classRefers = _classReferService.FindAll(classId);
                //foreach (var item in _classRefers)
                //{
                //    idLevels.Add(item.ClassReferId);
                //}
                var _studentClasses = new List<StudentClassViewModel>();
                var _studentClass = new StudentClassViewModel();
                var classes = Mapper.Map<IEnumerable<ClassList>>(_classService.FindAll(ClassType.Class, "SHOW")).ToList();
                foreach (var item in classes)
                {
                    _studentClass = new StudentClassViewModel();
                    _studentClass.userName = userName;
                    _studentClass.ClassId = item.Id;
                    _studentClass.ClassName = item.Name;
                    _studentClass.IsActive = false;
                    _studentClass.FromDate = DateTime.Now;
                    _studentClass.ToDate = DateTime.Now;
                    if (idLevels.Contains(item.Id))
                    {
                        if (dateExpired != null)
                            _studentClass.ToDate = dateExpired ?? DateTime.Now;
                        _studentClass.IsActive = true;
                    }
                    _studentClasses.Add(_studentClass);
                }
                hasValue = _studentClassService.Delete(userName);
                hasValue = _studentClassService.Insert(Mapper.Map<IEnumerable<M_StudentClass>>(_studentClasses));
                return string.Empty;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
    }
}