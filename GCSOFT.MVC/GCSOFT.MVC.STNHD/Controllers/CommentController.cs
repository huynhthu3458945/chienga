﻿using AutoMapper;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.STNHDService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.STNHD.Helper;
using GCSOFT.MVC.STNHD.Models;
using GCSOFT.MVC.Web.Helper;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.STNHD.Controllers
{
    public class CommentController : BaseController
    {
        private readonly IShareInfoService _shareInfoService;
        private readonly ICommentService _commentService;
        private readonly IVuViecService _vuViecService;
        private readonly IQuestionService _questionService;
        private readonly IFileResourceService _fileResourceService;
        private readonly ISetupService _setupService;
        private readonly IVoteService _voteService;
        private string ImagePath = "~/Files/Comment/{0}/{1}";
        public CommentController
            (
                IShareInfoService shareInfoService
                , ICommentService commentService
                , IVuViecService vuViecService
                , IQuestionService questionService
                , IFileResourceService fileResourceService
                , ISetupService setupService
                , IVoteService voteService
            ) : base(shareInfoService)
        {
            _shareInfoService = shareInfoService;
            _commentService = commentService;
            _vuViecService = vuViecService;
            _questionService = questionService;
            _fileResourceService = fileResourceService;
            _setupService = setupService;
            _voteService = voteService;
        }
        /// <summary>
        /// Load danh sách Comments
        /// </summary>
        /// <param name="id">Id Refer</param>
        /// <param name="t">Refer Type</param>
        /// <returns></returns>
        public ActionResult Index(int id, int t)
        {
            var _userInfo = GetUserInfo();
            if (_userInfo == null)
                return RedirectToAction("Index", "Student", new { url = CurrentUrl() });
            if (!_userInfo.ActivePhone)
                return RedirectToAction("Index", "ClassInfo");

            var setup = _setupService.Get();
            var commentPage = new CommentPage();
            commentPage.Type = (ResultType)t;
            commentPage.ReferId = id;
            commentPage.Comments = Mapper.Map<IEnumerable<CommentViewModel>>(_commentService.FindAll((ResultType)t, id));
            foreach (var item in commentPage.Comments)
            {
                item.NoOfVote = _voteService.NoOfVote(ResultType.Commment, item.Id);
                item.hasVote = _voteService.HasVote(ResultType.Commment, item.Id, GetUserInfo().email);
                item.IconVote = item.hasVote ? "zmdi-favorite" : "zmdi-favorite-outline";
            }
            commentPage.FileResources = Mapper.Map<IEnumerable<VM_FileResource>>(_fileResourceService.FindAll(ResultType.Commment, id));
            foreach (var item in commentPage.FileResources)
            {
                item.Domain = item.Source == Source.BackEnd ? setup.DomainBackEnd : setup.DomainFontEnd;
                item.ImagePathFull = item.ImagePath.Replace("~", item.Domain);
            }
            if (commentPage.FileResources.Count() == 0)
            {
                var _fileResources = new List<VM_FileResource>();
                var _fileResource = new VM_FileResource();
                _fileResource.LineNo = 0;
                _fileResources.Add(_fileResource);
                commentPage.FileResources = _fileResources;
            }
            commentPage.NoOfComment = _commentService.NoOfComment((ResultType)t, id);
            return PartialView("_index", commentPage);
        }
        [HttpPost]
        public ActionResult Create(CommentViewModel _commentViewModel)
        {
            var userInfo = GetUserInfo();
            var commentInfo = _commentViewModel;
            commentInfo.Id = _commentService.GetID();
            commentInfo.DateCreate = DateTime.Now;
            commentInfo.DateModified = DateTime.Now;
            commentInfo.FullName = userInfo.fullName;
            commentInfo.UserId = userInfo.Id;
            commentInfo.UserName = userInfo.email;
            commentInfo.StatusId = 11;
            commentInfo.StatusCode = "CD";
            commentInfo.StatusName = "Chờ Duyệt";
            //-- Add Resource
            var _uploadHelper = new UploadHelper();
            var _fileResources = new List<VM_FileResource>();
            commentInfo.FileResources.ToList().ForEach(item => {
                item.LineNo = commentInfo.Id;
                item.ReferId = commentInfo.ReferId; //Id 1 Sample
                item.CommentId = commentInfo.Id;
                item.Type = ResultType.Commment;
                item.FileType = FileType.Base64;
                item.Source = Source.FrontEnd;
                _fileResources.Add(item);
            });
            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFileBase file = Request.Files[i];
                var namControl = Request.Files.AllKeys[i].ToString();
                _uploadHelper = new UploadHelper();
                _uploadHelper.PathSave = HttpContext.Server.MapPath(string.Format(ImagePath, commentInfo.Id, ""));
                _uploadHelper.UploadFile(file);
                string index = Regex.Match(namControl, @"\d+").Value;
                if (!string.IsNullOrEmpty(_fileResources[int.Parse(index)].ImageName) && !string.IsNullOrEmpty(_uploadHelper.FileSave))
                    _uploadHelper.DeleteFileOnServer(_uploadHelper.PathSave, _fileResources[int.Parse(index)].ImageName);
                if (!string.IsNullOrEmpty(_uploadHelper.FileSave))
                {
                    _fileResources[int.Parse(index)].ImageName = _uploadHelper.FileSave;
                    _fileResources[int.Parse(index)].ImagePath = string.Format(ImagePath, commentInfo.Id, _uploadHelper.FileSave);
                }
            }
            //-- Add Resource End
            
            var mComment = Mapper.Map<M_Comment>(commentInfo);
            mComment.FileResources = Mapper.Map<IEnumerable<M_FileResource>>(_fileResources.Where(d => d.ImageName != null));
            var hasValue = _commentService.Insert(mComment);
            hasValue = _fileResourceService.Insert(mComment.FileResources);
            hasValue = _vuViecService.Commit();
            return Json("oke", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult Suggestions(QuestionSuggestion _questionSuggestion)
        {
            var questionSuggestion = _questionSuggestion;
            var mQuestion = Mapper.Map<M_Question>(_questionService.GetByCourse(questionSuggestion.CourseId));
            mQuestion.Id = _questionService.GetID();
            mQuestion.Type = QuestionType.Suggestions;
            mQuestion.Question = questionSuggestion.Question;
            mQuestion.Answer = questionSuggestion.Answer;
            mQuestion.DateQuestion = DateTime.Now;
            mQuestion.DateAnswer = null;
            mQuestion.StatusId = 11;
            mQuestion.StatusCode = "CD";
            mQuestion.StatusName = "Chờ Duyệt";
            var userInfo = GetUserInfo();
            mQuestion.userName = userInfo.email;
            mQuestion.fullName = userInfo.fullName;
            //-- Add Resource
            var _uploadHelper = new UploadHelper();
            var _fileResources = new List<VM_FileResource>();
            questionSuggestion.FileResources.ToList().ForEach(item => {
                item.LineNo = 100;
                item.ReferId = mQuestion.Id;
                item.Type = ResultType.Commment;
                item.FileType = FileType.Base64;
                item.Source = Source.FrontEnd;
                _fileResources.Add(item);
            });
            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFileBase file = Request.Files[i];
                var namControl = Request.Files.AllKeys[i].ToString();
                _uploadHelper = new UploadHelper();
                _uploadHelper.PathSave = HttpContext.Server.MapPath(string.Format(ImagePath, mQuestion.Id, ""));
                _uploadHelper.UploadFile(file);
                string index = Regex.Match(namControl, @"\d+").Value;
                if (!string.IsNullOrEmpty(_fileResources[int.Parse(index)].ImageName) && !string.IsNullOrEmpty(_uploadHelper.FileSave))
                    _uploadHelper.DeleteFileOnServer(_uploadHelper.PathSave, _fileResources[int.Parse(index)].ImageName);
                if (!string.IsNullOrEmpty(_uploadHelper.FileSave))
                {
                    _fileResources[int.Parse(index)].ImageName = _uploadHelper.FileSave;
                    _fileResources[int.Parse(index)].ImagePath = string.Format(ImagePath, mQuestion.Id, _uploadHelper.FileSave);
                    //ImageHelper imgHelper = new ImageHelper() { 
                    //    pathImg = HttpContext.Server.MapPath(string.Format(ImagePath, mQuestion.Id, _uploadHelper.FileSave))
                    //};
                    //_fileResources[int.Parse(index)].ImageBase64 = imgHelper.ImageToBase64();
                }
            }
            //-- Add Resource End
            mQuestion.FileResources = Mapper.Map<IEnumerable<M_FileResource>>(_fileResources.Where(d => d.ImageName != null));
            string hasValue = _questionService.Insert(mQuestion);
            hasValue = _fileResourceService.Insert(mQuestion.FileResources);
            hasValue = _vuViecService.Commit();
            return Json("", JsonRequestBehavior.AllowGet);
        }
    }
}