﻿using GCSOFT.MVC.Service.STNHDService.Interface;
using GCSOFT.MVC.STNHD.Models;
using GCSOFT.MVC.STNHD.Models.PageViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.STNHD.Controllers
{
    public class ContactController : BaseController
    {
        public ContactController
            (
                IShareInfoService shareInfoService
            ) : base(shareInfoService)
        {

        }
        // GET: Contact
        public ActionResult Index()
        {
            //ViewBag.ShareInfo = GetShareInfo();
            var contactInfo = new ContentPageViewModel();
            contactInfo.UserInfo = GetUserInfo() ?? new LoginResult();
            return View(contactInfo);
        }
    }
}