﻿using AutoMapper;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.STNHDService.Interface;
using GCSOFT.MVC.Service.STNHDService.Service;
using GCSOFT.MVC.STNHD.Models;
using GCSOFT.MVC.STNHD.Models.PageViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.STNHD.Controllers
{
    public class CourseController : BaseController
    {
        private readonly IShareInfoService _shareInfoService;
        private readonly ICoursePageService _courseService;
        private readonly IQuestionPageService _questionPageService;
        private readonly IVoteService _voteService;
        private readonly ICommentService _commentService;
        private readonly IFileResourceService _fileResourceService;
        private readonly IClassService _classService;
        private readonly ISetupService _setupService;
        public CourseController
            (
                IShareInfoService shareInfoService
                , ICoursePageService courseService
                , IQuestionPageService questionPageService
                , IVoteService voteService
                , ICommentService commentService
                , IFileResourceService fileResourceService
                , IClassService classService
                , ISetupService setupService
            ) : base(shareInfoService)
        {
            _shareInfoService = shareInfoService;
            _courseService = courseService;
            _questionPageService = questionPageService;
            _voteService = voteService;
            _commentService = commentService;
            _fileResourceService = fileResourceService;
            _classService = classService;
            _setupService = setupService;
        }
        public ActionResult Index(int id)
        {
            var _userInfo = GetUserInfo();
            if (_userInfo == null)
                return RedirectToAction("Index", "Student", new { url = CurrentUrl() });
            if (!_userInfo.ActivePhone)
                return RedirectToAction("Index", "ClassInfo");
            //ViewBag.ShareInfo = GetShareInfo();
            var userInfo = GetUserInfo();
            var courseVM = new CoursePageViewModel();
            courseVM.UserInfo = userInfo;
            courseVM.CourseInfo = Mapper.Map<CourseViewModel>(_courseService.GetCourse(id));
            courseVM.CourseContents = Mapper.Map<IEnumerable<CourseContentList>>(_courseService.GetCourseContentList(id, CourseType.Mindmap));
            courseVM.Courses = Mapper.Map<IEnumerable<CourseList>>(_shareInfoService.CourseList(false, courseVM.CourseInfo.ClassId));
            return View(courseVM);
        }

        public ActionResult Answer(int id, int l)
        {
            var _userInfo = GetUserInfo();
            if (_userInfo == null)
                return RedirectToAction("Index", "Student", new { url = CurrentUrl() });
            if (!_userInfo.ActivePhone)
                return RedirectToAction("Index", "ClassInfo");

            //ViewBag.ShareInfo = GetShareInfo();
            var courseContentVM = new CourseContentPageViewModel();
            courseContentVM.CourseInfo = Mapper.Map<CourseViewModel>(_courseService.GetCourse(id));
            courseContentVM.CourseContent = Mapper.Map<CourseContentCard>(_courseService.GetCourseContent(id, l));
            return View(courseContentVM);
        }
        /// <summary>
        /// Get list question ( Sample Refer ) by id course
        /// </summary>
        /// <param name="id">Id Course</param>
        /// <param name="l">Id Course Line No.</param>
        /// <returns></returns>
        public ActionResult SampleRefers(int id, int l)
        {
            var _userInfo = GetUserInfo();
            if (_userInfo == null)
                return RedirectToAction("Index", "Student", new { url = CurrentUrl() });
            if (!_userInfo.ActivePhone)
                return RedirectToAction("Index", "ClassInfo");
            //ViewBag.ShareInfo = GetShareInfo();

            var questionTypes = new List<QuestionType>();
            questionTypes.Add(QuestionType.Sample);

            var resultTypes = new List<ResultType>();
            resultTypes.Add(ResultType.Sample);

            var statusCode = new List<string>();
            statusCode.Add("DX");

            var sampleRefers = new SampleRefersViewModel();
            sampleRefers.UserInfo = GetUserInfo();
            sampleRefers.CourseContent = Mapper.Map<CourseContentList>(_courseService.GetCourseContent(id, l));
            sampleRefers.CourseInfo = Mapper.Map<CourseViewModel>(_courseService.GetCourse(id));

            sampleRefers.QuestionsInfos = Mapper.Map<IEnumerable<QuestionViewModel>>(_questionPageService.GetQuestionByCourseContent(id, l, questionTypes, statusCode));

            var setup = _setupService.Get();
            var idQuestions = sampleRefers.QuestionsInfos.Select(d => d.Id).ToList();
            sampleRefers.files = Mapper.Map<IEnumerable<VM_FileResource>>(_fileResourceService.FindAll(resultTypes, idQuestions));
            foreach (var item in sampleRefers.files)
            {
                item.Domain = item.Source == Source.BackEnd ? setup.DomainBackEnd : setup.DomainFontEnd;
                item.ImagePathFull = item.ImagePath.Replace("~", item.Domain);
            }
            sampleRefers.CourseContents = Mapper.Map<IEnumerable<CourseContentList>>(_courseService.GetCourseContentList(id, CourseType.Mindmap));

            var commentPages = new List<CommentPage>();
            var commentPage = new CommentPage();
            foreach (var item in sampleRefers.QuestionsInfos)
            {
                commentPage = new CommentPage();
                commentPage.Type = ResultType.Sample;
                commentPage.ReferId = item.Id;
                commentPage.Comments = Mapper.Map<IEnumerable<CommentViewModel>>(_commentService.FindAll(ResultType.Sample, item.Id));
                foreach (var comment in commentPage.Comments)
                {
                    comment.NoOfVote = _voteService.NoOfVote(ResultType.Commment, comment.Id);
                    comment.hasVote = _voteService.HasVote(ResultType.Commment, comment.Id, GetUserInfo().email);
                    comment.IconVote = comment.hasVote ? "fa-heart" : "fa-heart-o";
                }
                commentPage.FileResources = Mapper.Map<IEnumerable<VM_FileResource>>(_fileResourceService.FindAll(ResultType.Commment, item.Id));
                foreach (var fileresource in commentPage.FileResources)
                {
                    fileresource.Domain = fileresource.Source == Source.BackEnd ? setup.DomainBackEnd : setup.DomainFontEnd;
                    fileresource.ImagePathFull = fileresource.ImagePath.Replace("~", fileresource.Domain);
                }
                if (commentPage.FileResources.Count() == 0)
                {
                    var _fileResources = new List<VM_FileResource>();
                    var _fileResource = new VM_FileResource();
                    _fileResource.LineNo = 0;
                    _fileResources.Add(_fileResource);
                    commentPage.FileResources = _fileResources;
                }
                commentPage.NoOfComment = _commentService.NoOfComment(ResultType.Sample, item.Id);
                commentPages.Add(commentPage);
            }
            sampleRefers.CommentPages = commentPages;

            return View(sampleRefers);
        }
        public ActionResult Comment(int id, int t)
        {
            var _userInfo = GetUserInfo();
            if (_userInfo == null)
                return RedirectToAction("Index", "Student", new { url = CurrentUrl() });
            if (!_userInfo.ActivePhone)
                return RedirectToAction("Index", "ClassInfo");

            var setup = _setupService.Get();
            var commentPage = new CommentPage();
            commentPage.Type = (ResultType)t;
            commentPage.ReferId = id;
            commentPage.Comments = Mapper.Map<IEnumerable<CommentViewModel>>(_commentService.FindAll((ResultType)1, id));
            foreach (var item in commentPage.Comments)
            {
                item.NoOfVote = _voteService.NoOfVote(ResultType.Commment, item.Id);
                item.hasVote = _voteService.HasVote(ResultType.Commment, item.Id, GetUserInfo().email);
                item.IconVote = item.hasVote ? "fa-heart" : "fa-heart-o";
            }
            commentPage.FileResources = Mapper.Map<IEnumerable<VM_FileResource>>(_fileResourceService.FindAll(ResultType.Commment, id));
            foreach (var item in commentPage.FileResources)
            {
                item.Domain = item.Source == Source.BackEnd ? setup.DomainBackEnd : setup.DomainFontEnd;
                item.ImagePathFull = item.ImagePath.Replace("~", item.Domain);
            }
            if (commentPage.FileResources.Count() == 0)
            {
                var _fileResources = new List<VM_FileResource>();
                var _fileResource = new VM_FileResource();
                _fileResource.LineNo = 0;
                _fileResources.Add(_fileResource);
                commentPage.FileResources = _fileResources;
            }
            commentPage.NoOfComment = _commentService.NoOfComment((ResultType)t, id);
            return PartialView("_CommentBox", commentPage);
        }
        public ActionResult subjects(int id)
        {
            var _userInfo = GetUserInfo();
            if (_userInfo == null)
                return RedirectToAction("Index", "Student", new { url = CurrentUrl() });
            if (!_userInfo.ActivePhone)
                return RedirectToAction("Index", "ClassInfo");
            //ViewBag.ShareInfo = GetShareInfo();
            var subjectsInfo = new SubjectsPageViewModel();
            subjectsInfo.ClassInfo = Mapper.Map<ClassList>(_classService.GetBy(id));
            subjectsInfo.Courses = Mapper.Map<IEnumerable<CourseList>>(_shareInfoService.CourseList(false, id));
            var idCourses = subjectsInfo.Courses.Select(d => d.Id).ToList();
            subjectsInfo.CourseContents = Mapper.Map<IEnumerable<CourseContentList>>(_courseService.GetCourseContentList(idCourses));
            return View(subjectsInfo);
        }
        /// <summary>
        /// Load vote and comment
        /// </summary>
        /// <param name="id"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        public JsonResult LoadVoteComment(int id, int t)
        {
            var _voteCommentVM = new VoteCommentViewModel();
            _voteCommentVM.Id = id;
            _voteCommentVM.NoOfVote = _voteService.NoOfVote((ResultType)t, id);
            _voteCommentVM.NoOfComment = _commentService.NoOfComment((ResultType)t, id);
            _voteCommentVM.HasVote = _voteService.HasVote((ResultType)t, id, GetUserInfo().email);
            if (_voteCommentVM.HasVote)
                _voteCommentVM.IconVote = "fa fa-heart";
            return Json(_voteCommentVM, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Load view Suggestions
        /// </summary>
        /// <param name="id">Id Course</param>
        /// <returns></returns>
        public ActionResult Suggestions(int id)
        {
            var suggestion = new QuestionSuggestion();
            suggestion.CourseId = id;
            suggestion.IsReload = true;
            return PartialView("_Suggestions", suggestion);
        }
    }
}