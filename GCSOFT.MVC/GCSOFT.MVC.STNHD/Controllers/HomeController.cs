﻿using AutoMapper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.STNHDService.Interface;
using GCSOFT.MVC.Service.STNHDService.Service;
using GCSOFT.MVC.STNHD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.STNHD.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IHomeService _homeService;
        private readonly IShareInfoService _shareInfoService;
        private readonly ISupporterService _supportService;
        private readonly IFileResourceService _fileResourceService;
        private readonly ISetupService _setupService;
        private readonly IStudentClassService _studentClassService;

        public HomeController
            (
                IShareInfoService shareInfoService
                , IHomeService homeService
                , ISupporterService supportService
                , IFileResourceService fileResourceService
                , ISetupService setupService
                , IStudentClassService studentClassService
            ) : base(shareInfoService)
        {
            _shareInfoService = shareInfoService;
            _homeService = homeService;
            _supportService = supportService;
            _fileResourceService = fileResourceService;
            _setupService = setupService;
            _studentClassService = studentClassService;
        }
        public ActionResult Index()
        {
            var language = Getlanguage();
            var userInfo = GetUserInfo();
            //ViewBag.ShareInfo = GetShareInfo();
            var setup = _setupService.Get();
            var homeInfo = new HomeViewModel();
            homeInfo.Supporters = Mapper.Map<IEnumerable<SupporterViewModel>>(_supportService.FindAll());
            foreach (var item in homeInfo.Supporters)
            {
                if (language.Equals("en-US") && !string.IsNullOrEmpty(item.NameEN))
                    item.Name = item.NameEN;
            }
            var idSupporters = homeInfo.Supporters.Select(d => d.Id).ToList();
            homeInfo.FileResources = Mapper.Map<IEnumerable<VM_FileResource>>(_fileResourceService.FindAll(ResultType.Supporter, idSupporters));
            foreach (var item in homeInfo.FileResources)
            {
                item.Domain = item.Source == Source.BackEnd ? setup.DomainBackEnd : setup.DomainFontEnd;
                item.ImagePathFull = item.ImagePath.Replace("~", item.Domain);
            }
            return View(homeInfo);
        }
        public ActionResult samples()
        {
            var language = Getlanguage();
            var userInfo = GetUserInfo();
            if (userInfo == null)
                return RedirectToAction("Index", "Student", new { url = CurrentUrl() });
            if (!userInfo.ActivePhone)
                return RedirectToAction("Index", "ClassInfo");
            //ViewBag.ShareInfo = GetShareInfo();
            var setup = _setupService.Get();
            var avatarInfo = new VM_FileResource();
            var homeInfo = new HomeViewModel();
            var idClass = userInfo.ClassId;
            if (!_studentClassService.HasClass(userInfo.userName))
            {
                var _studentClassInit = new List<StudentClassViewModel>();
                var _studentClass = new StudentClassViewModel();
                var classList = Mapper.Map<IEnumerable<ClassList>>(_shareInfoService.ClassList(ClassType.Class, "SHOW"));
                foreach (var item in classList)
                {
                    _studentClass = new StudentClassViewModel();
                    _studentClass.ClassId = item.Id;
                    _studentClass.ClassName = item.Name;
                    _studentClass.IsActive = false;
                    if (_studentClass.ClassId == idClass)
                        _studentClass.IsActive = true;
                    if (language.Equals("en-US") && !string.IsNullOrEmpty(item.NameEN))
                        _studentClass.ClassName = item.NameEN;
                    _studentClassInit.Add(_studentClass);
                }
                homeInfo.StudentClasses = _studentClassInit;
            }
            else
            {
                homeInfo.StudentClasses = Mapper.Map<IEnumerable<StudentClassViewModel>>(_studentClassService.FindAllByActive(userInfo.userName));
                homeInfo.StudentClasses.ToList().ForEach(item => {
                    if (language.Equals("en-US"))
                    {
                        var classNameEN = _shareInfoService.GetClassBy(item.ClassId).NameEN;
                        if (!string.IsNullOrEmpty(classNameEN))
                            item.ClassName = classNameEN;
                    }
                    item.IsActive = false;
                    if (item.ClassId == idClass)
                        item.IsActive = true;
                });
            }

            homeInfo.Courses = Mapper.Map<IEnumerable<CourseList>>(_shareInfoService.CourseList(false, idClass));
            foreach (var item in homeInfo.Courses)
            {
                if (item.AvartarLineNo == 0)
                    avatarInfo = Mapper.Map<VM_FileResource>(_fileResourceService.GetBy(ResultType.AvatarCourse, 1000, 600));
                else
                    avatarInfo = Mapper.Map<VM_FileResource>(_fileResourceService.GetBy(ResultType.AvatarCourse, 1000, item.AvartarLineNo));
                item.Avatar = avatarInfo.ImagePath.Replace("~", setup.DomainFontEnd);
                if (language.Equals("en-US") && !string.IsNullOrEmpty(item.TitleEN))
                    item.Title = item.TitleEN;
            }
            homeInfo.UserInfo = userInfo;
            return View(homeInfo);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public ActionResult LoadCourse(int c) 
        {
            var language = Getlanguage();
            var setup = _setupService.Get();
            var avatarInfo = new VM_FileResource();
            var courses = Mapper.Map<IEnumerable<CourseList>>(_shareInfoService.CourseList(false, c));
            foreach (var item in courses)
            {
                if (item.AvartarLineNo == 0)
                    avatarInfo = Mapper.Map<VM_FileResource>(_fileResourceService.GetBy(ResultType.AvatarCourse, 1000, 600));
                else
                    avatarInfo = Mapper.Map<VM_FileResource>(_fileResourceService.GetBy(ResultType.AvatarCourse, 1000, item.AvartarLineNo));
                item.Avatar = avatarInfo.ImagePath.Replace("~", setup.DomainFontEnd);
                if (language.Equals("en-US") && !string.IsNullOrEmpty(item.TitleEN))
                    item.Title = item.TitleEN;
            }
            return PartialView("_Course", courses);
        }
    }
}