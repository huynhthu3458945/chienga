﻿using AutoMapper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.STNHDService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.STNHD.Helper;
using GCSOFT.MVC.STNHD.Models;
using Microsoft.SqlServer.Server;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace GCSOFT.MVC.STNHD.Controllers
{
    public class LoginController : BaseController
    {
        private readonly IShareInfoService _shareInfoService;
        private readonly IUserInfoService _userInfoService;
        private readonly IVuViecService _vuViecRepo;
        private readonly ISetupService _setupService;
        private readonly ILoginEntryService _loginEntryService;
        private M_LoginEntry loginEntry;
        private string hasValue;
        public LoginController
            (
                IShareInfoService shareInfoService
                , IUserInfoService userInfoService
                , IVuViecService vuViecRepo
                , ISetupService setupService
                , ILoginEntryService loginEntryService
            ) : base(shareInfoService)
        {
            _shareInfoService = shareInfoService;
            _userInfoService = userInfoService;
            _vuViecRepo = vuViecRepo;
            _setupService = setupService;
            _loginEntryService = loginEntryService;
        }
        //[AllowAnonymous]
        public ActionResult Index()
        {
            InitLanguage();
            //CookieHelper.RemoveCookie("UserInfo", "LoginResult");
            if (System.Web.HttpContext.Current.Request.Cookies["UserSTNHD"] != null)
            {
                System.Web.HttpCookie cookie = this.ControllerContext.HttpContext.Request.Cookies["UserSTNHD"];
                cookie.Expires = DateTime.Now.AddDays(-1);
                this.ControllerContext.HttpContext.Response.Cookies.Add(cookie);
            }
            return View();
        }
        /// <summary>
        /// Login user
        /// </summary>
        /// <param name="u">Userinfo</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Index(LoginResult u)
        {
            try
            {
                //InitLanguage();
                var userInfo = new LoginResult();
                if (u.userName.Contains("@"))
                {
                    userInfo = Mapper.Map<LoginResult>(_userInfoService.CheckByEmail(u.userName, u.password));
                }
                else
                {
                    userInfo = Mapper.Map<LoginResult>(_userInfoService.CheckByUsername(u.userName, u.password));
                }
                if (string.IsNullOrEmpty(userInfo.userName))
                    return Json(new { s = "error", url = "Thông tin đăng nhập chưa đúng<BR /> Vui lòng kiểm tra lại" }, JsonRequestBehavior.AllowGet);
                
                //FormsAuthentication.SetAuthCookie(userInfo.userName, true);
                userInfo.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                //CookieHelper.StoreInCookie("UserInfo", "", "LoginResult", Server.UrlEncode(JsonConvert.SerializeObject(userInfo)), null);

                //-- add to Cookie User
                System.Web.HttpCookie userCookie = new System.Web.HttpCookie("UserSTNHD", Server.UrlEncode(JsonConvert.SerializeObject(userInfo)));
                userCookie.Expires = DateTime.Now.AddYears(8);
                System.Web.HttpContext.Current.Response.Cookies.Add(userCookie);
                //-- add to Cookie User + 

                //-- Update Userinfo
                //SetDataLoginEntry(userInfo);
                //hasValue = _loginEntryService.Insert(loginEntry);
                //hasValue = _vuViecRepo.Commit();
                //-- Update Userinfo +
                if (!userInfo.ActivePhone)
                    return Json(new { s = "Oke", url = Url.Action("Index", "ClassInfo") }, JsonRequestBehavior.AllowGet);
                if (string.IsNullOrEmpty(u.returnUrl))
                    u.returnUrl = Url.Action("Index", "Home");
                //-- Post Userinfo to PVD
                //if (!userInfo.HasToken)
                //    UploadUsertoCloud(u.userName);
                if (!userInfo.HasVideoToken)
                    UploadUsertoCloudVideos(u.userName);
                //-- Post Userinfo to PVD +
                return Json(new { s = "Oke", url = u.returnUrl }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { s = "error", url = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Logout()
        {
            //FormsAuthentication.SignOut();
            //CookieHelper.RemoveCookie("UserInfo", "LoginResult");
            if (System.Web.HttpContext.Current.Request.Cookies["UserSTNHD"] != null)
            {
                System.Web.HttpCookie cookie = this.ControllerContext.HttpContext.Request.Cookies["UserSTNHD"];
                cookie.Expires = DateTime.Now.AddDays(-1);
                this.ControllerContext.HttpContext.Response.Cookies.Add(cookie);
            }
            return RedirectToAction("Index", "Home");
        }
        private void SetDataLoginEntry(LoginResult loginResult)
        {
            var loginEntryVM = new LoginEntryViewModel();
            //var ipClient = GetIPAddress();
            //-- Get ip info
            //if (!string.IsNullOrEmpty(ipClient))
            //{
            //    var client = new RestClient(string.Format("http://api.ipstack.com/{0}?access_key=c2547274e399416b3c9e3f1afdf37fe9", ipClient));
            //    client.Timeout = -1;
            //    var request = new RestRequest(Method.GET);
            //    IRestResponse response = client.Execute(request);
            //    loginEntryVM = JsonConvert.DeserializeObject<LoginEntryViewModel>(response.Content);
            //}

            //-- Get ip info +
            loginEntryVM.userName = loginResult.userName;
            loginEntryVM.SessionId = loginResult.SessionId;
            loginEntryVM.LoggedIn = true;
            loginEntryVM.DateLogin = DateTime.Now;
            
            loginEntry = Mapper.Map<M_LoginEntry>(loginEntryVM);
        }
        private string GetIPAddress()
        {
            string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(ip))
            {
                ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
            return ip;
        }
        protected string UploadUsertoCloud(string userName)
        {
            try
            {
                var _userInfo = Mapper.Map<UserInfoViewModel>(_userInfoService.GetBy(userName));
                if (!_userInfo.HasToken)
                {
                    var client = new RestClient("https://cdn.stnhd.com/video/push-userinfo.php");
                    client.Timeout = -1;
                    var request = new RestRequest(Method.POST);
                    request.AlwaysMultipartFormData = true;
                    request.AddParameter("id", _userInfo.Id);
                    request.AddParameter("hovaten", _userInfo.fullName);
                    request.AddParameter("username", _userInfo.userName);
                    request.AddParameter("ho", "");
                    request.AddParameter("ten", "");
                    request.AddParameter("email", _userInfo.Email);
                    request.AddParameter("sdt", _userInfo.Phone);
                    request.AddParameter("diachi", _userInfo.Address);
                    IRestResponse response = client.Execute(request);
                    _userInfo.HasToken = true;
                    hasValue = _userInfoService.Update(Mapper.Map<M_UserInfo>(_userInfo));
                    hasValue = _vuViecRepo.Commit();
                }
                return null;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        protected string UploadUsertoCloudVideos(string userName)
        {
            try
            {
                var _userInfo = Mapper.Map<UserInfoViewModel>(_userInfoService.GetBy(userName));
                if (!_userInfo.HasVideoToken)
                {
                    var client = new RestClient("https://th.sieutrinhohocduong.com/video/push-userinfo.php");
                    client.Timeout = -1;
                    var request = new RestRequest(Method.POST);
                    request.AlwaysMultipartFormData = true;
                    request.AddParameter("id", _userInfo.Id);
                    request.AddParameter("hovaten", _userInfo.fullName);
                    request.AddParameter("username", _userInfo.userName);
                    request.AddParameter("ho", "");
                    request.AddParameter("ten", "");
                    request.AddParameter("email", _userInfo.Email);
                    request.AddParameter("sdt", _userInfo.Phone);
                    request.AddParameter("diachi", _userInfo.Address);
                    IRestResponse response = client.Execute(request);

                    client = new RestClient("https://thcs.sieutrinhohocduong.com/video/push-userinfo.php");
                    client.Timeout = -1;
                    response = client.Execute(request);

                    client = new RestClient("https://thpt.sieutrinhohocduong.com/video/push-userinfo.php");
                    client.Timeout = -1;
                    response = client.Execute(request);

                    _userInfo.HasVideoToken = true;
                    hasValue = _userInfoService.Update(Mapper.Map<M_UserInfo>(_userInfo));
                    hasValue = _vuViecRepo.Commit();
                }
                return null;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
    }
}