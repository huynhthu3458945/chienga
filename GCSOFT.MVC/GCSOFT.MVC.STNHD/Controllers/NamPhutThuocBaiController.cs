﻿using AutoMapper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.NamPhutThuocBai;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.NamPhutThuocBaiService.Interface;
using GCSOFT.MVC.Service.STNHDService.Interface;
using GCSOFT.MVC.Service.StoreProcedure.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Service.Transaction.Interface;
using GCSOFT.MVC.STNHD.Helper;
using GCSOFT.MVC.STNHD.Models;
using GCSOFT.MVC.STNHD.Models.NamPhutViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.STNHD.Controllers
{
    public class NamPhutThuocBaiController : BaseController
    {
        private readonly IShareInfoService _shareInfoService;
        private readonly IClassService _classService;
        private readonly IProvinceService _provinceService;
        private readonly IDistrictService _districtService;
        private readonly IUserInfoService _userInfoService;
        private readonly ISMSService _smsService;
        private readonly IVuViecService _vuViecRepo;
        private readonly IContestantsService _contestantService;
        private readonly IStoreProcedureService _storeProService;
        private readonly IDeThiService _deThiService;
        private readonly INopBaiService _nopBaiService;
        private readonly IGiftService _giftService;
        private readonly IPointEntryService _pointEntryService;
        private readonly ITransactionGiftService _transGiftService;
        private string hasValue;
        public NamPhutThuocBaiController
            (
                IShareInfoService shareInfoService
                , IClassService classService
                , IProvinceService provinceService
                , IDistrictService districtService
                , IUserInfoService userInfoService
                , ISMSService smsService
                , IVuViecService vuViecRepo
                , IContestantsService contestantService
                , IStoreProcedureService storeProService
                , IDeThiService deThiService
                , INopBaiService nopBaiService
                , IGiftService giftService
                , IPointEntryService pointEntryService
                , ITransactionGiftService transGiftService
            ) : base(shareInfoService)
        {
            _shareInfoService = shareInfoService;
            _classService = classService;
            _provinceService = provinceService;
            _districtService = districtService;
            _userInfoService = userInfoService;
            _smsService = smsService;
            _vuViecRepo = vuViecRepo;
            _contestantService = contestantService;
            _storeProService = storeProService;
            _deThiService = deThiService;
            _nopBaiService = nopBaiService;
            _giftService = giftService;
            _pointEntryService = pointEntryService;
            _transGiftService = transGiftService;
        }
        // GET: NamPhuThuocBai
        public ActionResult Index()
        {
            InitLanguage();
            var userName = "";
            if (System.Web.HttpContext.Current.Request.Cookies["userNamPhut"] != null)
                userName = System.Web.HttpContext.Current.Request.Cookies["userNamPhut"].Value;
            var thiSinhInfo = new Contestants();
            var tongDiem = 0;
            if (!string.IsNullOrEmpty(userName))
            {
                thiSinhInfo = Mapper.Map<Contestants>(_contestantService.GetByUserName(userName)) ?? new Contestants();
                tongDiem = _pointEntryService.TotalPoint(userName); ;
            }
            ViewBag.BaiThi = _storeProService.SP_BaiThi(userName);
            ViewBag.userName = userName;
            ViewBag.ThiSinh = thiSinhInfo;
            ViewBag.tongDiem = tongDiem;
            return View();
        }
        public ActionResult BaiDuThi()
        {
            InitLanguage();
            if (System.Web.HttpContext.Current.Request.Cookies["userNamPhut"] == null)
                return RedirectToAction("DangNhapCuocThi");
            ViewBag.BaiThi = _storeProService.SP_BaiThi(GetUserName());
            return View();
        }
        [HttpGet]
        public ActionResult NopBai(string c)
        {
            InitLanguage();
            if (System.Web.HttpContext.Current.Request.Cookies["userNamPhut"] == null)
                return RedirectToAction("DangNhapCuocThi");
            var deThi = Mapper.Map<DeThi>(_deThiService.GetBy(c));
            ViewBag.DeThi = deThi;
            var userInfo = Mapper.Map<Contestants>(_contestantService.GetByUserName(GetUserName()));
            var nopBai = new NopBai()
            {
                userInfo = userInfo,
                Id = 0,
                CodeDethi = deThi.Code,
                IdContestants = userInfo.Id,
                userName = userInfo.Username,
                SBD = userInfo.IdentificationNumber,
                FullName = userInfo.ChildName,
                ClassId = userInfo.GradeId,
                ClassName = userInfo.GradeName
            };
            return View(nopBai);
        }
        [HttpPost]
        public ActionResult NopBai(NopBai nopBai)
        {
            InitLanguage();
            var schoolClass = Mapper.Map<ClassList>(_classService.Get(nopBai.ClassId));
            nopBai.ClassName = schoolClass.Name;
            nopBai.DateInput = DateTime.Now;
            hasValue = _nopBaiService.Insert(Mapper.Map<TB_NopBai>(nopBai));
            hasValue = _vuViecRepo.Commit();
            var url = Url.Action("BaiDuThi", new { msg = 2 });
            return Json(new { url = url }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult CapNhatBaiNop(string c, int id)
        {
            InitLanguage();
            if (System.Web.HttpContext.Current.Request.Cookies["userNamPhut"] == null)
                return RedirectToAction("DangNhapCuocThi");
            var deThi = Mapper.Map<DeThi>(_deThiService.GetBy(c));
            ViewBag.DeThi = deThi;
            var userInfo = Mapper.Map<Contestants>(_contestantService.GetByUserName(GetUserName()));
            var nopBai = Mapper.Map<NopBai>(_nopBaiService.Get(id));
            nopBai.userInfo = userInfo;
            return View(nopBai);
        }
        [HttpGet]
        public ActionResult CapNhatLuotView(string c, int id)
        {
            InitLanguage();
            if (System.Web.HttpContext.Current.Request.Cookies["userNamPhut"] == null)
                return RedirectToAction("DangNhapCuocThi");
            var deThi = Mapper.Map<DeThi>(_deThiService.GetBy(c));
            ViewBag.DeThi = deThi;
            var userInfo = Mapper.Map<Contestants>(_contestantService.GetByUserName(GetUserName()));
            var nopBai = Mapper.Map<NopBai>(_nopBaiService.Get(id));
            nopBai.userInfo = userInfo;
            return View(nopBai);
        }
        [HttpPost]
        public ActionResult CapNhatLuotView(NopBai nopBai)
        {
            InitLanguage();
            var _nopBai = _nopBaiService.Get(nopBai.Id);
            _nopBai.LuotView = nopBai.LuotView ?? 0;
            _nopBai.LuotInteract = nopBai.LuotInteract ?? 0;
            _nopBai.LuotShare = nopBai.LuotShare ?? 0;
            _nopBai.LuotComment = nopBai.LuotComment ?? 0;
            hasValue = _nopBaiService.Update(_nopBai);
            hasValue = _vuViecRepo.Commit();
            var url = Url.Action("BaiDuThi", new { msg = 2 });
            return Json(new { url = url }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult CapNhatBaiNop(NopBai nopBai)
        {
            InitLanguage();
            var _nopBai = _nopBaiService.Get(nopBai.Id);
            _nopBai.LinkFacebok = nopBai.LinkFacebok;
            _nopBai.LuotView = nopBai.LuotView ?? 0;
            _nopBai.LuotInteract = nopBai.LuotInteract ?? 0;
            _nopBai.LuotShare = nopBai.LuotShare ?? 0;
            _nopBai.LuotComment = nopBai.LuotComment ?? 0;
            hasValue = _nopBaiService.Update(_nopBai);
            hasValue = _vuViecRepo.Commit();
            var url = Url.Action("BaiDuThi", new { msg = 2 });
            return Json(new { url = url }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DangKy(string id)
        {
            InitLanguage();
            var contestants = new Contestants()
            {
                ClassList = Mapper.Map<IEnumerable<ClassList>>(_classService.FindAll(ClassType.Class, "SHOW")).ToList().ToSelectListItems(0),
                CityList = Mapper.Map<IEnumerable<ProvinceViewModel>>(_provinceService.FindAll()).ToSelectListItems(0),
                DistrictList = Mapper.Map<IEnumerable<DistrictViewModel>>(_districtService.FindAll(0)).ToSelectListItems(0)
            };
            if (!string.IsNullOrEmpty(id))
            {
                var userInfo = Mapper.Map<UserInfoViewModel>(_userInfoService.GetBy(int.Parse(id)));
                contestants.IsAccountSTNHD = true;
                contestants.Username = userInfo.userName;
                contestants.Password = userInfo.Password;
                contestants.ChildName = userInfo.fullName;
                contestants.Email = userInfo.Email;
                contestants.Phone = userInfo.Phone;
                contestants.Address = userInfo.Address;
            }
            return View(contestants);
        }
        public ActionResult DangNhapCuocThi()
        {
            InitLanguage();
            if (System.Web.HttpContext.Current.Request.Cookies["userNamPhut"] != null)
                System.Web.HttpContext.Current.Request.Cookies["userNamPhut"].Expires = DateTime.Now.AddDays(-1);
            return View();
        }
        public ActionResult QuenTenDangNhapVaMatKhau()
        {
            InitLanguage();
            
            return View(new ForgetUserAndPass());
        }
        public JsonResult CheckPhoneAndOTP(string phone, string otp)
        {
            var otpStatus = _smsService.CheckOTP(phone, otp);
            var msg = "";
            if (otpStatus.Equals("Nothing"))
                msg = "Mã OTP không tồn tại";
            if (otpStatus.Equals("expired"))
                msg = "Mã OTP đã hết hạn";
            if (!string.IsNullOrEmpty(msg))
                return Json(new { s = "error", result = msg }, JsonRequestBehavior.AllowGet);
            return Json(new { s = "", result = "" }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UserList(ForgetUserAndPass forgetuser)
        {
            var userInfos = Mapper.Map<IEnumerable<Contestants>>(_contestantService.FindAll(forgetuser.phonenumber));
            return View("UserList", userInfos);
        }
        public ActionResult ThongTinThiSinh()
        {
            InitLanguage();
            if (System.Web.HttpContext.Current.Request.Cookies["userNamPhut"] == null)
                return RedirectToAction("DangNhapCuocThi");
            var thiSinhInfo = Mapper.Map<Contestants>(_contestantService.GetByUserName(GetUserName()));
            thiSinhInfo.ClassList = Mapper.Map<IEnumerable<ClassList>>(_classService.FindAll(ClassType.Class, "SHOW")).ToList().ToSelectListItems(thiSinhInfo.GradeId);
            thiSinhInfo.CityList = Mapper.Map<IEnumerable<ProvinceViewModel>>(_provinceService.FindAll()).ToSelectListItems(thiSinhInfo.CityId);
            thiSinhInfo.DistrictList = Mapper.Map<IEnumerable<DistrictViewModel>>(_districtService.FindAll(thiSinhInfo.CityId)).ToSelectListItems(thiSinhInfo.DistrictId);
            return View(thiSinhInfo);
        }
        [HttpPost]
        public ActionResult ThongTinThiSinh(Contestants contestants)
        {
            var otpStatus = _smsService.CheckOTP(contestants.Phone, contestants.OTP);
            var msg = "";
            if (otpStatus.Equals("Nothing"))
                msg = "Mã OTP không tồn tại";
            if (otpStatus.Equals("expired"))
                msg = "Mã OTP đã hết hạn";
            if (!string.IsNullOrEmpty(msg))
                return Json(new { s = "error", result = msg }, JsonRequestBehavior.AllowGet);

            var provinceInfo = Mapper.Map<ProvinceViewModel>(_provinceService.GetBy(contestants.CityId)) ?? new ProvinceViewModel();
            contestants.CityName = provinceInfo.name;
            var districtInfo = Mapper.Map<DistrictViewModel>(_districtService.GetBy(contestants.DistrictId)) ?? new DistrictViewModel();
            contestants.DistrictName = districtInfo.name;
            var classInfo = Mapper.Map<ClassList>(_classService.GetBy(contestants.GradeId));
            contestants.GradeName = classInfo.Name;
            hasValue = _contestantService.Update(Mapper.Map<TB_Contestants>(contestants));
            
            hasValue = _vuViecRepo.Commit();

            var status = string.IsNullOrEmpty(hasValue) ? "oke" : "error";
            return Json(new { s = status, result = status.Equals("oke") ? Url.Action("ThongTinThiSinh", "NamPhutThuocBai", new { msg = 2 }) : hasValue }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ThoatTaiKhoan()
        {
            InitLanguage();
            if (System.Web.HttpContext.Current.Request.Cookies["userNamPhut"] != null)
            {
                HttpCookie cookie = this.ControllerContext.HttpContext.Request.Cookies["userNamPhut"];
                cookie.Expires = DateTime.Now.AddDays(-1);
                this.ControllerContext.HttpContext.Response.Cookies.Add(cookie);
            }
                
            return RedirectToAction("Index");
        }
        
        public ActionResult DangNhap()
        {
            InitLanguage();
            return View();
        }
        [HttpPost]
        public ActionResult DangKy(Contestants contestants)
        {
            var validCode = string.Empty;
            if (!contestants.IsAccountSTNHD)
            {
                var hasUserName = !string.IsNullOrEmpty(_userInfoService.GetBy(contestants.Username).userName);
                if (hasUserName)
                    validCode = "Tên Đăng Nhập đã tồn tại, vui lòng nhập Tên Đăng Nhập khác";
            }
            if (!string.IsNullOrEmpty(validCode))
                return Json(new { s = "error", result = validCode }, JsonRequestBehavior.AllowGet);

            var otpStatus = _smsService.CheckOTP(contestants.Phone, contestants.OTP);
            var msg = "";
            if (otpStatus.Equals("Nothing"))
                msg = "Mã OTP không tồn tại";
            if (otpStatus.Equals("expired"))
                msg = "Mã OTP đã hết hạn";
            if (!string.IsNullOrEmpty(msg))
                return Json(new { s = "error", result = msg }, JsonRequestBehavior.AllowGet);

            contestants.IdentificationNumber = "STNHD-" + _contestantService.GetIdentificationNumber();
            var provinceInfo = Mapper.Map<ProvinceViewModel>(_provinceService.GetBy(contestants.CityId)) ?? new ProvinceViewModel();
            contestants.CityName = provinceInfo.name;
            var districtInfo = Mapper.Map<DistrictViewModel>(_districtService.GetBy(contestants.DistrictId)) ?? new DistrictViewModel();
            contestants.DistrictName = districtInfo.name;
            contestants.DateCreate = DateTime.Now;
            hasValue = _contestantService.Insert(Mapper.Map<TB_Contestants>(contestants));
            if (!contestants.IsAccountSTNHD)
            {
                //-- Add to userinfo
                var _userInfo = new UserInfoViewModel();
                _userInfo.userName = contestants.Username;
                _userInfo.fullName = contestants.ParentName;
                _userInfo.HasClass = false;
                _userInfo.Password = contestants.Password;
                _userInfo.Email = contestants.Email;
                _userInfo.Phone = contestants.Phone;
                _userInfo.Address = contestants.Address;
                _userInfo.ActivePhone = false;
                _userInfo.ClassId = contestants.GradeId;
                _userInfo.HasToken = false;
                _userInfo.HasEbook = false;
                _userInfo.ChildFullName1 = contestants.ChildName;
                _userInfo.Child1_ClassId = contestants.GradeId;
                _userInfo.Child1_SchoolName = contestants.SchoolName;
                _userInfo.CityId = contestants.CityId;
                _userInfo.CityName = contestants.CityName;
                _userInfo.DistrictId = contestants.DistrictId;
                _userInfo.DistrictName = contestants.DistrictName;
                //-- Add to userinfo +
                hasValue = _userInfoService.Insert(Mapper.Map<M_UserInfo>(_userInfo));
            }
           
            hasValue = _vuViecRepo.Commit();

            var status = string.IsNullOrEmpty(hasValue) ? "oke" : "error";
            return Json(new { s = status, result = status.Equals("oke") ? Url.Action("DangKyThanhCong", "NamPhutThuocBai", new { id = contestants.IdentificationNumber }) : hasValue }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DangKyThanhCong(string id)
        {
            InitLanguage();
            var contestants = Mapper.Map<Contestants>(_contestantService.GetByIDNo(id));
            return View(contestants);
        }
        public ActionResult TheLeCuocThi()
        {
            InitLanguage();
            return View();
        }
        public ActionResult CauHoiThuongGap()
        {
            InitLanguage();
            return View();
        }
        public ActionResult HoTroHoiDap()
        {
            InitLanguage();
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult DangNhapCuocThi(LoginResult u)
        {
            try
            {
                var userInfo = Mapper.Map<ThongTinThiSinh>(_contestantService.GetByUserName(u.userName, u.password)) ?? new ThongTinThiSinh();
                if (string.IsNullOrEmpty(userInfo.Username))
                    return Json(new { s = "error", url = "Thông tin đăng nhập chưa đúng<BR /> Vui lòng kiểm tra lại" }, JsonRequestBehavior.AllowGet);
                
                HttpCookie userCookie = new HttpCookie("userNamPhut", userInfo.Username);
                userCookie.Expires = DateTime.Now.AddYears(8);
                System.Web.HttpContext.Current.Response.Cookies.Add(userCookie);

                var baiThi = _storeProService.SP_BaiThi(userInfo.Username).Where(d => d.IsActive).FirstOrDefault();
                
                return Json(new { s = "Oke", url = Url.Action("Index", "namphutthuocbai", new { msg = 2 }) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { s = "error", url = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult DangNhap(LoginResult u)
        {
            try
            {
                var userCuocThi = Mapper.Map<Contestants>(_contestantService.GetByUserName(u.userName));
                if (userCuocThi != null)
                    return Json(new { s = "error", url = string.Format("Tên đăng nhập {0} đã đăng ký cuộc thi<BR />Vui lòng chọn tên đăng nhập khác", u.userName) }, JsonRequestBehavior.AllowGet);
                var userInfo = new LoginResult();
                userInfo = Mapper.Map<LoginResult>(_userInfoService.CheckByUsername(u.userName, u.password));
                if (string.IsNullOrEmpty(userInfo.userName))
                    return Json(new { s = "error", url = "Thông tin đăng nhập chưa đúng<BR /> Vui lòng kiểm tra lại" }, JsonRequestBehavior.AllowGet);
                return Json(new { s = "Oke", url = Url.Action("dangky", "namphutthuocbai", new { id = userInfo.Id }) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { s = "error", url = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public string GetUserName()
        {
            try
            {
                if (System.Web.HttpContext.Current.Request.Cookies["userNamPhut"] != null)
                    return System.Web.HttpContext.Current.Request.Cookies["userNamPhut"].Value;
                return "";
            }
            catch { return ""; }
        }
        public ActionResult DoiQua()
        {
            InitLanguage();
            if (System.Web.HttpContext.Current.Request.Cookies["userNamPhut"] == null)
                return RedirectToAction("DangNhapCuocThi");
            var userName = GetUserName();
            var giftPage = new GiftPage();
            giftPage.Gifts = Mapper.Map<IEnumerable<Gift>>(_giftService.GetAllIsShow());
            giftPage.TotalPoint = _pointEntryService.TotalPoint(userName);
            giftPage.PointEntry = Mapper.Map<IEnumerable<PointEntry>>(_pointEntryService.FinAll(userName));
            giftPage.TransGifts = Mapper.Map<IEnumerable<TransactionGift>>(_transGiftService.FindAll(userName));
            return View(giftPage);
        }
        public ActionResult NhanQua(string giftCode)
        {
            if (System.Web.HttpContext.Current.Request.Cookies["userNamPhut"] == null)
                return RedirectToAction("DangNhapCuocThi");
            var transGift = new TransactionGift();
            transGift.Code = StringUtil.CheckLetter("G", _transGiftService.GetMax(), 4);
            transGift.userName = GetUserName();
            var userInf = Mapper.Map<Contestants>(_contestantService.GetByUserName(transGift.userName));
            transGift.FullName = userInf.ParentName;
            transGift.PhoneNo = userInf.Phone;
            transGift.Email = userInf.Email;
            var giftInfo = Mapper.Map<Gift>(_giftService.GetBy(giftCode));
            transGift.GiftCode = giftInfo.Code;
            transGift.GiftName = giftInfo.GiftName;
            transGift.DateCreate = DateTime.Now;
            transGift.LastDateUpdate = DateTime.Now;
            transGift.StatusCode = "A";
            transGift.StatusName = "Chờ xác nhận";
            transGift.Point = giftInfo.Point;
            return View(transGift);
        }
        [HttpPost]
        public ActionResult NhanQua(TransactionGift transGift)
        {
            try
            {
                var userName = GetUserName();
                var userInfo= Mapper.Map<Contestants>(_contestantService.GetByUserName(userName));
                var giftInfo = Mapper.Map<Gift>(_giftService.GetBy(transGift.GiftCode));
                int totalPoint = _pointEntryService.TotalPoint(userName);
                if (totalPoint < giftInfo.Point)
                {
                    return Json(new { s = "error", result = $"Số điểm hiển tại của bạn là {totalPoint}, không đủ để đổi quà {giftInfo.GiftName} cần {giftInfo.Point} điểm" }, JsonRequestBehavior.AllowGet);
                }

                transGift.Code = StringUtil.CheckLetter("G", _transGiftService.GetMax(), 4);
                transGift.userName = userName;
                transGift.DateCreate = DateTime.Now;
                transGift.LastDateUpdate = DateTime.Now;
                transGift.StatusCode = "A";
                transGift.StatusName = "Chờ xác nhận";
                //-- Insert point entry
                var pointEntry = new PointEntry();
                pointEntry.userName = transGift.userName;
                pointEntry.SBD = userInfo.IdentificationNumber;
                pointEntry.Deskcripton = "Đổi quà";
                pointEntry.Point = transGift.Point * -1;
                pointEntry.FromSource = "GIFT";
                pointEntry.FromSourceNo = transGift.Code;
                //-- Insert point entry +
                hasValue = _pointEntryService.Insert(Mapper.Map<TB_PointEntry>(pointEntry));
                hasValue = _transGiftService.Insert(Mapper.Map<TB_TransactionGift>(transGift));
                hasValue = _vuViecRepo.Commit();
                var url = Url.Action("DoiQua", new { msg = 2 });
                return Json(new { s = "oke", result = url }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var err = ex.ToString();
                return Json(new { s = "error", result = $"Đổi quà thất bại, vui lòng liên hệ hotline 0939 279 868 để được hổ trợ" }, JsonRequestBehavior.AllowGet);
            }
            
        }
    }
}
