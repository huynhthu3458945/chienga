﻿using GCSOFT.MVC.Service.STNHDService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.STNHD.Controllers
{
    public class SupportController : BaseController
    {
        public SupportController
            (
                IShareInfoService shareInfoService
            ) : base(shareInfoService)
        {

        }
        public ActionResult Guide()
        {
            return View();
        }
        public ActionResult AgencyPolicy()
        {
            return View();
        }
        // GET: Support
        public ActionResult Student(int? id)
        {
            var _id = id ?? 12;
            switch (_id)
            {
                case 12:
                    ViewBag.TitleVideo = "Đăng nhập và kích hoạt tài khoản Siêu Trí Nhớ Học Đường";
                    ViewBag.link = "https://daotao.stnhd.com/Files/HuongDanDaiLy/12_HuongDan_STNHD_DKvaKichHoat_TK_STNHD.mp4";
                    break;
                case 13:
                    ViewBag.TitleVideo = "Hướng dẫn học viên đổi mật khẩu";
                    ViewBag.link = "https://daotao.stnhd.com/Files/HuongDanDaiLy/13_HuongDan_STNHD_CachDoiMatKhau_TK_STNHD.mp4";
                    break;
                case 14:
                    ViewBag.TitleVideo = "Hướng dẫn lấy lại tên đăng nhập";
                    ViewBag.link = "https://daotao.stnhd.com/Files/HuongDanDaiLy/14_HuongDan_STNHD_LayLaiTenDangNhap.mp4";
                    break;
                case 15:
                    ViewBag.TitleVideo = "Hướng dẫn lấy lại mật khẩu";
                    ViewBag.link = "https://daotao.stnhd.com/Files/HuongDanDaiLy/15_HuongDan_STNHD_LayLaiMatKhai_HocVien.mp4";
                    break;
               
            }
            return View();
        }
        public ActionResult Agency(int? id)
        {
            var _id = id ?? 1;
            switch (_id)
            {
                case 1:
                    ViewBag.TitleVideo = "Đại lý đăng nhập và đổi mật khẩu";
                    ViewBag.link = "https://daotao.stnhd.com/Files/HuongDanDaiLy/1_HuongDan_STNHD_DangNhapvaDoiMatKhau.mp4";
                    break;
                case 2:
                    ViewBag.TitleVideo = "Đại lý đặt đơn hàng thẻ cho Tâm Trí Lực";
                    ViewBag.link = "https://daotao.stnhd.com/Files/HuongDanDaiLy/2_HuongDan_STNHD_DatTheChoDaiLy.mp4";
                    break;
                case 3:
                    ViewBag.TitleVideo = "Đại lý nhận thẻ vào kho";
                    ViewBag.link = "https://daotao.stnhd.com/Files/HuongDanDaiLy/3_HuongDan_STNHD_NhanThe.mp4";
                    break;
                case 4:
                    ViewBag.TitleVideo = "Bán mã kích hoạt khách lẻ";
                    ViewBag.link = "https://daotao.stnhd.com/Files/HuongDanDaiLy/4_HuongDan_STNHD_BanMa.mp4";
                    break;
                case 5:
                    ViewBag.TitleVideo = "Bán mã kích hoạt khách lẻ số lượng lớn";
                    ViewBag.link = "https://daotao.stnhd.com/Files/HuongDanDaiLy/5_HuongDan_STNHD_XuatMa.mp4";
                    break;
                case 6:
                    ViewBag.TitleVideo = "Bán mã kích hoạt cho đại lý con";
                    ViewBag.link = "https://daotao.stnhd.com/Files/HuongDanDaiLy/6_HuongDan_STNHD_BanHangChoDaiLyCon.mp4";
                    break;
                case 7:
                    ViewBag.TitleVideo = "Bán mã kích hoạt cho cộng tác viên";
                    ViewBag.link = "https://daotao.stnhd.com/Files/HuongDanDaiLy/7_HuongDan_STNHD_BanHangChoCTV.mp4";
                    break;
                case 8:
                    ViewBag.TitleVideo = "Thu hồi thẻ đại lý con";
                    ViewBag.link = "https://daotao.stnhd.com/Files/HuongDanDaiLy/8_HuongDan_STNHD_ThuHoiThe.mp4";
                    break;
                case 9:
                    ViewBag.TitleVideo = "Thu hồi thẻ cộng tác viên";
                    ViewBag.link = "https://daotao.stnhd.com/Files/HuongDanDaiLy/9_HuongDan_STNHD_ThuHoiTheCTV.mp4";
                    break;
                case 10:
                    ViewBag.TitleVideo = "Thu hồi mã kích hoạt đã bán";
                    ViewBag.link = "https://daotao.stnhd.com/Files/HuongDanDaiLy/10_HuongDan_STNHD_ThuHoiMaKichHoatDaBan.mp4";
                    break;
                case 11:
                    ViewBag.TitleVideo = "Chỉnh sửa thông tin và gửi lại mật khẩu cho đại lý con, cộng tác viên";
                    ViewBag.link = "https://daotao.stnhd.com/Files/HuongDanDaiLy/11_HuongDan_STNHD_ChinhSuaThongTinvaGuiLaiMatKhauChoDaiLyCon.mp4";
                    break;
            }
            return View();
        }
    }
}