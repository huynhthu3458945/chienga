﻿using GCSOFT.MVC.Service.STNHDService.Interface;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.STNHD.Controllers
{
    public class TestVideosController : BaseController
    {
        public TestVideosController
            (
                IShareInfoService shareInfoService
            ) : base(shareInfoService)
        {

        }
        // GET: TestVideos
        public ActionResult Index()
        {
           // ViewBag.ShareInfo = GetShareInfo();
            
            var client = new RestClient("https://dev.vdocipher.com/api/videos/c997920126ad4530b395d4d205a2e7f2/otp");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Apisecret OEwYtT4oU4Dwp38rdEVoqzd8L3RnII3JxrcUba0J2hy3e6wQGQKjC6Xk6YClZh2L");
            request.AddHeader("Content-Type", "application/json");
            IRestResponse response = client.Execute(request);
            Console.WriteLine(response.Content);

            ViewBag.otp = "";
            ViewBag.playbackInfo = "";
            return View();
        }
    }
}