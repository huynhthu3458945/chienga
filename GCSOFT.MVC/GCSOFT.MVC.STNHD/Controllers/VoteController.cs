﻿using AutoMapper;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.STNHDService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.STNHD.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.STNHD.Controllers
{
    public class VoteController : BaseController
    {
        private readonly IShareInfoService _shareInfoService;
        private readonly IVoteService _voteService;
        private readonly IVuViecService _vuViecRepo;
        private string hasValue;
        public VoteController
            (
                IShareInfoService shareInfoService
                , IVoteService voteService
                , IVuViecService vuViecRepo
            ) : base(shareInfoService)
        {
            _shareInfoService = shareInfoService;
            _voteService = voteService;
            _vuViecRepo = vuViecRepo;
        }
        /// <summary>
        /// Add vote to database
        /// </summary>
        /// <param name="id">Refer Id</param>
        /// <param name="t">Refer Type</param>
        /// <param name="h">Has Vote</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Create(int id,int t, bool h)
        {
            var userInfo = GetUserInfo();
            if (h)
                hasValue = _voteService.Delete((ResultType)t, id, userInfo.email);
            else
            {
                var voteInfo = new VoteViewModel();
                voteInfo.ReferId = id;
                voteInfo.Type = (ResultType)t;
                voteInfo.DateInput = DateTime.Now;
                voteInfo.UserId = userInfo.Id;
                voteInfo.UserName = userInfo.email;
                voteInfo.FullName = userInfo.fullName;
                hasValue = _voteService.Insert(Mapper.Map<M_Vote>(voteInfo));
            }
            hasValue = _vuViecRepo.Commit();
            int noOfComment = 0;
            if (string.IsNullOrEmpty(hasValue))
                noOfComment = _voteService.NoOfVote((ResultType)t, id);
            return Json(noOfComment, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetVote(int id, int t)
        {
            var _noOfVote = _voteService.NoOfVote((ResultType)t, id);
            return Json(new { noOfVote = _noOfVote }, JsonRequestBehavior.AllowGet);
        }

    }
}