﻿using GCSOFT.MVC.Service.STNHDService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.STNHD.Controllers
{
    public class activeController : BaseController
    {
        private readonly IShareInfoService _shareInfoService;
        private string hasValue;
        private string API_URL = ConfigurationManager.AppSettings["apiBaseAddress"];
        public activeController
            (
                IShareInfoService shareInfoService
            ) : base(shareInfoService)
        {
            _shareInfoService = shareInfoService;
        }
        // GET: active
        public ActionResult Index(string email, string token)
        {
            //ViewBag.ShareInfo = GetShareInfo();
            var client = new RestClient(string.Format("{0}/Outside/AccountActivation?token={1}&email={2}", API_URL, token, email));
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);
            return View();
        }
    }
}