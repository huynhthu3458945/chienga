﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.STNHD.Helper
{
    public class Paging
    {
        public int TotalRows { get; set; }
        public int PerPage { get; set; }
        public int CurPage { get; set; }
        public decimal TotalPage
        {
            get { return Math.Ceiling(TotalRows / (decimal)PerPage); }
        }
        public int start
        {
            get { return (CurPage - 1) * PerPage; }
        }
        public int offset { get { return PerPage; } }
        public int startIndex { get { return start; } }
        public Paging()
        {
            PerPage = 20;
        }
        public Paging(int _TotalRows, int _CurPage)
        {
            TotalRows = _TotalRows;
            CurPage = _CurPage;
            PerPage = 20;
        }
    }
}