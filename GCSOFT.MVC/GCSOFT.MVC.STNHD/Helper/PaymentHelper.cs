﻿using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Security;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace GCSOFT.MVC.STNHD.Helper
{
    public class PaymentHelper
    {
        public static string CreateDataToSign(object obj)
        {
            var orderType = obj.GetType();
            var properties = orderType
            .GetProperties();
            var sortedArr = properties.OrderBy(x => x.Name).ToArray();
            var sb = new StringBuilder();
            foreach (var prop in sortedArr)
            {
                if (prop.CanRead)
                {
                    if ((prop.PropertyType.Name == "String"
                    || prop.PropertyType.Name == "Decimal"
                    || prop.Name == "TotalAmount"
                    || prop.Name == "InternationalFee"
                    || prop.Name == "DomesticFee"
                    || prop.Name == "SSN"
                    || prop.PropertyType.Name == "Boolean"
                    || prop.Name == "IsTokenRequest"
                    || prop.Name == "SubscribeOnly"
                    || prop.Name == "SubscribeWithMin")
                    && prop.Name != "Signature")
                    {
                        var value = prop.GetValue(obj);
                        sb.Append(value?.ToString());
                    }
                }
            }
            return sb.ToString();
        }
        public string GenerateSignature(string privateKey, string input)
        {
            byte[] data = Encoding.ASCII.GetBytes(input);
            AsymmetricCipherKeyPair keyPair =
            (AsymmetricCipherKeyPair)new PemReader(new StringReader(privateKey)).ReadObject();
            ISigner signer = SignerUtilities.GetSigner("MD5withRSA");
            signer.Init(true, keyPair.Private);
            signer.BlockUpdate(data, 0, data.Length);
            byte[] output = signer.GenerateSignature();
            return Convert.ToBase64String(output);
        }
        public static bool VerifySignature(string data, string signature, string publicKey)
        {
            try
            {
                AsymmetricKeyParameter keyPair = (AsymmetricKeyParameter)
                new PemReader(new StringReader(publicKey)).ReadObject();
                var dataBytes = Encoding.UTF8.GetBytes(data);
                var signatureBytes = Convert.FromBase64String(signature);
                ISigner signer = SignerUtilities.GetSigner("MD5withRSA");
                signer.Init(false, keyPair);
                signer.BlockUpdate(dataBytes, 0, dataBytes.Length);
                return signer.VerifySignature(signatureBytes);
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static string TrippleDESDecrypt(string cipherText, string shareKey)
        {
            byte[] input = Convert.FromBase64String(cipherText);
            TripleDESCryptoServiceProvider tripleDES  = new TripleDESCryptoServiceProvider();
            tripleDES.Key = Encoding.UTF8.GetBytes(shareKey);
            tripleDES.Mode = CipherMode.ECB;
            tripleDES.Padding = PaddingMode.PKCS7;
            return Encoding.UTF8.GetString(tripleDES
            .CreateDecryptor()
            .TransformFinalBlock(input, 0, input.Length));
        }
    }
}