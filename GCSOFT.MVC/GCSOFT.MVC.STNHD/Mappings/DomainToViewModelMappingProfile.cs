﻿using AutoMapper;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.NamPhutThuocBai;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Model.Videos;
using GCSOFT.MVC.STNHD.Models;
using GCSOFT.MVC.STNHD.Models.NamPhutViewModels;
using GCSOFT.MVC.STNHD.Models.PageViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace GCSOFT.MVC.STNHD.Mappings
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "DomainToViewModelMappings"; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<E_Class, ClassList>();
            Mapper.CreateMap<E_Course, CourseList>();
            Mapper.CreateMap<M_Question, QuestionViewModel>();
            Mapper.CreateMap<E_Course, CourseViewModel>();
            Mapper.CreateMap<E_CourseContent, CourseContentList>();
            Mapper.CreateMap<E_CourseContent, CourseContentCard>();
            Mapper.CreateMap<M_Vote, VoteViewModel>();
            Mapper.CreateMap<M_Comment, CommentViewModel>();
            Mapper.CreateMap<M_UserInfo, LoginResult>();
            Mapper.CreateMap<M_FileResource, VM_FileResource>();
            Mapper.CreateMap<M_Supporter, SupporterViewModel>();
            Mapper.CreateMap<E_ClassRefer, ClassReferViewModel>();
            Mapper.CreateMap<M_StudentClass, StudentClassViewModel>();
            Mapper.CreateMap<M_UserInfo, UserInfoViewModel>();
            Mapper.CreateMap<M_Agency, AgencyList>();
            Mapper.CreateMap<M_MailSetup, MailSetup>();
            Mapper.CreateMap<M_Template, Template>();
            Mapper.CreateMap<M_SMS, SMSViewModel>();
            Mapper.CreateMap<M_ResetPassword, ResetPasswordViewModel>();
            Mapper.CreateMap<M_LoginEntry, LoginEntryViewModel>();
            Mapper.CreateMap<M_Customer, CustomerViewModel>();
            Mapper.CreateMap<M_OptionLine, OptionLineViewModel>();
            Mapper.CreateMap<E_Teacher, Teacher>();
            Mapper.CreateMap<M_PaymentResult, PaymentResultViewModel>();
            Mapper.CreateMap<M_CardLine, CardLine>();
            Mapper.CreateMap<M_CardEntry, CardEntry>();
            Mapper.CreateMap<M_District, DistrictViewModel>();
            Mapper.CreateMap<M_Province, ProvinceViewModel>();
            Mapper.CreateMap<M_EbookAccount, EbookAccount>();
            Mapper.CreateMap<M_Language, LanguageViewModel>();
            Mapper.CreateMap<M_MemoVideos, MemoVideos>();
            Mapper.CreateMap<TB_Contestants, Contestants>();
            Mapper.CreateMap<M_RetailSalesOrder, RetailSalesOrderModel>();
            Mapper.CreateMap<TB_DeThi, DeThi>();
            Mapper.CreateMap<TB_NopBai, NopBai>();
            Mapper.CreateMap<TB_Contestants, ThongTinThiSinh>();
            Mapper.CreateMap<TB_Gift, Gift>();
            Mapper.CreateMap<TB_PointEntry, PointEntry>();
            Mapper.CreateMap<TB_TransactionGift, TransactionGift>();
        }
    }
}