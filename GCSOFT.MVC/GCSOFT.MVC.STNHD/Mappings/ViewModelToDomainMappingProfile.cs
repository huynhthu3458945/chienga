﻿using AutoMapper;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.NamPhutThuocBai;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Model.Videos;
using GCSOFT.MVC.STNHD.Models;
using GCSOFT.MVC.STNHD.Models.NamPhutViewModels;
using GCSOFT.MVC.STNHD.Models.PageViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.STNHD.Mappings
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "ViewModelToDomainMappings"; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<ClassList, E_Class>();
            Mapper.CreateMap<CourseList, E_Course>();
            Mapper.CreateMap<QuestionViewModel, M_Question>();
            Mapper.CreateMap<CourseViewModel, E_Course>();
            Mapper.CreateMap<CourseContentList, E_CourseContent>();
            Mapper.CreateMap<CourseContentCard, E_CourseContent>();
            Mapper.CreateMap<VoteViewModel, M_Vote>();
            Mapper.CreateMap<CommentViewModel, M_Comment>();
            Mapper.CreateMap<LoginResult, M_UserInfo>();
            Mapper.CreateMap<VM_FileResource,M_FileResource>();
            Mapper.CreateMap<SupporterViewModel, M_Supporter>();
            Mapper.CreateMap<ClassReferViewModel, E_ClassRefer>();
            Mapper.CreateMap<StudentClassViewModel, M_StudentClass>();
            Mapper.CreateMap<UserInfoViewModel, M_UserInfo>();
            Mapper.CreateMap<AgencyList, M_Agency>();
            Mapper.CreateMap<MailSetup, M_MailSetup>();
            Mapper.CreateMap<Template, M_Template>();
            Mapper.CreateMap<SMSViewModel, M_SMS>();
            Mapper.CreateMap<ResetPasswordViewModel, M_ResetPassword>();
            Mapper.CreateMap<LoginEntryViewModel, M_LoginEntry>();
            Mapper.CreateMap<CustomerViewModel, M_Customer>();
            Mapper.CreateMap<OptionLineViewModel, M_OptionLine>();
            Mapper.CreateMap<Teacher, E_Teacher>();
            Mapper.CreateMap<PaymentResultViewModel, M_PaymentResult>();
            Mapper.CreateMap<CardLine, M_CardLine>();
            Mapper.CreateMap<CardEntry, M_CardEntry>();
            Mapper.CreateMap<DistrictViewModel, M_District>();
            Mapper.CreateMap<ProvinceViewModel, M_Province>();
            Mapper.CreateMap<EbookAccount, M_EbookAccount>();
            Mapper.CreateMap<LanguageViewModel, M_Language>();
            Mapper.CreateMap<MemoVideos, M_MemoVideos>();
            Mapper.CreateMap<Contestants, TB_Contestants>();
            Mapper.CreateMap<RetailSalesOrderModel, M_RetailSalesOrder>();
            Mapper.CreateMap<DeThi, TB_DeThi>();
            Mapper.CreateMap<NopBai, TB_NopBai>();
            Mapper.CreateMap<ThongTinThiSinh, TB_Contestants>();
            Mapper.CreateMap<Gift, TB_Gift>();
            Mapper.CreateMap<PointEntry, TB_PointEntry>();
            Mapper.CreateMap<TransactionGift, TB_TransactionGift>();
        }
    }
}