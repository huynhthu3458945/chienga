﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.STNHD.Models
{
    public class APIResult
    {
        public string returnCode { get; set; }
        public string message { get; set; }
        public List<String> errorCodes { get; set; }
    }
}