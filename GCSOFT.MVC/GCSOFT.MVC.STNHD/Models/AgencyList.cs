﻿using System;

namespace GCSOFT.MVC.STNHD.Models
{
    public class AgencyList
    {
        public string Id { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string LeavelName { get; set; }
        public string Address { get; set; }
        public string Image { get; set; }
        public bool ShowWebsite { get; set; }
        public string ImgCert { get; set; }
        public string BankAccNo { get; set; }
        public string BankAccName { get; set; }
        public string beneficiary { get; set; }
        public int RatingId { get; set; }
        public string RatingCode { get; set; }
        public string RatingName { get; set; }
        public DateTime? DateCreate { get; set; }
    }
}