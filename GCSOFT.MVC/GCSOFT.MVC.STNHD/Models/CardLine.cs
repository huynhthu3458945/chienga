﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.STNHD.Models
{
    public class CardLine
    {
        public long Id { get; set; }
        public string LotNumber { get; set; }
        public string SerialNumber { get; set; }
        public string CardNo { get; set; }
        public decimal Price { get; set; }
        public string PriceStr { get { return string.Format("{0:#,##}", Price); } }
        public string UserName { get; set; }
        public string UserFullName { get; set; }
        public DateTime DateCreate { get; set; }
        public UserType UserType { get; set; }
        public string UserNameActive { get; set; }
        public string UserActiveFullName { get; set; }
        public DateTime LastDateUpdate { get; set; }
        public int StatusId { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }
        public int LastStatusId { get; set; }
        public string LastStatusCode { get; set; }
        public string LastStatusName { get; set; }
        public int DurationId { get; set; }
        public string DurationCode { get; set; }
        public string DurationName { get; set; }
        public int ReasonId { get; set; }
        public string ReasonCode { get; set; }
        public string ReasonName { get; set; }
        public int LevelId { get; set; }
        public string LevelCode { get; set; }
        public string LevelName { get; set; }
    }
    public class CardEntry
    {
        public string LotNumber { get; set; }
        public long CardLineId { get; set; }
        public string SerialNumber { get; set; }
        public string CardNo { get; set; }
        public decimal Price { get; set; }
        public string UserName { get; set; }
        public string UserFullName { get; set; }
        public DateTime DateCreate { get; set; }
        public UserType UserType { get; set; }
        public string UserNameActive { get; set; }
        public string UserActiveFullName { get; set; }
        public DateTime Date { get; set; }
        public int StatusId { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }
        public CardSource Source { get; set; }
        public string SourceNo { get; set; }
        public int? AgencyId { get; set; }
        public string AgencyName { get; set; }
        public int DurationId { get; set; }
        public string DurationCode { get; set; }
        public string DurationName { get; set; }
        public int ReasonId { get; set; }
        public string ReasonCode { get; set; }
        public string ReasonName { get; set; }
        public int LevelId { get; set; }
        public string LevelCode { get; set; }
        public string LevelName { get; set; }
    }
}