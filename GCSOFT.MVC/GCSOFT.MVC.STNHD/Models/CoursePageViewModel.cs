﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.STNHD.Models
{
    public class CoursePageViewModel
    {
        public CourseViewModel CourseInfo { get; set; }
        public IEnumerable<CourseContentList> CourseContents { get; set; }
        public LoginResult UserInfo { get; set; }
        public IEnumerable<CourseList> Courses { get; set; }
    }
}