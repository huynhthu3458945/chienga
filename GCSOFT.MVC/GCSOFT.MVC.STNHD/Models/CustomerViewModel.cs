﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.STNHD.Models
{
    public class CustomerViewModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string FullName { get; set; }
        public string PhoneNo { get; set; }
        public string IsEmail { get; set; }
        public string Email { get; set; }
        public DateTime? RegisterDate { get; set; }
        public string RegistrationDate { get; set; }
        public string RegisterDateStr { get { return string.Format("{0:#,##0}", RegisterDate); } }
        public string Owner { get; set; }
        public int? NoOfCard { get; set; }
        public string LotNumber { get; set; }
        public string SerialNumber { get; set; }
        public string CardNo { get; set; }
        public string UserOwner { get; set; }
        public DateTime? DateCard { get; set; }
        public int StatusId { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }
    }
}