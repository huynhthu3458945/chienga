﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.STNHD.Models
{
    public class MemoVideos
    {
        public int Id { get; set; }
        public string TitleVN { get; set; }
        public string TitleEN { get; set; }
        public string LinkVN { get; set; }
        public string LinkEN { get; set; }
        public int SortOrder { get; set; }
        public int ParentSortOrder { get; set; }
        public string Time { get; set; }
    }
}