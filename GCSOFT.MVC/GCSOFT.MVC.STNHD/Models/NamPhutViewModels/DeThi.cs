﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.STNHD.Models.NamPhutViewModels
{
    public class DeThi
    {
        public int Id { get; set; }
        public int ClassId { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string NgayBatDauStr { get; set; }
        public string NgayKetThucStr { get; set; }
        public bool IsActive { get; set; }
    }
}