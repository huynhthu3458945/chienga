﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.STNHD.Models.NamPhutViewModels
{
    public class Gift
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int Point { get; set; }
        public string GiftName { get; set; }
    }
}