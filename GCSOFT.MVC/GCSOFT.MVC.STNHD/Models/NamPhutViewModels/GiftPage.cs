﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.STNHD.Models.NamPhutViewModels
{
    public class GiftPage
    {
        public IEnumerable<Gift> Gifts { get; set; }
        public int TotalPoint { get; set; }
        public IEnumerable<PointEntry> PointEntry { get; set; }
        public IEnumerable<TransactionGift> TransGifts { get; set; }
        public GiftPage()
        {
            TotalPoint = 0;
            Gifts = new List<Gift>();
            PointEntry = new List<PointEntry>();
            TransGifts = new List<TransactionGift>();
        }
    }
}