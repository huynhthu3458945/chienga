﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.STNHD.Models.NamPhutViewModels
{
    public class NopBai
    {
        public int Id { get; set; }
        public string userName { get; set; }
        public string SBD { get; set; }
        public string FullName { get; set; }
        public string CodeDethi { get; set; }
        public int IdContestants { get; set; }
        public string LinkFacebok { get; set; }
        public int? LuotView { get; set; }
        public int? LuotInteract { get; set; }
        public int? LuotShare { get; set; }
        public int? LuotComment { get; set; }
        public int IdTrongTaiVong1 { get; set; }
        public int IdTrongTaiChuyenMon1 { get; set; }
        public int IdTrongTaiChuyenMon2 { get; set; }
        public DateTime DateInput { get; set; }
        public int TieuChi1 { get; set; }
        public int TieuChi2 { get; set; }
        public int TieuChi3 { get; set; }
        public int TieuChi4 { get; set; }
        public int TieuChi5 { get; set; }
        public int ChuyenMon1_TrinhBay1 { get; set; }
        public int ChuyenMon1_TrinhBay2 { get; set; }
        public int ChuyenMon1_TrinhBay3 { get; set; }
        public int ChuyenMon1_NoiDung { get; set; }
        public int ChuyenMon1_Mindmap { get; set; }
        public int ChuyenMon2_TrinhBay1 { get; set; }
        public int ChuyenMon2_TrinhBay2 { get; set; }
        public int ChuyenMon2_TrinhBay3 { get; set; }
        public int ChuyenMon2_NoiDung { get; set; }
        public int ChuyenMon2_Mindmap { get; set; }
        public int TongDiem { get; set; }
        public int Point { get; set; }
        public string Vong1StatusCode { get; set; }
        public string Vong1StatusName { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }
        public string ChuyenMon1_StatusCode { get; set; }
        public string ChuyenMon1_StatusName { get; set; }
        public string ChuyenMon2_StatusCode { get; set; }
        public string ChuyenMon2_StatusName { get; set; }
        public bool IsChuyenMon { get; set; }
        public string UserTrongTaiVong1 { get; set; }
        public string UserTrongTaiChuyenMon1 { get; set; }
        public string UserTrongTaiChuyenMon2 { get; set; }
        public int TongDiemVong1 { get; set; }
        public string Remark1 { get; set; }
        public string Remark2 { get; set; }
        public string Remark3 { get; set; }
        public int ClassId { get; set; }
        public string ClassName { get; set; }
        public string FullNameTrongTaiVong1 { get; set; }
        public string FullNameTrongTaiChuyenMon1 { get; set; }
        public string FullNameTrongTaiChuyenMon2 { get; set; }
        public string NhaDtXuatSacCode { get; set; }
        public string NhaDtXuatSacName { get; set; }
        public string NhaDTTruyenCamHungCode { get; set; }
        public string NhaDTTruyenCamHungName { get; set; }

        public IEnumerable<SelectListItem> StatusList { get; set; }
        public IEnumerable<SelectListItem> ChuyenMon1StatusList { get; set; }
        public IEnumerable<SelectListItem> ChuyenMon2StatusList { get; set; }
        public IEnumerable<SelectListItem> TrongTaiVong1List { get; set; }
        public IEnumerable<SelectListItem> ChuyenMon1List { get; set; }
        public IEnumerable<SelectListItem> ChuyenMon2List { get; set; }
        public Contestants userInfo { get; set; }
        public NopBai()
        {
            userInfo = new Contestants();
        }
    }
}