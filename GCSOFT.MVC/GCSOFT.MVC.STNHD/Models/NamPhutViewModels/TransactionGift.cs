﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.STNHD.Models.NamPhutViewModels
{
    public class TransactionGift
    {
        public string Code { get; set; }
        public string userName { get; set; }
        public string FullName { get; set; }
        public string Address { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public string GiftCode { get; set; }
        public string GiftName { get; set; }
        public DateTime DateCreate { get; set; }
        public string DateTimeStr { get { return string.Format("{0:dd/MM/yyyy hh:mm}", DateCreate); } }
        public DateTime LastDateUpdate { get; set; }
        public string Remark { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }
        public int Point { get; set; }
        public string HistoryTransfer { get; set; }
    }
}