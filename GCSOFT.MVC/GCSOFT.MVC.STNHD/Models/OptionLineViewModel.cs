﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.STNHD.Models
{
    public class OptionLineViewModel
    {
        public int OptionHeaderID { get; set; }
        public int LineNo { get; set; }
        public string OptionHeaderCode { get; set; }
        public string OptionHeaderName { get; set; }
        [DisplayName("Mã")]
        public string Code { get; set; }
        [DisplayName("Tên")]
        public string Name { get; set; }
        [DisplayName("Thứ Tự")]
        public int Order { get; set; }
        [DisplayName("Ẩn/Hiện")]
        public bool IsHidden { get; set; }
    }
}