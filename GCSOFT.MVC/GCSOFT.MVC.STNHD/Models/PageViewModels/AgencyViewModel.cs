﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.STNHD.Models.PageViewModels
{
    public class AgencyViewModel
    {
        public LoginResult UserInfo { get; set; }
        public IEnumerable<AgencyList> AgencyList { get; set; }
    }
}