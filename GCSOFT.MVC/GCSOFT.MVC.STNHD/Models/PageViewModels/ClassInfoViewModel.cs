﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.STNHD.Models.PageViewModels
{
    public class ClassInfoViewModel
    {
        public IEnumerable<ClassList> ClassList { get; set; }
        public IEnumerable<ClassReferViewModel> ClassReferList { get; set; }
        public LoginResult UserInfo { get; set; }
        public SCBPayment PayInfo { get; set; }
    }
}