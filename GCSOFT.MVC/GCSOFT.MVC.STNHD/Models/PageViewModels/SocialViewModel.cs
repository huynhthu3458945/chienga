﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.STNHD.Models.PageViewModels
{
    public class SocialViewModel
    {
        public IEnumerable<QuestionViewModel> Socials { get; set; }
        public IEnumerable<QuestionViewModel> MyQuestions { get; set; }
        public LoginResult UserInfo { get; set; }
    }
}