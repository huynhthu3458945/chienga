﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.STNHD.Models
{
    public class PurchaseInfoModel
    {
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string TotalAmount { get; set; }
        public string Course { get; set; }
    }
}