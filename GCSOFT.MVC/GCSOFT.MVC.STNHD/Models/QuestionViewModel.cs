﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.STNHD.Models
{
    public class QuestionViewModel
    {
        public int Id { get; set; }
        public string Question { get; set; }
        public DateTime DateQuestion { get; set; }
        public string Answer { get; set; }
        public string MethodName { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }
        public string userName { get; set; }
        public string fullName { get; set; }
    }
    public class QuestionSuggestion
    {
        public int Id { get; set; }
        [AllowHtml]
        [DisplayName("Tiêu Đề")]
        public string Question { get; set; }
        [AllowHtml]
        [DisplayName("Nội Dung")]
        public string Answer { get; set; }
        [DisplayName("Ngày Góp ý")]
        public DateTime DateQuestion { get; set; }
        public int CourseId { get; set; }
        public bool IsReload { get; set; }
        public IEnumerable<VM_FileResource> FileResources { get; set; }
        public QuestionSuggestion()
        {
            var _fileResources = new List<VM_FileResource>();
            var _fileResource = new VM_FileResource();
            _fileResource.LineNo = 0;
            _fileResources.Add(_fileResource);
            FileResources = _fileResources;
        }
    }
}