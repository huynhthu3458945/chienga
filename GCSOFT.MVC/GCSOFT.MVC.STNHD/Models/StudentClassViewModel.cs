﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.STNHD.Models
{
    public class StudentClassViewModel
    {
        public int Id { get; set; }
        public string userName { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int ClassId { get; set; }
        public string ClassName { get; set; }
        public bool IsActive { get; set; }
        public StudentClassViewModel()
        {
            ClassId = 4;
        }
    }
    public class ActiveCode
    {
        public string code { get; set; }
    }
}