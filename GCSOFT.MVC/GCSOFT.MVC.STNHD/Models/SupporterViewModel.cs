﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.STNHD.Models
{
    public class SupporterViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string NameEN { get; set; }
        public int SortOrder { get; set; }
        public int NoOfColumn { get; set; }
    }
}