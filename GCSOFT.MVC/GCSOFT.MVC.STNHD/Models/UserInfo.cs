﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.STNHD.Models
{
    public class LoginResult
    {
        public int Id { get; set; }
        public string userName { get; set; }
        public string password { get; set; }
        public string fullName { get; set; }
        public string token { get; set; }
        public bool HasToken { get; set; }
        public bool HasVideoToken { get; set; }
        public int specialId { get; set; }
        public string email { get; set; }
        public string Phone { get; set; }
        public string Url { get; set; }
        public bool HasClass { get; set; }
        public bool ActivePhone { get; set; }
        public string returnUrl { get; set; }
        public string SessionId { get; set; }
        public string Address { get; set; }
        public int ClassId { get; set; }
        public bool HasEbook { get; set; }
        public List<string> errorCodes { get; set; }
    }
    public class GcError
    {
        public List<String> errorCodes { get; set; }
        public string returnCode { get; set; }
    }
    public class GcError2
    {
        public List<String> errorCodes { get; set; }
        public string returnCode { get; set; }
        public string userName { get; set; }
        public string refUser { get; set; }
        public string message { get; set; }
    }
    public class UserInfoViewModel
    {
        public int Id { get; set; }
        public string userName { get; set; }
        public string fullName { get; set; }
        public string Password { get; set; }
        public string RePassword { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Phone2 { get; set; }
        public string OTP { get; set; }
        public bool ActivePhone { get; set; }
        public string Address { get; set; }
        public string token { get; set; }
        public int? specialId { get; set; }
        public bool HasClass { get; set; }
        public string facebook { get; set; }
        public string zalo { get; set; }
        public string viber { get; set; }
        public int ClassId { get; set; }
        public string Serinumber { get; set; }
        public bool HasEbook { get; set; }
        public string ChildFullName1 { get; set; }
        public int Child1_ClassId { get; set; }
        public string ChildFullName2 { get; set; }
        public int Child2_ClassId { get; set; }
        public int? CityId { get; set; }
        public string CityName { get; set; }
        public int? DistrictId { get; set; }
        public string DistrictName { get; set; }
        public DateTime? DateActive { get; set; }
        public DateTime? DateExpired { get; set; }
        public bool HasToken { get; set; }
        public bool HasVideoToken { get; set; }
        public string Child1_SchoolName { get; set; }
        public string Child2_SchoolName { get; set; }
        public IEnumerable<SelectListItem> ClassList { get; set; }
        public IEnumerable<SelectListItem> CityList { get; set; }
        public IEnumerable<SelectListItem> DistrictList { get; set; }
        public string LevelCode { get; set; }
        public bool IsUpdateLevelCode300 { get; set; }
        public STNHDType? STNHDType { get; set; }
    }
    public class UserInfoLNLG
    {
        public string fullName { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string confirmPassword { get; set; }
        public string email { get; set; }
        public string phoneNumber { get; set; }
        public string address { get; set; }
        public string facebook { get; set; }
        public string zalo { get; set; }
        public string viber { get; set; }
        public bool isOldCustomer { get; set; }
        public country country { get; set; }
        public List<int> interestedCourses { get; set; }
    }
    public class country 
    {
        public int id { get; set; }
        public string value { get; set; }
    }
}