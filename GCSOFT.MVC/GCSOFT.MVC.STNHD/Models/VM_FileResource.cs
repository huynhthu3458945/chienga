﻿using GCSOFT.MVC.Model.MasterData;

namespace GCSOFT.MVC.STNHD.Models
{
    public class VM_FileResource
    {
        public ResultType Type { get; set; }
        public int ReferId { get; set; }
        public int CommentId { get; set; }
        public int LineNo { get; set; }
        public string Title { get; set; }
        public string Title2 { get; set; }
        public string Title3 { get; set; }
        public string Title4 { get; set; }
        public string ImageName { get; set; }
        public string ImagePath { get; set; }
        public string ImageUpload { get; set; }
        public string ImageBase64 { get; set; }
        public FileType FileType { get; set; }
        public Source Source { get; set; }
        public string Domain { get; set; }
        public string ImagePathFull { get; set; }
        public string ImageBase64Url
        {
            get
            {
                return string.Format("data:image/png;base64,{0}", ImageBase64);
            }
        }
    }
}