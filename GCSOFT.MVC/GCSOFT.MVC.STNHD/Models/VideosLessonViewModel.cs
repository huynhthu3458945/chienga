﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.STNHD.Models
{
    public class CourseVideosViewModel
    {
        public CourseViewModel CourseInfo { get; set; }
        public CourseContentList CourseVideo { get; set; }
        public IEnumerable<CourseContentList> CourseVideos { get; set; }
        public LoginResult UserInfo { get; set; }
        public bool HasVideos { get; set; }
    }
    public class LearningViewModel
    {
        public VideosLessionInfo LessionInfo { get; set; }
        public LoginResult UserInfo { get; set; }
        public VideosCourseData CourseData { get; set; }
        public List<VideosLesson> lessons { get; set; }
        
    }
    public class VideosCourseData
    {
        public int id { get; set; }
        public string name { get; set; }
        public string url { get; set; }
        public string token { get; set; }
    }
    public class VideosLesson
    {

        public string name { get; set; }

        public List<VideosItem> items { get; set; }
    }
    public class VideosItem
    {
        public int id { get; set; }
        public string name { get; set; }
        public string time { get; set; }

    }
    public class VideosLessionInfo
    {
        public int productId { get; set; }
        public string productName { get; set; }
    }
}