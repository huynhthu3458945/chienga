﻿using GCSOFT.MVC.Model.AgencyModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.AgencyService.Interface
{
    public interface IAgencyService
    {
        IEnumerable<M_Agency> FindAll();
        IEnumerable<M_Agency> FindAll(AgencyType type, int? parentId);
        IEnumerable<M_Agency> FindAll(AgencyType type);
        IEnumerable<M_Agency> FindAll(List<AgencyType> types);
        IEnumerable<M_Agency> FindAllByAgency(int? parentId);
        IEnumerable<M_Agency> FindAll(int[] ids);
        IEnumerable<M_Agency> FindAll(int packageId);
        IEnumerable<M_Agency> FindAllHasImage();
        M_Agency GetBy(int id);
        M_Agency GetBy2(int id);
        M_Agency GetByUserName(string userName);
        string Insert(M_Agency agency);
        string Update(M_Agency agency);
        string Delete(int[] ids);
        string HasDelete(int[] ids);
        string DeleteImg(int id);
        int GetID();
        bool CandAdd(string code, string xCode);
    }
}
