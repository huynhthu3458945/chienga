﻿using GCSOFT.MVC.Data.Repositories.AgencyRepositories.Interface;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Service.AgencyService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.AgencyService.Service
{
    public class PackageService : IPackageService
    {
        private readonly IPackageRepository _packedRepo;
        public PackageService
            (
                IPackageRepository packedRepo
            )
        {
            _packedRepo = packedRepo;
        }

        public string Delete(int[] ids)
        {
            try
            {
                _packedRepo.Delete(d => ids.Contains(d.Id));
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public IEnumerable<M_Package> FindAll()
        {
            return _packedRepo.GetAll();
        }

        public M_Package GetBy(int id)
        {
            return _packedRepo.GetById(id);
        }

        public int GetID()
        {
            return _packedRepo.GetKey(d => d.Id);
        }

        public string Insert(M_Package packed)
        {
            try
            {
                _packedRepo.Add(packed);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(M_Package packed)
        {
            try
            {
                _packedRepo.Update(packed);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
    }
}
