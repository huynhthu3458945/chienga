﻿using GCSOFT.MVC.Model.Approval;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.ApprovalService.Interface
{
    public interface IApprovalInfoService
    {
        IEnumerable<M_ApprovalInfo> FindAll();
        IEnumerable<M_ApprovalInfo> FindAll(ApprovalType type);
        IEnumerable<M_ApprovalInfo> FindAll(string approvalNo);
        M_ApprovalInfo GetBy(ApprovalType type, string code);
        string Insert(M_ApprovalInfo approvalInfo);
        string Update(M_ApprovalInfo approvalInfo);
        string Delete(ApprovalType type, string code);
    }
}
