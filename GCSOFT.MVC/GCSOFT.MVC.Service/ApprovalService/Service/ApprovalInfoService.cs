﻿using GCSOFT.MVC.Data.Repositories.ApprovalRepositories.Interface;
using GCSOFT.MVC.Model.Approval;
using GCSOFT.MVC.Service.ApprovalService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.ApprovalService.Service
{
    public class ApprovalInfoService : IApprovalInfoService
    {
        private readonly IApprovalInfoRepository _approvalInfoRepo;
        public ApprovalInfoService
            (
                IApprovalInfoRepository approvalInfoRepo
            )
        {
            _approvalInfoRepo = approvalInfoRepo;
        }

        public IEnumerable<M_ApprovalInfo> FindAll()
        {
            return _approvalInfoRepo.GetAll();
        }

        public IEnumerable<M_ApprovalInfo> FindAll(ApprovalType type)
        {
            return _approvalInfoRepo.GetMany(d => d.Type == type);
        }
        public IEnumerable<M_ApprovalInfo> FindAll(string approvalNo)
        {
            return _approvalInfoRepo.GetMany(d => d.ApprovalNo.Equals(approvalNo));
        }
        public M_ApprovalInfo GetBy(ApprovalType type, string code)
        {
            return _approvalInfoRepo.Get(d => d.Type == type && d.Code.Equals(code));
        }

        public string Insert(M_ApprovalInfo approvalInfo)
        {
            try
            {
                _approvalInfoRepo.Add(approvalInfo);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(M_ApprovalInfo approvalInfo)
        {
            try
            {
                _approvalInfoRepo.Update(approvalInfo);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public string Delete(ApprovalType type, string code)
        {
            try
            {
                _approvalInfoRepo.Delete(d => d.Type == type && d.Code.Equals(code));
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
    }
}
