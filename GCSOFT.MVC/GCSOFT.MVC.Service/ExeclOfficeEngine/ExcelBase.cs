﻿//####################################################################
//# Copyright (C) 2010-2011,  JSC.  All Rights Reserved. 
//#
//# History:
//#     Date Time       Updater         Comment
//#     30/08/2021      Huỳnh Thử      Tạo mới
//####################################################################

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace GCSOFT.MVC.Service.ExeclOfficeEngine
{
    public class ExcelBase
    {
        #region ---- Consts & Variables ----

        #region ---- Default Parameters Const----

        public const string DIVISIONID = "DivisionID";
        public const string ADDRESS = "Address";
        public const string TELEPHONE = "Telephone";
        public const string FAX = "Fax";
        public const string VATNO = "VATNo";
        public const string LANGUAGE = "Language";

        public const string DATEFORMAT = "DateFormat";
        public const string FONTSIZE = "FontSize";

        public const string EXCHANGERATEFORMAT = "ExchangeRateFormat";
        public const string EXCHANGEDECIMAL = "ExchangeDecimal";
        public const string QUANTITYFORMAT = "QuantityFormat";
        public const string QUANTITYDECIMAL = "QuantityDecimal";
        public const string ORIGINALFORMAT = "OriginalFormat";
        public const string ORIGINALDECIMAL = "OriginalDecimal";
        public const string CONVERTEDFORMAT = "ConvertedFormat";
        public const string CONVERTEDDECIMAL = "ConvertedDecimal";
        public const string OTHERFORMAT = "OtherFormat";
        public const string PRICEFORMAT = "PriceFormat";

        public const string SUPPRESSIFZEROQ = "SuppressIfZeroQ";
        public const string SUPPRESSIFZEROP = "SuppressIfZeroP";
        public const string SUPPRESSIFZEROO = "SuppressIfZeroO";
        public const string SUPPRESSIFZEROR = "SuppressIfZeroR";
        public const string SUPPRESSIFZEROC = "SuppressIfZeroC";
        public const string SUPPRESSIFZEROE = "SuppressIfZeroE";

        public const string DEFAULTTEMPFOLDER = "~/Content/Temp";
        public const string MODULETEMPLATEFOLDERFORMAT = "Report";
        //public const string MODULETEMPLATEFOLDERFORMAT = "~/Areas/{0}/Content/Report";
        public const string MODULETEMPLATEIMPORT = "~/Areas/{0}/Content/Template";

        #endregion ---- Default Parameters Const----

        #region ---- DataMaker Formatting Code Const ----

        /// <summary>
        /// DataMaker: Page number
        /// </summary>
        /// <remarks>
        /// How to use: [datamaker].
        /// The value of datamaker would be current documents page number.
        /// </remarks>
        public const string PAGENUMBER = "&P";

        /// <summary>
        /// DataMaker: Total pages
        /// </summary>
        /// <remarks>
        /// How to use: [datamaker].
        /// The value of datamaker would be documents total pages.
        /// </remarks>
        public const string TOTALPAGE = "&N";

        /// <summary>
        /// DataMaker: Current sheets name
        /// </summary>
        /// <remarks>
        /// How to use: [datamaker].
        /// The value of datamaker would be current sheets name.
        /// </remarks>
        public const string CURRENTSHEETNAME = "&A";

        /// <summary>
        /// DataMaker: Current documents name
        /// </summary>
        /// <remarks>
        /// How to use: [datamaker].
        /// The value of datamaker would be current documents name.
        /// </remarks>
        public const string DOCUMENTNAME = "&F";

        /// <summary>
        /// DataMaker: Current time
        /// </summary>
        /// <remarks>
        /// How to use: [datamaker].
        /// The value of datamaker would be current systems time.
        /// </remarks>
        public const string CURRENTTIME = "&T";

        /// <summary>
        /// DataMaker: Current date
        /// </summary>
        /// <remarks>
        /// How to use: [datamaker].
        /// The value of datamaker would be current systems date.
        /// </remarks>
        public const string CURRENTDATE = "&D";

        /// <summary>
        /// DataMaker: Format text. Double underline
        /// </summary>
        /// <remarks>
        /// How to use: [datamaker] %%=Model.Properties [datamaker].
        /// The value of properties would be in double underline style.
        /// </remarks>
        public const string DOUBLEUNDERLINE = "&E";

        /// <summary>
        /// DataMaker: Underline
        /// </summary>
        /// <remarks>
        /// How to use: [datamaker] %%=Model.Properties [datamaker].
        /// The value of properties would be in underline style.
        /// </remarks>
        public const string UNDERLINE = "&U";

        /// <summary>
        /// DataMaker: Italic.
        /// </summary>
        /// <remarks>
        /// How to use: [datamaker] %%=Model.Properties [datamaker].
        /// The value of properties would be in italic style.
        /// </remarks>
        public const string ITALIC = "&I";

        /// <summary>
        /// DataMaker: Bold.
        /// </summary>
        /// <remarks>
        /// How to use: [datamaker] %%=Model.Properties [datamaker].
        /// The value of properties would be in bold style.
        /// </remarks>
        public const string BOLD = "&B";

        /// <summary>
        /// DataMaker: Supper Script
        /// References: en.wikipedia.org/wiki/Subscript_and_superscript
        /// </summary>
        /// <remarks>
        /// How to use: Text [datamaker] %%=Model.Properties.
        /// The value of properties would be at top right.
        /// </remarks>
        public const string SUPERSCRIPT = "&X";

        /// <summary>
        /// DataMaker: Script
        /// References: en.wikipedia.org/wiki/Subscript_and_superscript
        /// </summary>
        /// <remarks>
        /// How to use: Text [datamaker] %%=Model.Properties.
        /// The value of properties would be at bottom right.
        /// </remarks>
        public const string SUBSCRIPT = "&Y";

        /// <summary>
        /// DataMaker: Strike through
        /// References: en.wikipedia.org/wiki/Strikethrough
        /// </summary>
        ///  <remarks>
        /// How to use: Text [datamaker] %%=Model.Properties [datamaker].
        /// The value of properties would be in strike through style.
        /// </remarks>
        public const string STRIKETHROUGH = "&S";

        #endregion ---- Formatting Code ----

        #region ---- Private properties ----

        private static string _DefaultFolder = DEFAULTTEMPFOLDER;

        #endregion ---- Private properties ----

        #region ---- Public properties ----

        // Public variables
        public static string DivisionID { get; set; }
        public static string Address { get; set; }
        public static string Telephone { get; set; }
        public static string Fax { get; set; }
        public static string VATNo { get; set; }
        public static string Language { get; set; }

        public static string DateFormat { get; set; }
        public static int? FontSize { get; set; }

        public static string ExchangeRateFormat { get; set; }
        public static int? ExchangeDecimal { get; set; }
        public static string ConvertedFormat { get; set; }
        public static int? ConvertedDecimal { get; set; }
        public static string OriginalFormat { get; set; }
        public static int? OriginalDecimal { get; set; }
        public static string QuantityFormat { get; set; }
        public static int? QuantityDecimal { get; set; }
        public static string OtherFormat { get; set; }
        public static int? PriceFormat { get; set; }

        public static byte? SuppressIfZeroC { get; set; }
        public static byte? SuppressIfZeroE { get; set; }
        public static byte? SuppressIfZeroQ { get; set; }
        public static byte? SuppressIfZeroP { get; set; }
        public static byte? SuppressIfZeroO { get; set; }
        public static byte? SuppressIfZeroR { get; set; }


        /// <summary>
        /// Get or set default temp folder [default: ~/Content/Temp]
        /// </summary>
        public static string DefaultFolder
        {
            get { return _DefaultFolder; }
            set { _DefaultFolder = value; }
        }

        public static List<KeyValuePair<string, object>> ExcelItems { get; set; }

        public static List<KeyValuePair<string, object>> ColumnsList { get; set; }

        public static List<KeyValuePair<string, string>> GroupFieldList { get; set; }

        public static List<KeyValuePair<string, Stream>> ImageList { get; set; }

        public static List<KeyValuePair<string, byte[]>> ImagePicture { get; set; }

        #endregion ---- Public properties ----

        #endregion ---- Consts & Variables ----

        #region ---- Public Methods ----

        #region ---- Add Parameters ----

        /// <summary>
        /// Initialize
        /// </summary>
        public static void InitParameter()
        {
            ExcelItems = new List<KeyValuePair<string, object>>();
        }

        /// <summary>
        /// Add default parameters
        /// </summary>
        public static void MergeWithDefaultParameters()
        {
            // Company infos
            AddInParameter(DIVISIONID, DivisionID ?? string.Empty);
            AddInParameter(ADDRESS, Address ?? string.Empty);
            AddInParameter(TELEPHONE, Telephone ?? string.Empty);
            AddInParameter(FAX, Fax ?? string.Empty);
            AddInParameter(VATNO, VATNo ?? string.Empty);

            // Language
            AddInParameter(LANGUAGE, Language ?? string.Empty);

            // Suppress
            AddInParameter(SUPPRESSIFZEROP, SuppressIfZeroP ?? 0);
            AddInParameter(SUPPRESSIFZEROO, SuppressIfZeroO ?? 0);
            AddInParameter(SUPPRESSIFZEROR, SuppressIfZeroR ?? 0);
            AddInParameter(SUPPRESSIFZEROC, SuppressIfZeroC ?? 0);
            AddInParameter(SUPPRESSIFZEROQ, SuppressIfZeroQ ?? 0);
            AddInParameter(SUPPRESSIFZEROE, SuppressIfZeroE ?? 0);

            // Style
            AddInParameter(DATEFORMAT, DateFormat ?? string.Empty);
            AddInParameter(FONTSIZE, FontSize ?? 10);

            // Format
            AddInParameter(EXCHANGERATEFORMAT, ExchangeRateFormat ?? string.Empty);
            AddInParameter(EXCHANGEDECIMAL, ExchangeDecimal ?? 0);
            AddInParameter(QUANTITYFORMAT, QuantityFormat ?? string.Empty);
            AddInParameter(QUANTITYDECIMAL, QuantityDecimal ?? 0);
            AddInParameter(ORIGINALFORMAT, OriginalFormat ?? string.Empty);
            AddInParameter(ORIGINALDECIMAL, OriginalDecimal ?? 0);
            AddInParameter(CONVERTEDFORMAT, ConvertedFormat ?? string.Empty);
            AddInParameter(CONVERTEDDECIMAL, ConvertedDecimal ?? 0);
            AddInParameter(PRICEFORMAT, PriceFormat ?? 0);
            AddInParameter(OTHERFORMAT, OtherFormat ?? string.Empty);
        }

        /// <summary>
        /// Add in parameter for report
        /// </summary>
        /// <param name="parameterName">Object Name</param>
        /// <param name="value">Object Value</param>
        public static void AddInParameter(string parameterName, object value)
        {
            if (ExcelItems == null)
            {
                ExcelItems = new List<KeyValuePair<string, object>>();
            }
            var pair = ExcelItems.FirstOrDefault(n => n.Key.Equals(parameterName));
            ExcelItems.Remove(pair);
            ExcelItems.Add(new KeyValuePair<string, object>(parameterName, value));
        }

        /// <summary>
        /// Add in image
        /// </summary>
        /// <param name="parameterName">Object Name</param>
        /// <param name="value">Object Value</param>
        public static void AddInImages(string parameterName, Stream value)
        {
            if (ImageList == null)
            {
                ImageList = new List<KeyValuePair<string, Stream>>();
            }

            var Exist = ImageList.FindAll(m => m.Key.Equals(parameterName));
            // Add item to references
            if (Exist.Count == 0)
            {
                ImageList.Add(new KeyValuePair<string, Stream>(parameterName, value));
            }
        }

        /// <summary>
        /// Add in image
        /// </summary>
        /// <param name="parameterName">Object Name</param>
        /// <param name="value">Object Value</param>
        public static void AddInImagesPicture(string parameterName, byte[] value)
        {
            if (ImagePicture == null)
            {
                ImagePicture = new List<KeyValuePair<string, byte[]>>();
            }
            var Exist = ImagePicture.FindAll(m => m.Key.Equals(parameterName));
            // Add item to references
            if (Exist.Count == 0)
            {
                ImagePicture.Add(new KeyValuePair<string, byte[]>(parameterName, value));
            }
        }

        /// <summary>
        /// Add columns
        /// </summary>
        /// <param name="parameterName">Object Name</param>
        /// <param name="value">Object Value</param>
        public static void AddColumns(string columnName, object value)
        {
            if (ColumnsList == null)
            {
                ColumnsList = new List<KeyValuePair<string, object>>();
            }

            // Add item to references
            if (!ColumnsList.Contains(new KeyValuePair<string, object>(columnName, value)))
            {
                ColumnsList.Add(new KeyValuePair<string, object>(columnName, value));
            }
        }

        /// <summary>
        /// Add Group Field
        /// </summary>
        /// <param name="parameterName">string ID</param>
        /// <param name="value">string Name</param>
        public static void AddGroupField(string fieldID, string fieldName)
        {
            if (GroupFieldList == null)
            {
                GroupFieldList = new List<KeyValuePair<string, string>>();
            }

            // Add item to references
            if (!GroupFieldList.Contains(new KeyValuePair<string, string>(fieldID, fieldName)))
            {
                GroupFieldList.Add(new KeyValuePair<string, string>(fieldID, fieldName));
            }
        }

        #endregion ---- Add Parameters ----

        #region ---- Create Temp Path ----

        /// <summary>
        /// Tạo excel path mặc định
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static string GetExcelTempPath(string filePath = null)
        {
            return filePath ?? GetDefaultTempPath(OfficeExtension.XLSX);
        }

        /// <summary>
        /// Tạo pdf path mặc định
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static string GetPDFTempPath(string filePath = null)
        {
            return filePath ?? GetDefaultTempPath(OfficeExtension.PDF);
        }

        /// <summary>
        /// Thiết lập đường dẫn mặc định khi không khai báo
        /// </summary>
        /// <param name="extentions"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static string GetDefaultTempPath(string extentions, string fileName = null)
        {
            // Folder mặc định
            string directory = string.Format("{0}", DefaultFolder);

            // Tạo folder nếu không tồn tại
            if (!Directory.Exists(HttpContext.Current.Server.MapPath(directory)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(directory));
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                return HttpContext.Current.Server.MapPath(
                    string.Format("{0}/{1}.{2}", directory, fileName, extentions));
            }

            return HttpContext.Current.Server.MapPath(
                string.Format("{0}/{1}.{2}", directory, Guid.NewGuid(), extentions));
        }

        /// <summary>
        /// Return full path
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static string GetFullPath(string fileName)
        {
            // Folder mặc định
            string directory = string.Format("{0}", DefaultFolder);

            // Tạo folder nếu không tồn tại
            if (!Directory.Exists(HttpContext.Current.Server.MapPath(directory)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(directory));
            }

            return HttpContext.Current.Server.MapPath(string.Format("{0}/{1}", directory, fileName));
        }

        /// <summary>
        /// Return file path
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="extensions"></param>
        /// <returns></returns>
        public static string GetModuleTemplatePath(string fileName, string extensions, bool isTemplate = false)
        {
            string moduldeTemplateFolder = isTemplate ? string.Format(MODULETEMPLATEIMPORT)
                : string.Format(MODULETEMPLATEFOLDERFORMAT);

            return
                HttpContext.Current.Server.MapPath(string.Format("{0}/{1}.{2}", moduldeTemplateFolder, fileName,
                                                                 extensions));
        }


        public static string GetModuleTemplatePath(string fileName, string extensions, string Module, bool isTemplate = false)
        {
            string moduldeTemplateFolder = isTemplate ? string.Format(MODULETEMPLATEIMPORT, Module)
                : string.Format(MODULETEMPLATEFOLDERFORMAT, Module);

            return
                HttpContext.Current.Server.MapPath(string.Format("{0}/{1}.{2}", moduldeTemplateFolder, fileName,
                                                                 extensions));
        }

        #endregion ---- Create Temp Path ----

        #endregion ---- Public Methods ----

    }
}
