﻿//####################################################################
//# Copyright (C) 2010-2011,  JSC.  All Rights Reserved. 
//#
//# History:
//#     Date Time       Updater         Comment
//#     30/08/2021      Huỳnh Thử      Tạo mới
//####################################################################

using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.Service.ExeclOfficeEngine
{
    public class ExcelConvert : ExcelBase
    {
        #region ---- Const ----

        public const string DEFAULTSHEETNAME = "Report";
        public const string DEFAULTPIVOTTABLENAME = "PivotTable";

        #endregion ---- Const ----

        #region ---- Public properties ----
        /// <summary>
        /// Get or Set default sheet name [default: Report]
        /// </summary>
        public static string DefaultSheetName { get; set; }

        /// <summary>
        /// Get or set default pivot table name [default: PivotTable]
        /// </summary>
        public static string DefaultPivotTableName { get; set; }

        /// <summary>
        /// Set true if using default sheet index [default: true | 0].
        /// Set false if using default sheet name.
        /// </summary>
        public static bool UsingSheetIndex { get; set; }

        /// <summary>
        /// Convert first sheet only [default: false]
        /// </summary>
        public static bool SheetFirstOrDefault { get; set; }

        /// <summary>
        /// Get or set call action open after publishing completed [default: false]
        /// </summary>
        public static bool OpenAfterPublish { get; set; }

        /// <summary>
        /// Get or set document properties [default: true]
        /// </summary>
        public static bool IncludeDocumentProperties { get; set; }

        /// <summary>
        /// Get or set ignore print areas [default: false]
        /// </summary>
        public static bool IgnorePrintAreas { get; set; }

        /// <summary>
        /// Get or set save changes document [default: true]
        /// </summary>
        public static bool SaveChanges { get; set; }

        /// <summary>
        /// Default PivotTable [Sheet: Report, PivotTableName: PivotTable] as contant
        /// </summary>
        public static List<KeyValuePair<string, string>> PivotTables { get; set; }

        #endregion ---- Public properties ----

        #region ---- Public methods ----

        /// <summary>
        /// Default init
        /// </summary>
        public static void InitDefaultParameter()
        {
            DefaultSheetName = DEFAULTSHEETNAME;
            DefaultPivotTableName = DEFAULTPIVOTTABLENAME;
            UsingSheetIndex = true;
            SheetFirstOrDefault = false;
            OpenAfterPublish = false;
            IncludeDocumentProperties = true;
            IgnorePrintAreas = false;
            SaveChanges = true;
            PivotTables = null;
        }

        /// <summary>
        /// Add pivot table info for current workbook
        /// </summary>
        /// <param name="sheetName"></param>
        /// <param name="pivotTableName"></param>
        public static void AddPivotTable(string sheetName, string pivotTableName)
        {
            if (PivotTables == null)
            {
                PivotTables = new List<KeyValuePair<string, string>>();
            }

            PivotTables.Add(new KeyValuePair<string, string>(sheetName, pivotTableName));
        }

        /// <summary>
        /// Convert stream to file
        /// </summary>
        /// <param name="streamIO"></param>
        /// <returns></returns>
        public static string SaveStreamToFile(Stream streamIO)
        {
            var excelSourcePath = GetExcelTempPath();

            try
            {
                Excel.SaveAs(streamIO, excelSourcePath, SheetFirstOrDefault,
                                  UsingSheetIndex);

                return excelSourcePath;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }

        /// <summary>
        /// Save stream to file with custom sheets index.
        /// First or default force override
        /// </summary>
        /// <param name="streamIO"></param>
        /// <param name="activeSheets"> </param>
        /// <returns></returns>
        public static string SaveStreamToFile(Stream streamIO, params int[] activeSheets)
        {
            var excelSourcePath = GetExcelTempPath();

            try
            {
                Excel.SaveAs(streamIO, excelSourcePath, SheetFirstOrDefault,
                                  UsingSheetIndex, activeSheets);

                return excelSourcePath;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }

        /// <summary>
        /// Save stream to file with custom sheets name.
        /// First or default force override
        /// </summary>
        /// <param name="streamIO"></param>
        /// <param name="activeSheets"> </param>
        /// <returns></returns>
        public static string SaveStreamToFile(Stream streamIO, params string[] activeSheets)
        {
            var excelSourcePath = GetExcelTempPath();

            try
            {
                Excel.SaveAs(streamIO, excelSourcePath, SheetFirstOrDefault,
                                  UsingSheetIndex, activeSheets);

                return excelSourcePath;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }

        /// <summary>
        /// Convert stream sang PDF
        /// </summary>
        /// <param name="streamIO"></param>
        /// <param name="excelSourcePath"></param>
        /// <param name="pdfDestinationPath"></param>
        /// <returns></returns>
        public static Stream ConvertToPDF(Stream streamIO, string excelSourcePath = null, string pdfDestinationPath = null)
        {
            try
            {
                var file = SaveStreamToFile(streamIO);

                // Convert stream PDF
                return ConvertToPDF(file);
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }

        }

        /// <summary>
        /// Convert stream sang PDF
        /// </summary>
        /// <param name="streamIO"></param>
        /// <param name="excelSourcePath"></param>
        /// <param name="pdfDestinationPath"></param>
        /// <param name="activeSheets"></param>
        /// <returns></returns>
        public static Stream ConvertToPDF(Stream streamIO, string excelSourcePath = null, string pdfDestinationPath = null,
            params int[] activeSheets)
        {
            try
            {
                var file = SaveStreamToFile(streamIO, activeSheets);

                // Convert stream PDF
                return ConvertToPDF(file);
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }

        }

        /// <summary>
        /// Convert stream sang PDF
        /// </summary>
        /// <param name="streamIO"></param>
        /// <param name="excelSourcePath"></param>
        /// <param name="pdfDestinationPath"></param>
        /// <param name="activeSheets"></param>
        /// <returns></returns>
        public static Stream ConvertToPDF(Stream streamIO, string excelSourcePath = null, string pdfDestinationPath = null,
            params string[] activeSheets)
        {
            try
            {
                var file = SaveStreamToFile(streamIO, activeSheets);

                // Convert stream PDF
                return ConvertToPDF(file);
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }

        }

        /// <summary>
        /// Convert excel to PDF file stream
        /// </summary>
        /// <param name="excelSourcePath"></param>
        /// <param name="pdfDestinationPath"></param>
        /// <returns></returns>
        public static Stream ConvertToPDF(string excelSourcePath = null, string pdfDestinationPath = null)
        {
            excelSourcePath = GetExcelTempPath(excelSourcePath);
            pdfDestinationPath = GetPDFTempPath(pdfDestinationPath);

            var excelApplication = new Application();
            Workbook excelWorkBook = null;
            var paramMissing = Type.Missing;
            const XlFixedFormatType paramExportFormat = XlFixedFormatType.xlTypePDF;
            const XlFixedFormatQuality paramExportQuality = XlFixedFormatQuality.xlQualityStandard;
            var paramOpenAfterPublish = OpenAfterPublish;
            var paramIncludeDocProps = IncludeDocumentProperties;
            var paramIgnorePrintAreas = IgnorePrintAreas;
            var paramSaveChanges = SaveChanges;
            var paramFromPage = Type.Missing;
            var paramToPage = Type.Missing;
            Stream ms = new MemoryStream();

            try
            {
                // Open the source workbook.
                excelWorkBook = excelApplication.Workbooks.Open(excelSourcePath,
                    paramMissing, paramMissing, paramMissing, paramMissing,
                    paramMissing, paramMissing, paramMissing, paramMissing,
                    paramMissing, paramMissing, paramMissing, paramMissing,
                    paramMissing, paramMissing);

                if (PivotTables != null)
                {
                    // Prevent alert dialog
                    excelApplication.DisplayAlerts = false;

                    // Alway save change for Pivot
                    paramSaveChanges = true;

                    foreach (var keyValuePair in PivotTables)
                    {
                        // Force change data pivottable
                        var pivotWorkSheet = (Worksheet)excelWorkBook.Sheets[keyValuePair.Key];
                        var pivot = (PivotTable)pivotWorkSheet.PivotTables(keyValuePair.Value);
                        pivot.RefreshTable();
                    }
                }

                // Save it in the target format.
                if (excelWorkBook != null)
                    excelWorkBook.ExportAsFixedFormat(paramExportFormat,
                        pdfDestinationPath, paramExportQuality,
                        paramIncludeDocProps, paramIgnorePrintAreas, paramFromPage,
                        paramToPage, paramOpenAfterPublish,
                        paramMissing);

                if (System.IO.File.Exists(pdfDestinationPath))
                {
                    ms = File.Open(pdfDestinationPath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                }

                return ms;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
            finally
            {
                PivotTables = null;

                // Close the workbook object.
                if (excelWorkBook != null)
                {
                    excelWorkBook.Close(paramSaveChanges, paramMissing, paramMissing);
                    excelWorkBook = null;
                }

                // Quit Excel and release the ApplicationClass object.
                excelApplication.Quit();
                excelApplication = null;

                //Xóa file temp
                DeleteTempFile();

                GC.Collect();
                GC.WaitForPendingFinalizers();

            }

        }


        /// <summary>
        /// Convert excel to PDF file.
        /// Return saved filename;
        /// </summary>
        /// <param name="excelSourcePath"></param>
        /// <param name="pdfDestinationPath"></param>
        /// <returns></returns>
        public static void ConvertToPDFAsFile(string excelSourcePath = null, string pdfDestinationPath = null)
        {
            excelSourcePath = GetExcelTempPath(excelSourcePath);
            pdfDestinationPath = GetPDFTempPath(pdfDestinationPath);

            var excelApplication = new Application();
            Workbook excelWorkBook = null;
            var paramMissing = Type.Missing;
            const XlFixedFormatType paramExportFormat = XlFixedFormatType.xlTypePDF;
            const XlFixedFormatQuality paramExportQuality = XlFixedFormatQuality.xlQualityStandard;
            var paramOpenAfterPublish = OpenAfterPublish;
            var paramIncludeDocProps = IncludeDocumentProperties;
            var paramIgnorePrintAreas = IgnorePrintAreas;
            var paramSaveChanges = SaveChanges;
            var paramFromPage = Type.Missing;
            var paramToPage = Type.Missing;

            try
            {
                // Open the source workbook.
                excelWorkBook = excelApplication.Workbooks.Open(excelSourcePath,
                    paramMissing, paramMissing, paramMissing, paramMissing,
                    paramMissing, paramMissing, paramMissing, paramMissing,
                    paramMissing, paramMissing, paramMissing, paramMissing,
                    paramMissing, paramMissing);

                if (PivotTables != null)
                {
                    // Prevent alert dialog
                    excelApplication.DisplayAlerts = false;

                    // Alway save change for Pivot
                    paramSaveChanges = true;

                    foreach (var keyValuePair in PivotTables)
                    {
                        // Force change data pivottable
                        var pivotWorkSheet = (Worksheet)excelWorkBook.Sheets[keyValuePair.Key];
                        var pivot = (PivotTable)pivotWorkSheet.PivotTables(keyValuePair.Value);
                        pivot.RefreshTable();
                    }
                }

                // Save it in the target format.
                if (excelWorkBook != null)
                    excelWorkBook.ExportAsFixedFormat(paramExportFormat,
                        pdfDestinationPath, paramExportQuality,
                        paramIncludeDocProps, paramIgnorePrintAreas, paramFromPage,
                        paramToPage, paramOpenAfterPublish,
                        paramMissing);
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
            finally
            {
                PivotTables = null;

                // Close the workbook object.
                if (excelWorkBook != null)
                {
                    excelWorkBook.Close(paramSaveChanges, paramMissing, paramMissing);
                    excelWorkBook = null;
                }

                // Quit Excel and release the ApplicationClass object.
                excelApplication.Quit();
                excelApplication = null;

                //Xóa file temp
                DeleteTempFile();

                GC.Collect();
                GC.WaitForPendingFinalizers();
            }

        }

        /// <summary>
        /// Refresh dữ liệu của các PivotTable trong excel
        /// </summary>
        /// <param name="streamIO"></param>
        /// <param name="excelSourcePath"></param>
        /// <returns></returns>
        public static Stream RefreshPivotTableData(Stream streamIO, string excelSourcePath = null)
        {
            try
            {
                // Save stream to file
                var file = SaveStreamToFile(streamIO);

                // Refresh Pivot Table
                RefreshPivotTableData(file);

                // Read to memory
                return Excel.ReadFile(file);
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }


        /// <summary>
        /// Refresh dữ liệu của các Pivot Table trong excel
        /// </summary>
        /// <param name="excelSourcePath"></param>
        public static void RefreshPivotTableData(string excelSourcePath = null)
        {
            var excelApplication = new Application();
            Workbook excelWorkBook = null;
            var paramMissing = Type.Missing;
            var paramSaveChanges = SaveChanges;

            excelSourcePath = GetExcelTempPath(excelSourcePath);

            try
            {
                // Open the source workbook.
                excelWorkBook = excelApplication.Workbooks.Open(excelSourcePath,
                    paramMissing, paramMissing, paramMissing, paramMissing,
                    paramMissing, paramMissing, paramMissing, paramMissing,
                    paramMissing, paramMissing, paramMissing, paramMissing,
                    paramMissing, paramMissing);

                if (PivotTables != null)
                {
                    // Prevent alert dialog
                    // Auto say yes for contents overriding
                    excelApplication.DisplayAlerts = false;

                    // Alway save change
                    paramSaveChanges = true;

                    foreach (var keyValuePair in PivotTables)
                    {
                        // Force change data pivottable
                        var pivotWorkSheet = (Worksheet)excelWorkBook.Sheets[keyValuePair.Key];
                        var pivot = (PivotTable)pivotWorkSheet.PivotTables(keyValuePair.Value);
                        pivot.RefreshTable();
                    }
                }
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
            finally
            {
                PivotTables = null;

                // Close the workbook object.
                if (excelWorkBook != null)
                {
                    excelWorkBook.Close(paramSaveChanges, paramMissing, paramMissing);
                    excelWorkBook = null;
                }

                // Quit Excel and release the ApplicationClass object.
                excelApplication.Quit();
                excelApplication = null;

                //Xóa file temp
                DeleteTempFile();

                GC.Collect();
                GC.WaitForPendingFinalizers();

            }

        }

        /// <summary>
        /// Convert stream excel to pdf and save file
        /// </summary>
        /// <param name="excelFilePath"></param>
        /// <returns></returns>
        public static Stream ConvertToPDFXls(string excelFilePath)
        {
            excelFilePath = GetPDFTempPath(excelFilePath);
            try
            {
                // Convert to stream PDF
                Stream stream = ConvertToPDF(excelFilePath);

                // Return Stream
                return stream;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }

        }

        /// <summary>
        /// Xóa các file temp với thời gian là 2 ngày
        /// </summary>
        private static void DeleteTempFile()
        {
            try
            {
                //Lấy các file trong thư mục temp
                var folderPath = HttpContext.Current.Server.MapPath(String.Format("~/Content/Temp/{0}"));

                if (System.IO.Directory.Exists(folderPath))
                {
                    var files = System.IO.Directory.GetFiles(folderPath);

                    //Thời gian xóa file - 2 ngày
                    var timeSpan = new System.TimeSpan(2, 0, 0, 0);

                    //Duyệt file và xóa file
                    foreach (var file in files)
                    {
                        if (System.IO.File.Exists(file))
                        {
                            var createDate = System.IO.File.GetCreationTime(file);
                            var timeFile = DateTime.Now - createDate;
                            if (timeFile.Days >= timeSpan.Days)
                            {
                                System.IO.File.Delete(file);
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }

        }

        #endregion ---- Public methods ----

    }
}
