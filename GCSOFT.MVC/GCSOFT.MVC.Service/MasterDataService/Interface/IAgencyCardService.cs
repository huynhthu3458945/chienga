﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface IAgencyCardService
    {
        IEnumerable<M_AgencyAndCard> FindAll();
        IEnumerable<M_AgencyAndCard> FindAll(int AgencyId);
        IEnumerable<M_AgencyAndCard> FindAll(int AgencyId, string statusCode, string seriNumner);
        IEnumerable<M_AgencyAndCard> FindAll(int AgencyId, string statusCode, string seriNumner, string levelCode);
        IEnumerable<M_AgencyAndCard> FindAll(int AgencyId, int start, int offset);
        IEnumerable<M_AgencyAndCard> FindAll(int AgencyId, string statusCode, string seriNumner, int start, int offset);
        IEnumerable<M_AgencyAndCard> FindAll(int AgencyId, string statusCode, string seriNumner, string levelCode, int start, int offset);
        IEnumerable<M_AgencyAndCard> FindAll(int AgencyId, string statusCode, int offset);
        IEnumerable<M_AgencyAndCard> GetNewCards(int AgencyId, string levelCode, string statusCode, int offset);
        IEnumerable<M_AgencyAndCard> FindAll(int AgencyId, string lotNumber);
        M_AgencyAndCard GetBy(int AgencyId, string SerialNumber);
        M_AgencyAndCard GetByStatus(string SerialNumber, string statusCode);
        M_AgencyAndCard GetBy(int AgencyId, string SerialNumber, string statusCode);
        M_AgencyAndCard GetBy(int AgencyId, long id);
        M_AgencyAndCard GetByStatus(int AgencyId, string statusCode);
        M_AgencyAndCard GetByStatus(int AgencyId, string statusCode, string levelCode);
        string Insert(M_AgencyAndCard agencyCard);
        string Insert(IEnumerable<M_AgencyAndCard> agencyCards);
        string Update(M_AgencyAndCard agencyCard);
        string Update(IEnumerable<M_AgencyAndCard> agencyCards);
        int TotalCard(int AgencyId, List<string> statusCodes);
        int TotalCard(int AgencyId, List<string> statusCodes, string levelCode);
        string Delete(int agencyId, List<long> ids);
    }
}
