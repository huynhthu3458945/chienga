﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface ICardEntryService
    {
        IEnumerable<M_CardEntry> FindAll();
        IEnumerable<M_CardEntry> FindAllByLotNumber(string lotNumber);
        IEnumerable<M_CardEntry> FindAllBySeriNumber(string seriNumner);
        IEnumerable<M_CardEntry> FindAllByCardNo(string cardNo);
        M_CardEntry GetBy(string seriNumber);
        string Insert(M_CardEntry cardEntry);
        string Insert(IEnumerable<M_CardEntry> cardEntries);
        string Update(M_CardEntry cardEntry);
        string Update(IEnumerable<M_CardEntry> cardEntries);
        long GetID();
    }
}
