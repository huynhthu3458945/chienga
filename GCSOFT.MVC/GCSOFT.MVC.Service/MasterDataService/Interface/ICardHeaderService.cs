﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface ICardHeaderService
    {
        IEnumerable<M_CardHeader> FindAll();
        IEnumerable<M_CardHeader> FindAll(CardType cardType);
        M_CardHeader GetBy(string code);
        string Insert(M_CardHeader cardHeader);
        string Update(M_CardHeader cardHeader);
        string GetMax();
    }
}
