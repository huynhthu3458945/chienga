﻿using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface ICardLineService
    {
        IEnumerable<M_CardLine> FindAll();
        IEnumerable<M_CardLine> FindAll(string lotNumber);
        IEnumerable<M_CardLine> FindAll(string lotNumber, int start, int offset);
        IEnumerable<M_CardLine> FindAll(string statusCode, int offset);
        IEnumerable<M_CardLine> FindPhone(string phone);
        M_CardLine GetBySerialNumber(string seriNumner);
        M_CardLine GetBySerialNumber(string seriNumner, string status);
        M_CardLine GetByCardNo(string cardNo);
        M_CardLine GetById(long id);
        string Insert(M_CardLine cardLine);
        string Insert(IEnumerable<M_CardLine> cardLines);
        string Update(M_CardLine cardLine);
        string Update(IEnumerable<M_CardLine> cardLines);
        long GetID();
        string CheckActiveCode(string activeCode);
        bool CheckExist(string cardNo);
        M_CardLine GetByIdCard(long idCard);
        bool UpdateCardNo(string cardNo, long Id);
    }
}
