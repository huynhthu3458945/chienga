﻿using GCSOFT.MVC.Model.MasterData;
using System.Collections.Generic;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface ICategoryService
    {
        IEnumerable<E_Category> FindAll();
        IEnumerable<E_Category> FindAll(List<int> categoryChooseds);
        IEnumerable<E_Category> GetParentCategory(int id);
        E_Category GetBy(int id);
        string Insert(E_Category cate);
        string Update(E_Category cate);
        string Delete(List<int> ids);
        int GetKeyCategory();
        int GetSortOrder(int? idParent);
        bool CandAddTextLink(int id, string linkWeb);
        int GetMaxLevel(int IdParentNo);
        string GetSearchName(string name, string url, int id, int? idParentNo, ref int levelCatetory, ref string breadCrumb);
    }
}
