﻿using GCSOFT.MVC.Model.MasterData;
using System.Collections.Generic;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface ICourseContentService
    {
        IEnumerable<E_CourseContent> FindAll(int idCourse);
        IEnumerable<E_CourseContent> FindAll(int idCourse, CourseType courseType);
        IEnumerable<E_CourseContent> FindAll(int idCourse, int lineNo, CourseType courseType);
        E_CourseContent GetBy(int idCourse, int lineNo, CourseType courseType);
        IEnumerable<E_CourseContent> FindAll(int idCourse, int lineNo, CourseType courseType, List<string> lessonTypeList);
        int GetID(int idCourse);
        string Update(E_CourseContent courseContent);
        E_CourseContent GetBy(int idCourse, int lineNo);
        int GetLastLineNo(int courseId);
        string Insert(IEnumerable<E_CourseContent> courseContents);
        string Insert(E_CourseContent courseContent);
        string Updates(IEnumerable<E_CourseContent> courseContents);
        string Delete(int idCourse, int lineNo);
        string Delete(int idCourse, string lessionTypeCode);
        bool HasSortOrder(int idCourse, int lineNo, int sortOrder, CourseType courseType);
    }
    public interface ISubEcourceContentService
    {
        List<E_SubEcourceContent> FindAll(int idCourse, int lineNo);
        string Insert(int idCourse, int lineNo, IEnumerable<E_SubEcourceContent> ecourseContents);
    }
}
