﻿using GCSOFT.MVC.Model.MasterData;
using System.Collections.Generic;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface ICourseService
    {
        IEnumerable<E_Course> FindAll();
        IEnumerable<E_Course> FindAll(int[] ids);
        IEnumerable<E_Course> GetCourseByClass(int classId);
        IEnumerable<E_Course> FindAll(int? classId, string title);
        IEnumerable<E_Course> FindAll(int? classId, string title, bool status, bool IsTNTD);
        E_Course GetBy(int id);
        E_Course GetBy3(int id); 
        E_Course GetBy_2(int id);
        string Insert(E_Course course);
        string Update(E_Course course);
        string Delete(int[] ids);
        string DeleteImg(int id);
        int GetID();
    }
}
