﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface IEbookAccountService
    {
        bool HasPhoneNumber(string phoneNumber);
        IEnumerable<M_EbookAccount> EbookAccounts(string phoneNumber);
        string Update(IEnumerable<M_EbookAccount> ebookAccs);
    }
}
