﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface IFileResourceService
    {
        IEnumerable<M_FileResource> FindAll();
        IEnumerable<M_FileResource> FindAll(ResultType type);
        IEnumerable<M_FileResource> FindAll(ResultType type, int referId);
        IEnumerable<M_FileResource> FindAllByComment(ResultType type, int commentId);
        IEnumerable<M_FileResource> FindAll(ResultType type, List<int> referIds);
        IEnumerable<M_FileResource> FindAllByComment(ResultType type, List<int> commentIds);
        IEnumerable<M_FileResource> FindAll(List<ResultType> types, List<int> referIds);
        IEnumerable<M_FileResource> GetImageNameToDeletes(ResultType type, int referId, List<string> fileNames);
        M_FileResource GetBy(ResultType resultType, int referId, int lineNo);
        string Insert(IEnumerable<M_FileResource> resourses);
        string Update(IEnumerable<M_FileResource> resourses, ResultType type, int referId);
        string Delete(ResultType type, int[] referId);
    }
}
