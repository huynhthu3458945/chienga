﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface IFirebaseService
    {
        IEnumerable<M_Firebase> FindAll();
        IEnumerable<M_Firebase> FindByStatusID(string statusID);
        string Update(M_Firebase m_Firebase);
    }
}
