﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface IHistoryRecall
    {
        string Insert(M_HistoryRecall historyChangeClass);
        List<M_HistoryRecall> GetAll(int id);
    }
}
