﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface IIntroduceStudentService
    {
        IEnumerable<M_IntroduceStudent> FindAll();
        IEnumerable<M_IntroduceStudent> FindUserNameOrFullName(string userName, string fullName);
        M_IntroduceStudent GetById(int id);
        string Insert(M_IntroduceStudent m_ViHat);
        string Update(M_IntroduceStudent m_ViHat);
        string Delete(int id);
    }
}
