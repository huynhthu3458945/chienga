﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface ILanguageService
    {
        IEnumerable<M_Language> FindAll();
        IEnumerable<M_Language> GetMany(int[] codes);
        M_Language GetBy(int id);
        string Insert(M_Language item);
        string Update(M_Language item);
        string Deletes(int[] ids);
        string DeleteImg(int id);
        int GetID();
        bool CanAdd(string code, string xCode);
        M_Language GetDefaultLanguage();
        M_Language GetLanguageBy(string code);
    }
}
