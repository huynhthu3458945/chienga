﻿using GCSOFT.MVC.Model.MasterData;
using System.Collections.Generic;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface IOptionTypeService
    {
        IEnumerable<M_OptionType> FindAll();
        M_OptionType GetBy(int id);
        string Insert(M_OptionType optionType);
        string Update(M_OptionType optionType);
        string Delete(int[] ids);
        int GetID();
    }
}
