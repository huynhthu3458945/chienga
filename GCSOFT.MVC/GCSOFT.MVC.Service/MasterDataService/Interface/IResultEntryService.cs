﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface IResultEntryService
    {
        IEnumerable<M_ResultEntry> FindAll();
        M_ResultEntry GetBy(int id);
        string Insert(M_ResultEntry resultEntry);
        string Update(M_ResultEntry resultEntry);
        string Delete(int[] ids);
        string Delete(ResultType resultType, int referId);
        string Delete(ResultType resultType, int referId, int lineNo);
        string Delete(ResultType resultType, int[] referIds);
    }
}
