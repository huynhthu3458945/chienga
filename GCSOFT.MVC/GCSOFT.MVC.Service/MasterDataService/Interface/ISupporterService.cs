﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface ISupporterService
    {
        IEnumerable<M_Supporter> FindAll();
        M_Supporter GetBy(int id);
        string Insert(M_Supporter supporter);
        string Update(M_Supporter supporter);
        string Delete(int[] ids);
        int GetID();
    }
}
