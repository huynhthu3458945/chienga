﻿using GCSOFT.MVC.Model.MasterData;
using System.Collections.Generic;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface ITeacherService
    {
        IEnumerable<E_Teacher> FindAll();
        IEnumerable<E_Teacher> FindAll(string levelCode, int start, int offset);
        IEnumerable<E_Teacher> FindAll(string levelCode);
        int TotalRecord(string levelCode);
        IEnumerable<E_Teacher> FindAll(List<int> teacherChooseds);
        IEnumerable<E_Teacher> FindAll(int[] ids);
        E_Teacher GetBy(int id);
        string Insert(E_Teacher teacher);
        string Update(E_Teacher teacher);
        string Delete(int[] ids);
        string DeleteImg(int id);
        int GetID();
    }
}
