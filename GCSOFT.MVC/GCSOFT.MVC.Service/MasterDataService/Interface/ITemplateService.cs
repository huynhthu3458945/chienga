﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface ITemplateService
    {
        IEnumerable<M_Template> FindAll();
        M_Template Get(string code);
        string Insert(M_Template template);
        string Update(M_Template template);
        string Delete(List<string> codes);
    }
}
