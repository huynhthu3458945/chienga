﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface IUserInfoService
    {
        IEnumerable<M_UserInfo> FindAll();
        IEnumerable<M_UserInfo> GetByFullNamePhoneEmail(M_UserInfoFilter f);
        int NoOfMemberResiger(string phone);
        bool HasUsernameByPhone(string phone);
        bool hasUsernameByEmail(string email);
        IEnumerable<M_UserInfo> GetUserByPhones(string phone);
        IEnumerable<M_UserInfo> GetUserByEmails(string email);
        M_UserInfo GetById(int id);
        M_UserInfo GetBy(int id);
        M_UserInfo GetBy(string userName);
        M_UserInfo GetBy(string userName, int id);
        M_UserInfo CheckByUsername(string username, string password);
        M_UserInfo CheckByUsernameAndEmail(string username, string email);
        M_UserInfo CheckByUsernameAndPhone(string username, string phone);
        M_UserInfo CheckByEmail(string email, string password);
        M_UserInfo CheckByPhone(string phone, string password);
        M_UserInfo GetByPhone(string phone);
        M_UserInfo GetByPhoneAndSTNHDType(string phone, STNHDType type);
        M_UserInfo GetBySerinumber(string seriNumber);
        M_UserInfo GetByPhone(string phone, int id);
        M_UserInfo GetByEmail(string email);
        M_UserInfo GetByEmail(string email, int id);
        string Insert(M_UserInfo userInfo);
        string Update(M_UserInfo userInfo);
        string Delete(int[] ids);
        int GetId();
        bool CheckBySerinumber(string seriNumber, STNHDType type);
        bool CheckByPhone(string phone, STNHDType type);
        bool CheckByEmail(string phone, STNHDType type);
    }
}
