﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface IViHatDetailService
    {
        IEnumerable<M_ViHatDetail> FindAll();
        M_ViHatDetail GetById(string apk);
        string Insert(M_ViHatDetail m_ViHat);
        string Insert(IEnumerable<M_ViHatDetail> m_ViHat, string apk);
        string Update(M_ViHatDetail m_ViHat);
        string Update(IEnumerable<M_ViHatDetail> m_ViHat, string apk);
        string Delete(IEnumerable<M_ViHatDetail> m_ViHat, string apk);
        string Delete(string apk);
    }
}
