﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class CardHeaderService : ICardHeaderService
    {
        private readonly ICardHeaderRepository _cardHdrRepo;
        public CardHeaderService
            (
                ICardHeaderRepository cardHdrRepo
            )
        {
            _cardHdrRepo = cardHdrRepo;
        }

        public IEnumerable<M_CardHeader> FindAll()
        {
            return _cardHdrRepo.GetAll();
        }
        public IEnumerable<M_CardHeader> FindAll(CardType cardType)
        {
            return _cardHdrRepo.GetMany(d => d.CardType == cardType);
        }
        public M_CardHeader GetBy(string code)
        {
            return _cardHdrRepo.GetById(code);
        }

        public string Insert(M_CardHeader cardHeader)
        {
            try
            {
                _cardHdrRepo.Add(cardHeader);
                return null;
            }
            catch (Exception ex) {
                return ex.ToString();
            }
        }

        public string Update(M_CardHeader cardHeader)
        {
            try
            {
                _cardHdrRepo.Update(cardHeader);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public string GetMax()
        {
            return _cardHdrRepo.GetAll().OrderByDescending(d => d.LotNumber).Select(d => d.LotNumber).FirstOrDefault();
        }
    }
}
