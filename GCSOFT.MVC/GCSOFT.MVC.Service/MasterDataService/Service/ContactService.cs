﻿using System;
using System.Linq;
using System.Collections.Generic;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class ContactService : IContactService
    {
        private readonly IContactRepository _contactRepo;
        public ContactService
            (
                IContactRepository contactRepo
            )
        {
            _contactRepo = contactRepo;
        }
        public int GetID()
        {
            return _contactRepo.GetKey(d => d.Id);
        }
    }
}
