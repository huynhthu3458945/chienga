﻿using System;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System.Collections.Generic;
using System.Linq;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class CourseContentService : ICourseContentService
    {
        private readonly ICourseContentRepository _courseContentRepo;
        public CourseContentService
            (
                ICourseContentRepository courseContentRepo
            )
        {
            _courseContentRepo = courseContentRepo;
        }

        public E_CourseContent GetBy(int idCourse, int lineNo)
        {
            return _courseContentRepo.Get(d => d.IdCourse == idCourse && d.LineNo == lineNo) ?? new E_CourseContent();
        }
        public E_CourseContent GetBy(int idCourse, int lineNo, CourseType courseType)
        {
            return _courseContentRepo.Get(d => d.IdCourse == idCourse && d.LineNo == lineNo && d.CourseType == courseType) ?? new E_CourseContent();
        }

        public int GetID(int idCourse)
        {
            return _courseContentRepo.GetKey(d => d.IdCourse == idCourse, d => d.LineNo);
        }
        public string Update(E_CourseContent courseContent)
        {
            try
            {
                _courseContentRepo.Update(courseContent);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public IEnumerable<E_CourseContent> FindAll(int idCourse)
        {
            return _courseContentRepo.GetMany(d => d.IdCourse == idCourse);
        }
        public IEnumerable<E_CourseContent> FindAll(int idCourse, CourseType courseType)
        {
            return _courseContentRepo.GetMany(d => d.IdCourse == idCourse && d.CourseType == courseType);
        }
        public IEnumerable<E_CourseContent> FindAll(int idCourse, int lineNo, CourseType courseType)
        {
            return _courseContentRepo.GetMany(d => d.IdCourse == idCourse && d.LineNo != lineNo && d.CourseType == courseType);
        }
        public IEnumerable<E_CourseContent> FindAll(int idCourse, int lineNo, CourseType courseType, List<string> lessonTypeList)
        {
            return _courseContentRepo.GetMany(d => d.IdCourse == idCourse && d.LineNo != lineNo && d.CourseType == courseType && lessonTypeList.Contains(d.LessonTypeCode));
        }
        public int GetLastLineNo(int courseId)
        {
            return (_courseContentRepo.GetMany(d => d.IdCourse == courseId).OrderByDescending(d => d.LineNo).FirstOrDefault() ?? new E_CourseContent()).LineNo;
        }

        public string Insert(IEnumerable<E_CourseContent> courseContents)
        {
            try
            {
                _courseContentRepo.Add(courseContents);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public string Insert(E_CourseContent courseContent)
        {
            try
            {
                _courseContentRepo.Add(courseContent);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public string Updates(IEnumerable<E_CourseContent> courseContents)
        {
            try
            {
                _courseContentRepo.Update(courseContents);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public bool HasSortOrder(int idCourse, int lineNo, int sortOrder, CourseType courseType)
        {
            return _courseContentRepo.GetMany(d => d.IdCourse == idCourse && d.LineNo != lineNo && d.SortOrder == sortOrder && d.CourseType == courseType).Any();
        }
        public string Delete(int idCourse, int lineNo)
        {
            try
            {
                _courseContentRepo.Delete(d => d.IdCourse == idCourse && d.LineNo == lineNo);
                return null;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }

        }
        public string Delete(int idCourse, string lessionTypeCode)
        {
            try
            {
                _courseContentRepo.Delete(d => d.IdCourse == idCourse && d.LessonTypeCode.Equals(lessionTypeCode));
                return null;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
    }
    public class SubEcourceContentService : ISubEcourceContentService
    {
        private readonly ISubEcourceContentRepository _subCourseContentRepo;
        public SubEcourceContentService
            (
                ISubEcourceContentRepository subCourseContentRepo
            )
        {
            _subCourseContentRepo = subCourseContentRepo;
        }

        public List<E_SubEcourceContent> FindAll(int idCourse, int lineNo)
        {
            var list = _subCourseContentRepo.GetMany(d => d.IdCourse == idCourse && d.LineNo == lineNo).ToList();
            if (list == null)
                list = new List<E_SubEcourceContent>();
            return list;
        }

        public string Insert(int idCourse, int lineNo, IEnumerable<E_SubEcourceContent> ecourseContents)
        {
            try
            {
                _subCourseContentRepo.Delete(d => d.IdCourse == idCourse && d.LineNo == lineNo);
                _subCourseContentRepo.Add(ecourseContents);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
    }
}