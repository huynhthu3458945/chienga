﻿using System;
using System.Linq;
using System.Collections.Generic;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class CourseService : ICourseService
    {
        private readonly ICourseRepository _courseRepo;
        private readonly ICourseContentRepository _courseContentRepo;
        private readonly ICourseSupportRepository _courseSupportRepo;
        private readonly ICourseCategoryRepository _courseCategroyRepo;
        private readonly ICourseResourceRepository _courseResourceRepo;
        private readonly IResultEntryRepository _resultEntryRepo;
        public CourseService
            (
                ICourseRepository courseRepo
                , ICourseContentRepository courseContentRepo
                , ICourseSupportRepository courseSupportRepo
                , ICourseCategoryRepository courseCategroyRepo
                , ICourseResourceRepository courseResourceRepo
                , IResultEntryRepository resultEntryRepo
            )
        {
            _courseRepo = courseRepo;
            _courseContentRepo = courseContentRepo;
            _courseSupportRepo = courseSupportRepo;
            _courseCategroyRepo = courseCategroyRepo;
            _courseResourceRepo = courseResourceRepo;
            _resultEntryRepo = resultEntryRepo;
        }

        public string Delete(int[] ids)
        {
            try
            {
                _courseRepo.Delete(d => ids.Contains(d.Id));
                _courseContentRepo.Delete(d => ids.Contains(d.IdCourse));
                _courseSupportRepo.Delete(d => ids.Contains(d.IdCourse));
                _courseCategroyRepo.Delete(d => ids.Contains(d.IdCourse));
                _courseResourceRepo.Delete(d => ids.Contains(d.IdCourse));
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public IEnumerable<E_Course> FindAll()
        {
            return _courseRepo.GetAll();
        }
        public IEnumerable<E_Course> FindAll(int? classId, string title)
        {
            return _courseRepo.GetMany(d => (d.ClassId == classId || classId == null) && (string.IsNullOrEmpty(title) || d.Title.Contains(title)));
        }
        public IEnumerable<E_Course> FindAll(int? classId, string title, bool status, bool isTNTD)
        {
            return _courseRepo.GetMany(d => (d.ClassId == classId || classId == null) && (string.IsNullOrEmpty(title) || d.Title.Contains(title)) && (d.Status || d.Status == status) && (d.IsTNTD || d.IsTNTD == isTNTD));
        }
        public IEnumerable<E_Course> FindAll(int[] ids)
        {
            return _courseRepo.GetMany(d => ids.Contains(d.Id));
        }
        public IEnumerable<E_Course> GetCourseByClass(int classId)
        {
            return _courseRepo.GetMany(d => d.ClassId == classId);
        }
        public E_Course GetBy(int id)
        {
            var course = _courseRepo.GetById(id);
            course.CourseContents = _courseContentRepo.GetMany(d => d.IdCourse == id && d.CourseType == CourseType.Mindmap);
            //course.CourseVideos = _courseContentRepo.GetMany(d => d.IdCourse == id && d.CourseType == CourseType.Videos);
            course.CourseSupports = _courseSupportRepo.GetMany(d => d.IdCourse == id);
            course.CourseCategories = _courseCategroyRepo.GetMany(d => d.IdCourse == id);
            course.CourseResources = _courseResourceRepo.GetMany(d => d.IdCourse == id);
            if (course.CourseContents == null)
                course.CourseContents = new List<E_CourseContent>();
            if (course.CourseSupports == null)
                course.CourseSupports = new List<E_CourseSupport>();
            if (course.CourseResources == null)
                course.CourseResources = new List<E_CourseResource>();
            return course;
        }
        public E_Course GetBy3(int id)
        {
            var course = _courseRepo.GetById(id);
            course.CourseSupports = _courseSupportRepo.GetMany(d => d.IdCourse == id);
            course.CourseCategories = _courseCategroyRepo.GetMany(d => d.IdCourse == id);
            course.CourseResources = _courseResourceRepo.GetMany(d => d.IdCourse == id);
            
            if (course.CourseSupports == null)
                course.CourseSupports = new List<E_CourseSupport>();
            if (course.CourseResources == null)
                course.CourseResources = new List<E_CourseResource>();
            return course;
        }
        public E_Course GetBy_2(int id)
        {
            return _courseRepo.GetById(id) ?? new E_Course();
        }
        public int GetID()
        {
            return _courseRepo.GetKey(d => d.Id);
        }

        public string Insert(E_Course course)
        {
            try
            {
                _courseRepo.Add(course);
                //if (course.CourseContents.Count() != 0)
                //    _courseContentRepo.Add(course.CourseContents);
                _courseSupportRepo.Add(course.CourseSupports);
                _courseCategroyRepo.Add(course.CourseCategories);
                _courseResourceRepo.Add(course.CourseResources);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public string DeleteImg(int id)
        {
            try
            {
                var course = _courseRepo.GetById(id);
                course.Image = null;
                _courseRepo.Update(course);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public string Update(E_Course course)
        {
            try
            {
                //-- Update Header
                _courseRepo.Update(course);
                //-- Update Course Content
                //-- Delete Line has been delete on UI
                //var idLineNos = course.CourseContents.Select(d => d.LineNo).ToArray();
                //_courseContentRepo.Delete(d => d.IdCourse == course.Id && !idLineNos.Contains(d.LineNo));

                //_resultEntryRepo.Delete(d => d.Type == ResultType.Cource && d.ReferId == course.Id && !idLineNos.Contains(d.LineNo ?? 0));

                ////-- Get IdLines In Database
                //idLineNos = _courseContentRepo.GetMany(d => d.IdCourse == course.Id).Select(d => d.LineNo).ToArray();
                ////-- Update
                //_courseContentRepo.Update(course.CourseContents.Where(d => idLineNos.Contains(d.LineNo)));
                ////-- Add
                //_courseContentRepo.Add(course.CourseContents.Where(d => !idLineNos.Contains(d.LineNo)));
                //-- Update Course Content End

                //-- Update Course Content
                //-- Delete Line has been delete on UI
                var idLineNos = course.CourseResources.Select(d => d.LineNo).ToArray();
                _courseResourceRepo.Delete(d => d.IdCourse == course.Id && !idLineNos.Contains(d.LineNo));
                //-- Get IdLines In Database
                idLineNos = _courseResourceRepo.GetMany(d => d.IdCourse == course.Id).Select(d => d.LineNo).ToArray();
                //-- Update
                _courseResourceRepo.Update(course.CourseResources.Where(d => idLineNos.Contains(d.LineNo)));
                //-- Add
                _courseResourceRepo.Add(course.CourseResources.Where(d => !idLineNos.Contains(d.LineNo)));
                //-- Update Course Content End

                //-- Update Course Support
                _courseSupportRepo.Delete(d => d.IdCourse == course.Id);
                _courseSupportRepo.Add(course.CourseSupports);
                //-- Update Course Support End
                //-- Update Course Category
                _courseCategroyRepo.Delete(d => d.IdCourse == course.Id);
                _courseCategroyRepo.Add(course.CourseCategories);
                //-- Update Course Category End
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
    }
}
