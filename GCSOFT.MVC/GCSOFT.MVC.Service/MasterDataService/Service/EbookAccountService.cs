﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class EbookAccountService : IEbookAccountService
    {
        private readonly IEbookAccountRepository _ebookAccountRepo;
        public EbookAccountService
            (
                IEbookAccountRepository ebookAccountRepo
            )
        {
            _ebookAccountRepo = ebookAccountRepo;
        }

        public IEnumerable<M_EbookAccount> EbookAccounts(string phoneNumber)
        {
            return _ebookAccountRepo.GetMany(d => d.PhoneNo.Equals(phoneNumber));
        }

        public bool HasPhoneNumber(string phoneNumber)
        {
            return _ebookAccountRepo.GetMany(d => d.PhoneNo.Equals(phoneNumber) && !d.hasEbook).Any();
        }
         
        public string Update(IEnumerable<M_EbookAccount> ebookAccs)
        {
            try
            {
                _ebookAccountRepo.Update(ebookAccs);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
    }
}
