﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class FileResourceService : IFileResourceService
    {
        private readonly IFileResourceRepository _fileResourceRepo;
        public FileResourceService
            (
                IFileResourceRepository fileResourceRepo
            )
        {
            _fileResourceRepo = fileResourceRepo;
        }
        public string Delete(ResultType type, int[] referId)
        {
            try
            {
                _fileResourceRepo.Delete(d => d.Type == type && referId.Contains(d.ReferId));
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public IEnumerable<M_FileResource> FindAll()
        {
            return _fileResourceRepo.GetAll();
        }

        public IEnumerable<M_FileResource> FindAll(ResultType type)
        {
            return _fileResourceRepo.GetMany(d => d.Type == type) ?? new List<M_FileResource>();
        }

        public IEnumerable<M_FileResource> FindAll(ResultType type, int referId)
        {
            return _fileResourceRepo.GetMany(d => d.Type == type && d.ReferId == referId) ?? new List<M_FileResource>();
        }
        public IEnumerable<M_FileResource> FindAllByComment(ResultType type, int commentId)
        {
            return _fileResourceRepo.GetMany(d => d.Type == type && d.CommentId == commentId) ?? new List<M_FileResource>();
        }
        public IEnumerable<M_FileResource> FindAll(List<ResultType> types, List<int> referIds)
        {
            return _fileResourceRepo.GetMany(d => types.Contains(d.Type) && referIds.Contains(d.ReferId)) ?? new List<M_FileResource>();
        }
        public IEnumerable<M_FileResource> FindAllByComment(ResultType type, List<int> commentIds)
        {
            return _fileResourceRepo.GetMany(d => d.Type == type && commentIds.Contains(d.CommentId)) ?? new List<M_FileResource>();
        }
        public IEnumerable<M_FileResource> FindAll(ResultType type, List<int> referIds)
        {
            return _fileResourceRepo.GetMany(d => d.Type == type && referIds.Contains(d.ReferId)) ?? new List<M_FileResource>();
        }
        public IEnumerable<M_FileResource> GetImageNameToDeletes(ResultType type, int referId, List<string> fileNames)
        {
            return _fileResourceRepo.GetMany(d => d.Type == type && d.ReferId == referId && !fileNames.Contains(d.ImageName)) ?? new List<M_FileResource>();
        }
        public M_FileResource GetBy(ResultType resultType, int referId, int lineNo)
        {
            return _fileResourceRepo.Get(d => d.Type == resultType && d.ReferId == referId && d.LineNo == lineNo) ?? new M_FileResource();
        }
        public string Insert(IEnumerable<M_FileResource> resourses)
        {
            try
            {
                _fileResourceRepo.Add(resourses);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(IEnumerable<M_FileResource> resourses, ResultType type, int referId)
        {
            try
            {
                var idLineNos = resourses.Select(d => d.LineNo).ToArray();
                _fileResourceRepo.Delete(d => d.Type == type && d.ReferId == referId && !idLineNos.Contains(d.LineNo));
                idLineNos = _fileResourceRepo.GetMany(d => d.Type == type && d.ReferId == referId).Select(d => d.LineNo).ToArray();
                _fileResourceRepo.Update(resourses.Where(d => idLineNos.Contains(d.LineNo)));
                _fileResourceRepo.Add(resourses.Where(d => !idLineNos.Contains(d.LineNo)));
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
    }
}
