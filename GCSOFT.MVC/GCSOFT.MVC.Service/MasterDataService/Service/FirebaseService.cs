﻿using System.Linq;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System.Collections.Generic;
using System;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class FirebaseService : IFirebaseService
    {
        private readonly IFirebaseRepository _firebaseRepo;
        public FirebaseService
            (
                IFirebaseRepository userRepo
            )
        {
            _firebaseRepo = userRepo;
        }
        public IEnumerable<M_Firebase> FindAll()
        {
            return _firebaseRepo.GetAll();
        }

        public IEnumerable<M_Firebase> FindByStatusID(string statusID)
        {
            return _firebaseRepo.GetMany(z => z.StatusID.Equals(statusID));
        }

        public string Update(M_Firebase m_Firebase)
        {
            try
            {
                _firebaseRepo.Update(m_Firebase);
                return null;
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
