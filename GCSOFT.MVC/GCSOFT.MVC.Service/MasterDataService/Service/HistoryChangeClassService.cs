﻿using System.Linq;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System.Collections.Generic;
using System;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class HistoryChangeClassService : IHistoryChangeClass
    {
        private readonly IHistoryChangeClassRepository _historyChangeClassRepo;
        public HistoryChangeClassService
            (
                IHistoryChangeClassRepository historyChangeClassRepo
            )
        {
            _historyChangeClassRepo = historyChangeClassRepo;
        }

        public IEnumerable<M_HistoryChangeClass> GetByUserName(string userName)
        {
            return _historyChangeClassRepo.GetMany(z => z.UserName.Equals(userName)).ToList();
        }

        public string Insert(M_HistoryChangeClass historyChangeClass)
        {
            try
            {
                _historyChangeClassRepo.Add(historyChangeClass);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
    }
}
