﻿using System.Linq;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System.Collections.Generic;
using System;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class HistoryInvoiceService : IHistoryInvoice
    {
        private readonly IHistoryInvoiceRepository _HistoryInvoiceRepository;
        public HistoryInvoiceService
            (
                IHistoryInvoiceRepository HistoryInvoiceRepository
            )
        {
            _HistoryInvoiceRepository = HistoryInvoiceRepository;
        }

        public List<M_HistoryInvoice> GetAll()
        {
            try
            {
                return _HistoryInvoiceRepository.GetAll().ToList();
            }
            catch (Exception ex) { return new List<M_HistoryInvoice>(); }
        }

            public string Insert(M_HistoryInvoice HistoryInvoice)
        {
            try
            {
                _HistoryInvoiceRepository.Add(HistoryInvoice);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

    }
}
