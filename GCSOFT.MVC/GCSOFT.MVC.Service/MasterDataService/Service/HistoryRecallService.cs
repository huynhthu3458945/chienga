﻿using System.Linq;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System.Collections.Generic;
using System;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class HistoryRecallService : IHistoryRecall
    {
        private readonly IHistoryRecallRepository _historyRecallRepository;
        public HistoryRecallService
            (
                IHistoryRecallRepository historyRecallRepository
            )
        {
            _historyRecallRepository = historyRecallRepository;
        }

        public List<M_HistoryRecall> GetAll(string seri, string cardNo, DateTime dataRecall, string reason)
        {
            try
            {
                return _historyRecallRepository.GetMany(z => z.Serinumber.Contains(seri) || z.CardNo02.Contains(cardNo) || DateTime.Compare(z.DateRecall.Value, dataRecall.Date) == 0 || z.Reason.Contains(reason)).ToList();
            }
            catch (Exception ex) { return new List<M_HistoryRecall>(); }
        }

        public List<M_HistoryRecall> GetAll(string seri, string cardNo, string reason)
        {
            try
            {
                return _historyRecallRepository.GetMany(z => z.Serinumber.Contains(seri) || z.CardNo02.Contains(cardNo) || z.Reason.Contains(reason)).ToList();
            }
            catch (Exception ex) { return new List<M_HistoryRecall>(); }
        }

        public List<M_HistoryRecall> GetAll(int id)
        {
            try
            {
                return _historyRecallRepository.GetAll().Where(z=>z.AgencyId == id).ToList();
            }
            catch (Exception ex) { return new List<M_HistoryRecall>(); }
        }

            public string Insert(M_HistoryRecall historyRecall)
        {
            try
            {
                _historyRecallRepository.Add(historyRecall);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
      
    }
}
