﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class LanguageService : ILanguageService
    {
        private readonly ILanguageRepository _languageRepo;
        public LanguageService
            (
                ILanguageRepository languageRepo
            )
        {
            _languageRepo = languageRepo;
        }

        public string DeleteImg(int id)
        {
            try
            {
                var item = _languageRepo.GetById(id);
                item.ImagePathForWeb = null;
                _languageRepo.Update(item);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Deletes(int[] ids)
        {
            try
            {
                _languageRepo.Delete(d => ids.Contains(d.Id));
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public IEnumerable<M_Language> FindAll()
        {
            return _languageRepo.GetAll().OrderByDescending(d => d.IsDefault);
        }
        public IEnumerable<M_Language> GetMany(int[] codes)
        {
            return _languageRepo.GetMany(d => codes.Contains(d.Id));
        }
        public M_Language GetBy(int id)
        {
            var item = _languageRepo.GetById(id);
            return item;
        }

        public int GetID()
        {
            return _languageRepo.GetKey(d => d.Id);
        }

        public string Insert(M_Language item)
        {
            try
            {
                _languageRepo.Add(item);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(M_Language item)
        {
            try
            {
                _languageRepo.Update(item);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public bool CanAdd(string code, string xCode)
        {
            M_Language language = null;
            //Them moi
            if (string.IsNullOrEmpty(xCode))
                language = _languageRepo.Get(d => d.Code.Equals(code, StringComparison.OrdinalIgnoreCase));
            else //Update
            {
                if (code.Equals(xCode)) //Khong thay doi No
                    return false;
                language = _languageRepo.Get(d => d.Code.Equals(code, StringComparison.OrdinalIgnoreCase));
            }
            if (language != null)
                return true;
            return false;
        }

        public M_Language GetDefaultLanguage()
        {
            var defaultLang = _languageRepo.GetAll().OrderByDescending(d => d.IsDefault);
            if (defaultLang == null)
                return new M_Language();
            return defaultLang.FirstOrDefault();
        }

        public M_Language GetLanguageBy(string code)
        {
            var language = _languageRepo.Get(d => d.Code.Equals(code, StringComparison.OrdinalIgnoreCase));
            if (language == null)
                return new M_Language();
            return language;
        }
    }
}
