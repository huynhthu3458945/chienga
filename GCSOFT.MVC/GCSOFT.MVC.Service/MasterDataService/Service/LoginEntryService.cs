﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class LoginEntryService : ILoginEntryService
    {
        private readonly ILoginEntryRepository _loginEntryRepo;
        public LoginEntryService
            (
                ILoginEntryRepository loginEntryRepo
            )
        {
            _loginEntryRepo = loginEntryRepo;
        }

        public IEnumerable<M_LoginEntry> FindAll()
        {
            return _loginEntryRepo.GetAll();
        }

        public IEnumerable<M_LoginEntry> FindAll(string userName)
        {
            return _loginEntryRepo.GetMany(d => d.userName.Equals(userName, StringComparison.OrdinalIgnoreCase));
        }

        public M_LoginEntry GetBy(int id)
        {
            return _loginEntryRepo.GetById(id);
        }

        public string Insert(M_LoginEntry loginEntry)
        {
            try
            {
                _loginEntryRepo.Add(loginEntry);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public bool IsYourLoginStillTrue(string userName, string sid)
        {
            return _loginEntryRepo.GetMany(d => d.LoggedIn && d.userName.Equals(userName, StringComparison.OrdinalIgnoreCase) && d.SessionId.Equals(sid)).Any();
        }
        public bool IsUserLoggedOnElsewhere(string userName, string sid)
        {
            return _loginEntryRepo.GetMany(d => d.LoggedIn && d.userName.Equals(userName, StringComparison.OrdinalIgnoreCase) && !d.SessionId.Equals(sid)).Any();
        }

        public string LogEveryoneElseOut(string userName, string sid)
        {
            try
            {
                var outLogins = _loginEntryRepo.GetMany(d => d.LoggedIn && d.userName.Equals(userName, StringComparison.OrdinalIgnoreCase) && !d.SessionId.Equals(sid));
                outLogins.ToList().ForEach(item => item.LoggedIn = false);
                _loginEntryRepo.Update(outLogins);
                return null;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public string Update(M_LoginEntry loginEntry)
        {
            try
            {
                _loginEntryRepo.Update(loginEntry);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
    }
}
