﻿using System.Linq;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System.Collections.Generic;
using System;
using GCSOFT.MVC.Data.Infrastructure;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class MessageMasterService : IMessageMasterService
    {
        private readonly IMessageMasterRepository _messageMasterRepository;
        private readonly IUnitOfWork _unitOfWork;
        public MessageMasterService
            (
                IMessageMasterRepository messageMasterRepository
                , IUnitOfWork unitOfWork
            )
        {
            _messageMasterRepository = messageMasterRepository;
            _unitOfWork = unitOfWork;
        }

        public string Delete(string apk)
        {
            try
            {
                _messageMasterRepository.Delete(z => z.APK == apk);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public IEnumerable<M_MessageMaster> FindAll()
        {
            return _messageMasterRepository.GetAll();
        }
        public M_MessageMaster GetByAPK(string apk)
        {
            return _messageMasterRepository.GetMany(z => z.APK == apk).FirstOrDefault();
        }
        public string Insert(M_MessageMaster m_MessageMaster)
        {
            try
            {
                _messageMasterRepository.Add(m_MessageMaster);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(M_MessageMaster m_MessageMaster)
        {
            try
            {
                _messageMasterRepository.Update(m_MessageMaster);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }
    }
}
