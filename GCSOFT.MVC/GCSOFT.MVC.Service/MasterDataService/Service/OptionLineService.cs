﻿using System;
using System.Collections.Generic;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System.Linq;
namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class OptionLineService : IOptionLineService
    {
        private readonly IOptionLineRepository _optionLineRepo;
        public OptionLineService
            (
                IOptionLineRepository optionLineRepo
            )
        {
            _optionLineRepo = optionLineRepo;
        }

        public int GetID()
        {
            return _optionLineRepo.GetKey(d => d.LineNo);
        }
        public IEnumerable<M_OptionLine> FindAll(int OptionHeaderID)
        {
            return _optionLineRepo.GetMany(d => d.OptionHeaderID == OptionHeaderID && d.IsHidden).OrderBy(d => d.Order);
        }
        public IEnumerable<M_OptionLine> FindAll(string OptionHeaderCode)
        {
            return _optionLineRepo.GetMany(d => d.OptionHeaderCode == OptionHeaderCode && d.IsHidden).OrderBy(d => d.Order);
        }
        public IEnumerable<M_OptionLine> FindAll(string OptionHeaderCode, List<string> statusCodes)
        {
            return _optionLineRepo.GetMany(d => d.OptionHeaderCode == OptionHeaderCode && statusCodes.Contains(d.Code));
        }
        public M_OptionLine Get(int OptionHeaderID, int lineNo)
        {
            return _optionLineRepo.Get(d => d.OptionHeaderID == OptionHeaderID && d.LineNo == lineNo);
        }
        public M_OptionLine Get(string OptionHeaderCode, string code)
        {
            return _optionLineRepo.Get(d => d.OptionHeaderCode == OptionHeaderCode && d.Code == code);
        }
        public IEnumerable<M_OptionLine> Gets(string OptionHeaderCode, string[] codes)
        {
            return _optionLineRepo.GetMany(d => d.OptionHeaderCode == OptionHeaderCode && codes.Contains(d.Code));
        }
        public string Delete(int lineNo)
        {
            try
            {
                _optionLineRepo.Delete(d => d.LineNo == lineNo);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }


        public string Insert(M_OptionLine optionLine)
        {
            try
            {
                _optionLineRepo.Add(optionLine);
                return null;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public string Update(M_OptionLine optionLine)
        {
            try
            {
                //-- Update
                var model = _optionLineRepo.Get(d => d.OptionHeaderCode == optionLine.OptionHeaderCode && d.Code == optionLine.Code);
                model.Code = optionLine.Code;
                model.Name = optionLine.Name;
                _optionLineRepo.Update(model);
               
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
    }
}
