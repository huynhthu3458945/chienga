﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class ParentsService : IParentsService
    {
        private readonly IParentsRepository _parentsRepo;
        private readonly IContactRepository _contactRepo;
        private readonly IChildrenRepository _childrenRepo;
        public ParentsService
            (
                IParentsRepository ClassRepo
                , IContactRepository contactRepo
                , IChildrenRepository childrenRepo
            )
        {
            _parentsRepo = ClassRepo;
            _contactRepo = contactRepo;
            _childrenRepo = childrenRepo;
        }

        public string Delete(int[] ids)
        {
            try
            {
                _parentsRepo.Delete(d => ids.Contains(d.Id));
                _contactRepo.Delete(d => ids.Contains(d.ReferId) && d.Type == 0);
                _childrenRepo.Delete(d => ids.Contains(d.ParentsId));
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public IEnumerable<M_Parents> FindAll()
        {
            return _parentsRepo.GetAll();
        }
        public IEnumerable<M_Parents> FindAll(int[] ids)
        {
            return _parentsRepo.GetMany(d => ids.Contains(d.Id));
        }
        public M_Parents GetBy(int id)
        {
            var parentInfo = _parentsRepo.GetById(id) ?? new M_Parents();
            parentInfo.Contacts = _contactRepo.GetMany(d => d.ReferId == id && d.Type == 0) ?? new List<M_Contact>();
            parentInfo.Childrens = _childrenRepo.GetMany(d => d.ParentsId == id) ?? new List<M_Children>();
            return parentInfo;
        }

        public int GetID()
        {
            return _parentsRepo.GetKey(d => d.Id);
        }

        public string Insert(M_Parents parents)
        {
            try
            {
                _parentsRepo.Add(parents);
                _contactRepo.Add(parents.Contacts);
                _childrenRepo.Add(parents.Childrens);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        
        public string Update(M_Parents parents)
        {
            try
            {
                _parentsRepo.Update(parents);

                _contactRepo.Delete(d => d.ReferId == parents.Id && d.Type == 0);
                _contactRepo.Add(parents.Contacts);

                _childrenRepo.Delete(d => d.ParentsId == parents.Id);
                _childrenRepo.Add(parents.Childrens);

                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
    }
}
