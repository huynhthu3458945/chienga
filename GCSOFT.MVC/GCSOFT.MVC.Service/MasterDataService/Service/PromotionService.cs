﻿// #################################################################
// # Copyright (C) 2021-2022, Tâm Trí Lực JSC.  All Rights Reserved.                       
// #
// # History：                                                                        
// #	Date Time	    Updated		    Content                	
// #    25/08/2021	    Huỳnh Thử 		Update
// ##################################################################

using System.Linq;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System.Collections.Generic;
using System;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class PromotionService : IPromotionService
    {
        private readonly IPromotionRepository _promotionRepo;
        public PromotionService
            (
                IPromotionRepository promotionRepo
            )
        {
            _promotionRepo = promotionRepo;
        }

        public IEnumerable<M_Promotion> GetAll()
        {
            return _promotionRepo.GetAll();
        }

        public M_Promotion GetByCode(string code)
        {
            return _promotionRepo.Get(z => z.Code.Equals(code));
        }

        public string Insert(M_Promotion promotion)
        {
            try
            {
                _promotionRepo.Add(promotion);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public string Update(M_Promotion promotion)
        {
            try
            {
                _promotionRepo.Update(promotion);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public string Remove(string[] codes)
        {
            try
            {
                foreach (var code in codes)
                {
                    _promotionRepo.Delete(d => d.Code.Equals(code));
                }
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
    }
}
