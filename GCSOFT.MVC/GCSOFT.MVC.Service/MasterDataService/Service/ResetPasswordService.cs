﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class ResetPasswordService : IResetPasswordService
    {
        private readonly IResetPasswordRepository _resetPasswordRepo;
        public ResetPasswordService
            (
                IResetPasswordRepository resetPasswordRepo
            )
        {
            _resetPasswordRepo = resetPasswordRepo;
        }

        public IEnumerable<M_ResetPassword> FindAll()
        {
            return _resetPasswordRepo.GetAll();
        }

        public M_ResetPassword GetBy(Guid id)
        {
            return _resetPasswordRepo.GetById(id);
        }

        public M_ResetPassword GetByExpiry(Guid id)
        {
            return _resetPasswordRepo.Get(d => d.Id.Equals(id) && DateTime.Now <= d.ExpiryDate) ?? new M_ResetPassword();
        }

        public string Insert(M_ResetPassword resetPass)
        {
            try
            {
                _resetPasswordRepo.Add(resetPass);
                return null;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public string Update(M_ResetPassword resetPass)
        {
            try
            {
                _resetPasswordRepo.Update(resetPass);
                return null;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
    }
}
