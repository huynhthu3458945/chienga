﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class SetupService : ISetupService
    {
        private readonly ISetupRepository _setupRepo;
        public SetupService
            (
                ISetupRepository setupRepo
            )
        {
            _setupRepo = setupRepo;
        }

        public M_Setup Get()
        {
            return _setupRepo.GetAll().FirstOrDefault();
        }
        public string Update(M_Setup _setup)
        {
            try
            {
                _setupRepo.Update(_setup);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
    }
}
