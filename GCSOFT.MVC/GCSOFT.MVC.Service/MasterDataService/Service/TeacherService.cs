﻿using System;
using System.Linq;
using System.Collections.Generic;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class TeacherService : ITeacherService
    {
        private readonly ITeacherRepository _teacherRepo;
        public TeacherService
            (
                ITeacherRepository teacherRepo
            )
        {
            _teacherRepo = teacherRepo;
        }

        public string Delete(int[] ids)
        {
            try
            {
                _teacherRepo.Delete(d => ids.Contains(d.Id));
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public IEnumerable<E_Teacher> FindAll()
        {
            return _teacherRepo.GetAll();
        }
        public IEnumerable<E_Teacher> FindAll(List<int> teacherChooseds)
        {
            return _teacherRepo.GetMany(d => !teacherChooseds.Contains(d.Id));
        }
        public IEnumerable<E_Teacher> FindAll(int[] ids)
        {
            return _teacherRepo.GetMany(d => ids.Contains(d.Id));
        }
        public E_Teacher GetBy(int id)
        {
            return _teacherRepo.GetById(id);
        }

        public int GetID()
        {
            return _teacherRepo.GetKey(d => d.Id);
        }

        public string Insert(E_Teacher teacher)
        {
            try
            {
                _teacherRepo.Add(teacher);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public string DeleteImg(int id)
        {
            try
            {
                var couser = _teacherRepo.GetById(id);
                couser.Image = null;
                _teacherRepo.Update(couser);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public string Update(E_Teacher teacher)
        {
            try
            {
                _teacherRepo.Update(teacher);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public IEnumerable<E_Teacher> FindAll(string levelCode)
        {
            return _teacherRepo.GetMany(d => d.LevelCode.Equals(levelCode));
        }
        public int TotalRecord(string levelCode)
        {
            if (string.IsNullOrEmpty(levelCode))
                return _teacherRepo.GetMany(d => !string.IsNullOrEmpty(d.Image)).Count();
            else
                return _teacherRepo.GetMany(d => !string.IsNullOrEmpty(d.Image) && d.LevelCode.Equals(levelCode)).Count();
        }
        public IEnumerable<E_Teacher> FindAll(string levelCode, int start, int offset)
        {
            if (string.IsNullOrEmpty(levelCode))
                return _teacherRepo.GetMany(d => !string.IsNullOrEmpty(d.Image)).Skip(start).Take(offset);
            else
                return _teacherRepo.GetMany(d => !string.IsNullOrEmpty(d.Image) && d.LevelCode.Equals(levelCode)).Skip(start).Take(offset);
        }
    }
}
