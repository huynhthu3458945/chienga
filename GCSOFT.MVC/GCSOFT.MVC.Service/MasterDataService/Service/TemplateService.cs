﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class TemplateService : ITemplateService
    {
        private readonly ITemplateRepository _templateRepo;
        public TemplateService
            (
                ITemplateRepository templateRepo
            )
        {
            _templateRepo= templateRepo;
        }

        public string Delete(List<string> codes)
        {
            try
            {
                _templateRepo.Delete(d => codes.Contains(d.Code));
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public IEnumerable<M_Template> FindAll()
        {
            return _templateRepo.GetAll();
        }

        public M_Template Get(string code)
        {
            return _templateRepo.Get(d => d.Code.Equals(code));
        }

        public string Insert(M_Template template)
        {
            try
            {
                _templateRepo.Add(template);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(M_Template template)
        {
            try
            {
                _templateRepo.Update(template);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
    }
}
