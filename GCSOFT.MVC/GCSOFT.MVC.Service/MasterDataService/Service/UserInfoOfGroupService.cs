﻿using System.Linq;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System.Collections.Generic;
using System;
using GCSOFT.MVC.Data.Infrastructure;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class UserInfoOfGroupService : IUserInfoOfGroupService
    {
        private readonly IUserInfoOfGroupRepository _userRepo;
        private readonly IUserInfoRepository _userInfoRepo;
        private readonly IUnitOfWork _unitOfWork;
        public UserInfoOfGroupService
            (
                IUserInfoOfGroupRepository userRepo
                , IUserInfoRepository userInfoRepo
                , IUnitOfWork unitOfWork
            )
        {
            _userRepo = userRepo;
            _userInfoRepo = userInfoRepo;
            _unitOfWork = unitOfWork;
        }

        public string Delete(string groupCode, string userName)
        {
            try
            {
                _userRepo.Delete(z=>z.GroupCode.Equals(groupCode) && z.userName.Equals(userName));
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public IEnumerable<M_UserInfoOfGroup> FindAll()
        {
            return _userRepo.GetAll();
        }

        public M_UserInfoOfGroup GetByGroupCodel(string groupCode)
        {
           return _userRepo.Get(z => z.GroupCode.Equals(groupCode));
        }

        public string Insert(M_UserInfoOfGroup userInfo)
        {
            try
            {
                _userRepo.Add(userInfo);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(M_UserInfoOfGroup userInfo)
        {
            try
            {
                _userRepo.Update(userInfo);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(IEnumerable<M_UserInfoOfGroup> nhomUser, string groupCode)
        {
            try
            {
                //-- Delete Line has been delete on UI
                int[] idUsers = nhomUser.Select(d => d.userId).ToArray();
                int[] idUserUpdateRoles = _userRepo.GetMany(d => d.GroupCode == groupCode && !idUsers.Contains(d.userId)).Select(d => d.userId).ToArray();
                var userInfos = _userInfoRepo.GetMany(d => idUserUpdateRoles.Contains(d.Id));
                foreach(var itemUserUpdateRole in userInfos)
                {
                    itemUserUpdateRole.RoleCode = "NONE";
                    itemUserUpdateRole.RoleName = "Không được phép đăng bài";
                    _userInfoRepo.Update(itemUserUpdateRole);
                }
                _userRepo.Delete(d => d.GroupCode == groupCode && !idUsers.Contains(d.userId));
                //--Get id alredy exist in Table Line Of Database
                idUsers = _userRepo.GetMany(d => d.GroupCode.Equals(groupCode)).Select(d => d.userId).ToArray();
                IEnumerable<M_UserInfoOfGroup> addOrUpdateUofGrp;
                //--Get Object End
                //Add
                addOrUpdateUofGrp = nhomUser.Where(d => !idUsers.Contains(d.userId));
                idUserUpdateRoles = nhomUser.Where(d => !idUsers.Contains(d.userId)).Select(d => d.userId).ToArray();
                var user = _userInfoRepo.GetMany(d => idUserUpdateRoles.Contains(d.Id));
                foreach (var item in user)
                {
                    item.RoleCode = "NONE";
                    item.RoleName = "Không được phép đăng bài";
                    _userInfoRepo.Update(item);
                }
                _userRepo.Add(addOrUpdateUofGrp);
                Commit();
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }
    }
}
