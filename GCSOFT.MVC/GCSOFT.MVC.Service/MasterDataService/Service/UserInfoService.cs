﻿using System.Linq;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System.Collections.Generic;
using System;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class UserInfoService : IUserInfoService
    {
        private readonly IUserInfoRepository _userRepo;
        public UserInfoService
            (
                IUserInfoRepository userRepo
            )
        {
            _userRepo = userRepo;
        }

        public string Delete(int[] ids)
        {
            try
            {
                _userRepo.Delete(d => ids.Contains(d.Id));
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public IEnumerable<M_UserInfo> FindAll()
        {
            return _userRepo.GetAll();
        }

        public M_UserInfo GetBy(int id)
        {
            return _userRepo.GetById(id) ?? new M_UserInfo();
        }
        public M_UserInfo GetById(int id)
        {
            return _userRepo.Get(d => d.Id == id) ?? new M_UserInfo();
        }
        public M_UserInfo GetBy(string userName)
        {
            return _userRepo.Get(d => d.userName.Equals(userName, StringComparison.OrdinalIgnoreCase)) ?? new M_UserInfo();
        }
        public int NoOfMemberResiger(string phone)
        {
            try
            {
                return _userRepo.GetMany(d => d.Phone.Equals(phone) && d.STNHDType == STNHDType.Active).Count();
            }
            catch { return 0; }
        }
        public M_UserInfo GetBy(string userName, int id)
        {
            return _userRepo.Get(d => d.userName.Equals(userName, StringComparison.OrdinalIgnoreCase) && d.Id != id) ?? new M_UserInfo();
        }
        public M_UserInfo CheckByUsername(string username, string password)
        {
            return _userRepo.Get(d => d.userName.Equals(username, StringComparison.OrdinalIgnoreCase) && d.Password.Equals(password)) ?? new M_UserInfo();
        }
        public M_UserInfo CheckByUsernameAndPhone(string username, string phone)
        {
            return _userRepo.Get(d => d.userName.Equals(username, StringComparison.OrdinalIgnoreCase) && d.Phone.Equals(phone)) ?? new M_UserInfo();
        }
        public M_UserInfo CheckByUsernameAndEmail(string username, string email)
        {
            return _userRepo.Get(d => d.userName.Equals(username, StringComparison.OrdinalIgnoreCase) && d.Email.Equals(email, StringComparison.OrdinalIgnoreCase)) ?? new M_UserInfo();
        }
        public M_UserInfo CheckByEmail(string email, string password)
        {
            return _userRepo.Get(d => d.Email.Equals(email, StringComparison.OrdinalIgnoreCase) && d.Password.Equals(password)) ?? new M_UserInfo();
        }
        public M_UserInfo CheckByPhone(string phone, string password)
        {
            return _userRepo.Get(d => d.Phone.Equals(phone, StringComparison.OrdinalIgnoreCase) && d.Password.Equals(password)) ?? new M_UserInfo();
        }
        public M_UserInfo GetByEmail(string email)
        {
            return _userRepo.Get(d => d.Email.Equals(email, StringComparison.OrdinalIgnoreCase)) ?? new M_UserInfo();
        }
        public M_UserInfo GetByEmail(string email, int id)
        {
            return _userRepo.Get(d => d.Email.Equals(email, StringComparison.OrdinalIgnoreCase) && d.Id != id) ?? new M_UserInfo();
        }
        public M_UserInfo GetByPhone(string phone, int id)
        {
            return _userRepo.Get(d => d.Phone.Equals(phone, StringComparison.OrdinalIgnoreCase) && d.Id != id) ?? new M_UserInfo();
        }
        public M_UserInfo GetByPhoneAndSTNHDType(string phone, STNHDType type)
        {
            return _userRepo.Get(d => d.Phone.Equals(phone, StringComparison.OrdinalIgnoreCase) && d.STNHDType == type) ?? new M_UserInfo();
        }

        public M_UserInfo GetByPhone(string phone)
        {
            return _userRepo.Get(d => d.Phone.Equals(phone, StringComparison.OrdinalIgnoreCase)) ?? new M_UserInfo();
        }
        public M_UserInfo GetBySerinumber(string seriNumber)
        {
            return _userRepo.Get(d => d.Serinumber.Equals(seriNumber, StringComparison.OrdinalIgnoreCase)) ?? new M_UserInfo();
        }
        public bool CheckBySerinumber(string seriNumber, STNHDType type)
        {
            return _userRepo.GetMany(d => d.Serinumber.Equals(seriNumber) && d.STNHDType == type).Any();
        }
        public bool CheckByPhone(string phone, STNHDType type)
        {
            return _userRepo.GetMany(d => d.Phone.Equals(phone) && d.STNHDType == type).Any();
        }
        public bool CheckByEmail(string email, STNHDType type)
        {
            return _userRepo.GetMany(d => d.Email.Equals(email) && d.STNHDType == type).Any();
        }
        public string Insert(M_UserInfo userInfo)
        {
            try
            {
                _userRepo.Add(userInfo);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(M_UserInfo userInfo)
        {
            try
            {
                _userRepo.Update(userInfo);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public int GetId()
        {
            return _userRepo.GetAll().LastOrDefault().Id;
        }
        public bool HasUsernameByPhone(string phone)
        {
            return _userRepo.GetMany(d => d.Phone.Equals(phone) && d.ActivePhone).Any();
        }
        public bool hasUsernameByEmail(string email)
        {
            return _userRepo.GetMany(d => d.Email.Equals(email, StringComparison.OrdinalIgnoreCase) && d.ActivePhone).Any();
        }
        public IEnumerable<M_UserInfo> GetUserByPhones(string phone)
        {
            return _userRepo.GetMany(d => d.Phone.Equals(phone) && d.ActivePhone);
        }
        public IEnumerable<M_UserInfo> GetUserByEmails(string email)
        {
            return _userRepo.GetMany(d => d.Email.Equals(email, StringComparison.OrdinalIgnoreCase) && d.ActivePhone);
        }

        public IEnumerable<M_UserInfo> GetByFullNamePhoneEmail(M_UserInfoFilter f)
        {
            return _userRepo.GetMany(
                d => (string.IsNullOrEmpty(f.fullName) || (!string.IsNullOrEmpty(f.fullName) && d.fullName.Contains(f.fullName)))
                    && (string.IsNullOrEmpty(f.Phone) || (!string.IsNullOrEmpty(f.Phone) && d.Phone.Contains(f.Phone)))
                    && (string.IsNullOrEmpty(f.Email) || (!string.IsNullOrEmpty(f.Email) && d.Email.Contains(f.Email))));
        }
    }
}
