﻿using System.Linq;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System.Collections.Generic;
using System;
using GCSOFT.MVC.Data.Infrastructure;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class ViHatDetailService : IViHatDetailService
    {
        private readonly IViHatDetailRepository _viHatDetailRepo;
        private readonly IUnitOfWork _unitOfWork;
        public ViHatDetailService
            (
                IViHatDetailRepository userRepo
                , IUnitOfWork unitOfWork
            )
        {
            _viHatDetailRepo = userRepo;
            _unitOfWork = unitOfWork;
        }

        public string Delete(string apk)
        {
            try
            {
                _viHatDetailRepo.Delete(z => z.APKMaster == apk);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public IEnumerable<M_ViHatDetail> FindAll()
        {
            return _viHatDetailRepo.GetAll();
        }
        public M_ViHatDetail GetById(string apk)
        {
            return _viHatDetailRepo.GetMany(z => z.APKMaster == apk).FirstOrDefault();
        }
        public string Insert(M_ViHatDetail m_ViHatDetail)
        {
            try
            {
                _viHatDetailRepo.Add(m_ViHatDetail);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(M_ViHatDetail m_ViHatDetail)
        {
            try
            {
                _viHatDetailRepo.Update(m_ViHatDetail);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public string Insert(IEnumerable<M_ViHatDetail> m_ViHat, string apk)
        {
            _viHatDetailRepo.Delete(z => z.APKMaster == apk);
            foreach (var item in m_ViHat)
            {
                _viHatDetailRepo.Add(item);
            }
            return null;
        }

        public string Update(IEnumerable<M_ViHatDetail> m_ViHat, string apk)
        {
            foreach (var item in m_ViHat)
            {
                _viHatDetailRepo.Add(item);
            }
            Commit();
            return null;
        }
        public string Delete(IEnumerable<M_ViHatDetail> m_ViHat, string apk)
        {
            _viHatDetailRepo.Delete(z => z.APKMaster == apk);
            return null;
        }
    }
}
