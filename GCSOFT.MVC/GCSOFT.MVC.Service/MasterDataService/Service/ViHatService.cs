﻿using System.Linq;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System.Collections.Generic;
using System;
using GCSOFT.MVC.Data.Infrastructure;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class ViHatService : IViHatService
    {
        private readonly IViHatRepository _viHatRepo;
        private readonly IUnitOfWork _unitOfWork;
        public ViHatService
            (
                IViHatRepository userRepo
                , IUnitOfWork unitOfWork
            )
        {
            _viHatRepo = userRepo;
            _unitOfWork = unitOfWork;
        }

        public string Delete(string apk)
        {
            try
            {
                _viHatRepo.Delete(z=>z.APK == apk);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public IEnumerable<M_ViHat> FindAll()
        {
            return _viHatRepo.GetAll();
        }
        public M_ViHat GetByAPK(string apk)
        {
            return _viHatRepo.GetMany(z=>z.APK == apk).FirstOrDefault();
        }
        public string Insert(M_ViHat m_ViHat)
        {
            try
            {
                _viHatRepo.Add(m_ViHat);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(M_ViHat m_ViHat)
        {
            try
            {
                _viHatRepo.Update(m_ViHat);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

       
    }
}
