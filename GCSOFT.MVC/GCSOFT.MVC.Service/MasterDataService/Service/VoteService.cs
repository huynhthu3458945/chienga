﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Service
{
    public class VoteService : IVoteService
    {
        private readonly IVoteRepository _voteRepo;
        public VoteService
            (
                IVoteRepository voteRepo
            )
        {
            _voteRepo = voteRepo;
        }
        public string Delete(int[] ids)
        {
            try
            {
                _voteRepo.Delete(d => ids.Contains(d.Id));
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public string Delete(ResultType resultType, int referId, string userName)
        {
            try
            {
                _voteRepo.Delete(d => d.Type == resultType && d.ReferId == referId && d.UserName.Equals(userName, StringComparison.OrdinalIgnoreCase));
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public IEnumerable<M_Vote> FindAll()
        {
            return _voteRepo.GetAll();
        }
        public IEnumerable<M_Vote> FindAll(ResultType type)
        {
            return _voteRepo.GetMany(d => d.Type == type);
        }
        public IEnumerable<M_Vote> FindAll(ResultType type, int referId)
        {
            return _voteRepo.GetMany(d => d.Type == type && d.ReferId == referId);
        }
        public IEnumerable<M_Vote> FindAll(List<ResultType> types, int referId)
        {
            return _voteRepo.GetMany(d => types.Contains(d.Type) && d.ReferId == referId);
        }
        public M_Vote GetBy(int id)
        {
            return _voteRepo.GetById(id) ?? new M_Vote();
        }
        public int GetID()
        {
            return _voteRepo.GetKey(d => d.Id);
        }
        public int NoOfVote(ResultType type, int referId)
        {
            return _voteRepo.GetMany(d => d.Type == type && d.ReferId == referId).Count();
        }
        public string Insert(M_Vote Class)
        {
            try
            {
                _voteRepo.Add(Class);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public string Update(M_Vote Class)
        {
            try
            {
                _voteRepo.Update(Class);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public bool HasVote(ResultType type, int referId, string userName)
        {
            try
            {
                return (_voteRepo.GetMany(d => d.Type == type && d.ReferId == referId && d.UserName.Equals(userName, StringComparison.OrdinalIgnoreCase)) ?? new List<M_Vote>()).Count() > 0;
            }
            catch { return false; }
        }
    }
}
