﻿using GCSOFT.MVC.Model.NamPhutThuocBai;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.NamPhutThuocBaiService.Interface
{
    public interface IDeThiService
    {
        IEnumerable<TB_DeThi> FindAll();
        TB_DeThi GetBy(int Id);
        TB_DeThi GetBy(string code);
        string Insert(TB_DeThi dethi);
        string Update(TB_DeThi dethi);
        string Delete(int id);
    }
}
