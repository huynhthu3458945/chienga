﻿using GCSOFT.MVC.Model.NamPhutThuocBai;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.NamPhutThuocBaiService.Interface
{
    public interface IMindMapPointService
    {
        IEnumerable<TB_MindMapPoint> FindAll(int IdNopBai);
        string Insert(IEnumerable<TB_MindMapPoint> mindmapPoints);
        string Detele(int idNopBai);
        bool HasPoint(int idNopBai);
    }
}
