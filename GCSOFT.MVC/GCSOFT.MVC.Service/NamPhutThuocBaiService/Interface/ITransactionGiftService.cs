﻿using GCSOFT.MVC.Model.NamPhutThuocBai;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.NamPhutThuocBaiService.Interface
{
    public interface ITransactionGiftService
    {
        IEnumerable<TB_TransactionGift> FindAll();
        IEnumerable<TB_TransactionGift> FindAll(string userName);
        TB_TransactionGift GetBy(string code);
        string Insert(TB_TransactionGift transaction);
        string Update(TB_TransactionGift transaction);
        string Delete(string code);
        string GetMax();
    }
}