﻿using GCSOFT.MVC.Data.Repositories.AgencyRepositories.Interface;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Model.NamPhutThuocBai;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.NamPhutThuocBaiService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.NamPhutThuocBaiService.Service
{
    public class ContestantsService : IContestantsService
    {
        private readonly IContestantsRepository _contestansRepo;
        public ContestantsService
            (
                IContestantsRepository contestansRepo
            )
        {
            _contestansRepo = contestansRepo;
        }

        public IEnumerable<TB_Contestants> FindAll()
        {
            return _contestansRepo.GetAll();
        }
        public IEnumerable<TB_Contestants> FindAll(string phoneNumber)
        {
            return _contestansRepo.GetMany(d => d.Phone == phoneNumber);
        }
        public TB_Contestants GetBy(int id)
        {
            return _contestansRepo.GetById(id);
        }

        public TB_Contestants GetByUserName(string userName)
        {
            return _contestansRepo.Get(d => d.Username.Equals(userName, StringComparison.OrdinalIgnoreCase));
        }
        public TB_Contestants GetByUserName(string userName, string passWord)
        {
            return _contestansRepo.Get(d => d.Username.Equals(userName, StringComparison.OrdinalIgnoreCase) && d.Password.Equals(passWord));
        }
        public TB_Contestants GetByIDNo(string id)
        {
            return _contestansRepo.Get(d => d.IdentificationNumber.Equals(id));
        }
        public string Insert(TB_Contestants contestants)
        {
            try
            {
                _contestansRepo.Add(contestants);
                return null;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public string Update(TB_Contestants contestants)
        {
            try
            {
                _contestansRepo.Update(contestants);
                return null;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public string GetIdentificationNumber()
        {
            try
            {
                var stringNumber = "";
                var maxNumber = _contestansRepo.GetAll().Count() + 1;
                if (maxNumber < 10)
                    stringNumber = string.Format("000{0}", maxNumber);
                else if (maxNumber < 100)
                    stringNumber = string.Format("00{0}", maxNumber);
                else if (maxNumber < 1000)
                    stringNumber = string.Format("0{0}", maxNumber);
                else
                    stringNumber = maxNumber.ToString();
                return stringNumber;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
    }
}
