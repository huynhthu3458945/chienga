﻿using GCSOFT.MVC.Data.Repositories.NamPhutThuocBaiRepositories.Interface;
using GCSOFT.MVC.Model.NamPhutThuocBai;
using GCSOFT.MVC.Service.NamPhutThuocBaiService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.NamPhutThuocBaiService.Service
{
    public class GiftService : IGiftService
    {
        private readonly IGiftRepository _giftRepo;
        public GiftService
            (
                IGiftRepository giftRepo
            )
        {
            _giftRepo = giftRepo;
        }

        public IEnumerable<TB_Gift> FindAll()
        {
            return _giftRepo.GetAll();
        }

        public IEnumerable<TB_Gift> GetAllIsShow()
        {
            return _giftRepo.GetMany(z=>z.IsShow);
        }

        public TB_Gift GetBy(string code)
        {
            return _giftRepo.Get(d => d.Code.Equals(code));
        }
    }
}
