﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.NamPhutThuocBai;
using GCSOFT.MVC.Service.NamPhutThuocBaiService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.NamPhutThuocBaiService.Service
{
    public class MindMapPointService : IMindMapPointService
    {
        private readonly IMindMapPointRepository _mindMapRepo;
        public MindMapPointService
            (
                IMindMapPointRepository mindMapRepo
            )
        {
            _mindMapRepo = mindMapRepo;
        }

        public string Detele(int idNopBai)
        {
            try
            {
                _mindMapRepo.Delete(d => d.IdNopBai == idNopBai);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public IEnumerable<TB_MindMapPoint> FindAll(int IdNopBai)
        {
            return _mindMapRepo.GetMany(d => d.IdNopBai == IdNopBai);
        }

        public string Insert(IEnumerable<TB_MindMapPoint> mindmapPoints)
        {
            try
            {
                _mindMapRepo.Add(mindmapPoints);
                return null;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public bool HasPoint(int idNopBai)
        {
            return _mindMapRepo.GetMany(d => d.IdNopBai == idNopBai).Any();
        }
    }
}
