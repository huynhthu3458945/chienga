﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.NamPhutThuocBai;
using GCSOFT.MVC.Service.NamPhutThuocBaiService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.NamPhutThuocBaiService.Service
{
    public class NopBaiService : INopBaiService
    {
        private readonly INopBaiRepository _nopBaiRepo;
        public NopBaiService
            (
                INopBaiRepository nopBaiRepo
            )
        {
            _nopBaiRepo = nopBaiRepo;
        }

        public IEnumerable<TB_NopBai> FindAll()
        {
            return _nopBaiRepo.GetAll();
        }
        public IEnumerable<TB_NopBai> FindAll(int classId)
        {
            return _nopBaiRepo.GetMany(d => d.ClassId == classId);
        }
        public IEnumerable<TB_NopBai> FindAll(List<string> techerUserNames)
        {
            return _nopBaiRepo.GetMany(d => techerUserNames.Contains(d.UserTrongTaiVong1));
        }
        public IEnumerable<TB_NopBai> FindAllUserName(string userName)
        {
            return _nopBaiRepo.GetMany(d => d.userName.Equals(userName)) ?? new List<TB_NopBai>();
        }
        public TB_NopBai GetBy(int Id)
        {
            return _nopBaiRepo.GetById(Id);
        }
        public TB_NopBai Get(int Id)
        {
            return _nopBaiRepo.Get(d => d.Id == Id);
        }
        public TB_NopBai GetBy(string userName)
        {
            return _nopBaiRepo.Get(d => d.userName.Equals(userName, StringComparison.OrdinalIgnoreCase));
        }
        public bool DaNopBai(string userName)
        {
            return _nopBaiRepo.GetMany(d => d.userName.Equals(userName, StringComparison.OrdinalIgnoreCase)).Any();
        }
        public string Insert(TB_NopBai nopBai)
        {
            try
            {
                _nopBaiRepo.Add(nopBai);
                return null;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public string Update(TB_NopBai nopBai)
        {
            try
            {
                _nopBaiRepo.Update(nopBai);
                return null;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public IEnumerable<TB_NopBai> FindAll(string techerUserName)
        {
            return _nopBaiRepo.GetMany(d => d.UserTrongTaiVong1.Equals(techerUserName, StringComparison.OrdinalIgnoreCase));
        }

        public IEnumerable<TB_NopBai> FindAllChuyenMon1()
        {
            return _nopBaiRepo.GetMany(d => d.IsChuyenMon);
        }

        public IEnumerable<TB_NopBai> FindAllChuyenMon1(string techerUserName)
        {
            return _nopBaiRepo.GetMany(d => d.IsChuyenMon && d.UserTrongTaiChuyenMon1.Equals(techerUserName, StringComparison.OrdinalIgnoreCase));
        }
        public IEnumerable<TB_NopBai> FindAllChuyenMon1(List<string> techerUserNames)
        {
            return _nopBaiRepo.GetMany(d => d.IsChuyenMon && techerUserNames.Contains(d.UserTrongTaiChuyenMon1));
        }

        public IEnumerable<TB_NopBai> FindAllChuyenMon2()
        {
            return _nopBaiRepo.GetMany(d => d.IsChuyenMon);
        }

        public IEnumerable<TB_NopBai> FindAllChuyenMon2(string techerUserName)
        {
            return _nopBaiRepo.GetMany(d => d.IsChuyenMon && d.UserTrongTaiChuyenMon2.Equals(techerUserName, StringComparison.OrdinalIgnoreCase));
        }
        public IEnumerable<TB_NopBai> FindAllChuyenMon2(List<string> techerUserNames)
        {
            return _nopBaiRepo.GetMany(d => d.IsChuyenMon && techerUserNames.Contains(d.UserTrongTaiChuyenMon2));
        }
        public string Update(IEnumerable<TB_NopBai> nopBais)
        {
            try
            {
                _nopBaiRepo.Update(nopBais);
                return null;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
    }
}
