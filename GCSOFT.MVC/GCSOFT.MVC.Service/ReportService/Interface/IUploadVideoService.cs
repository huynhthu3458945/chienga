﻿using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Model.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.AgencyService.Interface
{
    public interface IUploadVideoService
    {
        IEnumerable<R_UploadVideo> FindAll(UploadType type);
        string Insert(R_UploadVideo uploadVideo);
        string Update(R_UploadVideo uploadVideo);
        string Delete(int[] ids);
    }
}
