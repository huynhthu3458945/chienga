﻿using GCSOFT.MVC.Data.Repositories.AgencyRepositories.Interface;
using GCSOFT.MVC.Model.Report;
using GCSOFT.MVC.Service.AgencyService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.ReportService.Service
{
    public class UploadVideoService : IUploadVideoService
    {
        private readonly IUploadVideoRepository _uploadVideoRepo;
        public UploadVideoService
            (
                IUploadVideoRepository uploadVideoRepo
            )
        {
            _uploadVideoRepo = uploadVideoRepo;
        }

        public string Delete(int[] ids)
        {
            try
            {
                _uploadVideoRepo.Delete(d => ids.Contains(d.Id));
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public IEnumerable<R_UploadVideo> FindAll(UploadType type)
        {
            return _uploadVideoRepo.GetMany(d => d.Type == type).OrderByDescending(d => d.DateCreate);
        }

        public string Insert(R_UploadVideo uploadVideo)
        {
            try
            {
                _uploadVideoRepo.Add(uploadVideo);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(R_UploadVideo uploadVideo)
        {
            try
            {
                _uploadVideoRepo.Update(uploadVideo);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
    }
}
