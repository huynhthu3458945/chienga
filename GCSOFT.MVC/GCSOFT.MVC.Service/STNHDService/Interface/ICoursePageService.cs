﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.STNHDService.Interface
{
    public interface ICoursePageService
    {
        E_Course GetCourse(int idCourse);
        E_CourseContent GetCourseContent(int idCourse, int lineNo);
        IEnumerable<E_CourseContent> GetCourseContentList(int idCourse);
        IEnumerable<E_CourseContent> GetCourseContentList(int idCourse, CourseType courseType);
        IEnumerable<E_CourseContent> GetCourseContentList(List<int> idCourses);
        E_CourseContent GetParentTitle(int idCourse, int parentSortOrder, CourseType courseType);
    }
}
