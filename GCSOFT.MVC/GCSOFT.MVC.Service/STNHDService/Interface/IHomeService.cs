﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.STNHDService.Interface
{
    public interface IHomeService
    {
        IEnumerable<M_Question> QuestionList();
    }
}
