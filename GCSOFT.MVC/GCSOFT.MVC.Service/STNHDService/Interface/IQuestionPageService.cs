﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.STNHDService.Interface
{
    public interface IQuestionPageService
    {
        M_Question GetQuestion(int id);
        IEnumerable<M_Question> GetQuestionByCourseContent(int idCourse, int courseLineNo, List<QuestionType> questionTypes, List<string> statusCodes);
        IEnumerable<M_Question> GetQuestionByCourseContent(int idCourse, int courseLineNo, List<QuestionType> questionTypes, List<string> notStatusCodes, string userName);
    }
}
