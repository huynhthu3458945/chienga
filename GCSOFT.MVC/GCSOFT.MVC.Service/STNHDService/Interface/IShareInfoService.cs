﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.STNHDService.Interface
{
    public interface IShareInfoService
    {
        E_Class GetClassBy(int idClass);
        IEnumerable<E_Class> ClassList(ClassType classType, string statusCode);
        IEnumerable<E_Course> CourseList(bool status);
        E_Course GetCourseBy(int id);
        IEnumerable<E_Course> CourseList(bool status, int classId);
        IEnumerable<E_Course> CourseList(bool status, List<int> classIds);
        IEnumerable<M_StudentClass> FindAllByDate(string userName);
        string GetFileInfo(ResultType type, int referId, int lineNo);
        M_UserInfo GetByUsername(string userName);
        bool IsYourLoginStillTrue(string userName, string sid);
        bool IsUserLoggedOnElsewhere(string userName, string sid);
        string LogEveryoneElseOut(string userName, string sid);
        IEnumerable<M_Language> GetAllLanguage();
    }
}
