﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.STNHDService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.STNHDService.Service
{
    public class CoursePageService : ICoursePageService
    {
        private readonly ICourseRepository _courseRepo;
        private readonly ICourseContentRepository _courseContentRepo;
        public CoursePageService
            (
                ICourseRepository courseRepo
                , ICourseContentRepository courseContentRepo
            )
        {
            _courseRepo = courseRepo;
            _courseContentRepo = courseContentRepo;
        }

        public E_Course GetCourse(int idCourse)
        {
            return _courseRepo.GetById(idCourse) ?? new E_Course();
        }

        public E_CourseContent GetCourseContent(int idCourse, int lineNo)
        {
            return _courseContentRepo.Get(d => d.IdCourse == idCourse && d.LineNo == lineNo) ?? new E_CourseContent();
        }
        public IEnumerable<E_CourseContent> GetCourseContentList(int idCourse)
        {
            return _courseContentRepo.GetMany(d => d.IdCourse == idCourse && !d.IsHidden).OrderBy(d => d.SortOrder);
        }
        public IEnumerable<E_CourseContent> GetCourseContentList(int idCourse, CourseType courseType)
        {
            return _courseContentRepo.GetMany(d => d.IdCourse == idCourse && !d.IsHidden && d.CourseType == courseType).OrderBy(d => d.SortOrder);
        }
        public IEnumerable<E_CourseContent> GetCourseContentList(List<int> idCourses)
        {
            return _courseContentRepo.GetMany(d => idCourses.Contains(d.IdCourse)).OrderBy(d => d.SortOrder);
        }
        public E_CourseContent GetParentTitle(int idCourse, int parentSortOrder, CourseType courseType)
        {
            return _courseContentRepo.Get(d => d.IdCourse == idCourse && d.SortOrder == parentSortOrder && d.CourseType == courseType) ?? new E_CourseContent();
        }
    }
}
