﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.STNHDService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.STNHDService.Service
{
    public class HomeService : IHomeService
    { 
        private readonly IQuestionRepository _questionRepo;
        public HomeService
            (
                IQuestionRepository questionRepo
            )
        {
            _questionRepo = questionRepo;
        }
        public IEnumerable<M_Question> QuestionList()
        {
            return _questionRepo.GetMany(d => d.StatusCode.Equals("DTL") || d.StatusCode.Equals("DX"));
        }
    }
}
