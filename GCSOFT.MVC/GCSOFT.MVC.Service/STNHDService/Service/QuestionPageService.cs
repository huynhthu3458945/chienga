﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.STNHDService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.STNHDService.Service
{
    public class QuestionPageService : IQuestionPageService
    {
        private readonly IQuestionRepository _questionRepo;
        public QuestionPageService
            (
                IQuestionRepository questionRepo
            )
        {
            _questionRepo = questionRepo;
        }

        public M_Question GetQuestion(int id)
        {
            return _questionRepo.GetById(id) ?? new M_Question();
        }
        public IEnumerable<M_Question> GetQuestionByCourseContent(int idCourse, int courseLineNo, List<QuestionType> questionTypes, List<string> statusCodes)
        {
            return _questionRepo.GetMany(d => d.CourseId == idCourse && d.CourseContentLineNo == courseLineNo && questionTypes.Contains(d.Type) && statusCodes.Contains(d.StatusCode)) ?? new List<M_Question>();
        }
        public IEnumerable<M_Question> GetQuestionByCourseContent(int idCourse, int courseLineNo, List<QuestionType> questionTypes, List<string> notStatusCodes, string userName)
        {
            return _questionRepo.GetMany(d => d.CourseId == idCourse && d.CourseContentLineNo == courseLineNo && questionTypes.Contains(d.Type) && !notStatusCodes.Contains(d.StatusCode) && d.userName.Equals(userName, StringComparison.OrdinalIgnoreCase)) ?? new List<M_Question>();
        }
    }
}
