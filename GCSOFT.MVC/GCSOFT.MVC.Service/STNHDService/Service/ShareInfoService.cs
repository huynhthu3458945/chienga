﻿using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Data.Repositories.SystemRepositories.Interface;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.STNHDService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.STNHDService.Service
{
    public class ShareInfoService : IShareInfoService
    {
        private readonly IClassRepository _classRepo;
        private readonly ICourseRepository _courseRepo;
        private readonly IFileResourceRepository _fileResourceRepo;
        private readonly IStudentClassRepository _studentClassRepo;
        private readonly IUserInfoRepository _userInfoRepo;
        private readonly ILoginEntryRepository _loginEntryRepo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILanguageRepository _langRepo;
        public ShareInfoService
            (
                IClassRepository ClassRepo
                , ICourseRepository courseRepo
                , IFileResourceRepository fileResourceRepo
                , IStudentClassRepository studentClassRepo
                , IUserInfoRepository userInfoRepo
                , ILoginEntryRepository loginEntryRepo
                , IUnitOfWork unitOfWork
                , ILanguageRepository langRepo
            )
        {
            _classRepo = ClassRepo;
            _courseRepo = courseRepo;
            _fileResourceRepo = fileResourceRepo;
            _studentClassRepo = studentClassRepo;
            _userInfoRepo = userInfoRepo;
            _loginEntryRepo = loginEntryRepo;
            _unitOfWork = unitOfWork;
            _langRepo = langRepo;
        }

        public IEnumerable<E_Class> ClassList(ClassType classType, string statusCode)
        {
            return _classRepo.GetMany(d => d.ClassType == classType && d.StatusCode.Equals(statusCode)).OrderBy(d => d.SortOrder);
        }

        public IEnumerable<E_Course> CourseList(bool status)
        {
            return _courseRepo.GetMany(d => !d.Status).OrderBy(d => d.SortOrder);
        }
        public E_Course GetCourseBy(int id)
        {
            return _courseRepo.GetById(id) ?? new E_Course();
        }
        public IEnumerable<E_Course> CourseList(bool status, int classId)
        {
            return _courseRepo.GetMany(d => !d.Status && d.ClassId == classId).OrderBy(d => d.SortOrder);
        }
        public IEnumerable<E_Course> CourseList(bool status, List<int> classIds)
        {
            return _courseRepo.GetMany(d => !d.Status && classIds.Contains(d.ClassId)).OrderBy(d => d.SortOrder);
        }
        public IEnumerable<M_StudentClass> FindAllByDate(string userName)
        {
            return _studentClassRepo.GetMany(d => d.userName.Equals(userName, StringComparison.OrdinalIgnoreCase) && d.FromDate <= DateTime.Now && d.ToDate >= DateTime.Now);
        }
        public string GetFileInfo(ResultType type, int referId, int lineNo)
        {
            var fileInfo = _fileResourceRepo.Get(d => d.Type == type && d.ReferId == referId && d.LineNo == lineNo) ?? new M_FileResource();
            return fileInfo.FileType == FileType.ImagePath ? fileInfo.ImagePath?.Replace("~", "") : fileInfo.ImageBase64;
        }
        public M_UserInfo GetByUsername(string userName)
        {
            return _userInfoRepo.Get(d => d.userName.Equals(userName, StringComparison.OrdinalIgnoreCase));
        }
        public bool IsYourLoginStillTrue(string userName, string sid)
        {
            return _loginEntryRepo.GetMany(d => d.LoggedIn && d.userName.Equals(userName, StringComparison.OrdinalIgnoreCase) && d.SessionId.Equals(sid)).Any();
        }
        public bool IsUserLoggedOnElsewhere(string userName, string sid)
        {
            return _loginEntryRepo.GetMany(d => d.LoggedIn && d.userName.Equals(userName, StringComparison.OrdinalIgnoreCase) && !d.SessionId.Equals(sid)).Any();
        }

        public string LogEveryoneElseOut(string userName, string sid)
        {
            try
            {
                var outLogins = _loginEntryRepo.GetMany(d => d.LoggedIn && d.userName.Equals(userName, StringComparison.OrdinalIgnoreCase) && !d.SessionId.Equals(sid));
                outLogins.ToList().ForEach(item => item.LoggedIn = false);
                _loginEntryRepo.Update(outLogins);
                _unitOfWork.Commit();
                return null;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public IEnumerable<M_Language> GetAllLanguage()
        {
            return _langRepo.GetAll().OrderByDescending(d => d.IsDefault);
        }
        public E_Class GetClassBy(int idClass)
        {
            return _classRepo.GetById(idClass);
        }
    }
}
