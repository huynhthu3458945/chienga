﻿using GCSOFT.MVC.Model;
using GCSOFT.MVC.Model.StoreProcedue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.StoreProcedure.Interface
{
    public interface IStoreProcedureService
    {
        ProcResult InsertSO(string DocumentNo, string LotNumber, string UserName, string FullName, int FromSource, string SourceNo);
        ProcResult SP_AgencyAcceptOrder(int SalesType, string DocumentNo, string LotNumber, string UserName, string FullName, int FromSource, string SourceNo);
        ProcResult SP_SubAgencyAcceptOrder(int SalesType, string DocumentNo, string UserName, string FullName, int FromSource, string SourceNo, string StatusCardCode);
        ProcResult SP_ReturnOrder(int SalesType, string DocumentNo, string UserName, string FullName);
        IEnumerable<SpCustomerInfo> SP_CustomerInfo(string CardNo, string PhoneNo, string Email, string FullName, string SerialNumber, string Username, string DurationCode);
        IEnumerable<SpUserInfo> SP_UserInfo(string Username, string FullName, string Email, string Phone, string Serinumber, string CardNo);
        IEnumerable<SpGeneralInfo> SP_GeneralInfo(DateTime FromDate, DateTime ToDate, DateTime LastFromDate, DateTime LastToDate);
        IEnumerable<SpTTLBuyCustomerDtl> sp_rpt_TTLBuyCustomerDtl(DateTime FromDate, DateTime ToDate, DateTime LastFromDate, DateTime LastToDate);
        IEnumerable<SpAgencyLevelDtl> sp_rpt_AgencyLevelDtl(DateTime FromDate, DateTime ToDate, DateTime LastFromDate, DateTime LastToDate);
        IEnumerable<SpAgencyinfo> sp_rpt_agencyinfo(DateTime FromDate, DateTime ToDate);
        IEnumerable<ProcResult> sp_ChangeCard(string ConvertCardNo, int AgencyId, string FromStatusCode, string ToStatusCode, int NoOfConvert);
        IEnumerable<SPApprovalList> SP_LoadApprovalList(int ApprovalType, string StatusCode, string ApprovalNo);
        IEnumerable<SpCardSoldByAgency> SP_CardSoldByAgency(int AgencyId, string FullName, string PhoneNo, string Email, string CardNo, string SeriNumber, int Start, int Offset, DateTime FromDate, DateTime ToDate, string Action);
        IEnumerable<SpAgencyDetail> sp_AgencyDetail(DateTime FromDate, DateTime ToDate, int AgencyId);
        IEnumerable<ProcResultInt> SP_CountCardSoldByAgency(int AgencyId, string FullName, string PhoneNo, string Email, string CardNo, string SeriNumber);
        IEnumerable<SpAgencyBuy2Child> sp_AgencyBuy2Child(DateTime FromDate, DateTime ToDate, int AgencyId);
        IEnumerable<EbookUserByCity> sp_rpt_EbookUserByCity(int cityId, int districtId);
        IEnumerable<EbookUserDetailByCity> sp_rpt_EbookUserDetailByCity(int cityId, int districtId);
        IEnumerable<SpCardSoldByAgency> SP_CardSoldByAgencyAcitve(int AgencyId, string FullName, string PhoneNo, string Email, string CardNo, string SeriNumber, int Start, int Offset, DateTime FromDate, DateTime ToDate, string Action);
        IEnumerable<ProcResultInt> SP_CountCardSoldByAgencyActive(int AgencyId, string FullName, string PhoneNo, string Email, string CardNo, string SeriNumber);
        IEnumerable<QuanLyTrongTai> sp_QuanLyTrongTai(string userName);
        IEnumerable<SPBaiThi> SP_BaiThi(string userName);
        IEnumerable<SpCustomerAgencyBuy> SP_CustomerAgencyBuy(int AgencyId, string SeriNumber, string FullName, string PhoneNo, string Email, string LevelCode, DateTime FromDate, DateTime ToDate);
        IEnumerable<AgencyParentList> AgencyParentFindAll(string Action, int AgencyId);
        IEnumerable<AgencyParentInfo> AgencyParentGetInfo(string Action, int AgencyId);
        IEnumerable<AgencyBuyGeneralCount> SP_RPT_AgencyBuyGeneralCount(int AgencyId, DateTime FromDate, DateTime ToDate, int searchAgencyId, string searchPhone, string searchEmail, string GroupBy, string SortBy);
        IEnumerable<AgencyBuyGeneral> SP_RPT_AgencyBuyGeneral(int AgencyId, DateTime FromDate, DateTime ToDate, int searchAgencyId, string searchPhone, string searchEmail, string GroupBy, string SortBy, int Start, int Offset);
        IEnumerable<ReNewCardNo> SP_CardNoDuplicate();
        IEnumerable<SpAgencyDetailSales> SP_RPT_AgencyDetailSales(string Action, int? AgencyId, string ViewBy, DateTime FromDate, DateTime ToDate, int searchAgencyId, string cardNo, string levelcode, string seri, string fullName, string phone, string email, string carStatus, int Start, int Offset);
        IEnumerable<ProcResultInt> SP_RPT_AgencyDetailSalesCount(string Action, int? AgencyId, string ViewBy, DateTime FromDate, DateTime ToDate, int searchAgencyId, string cardNo, string levelcode, string seri, string fullName, string phone, string email, string carStatus, int Start, int Offset);
        IEnumerable<SpCustomerAgencyBuySumary> sp_CustomerAgencyBuySumary(int AgencyId, string SeriNumber, string FullName, string PhoneNo, string Email, string LevelCode, DateTime FromDate, DateTime ToDate);
        IEnumerable<AgencyBuyGeneral> SP_RPT_AgencyBuyGeneralDiffDate(int AgencyId, DateTime FromDateBuy, DateTime ToDateBuy, DateTime FromDateActive, DateTime ToDateActive, int searchAgencyId, string searchPhone, string searchEmail, string GroupBy, string SortBy, int Start, int Offset);
        IEnumerable<SP_SYS_Categroy_Tree> SP_SYS_Categroy_Tree(string Code);
        IEnumerable<SpDataPostBD> SP_DataPostBD(int AgencyId, DateTime FromDate, DateTime ToDate, int searchAgencyId, string searchPhone, string searchEmail);
        void SP_RecallLotnumber(string action, string username, string lotNumber);
        IEnumerable<SP_ListTop> SP_ListTop(string action, string codeDethi, int top);
        IEnumerable<SpRetailCustomer> SP_RPT_RetailCustomer(string Action, string phone, string email, int Start, int Offset);
        IEnumerable<AgencyChildBuyGeneral> SP_RPT_AgencyChildBuyGeneral(int AgencyId, DateTime FromDateBuy, DateTime ToDateBuy, DateTime FromDateActive, DateTime ToDateActive, int searchAgencyId, int Start, int Offset);
    }
}