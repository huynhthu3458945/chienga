﻿using GCSOFT.MVC.Service.StoreProcedure.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.StoreProcedure;
using GCSOFT.MVC.Model;
using GCSOFT.MVC.Model.StoreProcedue;

namespace GCSOFT.MVC.Service.StoreProcedure.Service
{
    public class StoreProcedureService : IStoreProcedureService
    {
        private readonly IStoreProcedureRepository _storeProcedureRepo;
        public StoreProcedureService
            (
                IStoreProcedureRepository storeProcedureRepo
            )
        {
            _storeProcedureRepo = storeProcedureRepo;
        }
        public ProcResult InsertSO(string DocumentNo, string LotNumber, string UserName, string FullName, int FromSource, string SourceNo)
        {
            return _storeProcedureRepo.InsertSO(DocumentNo, LotNumber, UserName, FullName, FromSource, SourceNo).FirstOrDefault() ?? new ProcResult();
        }
        public ProcResult SP_AgencyAcceptOrder(int SalesType, string DocumentNo, string LotNumber, string UserName, string FullName, int FromSource, string SourceNo)
        {
            return _storeProcedureRepo.SP_AgencyAcceptOrder(SalesType, DocumentNo, LotNumber, UserName, FullName, FromSource, SourceNo).FirstOrDefault() ?? new ProcResult();
        }
        public ProcResult SP_SubAgencyAcceptOrder(int SalesType, string DocumentNo, string UserName, string FullName, int FromSource, string SourceNo, string StatusCardCode)
        {
            return _storeProcedureRepo.SP_SubAgencyAcceptOrder(SalesType, DocumentNo, UserName, FullName, FromSource, SourceNo, StatusCardCode).FirstOrDefault() ?? new ProcResult();
        }
        public ProcResult SP_ReturnOrder(int SalesType, string DocumentNo, string UserName, string FullName)
        {
            return _storeProcedureRepo.SP_ReturnOrder(SalesType, DocumentNo, UserName, FullName).FirstOrDefault() ?? new ProcResult();
        }
        public IEnumerable<SpCustomerInfo> SP_CustomerInfo(string CardNo, string PhoneNo, string Email, string FullName, string SerialNumber, string Username, string DurationCode)
        {
            return _storeProcedureRepo.SP_CustomerInfo(CardNo, PhoneNo, Email, FullName, SerialNumber, Username, DurationCode);
        }
        public IEnumerable<SpUserInfo> SP_UserInfo(string Username, string FullName, string Email, string Phone, string Serinumber, string CardNo)
        {
            return _storeProcedureRepo.SP_UserInfo(Username, FullName, Email, Phone, Serinumber, CardNo);
        }
        public IEnumerable<SpGeneralInfo> SP_GeneralInfo(DateTime FromDate, DateTime ToDate, DateTime LastFromDate, DateTime LastToDate)
        {
            return _storeProcedureRepo.SP_GeneralInfo(FromDate, ToDate, LastFromDate, LastToDate);
        }
        public IEnumerable<SpTTLBuyCustomerDtl> sp_rpt_TTLBuyCustomerDtl(DateTime FromDate, DateTime ToDate, DateTime LastFromDate, DateTime LastToDate)
        {
            return _storeProcedureRepo.sp_rpt_TTLBuyCustomerDtl(FromDate, ToDate, LastFromDate, LastToDate);
        }
        public IEnumerable<SpAgencyLevelDtl> sp_rpt_AgencyLevelDtl(DateTime FromDate, DateTime ToDate, DateTime LastFromDate, DateTime LastToDate)
        {
            return _storeProcedureRepo.sp_rpt_AgencyLevelDtl(FromDate, ToDate, LastFromDate, LastToDate);
        }
        public IEnumerable<SpAgencyinfo> sp_rpt_agencyinfo(DateTime FromDate, DateTime ToDate)
        {
            return _storeProcedureRepo.sp_rpt_agencyinfo(FromDate, ToDate);
        }
        public IEnumerable<ProcResult> sp_ChangeCard(string ConvertCardNo, int AgencyId, string FromStatusCode, string ToStatusCode, int NoOfConvert)
        {
            return _storeProcedureRepo.sp_ChangeCard(ConvertCardNo, AgencyId, FromStatusCode, ToStatusCode, NoOfConvert);
        }
        public IEnumerable<SPApprovalList> SP_LoadApprovalList(int ApprovalType, string StatusCode, string ApprovalNo)
        {
            return _storeProcedureRepo.SP_LoadApprovalList(ApprovalType, StatusCode, ApprovalNo);
        }
        public IEnumerable<SpCardSoldByAgency> SP_CardSoldByAgency(int AgencyId, string FullName, string PhoneNo, string Email, string CardNo, string SeriNumber, int Start, int Offset, DateTime FromDate, DateTime ToDate, string Action)
        {
            return _storeProcedureRepo.SP_CardSoldByAgency(AgencyId, FullName, PhoneNo, Email, CardNo, SeriNumber, Start, Offset, FromDate, ToDate, Action);
        }
        public IEnumerable<ProcResultInt> SP_CountCardSoldByAgency(int AgencyId, string FullName, string PhoneNo, string Email, string CardNo, string SeriNumber)
        {
            return _storeProcedureRepo.SP_CountCardSoldByAgency(AgencyId, FullName, PhoneNo, Email, CardNo, SeriNumber);
        }
        public IEnumerable<SpAgencyDetail> sp_AgencyDetail(DateTime FromDate, DateTime ToDate, int AgencyId)
        {
            return _storeProcedureRepo.sp_AgencyDetail(FromDate, ToDate, AgencyId);
        }
        public IEnumerable<SpAgencyBuy2Child> sp_AgencyBuy2Child(DateTime FromDate, DateTime ToDate, int AgencyId)
        {
            return _storeProcedureRepo.sp_AgencyBuy2Child(FromDate, ToDate, AgencyId);
        }
        public IEnumerable<EbookUserByCity> sp_rpt_EbookUserByCity(int cityId, int districtId)
        {
            return _storeProcedureRepo.sp_rpt_EbookUserByCity(cityId, districtId);
        }
        public IEnumerable<EbookUserDetailByCity> sp_rpt_EbookUserDetailByCity(int cityId, int districtId)
        {
            return _storeProcedureRepo.sp_rpt_EbookUserDetailByCity(cityId, districtId);
        }

        public IEnumerable<SpCardSoldByAgency> SP_CardSoldByAgencyAcitve(int AgencyId, string FullName, string PhoneNo, string Email, string CardNo, string SeriNumber, int Start, int Offset, DateTime FromDate, DateTime ToDate, string Action)
        {
            return _storeProcedureRepo.SP_CardSoldByAgencyAcitve(AgencyId, FullName, PhoneNo, Email, CardNo, SeriNumber, Start, Offset, FromDate, ToDate, Action);
        }

        public IEnumerable<ProcResultInt> SP_CountCardSoldByAgencyActive(int AgencyId, string FullName, string PhoneNo, string Email, string CardNo, string SeriNumber)
        {
            return _storeProcedureRepo.SP_CountCardSoldByAgencyActive(AgencyId, FullName, PhoneNo, Email, CardNo, SeriNumber);
        }
        public IEnumerable<QuanLyTrongTai> sp_QuanLyTrongTai(string userName)
        {
            return _storeProcedureRepo.sp_QuanLyTrongTai(userName);
        }
        public IEnumerable<SPBaiThi> SP_BaiThi(string userName)
        {
            return _storeProcedureRepo.SP_BaiThi(userName);
        }
        public IEnumerable<SpCustomerAgencyBuy> SP_CustomerAgencyBuy(int AgencyId, string SeriNumber, string FullName, string PhoneNo, string Email, string LevelCode, DateTime FromDate, DateTime ToDate)
        {
            return _storeProcedureRepo.SP_CustomerAgencyBuy(AgencyId, SeriNumber, FullName, PhoneNo, Email, LevelCode, FromDate, ToDate);
        }
        public IEnumerable<AgencyParentList> AgencyParentFindAll(string Action, int AgencyId)
        {
            return _storeProcedureRepo.AgencyParentFindAll(Action, AgencyId);
        }
        public IEnumerable<AgencyParentInfo> AgencyParentGetInfo(string Action, int AgencyId)
        {
            return _storeProcedureRepo.AgencyParentGetInfo(Action, AgencyId);
        }
        public IEnumerable<AgencyBuyGeneralCount> SP_RPT_AgencyBuyGeneralCount(int AgencyId, DateTime FromDate, DateTime ToDate, int searchAgencyId, string searchPhone, string searchEmail, string GroupBy, string SortBy)
        {
            return _storeProcedureRepo.SP_RPT_AgencyBuyGeneralCount(AgencyId, FromDate, FromDate, searchAgencyId, searchPhone, searchEmail, GroupBy, SortBy);
        }
        public IEnumerable<AgencyBuyGeneral> SP_RPT_AgencyBuyGeneral(int AgencyId, DateTime FromDate, DateTime ToDate, int searchAgencyId, string searchPhone, string searchEmail, string GroupBy, string SortBy, int Start, int Offset)
        {
            return _storeProcedureRepo.SP_RPT_AgencyBuyGeneral(AgencyId, FromDate, ToDate, searchAgencyId, searchPhone, searchEmail, GroupBy, SortBy, Start, Offset);
        }

        public IEnumerable<ReNewCardNo> SP_CardNoDuplicate()
        {
            return _storeProcedureRepo.SP_CardNoDuplicate();
        }

        public IEnumerable<SpAgencyDetailSales> SP_RPT_AgencyDetailSales(string Action, int? AgencyId, string ViewBy, DateTime FromDate, DateTime ToDate, int searchAgencyId, string cardNo, string levelcode, string seri, string fullName, string phone, string email, string carStatus, int Start, int Offset)
        {
            return _storeProcedureRepo.SP_RPT_AgencyDetailSales(Action, AgencyId, ViewBy, FromDate, ToDate, searchAgencyId, cardNo, levelcode, seri, fullName, phone, email, carStatus, Start, Offset) ;
        }
        public IEnumerable<ProcResultInt> SP_RPT_AgencyDetailSalesCount(string Action, int? AgencyId, string ViewBy, DateTime FromDate, DateTime ToDate, int searchAgencyId, string cardNo, string levelcode, string seri, string fullName, string phone, string email, string carStatus, int Start, int Offset)
        {
            return _storeProcedureRepo.SP_RPT_AgencyDetailSalesCount(Action, AgencyId, ViewBy, FromDate, ToDate, searchAgencyId, cardNo, levelcode, seri, fullName, phone, email, carStatus, Start, Offset);
        }
        public IEnumerable<SpCustomerAgencyBuySumary> sp_CustomerAgencyBuySumary(int AgencyId, string SeriNumber, string FullName, string PhoneNo, string Email, string LevelCode, DateTime FromDate, DateTime ToDate)
        {
            return _storeProcedureRepo.sp_CustomerAgencyBuySumary(AgencyId, SeriNumber, FullName, PhoneNo, Email, LevelCode, FromDate, ToDate);
        }
        public IEnumerable<AgencyBuyGeneral> SP_RPT_AgencyBuyGeneralDiffDate(int AgencyId, DateTime FromDateBuy, DateTime ToDateBuy, DateTime FromDateActive, DateTime ToDateActive, int searchAgencyId, string searchPhone, string searchEmail, string GroupBy, string SortBy, int Start, int Offset)
        {
            return _storeProcedureRepo.SP_RPT_AgencyBuyGeneralDiffDate(AgencyId, FromDateBuy, ToDateBuy, FromDateActive, ToDateActive, searchAgencyId, searchPhone, searchEmail, GroupBy, SortBy, Start, Offset);
        }
        public IEnumerable<SP_SYS_Categroy_Tree> SP_SYS_Categroy_Tree(string Code)
        {
            return _storeProcedureRepo.SP_SYS_Categroy_Tree(Code);
        }
        public IEnumerable<SpDataPostBD> SP_DataPostBD(int AgencyId, DateTime FromDate, DateTime ToDate, int searchAgencyId, string searchPhone, string searchEmail)
        {
            return _storeProcedureRepo.SP_DataPostBD(AgencyId, FromDate, ToDate, searchAgencyId, searchPhone,searchEmail);
        }
        public void SP_RecallLotnumber(string action, string username, string lotNumber)
        {
             _storeProcedureRepo.SP_RecallLotnumber(action, username, lotNumber);
        }

        public IEnumerable<SP_ListTop> SP_ListTop(string action, string codeDethi, int top)
        {
            return _storeProcedureRepo.SP_ListTop(action, codeDethi, top);
        }

        public IEnumerable<SpRetailCustomer> SP_RPT_RetailCustomer(string Action, string phone, string email, int Start, int Offset)
        {
            return _storeProcedureRepo.SP_RPT_RetailCustomer(Action, phone, email, Start, Offset);
        }

        public IEnumerable<AgencyChildBuyGeneral> SP_RPT_AgencyChildBuyGeneral(int AgencyId, DateTime FromDateBuy, DateTime ToDateBuy, DateTime FromDateActive, DateTime ToDateActive, int searchAgencyId, int Start, int Offset)
        {
            return _storeProcedureRepo.SP_RPT_AgencyChildBuyGeneral(AgencyId, FromDateBuy, ToDateBuy, FromDateActive, ToDateActive, searchAgencyId, Start, Offset);
        }
    }
}