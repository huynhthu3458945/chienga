﻿using GCSOFT.MVC.Model.SystemModels;
using System.Collections.Generic;

namespace GCSOFT.MVC.Service.SystemService.Interface
{
    public interface ICongViecService
    {
        IEnumerable<HT_CongViec> FindAll();
        HT_CongViec Get(string code);
        string Insert(HT_CongViec congViec);
        string Update(HT_CongViec congViec);
        string Remove(string[] codes);
        void Commit();
        bool CanAdd(string code, string xCode);
        IEnumerable<HT_CongViec> GetParents(string code);
        IEnumerable<HT_CongViec> GetCategoryBy(string username);
        string GetLink(string name, string parentcode);
        char MaxLevel(string parentCode);
        string BuildLinkLevel(string parentCode, string level);
    }
}