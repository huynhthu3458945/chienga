﻿using GCSOFT.MVC.Model.SystemModels;
using System.Collections.Generic;

namespace GCSOFT.MVC.Service.SystemService.Interface
{
    public interface IUserService
    {
        IEnumerable<HT_Users> FindAll();
        IEnumerable<HT_Users> FindAll(List<int> userIds);
        IEnumerable<HT_Users> FindAll(string groupCode);
        HT_Users GetBy(int id);
        HT_Users GetBy(string userName);
        HT_Users Get(string userName, string password);
        string Insert(HT_Users user);
        string Insert2(HT_Users user);
        string Update(HT_Users user);
        string Delete(int[] userIds);
        string ChangePassword(HT_Users user);
        bool CandAdd(string userName, string xUserName);
        int GetID();
        void Commit();
    }
}