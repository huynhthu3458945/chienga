﻿using GCSOFT.MVC.Model;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.SystemModels;
using System.Collections.Generic;

namespace GCSOFT.MVC.Service.SystemService.Interface
{
    public interface IVuViecService
    {
        IEnumerable<HT_VuViec> FindAll();
        IEnumerable<HT_VuViec> GetRoleBy(string categoryCode, string userName);
        M_OptionLine Get(int OptionHeaderID, int lineNo);
        M_OptionLine Get(int OptionHeaderID, string OptionCode);
        IEnumerable<HT_CongViec> GetCategoryBy(string username);
        string Commit();
    }
}
