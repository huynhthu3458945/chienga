﻿using System;
using System.Collections.Generic;
using GCSOFT.MVC.Data.Repositories.SystemRepositories.Interface;
using GCSOFT.MVC.Model.SystemModels;
using GCSOFT.MVC.Service.SystemService.Interface;
using System.Linq;

namespace GCSOFT.MVC.Service.SystemService.Service
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IAuthenticationRepository _authenRepo;

        public AuthenticationService
            (
                IAuthenticationRepository authenRepo
            )
        {
            _authenRepo = authenRepo;
        }

        public IEnumerable<Permission> SystemFuncForTree(string groupCode)
        {
            return _authenRepo.FindAll().Where(d => d.GroupCode.Equals(groupCode));
        }

        public IEnumerable<PermissionOnSystemFunc> GetAllPermissionOnSystemFunc(string categoryCode, string groupCode)
        {
            return _authenRepo.GetAllPermissionOnSystemFunc(categoryCode, groupCode);
        }
    }
}
