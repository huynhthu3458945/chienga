﻿using System;
using System.Collections.Generic;
using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.SystemRepositories.Interface;
using GCSOFT.MVC.Model.SystemModels;
using GCSOFT.MVC.Service.SystemService.Interface;
using System.Text;
using System.Linq;

namespace GCSOFT.MVC.Service.SystemService.Service
{
    public class CongViecService : ICongViecService
    {
        private readonly ICongViecRepository _congViecRepository;
        private readonly IVuViecCuaCongViecRepository _vuViecCVRepository;
        private readonly IUnitOfWork _unitOfWork;

        public CongViecService
            (
                ICongViecRepository congViecRepository
                , IVuViecCuaCongViecRepository vuViecCVRepository
                , IUnitOfWork unitOfWork
            )
        {
            _congViecRepository = congViecRepository;
            _vuViecCVRepository = vuViecCVRepository;
            _unitOfWork = unitOfWork;
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public IEnumerable<HT_CongViec> FindAll()
        {
            return _congViecRepository.GetAll();
        }

        public HT_CongViec Get(string code)
        {
            var congViec = _congViecRepository.GetById(code);
            congViec.VuViecCuaCongViecs = _vuViecCVRepository.GetMany(d => d.CategoryCode.Equals(code));
            return congViec;
        }

        public IEnumerable<HT_CongViec> GetParents(string code)
        {
            return _congViecRepository.GetMany(d => !d.Code.Equals(code)).OrderBy(d => d.ParentPriorityLink);
        }

        public string Insert(HT_CongViec congViec)
        {
            try
            {
                _congViecRepository.Add(congViec);
                _vuViecCVRepository.Add(congViec.VuViecCuaCongViecs);
                Commit();
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(HT_CongViec congViec)
        {
            try
            {
                _vuViecCVRepository.Delete(d => d.CategoryCode.Equals(congViec.Code));
                _congViecRepository.Update(congViec);
                _vuViecCVRepository.Add(congViec.VuViecCuaCongViecs);
                Commit();
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Remove(string[] codes)
        {
            try
            {
                foreach (var code in codes)
                {
                    _congViecRepository.Delete(d => d.Code.Equals(code));
                    _vuViecCVRepository.Delete(d => d.CategoryCode.Equals(code));
                }
                Commit();
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public bool CanAdd(string code, string xCode)
        {
            HT_CongViec congViec = null;
            //Them moi
            if (string.IsNullOrEmpty(xCode))
                congViec = _congViecRepository.GetById(code);
            else //Update
            {
                if (code.Equals(xCode)) //Khong thay doi No
                    return false;
                congViec = _congViecRepository.GetById(code);
            }
            if (congViec != null)
                return true;
            return false;
        }

        public IEnumerable<HT_CongViec> GetCategoryBy(string username)
        {
            return _congViecRepository.GetCategoryBy(username);
        }

        public string GetLink(string name, string parentCode)
        {
            var links = new List<string>();
            HT_CongViec parentInfo = null;
            var linkStr = new StringBuilder();
            links.Add(name);
            if (!string.IsNullOrEmpty(parentCode))
            {
                do
                {
                    parentInfo = _congViecRepository.GetById(parentCode);
                    links.Add(parentInfo.Name);
                    parentCode = parentInfo.ParentCode;
                } while (!string.IsNullOrEmpty(parentCode));
            }
            for (int i = links.Count(); i > 0; i--)
            {
                if ((i - 1) == 0)
                    linkStr.Append(links[i - 1]);
                else
                    linkStr.Append(links[i - 1] + " > ");
            }
            return linkStr.ToString();
        }

        public char MaxLevel(string parentCode)
        {
            try
            {
                char letter;
                if (string.IsNullOrEmpty(parentCode))
                    letter = char.Parse(_congViecRepository.GetMany(d => string.IsNullOrEmpty(d.ParentCode)).Max(d => d.ParentPriority));
                else
                    letter = char.Parse(_congViecRepository.GetMany(d => d.ParentCode.Equals(parentCode)).Max(d => d.ParentPriority));
                if (string.IsNullOrEmpty(letter.ToString()))
                    return 'a';
                //return (((int)char.Parse(letter)) + 1).ToString();
                return (char)(((int)letter) + 1);
            }
            catch
            {
                return 'a';
            }            
        }

        public string BuildLinkLevel(string parentCode, string level)
        {
            if (string.IsNullOrEmpty(parentCode))
                return level;
            var parentInfo = _congViecRepository.GetById(parentCode);
            return parentInfo.ParentPriorityLink + "." + level;
        }
    }
}