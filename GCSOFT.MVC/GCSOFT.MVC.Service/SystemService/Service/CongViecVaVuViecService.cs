﻿using System;
using System.Collections.Generic;
using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.SystemRepositories.Interface;
using GCSOFT.MVC.Model.SystemModels;
using GCSOFT.MVC.Service.SystemService.Interface;
using System.Linq;

namespace GCSOFT.MVC.Service.SystemService.Service
{
    public class CongViecVaVuViecService : ICongViecVaVuViecService
    {
        private readonly ICongViecVaVuViecRepository _congViecVaVuViecRepo;
        private readonly ICongViecRepository _congViecRepo;
        private readonly IUnitOfWork _unitOfWork;

        public CongViecVaVuViecService(
            ICongViecVaVuViecRepository congViecVaVuViecRepo,
            ICongViecRepository congViecRepo,
            IUnitOfWork unitOfWork
            )
        {
            _congViecVaVuViecRepo = congViecVaVuViecRepo;
            _congViecRepo = congViecRepo;
            _unitOfWork = unitOfWork;
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public HT_CongViecVaVuViec Get(string categoryCode, string userGroupCode)
        {
            return _congViecVaVuViecRepo.Get(d => d.CategoryCode.Equals(categoryCode) && d.UserGroupCode.Equals(userGroupCode));
        }

        public string Insert(IEnumerable<HT_CongViecVaVuViec> congViecVaVuViecs)
        {
            try
            {
                _congViecVaVuViecRepo.Add(congViecVaVuViecs.Where(d => !string.IsNullOrEmpty(d.FunctionCode)));
                Commit();
                var congViecVaVuViec = congViecVaVuViecs.FirstOrDefault();
                SaveParentNo(congViecVaVuViec.CategoryCode, congViecVaVuViec.UserGroupCode);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(IEnumerable<HT_CongViecVaVuViec> congViecVaVuViecs)
        {
            try
            {
                var congViecVaVuViec = congViecVaVuViecs.FirstOrDefault();
                DeleteSystemFuncAndRole(congViecVaVuViec.CategoryCode, congViecVaVuViec.UserGroupCode);
                _congViecVaVuViecRepo.Add(congViecVaVuViecs.Where(d => !string.IsNullOrEmpty(d.FunctionCode)));
                Commit();
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        private void SaveParentNo(string categoryCode, string userGrpCode)
        {
            bool check = true;
            var category = _congViecRepo.GetById(categoryCode);
            while (check)
            {
                if (string.IsNullOrEmpty(category.ParentCode))
                {
                    check = false;
                    break;
                }
                else
                {
                    var congViecVaVuViecParent = _congViecVaVuViecRepo.Get(d => d.CategoryCode.Equals(category.ParentCode) && d.UserGroupCode.Equals(userGrpCode));
                    if (congViecVaVuViecParent == null) //Chua ton tai
                    {
                        var systemFuncAndRole = new HT_CongViecVaVuViec()
                        {
                            CategoryCode = category.ParentCode,
                            UserGroupCode = userGrpCode,
                            FunctionCode = "001"
                        };
                        _congViecVaVuViecRepo.Add(systemFuncAndRole);
                        Commit();
                        SaveParentNo(category.ParentCode, userGrpCode);
                    }
                    else
                    {
                        check = false;
                        break;
                    }
                }
            }
        }

        private void DeleteSystemFuncAndRole(string categoryCode, string userGrpCode)
        {
            _congViecVaVuViecRepo.Delete(d => d.CategoryCode.Equals(categoryCode) && d.UserGroupCode.Equals(userGrpCode));
            Commit();
        }
    }
}
