﻿using System;
using System.Collections.Generic;
using GCSOFT.MVC.Data.Infrastructure;
using GCSOFT.MVC.Data.Repositories.SystemRepositories.Interface;
using GCSOFT.MVC.Model.SystemModels;
using GCSOFT.MVC.Service.SystemService.Interface;
using System.Linq;

namespace GCSOFT.MVC.Service.SystemService.Service
{
    public class UserGroupService : IUserGroupService
    {
        private readonly IUserGroupRepository _userGrpRepo;
        private readonly IUserThuocNhomRepository _userThuocNhomRepo;
        private readonly IUnitOfWork _unitOfWork;

        public UserGroupService
            (
                IUserGroupRepository userGrpRepo
                , IUserThuocNhomRepository userThuocNhomRepo
                , IUnitOfWork unitOfWork
            )
        {
            _userGrpRepo = userGrpRepo;
            _userThuocNhomRepo = userThuocNhomRepo;
            _unitOfWork = unitOfWork;
        }

        public bool CanAdd(string code, string xCode)
        {
            if (!string.IsNullOrEmpty(xCode) && code.Equals(xCode))
                return false;
            var nhomUser = _userGrpRepo.GetById(code);
            if (nhomUser == null)
                return false;
            return true;
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public string Delete(string code)
        {
            try
            {
                _userGrpRepo.Delete(d => d.Code.Equals(code));
                Commit();
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Delete(string[] codes)
        {
            try
            {
                HT_NhomUser userGrp = null;
                foreach (var item in codes)
                {
                    userGrp = _userGrpRepo.GetById(item);
                    _userGrpRepo.Delete(userGrp);
                    _userThuocNhomRepo.Delete(d => d.UserGroupCode == item);
                }
                Commit();
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public IEnumerable<HT_NhomUser> FindAll()
        {
            return _userGrpRepo.GetAll();
        }

        public HT_NhomUser GetBy(string code)
        {
            var userGroup = _userGrpRepo.GetById(code);
            userGroup.UserOfGroups = _userThuocNhomRepo.GetMany(d => d.UserGroupCode.Equals(code));
            return userGroup;
        }

        public HT_NhomUser GetBy2(string code)
        {
            return _userGrpRepo.GetById(code);
        }

        public string Insert(HT_NhomUser nhomUser)
        {
            try
            {
                _userGrpRepo.Add(nhomUser);
                _userThuocNhomRepo.Add(nhomUser.UserOfGroups);
                Commit();
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(HT_NhomUser nhomUser)
        {
            try
            {
                //-- Delete Line has been delete on UI
                int[] idUsers = nhomUser.UserOfGroups.Select(d => d.UserID).ToArray();
                _userThuocNhomRepo.Delete(d => d.UserGroupCode == nhomUser.Code && !idUsers.Contains(d.UserID));
                //-- Update Header
                _userGrpRepo.Update(nhomUser);
                //--Get id alredy exist in Table Line Of Database
                idUsers = _userThuocNhomRepo.GetMany(d => d.UserGroupCode.Equals(nhomUser.Code)).Select(d => d.UserID).ToArray();
                IEnumerable<HT_UserThuocNhom> addOrUpdateUofGrp;
                //--Get Object End
                //Add
                addOrUpdateUofGrp = nhomUser.UserOfGroups.Where(d => !idUsers.Contains(d.UserID));
                _userThuocNhomRepo.Add(addOrUpdateUofGrp);
                Commit();
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
    }
}