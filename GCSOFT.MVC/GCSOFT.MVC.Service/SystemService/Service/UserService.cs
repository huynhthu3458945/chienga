﻿using System;
using System.Linq;
using System.Collections.Generic;
using GCSOFT.MVC.Data.Repositories.SystemRepositories.Interface;
using GCSOFT.MVC.Model.SystemModels;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Data.Infrastructure;

namespace GCSOFT.MVC.Service.SystemService.Service
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepo;
        private readonly IUserThuocNhomRepository _userThuocNhomRepo;
        private readonly IUnitOfWork _unitOfWork;
        public UserService(IUserRepository userRepo, IUserThuocNhomRepository userThuocNhomRepo, IUnitOfWork unitOfWork)
        {
            _userRepo = userRepo;
            _userThuocNhomRepo = userThuocNhomRepo;
            _unitOfWork = unitOfWork;
        }

        public bool CandAdd(string userName, string xUserName)
        {
            if (!string.IsNullOrEmpty(xUserName) && userName.Equals(xUserName))
                return false;
            var user = _userRepo.Get(d => d.Username.Equals(userName));
            if (user == null)
                return false;
            return true;
        }

        public string ChangePassword(HT_Users user)
        {
            try
            {
                _userRepo.Update(user);
                Commit();
                return "";
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public string Delete(int[] userIds)
        {
            try
            {
                _userRepo.Delete(d => userIds.Contains(d.ID));
                _userThuocNhomRepo.Delete(d => userIds.Contains(d.UserID));
                Commit();
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public IEnumerable<HT_Users> FindAll()
        {
            return _userRepo.GetAll();
        }
        public IEnumerable<HT_Users> FindAll(List<int> userIds)
        {
            return _userRepo.GetMany(d => userIds.Contains(d.ID));
        }
        public IEnumerable<HT_Users> FindAll(string groupCode)
        {
            var sql = from utn in _userThuocNhomRepo.GetMany(d => d.UserGroupCode.Equals(groupCode))
                      join u in _userRepo.GetAll() on utn.UserID equals u.ID
                      select u;
            return sql;
                        
        }
        public HT_Users Get(string userName, string password)
        {
            return _userRepo.Get(d => d.Username.ToLower().Equals(userName.ToLower()) && d.Password.Equals(password));
        }

        public HT_Users GetBy(int id)
        {
            var user = _userRepo.GetById(id);
            user.UserOfGroups = _userThuocNhomRepo.GetMany(d => d.UserID == id);
            return user;
        }
        public HT_Users GetBy(string userName)
        {
            return _userRepo.Get(d => d.Username.Equals(userName, StringComparison.OrdinalIgnoreCase));
        }
        public string Insert(HT_Users user)
        {
            try
            {
                _userRepo.Add(user);
                _userThuocNhomRepo.Add(user.UserOfGroups);
                Commit();
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public string Insert2(HT_Users user)
        {
            try
            {
                _userRepo.Add(user);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public string Update(HT_Users user)
        {
            try
            {
                var groupCodes = user.UserOfGroups.Select(d => d.UserGroupCode.ToLower()).ToArray();
                _userThuocNhomRepo.Delete(d => d.UserID == user.ID && !groupCodes.Contains(d.UserGroupCode.ToLower()));
                _userRepo.Update(user);
                foreach (var item in user.UserOfGroups)
                {
                    if (_userThuocNhomRepo.Get(d => d.UserGroupCode.Equals(item.UserGroupCode) && d.UserID == item.UserID) == null)
                        _userThuocNhomRepo.Add(item);
                }
                Commit();
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public int GetID()
        {
            return _userRepo.GetID() + 1;
        }
    }
}