﻿using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.MasterDataService.Interface
{
    public interface IPaymentResultService
    {
        IEnumerable<M_PaymentResult> FindAll(PaymentType type);
        M_PaymentResult GetBy(PaymentType type, string transactionID);
        string Insert(M_PaymentResult paymentResult);
        string Update(M_PaymentResult paymentResult);
        string Delete(PaymentType type, List<string> transactionIDs);
        string GetMax(PaymentType type);
    }
}
