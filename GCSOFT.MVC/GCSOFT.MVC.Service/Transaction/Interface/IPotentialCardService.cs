﻿using GCSOFT.MVC.Model.AgencyModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.Transaction.Interface
{
    public interface IPotentialCardService
    {
        IEnumerable<M_PotentialCard> FindAll();
        M_PotentialCard GetBy(string code);
        M_PotentialCard GetBy2(string code);
        string Insert(M_PotentialCard potentialCard);
        string Update(M_PotentialCard potentialCard);
        string Delete(string code);
        string GetMax();
    }
}
