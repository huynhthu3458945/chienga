﻿using GCSOFT.MVC.Model.AgencyModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.Transaction.Interface
{
    public interface IPotentialCustomerService
    {
        IEnumerable<M_PotentialCustomer> FindAll(string potentialCode);
        IEnumerable<M_PotentialCustomer> FindAll(string potentialCode, int start, int offset);
        IEnumerable<M_PotentialCustomer> FindAll(int agencyId);
        IEnumerable<M_PotentialCustomer> FindAll(int agencyId, int start, int offset);
        IEnumerable<M_PotentialCustomer> FindAll(int agencyId,string fullName, string phone, string email, int start, int offset);
        string Insert(IEnumerable<M_PotentialCustomer> potentialCusts);
        string Insert(M_PotentialCustomer potentialCusts);
        string Delete(string potentialCode);
        bool CheckExist(string cardNo);
        bool CheckByPhone(string phone);
        bool checkByEmail(string email);
    }
}
