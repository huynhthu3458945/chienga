﻿using GCSOFT.MVC.Model.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.Transaction.Interface
{
    public interface IRetailSalesOrderService
    {
        IEnumerable<M_RetailSalesOrder> FindAll(string statusCode);
        IEnumerable<M_RetailSalesOrder> FindSUCCESS(string statusCode1, string statusCode2);
        IEnumerable<M_RetailSalesOrder> FindAllNot(string notStatusCode1, string notStatusCode2);
        M_RetailSalesOrder GetBy(string transactionID);
        string Insert(M_RetailSalesOrder retailSalesOrder);
        string Update(M_RetailSalesOrder retailSalesOrder);
        string Delete(List<string> transactionIDs);
        string UpdateStatus(string transactionID, string statusCode, string transactionInfo, string cardNo);
        string GetMax();
    }
}
