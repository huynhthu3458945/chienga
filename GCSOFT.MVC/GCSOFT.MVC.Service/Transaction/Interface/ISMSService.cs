﻿using GCSOFT.MVC.Model.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.Transaction.Interface
{
    public interface ISMSService
    {
        IEnumerable<M_SMS> FindAll();
        string CheckOTP(string phoneNo, string OTP);
        string CheckOTP(string phoneNo, string OTP, string seriNumber);
        string Insert(M_SMS sms);
        bool AllowSendSMS(string p, int noOfSend);
    }
}
