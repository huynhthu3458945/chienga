﻿using GCSOFT.MVC.Model.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.Transaction.Interface
{
    public interface ISalesLineService
    {
        IEnumerable<M_SalesLine> FindAll(SalesType type, string code);
        IEnumerable<M_SalesLine> FindAll(SalesType type, string code, int start, int offset);
        string Insert(M_SalesLine salesLine);
        string Insert(IEnumerable<M_SalesLine> salesLines);
        string Update(M_SalesLine salesLine);
        string Update(IEnumerable<M_SalesLine> salesLines);
    }
}
