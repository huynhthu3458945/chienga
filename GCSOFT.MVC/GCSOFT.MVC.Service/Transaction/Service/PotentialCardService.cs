﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Service.Transaction.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.Transaction.Service
{
    public class PotentialCardService : IPotentialCardService
    {
        private readonly IPotentialCardRepository _potenticalCardRepo;
        public PotentialCardService
            (
                IPotentialCardRepository potenticalCardRepo
            )
        {
            _potenticalCardRepo = potenticalCardRepo;
        }

        public string Delete(string code)
        {
            try
            {
                _potenticalCardRepo.Delete(d => d.Code.Equals(code));
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public IEnumerable<M_PotentialCard> FindAll()
        {
            return _potenticalCardRepo.GetAll();
        }

        public M_PotentialCard GetBy(string code)
        {
            return _potenticalCardRepo.GetById(code);
        }

        public M_PotentialCard GetBy2(string code)
        {
            return _potenticalCardRepo.Get(d => d.Code.Equals(code));
        }

        public string GetMax()
        {
            return _potenticalCardRepo.GetAll().OrderByDescending(d => d.Code).Select(d => d.Code).FirstOrDefault();
        }

        public string Insert(M_PotentialCard potentialCard)
        {
            try
            {
                 _potenticalCardRepo.Add(potentialCard);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(M_PotentialCard potentialCard)
        {
            try
            {
                _potenticalCardRepo.Update(potentialCard);
                return null;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
    }
}
