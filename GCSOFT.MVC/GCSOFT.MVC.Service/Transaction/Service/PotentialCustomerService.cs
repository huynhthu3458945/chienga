﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Service.Transaction.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.Transaction.Service
{
    public class PotentialCustomerService : IPotentialCustomerService
    {
        private readonly IPotentialCustomerRepository _potenticalCustRepo;
        public PotentialCustomerService
            (
                IPotentialCustomerRepository potenticalCustRepo
            )
        {
            _potenticalCustRepo = potenticalCustRepo;
        }

        public string Delete(string potentialCode)
        {
            try
            {
                _potenticalCustRepo.Delete(d => d.PotentialCode.Equals(potentialCode));
                return null;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public IEnumerable<M_PotentialCustomer> FindAll(string potentialCode)
        {
            return _potenticalCustRepo.GetMany(d => d.PotentialCode.Equals(potentialCode));
        }

        public IEnumerable<M_PotentialCustomer> FindAll(string potentialCode, int start, int offset)
        {
            return _potenticalCustRepo.GetMany(d => d.PotentialCode.Equals(potentialCode)).Skip(start).Take(offset);
        }

        public IEnumerable<M_PotentialCustomer> FindAll(int agencyId)
        {
            return _potenticalCustRepo.GetMany(d => d.AgencyId.Equals(agencyId));
        }

        public IEnumerable<M_PotentialCustomer> FindAll(int agencyId, int start, int offset)
        {
            return _potenticalCustRepo.GetMany(d => d.AgencyId.Equals(agencyId)).Skip(start).Take(offset);
        }
        public IEnumerable<M_PotentialCustomer> FindAll(int agencyId, string fullName, string phone, string email, int start, int offset)
        {
             var res = _potenticalCustRepo.GetMany(d => d.AgencyId.Equals(agencyId)).Skip(start).Take(offset);
            if (!string.IsNullOrEmpty(fullName))
                res = res.Where(z => z.fullName != null && z.fullName.Equals(fullName));
            if (!string.IsNullOrEmpty(phone))
                res = res.Where(z => z.Phone != null && z.Phone.Equals(phone));
            if (!string.IsNullOrEmpty(email))
                res = res.Where(z => z.Email != null && z.Email.Equals(email));
            return res;
        }
        public string Insert(M_PotentialCustomer potentialCusts)
        {
            try
            {
                _potenticalCustRepo.Add(potentialCusts);
                return null;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public string Insert(IEnumerable<M_PotentialCustomer> potentialCusts)
        {
            try
            {
                _potenticalCustRepo.Add(potentialCusts);
                return null;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public bool CheckExist(string cardNo)
        {
            return _potenticalCustRepo.GetMany(d => d.ActionCode.Equals(cardNo)).Any();
        }
        public bool CheckByPhone(string phone)
        {
            return _potenticalCustRepo.GetMany(d => d.Phone.Equals(phone) && !string.IsNullOrEmpty(d.ActionCode)).Any();
        }
        public bool checkByEmail(string email)
        {
            return _potenticalCustRepo.GetMany(d => d.Email.Equals(email) && !string.IsNullOrEmpty(d.ActionCode)).Any();
        }
    }
}
