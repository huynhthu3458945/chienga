﻿using GCSOFT.MVC.Data.Repositories.TransactionRepositories.Interface;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Service.Transaction.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.Transaction.Service
{
    public class SMSService : ISMSService
    {
        private readonly ISMSRepository _smsRepo;
        public SMSService
            (
                ISMSRepository smsRepo
            )
        {
            _smsRepo = smsRepo;
        }

        public string CheckOTP(string phoneNo, string OTP)
        {
            try
            {
                var smsOTP = _smsRepo.Get(d => d.PhoneNo.Equals(phoneNo) && d.OTP.Equals(OTP));
                if (smsOTP == null)
                    return "Nothing";
                smsOTP = _smsRepo.Get(d => d.PhoneNo.Equals(phoneNo) && d.OTP.Equals(OTP) && DateTime.Now <= d.DateInput);
                if (smsOTP == null)
                    return "expired";
                return "Oke";
            }
            catch
            {
                return "Nothing";
            }
        }
        public string CheckOTP(string phoneNo, string OTP, string seriNumber)
        {
            try
            {
                var smsOTP = _smsRepo.Get(d => d.PhoneNo.Equals(phoneNo) && d.OTP.Equals(OTP) && d.Serinumber.Equals(seriNumber));
                if (smsOTP == null)
                    return "Nothing";
                smsOTP = _smsRepo.Get(d => d.PhoneNo.Equals(phoneNo) && d.OTP.Equals(OTP) && d.Serinumber.Equals(seriNumber) && DateTime.Now <= d.DateInput);
                if (smsOTP == null)
                    return "expired";
                return "Oke";
            }
            catch
            {
                return "Nothing";
            }
        }
        public IEnumerable<M_SMS> FindAll()
        {
            return _smsRepo.GetAll();
        }

        public string Insert(M_SMS sms)
        {
            try
            {
                _smsRepo.Add(sms);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public bool AllowSendSMS(string p, int noOfSend)
        {
            return _smsRepo.GetMany(d => d.PhoneNo.Equals(p) && d.DateInput.Day == DateTime.Now.Day && d.DateInput.Month == DateTime.Now.Month && d.DateInput.Year == DateTime.Now.Year).Count() < noOfSend;
        }
    }
}
