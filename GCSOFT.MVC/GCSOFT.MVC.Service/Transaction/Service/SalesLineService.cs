﻿using GCSOFT.MVC.Data.Repositories.TransactionRepositories.Interface;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Service.Transaction.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.Transaction.Service
{
    public class SalesLineService : ISalesLineService
    {
        private readonly ISalesLineRepository _salesLineRepo;
        public SalesLineService
            (
                ISalesLineRepository salesLineRepo
            )
        {
            _salesLineRepo = salesLineRepo;
        }

        public IEnumerable<M_SalesLine> FindAll(SalesType type, string code)
        {
            return _salesLineRepo.GetMany(d => d.SalesType == type && d.DocumentCode.Equals(code));
        }
        public IEnumerable<M_SalesLine> FindAll(SalesType type, string code, int start, int offset)
        {
            return _salesLineRepo.GetMany(d => d.SalesType == type && d.DocumentCode.Equals(code)).Skip(start).Take(offset);
        }
        public string Insert(M_SalesLine salesLine)
        {
            try
            {
                _salesLineRepo.Add(salesLine);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Insert(IEnumerable<M_SalesLine> salesLines)
        {
            try
            {
                _salesLineRepo.Add(salesLines);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(M_SalesLine salesLine)
        {
            try
            {
                _salesLineRepo.Update(salesLine);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }

        public string Update(IEnumerable<M_SalesLine> salesLines)
        {
            try
            {
                _salesLineRepo.Update(salesLines);
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
    }
}
