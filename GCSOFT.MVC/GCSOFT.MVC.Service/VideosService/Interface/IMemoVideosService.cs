﻿using GCSOFT.MVC.Model.Videos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.VideosService.Interface
{
    public interface IMemoVideosService
    {
        IEnumerable<M_MemoVideos> FindAll();
        M_MemoVideos GetBy(int id);
    }
}
