﻿using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using GCSOFT.MVC.Model.Videos;
using GCSOFT.MVC.Service.VideosService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCSOFT.MVC.Service.VideosService.Service
{
    public class MemoVideosService : IMemoVideosService
    {
        private readonly IMemoVideosRepository _memoVideosRepo;
        public MemoVideosService
            (
                IMemoVideosRepository memoVideosRepo
            )
        {
            _memoVideosRepo = memoVideosRepo;
        }

        public IEnumerable<M_MemoVideos> FindAll()
        {
            return _memoVideosRepo.GetAll().OrderBy(d => d.SortOrder);
        }

        public M_MemoVideos GetBy(int id)
        {
            return _memoVideosRepo.GetById(id);
        }
    }
}
