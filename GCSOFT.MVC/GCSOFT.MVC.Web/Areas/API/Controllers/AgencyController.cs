﻿using AutoMapper;
using Firebase.Auth;
using Firebase.Storage;
using GCSOFT.MVC.Web.Areas.API.Data;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.API.Controllers
{
    [CompressResponseAttribute]
    public class AgencyController : Controller
    {
        private readonly HttpClientHelper _http;
        public AgencyController(HttpClientHelper httpClient)
        {
            _http = httpClient;
        }

        /// <summary>
        ///     Index
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [27/09/2021]
        /// </history>
        public ActionResult Index(int? p, string groupCode)
        {
            string api = $"/api/vi-VN/AdminSTNHD/AgencyInfo?groupCode={groupCode}";
            var res = _http.GetAsync<AgencyAPI>(api).GetAwaiter().GetResult();

            int pageString = p ?? 0;
            int page = pageString == 0 ? 1 : pageString;
            int totalRecord = res.data.Count;

            var model = new AgencyAPIs();
            model.Paging = new Paging(totalRecord, page);
            // get Group Code
            api = $"/api/vi-VN/AdminSTNHD/AgencyGroupCodes";
            var levelCodes = _http.GetAsync<AgencyAPI>(api).GetAwaiter().GetResult();
            model.AgencyGroupCodes = levelCodes.data.ToSelectListItems(groupCode);
            if (res.data != null)
            {
                var list = res.data.Skip(model.Paging.start).Take(model.Paging.offset).ToList();
                model.ListAgency = list;
            }    
               
            return View(model);
        }

        /// <summary>
        /// Edit
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [27/09/2021]
        /// </history>
        public ActionResult Edit(int? id)
        {
            AgencyAPI model = GetById(id);
            string api = string.Empty;
            // get Group Code
            api = $"/api/vi-VN/AdminSTNHD/AgencyGroupCodes";
            var levelCodes = _http.GetAsync<AgencyAPI>(api).GetAwaiter().GetResult();
            model.AgencyGroupCodes = levelCodes.data.ToSelectListItems(model.AgencyGroupCode);
            // City
            api = $"/api/Province";
            var citys = _http.GetAsync<Combobox>(api).GetAwaiter().GetResult();
            model.CityList = ToSelectListItems(citys.data, model.CityId);
            // District
            api = $"/api/District?ProvinceId={model.CityId}";
            var districts = _http.GetAsync<Combobox>(api).GetAwaiter().GetResult();
            model.DistrictList = ToSelectListItems(districts.data, model.DistrictId);
            return View(model);
        }

        /// <summary>
        /// GetById
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [28/09/2021]
        /// </history>
        private AgencyAPI GetById(int? id)
        {
            string api = $"/api/vi-VN/AdminSTNHD/AgencyById?id={id}";
            var res = _http.GetAsync<AgencyAPI>(api).GetAwaiter().GetResult();
            var model = new AgencyAPI();
            if (res.data != null)
            {
                model = res.data.FirstOrDefault();
            }

            return model;
        }

        /// <summary>
        /// Edit
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [27/09/2021]
        /// </history>
        public ActionResult Create()
        {
            AgencyAPI model = new AgencyAPI();
            string api = string.Empty;
            // get Group Code
            api = $"/api/vi-VN/AdminSTNHD/AgencyGroupCodes";
            var levelCodes = _http.GetAsync<AgencyAPI>(api).GetAwaiter().GetResult();
            model.AgencyGroupCodes = levelCodes.data.ToSelectListItems(string.Empty);
            // City
            api = $"/api/Province";
            var citys = _http.GetAsync<Combobox>(api).GetAwaiter().GetResult();
            model.CityList = ToSelectListItems(citys.data, string.Empty);
            model.DistrictList = new List<SelectListItem>();
            return View(model);
        }

        /// <summary>
        /// ToSelectListItems
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [27/09/2021]
        /// </history>
        public IEnumerable<SelectListItem> ToSelectListItems( IEnumerable<Combobox> parents, string selectedId)
        {
            return parents.Select(item => new SelectListItem
            {
                Selected = (item.id == selectedId),
                Text = item.name,
                Value = item.id.ToString()
            });
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [28/09/2021]
        /// </history>
        [HttpPost]
        public async Task<ActionResult> Create(AgencyAPI model, HttpPostedFileBase file)
        {
            string api = "/api/vi-VN/AdminSTNHD/AgencyPost";
            var res = _http.PostAsync(api, model).GetAwaiter().GetResult();

            FileStream stream;
            if (file != null && file.ContentLength > 0)
            {
                string path = Path.Combine(Server.MapPath("~/Content/images"), file.FileName);
                file.SaveAs(path);
                stream = new FileStream(Path.Combine(path), FileMode.Open);
                await Task.Run(() => UpLoad(stream, file.FileName, res.retCode, true));
            }
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Edit
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [28/09/2021]
        /// </history>
        [HttpPost]
        public async Task<ActionResult> Edit(AgencyAPI model, HttpPostedFileBase file)
        {
            string api = "/api/vi-VN/AdminSTNHD/AgencyPut";
            var res = _http.PostAsync(api, model).GetAwaiter().GetResult();

            model = GetById(model.Id);

            FileStream stream;
            if (file != null && file.ContentLength > 0)
            {
                string path = Path.Combine(Server.MapPath("~/Content/images"), file.FileName);
                file.SaveAs(path);
                stream = new FileStream(Path.Combine(path), FileMode.Open);
                await Task.Run(() => UpLoad(stream, file.FileName, res.retCode, false, res.retText));
            }

            // get Group Code
            api = $"/api/vi-VN/AdminSTNHD/AgencyGroupCodes";
            var levelCodes = _http.GetAsync<AgencyAPI>(api).GetAwaiter().GetResult();
            model.AgencyGroupCodes = levelCodes.data.ToSelectListItems(string.Empty);
            // City
            api = $"/api/Province";
            var citys = _http.GetAsync<Combobox>(api).GetAwaiter().GetResult();
            model.CityList = ToSelectListItems(citys.data, model.CityId);
            // District
            api = $"/api/District?ProvinceId={model.CityId}";
            var districts = _http.GetAsync<Combobox>(api).GetAwaiter().GetResult();
            model.DistrictList = ToSelectListItems(districts.data, model.DistrictId);
            return View(model);
        }

        /// <summary>
        /// Edit
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [28/09/2021]
        /// </history>
        public async Task<ActionResult> Delete(int? id)
        {
            string api = $"/api/vi-VN/AdminSTNHD/AgencyDelete?id={id}";
            var res = _http.PostAsync(api, new object { }).GetAwaiter().GetResult();
            await Task.Run(() => DeleteFile(res.retText));
            return RedirectToAction("Index");
        }
        public JsonResult FetchDistricts(int provinceId)
        {

            // City
            string api = $"/api/District?ProvinceId={provinceId}";
            var districts = _http.GetAsync<Combobox>(api).GetAwaiter().GetResult();
            //var model = new List<Combobox>();
            //foreach(var item in districts.data)
            //{

            //}
            return Json(districts.data, JsonRequestBehavior.AllowGet);
        }

        public async void UpLoad(FileStream stream, string fileName, string id, bool isAdd = false, string fileNameOld = "")
        {
            var auth = new FirebaseAuthProvider(new FirebaseConfig(FiresbaseConfigHelper.ApiKey));
            var a = await auth.SignInWithEmailAndPasswordAsync(FiresbaseConfigHelper.AuthEmail, FiresbaseConfigHelper.AuthPass);

            var cancellation = new CancellationTokenSource();
            var task = new FirebaseStorage(
                FiresbaseConfigHelper.Bucket,
                new FirebaseStorageOptions
                {
                    AuthTokenAsyncFactory = () => Task.FromResult(a.FirebaseToken),
                    ThrowOnCancel = true
                })
                    .Child(FiresbaseConfigHelper.Folder)
                    .Child(fileName)
                    .PutAsync(stream, cancellation.Token);
            try
            {
                string link = await task;
                var model = GetById(Int32.Parse(id));
                model.Avatar = link;
                string api = "/api/vi-VN/AdminSTNHD/AgencyPut";
                var res = _http.PostAsync(api, model).GetAwaiter().GetResult();
                if (!isAdd)
                    DeleteFile(fileNameOld);
            }
            catch (Exception ex)
            {

            }
        }

        public async void DeleteFile(string file)
        {
            var auth = new FirebaseAuthProvider(new FirebaseConfig(FiresbaseConfigHelper.ApiKey));
            var a = await auth.SignInWithEmailAndPasswordAsync(FiresbaseConfigHelper.AuthEmail, FiresbaseConfigHelper.AuthPass);
            string decodedFile = HttpUtility.UrlDecode(file);
            FirebaseStorageReference reference = new FirebaseStorage(
                FiresbaseConfigHelper.Bucket,
                new FirebaseStorageOptions
                {
                    AuthTokenAsyncFactory = () => Task.FromResult(a.FirebaseToken),
                    ThrowOnCancel = true
                })
                    .Child(FiresbaseConfigHelper.Folder)
                    .Child(decodedFile);
            try
            {
                await reference.DeleteAsync();
            }
            catch (Exception ex)
            {

            }
        }
        public class Combobox
        {
            public string id { get; set; }
            public string name { get; set; }
        }
    }
}