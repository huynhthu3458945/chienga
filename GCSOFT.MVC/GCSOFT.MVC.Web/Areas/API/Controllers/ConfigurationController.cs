﻿using GCSOFT.MVC.Web.Areas.API.Data;
using GCSOFT.MVC.Web.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.API.Controllers
{
    public class ConfigurationController : Controller
    {
        private readonly HttpClientHelper _http;
        public ConfigurationController(HttpClientHelper httpClient)
        {
            _http = httpClient;
        }

        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [27/09/2021]
        /// </history>
        public ActionResult Index()
        {
            return View(GetConfiguration());
        }

        /// <summary>
        /// Edit
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [27/09/2021]
        /// </history>
        public ActionResult Edit()
        {
            return View(GetConfiguration());
        }

        /// <summary>
        /// Edit
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [27/09/2021]
        /// </history>
        [HttpPost]
        public ActionResult Edit(ConfigurationAPI model, HttpPostedFileBase fileIcon_SachSTN,
            HttpPostedFileBase fileIcon_VideoKTGN, HttpPostedFileBase fileIcon_VideoBGSGK,
            HttpPostedFileBase fileIcon_Mindmap, HttpPostedFileBase fileIcon_5PTB, HttpPostedFileBase fileIcon_DienDan,
            HttpPostedFileBase fileImg_SachSTN, HttpPostedFileBase fileImg_VideoKTGN, HttpPostedFileBase fileImg_VideoBGSGK,
            HttpPostedFileBase fileImg_Mindmap, HttpPostedFileBase fileImg_5PTB, HttpPostedFileBase fileImg_DienDan)
        {

        string api = "/api/vi-VN/AdminSTNHD/Configuration";
            var res = _http.PostAsync(api, model).GetAwaiter().GetResult();
            return View(GetConfiguration());
        }

        /// <summary>
        /// GetConfiguration
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [27/09/2021]
        /// </history>
        public ConfigurationAPI GetConfiguration()
        {
            string api = "/api/vi-VN/AdminSTNHD/ConfigurationInfo";
            var res = _http.GetAsync<ConfigurationAPI>(api).GetAwaiter().GetResult();
            var model = new ConfigurationAPI();
            if (res.data != null)
                model = res.data.FirstOrDefault();
            return model;
        }
    }
}