﻿using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using AutoMapper;
using GCSOFT.MVC.Web.Areas.API.Data;
using GCSOFT.MVC.Web.ViewModels;
using Firebase.Auth;
using Firebase.Storage;
using GCSOFT.MVC.Web.Helper;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using GCSOFT.MVC.Model.MasterData;

namespace GCSOFT.MVC.Web.Areas.API.Controllers
{
    public class DownLoadFirebaseController : BaseController
    {
        private readonly IVuViecService _vuViecService;
        private readonly IFirebaseService _firebaseService;
        public DownLoadFirebaseController(IVuViecService vuViecService, IFirebaseService firebaseService) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _firebaseService = firebaseService;
        }
        /// <summary>
        /// Index
        /// </summary>
        /// <param name="p"></param>
        /// <param name="statusID"></param>
        /// <returns></returns>
        public ActionResult Index(int? p, string statusID)
        {
            var model = new Firebases();
            int pageString = p ?? 0;
            int page = pageString == 0 ? 1 : pageString;
            int totalRecord = _firebaseService.FindAll().Count();
            model.Paging = new Paging(totalRecord, page);
            model.StatusList = new List<SelectListItem>()
            {
                new SelectListItem()
                {
                    Value = "DOWNLOAD",
                    Text = "Tải Về"
                },
                new SelectListItem()
                {
                    Value = "DELETE",
                    Text = "Đã Xóa"
                },
            };
            model.ListFirebase = Mapper.Map<IEnumerable<VMFirebase>>(string.IsNullOrEmpty(statusID) ? _firebaseService.FindAll().Skip(model.Paging.start).Take(model.Paging.offset).ToList() : _firebaseService.FindByStatusID(statusID).Skip(model.Paging.start).Take(model.Paging.offset).ToList());
            return View(model);
        }

        /// <summary>
        /// Down Load và Xoa du lieu
        /// </summary>
        /// <param name="id">Code</param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [23/12/2021]
        /// </history>
        public async Task <string> DownLoadAndDelete()
        {
            
            try
            {
                string url = string.Empty;
                string fileNameSplit = string.Empty;
                string fileName = string.Empty;
                string decodedFileName = string.Empty;
                foreach (var item in _firebaseService.FindByStatusID("DOWNLOAD"))
                {
                     url = item.Link;
                     fileNameSplit = url.Split('?')[0];
                     fileName = fileNameSplit.Substring(fileNameSplit.LastIndexOf("o/") + 2, fileNameSplit.Length - fileNameSplit.LastIndexOf("o/") - 2);
                     decodedFileName = HttpUtility.UrlDecode(fileName);

                    if (StartDownload(url, decodedFileName))
                    {
                        item.StatusID = "DELETE";
                        item.StatusName = "Đã Xóa";
                        _firebaseService.Update(Mapper.Map<M_Firebase>(item));
                        _vuViecService.Commit();
                        Delete(decodedFileName);
                    }
                }
                return "Tải về và Xóa dữ liệu không thành công !";
            }
            catch { return "Tải về và Xóa dữ liệu không thành công"; }
        }

        /// <summary>
        /// Delele file on FireBase
        /// </summary>
        /// <param name="file"></param>
        /// <history>
        ///     [Huỳnh Thử] Create [23/12/2021]
        /// </history>
        public async void Delete(string file)
        {
            var auth = new FirebaseAuthProvider(new FirebaseConfig(FiresbaseConfigHelper.ApiKey));
            var a = await auth.SignInWithEmailAndPasswordAsync(FiresbaseConfigHelper.AuthEmail, FiresbaseConfigHelper.AuthPass);
            string decodedFile = HttpUtility.UrlDecode(file);
            FirebaseStorageReference reference = new FirebaseStorage(
                FiresbaseConfigHelper.Bucket,
                new FirebaseStorageOptions
                {
                    AuthTokenAsyncFactory = () => Task.FromResult(a.FirebaseToken),
                    ThrowOnCancel = true
                })
                    .Child(decodedFile);
            try
            {
                await reference.DeleteAsync();
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// Down Load File
        /// </summary>
        /// <param name="url"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [23/12/2021]
        /// </history>
        public bool StartDownload(string url, string fileName)
        {
            try
            {
                System.IO.Directory.CreateDirectory(Server.MapPath("~/Content/images/Download"));
                string path = Path.Combine(Server.MapPath("~/Content/images/Download"), (fileName.Contains("/") ? fileName.Split('/')[fileName.Split('/').Length - 1] : fileName));

                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                }
                using (WebClient client = new WebClient())
                {
                    var ur = new Uri(url);
                    client.DownloadFile(ur, path);
                    return System.IO.File.Exists(path);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Was not able to download file!");
                Console.Write(e);
                return false;
            }
            finally
            {

            }
        }
    }
}