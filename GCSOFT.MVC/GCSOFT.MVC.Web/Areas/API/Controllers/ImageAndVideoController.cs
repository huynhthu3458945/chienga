﻿using AutoMapper;
using Firebase.Auth;
using Firebase.Storage;
using GCSOFT.MVC.Web.Areas.API.Data;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.API.Controllers
{
    [CompressResponseAttribute]
    public class ImageAndVideoController : Controller
    {
        private readonly HttpClientHelper _http;
        public ImageAndVideoController(HttpClientHelper httpClient)
        {
            _http = httpClient;
        }

        /// <summary>
        ///     Index
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [27/09/2021]
        /// </history>
        public ActionResult Index(int? p, string groupCode )
        {
            string api = $"/api/vi-VN/AdminSTNHD/ImageAndVideoInfo?groupCode={groupCode}";
            var res = _http.GetAsync<ImageAndVideo>(api).GetAwaiter().GetResult();

            int pageString = p ?? 0;
            int page = pageString == 0 ? 1 : pageString;
            int totalRecord = res.data.Count;

            var model = new ImageAndVideos();
            model.Paging = new Paging(totalRecord, page);
            // get Group Code
            api = $"/api/vi-VN/AdminSTNHD/ImageAndVideoGroupCodes";
            var groupCodes = _http.GetAsync<ImageAndVideo>(api).GetAwaiter().GetResult();
            model.GroupCodes = groupCodes.data.ToSelectListItems(groupCode);
            if (res.data != null)
            {
                var list = res.data.Skip(model.Paging.start).Take(model.Paging.offset).ToList();
                model.ListImageAndVideo = list;
            }    
               
            return View(model);
        }

        /// <summary>
        /// Edit
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [27/09/2021]
        /// </history>
        public ActionResult Edit(int? id)
        {
            ImageAndVideo model = GetById(id);
            return View(model);
        }

        /// <summary>
        /// GetById
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [28/09/2021]
        /// </history>
        private ImageAndVideo GetById(int? id)
        {
            string api = $"/api/vi-VN/AdminSTNHD/ImageAndVideoById?id={id}";
            var res = _http.GetAsync<ImageAndVideo>(api).GetAwaiter().GetResult();
            var model = new ImageAndVideo();
            if (res.data != null)
            {
                model = res.data.FirstOrDefault();
            }

            return model;
        }

        /// <summary>
        /// Edit
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [27/09/2021]
        /// </history>
        public ActionResult Create()
        {
            ImageAndVideo model = new ImageAndVideo();
            return View(model);
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [28/09/2021]
        /// </history>
        [HttpPost]
        public async Task<ActionResult> Create(ImageAndVideo model, HttpPostedFileBase fileDesktop, HttpPostedFileBase fileMobile, HttpPostedFileBase fileIpad)
        {
            string api = "/api/vi-VN/AdminSTNHD/ImageAndVideoPost";
            var res = _http.PostAsync(api, model).GetAwaiter().GetResult();
            FileStream stream;
            if (fileDesktop != null && fileDesktop.ContentLength > 0)
            {
                string path = Path.Combine(Server.MapPath("~/Content/images"), fileDesktop.FileName);
                fileDesktop.SaveAs(path);
                stream = new FileStream(Path.Combine(path), FileMode.Open);
                await Task.Run(() => UpLoad(stream, fileDesktop.FileName, res.retCode, true, string.Empty, "Desktop"));
            }
            if (fileMobile != null && fileMobile.ContentLength > 0)
            {
                string path = Path.Combine(Server.MapPath("~/Content/images"), fileMobile.FileName);
                fileMobile.SaveAs(path);
                stream = new FileStream(Path.Combine(path), FileMode.Open);
                await Task.Run(() => UpLoad(stream, fileMobile.FileName, res.retCode, true, string.Empty, "Mobile"));
            }
            if (fileIpad != null && fileIpad.ContentLength > 0)
            {
                string path = Path.Combine(Server.MapPath("~/Content/images"), fileIpad.FileName);
                fileIpad.SaveAs(path);
                stream = new FileStream(Path.Combine(path), FileMode.Open);
                await Task.Run(() => UpLoad(stream, fileIpad.FileName, res.retCode, true, string.Empty, "Ipad"));
            }
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Edit
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [28/09/2021]
        /// </history>
        [HttpPost]
        public async Task<ActionResult> Edit(ImageAndVideo model, HttpPostedFileBase fileDesktop, HttpPostedFileBase fileMobile, HttpPostedFileBase fileIpad)
        {
            string api = "/api/vi-VN/AdminSTNHD/ImageAndVideoPut";
            var res = _http.PostAsync(api, model).GetAwaiter().GetResult();
            model = GetById(model.Id);
            FileStream stream;
            if (fileDesktop != null && fileDesktop.ContentLength > 0)
            {
                string path = Path.Combine(Server.MapPath("~/Content/images"), fileDesktop.FileName);
                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                        fileDesktop.SaveAs(path);
                    }
                    else
                        fileDesktop.SaveAs(path);
                stream = new FileStream(Path.Combine(path), FileMode.Open);
                await Task.Run(() => UpLoad(stream, fileDesktop.FileName, res.retCode, false, res.retText, "Desktop"));
            }
            if (fileMobile != null && fileMobile.ContentLength > 0)
            {
                string path = Path.Combine(Server.MapPath("~/Content/images"), fileMobile.FileName);
                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                    fileMobile.SaveAs(path);
                }
                else
                    fileMobile.SaveAs(path);
                stream = new FileStream(Path.Combine(path), FileMode.Open);
                await Task.Run(() => UpLoad(stream, fileMobile.FileName, res.retCode, false, res.retText, "Mobile"));
            }
            if (fileIpad != null && fileIpad.ContentLength > 0)
            {
                string path = Path.Combine(Server.MapPath("~/Content/images"), fileIpad.FileName);
                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                    fileIpad.SaveAs(path);
                }
                else
                    fileIpad.SaveAs(path);
                stream = new FileStream(Path.Combine(path), FileMode.Open);
                await Task.Run(() => UpLoad(stream, fileIpad.FileName, res.retCode, false, res.retText, "Ipad"));
            }

            return View(model);
        }

        /// <summary>
        /// Edit
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [28/09/2021]
        /// </history>
        public async Task<ActionResult> Delete(int? id)
        {
            string api = $"/api/vi-VN/AdminSTNHD/ImageAndVideoDelete?id={id}";
            var res = _http.PostAsync(api, new object { }).GetAwaiter().GetResult();
            foreach(var item in res.retText.Split(';'))
            {
                await Task.Run(() => DeleteFile(item));
            }
            return RedirectToAction("Index");
        }

        public async void UpLoad(FileStream stream, string fileName, string id, bool isAdd = false, string fileNameOld = "", string fileType = "")
        {
            var auth = new FirebaseAuthProvider(new FirebaseConfig(FiresbaseConfigHelper.ApiKey));
            var a = await auth.SignInWithEmailAndPasswordAsync(FiresbaseConfigHelper.AuthEmail, FiresbaseConfigHelper.AuthPass);

            var cancellation = new CancellationTokenSource();
            var task = new FirebaseStorage(
                FiresbaseConfigHelper.Bucket,
                new FirebaseStorageOptions
                {
                    AuthTokenAsyncFactory = () => Task.FromResult(a.FirebaseToken),
                    ThrowOnCancel = true
                })
                    .Child(FiresbaseConfigHelper.Folder)
                    .Child(fileName)
                    .PutAsync(stream, cancellation.Token);
            try
            {
                string link = await task;
                var model = GetById(Int32.Parse(id));
                if(fileType == "Desktop")
                {
                    model.Image_Desktop = link;
                    model.FileType = "Desktop";
                }    
                else if(fileType == "Mobile")
                {
                    model.Image_Mobile = link;
                    model.FileType = "Mobile";
                }   
                else if(fileType == "Ipad")
                {
                    model.Image_Ipad = link;
                    model.FileType = "Ipad";
                }    
                string api = "/api/vi-VN/AdminSTNHD/ImageAndVideoPut";
                var res = _http.PostAsync(api, model).GetAwaiter().GetResult();
                if (!isAdd)
                {
                    if (fileType == "Desktop")
                    {
                        DeleteFile(res.retText);
                    }
                    else if (fileType == "Mobile")
                    {
                        DeleteFile(res.retText);
                    }
                    else if (fileType == "Ipad")
                    {
                        DeleteFile(res.retText);
                    }
                }    
                    
            }
            catch (Exception ex)
            {

            }
        }

        public async void DeleteFile(string file)
        {
            var auth = new FirebaseAuthProvider(new FirebaseConfig(FiresbaseConfigHelper.ApiKey));
            var a = await auth.SignInWithEmailAndPasswordAsync(FiresbaseConfigHelper.AuthEmail, FiresbaseConfigHelper.AuthPass);
            string decodedFile = HttpUtility.UrlDecode(file);
            FirebaseStorageReference reference = new FirebaseStorage(
                FiresbaseConfigHelper.Bucket,
                new FirebaseStorageOptions
                {
                    AuthTokenAsyncFactory = () => Task.FromResult(a.FirebaseToken),
                    ThrowOnCancel = true
                })
                    .Child(FiresbaseConfigHelper.Folder)
                    .Child(decodedFile);

            try
            {
                await reference.DeleteAsync();
            }
            catch (Exception ex)
            {

            }
        }

    }
}