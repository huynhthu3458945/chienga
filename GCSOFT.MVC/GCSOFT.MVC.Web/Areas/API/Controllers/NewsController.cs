﻿using AutoMapper;
using Firebase.Auth;
using Firebase.Storage;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Areas.API.Data;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.API.Controllers
{
    [CompressResponseAttribute]
    public class NewsController : BaseController
    {
        private readonly IVuViecService _vuViecService;
        private readonly HttpClientHelper _http;
        private const string Folder = "STNHD.COM/News";
        private string sysCategory = "WEB_NEWS";
        private bool? permission = false;
        private readonly IOptionLineService _optionLineService;
        //public NewsController(HttpClientHelper httpClient)
        //{
        //    _http = httpClient;
        //}
        public NewsController
            (
                IVuViecService vuViecService
                , HttpClientHelper httpClient
                , IOptionLineService optionLineService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _optionLineService = optionLineService;
            _http = httpClient;
        }
        /// <summary>
        ///     Index
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [27/09/2021]
        /// </history>
        public ActionResult Index(int? p, string groupCode )
        {
            //#region -- Role user --
            //permission = GetPermission(sysCategory, Authorities.View);
            //if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            //if (!permission.Value) return View("AccessDenied");
            //#endregion
            try
            {
                string api = $"/api/vi-VN/AdminSTNHD/NewsInfo?GroupCode={groupCode}";
                var res = _http.GetAsync<News>(api).GetAwaiter().GetResult();

                int pageString = p ?? 0;
                int page = pageString == 0 ? 1 : pageString;
                int totalRecord = res.data.Count;

                var model = new NewsIndex();
                model.Paging = new Paging(totalRecord, page);
                // get Group Code
                api = $"/api/vi-VN/AdminSTNHD/NewsGroupCodes";
                var GroupCodes = _http.GetAsync<News>(api).GetAwaiter().GetResult();
                model.GroupCodes = GroupCodes.data.ToSelectListItems(groupCode);
                if (res.data != null)
                {
                    var list = res.data.Skip(model.Paging.start).Take(model.Paging.offset).ToList();
                    model.ListNews = list;
                }
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.ToString();
                return View("Error");
            }
            
        }

        /// <summary>
        /// Edit
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [27/09/2021]
        /// </history>
        public ActionResult Edit(int? id)
        {
            //#region -- Role user --
            //permission = GetPermission(sysCategory, Authorities.Edit);
            //if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            //if (!permission.Value) return View("AccessDenied");
            //#endregion
            News model = GetById(id);
            // get Group Code
            model.GroupCodes = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("GRPNEWS")).ToSelectListItems(model.GroupCode);
            model.DateCreateStr = string.Format("{0:dd/MM/yyyy}", model.DateCreate);
            return View(model);
        }

        /// <summary>
        /// GetById
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [28/09/2021]
        /// </history>
        private News GetById(int? id)
        {
            string api = $"/api/vi-VN/AdminSTNHD/NewsById?id={id}";
            var res = _http.GetAsync<News>(api).GetAwaiter().GetResult();
            var model = new News();
            if (res.data != null)
            {
                model = res.data.FirstOrDefault();
            }

            return model;
        }

        /// <summary>
        /// Edit
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [27/09/2021]
        /// </history>
        public ActionResult Create()
        {
            //#region -- Role user --
            //permission = GetPermission(sysCategory, Authorities.Add);
            //if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            //if (!permission.Value) return View("AccessDenied");
            //#endregion
            News model = new News();
            string api = string.Empty;
            // get Group Code
            api = $"/api/vi-VN/AdminSTNHD/NewsGroupCodes";
            model.DateCreate = DateTime.Now;
            model.GroupCodes = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("GRPNEWS")).ToSelectListItems("");
            return View(model);
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [28/09/2021]
        /// </history>
        [HttpPost]
        public async Task<ActionResult> Create(News model, HttpPostedFileBase file)
        {
            string api = "/api/vi-VN/AdminSTNHD/NewsPost";
            model.DateCreate = DateTime.Parse(StringUtil.ConvertStringToDate(model.DateCreateStr));
            model.Title = StringUtil.UppercaseWords(model.Title);
            model.GroupName = model.GroupCode.Equals("NEWS") ? "Tin Tức - Sự Kiện" : "Chính Sách Đối Tác";
            var res = _http.PostAsync(api, model).GetAwaiter().GetResult();

            FileStream stream;
            if (file != null && file.ContentLength > 0)
            {
                string path = Path.Combine(Server.MapPath("~/Content/images"), file.FileName);
                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                    file.SaveAs(path);
                }
                else
                    file.SaveAs(path);
                stream = new FileStream(Path.Combine(path), FileMode.Open);
                await Task.Run(() => UpLoad(stream, file.FileName, res.retCode, true)); 
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Edit
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [28/09/2021]
        /// </history>
        [HttpPost]
        public async Task<ActionResult> Edit(News model, HttpPostedFileBase file)
        {
            try
            {
                string api = "/api/vi-VN/AdminSTNHD/NewsPut";
                model.DateCreate = DateTime.Parse(StringUtil.ConvertStringToDate(model.DateCreateStr));
                var res = _http.PostAsync(api, model).GetAwaiter().GetResult();
                model = GetById(model.Id);
                FileStream stream;
                if (file != null && file.ContentLength > 0)
                {
                    string path = Path.Combine(Server.MapPath("~/Content/images"), file.FileName);
                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                        file.SaveAs(path);
                    }
                    else
                        file.SaveAs(path);
                    stream = new FileStream(Path.Combine(path), FileMode.Open);
                    await Task.Run(() => UpLoad(stream, file.FileName, res.retCode, false, res.retText));
                }

                // get Group Code
                api = $"/api/vi-VN/AdminSTNHD/NewsGroupCodes";
                var GroupCodes = _http.GetAsync<News>(api).GetAwaiter().GetResult();
                model.GroupCodes = GroupCodes.data.ToSelectListItems(string.Empty);
                return RedirectToAction("Edit", new { id = model.Id, msg = "1" });
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
           
        }

        /// <summary>
        /// Edit
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [28/09/2021]
        /// </history>
        public async Task<ActionResult> Delete(int? id)
        {
            News model = GetById(id);
            string api = $"/api/vi-VN/AdminSTNHD/NewsDelete?id={id}";
            var res = _http.PostAsync(api, new object { }).GetAwaiter().GetResult();
            if (!string.IsNullOrEmpty(model.ImageNews))
                await Task.Run(() => DeleteFile(model.ImageNews));
            return RedirectToAction("Index");
        }

        public async void UpLoad(FileStream stream, string fileName, string id, bool isAdd = false, string fileNameOld = "")
        {
            var auth = new FirebaseAuthProvider(new FirebaseConfig(FiresbaseConfigHelper.ApiKey));
            var a = await auth.SignInWithEmailAndPasswordAsync(FiresbaseConfigHelper.AuthEmail, FiresbaseConfigHelper.AuthPass);

            var cancellation = new CancellationTokenSource();
            var task = new FirebaseStorage(
                FiresbaseConfigHelper.Bucket,
                new FirebaseStorageOptions
                    {
                    AuthTokenAsyncFactory = () => Task.FromResult(a.FirebaseToken),
                    ThrowOnCancel = true
                })
                    .Child(Folder)
                    .Child(fileName)
                    .PutAsync(stream, cancellation.Token);
            try
            {
                string link = await task;
                var model = GetById(Int32.Parse(id));
                model.ImageNews = link;
                string api = "/api/vi-VN/AdminSTNHD/NewsPut";
                var res = _http.PostAsync(api, model).GetAwaiter().GetResult();
                if(!isAdd)
                    DeleteFile(fileNameOld);
            }
            catch (Exception ex)
            {
               
            }
        }

        public async void DeleteFile(string file)
        {
            var auth = new FirebaseAuthProvider(new FirebaseConfig(FiresbaseConfigHelper.ApiKey));
            var a = await auth.SignInWithEmailAndPasswordAsync(FiresbaseConfigHelper.AuthEmail, FiresbaseConfigHelper.AuthPass);
            string decodedFile = HttpUtility.UrlDecode(file);
            FirebaseStorageReference reference = new FirebaseStorage(
                FiresbaseConfigHelper.Bucket,
                new FirebaseStorageOptions
                {
                    AuthTokenAsyncFactory = () => Task.FromResult(a.FirebaseToken),
                    ThrowOnCancel = true
                })
                    .Child("STNHD.COM")
                    .Child(decodedFile);
            try
            {
                await reference.DeleteAsync();
            }
            catch (Exception ex)
            {

            }
        }
    }
}