﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GCSOFT.MVC.Web.Areas.API.Controllers
{
    public class SMS2Controller : ApiController
    {
        // GET: api/SMS2
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/SMS2/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/SMS2
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/SMS2/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/SMS2/5
        public void Delete(int id)
        {
        }
    }
}
