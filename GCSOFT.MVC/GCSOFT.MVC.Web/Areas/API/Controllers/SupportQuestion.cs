﻿using AutoMapper;
using Firebase.Auth;
using Firebase.Storage;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Web.Areas.API.Data;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.API.Controllers
{
    [CompressResponseAttribute]
    public class SupportQuestionController : Controller
    {
        private readonly HttpClientHelper _http;
        public SupportQuestionController(HttpClientHelper httpClient)
        {
            _http = httpClient;
        }

        /// <summary>
        ///     Index
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [27/09/2021]
        /// </history>
        public ActionResult Index(int? p, string groupCode )
        {
            string api = $"/api/vi-VN/AdminSTNHD/SupportQuestionInfo?GroupCode={groupCode}";
            var res = _http.GetAsync<SupportQuestion>(api).GetAwaiter().GetResult();

            int pageString = p ?? 0;
            int page = pageString == 0 ? 1 : pageString;
            int totalRecord = res.data.Count;

            var model = new SupportQuestions();
            model.Paging = new Paging(totalRecord, page);
            // get Group Code
            api = $"/api/vi-VN/AdminSTNHD/SupportQuestionGroupCodes";
            var GroupCodes = _http.GetAsync<SupportQuestion>(api).GetAwaiter().GetResult();
            model.GroupCodes = GroupCodes.data.ToSelectListItems(groupCode);
            if (res.data != null)
            {
                var list = res.data.Skip(model.Paging.start).Take(model.Paging.offset).ToList();
                model.ListSupportQuestion = list;
            }    
               
            return View(model);
        }

        /// <summary>
        /// Edit
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [27/09/2021]
        /// </history>
        public ActionResult Edit(int? id)
        {
            SupportQuestion model = GetById(id);
            string api = string.Empty;
            // get Group Code
            api = $"/api/vi-VN/AdminSTNHD/SupportQuestionGroupCodes";
            var GroupCodes = _http.GetAsync<SupportQuestion>(api).GetAwaiter().GetResult();
            model.GroupCodes = GroupCodes.data.ToSelectListItems(model.GroupCode);
            model.DateInputStr = string.Format("{0:dd/MM/yyyy}", model.DateInput);
            return View(model);
        }

        /// <summary>
        /// GetById
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [28/09/2021]
        /// </history>
        private SupportQuestion GetById(int? id)
        {
            string api = $"/api/vi-VN/AdminSTNHD/SupportQuestionById?id={id}";
            var res = _http.GetAsync<SupportQuestion>(api).GetAwaiter().GetResult();
            var model = new SupportQuestion();
            if (res.data != null)
            {
                model = res.data.FirstOrDefault();
            }

            return model;
        }

        /// <summary>
        /// Edit
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [27/09/2021]
        /// </history>
        public ActionResult Create()
        {
            SupportQuestion model = new SupportQuestion();
            string api = string.Empty;
            // get Group Code
            api = $"/api/vi-VN/AdminSTNHD/SupportQuestionGroupCodes";
            model.DateInput = DateTime.Now;
            var GroupCodes = _http.GetAsync<SupportQuestion>(api).GetAwaiter().GetResult();
            model.GroupCodes = GroupCodes.data.ToSelectListItems(string.Empty);
            return View(model);
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [28/09/2021]
        /// </history>
        [HttpPost]
        public async Task<ActionResult> Create(SupportQuestion model, HttpPostedFileBase file)
        {
            string api = "/api/vi-VN/AdminSTNHD/SupportQuestionPost";
            model.DateInput = DateTime.Parse(StringUtil.ConvertStringToDate(model.DateInputStr));
            var res = _http.PostAsync(api, model).GetAwaiter().GetResult();

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Edit
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [28/09/2021]
        /// </history>
        [HttpPost]
        public async Task<ActionResult> Edit(SupportQuestion model)
        {
            string api = "/api/vi-VN/AdminSTNHD/SupportQuestionPut";
            model.DateInput = DateTime.Parse(StringUtil.ConvertStringToDate(model.DateInputStr));
            var res = _http.PostAsync(api, model).GetAwaiter().GetResult();
            model = GetById(model.Id);
            
            // get Group Code
            api = $"/api/vi-VN/AdminSTNHD/SupportQuestionGroupCodes";
            var GroupCodes = _http.GetAsync<SupportQuestion>(api).GetAwaiter().GetResult();
            model.GroupCodes = GroupCodes.data.ToSelectListItems(string.Empty);
            return View(model);
        }

        /// <summary>
        /// Edit
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [28/09/2021]
        /// </history>
        public async Task<ActionResult> Delete(int? id)
        {
            string api = $"/api/vi-VN/AdminSTNHD/SupportQuestionDelete?id={id}";
            var res = _http.PostAsync(api, new object { }).GetAwaiter().GetResult();
            return RedirectToAction("Index");
        }
    }
}