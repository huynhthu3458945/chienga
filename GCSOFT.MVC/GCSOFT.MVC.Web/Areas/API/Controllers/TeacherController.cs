﻿using AutoMapper;
using Firebase.Auth;
using Firebase.Storage;
using GCSOFT.MVC.Web.Areas.API.Data;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.API.Controllers
{
    [CompressResponseAttribute]
    public class TeacherController : Controller
    {
        private readonly HttpClientHelper _http;
        public TeacherController(HttpClientHelper httpClient)
        {
            _http = httpClient;
        }

        /// <summary>
        ///     Index
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [27/09/2021]
        /// </history>
        public async Task<ActionResult> Index(int? p, string levelCode )
        {
            string api = $"/api/vi-VN/AdminSTNHD/TeacherInfo?levelCode={levelCode}";
            var res = _http.GetAsync<Teacher>(api).GetAwaiter().GetResult();

            int pageString = p ?? 0;
            int page = pageString == 0 ? 1 : pageString;
            int totalRecord = res.data.Count;

            var model = new Teachers();
            model.Paging = new Paging(totalRecord, page);
            // get Group Code
            api = $"/api/vi-VN/AdminSTNHD/TeacherLevelCodes";
            var levelCodes = _http.GetAsync<Teacher>(api).GetAwaiter().GetResult();
            model.LevelCodes = levelCodes.data.ToSelectListItems(levelCode);
            if (res.data != null)
            {
                var list = res.data.Skip(model.Paging.start).Take(model.Paging.offset).ToList();
                model.ListTeacher = list;
            }
            return View(model);
        }

        /// <summary>
        /// Edit
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [27/09/2021]
        /// </history>
        public ActionResult Edit(int? id)
        {
            Teacher model = GetById(id);
            string api = string.Empty;
            // get Group Code
            api = $"/api/vi-VN/AdminSTNHD/TeacherLevelCodes";
            var levelCodes = _http.GetAsync<Teacher>(api).GetAwaiter().GetResult();
            model.LevelCodes = levelCodes.data.ToSelectListItems(model.LevelCode);
            return View(model);
        }

        /// <summary>
        /// GetById
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [28/09/2021]
        /// </history>
        private Teacher GetById(int? id)
        {
            string api = $"/api/vi-VN/AdminSTNHD/TeacherById?id={id}";
            var res = _http.GetAsync<Teacher>(api).GetAwaiter().GetResult();
            var model = new Teacher();
            if (res.data != null)
            {
                model = res.data.FirstOrDefault();
            }

            return model;
        }

        /// <summary>
        /// Edit
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [27/09/2021]
        /// </history>
        public ActionResult Create()
        {
            Teacher model = new Teacher();
            string api = string.Empty;
            // get Group Code
            api = $"/api/vi-VN/AdminSTNHD/TeacherLevelCodes";
            var levelCodes = _http.GetAsync<Teacher>(api).GetAwaiter().GetResult();
            model.LevelCodes = levelCodes.data.ToSelectListItems(string.Empty);
            return View(model);
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [28/09/2021]
        /// </history>
        [HttpPost]
        public async Task<ActionResult> Create(Teacher model, HttpPostedFileBase file)
        {
            string api = "/api/vi-VN/AdminSTNHD/TeacherPost";
            var res = _http.PostAsync(api, model).GetAwaiter().GetResult();

            FileStream stream;
            if (file != null && file.ContentLength > 0)
            {
                string path = Path.Combine(Server.MapPath("~/Content/images"), file.FileName);
                file.SaveAs(path);
                stream = new FileStream(Path.Combine(path), FileMode.Open);
                await Task.Run(() => UpLoad(stream, file.FileName, res.retCode, true)); 
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Edit
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [28/09/2021]
        /// </history>
        [HttpPost]
        public async Task<ActionResult> Edit(Teacher model, HttpPostedFileBase file)
        {
            string api = "/api/vi-VN/AdminSTNHD/TeacherPut";
            var res = _http.PostAsync(api, model).GetAwaiter().GetResult();
            model = GetById(model.Id);
            FileStream stream;
            if (file != null && file.ContentLength > 0)
            {
                string path = Path.Combine(Server.MapPath("~/Content/images"), file.FileName);
                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                    file.SaveAs(path);
                }
                else
                    file.SaveAs(path);
                stream = new FileStream(Path.Combine(path), FileMode.Open);
                await Task.Run(() => UpLoad(stream, file.FileName, res.retCode, false, res.retText));
            }
            
            // get Group Code
            api = $"/api/vi-VN/AdminSTNHD/TeacherLevelCodes";
            var levelCodes = _http.GetAsync<Teacher>(api).GetAwaiter().GetResult();
            model.LevelCodes = levelCodes.data.ToSelectListItems(string.Empty);
            return View(model);
        }

        /// <summary>
        /// Edit
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [28/09/2021]
        /// </history>
        public async Task<ActionResult> Delete(int? id)
        {
            string api = $"/api/vi-VN/AdminSTNHD/TeacherDelete?id={id}";
            var res = _http.PostAsync(api, new object { }).GetAwaiter().GetResult();
            await Task.Run(() => DeleteFile(res.retText));
            return RedirectToAction("Index");
        }

        public async void UpLoad(FileStream stream, string fileName, string id, bool isAdd = false, string fileNameOld = "")
        {
            var auth = new FirebaseAuthProvider(new FirebaseConfig(FiresbaseConfigHelper.ApiKey));
            var a = await auth.SignInWithEmailAndPasswordAsync(FiresbaseConfigHelper.AuthEmail, FiresbaseConfigHelper.AuthPass);

            var cancellation = new CancellationTokenSource();
            var task = new FirebaseStorage(
                FiresbaseConfigHelper.Bucket,
                new FirebaseStorageOptions
                    {
                    AuthTokenAsyncFactory = () => Task.FromResult(a.FirebaseToken),
                    ThrowOnCancel = true
                })
                    .Child(FiresbaseConfigHelper.Folder)
                    .Child(fileName)
                    .PutAsync(stream, cancellation.Token);
            try
            {
                string link = await task;
                var model = GetById(Int32.Parse(id));
                model.Avartar = link;
                string api = "/api/vi-VN/AdminSTNHD/TeacherPut";
                var res = _http.PostAsync(api, model).GetAwaiter().GetResult();
                if(!isAdd)
                    DeleteFile(fileNameOld);
            }
            catch (Exception ex)
            {
               
            }
        }

        public async void DeleteFile(string file)
        {
            var auth = new FirebaseAuthProvider(new FirebaseConfig(FiresbaseConfigHelper.ApiKey));
            var a = await auth.SignInWithEmailAndPasswordAsync(FiresbaseConfigHelper.AuthEmail, FiresbaseConfigHelper.AuthPass);
            string decodedFile = HttpUtility.UrlDecode(file);
            FirebaseStorageReference reference = new FirebaseStorage(
                FiresbaseConfigHelper.Bucket,
                new FirebaseStorageOptions
                {
                    AuthTokenAsyncFactory = () => Task.FromResult(a.FirebaseToken),
                    ThrowOnCancel = true
                })
                    .Child(FiresbaseConfigHelper.Folder)
                    .Child(decodedFile);
            try
            {
                await reference.DeleteAsync();
            }
            catch (Exception ex)
            {

            }
        }
    }
}