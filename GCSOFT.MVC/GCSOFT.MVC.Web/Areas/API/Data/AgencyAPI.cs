﻿using GCSOFT.MVC.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.API.Data
{
    public class AgencyAPI
    {
      
        public int Id { get; set; }
        [DisplayName("Tên đối tác")]
        public string FullName { get; set; }
        [DisplayName("Hình ảnh đối tác")]
        public string Avatar { get; set; }
        [DisplayName("Nhóm đối tác")]
        public string AgencyGroupCode { get; set; }
        public string AgencyGroupName { get; set; }
        public string NoOfDiamond { get; set; }
        [DisplayName("Quận/Huyện")]
        public string DistrictId { get; set; }
        public string DistrictName { get; set; }
        [DisplayName("Tỉnh/TP")]
        public string CityId { get; set; }
        public string CityName { get; set; }
        [DisplayName("Địa chỉ")]
        public string FullAddress { get; set; }
        [DisplayName("Số điện thoại")]
        public string PhoneNo { get; set; }
        [DisplayName("Email")]
        public string Email { get; set; }
        public IEnumerable<SelectListItem> AgencyGroupCodes { get; set; }
        public IEnumerable<SelectListItem> CityList { get; set; }
        public IEnumerable<SelectListItem> DistrictList { get; set; }
    }

    public class AgencyAPIs
    {
        public string GroupCode { get; set; }
        public Paging Paging { get; set; }
        public IEnumerable<SelectListItem> AgencyGroupCodes { get; set; }
        public IEnumerable<AgencyAPI> ListAgency { get; set; }
    }
}