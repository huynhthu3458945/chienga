﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.API.Data
{
    public class ConfigurationAPI
    {
        public string Id { get; set; }
        [DisplayName("Đường dẫn website Tâm Trí Lực")]
        public string Link_TTL { get; set; }
        [DisplayName("Đường dẫn facebook")]
        public string Link_Facebook { get; set; }
        [DisplayName("Đường dẫn youtube")]
        public string Link_Youtube { get; set; }
        [DisplayName("Đường dẫn tictok")]
        public string Link_Tictok { get; set; }
        [DisplayName("Đường dẫn 5 Phút thuộc bài")]
        public string Link_5PTB { get; set; }
        [DisplayName("Đường dẫn trang đại sứ")]
        public string Link_DaiSuThuongHieu { get; set; }

        [DisplayName("Nội dung bộ công cụ")]
        [AllowHtml]
        public string Content_BoCongCu { get; set; }

        [DisplayName("Nội dung 3 bộ công cụ")]
        [AllowHtml]
        public string Content_3PhienBan { get; set; }

        [DisplayName("Đường dẫn tải ứng dụng trên Android")]
        public string Link_TaiAndroid { get; set; }

        [DisplayName("Đường dẫn tải ứng dụng trên IOS")]
        public string Link_TaiIOS { get; set; }

        [DisplayName("Đường dẫn tải ứng dụng trên Desktop")]
        public string Link_TaiDesktop { get; set; }

        [DisplayName("Icon Sách")]
        public string Icon_SachSTN { get; set; }

        [DisplayName("Icon Video Kỹ Thuật Ghi Nhớ")]
        public string Icon_VideoKTGN { get; set; }

        [DisplayName("Icon Video Bài Giảng SGK")]
        public string Icon_VideoBGSGK { get; set; }

        [DisplayName("Icon Sơ Đồ Tư Duy")]
        public string Icon_Mindmap { get; set; }

        [DisplayName("Icon 5 Phút thuộc bài")]
        public string Icon_5PTB { get; set; }

        [DisplayName("Icon Diễn đàn học tập")]
        public string Icon_DienDan { get; set; }

        [DisplayName("Tiêu đề Sách STNHĐ")]
        public string Title_SachSTNHD { get; set; }
        [DisplayName("Tiêu đề Video Kỹ Thuật Ghi Nhớ")]
        public string Title_VideoKTGN { get; set; }
        [DisplayName("Tiêu đề Video SGK")]
        public string Title_VideoBGSGK { get; set; }
        [DisplayName("Tiêu đề sơ đồ tư duy")]
        public string Title_Mindmap { get; set; }
        [DisplayName("Tiêu đề 5 Phút Thuộc Bài")]
        public string Title_5PTB { get; set; }
        [DisplayName("Tiêu đề diễn đàn")]
        public string Title_DienDan { get; set; }
        [DisplayName("Nội dung Sách STNHĐ")]
        [AllowHtml]
        public string Content_SachSTN { get; set; }
        [DisplayName("Nội dung Video Kỹ Thuật Ghi Nhớ")]
        [AllowHtml]
        public string Content_VideoKTGN { get; set; }
        [AllowHtml]
        [DisplayName("Nội dung Video bài giảng SGK")]
        public string Content_VideoBGSGK { get; set; }
        [AllowHtml]
        [DisplayName("Nội dung Mindmap")]
        public string Content_Mindmap { get; set; }
        [AllowHtml]
        [DisplayName("Nội dung 5 Phút thuộc bài")]
        public string Content_5PTB { get; set; }
        [AllowHtml]
        [DisplayName("Nội dung diễn đàn")]
        public string Content_DienDan { get; set; }
        [DisplayName("Hình ảnh sách STNHĐ")]
        public string Img_SachSTN { get; set; }
        [DisplayName("Hình ảnh Video KTGN")]
        public string Img_VideoKTGN { get; set; }
        [DisplayName("Hình ảnh Video BGSGK")]
        public string Img_VideoBGSGK { get; set; }
        [DisplayName("Hình ảnh sơ đồ tư duy")]
        public string Img_Mindmap { get; set; }
        [DisplayName("Hình ảnh 5 Phút thuộc bài")]
        public string Img_5PTB { get; set; }
        [DisplayName("Hình ảnh diễn đàn")]
        public string Img_DienDan { get; set; }
        [DisplayName("Màu sắc STNHĐ")]
        public string Color_SachSTN { get; set; }
        [DisplayName("Màu sắc videos KTGN")]
        public string Color_VideoKTGN { get; set; }
        [DisplayName("Màu sắc Videos SGK")]
        public string Color_VideoBGSGK { get; set; }
        [DisplayName("Màu sách mindmap")]
        public string Color_Mindmap { get; set; }
        [DisplayName("Màu sắc 5 PTB")]
        public string Color_5PTB { get; set; }
        [DisplayName("Màu sách diễn đàn")]
        public string Color_DienDan { get; set; }
    }
}