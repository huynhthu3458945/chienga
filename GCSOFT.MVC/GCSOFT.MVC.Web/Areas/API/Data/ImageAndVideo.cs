﻿using GCSOFT.MVC.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.API.Data
{
    public class ImageAndVideo
    {
        public int Id { get; set; }
        [DisplayName("Mã nhóm")]
        public string GroupCode { get; set; }
        [DisplayName("Tên nhóm")]
        public string GroupName { get; set; }
        [DisplayName("Tiêu đề")]
        public string Title { get; set; }
        [DisplayName("Link youtube")]
        public string Link_Youtube { get; set; }
        [DisplayName("Link website")]
        public string Link_AnotherWeb { get; set; }
        [DisplayName("Hình desktop")]
        public string Image_Desktop { get; set; }
        [DisplayName("Hình Mobile")]
        public string Image_Mobile { get; set; }
        [DisplayName("Hình Ipad")]
        public string Image_Ipad { get; set; }
        public int SortOrder { get; set; }
        [AllowHtml]
        [DisplayName("Nội dung")]
        public string Content { get; set; }
        public string FileType { get; set; }
    }

    public class ImageAndVideos
    {
        public string GroupCode { get; set; }
        public Paging Paging { get; set; }
        public IEnumerable<SelectListItem> GroupCodes { get; set; }
        public IEnumerable<ImageAndVideo> ListImageAndVideo { get; set; }
    }
}