﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.Web.Mvc;
using GCSOFT.MVC.Web.ViewModels;

namespace GCSOFT.MVC.Web.Areas.API.Data
{
    public class News
    {
        public int Id { get; set; }
        [DisplayName("Nhóm")]
        public string GroupCode { get; set; }
        public string GroupName { get; set; }
        [DisplayName("Tiêu đề tin")]
        public string Title { get; set; }
        public DateTime? DateCreate { get; set; }
        [DisplayName("Ngày tạo")]
        public string DateCreateStrIndex { get { return string.Format("{0:dd/MM/yyyy}", DateCreate); } }
        [DisplayName("Ngày tạo")]
        public string DateCreateStr { get; set; }
        [DisplayName("Ảnh đại diện( 375 x 180 )")]
        public string ImageNews { get; set; }
        [DisplayName("Nội dung tin")]
        [AllowHtml]
        public string FullDescription { get; set; }
        [DisplayName("Sắp xếp")]
        public int SortOrder { get; set; }
        [DisplayName("Filedownload")]
        public string FileDownload { get; set; }
        public IEnumerable<SelectListItem> GroupCodes { get; set; }
    }
    public class NewsIndex
    {
        public string GroupCode { get; set; }
        public Paging Paging { get; set; }
        public IEnumerable<SelectListItem> GroupCodes { get; set; }
        public IEnumerable<News> ListNews { get; set; }
    }
}