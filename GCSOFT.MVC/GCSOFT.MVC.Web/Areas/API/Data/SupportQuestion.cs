﻿using GCSOFT.MVC.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.API.Data
{
    public class SupportQuestion
    {
        public int Id { get; set; }
        [DisplayName("Nhóm")]
        public string GroupCode { get; set; }
        public string GroupName { get; set; }
        [DisplayName("Câu hỏi")]
        public string QuestionName { get; set; }
        [DisplayName("Câu trải lời")]
        [AllowHtml]
        public string AnswerName { get; set; }
        [DisplayName("Thứ tự")]
        public int SortOrder { get; set; }
        [DisplayName("Ngày nhập")]
        public DateTime? DateInput { get; set; }
        public string DateInputStrIndex { get { return string.Format("{0:dd/MM/yyyy}", DateInput); } }
        [DisplayName("Ngày nhập")]
        public string DateInputStr { get; set; }
        public IEnumerable<SelectListItem> GroupCodes { get; set; }
    }
    public class SupportQuestions
    {
        public string GroupCode { get; set; }
        public Paging Paging { get; set; }
        public IEnumerable<SelectListItem> GroupCodes { get; set; }
        public IEnumerable<SupportQuestion> ListSupportQuestion { get; set; }
    }
}