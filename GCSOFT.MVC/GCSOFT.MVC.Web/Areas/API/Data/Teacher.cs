﻿using GCSOFT.MVC.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.API.Data
{
    public class Teacher
    {
        public int Id { get; set; }
        [DisplayName("Giáo viên cấp")]
        public string LevelCode { get; set; }
        public string LevelName { get; set; }
        [DisplayName("Tên giáo viên")]
        public string FullName { get; set; }
        [DisplayName("Trường học")]
        public string SchoolName { get; set; }
        [DisplayName("Diễn giải")]
        [AllowHtml]
        public string Description { get; set; }
        [DisplayName("Học vấn")]
        public string Position { get; set; }
        [DisplayName("Kinh nghiệm")]
        public string TeachingExperience { get; set; }
        [DisplayName("Hình ảnh giáo viên")]
        public string Avartar { get; set; }
        public IEnumerable<SelectListItem> LevelCodes { get; set; }
    }

    public class Teachers
    {
        public string LevelCode { get; set; }
        public Paging Paging { get; set; }
        public IEnumerable<SelectListItem> LevelCodes { get; set; }
        public IEnumerable<Teacher> ListTeacher { get; set; }
    }
}