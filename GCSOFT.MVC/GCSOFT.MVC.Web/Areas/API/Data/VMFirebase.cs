﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GCSOFT.MVC.Web.ViewModels;

namespace GCSOFT.MVC.Web.Areas.API.Data
{
    public class VMFirebase
    {
        public int Id { get; set; }
        public string Link { get; set; }
        public string StatusID { get; set; }
        public string StatusName { get; set; }
    }

    public class Firebases
    {
        public string StatusID { get; set; }
        public Paging Paging { get; set; }
        public List<SelectListItem> StatusList { get; set; }
        public IEnumerable<VMFirebase> ListFirebase { get; set; }
    }
}