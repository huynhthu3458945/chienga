﻿using AutoMapper;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;
using System.Web;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using Newtonsoft.Json;
using GCSOFT.MVC.Web.Helper;

namespace GCSOFT.MVC.Web.Areas.Administrator.Controllers
{
    public class BaseController : Controller
    {
        private readonly IVuViecService _vuViecService;
        private readonly IOptionLineService _optionLineService;
        private string appPath;
        public BaseController(IVuViecService vuViecService, IOptionLineService optionLineService = null)
        {
            _vuViecService = vuViecService;
            _optionLineService = optionLineService;
        }

        public UserLoginVMs GetUser()
        {
            UserLoginVMs user = new UserLoginVMs();
            //if (Session["User"] != null)
            //{
            //    user = (UserLoginVMs)Session["User"];
            //    return user;
            //}
            if (System.Web.HttpContext.Current.Request.Cookies["AdminUser"] != null)
            {
                var userValue = Server.UrlDecode(System.Web.HttpContext.Current.Request.Cookies["AdminUser"].Value);
                var userInfo = JsonConvert.DeserializeObject<UserLoginVMs>(userValue);
                if (Session["CategoryByUser"] == null)
                {
                    var congViecs = Mapper.Map<List<CategoryUser>>(_vuViecService.GetCategoryBy(userInfo.Username).ToList());
                    Session["CategoryByUser"] = congViecs;
                }
                return userInfo;
            }
            
            return null;
        }

        /// <summary>
        /// Get quyền user theo công việc & vụ việc. Đang sử dụng
        /// </summary>
        /// <param name="categoryCode"></param>
        /// <param name="functionCode"></param>
        /// <returns>null:hết session(View("LogOn")), false: ko có quyền(View("AccessDenied")), true: có quyền(tiếp tục)</returns>
        public bool? GetPermission(string categoryCode, string functionCode)
        {
            UserLoginVMs user = GetUser();
            if (user == null)
                return null;
            else
            {
                var vuViecs = GetRole(categoryCode, user.Username).ToList();
                ViewBag.vuViecAccess = vuViecs;
                if (vuViecs.Find(d => d.Code.Equals(functionCode)) == null)
                    return false;
            }
            return true;
        }

        private IEnumerable<VuViecVMs> GetRole(string categoryCode, string userName)
        {
            return Mapper.Map<IEnumerable<VuViecVMs>>(_vuViecService.GetRoleBy(categoryCode, userName));
        }

        public string GetAppPath()
        {
            appPath = System.Web.HttpContext.Current.Request.ApplicationPath;
            if (appPath == string.Empty || appPath.Equals("/")) appPath = "";
            return appPath;
        }

        public string CurrentUrl()
        {
            return HttpUtility.HtmlEncode(System.Web.HttpContext.Current.Request.Url.AbsoluteUri);
        }

        protected VM_OptionLine OptionLine(int idOptionHdr, int idLine)
        {
            return Mapper.Map<VM_OptionLine>(_vuViecService.Get(idOptionHdr, idLine)) ?? new VM_OptionLine();
        }
        protected VM_OptionLine OptionLine(int idOptionHdr, string optionCode)
        {
            return Mapper.Map<VM_OptionLine>(_vuViecService.Get(idOptionHdr, optionCode)) ?? new VM_OptionLine();
        }
        public JsonResult LoadDurationCodeChangeLevelCode(string levelCode)
        {
            string durationCode = string.Empty;
            switch (levelCode)
            {

                case "100": durationCode = "ALL";
                    break;
                case "300": durationCode = "1Y";
                    break;
                case "500": durationCode = "2Y";
                    break;
                case "600": durationCode = "3Y";
                    break;
                case "700": durationCode = "4Y";
                    break;
                case "800": durationCode = "5Y";
                    break;

            }
            return Json(Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("CARDINFO")).ToSelectListItems(durationCode), JsonRequestBehavior.AllowGet);
        }

        public string GetDurationCodeByLevelCode(string levelCode)
        {
            string durationCode = string.Empty;
            switch (levelCode)
            {

                case "100":
                    durationCode = "ALL";
                    break;
                case "300":
                    durationCode = "1Y";
                    break;
                case "500":
                    durationCode = "2Y";
                    break;
                case "600":
                    durationCode = "3Y";
                    break;
                case "700":
                    durationCode = "4Y";
                    break;
                case "200": 
                case "800":
                    durationCode = "5Y";
                    break;

            }
            return durationCode;
        }
    }
}