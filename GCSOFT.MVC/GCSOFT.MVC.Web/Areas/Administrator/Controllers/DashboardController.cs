﻿using System.Web.Mvc;
using GCSOFT.MVC.Service.SystemService.Interface;

namespace GCSOFT.MVC.Web.Areas.Administrator.Controllers
{
    public class DashboardController : BaseController
    {
        public DashboardController(IVuViecService vuViecService) : base(vuViecService)
        {
        }

        // GET: Administrator/Dashboard
        public ActionResult Index()
        {
            if (GetUser() == null)
                return RedirectToAction("Index", "Login", new { area = "administrator" });
            return View();
        }
    }
}