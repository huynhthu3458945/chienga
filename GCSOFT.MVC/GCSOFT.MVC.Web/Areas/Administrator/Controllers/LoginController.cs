﻿using System.Web.Mvc;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using AutoMapper;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using Newtonsoft.Json;
using System;

namespace GCSOFT.MVC.Web.Areas.Administrator.Controllers
{
    public class LoginController : BaseController
    {
        private readonly IUserService _userService;
        private readonly ICongViecService _congViecService;
        private UserLoginVMs userVMs;
        public LoginController
            (
                IVuViecService vuViecService
                , IUserService userService
                , ICongViecService congViecService
            )
            : base(vuViecService)
        {
            _userService = userService;
            _congViecService = congViecService;
        }

        #region -- Get Action --
        public ActionResult Index()
        {
            //Session.Remove("User");
            Session.Remove("CategoryByUser");
            if (System.Web.HttpContext.Current.Request.Cookies["AdminUser"] != null)
            {
                HttpCookie cookie = this.ControllerContext.HttpContext.Request.Cookies["AdminUser"];
                cookie.Expires = DateTime.Now.AddDays(-1);
                this.ControllerContext.HttpContext.Response.Cookies.Add(cookie);
            }
            //if (System.Web.HttpContext.Current.Request.Cookies["CategoryByUser"] != null)
            //{
            //    HttpCookie cookie = this.ControllerContext.HttpContext.Request.Cookies["CategoryByUser"];
            //    cookie.Expires = DateTime.Now.AddDays(-1);
            //    this.ControllerContext.HttpContext.Response.Cookies.Add(cookie);
            //}
            return View();
        }

        public ActionResult Logout(string url)
        {
            return RedirectToAction("Index", "Login", new { area = "administrator", url = url });
        }

        public ActionResult AccessDenied()
        {
            return View();
        }
        #endregion

        #region -- Post Action --
        /// <summary>
        /// 
        /// </summary>
        /// <param name="u">Username</param>
        /// <param name="p">Password</param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1054:UriParametersShouldNotBeStrings",
            Justification = "Needs to take same parameter type as Controller.Redirect()")]
        public ActionResult Index(string u, string p, string url)
        {
            if (!ValidateLogOn(u, p))
            {
                ViewBag.InValid = "1";
                ViewBag.Url = url;
                return View();
            }
            //-- add to Cookie User
            HttpCookie userCookie = new HttpCookie("AdminUser", Server.UrlEncode(JsonConvert.SerializeObject(userVMs)));
            userCookie.Expires = DateTime.Now.AddYears(8);
            System.Web.HttpContext.Current.Response.Cookies.Add(userCookie);
            //-- add to Cookie User + 

            if (GetUser() == null)
                return RedirectToAction("Index", "Login", new { area = "administrator" });
            if (!string.IsNullOrEmpty(url))
                return Redirect(url);
            return RedirectToAction("Index", "Dashboard", new { area = "administrator" });
        }
        #endregion

        #region -- Functions --
        private bool ValidateLogOn (string userName, string password)
        {
            var cryptor = CryptorEngine.Encrypt(password, true, "Hieu.Le-GC.Soft");
            userVMs = Mapper.Map<UserLoginVMs>(_userService.Get(userName, cryptor));
            if (userVMs == null)
                return false;
            return true;
        }
        #endregion
    }
}