﻿using AutoMapper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.ExeclOfficeEngine;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Areas.MasterData.Models;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels.Administrator;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.Administrator.Controllers
{
    public class MessageController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IUserInfoService _userInfoService;
        private readonly IMailSetupService _mailSetupService;
        private readonly IMessageMasterService _messageMasterService;
        private readonly IMessageDetailService _messageDetailService;
        private readonly HttpClientHelper _http;
        private string sysCategory = "ADMIN_PUSHMESSAGE";
        private string hasValue = string.Empty;
        private bool? permission = false;
        private M_MessageMaster modelMasterEntities = new M_MessageMaster();
        private IEnumerable<M_MessageDetail> modelDetailEntities = new List<M_MessageDetail>();
        #endregion

        #region -- Contructor --
        public MessageController(IVuViecService vuViecService,
            IUserInfoService userInfoService,
            IMailSetupService mailSetupService,
            IMessageMasterService messageMasterService,
            IMessageDetailService messageDetailService,
            HttpClientHelper http
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _userInfoService = userInfoService;
            _messageMasterService = messageMasterService;
            _messageDetailService = messageDetailService;
            _mailSetupService = mailSetupService;
            _http = http;
        }
        #endregion
        // GET: Administrator/Message
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var model = Mapper.Map<IEnumerable<MessageVMs>>(_messageMasterService.FindAll());
            return View(model);
        }
        public JsonResult LoadDataUser(GridSettings grid, M_UserInfoFilter filter)
        {
            var listUser = _userInfoService.GetByFullNamePhoneEmail(filter);
            var list = Mapper.Map<IEnumerable<M_UserInfo>>(listUser).AsQueryable();
            list = JqGrid.SetupGrid<M_UserInfo>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }

        public ActionResult OpenPopupUser()
        {
            return PartialView("_PopupUser");
        }

        //public PartialViewResult UserInfoResult(MessageVMs pc)
        //{
        //    M_UserInfoFilter filter = new M_UserInfoFilter();
        //    filter.Phone = pc.Phone;
        //    filter.fullName = pc.FullName;
        //    filter.Email = pc.Email;
        //    var userInfos = _userInfoService.GetByFullNamePhoneEmail(filter);
        //    pc.ListUserInfo = Mapper.Map<IEnumerable<VMUserInfo>>(userInfos).ToList();
        //    return PartialView("_UserInfo", pc.ListUserInfo);
        //}

        [HttpPost]
        public JsonResult RenderHtml(int stt)
        {
            try
            {
                var PostedFile = Request.Files[0];
                string filename = PostedFile.FileName;
                string targetpath = Server.MapPath("/Areas/Administrator/Files/");
                if (!Directory.Exists(targetpath))
                    Directory.CreateDirectory(targetpath);
                PostedFile.SaveAs(targetpath + filename);
                string pathToExcelFile = targetpath + filename;
                var connectionString = "";
                if (filename.EndsWith(".xls"))
                {
                    connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=Excel 8.0;", pathToExcelFile);
                }
                else if (filename.EndsWith(".xlsx"))
                {
                    connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";", pathToExcelFile);
                }
                var adapter = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", connectionString);
                var ds = new DataSet();
                adapter.Fill(ds, "ExcelTable");
                DataTable dtable = ds.Tables["ExcelTable"];
                string stringBuilder = string.Empty;
                foreach (DataRow item in dtable.Rows)
                {
                    stringBuilder += "<tr>";

                    stringBuilder += "<td class=\"uk-text-middle\">";
                    stringBuilder += (stt + 1);
                    stringBuilder += "</td>";

                    stringBuilder += "<td>";
                    stringBuilder += item["UserId"];
                    stringBuilder += $"<input id=\"\" type=\"hidden\" value=\"{item["UserId"]}\" name=\"ListMessageDetail[{stt}].UserID\" class=\"idUser\">";
                    stringBuilder += "</td>";

                    stringBuilder += "<td>";
                    stringBuilder += item["UserName"];
                    stringBuilder += $"<input id=\"\" type=\"hidden\" value=\"{item["UserName"]}\" name=\"ListMessageDetail[{stt}].UserName\">";
                    stringBuilder += "</td>";

                    stringBuilder += "<td>";
                    stringBuilder += item["FullName"];
                    stringBuilder += $"<input id=\"\" type=\"hidden\" value=\"{item["FullName"]}\" name=\"ListMessageDetail[{stt}].FullName\">";
                    stringBuilder += "</td>";

                    stringBuilder += "<td>";
                    stringBuilder += item["Phone"];
                    stringBuilder += $"<input id=\"\" type=\"hidden\" value=\"{item["Phone"]}\" name =\"ListMessageDetail[{stt}].Phone\">";
                    stringBuilder += "</td>";

                    stringBuilder += "<td>";
                    stringBuilder += item["Email"];
                    stringBuilder += $"<input id=\"\" type=\"hidden\" value=\"{item["Email"]}\" name =\"ListMessageDetail[{stt}].Email\">";
                    stringBuilder += "</td>";

                    stringBuilder += "<td class=\"uk-text-right\">";
                    stringBuilder += $"<a href=\"javascript:void(0);\" onclick=\'deleteRow(\"tbody\", \"{stt}\", \"true\");\'><i class=\"material-icons\">&#xE872;</i></a>";
                    stringBuilder += "</td>";

                    stringBuilder += "</tr>";
                }
                return Json(new { success = true, html = stringBuilder });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, html = ex.ToString() }); ;
            }
        }
        public ActionResult Create()
        {
            var model = new MessageVMs();
            model.CreateDate = DateTime.Now;
            model.ListMessageDetail = new List<M_MessageDetail>();
            model.CreateDateStr = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            model.TypeList = new List<SelectListItem>()
            {
                new SelectListItem()
                {
                    Text="Tất Cả",
                    Value = "All"
                },
                new SelectListItem()
                {
                    Text="Gửi Theo User",
                    Value = "NotAll"
                }
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(MessageVMs model)
        {
            try
            {
                model.APK = Guid.NewGuid().ToString();
                SetData(model);
                hasValue = _messageMasterService.Insert(modelMasterEntities);
                hasValue = _messageDetailService.Insert(modelDetailEntities, model.APK);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = model.APK, msg = "1" });
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        public ActionResult Edit(string id)
        {
            var model = new MessageVMs();
            var messageMaster = _messageMasterService.GetByAPK(id);
            model = Mapper.Map<MessageVMs>(messageMaster);
            model.TypeList = new List<SelectListItem>()
            {
                new SelectListItem()
                {
                    Text="Tất Cả",
                    Value = "All",
                    Selected = messageMaster.Type.Equals("All") ? true: false
                },
                new SelectListItem()
                {
                    Text="Gửi Theo User",
                    Value = "NotAll",
                    Selected = messageMaster.Type.Equals("NotAll") ? true: false
                }
            };
            model.DescriptionVM = messageMaster.Description;
            model.CreateDateStr = string.Format("{0:dd/MM/yyyy}", model.CreateDate);
            model.ListMessageDetail = _messageDetailService.FindAll().Where(z => z.APKMaster == id).ToList();
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(MessageVMs model)
        {
            try
            {
                SetData(model);
                var entities = _messageMasterService.GetByAPK(model.APK);
                modelMasterEntities.ID = entities.ID;
                hasValue = _messageMasterService.Update(modelMasterEntities);
                hasValue = _messageDetailService.Insert(modelDetailEntities, model.APK);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    if (string.IsNullOrEmpty(hasValue))
                        return RedirectToAction("Edit", new { id = model.APK, msg = "1" });
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        public void SetData(MessageVMs model)
        {
            model.FullName = GetUser().FullName;
            modelMasterEntities = Mapper.Map<M_MessageMaster>(model);
            modelMasterEntities.Description = model.DescriptionVM;
            if (!string.IsNullOrEmpty(model.CreateDateStr))
                modelMasterEntities.CreateDate = DateTime.Parse(StringUtil.ConvertStringToDate(model.CreateDateStr));
            else
                modelMasterEntities.CreateDate = null;

            modelDetailEntities = model.ListMessageDetail;
            if(modelDetailEntities != null)
                foreach (var item in modelDetailEntities)
                {
                    item.APKMaster = modelMasterEntities.APK;
                }
        }
        public ActionResult DownloadExcel()
        {
            var res = File(HttpContext.Server.MapPath("/Areas/Administrator/Report/Message.xlsx"), OfficeContentType.XLSX, "Message.xlsx");
            return res;
        }
        public JsonResult Notification(string apk)
        {
            try
            {
                var model = _messageMasterService.GetByAPK(apk);
                string url = "https://apipro.5phutthuocbai.com/api/MstNotifications/push?appName=5PTB";
                var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
                var notification = new Noti();
                List<int> user_ids = new List<int>();
                notification.type = "NOTIFICATION";
                notification.title = model.Title;
                notification.image = "https://5phutthuocbai.com/static/media/voi.ac034b38.png";
                notification.date = DateTime.Now;


                // Gửi All chỉ gửi thông báo về app, Không gửi mail
                if (model.Type.Equals("All"))
                {
                    // noti
                    //user_ids.Add(186935);
                    notification.listUserId = user_ids;
                    notification.description = model.Description;
                    notification.name = "5 Phút Thuộc Bài";
                    var res = _http.PostUrlAsync(url, notification);
                }
                else  // Not All
                {
                    // Chứa <<FullName>> chạy foreach Replace và gửi từng User
                    if (model.Description.Contains("<<FullName>>"))
                    {
                        foreach (var item in _messageDetailService.FindAll().Where(z => z.APKMaster == apk))
                        {
                            // send mail
                            var emailTos = new List<string>();
                            emailTos.Add(item.Email);
                            user_ids = new List<int>();
                            user_ids.Add(item.UserID);
                            var _mailHelper = new EmailHelper()
                            {
                                EmailTos = emailTos,
                                DisplayName = emailInfo.DisplayName,
                                Title = model.Title,
                                Body = model.Description.Replace("<<FullName>>", item.FullName),
                                MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
                            };
                            hasValue = _mailHelper.SendEmailsThread();

                            // noti
                            notification.description = model.Description.Replace("<<FullName>>", item.FullName);
                            notification.listUserId = user_ids;
                            notification.name = "5 Phút Thuộc Bài";
                            var res = _http.PostUrlAsync(url, notification).Result;
                        }
                    }
                    else // Không chứa <<FullName>> add vào danh sách gửi 1 lần
                    {
                        var emailTos = new List<string>();

                        foreach (var item in _messageDetailService.FindAll().Where(z => z.APKMaster == apk))
                        {
                            emailTos = new List<string>();
                            emailTos.Add(item.Email);

                            // send mail
                            var _mailHelper = new EmailHelper()
                            {
                                EmailTos = emailTos,
                                DisplayName = emailInfo.DisplayName,
                                Title = model.Title,
                                Body = model.Description,
                                MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
                            };
                            hasValue = _mailHelper.SendEmailsThread();
                            user_ids.Add(item.UserID);
                        }

                        // noti
                        notification.listUserId = user_ids;
                        notification.description = model.Description;
                        notification.name = "5 Phút Thuộc Bài";
                        var res = _http.PostUrlAsync(url, notification);
                    }
                    
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult Success()
        {
            return View();
        }
        public class Noti
        {
            public List<int> listUserId { get; set; }
            public string image { get; set; }
            public bool seen { get; set; }
            public string title { get; set; }
            public string type { get; set; }
            public DateTime date { get; set; }
            public string name { get; set; }
            public string description { get; set; }
        }
    }
}