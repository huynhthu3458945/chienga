﻿using System.Web.Mvc;
using System.Collections.Generic;
using System.Linq;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Model.SystemModels;
using System.Text;
using GCSOFT.MVC.Web.ViewModels;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels.HT_CongViecVaVuViec;

namespace GCSOFT.MVC.Web.Areas.Administrator.Controllers
{
	[CompressResponseAttribute]
    public class SysAuthenticationController : BaseController
    {
        #region -- Properties --
        private readonly IUserGroupService _userGrpServices;
        private readonly ICongViecService _congViecServices;
        private readonly IVuViecService _vuViecService;
        private readonly IAuthenticationService _authenServices;
        private readonly ICongViecVaVuViecService _congViecVaVuViecService;

        private IEnumerable<Permission> permissions = null;
        private HT_NhomUser userGroup;
        private StringBuilder buildTree;
        private string sysCategory = "UG";
        private bool? permisson;
        #endregion

        #region -- Contructor --
        public SysAuthenticationController
            (
                IVuViecService vuViecService
                , ICongViecService congViecServices
                , IUserGroupService userGrpServices
                , IAuthenticationService authenServices
                , ICongViecVaVuViecService congViecVaVuViecService
            ) : base(vuViecService)
        {
            _congViecServices = congViecServices;
            _vuViecService = vuViecService;
            _userGrpServices = userGrpServices;
            _authenServices = authenServices;
            _congViecVaVuViecService = congViecVaVuViecService;
        }
        #endregion

        #region -- Get Action --
        public ActionResult Index(string id, bool? chkTatCa)
        {
            #region Role user
            //if (GetUserVM() == null)
            //    return RedirectToAction("Index", "GC_Logon"); ;
            //permisson = GetPermission(_roleServices.GetRoleBy(sysFuncNo, GetUserVM().Email), Authorities.Access);
            //if (!permisson.Value)
            //    return View("AccessDenied");
            #endregion
            bool tatCa = chkTatCa ?? false;
            buildTree = new StringBuilder();
            userGroup = _userGrpServices.GetBy(id);
            if (!tatCa)
            {
                permissions = _authenServices.SystemFuncForTree(id);
                buildTree = TreeSystemFuncHelper.BuildTreeCongViecOfNhom(permissions);
            }
            else
            {
                var systemFuncs = _congViecServices.FindAll();
                buildTree = TreeSystemFuncHelper.BuildTreeSystemFunc(systemFuncs);
            }
            var authenVM = new AuthenticationVM()
            {
                CategoryTree = buildTree.ToString(),
                chkTatCa = tatCa,
                userGroups = userGroup
            };
            return View(authenVM);
        }
        #endregion

        /// <summary>
        /// Lấy vụ việc của công việc
        /// </summary>
        /// <param name="s">CategoryCode</param>
        /// <param name="g">GroupCode</param>
        /// <returns></returns>
        public ActionResult GetRoleSystemFunc(string c, string g)
        {
            IEnumerable<PermissionOnSystemFunc> lst = _authenServices.GetAllPermissionOnSystemFunc(c, g);
            return Json(lst);
        }

        /// <summary>
        /// Câp nhật quyền cho nhóm user
        /// </summary>
        /// <param name="sAndRVM">SystemFuncAndRoleVM</param>
        /// <returns></returns>
        public JsonResult UpdateRoles(CongViecVaVuViecVM congViecVaVuViecVM)
        {
            bool hasValue = false;
            var congViecVaVuViec = _congViecVaVuViecService.Get(congViecVaVuViecVM.c, congViecVaVuViecVM.u);
            var congViecVaVuViecs = new List<HT_CongViecVaVuViec>();
            congViecVaVuViecVM.fs.ToList().ForEach(item => {
                congViecVaVuViecs.Add(new HT_CongViecVaVuViec
                {
                    CategoryCode = congViecVaVuViecVM.c,
                    UserGroupCode = congViecVaVuViecVM.u,
                    FunctionCode = item
                });
            });
            if (congViecVaVuViec != null) //Update
                hasValue = string.IsNullOrEmpty(_congViecVaVuViecService.Update(congViecVaVuViecs)) ? true : false;
            else //Insert
                hasValue = string.IsNullOrEmpty(_congViecVaVuViecService.Insert(congViecVaVuViecs)) ? true : false;
            return Json(hasValue);
        }
    }
}
