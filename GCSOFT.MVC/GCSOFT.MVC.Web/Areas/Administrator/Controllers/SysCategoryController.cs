﻿using System.Web.Mvc;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System;
using GCSOFT.MVC.Model.SystemModels;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Service.StoreProcedure.Interface;

namespace GCSOFT.MVC.Web.Areas.Administrator.Controllers
{
    [CompressResponseAttribute]
    public class SysCategoryController : BaseController
    {
        private readonly ICongViecService _congViecService;
        private readonly IVuViecService _vuViecService;
        private readonly IStoreProcedureService _storeService;
        private string sysCategory = "CV";
        private bool? permission = false;
        private CategoryCard categoryCard;
        private HT_CongViec congViec;
        private string hasValue;

        public SysCategoryController
            (
                IVuViecService vuViecService
                , ICongViecService congViecService
                , IStoreProcedureService storeService
            ) : base(vuViecService)
        {
            _congViecService = congViecService;
            _vuViecService = vuViecService;
            _storeService = storeService;
        }
        
        #region -- Get Action --
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }

        public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            categoryCard = new CategoryCard()
            {
                Functions = Mapper.Map<IEnumerable<VuViecVMs>>(_vuViecService.FindAll())
                //, Parents = Mapper.Map<IEnumerable<CategoryListVMs>>(_congViecService.GetParents("")).ToSelectListItems("")
                ,Parents = Mapper.Map<IEnumerable<CategoryListVMs>>(_storeService.SP_SYS_Categroy_Tree("")).ToSelectListItems("")
            };
            return View(categoryCard);
        }

        public ActionResult Edit(string id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            categoryCard = Mapper.Map<CategoryCard>(_congViecService.Get(id));
            if (categoryCard == null)
                return HttpNotFound();
            categoryCard.xCode = categoryCard.Code;
            categoryCard.xParentCode = categoryCard.ParentCode;
            //categoryCard.Parents = Mapper.Map<IEnumerable<CategoryListVMs>>(_congViecService.GetParents(categoryCard.Code)).ToSelectListItems(categoryCard.ParentCode);
            categoryCard.Parents = Mapper.Map<IEnumerable<CategoryListVMs>>(_storeService.SP_SYS_Categroy_Tree(categoryCard.Code)).ToSelectListItems(categoryCard.ParentCode);
            categoryCard.Functions = Mapper.Map<IEnumerable<VuViecVMs>>(_vuViecService.FindAll());
            foreach (var item in categoryCard.VuViecCuaCongViecs)
            {
                categoryCard.Functions.Where(d => d.Code.Equals(item.FunctionCode)).Select(d => d.Check = true).FirstOrDefault();
            }
            return View(categoryCard);
        }

        public ActionResult Details(string id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.ViewDetail);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            categoryCard = Mapper.Map<CategoryCard>(_congViecService.Get(id));
            if (categoryCard == null)
                return HttpNotFound();
            categoryCard.Functions = Mapper.Map<IEnumerable<VuViecVMs>>(_vuViecService.FindAll());
            foreach (var item in categoryCard.VuViecCuaCongViecs)
            {
                categoryCard.Functions.Where(d => d.Code.Equals(item.FunctionCode)).Select(d => d.Check = true).FirstOrDefault();
            }
            return View(categoryCard);
        }
        #endregion

        #region -- Post Action --
        [HttpPost]
        public ActionResult Create(CategoryCard cateCard)
        {
            try
            {
                SetData(cateCard);
                hasValue = _congViecService.Insert(congViec);
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = cateCard.Code, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        [HttpPost]
        public ActionResult Edit(CategoryCard cateCard)
        {
            try
            {
                SetData(cateCard);
                hasValue = _congViecService.Update(congViec);
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { area = "administrator", id = cateCard.Code, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        #endregion

        #region -- Json Action --
        public JsonResult LoadData(GridSettings grid)
        {
            var list = Mapper.Map<IEnumerable<CategoryListVMs>>(_congViecService.FindAll().OrderByDescending(d => d.ParentPriorityLink)).AsQueryable();
            list = JqGrid.SetupGrid<CategoryListVMs>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Xoa du lieu
        /// </summary>
        /// <param name="id">Code</param>
        /// <returns></returns>
        public JsonResult Deletes(string id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                string[] _codes = id.Split(',').Select(item => item).ToArray();
                string hasValue = _congViecService.Remove(_codes);
                if (string.IsNullOrEmpty(hasValue))
                    return Json(new { v = true, count = _codes.Count() });
                return Json("Xóa dữ liệu không thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }

        /// <summary>
        /// Check Category Code already exists
        /// </summary>
        /// <param name="c">Code</param>
        /// <param name="xc">xCode</param>
        /// <returns></returns>
        public JsonResult CandAdd(string c, string xc)
        {
            return Json(_congViecService.CanAdd(c, xc));
        }
        #endregion

        #region -- Functions --
        private void SetData(CategoryCard cateCard)
        {
            if (string.IsNullOrEmpty(cateCard.xCode) || (cateCard.Code.Equals(cateCard.xCode) && cateCard.ParentCode != cateCard.xParentCode))
            {
                cateCard.Link = _congViecService.GetLink(cateCard.Name, cateCard.ParentCode);
                cateCard.ParentPriority = _congViecService.MaxLevel(cateCard.ParentCode).ToString();
                cateCard.ParentPriorityLink = _congViecService.BuildLinkLevel(cateCard.ParentCode, cateCard.ParentPriority);
            }
            cateCard.ParentName = null;
            if (!string.IsNullOrEmpty(cateCard.ParentCode))
            {
                var parentInfo = _congViecService.Get(cateCard.ParentCode);
                cateCard.ParentName = parentInfo != null ? parentInfo.Name : cateCard.Name;
            }
            if (cateCard.Functions == null)
                return;
            var categoryFunctions = new List<CategoryFunctionVMs>();
            cateCard.Functions.Where(d => d.Check).ToList().ForEach(item => {
                categoryFunctions.Add(new CategoryFunctionVMs() {
                    CategoryCode = cateCard.Code,
                    FunctionCode = item.Code
                });
            });
            cateCard.VuViecCuaCongViecs = categoryFunctions;
            congViec = Mapper.Map<HT_CongViec>(cateCard);
        }
        #endregion
    }
}