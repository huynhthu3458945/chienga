﻿using System.Web.Mvc;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Model.SystemModels;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Web.Areas.Agency.Data;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Web.ViewModels.HienTai;
using System.Data.SqlClient;
using System.Configuration;
using Dapper;
using System.Reflection;
using GCSOFT.MVC.Web.Areas.MasterData.Models;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Web.Extension;

namespace GCSOFT.MVC.Web.Areas.Administrator.Controllers
{
	[CompressResponseAttribute]
    public class SysUserController : BaseController
    {
		#region -- Properties --
		private readonly IVuViecService _vuViecService;
		private readonly IUserService _userService;
        private readonly IUserGroupService _userGroupService;
        private readonly IUserThuocNhomService _userThuocNhomService;
        private readonly IAgencyService _agenctyService;
        private readonly ITemplateService _templateService;
        private readonly IMailSetupService _mailSetupService;
        private string hienTaiConnection = ConfigurationManager.ConnectionStrings["HienTaiConnection"].ConnectionString;
        private string sysCategory = "U";
        private bool? permission = false;
        private UserCard userCard;
        private HT_Users user;
        private string hasValue;
		#endregion
		
		#region -- Contructor --
		public SysUserController
            (
				IVuViecService vuViecService
                , IUserService userService
                , IUserGroupService userGroupService
                , IUserThuocNhomService userThuocNhomService
                , IAgencyService agenctyService
                , ITemplateService templateService
                , IMailSetupService mailSetupService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _userService = userService;
            _userGroupService = userGroupService;
            _userThuocNhomService = userThuocNhomService;
            _agenctyService = agenctyService;
            _templateService = templateService;
            _mailSetupService = mailSetupService;
        }
		#endregion
		
		#region -- Get --
		public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }
		
		public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            userCard = new UserCard();
            return View(userCard);
        }

        public ActionResult Edit(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            user = _userService.GetBy(id);
            userCard = Mapper.Map<UserCard>(user);
            userCard.Password2 = CryptorEngine.Decrypt(userCard.Password, true, "Hieu.Le-GC.Soft");
            if (userCard == null) return HttpNotFound();
            foreach (var item in userCard.UserOfGroups)
            {
                item.NhomUserInfo = Mapper.Map<NhomUsers>(_userGroupService.GetBy2(item.UserGroupCode));
            }
            return View(userCard);
        }
		
		public ActionResult Details(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.ViewDetail);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            user = _userService.GetBy(id);
            userCard = Mapper.Map<UserCard>(user);
            if (userCard == null) return HttpNotFound();
            foreach (var item in userCard.UserOfGroups)
            {
                item.NhomUserInfo = Mapper.Map<NhomUsers>(_userGroupService.GetBy2(item.UserGroupCode));
            }
            return View(userCard);
        }

        public ActionResult OpenPopupUser()
        {
            return PartialView("_PopupUser");
        }

        public ActionResult ChangePassword(int? id)
        {
            var user = Mapper.Map<UserLoginVMs>(_userService.GetBy(id ?? GetUser().ID));
            if (id == null)
                return PartialView("_ChangePassword", user);
            return PartialView("_ChangePassword", user);
        }
        #endregion

        #region -- Post --
        [HttpPost]
        public ActionResult Create(UserCard userCard2)
        {
            try
            {
                string pass = userCard2.Password;
                userCard2.ID = _userService.GetID();
                userCard2.Password = CryptorEngine.Encrypt(userCard2.Password, true, "Hieu.Le-GC.Soft");
                SetData(userCard2);
                hasValue = _userService.Insert(user);
                string contentSMS = $"5 Phut Thuoc Bai\n" +
                                    $"Than gui Anh Hoa\n" +
                                    $"Thong tin dang nhap:\n" +
                                    $"Ten dang nhap: hoavtv7@gmail.com \n" +
                                    $"mat khau:  nhodedang\n" +
                                    $"Vui long tai app 5 Phut Thuoc Bai tai https://www.5phutthuocbai.com/download \n" +
                                    $"sau do vao app de vao lop hoc.";
                // gửi sms
                // gửi sms
                var smsHelper = new SMSHelper()
                {
                    PhoneNo = "0983606035",
                    Content = contentSMS.NonUnicode()
                };
                if (smsHelper.IsMobiphone())
                {
                    smsHelper.Content = smsHelper.Content;
                    hasValue = smsHelper.SendMobiphone();
                }
                else
                    hasValue = smsHelper.Send();
                //-- Gửi qua email
                var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
                var mailContent = $"Thân gửi Anh Hòa,<br>" +
                                  $" Thông tin đăng nhập:<br>" +
                                  $" Tài khoản: hoavtv7@gmail.com<br>" +
                                  $" Mật khẩu: nhodedang<br>" +
                                  $" Vui lòng tải app 5 Phút Thuộc Bài tại https://www.5phutthuocbai.com/download <br>" +
                                  $" sau đó vào app để vào lớp học.";
                var emailTos = new List<string>();
                emailTos.Add("hoavtv7@gmail.com");
                var _mailHelper = new EmailHelper()
                {
                    EmailTos = emailTos,
                    DisplayName = "5 PHÚT THUỘC BÀI",
                    Title = "[5 Phút Thuộc Bài] - Tài Khoản Đăng Nhập",
                    Body = mailContent,
                    MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
                };
                hasValue = _mailHelper.DoSendMail();

                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = user.ID, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
		
		 [HttpPost]
        public ActionResult Edit(UserCard userCard2)
        {
            try
            {
                SetData(userCard2);
                hasValue = _userService.Update(user);
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { area = "administrator", id = userCard2.ID, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        [HttpPost]
        public ActionResult ChangePassword(UserLoginVMs userVM)
        {
            var user = _userService.GetBy(userVM.ID);
            user.Password = CryptorEngine.Encrypt(userVM.Password, true, "Hieu.Le-GC.Soft");
            return Json(_userService.ChangePassword(user));
        }
        #endregion

        #region -- Json --
        public JsonResult LoadData(GridSettings grid)
        {
            var list = Mapper.Map<IEnumerable<UserList>>(_userService.FindAll()).AsQueryable();
            list = JqGrid.SetupGrid<UserList>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
		
		 /// <summary>
        /// Xoa du lieu
        /// </summary>
        /// <param name="id">Code</param>
        /// <returns></returns>
        public JsonResult Deletes(string id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                int[] _ids = id.Split(',').Select(item => int.Parse(item)).ToArray();
                string hasValue = _userService.Delete(_ids);
                if (string.IsNullOrEmpty(hasValue))
                    return Json(new { v = true, count = _ids.Count() });
                return Json("Xóa dữ liệu không thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }

        /// <summary>
        /// Kiểm tra Username đã tồn tại
        /// </summary>
        /// <param name="u"></param>
        /// <param name="xu"></param>
        /// <returns></returns>
        public JsonResult CandAdd(string u, string xu)
        {
            return Json(_userService.CandAdd(u, xu));
        }
        #endregion

        #region -- Functions --
        private void SetData(UserCard userCard2)
        {
            userCard2.FirstName = StringUtil.UppercaseWords(userCard2.FirstName.Trim());
            userCard2.LastName = StringUtil.UppercaseWords(userCard2.LastName.Trim());
            userCard2.FullName = userCard2.FirstName + " " + userCard2.LastName;
            foreach (var item in userCard2.UserOfGroups)
            {
                item.UserID = userCard2.ID;
            }
            user = Mapper.Map<HT_Users>(userCard2);
        }
        /// <summary>
        /// Tạo user cho đại lý
        /// </summary>
        /// <param name="id">Id Đại Lý</param>
        /// <param name="u">User name</param>
        /// <param name="g">Group code</param>
        /// <returns></returns>
        public ActionResult CreateAgency(int id, string u, string g)
        {
            try
            {
                u = u.Trim();
                RandomNumberGenerator randomNumber = new RandomNumberGenerator();
                var _pass = randomNumber.RandomNumber(10000, 99999);
                var agencyInfo = Mapper.Map<AgencyCard>(_agenctyService.GetBy2(id));
                agencyInfo.HasAccount = true;
                agencyInfo.IsFirstLogin = true;
                agencyInfo.UserName = u;
                agencyInfo.DateCreate = DateTime.Now;
                hasValue = _agenctyService.Update(Mapper.Map<M_Agency>(agencyInfo));
                hasValue = _vuViecService.Commit();

                userCard = new UserCard()
                {
                    ID = _userService.GetID(),
                    Password = CryptorEngine.Encrypt(_pass.ToString(), true, "Hieu.Le-GC.Soft"),
                    Username = u,
                    Email = agencyInfo.Email,
                    Phone = agencyInfo.Phone,
                    Description = agencyInfo.PackageName,
                    Position = agencyInfo.LeavelName,
                    FirstName = agencyInfo.FirstName,
                    LastName = agencyInfo.LastName,
                    FullName = agencyInfo.FullName,
                    Address = agencyInfo.Address
                };
                hasValue = _userService.Insert2(Mapper.Map<HT_Users>(userCard));
                hasValue = _vuViecService.Commit();
                var userInfo = Mapper.Map<UserCard>(_userService.GetBy(userCard.Username));
                var _userThuocNhom = new UserThuocNhomVM()
                {
                    UserID = userInfo.ID,
                    UserGroupCode = g
                };
                hasValue = _userThuocNhomService.Insert2(Mapper.Map<HT_UserThuocNhom>(_userThuocNhom));
                hasValue = _vuViecService.Commit();
                hasValue = _userThuocNhomService.Delete("ADMIN_HIENTAI", userInfo.ID);
                if (agencyInfo.IsOutlierAdmin && agencyInfo.HasAccount)
                {
                    _userThuocNhom = new UserThuocNhomVM()
                    {
                        UserID = userInfo.ID,
                        UserGroupCode = "ADMIN_HIENTAI"
                    };
                    hasValue = _userThuocNhomService.Insert2(Mapper.Map<HT_UserThuocNhom>(_userThuocNhom));
                    hasValue = _vuViecService.Commit();
                    var teacherInfo = new O_TeacherInfo()
                    {
                        Code = agencyInfo.Id.ToString(),
                        TeacherType = 0,
                        FullName = agencyInfo.FullName,
                        Phone = agencyInfo.Phone,
                        Email = agencyInfo.Email,
                        Username = agencyInfo.UserName,
                        WorkDate = DateTime.Now,
                        ParentTeacherId = null,
                        AgencyId = agencyInfo.Id,
                        RoleCode = "ADMIN_HIENTAI",
                        xRoleCode = "",
                        HasAccount = true
                    };
                    using (var conn = new SqlConnection(hienTaiConnection))
                    {
                        if (conn.State == System.Data.ConnectionState.Closed)
                            conn.Open();
                        var paramaters = new DynamicParameters();
                        paramaters.Add("@Action", "INSERT");
                        foreach (PropertyInfo propertyInfo in teacherInfo.GetType().GetProperties())
                        {
                            paramaters.Add("@" + propertyInfo.Name, propertyInfo.GetValue(teacherInfo, null));
                        }
                        paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                        paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                        var result = conn.Execute("SP2_O_Teacher", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    }
                }
                var error = string.IsNullOrEmpty(hasValue) ? "Oke" : "Error";
                var msg = string.IsNullOrEmpty(hasValue) ? "" : hasValue;
                return Json(new { s = error, url = msg }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { s = "Error", url = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
            
        }
        #endregion
    }
}
