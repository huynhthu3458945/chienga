﻿using System.Web.Mvc;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using System.Net;

namespace GCSOFT.MVC.Web.Areas.Administrator.Controllers
{
    public class TestController : BaseController
    {
        public TestController(IVuViecService vuViecService) : base(vuViecService)
        {
        }

        public ActionResult Index()
        {
            return View();
        }
    }
}