﻿using AutoMapper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Areas.Agency.Data;
using GCSOFT.MVC.Web.Areas.MasterData.Models;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.Agency.Controllers
{
    [CompressResponseAttribute]
    public class AgencyAllController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IAgencyService _agencyService;
        private readonly IPackageService _packageService;
        private readonly IProvinceService _provinceService;
        private readonly IDistrictService _districtService;
        private readonly IAgencyCardService _agencyCardService;
        private readonly IUserService _userService;
        private string sysCategory = "AGENCYALL";
        private bool? permission = false;
        private AgencyCard agencyCard;
        private M_Agency mAgency;
        private string hasValue;
        #endregion

        #region -- Contructor --
        public AgencyAllController
            (
                IVuViecService vuViecService
                , IAgencyService agencyService
                , IPackageService packageService
                , IProvinceService provinceService
                , IDistrictService districtService
                , IAgencyCardService agencyCardService
                , IUserService userService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _agencyService = agencyService;
            _packageService = packageService;
            _provinceService = provinceService;
            _districtService = districtService;
            _agencyCardService = agencyCardService;
            _userService = userService;
        }
        
        #endregion
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }
        public ActionResult Edit(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            agencyCard = Mapper.Map<AgencyCard>(_agencyService.GetBy(id));
            if (agencyCard == null)
                return HttpNotFound();
            ViewBag.hasAccount = agencyCard.HasAccount;
            agencyCard.PackageList = Mapper.Map<IEnumerable<PackageList>>(_packageService.FindAll()).ToSelectListItems(agencyCard.PackageId);
            agencyCard.CityList = Mapper.Map<IEnumerable<ProvinceViewModel>>(_provinceService.FindAll()).ToSelectListItems(agencyCard.CityId ?? 0);
            agencyCard.DistrictList = Mapper.Map<IEnumerable<DistrictViewModel>>(_districtService.FindAll(agencyCard.CityId ?? 0)).ToSelectListItems(agencyCard.DistrictId ?? 0);
            agencyCard.RegistrationDateStr = string.Format("{0:dd/MM/yyyy}", agencyCard.RegistrationDate);
            agencyCard.DateBlockStr = string.Format("{0:dd/MM/yyyy}", agencyCard.DateBlock);
            agencyCard.PasswordAgency = CryptorEngine.Decrypt(Mapper.Map<UserCard>(_userService.GetBy(agencyCard.UserName)).Password, true, "Hieu.Le-GC.Soft");
            agencyCard.ParentAgencyInfo = Mapper.Map<AgencyList>(_agencyService.GetBy(agencyCard.ParentAgencyId ?? 0)) ?? new AgencyList();
            return View(agencyCard);
        }

        public JsonResult LoadData(GridSettings grid)
        {
            var types = new List<AgencyType>();
            types.Add(AgencyType.Agency);
            types.Add(AgencyType.Partner);
            types.Add(AgencyType.RetailCustomers);
            var list = Mapper.Map<IEnumerable<AgencyList>>(_agencyService.FindAll(types)).AsQueryable();
            list = JqGrid.SetupGrid<AgencyList>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult LegalForm(int id, string type)
        {
            var _agencyInfo = Mapper.Map<AgencyCard>(_agencyService.GetBy(id)) ?? new AgencyCard();
            _agencyInfo.CityList = Mapper.Map<IEnumerable<ProvinceViewModel>>(_provinceService.FindAll()).ToSelectListItems(0);
            _agencyInfo.DistrictList = Mapper.Map<IEnumerable<DistrictViewModel>>(_districtService.FindAll(0)).ToSelectListItems(0);
            if (type.Equals("Cá Nhân"))
                return PartialView("_personal", _agencyInfo);
            else
                return PartialView("_Company", _agencyInfo);
        }
    }
}