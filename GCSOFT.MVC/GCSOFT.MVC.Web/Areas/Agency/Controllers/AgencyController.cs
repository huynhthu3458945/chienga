﻿using AutoMapper;
using Dapper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Model.SystemModels;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Areas.Agency.Data;
using GCSOFT.MVC.Web.Areas.MasterData.Models;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels.HienTai;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.Agency.Controllers
{
    [CompressResponseAttribute]
    public class AgencyController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IAgencyService _agencyService;
        private readonly IPackageService _packageService;
        private readonly IProvinceService _provinceService;
        private readonly IDistrictService _districtService;
        private readonly IAgencyCardService _agencyCardService;
        private readonly IUserService _userService;
        private readonly IUserGroupService _userGroupService;
        private readonly IUserThuocNhomService _userThuocNhomService;
        private string sysCategory = "AGENCY";
        private string hienTaiConnection = ConfigurationManager.ConnectionStrings["HienTaiConnection"].ConnectionString;
        private bool? permission = false;
        private AgencyCard agencyCard;
        private M_Agency mAgency;
        private string hasValue;
        #endregion

        #region -- Contructor --
        public AgencyController
            (
                IVuViecService vuViecService
                , IAgencyService agencyService
                , IPackageService packageService
                , IProvinceService provinceService
                , IDistrictService districtService
                , IAgencyCardService agencyCardService
                , IUserService userService
                , IUserGroupService userGroupService
                , IUserThuocNhomService userThuocNhomService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _agencyService = agencyService;
            _packageService = packageService;
            _provinceService = provinceService;
            _districtService = districtService;
            _agencyCardService = agencyCardService;
            _userService = userService;
            _userGroupService = userGroupService;
            _userThuocNhomService = userThuocNhomService;
        }
        
        #endregion
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }
        public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            agencyCard = new AgencyCard();
            agencyCard.PackageList = Mapper.Map<IEnumerable<PackageList>>(_packageService.FindAll()).ToSelectListItems(0);
            var cityList = _provinceService.FindAll().ToList();
            agencyCard.CityList = Mapper.Map<IEnumerable<ProvinceViewModel>>(_provinceService.FindAll()).ToSelectListItems(0);
            agencyCard.DistrictList = Mapper.Map<IEnumerable<DistrictViewModel>>(_districtService.FindAll(0)).ToSelectListItems(0);
            agencyCard.Legal = "Cá Nhân";
            ViewBag.hasAccount = false;
            ViewBag.IsAccounting = (GetPermission(sysCategory, Authorities.Access) ?? false) ? "" : "disabled";
            return View(agencyCard);
        }

        [HttpPost]
        public ActionResult Create(AgencyCard _agencyCard)
        {
            try
            {
                SetData(_agencyCard, true);
                hasValue = _agencyService.Insert(mAgency);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = mAgency.Id, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        public ActionResult Edit(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            agencyCard = Mapper.Map<AgencyCard>(_agencyService.GetBy(id));
            if (agencyCard == null)
                return HttpNotFound();
            ViewBag.hasAccount = agencyCard.HasAccount;
            agencyCard.PackageList = Mapper.Map<IEnumerable<PackageList>>(_packageService.FindAll()).ToSelectListItems(agencyCard.PackageId);
            agencyCard.CityList = Mapper.Map<IEnumerable<ProvinceViewModel>>(_provinceService.FindAll()).ToSelectListItems(agencyCard.CityId ?? 0);
            agencyCard.DistrictList = Mapper.Map<IEnumerable<DistrictViewModel>>(_districtService.FindAll(agencyCard.CityId ?? 0)).ToSelectListItems(agencyCard.DistrictId ?? 0);
            agencyCard.RegistrationDateStr = string.Format("{0:dd/MM/yyyy}", agencyCard.RegistrationDate);
            agencyCard.DateBlockStr = string.Format("{0:dd/MM/yyyy}", agencyCard.DateBlock);
            ViewBag.IsAccounting = (GetPermission(sysCategory, Authorities.Access) ?? false) ? "" : "disabled";
            return View(agencyCard);
        }

        [HttpPost]
        public ActionResult Edit(AgencyCard _agencyCard)
        {
            try
            {
                SetData(_agencyCard, false);
                hasValue = _agencyService.Update(mAgency);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                {
                    AddRoleHienTaiToAgency(mAgency);
                    return RedirectToAction("Edit", new { id = mAgency.Id, msg = "1" });
                }
                    
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        public ActionResult Deletes(string id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                int[] _codes = id.Split(',').Select(item => int.Parse(item)).ToArray();
                var teacherImgs = _agencyService.FindAll(_codes).Where(d => !string.IsNullOrEmpty(d.Image)).ToList();

                hasValue = _agencyService.Delete(_codes);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                {
                    UploadHelper _upload = new UploadHelper();
                    string pathImgTeacher = HttpContext.Server.MapPath("~/Files/Agencry/");
                    teacherImgs.ForEach(item =>
                    {
                        _upload.DeleteFileOnServer(pathImgTeacher + item.Id.ToString() + "/", item.Image);
                    });
                    //-- Delete Foloder
                    if (Directory.Exists(pathImgTeacher + teacherImgs.Select(d => d.Id).FirstOrDefault()))
                        Directory.Delete(pathImgTeacher + teacherImgs.Select(d => d.Id).FirstOrDefault(), true);
                    return Json(new { v = true, count = _codes.Count() });
                }
                return Json("Xóa dữ liệu không thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }
        public JsonResult RemoveImg(int id, string img)
        {
            hasValue = _agencyService.DeleteImg(id);
            hasValue = _vuViecService.Commit();
            if (string.IsNullOrEmpty(hasValue))
            {
                UploadHelper _upload = new UploadHelper();
                string PathSave = HttpContext.Server.MapPath("~/Files/Agencry/" + id.ToString() + "/");
                _upload.DeleteFileOnServer(PathSave, img);
            }
            return Json(string.IsNullOrEmpty(hasValue));
        }
        public JsonResult ShowWebsite(int id)
        {
            var agencyInfo = _agencyService.GetBy2(id);
            if (string.IsNullOrEmpty(agencyInfo.Image))
            {
                return Json(new { code = 1, msg = "Chưa có hình ảnh" });
            }
            agencyInfo.DateCreate = DateTime.Now;
            agencyInfo.RatingCode = "100";
            hasValue = _agencyService.Update(agencyInfo);
            hasValue = _vuViecService.Commit();
            if (string.IsNullOrEmpty(hasValue))
                return Json(new { code = 0, msg = "Hiển thị hình ảnh lên website thành công" });
            else
                return Json(new { code = 1, msg = hasValue });
        }
        public JsonResult LoadData(GridSettings grid)
        {
            var list = Mapper.Map<IEnumerable<AgencyList>>(_agencyService.FindAll(AgencyType.Agency, null)).AsQueryable();
            list = JqGrid.SetupGrid<AgencyList>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
        private void SetData(AgencyCard _agencyCard, bool isCreate)
        {
            agencyCard = _agencyCard;
            if (isCreate)
                agencyCard.Id = _agencyService.GetID();
            var packageInfo = Mapper.Map<PackageList>(_packageService.GetBy(agencyCard.PackageId));
            agencyCard.PackageName = packageInfo.Name;
            agencyCard.LeavelName = packageInfo.LeavelName;
            if (!string.IsNullOrEmpty(agencyCard.RegistrationDateStr))
                agencyCard.RegistrationDate = DateTime.Parse(StringUtil.ConvertStringToDate(agencyCard.RegistrationDateStr));
            else
                agencyCard.RegistrationDate = null;
            if (!string.IsNullOrEmpty(agencyCard.DateBlockStr))
                agencyCard.DateBlock = DateTime.Parse(StringUtil.ConvertStringToDate(agencyCard.DateBlockStr));
            else
                agencyCard.DateBlock = null;
            var provinceInfo = Mapper.Map<ProvinceViewModel>(_provinceService.GetBy(agencyCard.CityId ?? 0)) ?? new ProvinceViewModel();
            agencyCard.CityName = provinceInfo.name;
            var districtInfo = Mapper.Map<DistrictViewModel>(_districtService.GetBy(agencyCard.DistrictId ?? 0)) ?? new DistrictViewModel();
            agencyCard.DistrictName = districtInfo.name;
            UploadHelper _uploadHelper = new UploadHelper();
            _uploadHelper.PathSave = HttpContext.Server.MapPath("~/Files/Agencry/" + agencyCard.Id.ToString() + "/");
            _uploadHelper.UploadFile(Request.Files["fileUpload"]);
            if (!string.IsNullOrEmpty(_uploadHelper.FileSave))
                agencyCard.Image = _uploadHelper.FileSave;

            mAgency = Mapper.Map<M_Agency>(agencyCard);
        }
        public ActionResult LegalForm(int id, string type)
        {
            var _agencyInfo = Mapper.Map<AgencyCard>(_agencyService.GetBy(id)) ?? new AgencyCard();
            _agencyInfo.CityList = Mapper.Map<IEnumerable<ProvinceViewModel>>(_provinceService.FindAll()).ToSelectListItems(0);
            _agencyInfo.DistrictList = Mapper.Map<IEnumerable<DistrictViewModel>>(_districtService.FindAll(0)).ToSelectListItems(0);
            if (type.Equals("Cá Nhân"))
                return PartialView("_personal", _agencyInfo);
            else
                return PartialView("_Company", _agencyInfo);
        }
        public string AddRoleHienTaiToAgency(M_Agency mAgency)
        {
            try
            {
                var userInfo = Mapper.Map<UserCard>(_userService.GetBy(mAgency.UserName));
                hasValue = _userThuocNhomService.Delete("ADMIN_HIENTAI", userInfo.ID);
                if (mAgency.IsOutlierAdmin && mAgency.HasAccount)
                {
                    var _userThuocNhom = new UserThuocNhomVM()
                    {
                        UserID = userInfo.ID,
                        UserGroupCode = "ADMIN_HIENTAI"
                    };
                    hasValue = _userThuocNhomService.Insert2(Mapper.Map<HT_UserThuocNhom>(_userThuocNhom));
                    hasValue = _vuViecService.Commit();
                    var teacherInfo = new O_TeacherInfo()
                    {
                        Code = mAgency.Code.ToString(),
                        TeacherType = 0,
                        FullName = mAgency.FullName,
                        Phone = mAgency.Phone,
                        Email = mAgency.Email,
                        Username = mAgency.UserName,
                        WorkDate = DateTime.Now,
                        ParentTeacherId = null,
                        AgencyId = mAgency.Id,
                        RoleCode = "ADMIN_HIENTAI",
                        xRoleCode = "",
                        HasAccount = true,
                        NoOfOutlier = mAgency.NoOfOutlier
                    };
                    using (var conn = new SqlConnection(hienTaiConnection))
                    {
                        if (conn.State == System.Data.ConnectionState.Closed)
                            conn.Open();
                        var paramaters = new DynamicParameters();
                        paramaters.Add("@Action", "INSERT_FROM_TTL");
                        foreach (PropertyInfo propertyInfo in teacherInfo.GetType().GetProperties())
                        {
                            paramaters.Add("@" + propertyInfo.Name, propertyInfo.GetValue(teacherInfo, null));
                        }
                        paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                        paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                        var result = conn.Execute("SP2_O_Teacher", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    }
                }
                return "";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            
        }
    }
}