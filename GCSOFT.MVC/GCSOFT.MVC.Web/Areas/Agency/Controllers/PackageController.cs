﻿using AutoMapper;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Areas.Agency.Data;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.Agency.Controllers
{
    [CompressResponseAttribute]
    public class PackageController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IPackageService _packedService;
        private readonly IAgencyService _agencyService;
        private string sysCategory = "PACKAGE";
        private bool? permission = false;
        private PackageCard packageCard;
        private M_Package mPackege;
        private string hasValue;
        #endregion

        #region -- Contructor --
        public PackageController
            (
                IVuViecService vuViecService
                , IPackageService packedService
                , IAgencyService agencyService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _packedService = packedService;
            _agencyService = agencyService;
        }
        #endregion
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }
        public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            packageCard = new PackageCard();
            return View(packageCard);
        }
        public ActionResult Edit(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            packageCard = Mapper.Map<PackageCard>(_packedService.GetBy(id));
            if (packageCard == null)
                return HttpNotFound();
            return View(packageCard);
        }
        public ActionResult Details(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.ViewDetail);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            packageCard = Mapper.Map<PackageCard>(_packedService.GetBy(id));
            if (packageCard == null)
                return HttpNotFound();
            return View(packageCard);
        }
        public ActionResult Deletes(string id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                int[] _codes = id.Split(',').Select(item => int.Parse(item)).ToArray();
                hasValue = _packedService.Delete(_codes);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return Json(new { v = true, count = _codes.Count() });
                return Json("Xóa dữ liệu không thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }
        public JsonResult LoadData(GridSettings grid)
        {
            var list = Mapper.Map<IEnumerable<PackageList>>(_packedService.FindAll()).AsQueryable();
            list = JqGrid.SetupGrid<PackageList>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
        private void SetData(PackageCard _packageCard, bool isCreate)
        {
            packageCard = _packageCard;
            if (isCreate)
                packageCard.Id = _packedService.GetID();
            mPackege = Mapper.Map<M_Package>(packageCard);
        }
        [HttpPost]
        public ActionResult Create(PackageCard _packageCard)
        {
            try
            {
                SetData(_packageCard, true);
                hasValue = _packedService.Insert(mPackege);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = mPackege.Id, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult Edit(PackageCard _packageCard)
        {
            try
            {
                SetData(_packageCard, false);
                hasValue = _packedService.Update(mPackege);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = mPackege.Id, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        /// <summary>
        /// Get Packed Info from Agency ID
        /// </summary>
        /// <param name="a">Agency Id</param>
        /// <returns></returns>
        public JsonResult PackageInfo(int a)
        {
            try
            {
                var agencyInfo = Mapper.Map<AgencyCard>(_agencyService.GetBy(a));
                var packedInfo = Mapper.Map<PackageCard>(_packedService.GetBy(agencyInfo.PackageId));
                return Json(packedInfo, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new PackageCard(), JsonRequestBehavior.AllowGet);
            }
            
        }
    }
}