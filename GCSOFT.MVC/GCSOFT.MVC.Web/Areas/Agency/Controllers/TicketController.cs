﻿using AutoMapper;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Areas.Agency.Data;
using GCSOFT.MVC.Web.Areas.MasterData.Models;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.Agency.Controllers
{
    [CompressResponseAttribute]
    public class TicketController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly ITicketService _ticketService;
        private readonly IOptionLineService _optionLineService;
        private readonly IUserService _userService;
        private readonly IUserThuocNhomService _userThuocNhomService;
        private readonly IMailSetupService _mailSetupService;
        private string sysCategory = "TICKET";
        private bool? permission = false;
        private TicketViewModel ticketViewModel;
        private M_Ticket mTicket;
        private string hasValue;
        #endregion

        #region -- Contructor --
        public TicketController
            (
                IVuViecService vuViecService
                , ITicketService ticketService
                , IOptionLineService optionLineService
                , IUserService userService
                , IUserThuocNhomService userThuocNhomService
                , IMailSetupService mailSetupService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _ticketService = ticketService;
            _optionLineService = optionLineService;
            _userService = userService;
            _userThuocNhomService = userThuocNhomService;
            _mailSetupService = mailSetupService;
        }
        #endregion
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }
        public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var userIds = _userThuocNhomService.GetMany("TICKET").Select(d => d.UserID).ToList() ?? new List<int>();
            ticketViewModel = new TicketViewModel();
            ticketViewModel.SendBy = GetUser().Username;
            ticketViewModel.SendByName = GetUser().FullName;
            ticketViewModel.DateCreateStr = string.Format("{0:dd/MM/yyyy}", ticketViewModel.DateCreate);
            ticketViewModel.DateModifyStr = string.Format("{0:dd/MM/yyyy}", ticketViewModel.DateModify);
            ticketViewModel.SystemList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("SYSTEM")).ToSelectListItems("");
            ticketViewModel.StatusList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("STA_TICKED")).ToSelectListItems("");
            ticketViewModel.PICList = Mapper.Map<IEnumerable<UserVM>>(_userService.FindAll(userIds)).ToList().ToSelectListItems("");
            ticketViewModel.TicketList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("HIENTAI_TICKET_TYPE")).ToSelectListItems("");
            return View(ticketViewModel);
        }
        public ActionResult Edit(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            ticketViewModel = Mapper.Map<TicketViewModel>(_ticketService.GetBy(id));
            if (ticketViewModel == null)
                return HttpNotFound();
            ticketViewModel.DateCreateStr = string.Format("{0:dd/MM/yyyy}", ticketViewModel.DateCreate);
            ticketViewModel.DateModifyStr = string.Format("{0:dd/MM/yyyy}", ticketViewModel.DateModify);
            var userIds = _userThuocNhomService.GetMany("TICKET").Select(d => d.UserID).ToList() ?? new List<int>();
            ticketViewModel.SystemList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("SYSTEM")).ToSelectListItems(ticketViewModel.SystemCode);
            ticketViewModel.StatusList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("STA_TICKED")).ToSelectListItems(ticketViewModel.StatusCode);
            ticketViewModel.PICList = Mapper.Map<IEnumerable<UserVM>>(_userService.FindAll(userIds)).ToList().ToSelectListItems(ticketViewModel.PIC);
            ticketViewModel.TicketList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("HIENTAI_TICKET_TYPE")).ToSelectListItems(ticketViewModel.TicketType);
            return View(ticketViewModel);
        }
        public ActionResult Deletes(string id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                int[] _codes = id.Split(',').Select(item => int.Parse(item)).ToArray();
                hasValue = _ticketService.Delete(_codes);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return Json(new { v = true, count = _codes.Count() });
                return Json("Xóa dữ liệu không thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }
        public JsonResult LoadData(GridSettings grid)
        {
            var list = Mapper.Map<IEnumerable<TicketList>>(_ticketService.FindAll()).AsQueryable();
            list = JqGrid.SetupGrid<TicketList>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
        private void SetData(TicketViewModel _tickedViewModel, bool isCreate)
        {
            ticketViewModel = _tickedViewModel;
            if (isCreate)
            {
                ticketViewModel.DateCreate = DateTime.Now;
                ticketViewModel.SendBy = GetUser().Username;
                ticketViewModel.SendByName = GetUser().FullName;
            }

            ticketViewModel.DateModify = DateTime.Now;


            var picInfo = Mapper.Map<UserVM>(_userService.GetBy(ticketViewModel.PIC));
            ticketViewModel.PICName = picInfo.FullName;

            var systemInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("SYSTEM", ticketViewModel.SystemCode)) ?? new VM_OptionLine();
            ticketViewModel.SystemId = systemInfo.LineNo;
            ticketViewModel.SystemName = systemInfo.Name;

            var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("STA_TICKED", ticketViewModel.StatusCode)) ?? new VM_OptionLine();
            ticketViewModel.StatusId = statusInfo.LineNo;
            ticketViewModel.StatusName = statusInfo.Name;

            mTicket = Mapper.Map<M_Ticket>(ticketViewModel);
        }
        [HttpPost]
        public ActionResult Create(TicketViewModel _tickedViewModel)
        {
            try
            {
                SetData(_tickedViewModel, true);
                hasValue = _ticketService.Insert(mTicket);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = mTicket.Id, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult Edit(TicketViewModel _tickedViewModel)
        {
            try
            {
                SetData(_tickedViewModel, false);
                hasValue = _ticketService.Update(mTicket);
                hasValue = _vuViecService.Commit();
                SendMail(_tickedViewModel);
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = mTicket.Id, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        public void SendMail(TicketViewModel model)
        {
            var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
            var emailTos = new List<string>();
            emailTos.Add(model.SendByEmail);
            var _mailHelper = new EmailHelper()
            {
                EmailTos = emailTos,
                DisplayName = "Đào Tạo Hiền Tài",
                Title = "Phản Hồi Ticket",
                Body = $"<p>Chào <strong>{model.SendByName}!</strong></p>"
                + $"<p>Bạn Vừa được nhập 1 phản hồi IT của Mã Ticket: <strong> {model.Code} </strong></p>"
                + $"<p>Họ Tên: <strong>{GetUser().FullName}</strong></p>"
                + $"<p>Nội Dung: </p>"
                + $"    <p>+ Nguyên nhân: <strong>{model.Reason}</strong></p>"
                + $"    <p>+ Khắc phục: <strong>{model.Overcome}</strong></p>"
                + $"    <p>+ Hướng sử lý tiếp theo: <strong>{model.SolutionNet}</strong></p>"
                + $"<p><strong>Đường dẫn: <a href=\"https://agency.stnhd.com/HienTai/Ticket/Edit/{model.Id}\">https://agency.stnhd.com/HienTai/Ticket/Edit/{model.Id}</a><strong></p>",
                MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
            };
            _mailHelper.SendEmailsThread();
        }

    }
}