﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.Agency.Data
{
    public class TicketList
    {
        public int Id { get; set; }
        [DisplayName("Tiêu Đề")]
        public string Title { get; set; }
        [DisplayName("Ngày Tạo")]
        public DateTime DateCreate { get; set; }
        public string DateCreateStr { get { return string.Format("{0:dd/MM/yyyy}", DateCreate); } }
        [DisplayName("Người Gửi Yêu Cầu")]
        public string SendByName { get; set; }
        [DisplayName("Người Tiếp Nhận")]
        public string PICName { get; set; }
        [DisplayName("Trạng Thái")]
        public string StatusName { get; set; }

        [DisplayName("Mã Ticket")]
        public string Code { get; set; }
    }
    public class TicketViewModel
    {
        public int Id { get; set; }
        [DisplayName("Tiêu Đề")]
        public string Title { get; set; }
        [DisplayName("Câu Hỏi")]
        [AllowHtml]
        public string Question { get; set; }
        [DisplayName("Câu Trả Lời")]
        [AllowHtml]
        public string Anser { get; set; }

        public DateTime DateCreate { get; set; }
        [DisplayName("Ngày Tạo")]
        public string DateCreateStr { get; set; }
        
        public DateTime DateModify { get; set; }
        [DisplayName("Ngày Chỉnh Sửa")]
        public string DateModifyStr { get; set; }

        public int SystemId { get; set; }
        [DisplayName("Hệ Thống")]
        public string SystemCode { get; set; }
        public string SystemName { get; set; }
        public string Code { get; set; }
        public string Link { get; set; }

        public int SendById { get; set; }
        public string SendBy { get; set; }
        [DisplayName("Người Gửi Yêu Cầu")]
        public string SendByName { get; set; }
        public string SendByEmail { get; set; }

        [DisplayName("Người Tiếp Nhận")]
        public string PIC { get; set; }
        public string PICName { get; set; }

        public int StatusId { get; set; }
        [DisplayName("Trạng Thái")]
        public string StatusCode { get; set; }
        public string StatusName { get; set; }

        [DisplayName("Nguyên Nhân")]
        public string Reason { get; set; }
        [DisplayName("Khắc Phục")]
        public string Overcome { get; set; }
        [DisplayName("Hướng Xử Lý Tiếp Theo")]
        public string SolutionNet { get; set; }
        [DisplayName("Loại Ticket")]
        public string TicketType { get; set; }
        public IEnumerable<SelectListItem> SystemList { get; set; }
        public IEnumerable<SelectListItem> PICList { get; set; }
        public IEnumerable<SelectListItem> StatusList { get; set; }
        public IEnumerable<SelectListItem> TicketList { get; set; }
        
        public TicketViewModel()
        {
            DateCreate = DateTime.Now;
            DateModify = DateTime.Now;
        }
    }
}