﻿using AutoMapper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.Approval;
using GCSOFT.MVC.Model.StoreProcedue;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Service.ApprovalService.Interface;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.StoreProcedure.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Service.Transaction.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Areas.Approval.Data;
using GCSOFT.MVC.Web.Areas.MasterData.Models;
using GCSOFT.MVC.Web.Areas.Transaction.Data;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.Approval.Controllers
{
    [CompressResponseAttribute]
    public class ApprovalController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IApprovalInfoService _approvalInfoService;
        private readonly IFlowService _flowService;
        private readonly ISalesHeaderService _salesHdrService;
        private readonly IOptionLineService _optionLineService;
        private readonly ITemplateService _templateService;
        private readonly ISetupService _setupService;
        private readonly IMailSetupService _mailSetupService;
        private readonly IUserService _userService;
        private readonly IStoreProcedureService _storeService;
        private string sysCategory = "APPROVAL";
        private bool? permission = false;
        private string emailTitle;
        private string emailContent;
        private string hasValue;
        #endregion

        #region -- Contructor --
        public ApprovalController
            (
                IVuViecService vuViecService
                , IApprovalInfoService approvalInfoService
                , IFlowService flowService
                , ISalesHeaderService salesHdrService
                , IOptionLineService optionLineService
                , ITemplateService templateService
                , ISetupService setupService
                , IMailSetupService mailSetupService
                , IUserService userService
                , IStoreProcedureService storeService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _approvalInfoService = approvalInfoService;
            _flowService = flowService;
            _salesHdrService = salesHdrService;
            _optionLineService = optionLineService;
            _templateService = templateService;
            _setupService = setupService;
            _mailSetupService = mailSetupService;
            _userService = userService;
            _storeService = storeService;
        }
        #endregion
        #region -- Get --
        public ActionResult Online(string k)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var userInfo = GetUser();
            //var key = CryptorEngine.Decrypt(string.Format("{0}_{1}_{2}_{3}_{4}", f.Type, f.Code, f.ApprovalNo, "HasComment", "Approval"), true, "APRVONL")
            var key = CryptorEngine.Decrypt(k, true, "APRVONL");
            var approvalType = int.Parse(key.Split('_')[0]);
            var documentNo = key.Split('_')[1];
            var approvalNo = key.Split('_')[2];
            var hasComment = key.Split('_')[3];
            var approval = key.Split('_')[4];
            if (approvalNo != userInfo.Username)
                return View("AccessDenied");
            var flowInfo = Mapper.Map<FlowApproval>(_flowService.GetBy((ApprovalType)approvalType, documentNo, approvalNo));
            var approvalInfo = Mapper.Map<ApprovalInfoViewModel>(_approvalInfoService.GetBy((ApprovalType)approvalType, documentNo));
            flowInfo.Description = approvalInfo.Description;
            flowInfo.key = k;
            flowInfo.StatusCode = approvalInfo.StatusCode;
            flowInfo.Type2 = (int)flowInfo.Type;
            flowInfo.ApprovalList = Mapper.Map<IEnumerable<FlowViewModel>>(_flowService.FindAll((ApprovalType)approvalType, documentNo));
            return View(flowInfo);
        }
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var approvalpage = new ApprovalPage()
            {
                StatusList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("APPSTATUS")).ToSelectListItems("100"),
                ApprovalTypeList = CboApprovalType().ToSelectListItems(-1)
            };
            return View(approvalpage);
        }
        public Dictionary<int, string> CboApprovalType()
        {
            Dictionary<int, string> dics = new Dictionary<int, string>();
            dics[ApprovalType.PurchaseOrder.GetHashCode()] = StringUtil.GetDescriptionEnum(ApprovalType.PurchaseOrder);
            dics[ApprovalType.Convert.GetHashCode()] = StringUtil.GetDescriptionEnum(ApprovalType.Convert);
            return dics;
        }
        /// <summary>
        /// Load Approval List
        /// </summary>
        /// <param name="grid"></param>
        /// <param name="at">Approval Type</param>
        /// <param name="sc">Status Code</param>
        /// <returns></returns>
        public JsonResult LoadData(GridSettings grid, int? at, string sc)
        {
            var userInfo = GetUser();
            var approvalList = _storeService.SP_LoadApprovalList(at ?? 0, sc, userInfo.Username).AsQueryable();
            approvalList.ToList().ForEach(item => {
                item.Key = CryptorEngine.Encrypt(string.Format("{0}_{1}_{2}_{3}_{4}", (int)item.ApprovalType, item.Code, item.ApprovalNo, "HasComment", "Approval"), true, "APRVONL");
            });
            approvalList = JqGrid.SetupGrid<SPApprovalList>(grid, approvalList);
            return Json(approvalList.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region -- Json --
        /// <summary>
        /// Send to Approval
        /// </summary>
        /// <param name="t">Approval Type</param>
        /// <param name="c">Code</param>
        /// <returns></returns>
        public JsonResult POSendToApproval(string c)
        {
            try
            {
                var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("ORDERSTATUS", "300"));
                var purchaseOrder = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(SalesType.PurchaseOrder, c));
                purchaseOrder.StatusId = statusInfo.LineNo;
                purchaseOrder.StatusCode = statusInfo.Code;
                purchaseOrder.StatusName = statusInfo.Name;
                hasValue = _salesHdrService.Update(Mapper.Map<M_SalesHeader>(purchaseOrder));

                //Get email nguoi duyệt.
                var priorities = Mapper.Map<IEnumerable<FlowViewModel>>(_flowService.FindAll(ApprovalType.PurchaseOrder, c));
                var minPriority = priorities.Where(d => d.StatusCode.Equals("100")).LastOrDefault().Priority;
                var flowInfo = Mapper.Map<IEnumerable<FlowViewModel>>(_flowService.FindAll(ApprovalType.PurchaseOrder, c, minPriority)).FirstOrDefault();
                statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("APPSTATUS", "100"));
                var userInfo = GetUser();
                var approvalInfo = new ApprovalInfoViewModel()
                {
                    Type = ApprovalType.PurchaseOrder,
                    Code = c,
                    SendBy = userInfo.Username,
                    SendByName = userInfo.FullName,
                    SendDate = DateTime.Now,
                    StatusId = statusInfo.LineNo,
                    StatusCode = statusInfo.Code,
                    StatusName = statusInfo.Name,
                    ApprovalNo = flowInfo.ApprovalNo,
                    ApprovalName = flowInfo.ApprovalName,
                    ApprovalEmail = flowInfo.ApprovalEmail,
                    Description = string.Format("{0} của đại lý {1} số lượng thẻ {2}", purchaseOrder.Code, purchaseOrder.CustomerName, purchaseOrder.TotalCard)
                };

                var _approvalInfo = Mapper.Map<ApprovalInfoViewModel>(_approvalInfoService.GetBy(ApprovalType.PurchaseOrder, c));
                if (_approvalInfo == null)
                    hasValue = _approvalInfoService.Insert(Mapper.Map<M_ApprovalInfo>(approvalInfo));
                else
                    hasValue = _approvalInfoService.Update(Mapper.Map<M_ApprovalInfo>(approvalInfo));
                BuiltEmailPO(purchaseOrder, flowInfo, approvalInfo, priorities);
                var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());

                var emailTos = new List<string>();
                emailTos.Add(flowInfo.ApprovalEmail);
                var _mailHelper = new EmailHelper()
                {
                    EmailTos = emailTos,
                    DisplayName = emailInfo.DisplayName,
                    Title = emailTitle,
                    Body = emailContent,
                    MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
                };
                hasValue = _mailHelper.DoSendMail();
                hasValue = _vuViecService.Commit();
                var _status = string.IsNullOrEmpty(hasValue) ? "Oke" : "Error";
                var _msg = string.IsNullOrEmpty(hasValue) ? "" : hasValue;
                return Json(new { s = _status, m = _msg }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { s = "Error", m = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// Cập nhật trạng thái đơn hàng
        /// </summary>
        /// <param name="t">Approval Type</param>
        /// <param name="c">Document No.</param>
        /// <param name="s">Status Code</param>
        /// <param name="p"></param>
        /// <param name="r">Remark</param>
        /// <returns></returns>
        public JsonResult ApprovalOrRejcectPO(int t, string c, string s, int p, string r)
        {
            try
            {
                var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
                var purchaseOrder = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(SalesType.PurchaseOrder, c));
                var flowInfo = Mapper.Map<FlowViewModel>(_flowService.GetBy((ApprovalType)t, c, GetUser().Username));
                var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("APPSTATUS", s));
                flowInfo.DateCfm = DateTime.Now;
                flowInfo.StatusId = statusInfo.LineNo;
                flowInfo.StatusCode = statusInfo.Code;
                flowInfo.StatusName = statusInfo.Name;
                flowInfo.Remark = r;
                hasValue = _flowService.Update(Mapper.Map<M_Flow>(flowInfo));
                hasValue = _vuViecService.Commit();
                var priorities = Mapper.Map<IEnumerable<FlowViewModel>>(_flowService.FindAll((ApprovalType)t, c));
                var approvalInfo = Mapper.Map<ApprovalInfoViewModel>(_approvalInfoService.GetBy((ApprovalType)t, c));
                var nextFlowInfo = Mapper.Map<FlowViewModel>(_flowService.GetNextFlow((ApprovalType)t, c, p));
                if (nextFlowInfo == null)
                {
                    statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("APPSTATUS", s));
                    approvalInfo.StatusId = statusInfo.LineNo;
                    approvalInfo.StatusCode = statusInfo.Code;
                    approvalInfo.StatusName = statusInfo.Name;
                    if (s.Equals("300"))
                        statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("ORDERSTATUS", "1100"));
                    else
                        statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("ORDERSTATUS", "400"));
                    purchaseOrder.StatusId = statusInfo.LineNo;
                    purchaseOrder.StatusCode = statusInfo.Code;
                    purchaseOrder.StatusName = statusInfo.Name;

                    hasValue = _salesHdrService.Update(Mapper.Map<M_SalesHeader>(purchaseOrder));
                    nextFlowInfo = new FlowViewModel()
                    {
                        ApprovalName = approvalInfo.SendByName
                    };
                    BuiltEmailPO(purchaseOrder, nextFlowInfo, approvalInfo, priorities, "Đã Duyệt");

                    var userSend = Mapper.Map<UserVM>(_userService.GetBy(approvalInfo.SendBy));
                    var emailTos = new List<string>();
                    emailTos.Add(userSend.Email);
                    var emailCCs = new List<string>();
                    priorities.ToList().ForEach(item => {
                        emailCCs.Add(item.ApprovalEmail);
                    });
                    var _mailHelper = new EmailHelper()
                    {
                        EmailTos = emailTos,
                        DisplayName = emailInfo.DisplayName,
                        Title = emailTitle,
                        Body = emailContent,
                        MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
                    };
                    hasValue = _mailHelper.DoSendMail();
                }
                else
                {
                    approvalInfo.ApprovalNo = nextFlowInfo.ApprovalNo;
                    approvalInfo.ApprovalName = nextFlowInfo.ApprovalName;
                    approvalInfo.ApprovalEmail = nextFlowInfo.ApprovalEmail;
                    BuiltEmailPO(purchaseOrder, nextFlowInfo, approvalInfo, priorities);
                    var emailTos = new List<string>();
                    emailTos.Add(nextFlowInfo.ApprovalEmail);
                    var _mailHelper = new EmailHelper()
                    {
                        EmailTos = emailTos,
                        DisplayName = emailInfo.DisplayName,
                        Title = emailTitle,
                        Body = emailContent,
                        MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
                    };
                    hasValue = _mailHelper.DoSendMail();
                }
                hasValue = _approvalInfoService.Update(Mapper.Map<M_ApprovalInfo>(approvalInfo));
                hasValue = _vuViecService.Commit();
                var _status = string.IsNullOrEmpty(hasValue) ? "Oke" : "Error";
                var _msg = string.IsNullOrEmpty(hasValue) ? "" : hasValue;
                return Json(new { s = _status, m = _msg, status = s }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex) { return Json(new { s = "Error", m = ex.ToString(), status = "" }, JsonRequestBehavior.AllowGet); }
        }
        #endregion

        #region -- Email Content --
        private void BuiltEmailPO(SalesOrderCard purchaseOrder, FlowViewModel flowInfo, ApprovalInfoViewModel approvalInfo, IEnumerable<FlowViewModel> priorities, string statusName = "Chờ Duyệt")
        {
            var setup = _setupService.Get();
            var templateInfo = Mapper.Map<Template>(_templateService.Get("POSEND"));
            emailTitle = templateInfo.Title.Replace("{{PO_NO}}", purchaseOrder.Code).Replace("{{AGENCY}}", purchaseOrder.CustomerName).Replace("{{NOOFCARD}}", string.Format("{0:#,##0}", purchaseOrder.TotalCard)).Replace("{{USER}}", approvalInfo.SendByName).Replace("{{STATUS_NAME}}", statusName);

            var content = templateInfo.Content;
            content = content.Replace("{{FULLNAME}}", flowInfo.ApprovalName);
            content = content.Replace("{{PO_NO}}", purchaseOrder.Code);
            content = content.Replace("{{DOCUMENT_DATE}}", purchaseOrder.DocumentDateStr);
            content = content.Replace("{{AGENCY_NAME}}", purchaseOrder.CustomerName);
            content = content.Replace("{{AGENCY_ADD}}", purchaseOrder.CustomerAddress);
            content = content.Replace("{{AGENCY_EMAIL}}", purchaseOrder.CustomerEmail);
            content = content.Replace("{{AGENCY_PHONE}}", purchaseOrder.CustomerPhoneNo);
            content = content.Replace("{{NOOFCARD}}", string.Format("{0:#,##0}", purchaseOrder.NoOfCard));
            content = content.Replace("{{GIFT_CARD}}", string.Format("{0:#,##0}", purchaseOrder.GiftCard));
            content = content.Replace("{{AMOUNT}}", string.Format("{0:#,##0}", purchaseOrder.Amount));
            content = content.Replace("{{DISCOUNTPERCENT}}", string.Format("{0:#,##0}%", purchaseOrder.DiscountPercent));
            content = content.Replace("{{DISCOUNT_AMT}}", string.Format("{0:#,##0}", purchaseOrder.DiscountAmt));
            content = content.Replace("{{TOTALAMOUNT}}", string.Format("{0:#,##0}", purchaseOrder.TotalAmount));
            content = content.Replace("{{REMARK}}", purchaseOrder.Remark);

            //var keyApproval = CryptorEngine.Encrypt(string.Format("{0}_{1}_{2}_{3}_{4}", (int)ApprovalType.PurchaseOrder, purchaseOrder.Code, flowInfo.ApprovalNo, "NoComment", "Approval"), true, "APRVONL");
            //content = content.Replace("{{LinkApprovalNoComment}}", string.Format("{0}/Approval/Approval/Online?k={1}", setup.DomainBackEnd, keyApproval));

            var keyApproval = HttpUtility.UrlEncode(CryptorEngine.Encrypt(string.Format("{0}_{1}_{2}_{3}_{4}", (int)ApprovalType.PurchaseOrder, purchaseOrder.Code, flowInfo.ApprovalNo, "HasComment", "Approval"), true, "APRVONL"));
            content = content.Replace("{{LinkApprovalWithComment}}", string.Format("{0}/Approval/Approval/Online?k={1}", setup.DomainBackEnd, keyApproval));

            keyApproval = HttpUtility.UrlEncode(CryptorEngine.Encrypt(string.Format("{0}_{1}_{2}_{3}_{4}", (int)ApprovalType.PurchaseOrder, purchaseOrder.Code, flowInfo.ApprovalNo, "HasComment", "Reject"), true, "APRVONL"));
            content = content.Replace("{{LinkRejct}}", string.Format("{0}/Approval/Approval/Online?k={1}", setup.DomainBackEnd, keyApproval));

            var tableAprl = new StringBuilder();
            tableAprl.Append("<table border=\"1\" cellpadding=\"1\" cellspacing=\"0\" style=\"width:100%;\">");
            tableAprl.Append("<tbody>");
            tableAprl.Append("<tr>");
            tableAprl.Append("<td style=\"width: 10%; text-align: center; background-color: rgb(0, 153, 255);\"><span style=\"color:#f0ffff;\"><strong>Bộ Phận</strong></span></td>");
            tableAprl.Append("<td style=\"width: 30%; text-align: center; background-color: rgb(0, 153, 255);\"><span style=\"color:#ffffff;\"><strong>Người Duyệt</strong></span></td>");
            tableAprl.Append("<td style=\"width: 15%; text-align: center; background-color: rgb(0, 153, 255);\"><span style=\"color:#ffffff;\"><strong>Trạng Th&aacute;i</strong></span></td>");
            tableAprl.Append("<td style=\"width: 15%; text-align: center; background-color: rgb(0, 153, 255);\"><span style=\"color:#ffffff;\"><strong>Ng&agrave;y Duyệt</strong></span></td>");
            tableAprl.Append("<td style=\"text-align: center; background-color: rgb(0, 153, 255);\"><span style=\"color:#ffffff;\"><strong>Ghi Ch&uacute;</strong></span></td>");
            tableAprl.Append("</tr>");
            foreach (var item in priorities)
            {
                tableAprl.Append("<tr>");
                tableAprl.Append(string.Format("<td>{0}</td>", item.GroupName));
                tableAprl.Append(string.Format("<td>{0}</td>", item.ApprovalName));
                tableAprl.Append(string.Format("<td style=\"text-align: center;\">{0}</td>", item.StatusName));
                tableAprl.Append(string.Format("<td style=\"text-align: center;\">{0}</td>", item.StatusCode.Equals("100") ? "" : string.Format("{0:dd/MM/yyyy hh:mm}", item.DateCfm)));
                tableAprl.Append(string.Format("<td>{0}</td>", item.Remark));
                tableAprl.Append("</tr>");
            }
            tableAprl.Append("</tbody>");
            tableAprl.Append("</table>");

            content = content.Replace("{{APPROVALINFO}}", tableAprl.ToString());
            emailContent = content;
        }
        #endregion
    }
}