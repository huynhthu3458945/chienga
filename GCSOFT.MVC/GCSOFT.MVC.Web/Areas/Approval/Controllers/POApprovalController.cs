﻿using AutoMapper;
using GCSOFT.MVC.Model.Approval;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Service.ApprovalService.Interface;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Service.Transaction.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Areas.Approval.Data;
using GCSOFT.MVC.Web.Areas.Transaction.Data;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.Approval.Controllers
{
    [CompressResponseAttribute]
    public class POApprovalController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IApprovalInfoService _approvalInfoService;
        private readonly IFlowService _flowService;
        private readonly ISalesHeaderService _salesHdrService;
        private readonly IOptionLineService _optionLineService;
        private readonly ITemplateService _templateService;
        private readonly ISetupService _setupService;
        private readonly IMailSetupService _mailSetupService;
        private readonly IUserService _userService;
        private string sysCategory = "POAPPROVAL";
        private bool? permission = false;
        private string emailTitle;
        private string emailContent;
        private string hasValue;
        #endregion

        #region -- Contructor --
        public POApprovalController
            (
                IVuViecService vuViecService
                , IApprovalInfoService approvalInfoService
                , IFlowService flowService
                , ISalesHeaderService salesHdrService
                , IOptionLineService optionLineService
                , ITemplateService templateService
                , ISetupService setupService
                , IMailSetupService mailSetupService
                , IUserService userService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _approvalInfoService = approvalInfoService;
            _flowService = flowService;
            _salesHdrService = salesHdrService;
            _optionLineService = optionLineService;
            _templateService = templateService;
            _setupService = setupService;
            _mailSetupService = mailSetupService;
            _userService = userService;
        }
        #endregion
        public JsonResult SendToApproval(string c)
        {
            try
            {
                var userInfo = GetUser();
                var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("ORDERSTATUS", "300")); //Chuyển Trạng Thái Sang Đang Duyệt
                var purchaseOrder = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(SalesType.PurchaseOrder, c));
                purchaseOrder.StatusId = statusInfo.LineNo;
                purchaseOrder.StatusCode = statusInfo.Code;
                purchaseOrder.StatusName = statusInfo.Name;
                hasValue = _salesHdrService.Update(Mapper.Map<M_SalesHeader>(purchaseOrder));

                statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("APPSTATUS", "100")); //Chuyển Trạng Thái Sang Chờ Duyệt
                var priorities = Mapper.Map<IEnumerable<FlowViewModel>>(_flowService.FindAll(ApprovalType.PurchaseOrder, c));
                var minFlowInfo = priorities.Where(d => d.StatusCode.Equals("100")).OrderBy(d => d.Priority).FirstOrDefault();
                var nextFlowInfo = Mapper.Map<FlowViewModel>(_flowService.GetNextFlow(ApprovalType.PurchaseOrder, c, minFlowInfo.Priority));

                var approvalInfo = new ApprovalInfoViewModel()
                {
                    Type = ApprovalType.PurchaseOrder,
                    Code = c,
                    SendBy = userInfo.Username,
                    SendByName = userInfo.FullName,
                    SendDate = DateTime.Now,
                    StatusId = statusInfo.LineNo,
                    StatusCode = statusInfo.Code,
                    StatusName = statusInfo.Name,
                    NextPriority = nextFlowInfo.Priority,
                    Description = string.Format("{0} của đại lý {1} số lượng thẻ {2}", purchaseOrder.Code, purchaseOrder.CustomerName, purchaseOrder.TotalCard)
                };
                statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("APPSTATUS", "200"));
                var _flowInit = new FlowViewModel() {
                    Type=ApprovalType.PurchaseOrder,
                    Code = c,
                    ApprovalNo = userInfo.Username,
                    ApprovalName = userInfo.FullName,
                    ApprovalEmail = userInfo.Email,
                    DateCfm = DateTime.Now,
                    StatusId = statusInfo.LineNo,
                    StatusCode = statusInfo.Code,
                    StatusName = statusInfo.Name,
                    GroupCode = "SALES",
                    LineNo = 0,
                    GroupName = "Bộ Phận Bán Hàng"
                };
                var _flowActives = Mapper.Map<IEnumerable<FlowViewModel>>(_flowService.FindAll(ApprovalType.PurchaseOrder, c, minFlowInfo.Priority));
                _flowActives.ToList().ForEach(item => {
                    item.IsActive = 1;
                });
                hasValue = _flowService.Insert(Mapper.Map<M_Flow>(_flowInit));
                hasValue = _approvalInfoService.Delete(ApprovalType.PurchaseOrder, c);
                hasValue = _approvalInfoService.Insert(Mapper.Map<M_ApprovalInfo>(approvalInfo));
                hasValue = _flowService.Update(Mapper.Map<IEnumerable<M_Flow>>(_flowActives));
                hasValue = _vuViecService.Commit();
                var _status = string.IsNullOrEmpty(hasValue) ? "Oke" : "Error";
                var _msg = string.IsNullOrEmpty(hasValue) ? "" : hasValue;
                return Json(new { s = _status, m = _msg }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex) { return Json(new { s = "Error", m = ex.ToString() }, JsonRequestBehavior.AllowGet); }
        }
        /// <summary>
        /// Cập nhật trạng thái đơn hàng
        /// </summary>
        /// <param name="t">Approval Type</param>
        /// <param name="c">Document No.</param>
        /// <param name="s">Status Code</param>
        /// <param name="p"></param>
        /// <param name="r">Remark</param>
        /// <param name="g">Group Code</param>
        /// <returns></returns>
        public JsonResult ApprovalOrRejcectPO(int t, string c, string s, int p, string r, string g)
        {
            return null;
            //try
            //{
            //    var userInfo = GetUser();
            //    var purchaseOrder = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(SalesType.PurchaseOrder, c));
            //    var flowInfo = Mapper.Map<FlowViewModel>(_flowService.GetByGroupCode((ApprovalType)t, c, g));

            //    var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("APPSTATUS", s));
            //    flowInfo.DateCfm = DateTime.Now;
            //    flowInfo.StatusId = statusInfo.LineNo;
            //    flowInfo.StatusCode = statusInfo.Code;
            //    flowInfo.StatusName = statusInfo.Name;
            //    flowInfo.Remark = r;
            //    hasValue = _flowService.Update(Mapper.Map<M_Flow>(flowInfo));
            //    hasValue = _vuViecService.Commit();
            //    var priorities = Mapper.Map<IEnumerable<FlowViewModel>>(_flowService.FindAll((ApprovalType)t, c));
            //    //var approvalInfo = Mapper.Map<ApprovalInfoViewModel>(_approvalInfoService.GetBy((ApprovalType)t, c));
            //    //-- Change ApprovalInfo
            //    var approvalInfo = new ApprovalInfoViewModel()
            //    {
            //        Type = ApprovalType.PurchaseOrder,
            //        Code = c,
            //        SendBy = userInfo.Username,
            //        SendByName = userInfo.FullName,
            //        SendDate = DateTime.Now,
            //        StatusId = statusInfo.LineNo,
            //        StatusCode = statusInfo.Code,
            //        StatusName = statusInfo.Name,
            //        NextPriority = minPriority,
            //        Description = string.Format("{0} của đại lý {1} số lượng thẻ {2}", purchaseOrder.Code, purchaseOrder.CustomerName, purchaseOrder.TotalCard)
            //    };
            //    //-- Change ApprovalInfo +
            //    var nextFlowInfo = Mapper.Map<FlowViewModel>(_flowService.GetNextFlow((ApprovalType)t, c, p));
            //    if (nextFlowInfo == null)
            //    {
            //        statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("ORDERSTATUS", "400"));
            //        approvalInfo.StatusId = statusInfo.LineNo;
            //        approvalInfo.StatusCode = statusInfo.Code;
            //        approvalInfo.StatusName = statusInfo.Name;

            //        purchaseOrder.StatusId = statusInfo.LineNo;
            //        purchaseOrder.StatusCode = statusInfo.Code;
            //        purchaseOrder.StatusName = statusInfo.Name;
            //        purchaseOrder.LastModifyDate = DateTime.Now;
            //        purchaseOrder.LastModifyBy = GetUser().Username;

            //        hasValue = _salesHdrService.Update(Mapper.Map<M_SalesHeader>(purchaseOrder));

            //    }
            //    else
            //    {
            //        approvalInfo.ApprovalNo = nextFlowInfo.ApprovalNo;
            //        approvalInfo.ApprovalName = nextFlowInfo.ApprovalName;
            //        approvalInfo.ApprovalEmail = nextFlowInfo.ApprovalEmail;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    return Json(new { s = "Error", m = ex.ToString(), status = "" }, JsonRequestBehavior.AllowGet);
            //}
        }
    }
}