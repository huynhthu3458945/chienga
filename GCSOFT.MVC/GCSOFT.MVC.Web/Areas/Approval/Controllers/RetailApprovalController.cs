﻿using AutoMapper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.Approval;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Service.ApprovalService.Interface;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.StoreProcedure.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Service.Transaction.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Areas.Agency.Data;
using GCSOFT.MVC.Web.Areas.Approval.Data;
using GCSOFT.MVC.Web.Areas.MasterData.Models;
using GCSOFT.MVC.Web.Areas.Transaction.Data;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.Approval.Controllers
{
    [CompressResponseAttribute]
    public class RetailApprovalController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IApprovalInfoService _approvalInfoService;
        private readonly IFlowService _flowService;
        private readonly ISalesHeaderService _salesHdrService;
        private readonly IOptionLineService _optionLineService;
        private readonly ITemplateService _templateService;
        private readonly ISetupService _setupService;
        private readonly IMailSetupService _mailSetupService;
        private readonly IUserService _userService;
        private readonly IStoreProcedureService _storeProcedureService;
        private string sysCategory = "APPROVAL";
        private bool? permission = false;
        private string emailTitle;
        private string emailContent;
        private string hasValue;
        #endregion

        #region -- Contructor --
        public RetailApprovalController
            (
                IVuViecService vuViecService
                , IApprovalInfoService approvalInfoService
                , IFlowService flowService
                , ISalesHeaderService salesHdrService
                , IOptionLineService optionLineService
                , ITemplateService templateService
                , ISetupService setupService
                , IMailSetupService mailSetupService
                , IUserService userService
                , IStoreProcedureService storeProcedureService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _approvalInfoService = approvalInfoService;
            _flowService = flowService;
            _salesHdrService = salesHdrService;
            _optionLineService = optionLineService;
            _templateService = templateService;
            _setupService = setupService;
            _mailSetupService = mailSetupService;
            _userService = userService;
            _storeProcedureService = storeProcedureService;
        }
        #endregion
        #region -- Json --
        /// <summary>
        /// Send to Approval
        /// </summary>
        /// <param name="t">Approval Type</param>
        /// <param name="c">Code</param>
        /// <returns></returns>
        public JsonResult SendToApproval(string c)
        {
            try
            {
                var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("ORDERSTATUS", "300"));
                var purchaseOrder = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(SalesType.Convert, c));
                purchaseOrder.StatusId = statusInfo.LineNo;
                purchaseOrder.StatusCode = statusInfo.Code;
                purchaseOrder.StatusName = statusInfo.Name;
                hasValue = _salesHdrService.Update(Mapper.Map<M_SalesHeader>(purchaseOrder));

                //Get email nguoi duyệt.
                var priorities = Mapper.Map<IEnumerable<FlowViewModel>>(_flowService.FindAll(ApprovalType.Convert, c));
                var minPriority = priorities.Where(d => d.StatusCode.Equals("100")).LastOrDefault().Priority;
                var flowInfo = Mapper.Map<IEnumerable<FlowViewModel>>(_flowService.FindAll(ApprovalType.Convert, c, minPriority)).FirstOrDefault();
                statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("APPSTATUS", "100"));
                var userInfo = GetUser();
                var approvalInfo = new ApprovalInfoViewModel()
                {
                    Type = ApprovalType.Convert,
                    Code = c,
                    SendBy = userInfo.Username,
                    SendByName = userInfo.FullName,
                    SendDate = DateTime.Now,
                    StatusId = statusInfo.LineNo,
                    StatusCode = statusInfo.Code,
                    StatusName = statusInfo.Name,
                    ApprovalNo = flowInfo.ApprovalNo,
                    ApprovalName = flowInfo.ApprovalName,
                    ApprovalEmail = flowInfo.ApprovalEmail,
                    Description = string.Format("{0} {1}", purchaseOrder.CustomerName, purchaseOrder.TTLRemark)
                };

                var _approvalInfo = Mapper.Map<ApprovalInfoViewModel>(_approvalInfoService.GetBy(ApprovalType.Convert, c));
                if (_approvalInfo == null)
                    hasValue = _approvalInfoService.Insert(Mapper.Map<M_ApprovalInfo>(approvalInfo));
                else
                    hasValue = _approvalInfoService.Update(Mapper.Map<M_ApprovalInfo>(approvalInfo));
                BuiltEmailPO(purchaseOrder, flowInfo, approvalInfo, priorities);
                var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());

                var emailTos = new List<string>();
                emailTos.Add(flowInfo.ApprovalEmail);
                var _mailHelper = new EmailHelper()
                {
                    EmailTos = emailTos,
                    DisplayName = emailInfo.DisplayName,
                    Title = emailTitle,
                    Body = emailContent,
                    MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
                };
                hasValue = _mailHelper.DoSendMail();
                hasValue = _vuViecService.Commit();
                var _status = string.IsNullOrEmpty(hasValue) ? "Oke" : "Error";
                var _msg = string.IsNullOrEmpty(hasValue) ? "" : hasValue;
                return Json(new { s = _status, m = _msg }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { s = "Error", m = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// Cập nhật trạng thái đơn hàng
        /// </summary>
        /// <param name="t">Approval Type</param>
        /// <param name="c">Document No.</param>
        /// <param name="s">Status Code</param>
        /// <param name="p"></param>
        /// <param name="r">Remark</param>
        /// <returns></returns>
        public JsonResult ApprovalOrRejcect(int t, string c, string s, int p, string r)
        {
            try
            {
                var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
                var purchaseOrder = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(SalesType.Convert, c));
                if (purchaseOrder.StatusCode.Equals("400"))
                    return Json(new { s = "Error", m = "Đã duyệt", status = "" }, JsonRequestBehavior.AllowGet);
                var flowInfo = Mapper.Map<FlowViewModel>(_flowService.GetBy((ApprovalType)t, c, GetUser().Username));
                var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("APPSTATUS", s));
                flowInfo.DateCfm = DateTime.Now;
                flowInfo.StatusId = statusInfo.LineNo;
                flowInfo.StatusCode = statusInfo.Code;
                flowInfo.StatusName = statusInfo.Name;
                flowInfo.Remark = r;
                hasValue = _flowService.Update(Mapper.Map<M_Flow>(flowInfo));
                hasValue = _vuViecService.Commit();
                var priorities = Mapper.Map<IEnumerable<FlowViewModel>>(_flowService.FindAll((ApprovalType)t, c));
                var approvalInfo = Mapper.Map<ApprovalInfoViewModel>(_approvalInfoService.GetBy((ApprovalType)t, c));
                var nextFlowInfo = Mapper.Map<FlowViewModel>(_flowService.GetNextFlow((ApprovalType)t, c, p));
                if (nextFlowInfo == null)
                {
                    statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("APPSTATUS", s));
                    approvalInfo.StatusId = statusInfo.LineNo;
                    approvalInfo.StatusCode = statusInfo.Code;
                    approvalInfo.StatusName = statusInfo.Name;
                    if (s.Equals("300"))
                        statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("ORDERSTATUS", "1100"));
                    else
                        statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("ORDERSTATUS", "400"));
                    purchaseOrder.StatusId = statusInfo.LineNo;
                    purchaseOrder.StatusCode = statusInfo.Code;
                    purchaseOrder.StatusName = statusInfo.Name;
                    hasValue = _salesHdrService.Update(Mapper.Map<M_SalesHeader>(purchaseOrder));
                    nextFlowInfo = new FlowViewModel()
                    {
                        ApprovalName = approvalInfo.SendByName
                    };
                    BuiltEmailPO(purchaseOrder, nextFlowInfo, approvalInfo, priorities, "Đã Duyệt");

                    var userSend = Mapper.Map<UserVM>(_userService.GetBy(approvalInfo.SendBy));
                    var emailTos = new List<string>();
                    emailTos.Add(userSend.Email);
                    var emailCCs = new List<string>();
                    priorities.ToList().ForEach(item => {
                        emailCCs.Add(item.ApprovalEmail);
                    });
                    var _mailHelper = new EmailHelper()
                    {
                        EmailTos = emailTos,
                        DisplayName = emailInfo.DisplayName,
                        Title = emailTitle,
                        Body = emailContent,
                        MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
                    };
                    hasValue = _mailHelper.DoSendMail();
                    _storeProcedureService.sp_ChangeCard(purchaseOrder.Code, purchaseOrder.CustomerId, purchaseOrder.FromLevelCode, purchaseOrder.ToLevelCode, purchaseOrder.TotalCard);
                }
                else
                {
                    approvalInfo.ApprovalNo = nextFlowInfo.ApprovalNo;
                    approvalInfo.ApprovalName = nextFlowInfo.ApprovalName;
                    approvalInfo.ApprovalEmail = nextFlowInfo.ApprovalEmail;
                    BuiltEmailPO(purchaseOrder, nextFlowInfo, approvalInfo, priorities);
                    var emailTos = new List<string>();
                    emailTos.Add(nextFlowInfo.ApprovalEmail);
                    var _mailHelper = new EmailHelper()
                    {
                        EmailTos = emailTos,
                        DisplayName = emailInfo.DisplayName,
                        Title = emailTitle,
                        Body = emailContent,
                        MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
                    };
                    //-- Create PO and AutoApproval
                    if (!purchaseOrder.StatusCode.Equals("400"))
                    {
                        if (purchaseOrder.ConvertType == ConvertType.L2VC)
                            CreatePO2VIP_CAP(purchaseOrder);
                        else
                            CreatePO2LOP_CAP(purchaseOrder);
                    }
                    //-- Cretea PO and AutoApproval +
                    hasValue = _mailHelper.DoSendMail();
                }
                hasValue = _approvalInfoService.Update(Mapper.Map<M_ApprovalInfo>(approvalInfo));
                hasValue = _vuViecService.Commit();
                var _status = string.IsNullOrEmpty(hasValue) ? "Oke" : "Error";
                var _msg = string.IsNullOrEmpty(hasValue) ? "" : hasValue;
                return Json(new { s = _status, m = _msg, status = s }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex) { return Json(new { s = "Error", m = ex.ToString(), status = "" }, JsonRequestBehavior.AllowGet); }
        }
        private string CreatePO2LOP_CAP(SalesOrderCard purchaseOrder)
        {
            try
            {
                var _po = new SalesOrderCard();
                _po.FromSoure = SalesType.Convert;
                _po.SourceNo = purchaseOrder.Code;
                _po.SalesType = SalesType.PurchaseOrder;
                _po.Code = StringUtil.CheckLetter("PO", _salesHdrService.GetMax(SalesType.PurchaseOrder), 4);
                _po.TotalCard = (purchaseOrder.SuggestedNoOfCard ?? 0) - purchaseOrder.TotalCard;
                _po.NoOfCard = _po.TotalCard;
                _po.Price = (string.IsNullOrEmpty(purchaseOrder.ToLevelCode) ? "300" : purchaseOrder.ToLevelCode) == "300" ? 500000 : 1368000;
                _po.Amount = _po.TotalCard * (_po.Price ?? 0);
                _po.TotalAmount = _po.Amount;
                var statusInfo2 = Mapper.Map<VM_OptionLine>(_optionLineService.Get("ORDERSTATUS", "700"));
                _po.StatusId = statusInfo2.LineNo;
                _po.StatusCode = statusInfo2.Code;
                _po.StatusName = statusInfo2.Name;
                _po.CreateBy = GetUser().Username;
                _po.DateCreate = DateTime.Now;
                _po.LastModifyBy = GetUser().Username;

                _po.LastModifyDate = DateTime.Now;
                _po.PostingDate = DateTime.Now;
                _po.CustomerId = purchaseOrder.CustomerId;
                _po.CustomerName = purchaseOrder.CustomerName;
                _po.CustomerAddress = purchaseOrder.CustomerAddress;
                _po.CustomerEmail = purchaseOrder.CustomerEmail;
                _po.CustomerPhoneNo = purchaseOrder.CustomerPhoneNo;
                var DurationInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("CARDINFO", GetDurationCodeByLevelCode(purchaseOrder.ToLevelCode))) ?? new VM_OptionLine();
                var ReasonInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("REASON", "DT")) ?? new VM_OptionLine();
                var LevelInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("LV", purchaseOrder.ToLevelCode)) ?? new VM_OptionLine();
                _po.DurationId = DurationInfo.LineNo;
                _po.DurationCode = DurationInfo.Code;
                _po.DurationName = DurationInfo.Name;
                _po.ReasonId = ReasonInfo.LineNo;
                _po.ReasonCode = ReasonInfo.Code;
                _po.ReasonName = ReasonInfo.Name;
                _po.LevelId = LevelInfo.LineNo;
                _po.LevelCode = LevelInfo.Code;
                _po.LevelName = LevelInfo.Name;
                _po.Flows = purchaseOrder.Flows;
                foreach (var item in _po.Flows)
                {
                    item.Type = ApprovalType.PurchaseOrder;
                    item.Code = _po.Code;
                }
                var mPO = Mapper.Map<M_SalesHeader>(_po);
                hasValue = _salesHdrService.Insert(mPO);
                hasValue = _flowService.Delete(ApprovalType.PurchaseOrder, mPO.Code);
                hasValue = _flowService.Insert(mPO.Flows);
                hasValue = _vuViecService.Commit();
                return null;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        private string CreatePO2VIP_CAP(SalesOrderCard purchaseOrder)
        {
            try
            {
                var _po = new SalesOrderCard();
                _po.FromSoure = SalesType.Convert;
                _po.SourceNo = purchaseOrder.Code;
                _po.SalesType = SalesType.PurchaseOrder;
                _po.Code = StringUtil.CheckLetter("PO", _salesHdrService.GetMax(SalesType.PurchaseOrder), 4);
                _po.TotalCard = (purchaseOrder.SuggestedNoOfCard ?? 0);
                _po.NoOfCard = _po.TotalCard;
                _po.Price = (string.IsNullOrEmpty(purchaseOrder.ToLevelCode) ? "100" : purchaseOrder.ToLevelCode) == "100" ? 2368000 : 500000;
                _po.Amount = _po.TotalCard * (_po.Price ?? 0);
                _po.TotalAmount = _po.Amount;
                var statusInfo2 = Mapper.Map<VM_OptionLine>(_optionLineService.Get("ORDERSTATUS", "700"));
                _po.StatusId = statusInfo2.LineNo;
                _po.StatusCode = statusInfo2.Code;
                _po.StatusName = statusInfo2.Name;
                _po.CreateBy = GetUser().Username;
                _po.DateCreate = DateTime.Now;
                _po.LastModifyBy = GetUser().Username;

                _po.LastModifyDate = DateTime.Now;
                _po.PostingDate = DateTime.Now;
                _po.CustomerId = purchaseOrder.CustomerId;
                _po.CustomerName = purchaseOrder.CustomerName;
                _po.CustomerAddress = purchaseOrder.CustomerAddress;
                _po.CustomerEmail = purchaseOrder.CustomerEmail;
                _po.CustomerPhoneNo = purchaseOrder.CustomerPhoneNo;
                var DurationInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("CARDINFO", GetDurationCodeByLevelCode(purchaseOrder.ToLevelCode))) ?? new VM_OptionLine();
                var ReasonInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("REASON", "DT")) ?? new VM_OptionLine();
                var LevelInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("LV", purchaseOrder.ToLevelCode)) ?? new VM_OptionLine();
                _po.DurationId = DurationInfo.LineNo;
                _po.DurationCode = DurationInfo.Code;
                _po.DurationName = DurationInfo.Name;
                _po.ReasonId = ReasonInfo.LineNo;
                _po.ReasonCode = ReasonInfo.Code;
                _po.ReasonName = ReasonInfo.Name;
                _po.LevelId = LevelInfo.LineNo;
                _po.LevelCode = LevelInfo.Code;
                _po.LevelName = LevelInfo.Name;
                _po.Flows = purchaseOrder.Flows;
                foreach (var item in _po.Flows)
                {
                    item.Type = ApprovalType.PurchaseOrder;
                    item.Code = _po.Code;
                }
                var mPO = Mapper.Map<M_SalesHeader>(_po);
                hasValue = _salesHdrService.Insert(mPO);
                hasValue = _flowService.Delete(ApprovalType.PurchaseOrder, mPO.Code);
                hasValue = _flowService.Insert(mPO.Flows);
                hasValue = _vuViecService.Commit();
                return null;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        #endregion

        #region -- Email Content --
        private void BuiltEmailPO(SalesOrderCard purchaseOrder, FlowViewModel flowInfo, ApprovalInfoViewModel approvalInfo, IEnumerable<FlowViewModel> priorities, string statusName = "Chờ Duyệt")
        {
            var setup = _setupService.Get();
            var templateInfo = Mapper.Map<Template>(_templateService.Get("POSEND"));
            emailTitle = templateInfo.Title.Replace("{{PO_NO}}", purchaseOrder.Code).Replace("{{AGENCY}}", purchaseOrder.CustomerName).Replace("{{NOOFCARD}}", string.Format("{0:#,##0}", purchaseOrder.TotalCard)).Replace("{{USER}}", approvalInfo.SendByName).Replace("{{STATUS_NAME}}", statusName);

            var content = templateInfo.Content;
            content = content.Replace("{{FULLNAME}}", flowInfo.ApprovalName);
            content = content.Replace("{{PO_NO}}", purchaseOrder.Code);
            content = content.Replace("{{DOCUMENT_DATE}}", purchaseOrder.DocumentDateStr);
            content = content.Replace("{{AGENCY_NAME}}", purchaseOrder.CustomerName);
            content = content.Replace("{{AGENCY_ADD}}", purchaseOrder.CustomerAddress);
            content = content.Replace("{{AGENCY_EMAIL}}", purchaseOrder.CustomerEmail);
            content = content.Replace("{{AGENCY_PHONE}}", purchaseOrder.CustomerPhoneNo);
            content = content.Replace("{{NOOFCARD}}", string.Format("{0:#,##0}", purchaseOrder.NoOfCard));
            content = content.Replace("{{GIFT_CARD}}", string.Format("{0:#,##0}", purchaseOrder.GiftCard));
            content = content.Replace("{{AMOUNT}}", string.Format("{0:#,##0}", purchaseOrder.Amount));
            content = content.Replace("{{DISCOUNTPERCENT}}", string.Format("{0:#,##0}%", purchaseOrder.DiscountPercent));
            content = content.Replace("{{DISCOUNT_AMT}}", string.Format("{0:#,##0}", purchaseOrder.DiscountAmt));
            content = content.Replace("{{TOTALAMOUNT}}", string.Format("{0:#,##0}", purchaseOrder.TotalAmount));
            content = content.Replace("{{REMARK}}", purchaseOrder.Remark);

            var keyApproval = HttpUtility.UrlEncode(CryptorEngine.Encrypt(string.Format("{0}_{1}_{2}_{3}_{4}", (int)ApprovalType.Convert, purchaseOrder.Code, flowInfo.ApprovalNo, "HasComment", "Approval"), true, "APRVONL"));
            content = content.Replace("{{LinkApprovalWithComment}}", string.Format("{0}/Approval/Approval/Online?k={1}", setup.DomainBackEnd, keyApproval));

            keyApproval = HttpUtility.UrlEncode(CryptorEngine.Encrypt(string.Format("{0}_{1}_{2}_{3}_{4}", (int)ApprovalType.Convert, purchaseOrder.Code, flowInfo.ApprovalNo, "HasComment", "Reject"), true, "APRVONL"));
            content = content.Replace("{{LinkRejct}}", string.Format("{0}/Approval/Approval/Online?k={1}", setup.DomainBackEnd, keyApproval));

            var tableAprl = new StringBuilder();
            tableAprl.Append("<table border=\"1\" cellpadding=\"1\" cellspacing=\"0\" style=\"width:100%;\">");
            tableAprl.Append("<tbody>");
            tableAprl.Append("<tr>");
            tableAprl.Append("<td style=\"width: 10%; text-align: center; background-color: rgb(0, 153, 255);\"><span style=\"color:#f0ffff;\"><strong>Bộ Phận</strong></span></td>");
            tableAprl.Append("<td style=\"width: 30%; text-align: center; background-color: rgb(0, 153, 255);\"><span style=\"color:#ffffff;\"><strong>Người Duyệt</strong></span></td>");
            tableAprl.Append("<td style=\"width: 15%; text-align: center; background-color: rgb(0, 153, 255);\"><span style=\"color:#ffffff;\"><strong>Trạng Th&aacute;i</strong></span></td>");
            tableAprl.Append("<td style=\"width: 15%; text-align: center; background-color: rgb(0, 153, 255);\"><span style=\"color:#ffffff;\"><strong>Ng&agrave;y Duyệt</strong></span></td>");
            tableAprl.Append("<td style=\"text-align: center; background-color: rgb(0, 153, 255);\"><span style=\"color:#ffffff;\"><strong>Ghi Ch&uacute;</strong></span></td>");
            tableAprl.Append("</tr>");
            foreach (var item in priorities)
            {
                tableAprl.Append("<tr>");
                tableAprl.Append(string.Format("<td>{0}</td>", item.GroupName));
                tableAprl.Append(string.Format("<td>{0}</td>", item.ApprovalName));
                tableAprl.Append(string.Format("<td style=\"text-align: center;\">{0}</td>", item.StatusName));
                tableAprl.Append(string.Format("<td style=\"text-align: center;\">{0}</td>", item.StatusCode.Equals("100") ? "" : string.Format("{0:dd/MM/yyyy hh:mm}", item.DateCfm)));
                tableAprl.Append(string.Format("<td>{0}</td>", item.Remark));
                tableAprl.Append("</tr>");
            }
            tableAprl.Append("</tbody>");
            tableAprl.Append("</table>");

            content = content.Replace("{{APPROVALINFO}}", tableAprl.ToString());
            emailContent = content;
        }
        #endregion
    }
}