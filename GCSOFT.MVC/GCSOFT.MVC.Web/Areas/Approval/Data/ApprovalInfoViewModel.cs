﻿using GCSOFT.MVC.Model.Approval;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.Web.Areas.Approval.Data
{
    public class ApprovalInfoViewModel
    {
        public ApprovalType Type { get; set; }
        public string Code { get; set; }
        public string SendBy { get; set; }
        public string SendByName { get; set; }
        public DateTime SendDate { get; set; }
        public int StatusId { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }
        public string ApprovalNo { get; set; }
        public string ApprovalName { get; set; }
        public string ApprovalEmail { get; set; }
        public string Description { get; set; }
        public int NextPriority { get; set; }
        public string GroupCode { get; set; }
    }
    public class ApprovalList
    {
        public ApprovalType Type { get; set; }
        public string TypeStr { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string StatusName { get; set; }
        public string DateApproval { get; set; }
        public string Key { get; set; }
    }
}