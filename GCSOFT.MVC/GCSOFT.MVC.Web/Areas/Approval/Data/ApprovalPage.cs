﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.Approval.Data
{
    public class ApprovalPage
    {
        public string ApprovalType { get; set; }
        [DisplayName("Loại Phiếu")]
        public IEnumerable<SelectListItem> ApprovalTypeList { get; set; }
        public string StatusCode { get; set; }
        [DisplayName("Trạng Thái")]
        public IEnumerable<SelectListItem> StatusList { get; set; }
    }
}