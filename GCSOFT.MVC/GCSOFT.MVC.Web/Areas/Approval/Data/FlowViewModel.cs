﻿using GCSOFT.MVC.Model.Approval;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.Approval.Data
{
    public class FlowViewModel
    {
        public ApprovalType Type { get; set; }
        public string Code { get; set; }
        public int LineNo { get; set; }
        [DisplayName("Bộ Phận")]
        public string GroupCode { get; set; }
        public string GroupName { get; set; }
        [DisplayName("Người Duyệt")]
        public string ApprovalNo { get; set; }
        public string ApprovalName { get; set; }
        public string ApprovalEmail { get; set; }
        public IEnumerable<SelectListItem> ApprovalList { get; set; }
        public DateTime? DateCfm { get; set; }
        [DisplayName("Ngày Duyệt")]
        public string DateCfmStr { get { return DateCfm == null ? "" : string.Format("{0:dd/MM/yyy}", DateCfm); } }
        public int StatusId { get; set; }
        public string StatusCode { get; set; }
        [DisplayName("Trạng Thái")]
        public string StatusName { get; set; }
        [DisplayName("Độ Ưu Tiên")]
        public int Priority { get; set; }
        public string Remark { get; set; }
        public int IsActive { get; set; }
    }
    public class FlowApproval
    {
        public ApprovalType Type { get; set; }
        public int Type2 { get; set; }
        public string Code { get; set; }
        public string ApprovalNo { get; set; }
        public string Description { get; set; }
        public string Remark { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }
        public int Priority { get; set; }
        public string key { get; set; }
        public bool IsActive { get; set; }
        public IEnumerable<FlowViewModel> ApprovalList { get; set; }
    }
}