﻿using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.ChienGa
{
    public class ChienGaAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "ChienGa";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "ChienGa_default",
                "ChienGa/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}