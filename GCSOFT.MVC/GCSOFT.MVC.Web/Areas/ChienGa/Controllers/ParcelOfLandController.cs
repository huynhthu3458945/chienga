﻿using AutoMapper;
using Dapper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Areas.ChienGa.Data;
using GCSOFT.MVC.Web.Areas.MasterData.Models;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.ViewModels;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.ChienGa.Controllers
{
    public class ParcelOfLandController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IOptionLineService _optionLineService;
        private string sysCategory = "Admin_ParcelOfLand";
        private bool? permission = false;
        private string hasValue;
        private string hienTaiConnection = ConfigurationManager.ConnectionStrings["GCConnection"].ConnectionString;
        #endregion

        #region -- Contructor --
        public ParcelOfLandController
            (
                IVuViecService vuViecService
                , IOptionLineService optionLineService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _optionLineService = optionLineService;
        }
        #endregion

        // GET: ChienGa/ParcelOfLand
        public ActionResult Index(int? currPage, string ward, string soilType, string volatility, string fullNameMale, int? yearBirthMale, string phone, string address)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var parcelOfLands = new ParcelOfLandListPage();
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                var paramaters = new DynamicParameters();

                int pageString = currPage ?? 0;
                int _currPage = pageString == 0 ? 1 : pageString;


                paramaters = new DynamicParameters();
                paramaters.Add("@Action", "TOTAL.ROW");

                paramaters.Add("@Ward", ward ?? string.Empty);
                paramaters.Add("@SoilType", soilType ?? string.Empty);
                paramaters.Add("@Volatility", volatility ?? string.Empty);
                paramaters.Add("@fullNameMale", fullNameMale ?? string.Empty);
                paramaters.Add("@yearBirthMale", yearBirthMale ?? 0);
                paramaters.Add("@phone", phone ?? string.Empty);
                paramaters.Add("@address", address ?? string.Empty);

                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var recordOfPageResults = conn.Query<RecordOfPage>("SP_ParcelOfLand", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                int totalRecord = recordOfPageResults.FirstOrDefault().TotalRow;
                parcelOfLands.paging = new Paging(totalRecord, _currPage);

                paramaters = new DynamicParameters();
                paramaters.Add("@Action", "FINDALL");
                paramaters.Add("@Ward", ward ?? string.Empty);
                paramaters.Add("@SoilType", soilType ?? string.Empty);
                paramaters.Add("@Volatility", volatility ?? string.Empty);
                paramaters.Add("@fullNameMale", fullNameMale ?? string.Empty);
                paramaters.Add("@yearBirthMale", yearBirthMale ?? 0);
                paramaters.Add("@phone", phone ?? string.Empty);
                paramaters.Add("@address", address ?? string.Empty);
                paramaters.Add("@Start", parcelOfLands.paging.start);
                paramaters.Add("@Offset", parcelOfLands.paging.offset);

                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var res = conn.Query<ParcelOfLandViewModel>("SP_ParcelOfLand", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);

                parcelOfLands.Ward = ward;
                parcelOfLands.WardList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("XaPhuong")).ToSelectListItems(ward ?? "");

                parcelOfLands.SoilType = soilType;
                parcelOfLands.SoilTypeList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("LoaiDat")).ToSelectListItems(soilType ?? "", true);

                parcelOfLands.Volatility = volatility;
                parcelOfLands.VolatilityList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("BienDong")).ToSelectListItems(volatility ?? "");
                parcelOfLands.ParcelOfLandList = res.ToList();
            }
            return View(parcelOfLands);
        }

        public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var model = new ParcelOfLandViewModel();
            model.WardList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("XaPhuong")).ToSelectListItems("");
            model.SoilTypeList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("LoaiDat")).ToSelectListItems("", true);
            model.VolatilityList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("BienDong")).ToSelectListItems("");
            model.VocativeList1 = LoadVocative("");
            model.VocativeList2 = LoadVocative("");
            model.DateCreate = DateTime.Now.Date;
            model.DateCreateStr = string.Format("{0:dd/MM/yyyy}", model.DateCreate);
            model.IsCreate = true;

            using (var conn = new SqlConnection(hienTaiConnection))
            {
                var paramaters = new DynamicParameters();
                paramaters = new DynamicParameters();
                paramaters.Add("@Action", "GETMAX");
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var result = conn.Execute("SP_ParcelOfLand", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                model.Code = StringUtil.CheckLetter("TD", paramaters.Get<string>("@RetText"), 4);
            }
            return View(model);
        }
        [HttpPost]
        public ActionResult Create(ParcelOfLandViewModel model)
        {
            try
            {
                model.DateCreate = DateTime.Parse(StringUtil.ConvertStringToDate(model.DateCreateStr));
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    var paramaters = new DynamicParameters();
                    paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "ADD");
                    foreach (PropertyInfo propertyInfo in model.GetType().GetProperties())
                    {

                        var listPro = new List<string>() { "DateCreateStr", "WardList", "SoilTypeList", "VolatilityList", "ParcelBig", "IsCreate", "VocativeList1", "VocativeList2", "AreaBig" };
                        if (!listPro.Contains(propertyInfo.Name))
                            paramaters.Add("@" + propertyInfo.Name, propertyInfo.GetValue(model, null));
                    }
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP_ParcelOfLand", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return RedirectToAction("Edit", new { id = paramaters.Get<Int32>("@RetCode"), msg = "1" });
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        public ActionResult Edit(int? id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var model = new ParcelOfLandViewModel();
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                var paramaters = new DynamicParameters();
                paramaters = new DynamicParameters();
                paramaters.Add("@Action", "FINDBYID");
                paramaters.Add("@Id", id ?? 0);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var res = conn.Query<ParcelOfLandViewModel>("SP_ParcelOfLand", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);


                model = res.ToList().FirstOrDefault();
                model.WardList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("XaPhuong")).ToSelectListItems(model.Ward);
                model.SoilTypeList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("LoaiDat")).ToSelectListItems(model.SoilType, true);
                model.VolatilityList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("BienDong")).ToSelectListItems(model.Volatility);
                model.VocativeList1 = LoadVocative(model.Vocative1);
                model.VocativeList2 = LoadVocative(model.Vocative2);
                model.DateCreateStr = string.Format("{0:dd/MM/yyyy}", model.DateCreate);
            }
            model.IsCreate = false;
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(ParcelOfLandViewModel model)
        {
            try
            {
                model.DateCreate = DateTime.Parse(StringUtil.ConvertStringToDate(model.DateCreateStr));
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    var paramaters = new DynamicParameters();
                    paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "EDIT");
                    foreach (PropertyInfo propertyInfo in model.GetType().GetProperties())
                    {

                        var listPro = new List<string>() { "DateCreateStr", "WardList", "SoilTypeList", "VolatilityList", "ParcelBig", "IsCreate", "VocativeList1", "VocativeList2", "AreaBig" };
                        if (!listPro.Contains(propertyInfo.Name))
                            paramaters.Add("@" + propertyInfo.Name, propertyInfo.GetValue(model, null));
                    }
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP_ParcelOfLand", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    return RedirectToAction("Edit", new { id = paramaters.Get<Int32>("@RetCode"), msg = "1" });
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public ActionResult Deletes(int id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json(new
            {
                success = false,
                message = "Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại"
            }, JsonRequestBehavior.AllowGet);

            if (!permission.Value)
                return Json(new
                {
                    success = false,
                    message = "Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ."
                }, JsonRequestBehavior.AllowGet);
            #endregion
            try
            {
                using (var conn = new SqlConnection(hienTaiConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "DELETE");
                    paramaters.Add("@Id", id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = conn.Execute("SP_ParcelOfLand", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                }
                return Json(new
                {
                    success = true,
                    message = "Xóa dữ liệu không thành công !"
                }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new
                {
                    success = false,
                    message = "Xóa dữ liệu không thành công"
                }, JsonRequestBehavior.AllowGet);

            }
        }
        public ActionResult LoadParcel(string ward, string soilType, string volatility, int? mapOld, int? parcelOld, decimal? areaOld, string code)
        {
            var listParce = new List<ParcelOfLandViliViewModel>();
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                var paramaters = new DynamicParameters();
                paramaters = new DynamicParameters();
                paramaters.Add("@Action", "LOADPARCEL");
                paramaters.Add("@Ward", ward ?? string.Empty);
                paramaters.Add("@SoilType", soilType ?? string.Empty);
                paramaters.Add("@Volatility", volatility ?? string.Empty);
                paramaters.Add("@MapOld", mapOld ?? 0);
                paramaters.Add("@ParcelOld", parcelOld ?? 0);
                paramaters.Add("@AreaOld", areaOld ?? 0);
                paramaters.Add("@Code", code ?? string.Empty);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var res = conn.Query<ParcelOfLandViliViewModel>("SP_ParcelOfLand", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                listParce = res.ToList();
            }

            return Json(listParce, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckParcel(string ward, int? map, int? parcel, decimal? area)
        {
            var hasError = false;
            using (var conn = new SqlConnection(hienTaiConnection))
            {
                var paramaters = new DynamicParameters();
                paramaters = new DynamicParameters();
                paramaters.Add("@Action", "CHECKPARCEL");
                paramaters.Add("@Ward", ward ?? string.Empty);
                paramaters.Add("@MapNew", map ?? 0);
                paramaters.Add("@ParcelNew", parcel ?? 0);
                paramaters.Add("@AreaNew", area ?? 0);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var res = conn.Query<ParcelOfLandViliViewModel>("SP_ParcelOfLand", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                hasError = res.ToList().Count() > 0;
            }

            return Json(new { hasError = hasError }, JsonRequestBehavior.AllowGet);
        }

        public IEnumerable<SelectListItem> LoadVocative(string value)
        {
            return Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("XungHo")).ToSelectListItems(value);
        }
    }
}