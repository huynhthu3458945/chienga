﻿using System.Web.Mvc;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using System.Net;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Model.SystemModels;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Model.MasterData;

namespace GCSOFT.MVC.Web.Areas.Community.Controllers
{
    [CompressResponseAttribute]
    public class StudentController : BaseController
    {
        #region -- Properties --
        private readonly IUserGroupService _userGrpServices;
        private readonly IUserInfoOfGroupService _userInfoOfGroupService;
        private readonly IOptionLineService _optionLineService;
        private readonly IUserService _userService;
        private readonly IUserInfoService _userInfoService;
        private readonly IVuViecService _vuViecService;
        private string sysCategory = "STUDENT";
        private bool? permission = false;
        private string optionHeaderCode = "UIG";
        private string optionHeaderCode_ROLE_SOCIAL = "ROLE_SOCIAL";
        private HT_NhomUser nhomUser;
        private string hasValue;
        #endregion

        #region -- Contructor --
        public StudentController
            (
                IVuViecService vuViecService
                , IOptionLineService optionLineService
                , IUserGroupService userGrpServices
                , IUserInfoOfGroupService userInfoOfGroupService
                , IUserInfoService userInfoService
                , IUserService userService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _optionLineService = optionLineService;
            _userGrpServices = userGrpServices;
            _userInfoOfGroupService = userInfoOfGroupService;
            _userInfoService = userInfoService;
            _userService = userService;
        }
        #endregion

        #region -- Get --
        public ActionResult Index(string id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (permission.Value) return View("AccessDenied");
            #endregion
            var model = new StudentVMs();
            model.GroupCodeList = _optionLineService.FindAll(optionHeaderCode).ToSelectListItems(id);
            model.UserInfoOfGroupList = Mapper.Map<IEnumerable<VMUserInfoOfGroup>>(_userInfoOfGroupService.FindAll().Where(z => (string.IsNullOrEmpty(id) || id.Equals("All")) || (!string.IsNullOrEmpty(id) && z.GroupCode.Equals(id))));
            foreach(var item in model.UserInfoOfGroupList)
            {
                var userInfo = _userInfoService.GetBy(item.userId);
                item.fullName = userInfo.fullName;
                item.Phone = userInfo.Phone;
                item.Email = userInfo.Email;
            }
            return View(model);
        }
        public ActionResult GetCarLinePartialView(string id)
        {
            
            var model = new StudentVMs();
            model.GroupCodeList = _optionLineService.FindAll(optionHeaderCode).ToSelectListItems(id);
            model.UserInfoOfGroupList = Mapper.Map<IEnumerable<VMUserInfoOfGroup>>(_userInfoOfGroupService.FindAll().Where(z => (string.IsNullOrEmpty(id) || id.Equals("All")) || (!string.IsNullOrEmpty(id) && z.GroupCode.Equals(id))));
            foreach (var item in model.UserInfoOfGroupList)
            {
                var userInfo = _userInfoService.GetBy(item.userId);
                item.fullName = userInfo.fullName;
                item.Phone = userInfo.Phone;
                item.Email = userInfo.Email;
            }
            return PartialView("_CardLine", model.UserInfoOfGroupList);
        }
        public ActionResult PopupPhanQuyen(string userName)
        {
            var model = new PhanQuyen();
            model.UserName = userName;
            model.VMOptionLineList = Mapper.Map<IEnumerable<VMOptionLine>>(_optionLineService.FindAll(optionHeaderCode_ROLE_SOCIAL));
            var user = _userInfoService.GetBy(userName);
           
           
            foreach(var item in model.VMOptionLineList)
            {
                if (!string.IsNullOrEmpty(user.RoleCode))
                {
                    if (item.Code.Equals(user.RoleCode))
                        item.Check = true;
                    else
                        item.Check = false;
                }
                else
                {
                    if (item.Code.Equals("NONE"))
                        item.Check = true;
                }
                
            }
            return PartialView("_PopupPhanQuyen", model);
        }
        [HttpPost]
        public ActionResult PopupPhanQuyen(PhanQuyen model)
        {
            if (ModelState.IsValid)
            {
                var user = _userInfoService.GetBy(model.UserName);
                user.RoleCode = model.VMOptionLineList.FirstOrDefault(z => z.Check).Code;
                user.RoleName = model.VMOptionLineList.FirstOrDefault(z => z.Check).Name;
                _userInfoService.Update(user);
                _vuViecService.Commit();
            }
            return Json("oke");
        }
        public ActionResult OpenPopupUser()
        {
            return PartialView("_PopupUser");
        }

        #endregion

        #region -- Post --

        [HttpPost]
        public ActionResult Edit(StudentVMs model)
        {
            try
            {
                model = SetData(model);
                hasValue = _userInfoOfGroupService.Update(model.ModelUserInfoOfGroup, model.GroupCode);
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Index", new { area = "Community", id = model.GroupCode});
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        #endregion

        #region -- Json --
        public JsonResult LoadData(GridSettings grid)
        {
            var list = Mapper.Map<IEnumerable<NhomUsers>>(_userGrpServices.FindAll()).AsQueryable();
            list = JqGrid.SetupGrid<NhomUsers>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadDataUser(GridSettings grid, M_UserInfoFilter filter)
        {
            var listUser = _userInfoService.GetByFullNamePhoneEmail(filter);
            var list = Mapper.Map<IEnumerable<M_UserInfo>>(listUser).AsQueryable();
            list = JqGrid.SetupGrid<M_UserInfo>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Xoa du lieu
        /// </summary>
        /// <param name="id">Code</param>
        /// <returns></returns>
        public JsonResult Deletes(string id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                string[] _codes = id.Split(',').Select(item => item).ToArray();
                string hasValue = _userGrpServices.Delete(_codes);
                if (string.IsNullOrEmpty(hasValue))
                    return Json(new { v = true, count = _codes.Count() });
                return Json("Xóa dữ liệu không thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }
        
        #endregion

        #region -- Functions --
        private StudentVMs SetData(StudentVMs userGrpCard)
        {
            var optionLine = _optionLineService.Get(optionHeaderCode,userGrpCard.GroupCode);
            DateTime dateTime = DateTime.Now;
            foreach (var item in userGrpCard.UserInfoOfGroupList)
            {
                item.GroupCode = userGrpCard.GroupCode;
                item.GroupName = optionLine.Name;
                item.DateInput = dateTime;

                userGrpCard.ModelUserInfoOfGroup.Add(Mapper.Map<M_UserInfoOfGroup>(item));
            }
            return userGrpCard;
        }
        #endregion
    }
}