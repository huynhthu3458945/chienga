﻿using AutoMapper;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.StoreProcedure.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.Community.Controllers
{
    public class SysUserGroupController : BaseController
    {
        private readonly ICongViecService _congViecService;
        private readonly IOptionLineService _optionLineService;
        private readonly IOptionTypeService _optiontypeService;
        private readonly IUserInfoOfGroupService _userInfoOfGroupService;
        private readonly IVuViecService _vuViecService;
        private readonly IStoreProcedureService _storeService;
        private string sysCategory = "USERINFOOFGROUP";
        private bool? permission = false;
        private string hasValue;
        private string optionHeaderCode = "UIG";
        private M_OptionLine m_OptionLine;
        private VM_OptionLine vm_OptionLine;
        public SysUserGroupController
            (
                IVuViecService vuViecService
                , IOptionLineService optionLineService
                , IOptionTypeService optiontypeService
                , IUserInfoOfGroupService userInfoOfGroupService
                , ICongViecService congViecService
                , IStoreProcedureService storeService
            ) : base(vuViecService)
        {
            _congViecService = congViecService;
            _optionLineService = optionLineService;
            _optiontypeService = optiontypeService;
            _userInfoOfGroupService = userInfoOfGroupService;
            _vuViecService = vuViecService;
            _storeService = storeService;
        }

        // GET: Community/SysCategory
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }
        public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            VM_OptionLine model = new VM_OptionLine();
            model.LineNo = _optionLineService.GetID();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(VM_OptionLine _vm_OptionLine)
        {
            try
            {
                SetData(_vm_OptionLine, true);
                hasValue = _optionLineService.Insert(m_OptionLine);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = m_OptionLine.Code});
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public ActionResult Edit(string id)
        {
            if (id == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            vm_OptionLine = Mapper.Map<VM_OptionLine>(_optionLineService.Get(optionHeaderCode, id));
            if (vm_OptionLine == null)
                return HttpNotFound();
            return View(vm_OptionLine);
        }
        [HttpPost]
        public ActionResult Edit(VM_OptionLine vM_OptionLine)
        {
            try
            {
                SetData(vM_OptionLine, false);
                hasValue = _optionLineService.Update(m_OptionLine);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { code = vM_OptionLine.LineNo });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        #region -- Json --
        public JsonResult LoadData(GridSettings grid)
        {
            var list = _optionLineService.FindAll("UIG").OrderBy(z=>z.Order).AsQueryable();
            list = JqGrid.SetupGrid<M_OptionLine>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Xoa du lieu
        /// </summary>
        /// <param name="id">Code</param>
        /// <returns></returns>
        public JsonResult Deletes(string id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                string[] _codes = id.Split(',').Select(item => item).ToArray();
                foreach(var code in _codes)
                {
                    var userInfoOfGroup = _userInfoOfGroupService.GetByGroupCodel(code);
                    if (userInfoOfGroup != null)
                        return Json("Nhóm đã được sử dụng, không được phép xóa!");
                    vm_OptionLine = Mapper.Map<VM_OptionLine>(_optionLineService.Get(optionHeaderCode, code));
                    string hasValue = _optionLineService.Delete(vm_OptionLine.LineNo);
                    hasValue = _vuViecService.Commit();
                }
                if (string.IsNullOrEmpty(hasValue))
                    return Json(new { v = false, count = _codes.Count() });
                return Json("Xóa dữ liệu không thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }
        #endregion  -- Json --

        #region -- Functions --
        private void SetData(VM_OptionLine _vm_OptionLine, bool isCreate)
        {
            vm_OptionLine = _vm_OptionLine;
            M_OptionType optionType = _optiontypeService.FindAll().Where(z => z.Code.Equals(optionHeaderCode)).FirstOrDefault();
            vm_OptionLine.OptionHeaderID = optionType.ID;
            vm_OptionLine.OptionHeaderCode = optionType.Code;
            vm_OptionLine.OptionHeaderName = optionType.Name;
            vm_OptionLine.IsHidden = true;
            if (isCreate)
                vm_OptionLine.LineNo = _optionLineService.GetID();
            m_OptionLine = Mapper.Map<M_OptionLine>(vm_OptionLine);
        }
        #endregion
    }
}