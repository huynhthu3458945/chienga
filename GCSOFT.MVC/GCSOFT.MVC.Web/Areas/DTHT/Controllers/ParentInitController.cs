﻿using AutoMapper;
using Dapper;
using GCSOFT.MVC.Web.Areas.DTHT.Data;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.DTHT.Controllers
{
    public class ParentInitController : Controller
    {
        private string _connectionString = ConfigurationManager.AppSettings["SqlConectionString"];
        // GET: DTHT/PhieuThongTin
        public ActionResult Index()
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.OpenAsync();

                var paramaters = new DynamicParameters();
                paramaters.Add("@Action", "FINDALL");
                var districtResults = conn.QueryAsync<IEnumerable<ParentInit2>>("sp_ParentInit", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                return View(districtResults);
            }
        }
    }
}