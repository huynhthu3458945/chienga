﻿using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.DTHT
{
    public class DTHTAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "DTHT";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "DTHT_default",
                "DTHT/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}