﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.Web.Areas.DTHT.Data
{
    public class ParentInit2
    {
        public string SoBaoDanh { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string TrangThaiPhuHuynh { get; set; }
        public string TrangThaiOutlier { get; set; }
        public string TrangThaiKyCamKet { get; set; }
        public string link { get; set; }
    }
}