﻿// #################################################################
// # Copyright (C) 2021-2022, Tâm Trí Lực JSC.  All Rights Reserved.                       
// #
// # History：                                                                        
// #	Date Time	    Updated		    Content                	
// #    25/08/2021	    Huỳnh Thử 		Update   -- Add tiền tố Khuyến mãi khi tạo mới Serinumber
// ##################################################################

using AutoMapper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.StoreProcedue;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.StoreProcedure.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Areas.MasterData.Models;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.MasterData.Controllers
{
    [CompressResponseAttribute]
    public class CardController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly ICardHeaderService _cardHeaderService;
        private readonly ICardLineService _cardLineService;
        private readonly ICardEntryService _cardEntryService;
        private readonly IOptionLineService _optionLineService;
        private readonly IPromotionService _promotionService;
        private readonly IStoreProcedureService _StoreProcedureService;
        private readonly IMailSetupService _mailSetupService;
        private readonly ITemplateService _templateService;
        private string sysCategory = "CARD2";
        private bool? permission = false;
        private CardHeaderCard cardHeader;
        private M_CardHeader mCardHeader;
        private string hasValue;
        #endregion

        #region -- Contructor --
        public CardController
            (
                IVuViecService vuViecService
                , ICardHeaderService cardHeaderService
                , ICardLineService cardLineService
                , ICardEntryService cardEntryService
                , IOptionLineService optionLineService
                , IPromotionService promotionService
                , IStoreProcedureService storeProcedureService
                , IMailSetupService mailSetupService
                , ITemplateService templateService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _cardHeaderService = cardHeaderService;
            _cardLineService = cardLineService;
            _cardEntryService = cardEntryService;
            _optionLineService = optionLineService;
            _promotionService = promotionService;
            _StoreProcedureService = storeProcedureService;
            _mailSetupService = mailSetupService;
            _templateService = templateService;
        }
        #endregion
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }

        /// <summary>
        ///     Create
        /// </summary>
        /// <param name="noc"></param>
        /// <param name="p"></param>
        /// <param name="d"></param>
        /// <param name="r"></param>
        /// <param name="l"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Update [25/08/2021] -- Bổ sung load Khuyến mãi
        /// </history>
        public ActionResult Create(int? noc, decimal? p, string d, string r, string l, string pro)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            cardHeader = new CardHeaderCard();
            cardHeader.NoOfCard = noc ?? 0;
            cardHeader.Price = p ?? 0;
            cardHeader.LotNumber = StringUtil.CheckLetter("LO", _cardHeaderService.GetMax(), 3);
            var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("TTT", "50"));
            cardHeader.StatusName = statusInfo.Name;
            cardHeader.DurationList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("CARDINFO")).ToSelectListItems(d ?? "");
            cardHeader.ReasonList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("REASON")).ToSelectListItems(r ?? "");
            cardHeader.LevelList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("LV")).ToSelectListItems(l ?? ""); 
            var promotionList = _promotionService.GetAll();
            cardHeader.PromotionList = Mapper.Map<IEnumerable<VM_Promotion>>(promotionList.Where(z => z.FromDate.Value.Date <= DateTime.Now.Date && DateTime.Now.Date <= z.ToDate.Value.Date)).ToSelectListItems(pro ?? "");
            cardHeader.UserFullName = GetUser().FullName;
            return View(cardHeader);
        }
        [HttpPost]
        public ActionResult Create(CardHeaderCard _cardHdr)
        {
            try
            {
                SetData(_cardHdr, true);
                _cardHeaderService.Insert(mCardHeader);
                _cardLineService.Insert(mCardHeader.CardLines);
                _cardEntryService.Insert(mCardHeader.CardEntries);
                _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = mCardHeader.LotNumber, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        /// <summary>
        ///     Create
        /// </summary>
        /// <param name="noc"></param>
        /// <param name="p"></param>
        /// <param name="d"></param>
        /// <param name="r"></param>
        /// <param name="l"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Update [25/08/2021] -- Bổ sung load Khuyến mãi
        /// </history>
        public ActionResult Edit(string id, int? p)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            cardHeader = Mapper.Map<CardHeaderCard>(_cardHeaderService.GetBy(id));
            if (cardHeader == null)
                return HttpNotFound();
            cardHeader.DurationList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("CARDINFO")).ToSelectListItems(cardHeader.DurationCode);
            cardHeader.ReasonList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("REASON")).ToSelectListItems(cardHeader.ReasonCode);
            cardHeader.LevelList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("LV")).ToSelectListItems(cardHeader.LevelCode); var promotionList = _promotionService.GetAll();
            cardHeader.PromotionList = Mapper.Map<IEnumerable<VM_Promotion>>(promotionList.Where(z => z.FromDate.Value.Date <= DateTime.Now.Date && DateTime.Now.Date <= z.ToDate.Value.Date)).ToSelectListItems(cardHeader.PromotionCode);
            //-- Paging
            int pageString = p ?? 0;
            int page = pageString == 0 ? 1 : pageString;
            var totalRecord = Mapper.Map<IEnumerable<CardLine>>(_cardLineService.FindAll(id)).Count();
            cardHeader.paging = new Paging(totalRecord, page);
            cardHeader.CardLines = Mapper.Map<IEnumerable<CardLine>>(_cardLineService.FindAll(id, cardHeader.paging.start, cardHeader.paging.offset));
            //-- Paging +
            return View(cardHeader);
        }
        [HttpPost]
        public ActionResult Edit(CardHeaderCard _cardHdr)
        {
            try
            {
                SetData(_cardHdr, false);
                hasValue = _cardHeaderService.Update(mCardHeader);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = mCardHeader.LotNumber, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public JsonResult LoadData(GridSettings grid)
        {
            var list = Mapper.Map<IEnumerable<CardHeaderList>>(_cardHeaderService.FindAll()).AsQueryable();
            list = JqGrid.SetupGrid<CardHeaderList>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Cấp lại mã kích hoạt
        /// </summary>
        /// <param name="grid"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Update [06/09/2021] 
        /// </history>
        public JsonResult ReNewCard()
        {
            try
            {
                var res = _StoreProcedureService.SP_CardNoDuplicate();
                var _cardNo = string.Empty;
                var _cardNo2 = string.Empty;
                var cardNoList = new List<string>();
                var hasCardNo = true;
                foreach (var cardNo in res)
                {
                    do
                    {
                        _cardNo2 = GetBuildCardNo().ToString();
                        _cardNo = CryptorEngine.Encrypt(_cardNo2, true, "TTLSTNHD@CARD");
                        hasCardNo = cardNoList.Where(d => d == _cardNo2).Any();
                    } while (_cardLineService.CheckExist(_cardNo) || hasCardNo);

                    cardNoList.Add(_cardNo2);

                    bool isSucccesUpdate = _cardLineService.UpdateCardNo(_cardNo, cardNo.Id);
                    _vuViecService.Commit();
                    cardNo.CardNo = _cardNo;
                    // Send Mail and SMS
                    if (isSucccesUpdate)
                    {
                        SendMail(cardNo);
                    }    

                }
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// GetBuildCardNo
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [06/09/2021]
        /// </history>
        private StringBuilder GetBuildCardNo()
        {
            var builder = new StringBuilder();
            var randomNumber = new RandomNumberGenerator();
            builder.Append(randomNumber.RandomString(4, true).ToUpper());
            builder.Append(randomNumber.RandomNumber(1000, 9999));
            builder.Append(randomNumber.RandomString(2, false).ToUpper());

            return builder;
        }

        /// <summary>
        ///     SendMail
        /// </summary>
        /// <param name="cardLines"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [06/09/2021]
        /// </history>
        public string SendMail(ReNewCardNo item)
        {
            try
            {
                    //-- Gửi qua email
                    var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
                    var mailTemplate = Mapper.Map<Template>(_templateService.Get("EMAILACTIVECODE"));
                    var mailTitle = mailTemplate.Title;
                    var mailContent = mailTemplate.Content.Replace("{{FULLNAME}}", item.FullName);
                    mailContent = mailContent.Replace("{{ACTIVECODE}}", CryptorEngine.Decrypt(item.CardNo, true, "TTLSTNHD@CARD"));
                    var emailTos = new List<string>();
                    emailTos.Add(item.Email);
                    var _mailHelper = new EmailHelper()
                    {
                        EmailTos = emailTos,
                        DisplayName = emailInfo.DisplayName,
                        Title = mailTitle,
                        Body = mailContent,
                        MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
                    };
                    string hasValue = _mailHelper.SendEmailsThread();

                    if (!string.IsNullOrEmpty(item.PhoneNo))
                    {
                        var newPhoneNo = item.PhoneNo.Trim();
                        var firstNumber = int.Parse(newPhoneNo[0].ToString());
                        if (firstNumber != 0)
                        {
                            newPhoneNo = "0" + newPhoneNo;
                        }
                        //-- Gui qua SMS
                        var smsTemplate = Mapper.Map<Template>(_templateService.Get("SMS_MAKICHHOAT"));
                        var smsHelper = new SMSHelper()
                        {
                            PhoneNo = newPhoneNo,
                            Content = StringUtil.StripTagsRegexCompiled(smsTemplate.Content).Replace("{{NAME}}", "vao 5 Phut Thuoc Bai").Replace("{{MAKICHHOAT}}", CryptorEngine.Decrypt(item.CardNo, true, "TTLSTNHD@CARD")).Replace("{{LINK}}", "https://sieutrinhohocduong.com/student")
                        };
                        if (smsHelper.IsMobiphone())
                        {
                            smsTemplate = Mapper.Map<Template>(_templateService.Get("MOBI_TKKH_KH"));
                            smsHelper.Content = string.Format(smsTemplate.SMSContent, "5 Phut Thuoc Bai", CryptorEngine.Decrypt(item.CardNo, true, "TTLSTNHD@CARD"));
                            hasValue = smsHelper.SendMobiphone();
                        }
                        else
                            hasValue = smsHelper.Send();
                        //-- Gui qua SMS +
                    }
                //-- Gửi qua email +
                return string.Empty;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        /// <summary>
        ///  SetData   
        /// </summary>
        /// <param name="_cardHdr"></param>
        /// <param name="isCreate"></param>
        /// <history>
        ///     [Huỳnh Thử] Update [25/08/2021] -- Add tiền tố Khuyến mãi khi tạo mới Serinumber
        /// </history>
        private void SetData(CardHeaderCard _cardHdr, bool isCreate)
        {
            cardHeader = _cardHdr;
            if (isCreate)
            {
                cardHeader.LotNumber = StringUtil.CheckLetter("LO", _cardHeaderService.GetMax(), 3);
                cardHeader.DateCreate = DateTime.Now;
                cardHeader.DateOrder = DateTime.Now;
                var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("TTT", "100"));
                var DurationInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("CARDINFO", cardHeader.DurationCode));
                var ReasonInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("REASON", cardHeader.ReasonCode));
                var LevelInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("LV", cardHeader.LevelCode));
                cardHeader.StatusId = statusInfo.LineNo;
                cardHeader.StatusCode = statusInfo.Code;
                cardHeader.StatusName = statusInfo.Name;
                cardHeader.UserName = GetUser().Username;
                cardHeader.UserFullName = GetUser().FullName;

                cardHeader.DurationId = DurationInfo.LineNo;
                cardHeader.DurationCode = DurationInfo.Code;
                cardHeader.DurationName = DurationInfo.Name;

                cardHeader.ReasonId = ReasonInfo.LineNo;
                cardHeader.ReasonCode = ReasonInfo.Code;
                cardHeader.ReasonName = ReasonInfo.Name;

                cardHeader.LevelId = LevelInfo.LineNo;
                cardHeader.LevelCode = LevelInfo.Code;
                cardHeader.LevelName = LevelInfo.Name;

                var cardLine = new CardLine();
                var cardLines = new List<CardLine>();
                var idCard = _cardLineService.GetID() - 1;

                var cardEntry = new CardEntry();
                var cardEntries = new List<CardEntry>();
                var idCardEntry = _cardEntryService.GetID() - 1;

                statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("CARD", "100"));
                string prefixPromoyion = _promotionService.GetByCode(cardHeader.PromotionCode)?.Prefix ?? "";
                for (int i = 0; i < cardHeader.NoOfCard; i++)
                {
                    idCard += 1;
                    var _serinumner = "";
                    var _levelCode = cardHeader.LevelCode ?? "100";
                    if (_levelCode.Equals("200"))
                        _serinumner = StringUtil.GetProductID($"{prefixPromoyion}T", (idCard - 1).ToString(), 9);
                    else if (_levelCode.Equals("300"))
                        _serinumner = StringUtil.GetProductID($"{prefixPromoyion}L", (idCard - 1).ToString(), 9);
                    else
                        _serinumner = StringUtil.GetProductID($"{prefixPromoyion}S", (idCard - 1).ToString(), 9);
                    cardLine = new CardLine()
                    {
                        Id = idCard,
                        LotNumber = cardHeader.LotNumber,
                        //SerialNumber = (cardHeader.LevelCode ?? "100").Equals("100") ? StringUtil.GetProductID("S", (idCard - 1).ToString(), 9) : StringUtil.GetProductID("T", (idCard - 1).ToString(), 9),
                        SerialNumber = _serinumner,
                        Price = cardHeader.Price,
                        UserName = cardHeader.UserName,
                        UserFullName = cardHeader.UserFullName,
                        DateCreate = cardHeader.DateCreate,

                        UserType = UserType.TTL,
                        UserNameActive = cardHeader.UserName,
                        UserActiveFullName = cardHeader.UserFullName,
                        LastDateUpdate = cardHeader.DateCreate,
                        StatusId = statusInfo.LineNo,
                        StatusCode = statusInfo.Code,
                        StatusName = statusInfo.Name,
                        LastStatusId = statusInfo.LineNo,
                        LastStatusCode = statusInfo.Code,
                        LastStatusName = statusInfo.Name,
                        DurationId = DurationInfo.LineNo,
                        DurationCode = DurationInfo.Code,
                        DurationName = DurationInfo.Name,
                        ReasonId = ReasonInfo.LineNo,
                        ReasonCode = ReasonInfo.Code,
                        ReasonName = ReasonInfo.Name,
                        LevelId = LevelInfo.LineNo,
                        LevelCode = LevelInfo.Code,
                        LevelName = LevelInfo.Name
                    };
                    cardLines.Add(cardLine);

                    idCardEntry += 1;
                    cardEntry = new CardEntry()
                    {
                        //Id = idCardEntry,
                        LotNumber = cardHeader.LotNumber,
                        CardLineId = idCard,
                        SerialNumber = cardLine.SerialNumber,
                        CardNo = cardLine.CardNo,
                        Price = cardLine.Price,
                        UserName = cardLine.UserName,
                        UserFullName = cardLine.UserFullName,
                        DateCreate = cardLine.DateCreate,
                        UserType = UserType.TTL,
                        UserNameActive = cardLine.UserNameActive,
                        UserActiveFullName = cardLine.UserActiveFullName,
                        Date = cardLine.DateCreate,
                        StatusId = statusInfo.LineNo,
                        StatusCode = statusInfo.Code,
                        StatusName = statusInfo.Name,
                        Source = CardSource.CardSTNHD,
                        SourceNo = cardHeader.LotNumber,
                        DurationId = DurationInfo.LineNo,
                        DurationCode = DurationInfo.Code,
                        DurationName = DurationInfo.Name,
                        ReasonId = ReasonInfo.LineNo,
                        ReasonCode = ReasonInfo.Code,
                        ReasonName = ReasonInfo.Name,
                        LevelId = LevelInfo.LineNo,
                        LevelCode = LevelInfo.Code,
                        LevelName = LevelInfo.Name
                    };
                    cardEntries.Add(cardEntry);
                }
                cardHeader.CardLines = cardLines;
                cardHeader.CardEntries = cardEntries;
            }
            mCardHeader = Mapper.Map<M_CardHeader>(cardHeader);
        }
    }
}