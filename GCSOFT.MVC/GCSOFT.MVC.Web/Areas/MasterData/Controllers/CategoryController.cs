﻿using System.Web.Mvc;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using System.Net;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Data.Common;

namespace GCSOFT.MVC.Web.Areas.MasterData.Controllers
{
	[CompressResponseAttribute]
    public class CategoryController : BaseController
    {
		#region -- Properties --
		private readonly IVuViecService _vuViecService;
		private readonly ICategoryService _categoryService;
        private string sysCategory = "CATEGORY";
        private bool? permission = false;
        private VM_Category vm_Category;
        private E_Category e_Category;
        private string hasValue;
		#endregion
		
		#region -- Contructor --
		public CategoryController
            (
				IVuViecService vuViecService
                , ICategoryService categoryService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _categoryService = categoryService;
        }
		#endregion

        public ActionResult Index()
        {
			#region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }
        public ActionResult IndexPopup()
        {
            return View("_IndexPopup");
        }
        public ActionResult Details(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.ViewDetail);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            vm_Category = Mapper.Map<VM_Category>(_categoryService.GetBy(id));
            if (vm_Category == null)
                return HttpNotFound();
            return View(vm_Category);
        }

        public ActionResult Create()
        {
			#region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            vm_Category = new VM_Category();
            var categoryParents = _categoryService.GetParentCategory(0);
            vm_Category.ParentCategorys = categoryParents.ToSelectListItems(0);
            return View(vm_Category);
        }

        [HttpPost]
        public ActionResult Create(VM_Category _vm_Category)
        {
			try
            {
                SetData(_vm_Category, true);
                hasValue = _categoryService.Insert(e_Category);
				hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = e_Category.Id, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        public ActionResult Edit(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var categoryParents = _categoryService.GetParentCategory(id);
            vm_Category = Mapper.Map<VM_Category>(_categoryService.GetBy(id));
            vm_Category.xParentNo = vm_Category.ParentNo;
            vm_Category.ParentCategorys = categoryParents.ToSelectListItems(vm_Category.ParentNo ?? 0);
            if (vm_Category == null)
				return HttpNotFound();
            return View(vm_Category);
        }

        [HttpPost]
        public ActionResult Edit(VM_Category _vm_Category)
        {
			try
            {
                SetData(_vm_Category, false);
                hasValue = _categoryService.Update(e_Category);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = e_Category.Id, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        public ActionResult Deletes(string id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                List<int> _codes = id.Split(',').Select(item => int.Parse(item)).ToList();
                hasValue = _categoryService.Delete(_codes);
				hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return Json(new { v = true, count = _codes.Count() });
                return Json("Xóa dữ liệu không thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }
		#region -- Json --
		public JsonResult LoadData(GridSettings grid)
        {
            var list = Mapper.Map<IEnumerable<VM_Category_Index>>(_categoryService.FindAll()).AsQueryable();
            list = JqGrid.SetupGrid<VM_Category_Index>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
        public JsonResult LoadDataPopup(GridSettings grid, string idCategoryChoosed)
        {
            List<int> idCategoryChooseds = new List<int>();
            if (!string.IsNullOrEmpty(idCategoryChoosed))
                idCategoryChooseds = idCategoryChoosed.Split(',').Select(item => int.Parse(item)).ToList();
            var list = Mapper.Map<IEnumerable<VM_Category_Index>>(_categoryService.FindAll(idCategoryChooseds)).AsQueryable();
            list = JqGrid.SetupGrid<VM_Category_Index>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region -- Functions --
        private void SetData(VM_Category _vm_Category, bool isCreate)
        {
            vm_Category = _vm_Category;
            string breadCrumb = string.Empty;
            int levelCategory = 0;
            if (isCreate)
                vm_Category.Id = _categoryService.GetKeyCategory();
            if (isCreate || vm_Category.xParentNo != vm_Category.ParentNo)
            {
                if (vm_Category.ParentNo == null)
                {
                    vm_Category.SearchName = _categoryService.GetSearchName(vm_Category.Name, vm_Category.LinkWeb, vm_Category.Id, vm_Category.ParentNo, ref levelCategory, ref breadCrumb);
                    vm_Category.Level = _categoryService.GetMaxLevel(0);
                    vm_Category.LevelStr = vm_Category.Level.ToString();
                    vm_Category.LevelCategory = 1;
                }
                else
                {

                    var parentInfo = _categoryService.GetBy(vm_Category.ParentNo ?? 0);
                    vm_Category.SearchName = _categoryService.GetSearchName(vm_Category.Name, vm_Category.LinkWeb, vm_Category.Id, vm_Category.ParentNo, ref levelCategory, ref breadCrumb);
                    vm_Category.Level = _categoryService.GetMaxLevel(vm_Category.ParentNo ?? 0);
                    vm_Category.LevelStr = parentInfo.LevelStr + "." + vm_Category.Level.ToString();
                    vm_Category.LevelCategory = levelCategory;
                }
            }
            else {
                vm_Category.SearchName = _categoryService.GetSearchName(vm_Category.Name, vm_Category.LinkWeb, vm_Category.Id, vm_Category.ParentNo, ref levelCategory, ref breadCrumb);
                vm_Category.Level = vm_Category.Level;
                vm_Category.LevelStr = vm_Category.LevelStr;
                vm_Category.LevelCategory = levelCategory;
            }
            vm_Category.BreadCrumb = breadCrumb;
            vm_Category.LinkWeb = StringUtil.ConvertToUnsign(vm_Category.LinkWeb);
            e_Category = Mapper.Map<E_Category>(vm_Category);
        }
		#endregion
    }
}