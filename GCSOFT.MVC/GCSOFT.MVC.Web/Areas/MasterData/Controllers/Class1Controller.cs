﻿using System.Web.Mvc;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using System.Net;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Areas.MasterData.Models;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Web.ViewModels.MasterData;

namespace GCSOFT.MVC.Web.Areas.MasterData.Controllers
{
	[CompressResponseAttribute]
    public class Class1Controller : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IClassService _classService;
        private readonly IOptionLineService _optionLineService;
        private string sysCategory = "CLASS";
        private ClassType classType = ClassType.Class;
        private bool? permission = false;
        private VM_Class vmClass;
        private E_Class eClass;
        private string hasValue;
        #endregion

        #region -- Contructor --
        public Class1Controller
            (
                IVuViecService vuViecService
                , IClassService teacherService
                , IOptionLineService optionLineService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _classService = teacherService;
            _optionLineService = optionLineService;
        }
        #endregion

        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }
        public ActionResult IndexPopup()
        {
            return View("_IndexPopup");
        }
        public ActionResult Details(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.ViewDetail);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            vmClass = Mapper.Map<VM_Class>(_classService.GetBy(id));
            if (vmClass == null)
                return HttpNotFound();
            return View(vmClass);
        }

        public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            vmClass = new VM_Class();
            vmClass.StatusList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll(4)).ToSelectListItems(0);
            vmClass.LevelList = Mapper.Map<IEnumerable<VM_Class>>(_classService.FindAll(ClassType.Level)).ToSelectListItems(0);
            return View(vmClass);
        }

        [HttpPost]
        public ActionResult Create(VM_Class _vmClass)
        {
            try
            {
                SetData(_vmClass, true);
                hasValue = _classService.Insert(eClass);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = eClass.Id, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        public ActionResult Edit(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            vmClass = Mapper.Map<VM_Class>(_classService.GetBy(id));
            if (vmClass == null)
                return HttpNotFound();
            vmClass.StatusList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll(4)).ToSelectListItems(vmClass.StatusId);
            vmClass.LevelList = Mapper.Map<IEnumerable<VM_Class>>(_classService.FindAll(ClassType.Level)).ToSelectListItems(vmClass.LevelId);
            return View(vmClass);
        }

        [HttpPost]
        public ActionResult Edit(VM_Class _vmClass)
        {
            try
            {
                SetData(_vmClass, false);
                hasValue = _classService.Update(eClass);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = eClass.Id, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        public ActionResult Deletes(string id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                int[] _codes = id.Split(',').Select(item => int.Parse(item)).ToArray();
                hasValue = _classService.Delete(_codes);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return Json(new { v = true, count = _codes.Count() });
                return Json("Xóa dữ liệu không thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }
        #region -- Json --
        public JsonResult LoadData(GridSettings grid)
        {
            var list = Mapper.Map<IEnumerable<VM_Class>>(_classService.FindAll(classType)).AsQueryable();
            list = JqGrid.SetupGrid<VM_Class>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
        public JsonResult LoadDataPopup(GridSettings grid, string idClassChoosed)
        {
            List<int> idClassChooseds = new List<int>();
            if (!string.IsNullOrEmpty(idClassChoosed))
                idClassChooseds = idClassChoosed.Split(',').Select(item => int.Parse(item)).ToList();
            var list = Mapper.Map<IEnumerable<VM_Class>>(_classService.FindAll(classType, idClassChooseds)).AsQueryable();
            list = JqGrid.SetupGrid<VM_Class>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region -- Functions --
        private void SetData(VM_Class _vmClass, bool isCreate)
        {
            vmClass = _vmClass;
            if (isCreate)
                vmClass.Id = _classService.GetID();
            vmClass.ClassType = classType;
            var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get(4, vmClass.StatusId)) ?? new VM_OptionLine();
            vmClass.StatusCode = statusInfo.Code;
            vmClass.StatusName = statusInfo.Name;
            vmClass.LevelName = Mapper.Map<VM_Class>(_classService.GetBy(vmClass.LevelId)).Name;
            eClass = Mapper.Map<E_Class>(vmClass);
        }
        #endregion
    }
}
