﻿using AutoMapper;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Areas.MasterData.Models;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Mvc;
using System.Linq;
using System.Data.SqlClient;
using Dapper;

namespace GCSOFT.MVC.Web.Areas.MasterData.Controllers
{
    [CompressResponseAttribute]
    public class CourseContentController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly ICourseContentService _courseContentService;
        private readonly ISubEcourceContentService _subSourseContentService;
        private readonly ICourseResourceService _courseResourceService;
        private readonly IResultEntryService _resultEntryService;
        private readonly ICourseService _courceService;
        private readonly IOptionLineService _optionLineService;
        private string stnhdConnection = ConfigurationManager.ConnectionStrings["GCConnection"].ConnectionString;
        private CourseContentLine courseContent;
        private string sysCategory = "COURSECONTENT";
        private bool? permission = false;
        private string hasValue;
        #endregion
        #region -- Contructor --
        public CourseContentController
            (
               IVuViecService vuViecService
                , ICourseContentService courseContentService
                , ICourseResourceService courseResourceService
                , IResultEntryService resultEntryService
                , ICourseService courceService
                , IOptionLineService optionLineService
                , ISubEcourceContentService subSourseContentService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _courseContentService = courseContentService;
            _courseResourceService = courseResourceService;
            _resultEntryService = resultEntryService;
            _courceService = courceService;
            _optionLineService = optionLineService;
            _subSourseContentService = subSourseContentService;
        }
        #endregion
        public ActionResult Edit(int id, int lineNo)
        {
            LoadData(id, lineNo);
            if (courseContent == null)
                return HttpNotFound();
            return View(courseContent);
        }
        public ActionResult Details(int id, int lineNo)
        {
            LoadData(id, lineNo);
            if (courseContent == null)
                return HttpNotFound();
            return View(courseContent);
        }
        [HttpPost]
        public ActionResult Edit(CourseContentLine _courseContent)
        {
            try
            {
                var e_CourseContent = Mapper.Map<E_CourseContent>(_courseContent);
                hasValue = _courseContentService.Update(e_CourseContent);
                hasValue = _resultEntryService.Delete(ResultType.Cource, e_CourseContent.IdCourse, e_CourseContent.LineNo);
                hasValue = _resultEntryService.Insert(GetResultEntry(e_CourseContent));
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = e_CourseContent.IdCourse, lineNo = e_CourseContent.LineNo, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        private void LoadData(int id, int lineNo)
        {
            ViewBag.CourseContentLine = Mapper.Map<IEnumerable<CourseContentLine>>(_courseContentService.FindAll(id));
            ViewBag.CourseResourceLine = Mapper.Map<IEnumerable<CourseResourceLine>>(_courseResourceService.FindAll(id));
            courseContent = Mapper.Map<CourseContentLine>(_courseContentService.GetBy(id, lineNo));
        }
        private M_ResultEntry GetResultEntry(E_CourseContent _eCourceContent)
        {
            var courseInfo = Mapper.Map<CourseCard>(_courceService.GetBy_2(_eCourceContent.IdCourse));
            var _resultEntry = new VM_ResultEntry();
            _resultEntry.Type = ResultType.Cource;
            _resultEntry.ReferId = _eCourceContent.IdCourse;
            _resultEntry.LineNo = _eCourceContent.LineNo;
            _resultEntry.Question = _eCourceContent.Title;
            _resultEntry.Answer = _eCourceContent.FullDescription;
            _resultEntry.QuestionUnsigned = "";
            _resultEntry.AnswerUnsigned = "";
            _resultEntry.QuestionDate = null;
            _resultEntry.AnswerDate = null;
            _resultEntry.Questioner_Id = null;
            _resultEntry.Questioner_FB_Link = null;
            _resultEntry.Questioner_FB_Name = null;
            _resultEntry.Supporter_Id = null;
            _resultEntry.Parent_Id = null;
            _resultEntry.parent_Name = null;
            _resultEntry.Level_Id = null;
            _resultEntry.Level_Name = null;
            _resultEntry.Class_Id = courseInfo.ClassId;
            _resultEntry.Class_Name = courseInfo.ClassName;
            _resultEntry.Course_Id = courseInfo.Id;
            _resultEntry.Course_Name = courseInfo.Title;
            _resultEntry.Method_Id = null;
            _resultEntry.method_Name = null;
            _resultEntry.Group_Id = null;
            _resultEntry.GroupName = null;
            _resultEntry.Status_Id = null;
            _resultEntry.Status_Name = null;
            return Mapper.Map<M_ResultEntry>(_resultEntry);
        }

        /// <summary>
        /// Load Course Content By Course
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public JsonResult FetchCourseContents(int c)
        {
            return Json(Mapper.Map<IEnumerable<CourseContentCard>>(_courseContentService.FindAll(c)), JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult CreateCourseContent(int idCourse)
        {
            var courserContentLine = new CourseContentLine();
            courserContentLine.IdCourse = idCourse;
            courserContentLine.LessonTypeList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("COURSETYPE")).ToSelectListItems("");
            courserContentLine.ParentSortOrderList = Mapper.Map<IEnumerable<CourseContentCard>>(_courseContentService.FindAll(idCourse, -1, CourseType.Videos)).ToSelectListItems(0);
            courserContentLine.ReviewList = ReviewList(idCourse).ToSelectListItems(0);
            return PartialView("_CreateCourseContent", courserContentLine);
        }
        public PartialViewResult EditCourseContent(int idCourse, int lineNo)
        {
            var courserContentLine = Mapper.Map<CourseContentLine>(_courseContentService.GetBy(idCourse, lineNo));
            courserContentLine.LessonTypeList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("COURSETYPE")).ToSelectListItems(courserContentLine.LessonTypeCode);
            courserContentLine.ParentSortOrderList = Mapper.Map<IEnumerable<CourseContentCard>>(_courseContentService.FindAll(idCourse, lineNo, CourseType.Videos)).ToSelectListItems(0);
            courserContentLine.ReviewList = ReviewList(idCourse).ToSelectListItems(0);
            courserContentLine.ReviewCodeArrs = Mapper.Map<List<SubEcourceContent>>(_subSourseContentService.FindAll(idCourse, lineNo)).Select(d => d.SubLineNo).ToArray();
            return PartialView("_EditCourseContent", courserContentLine);
        }
        [HttpPost]
        public ActionResult CreateCourseContent(CourseContentLine courseContentLine)
        {
            try
            {
                var hasSortOrder = _courseContentService.HasSortOrder(courseContentLine.IdCourse, courseContentLine.LineNo, courseContentLine.SortOrder, CourseType.Videos);
                if (hasSortOrder)
                    return Json(new { statusCode = "ERROR", statusName = "Số thứ tự đã tồn tại, vui lòng nhập số thứ tự khác" }, JsonRequestBehavior.AllowGet);
                var _lessonTypeInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("COURSETYPE", courseContentLine.LessonTypeCode));
                if (!courseContentLine.LessonTypeCode.Equals("VIDEOS") && (!string.IsNullOrEmpty(courseContentLine.LinkVideoEN) || !string.IsNullOrEmpty(courseContentLine.LinkVideoVN)))
                    return Json(new { statusCode = "ERROR", statusName = "Loại bài học không phải Videos, không được nhập link" }, JsonRequestBehavior.AllowGet);
                courseContentLine.LineNo = _courseContentService.GetID(courseContentLine.IdCourse);
                courseContentLine.CourseType = CourseType.Videos;
                courseContentLine.LessonTypeName = _lessonTypeInfo.Name;
                hasValue = _courseContentService.Insert(Mapper.Map<E_CourseContent>(courseContentLine));
                hasValue = SubCourseContents(courseContentLine.IdCourse, courseContentLine.LineNo, courseContentLine.ReviewCodeArrs);
                hasValue = _vuViecService.Commit();
                return Json(new { statusCode = string.IsNullOrEmpty(hasValue) ? "SUCCESS" : "ERROR", statusName = hasValue }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { statusCode = "ERROR", statusName = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult EditCourseContent(CourseContentLine courseContentLine)
        {
            try
            {
                var hasSortOrder = _courseContentService.HasSortOrder(courseContentLine.IdCourse, courseContentLine.LineNo, courseContentLine.SortOrder, CourseType.Videos);
                if (hasSortOrder)
                    return Json(new { statusCode = "ERROR", statusName = "Số thứ tự đã tồn tại, vui lòng nhập số thứ tự khác" }, JsonRequestBehavior.AllowGet);
                var _lessonTypeInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("COURSETYPE", courseContentLine.LessonTypeCode));
                if (!courseContentLine.LessonTypeCode.Equals("VIDEOS") && (!string.IsNullOrEmpty(courseContentLine.LinkVideoEN) || !string.IsNullOrEmpty(courseContentLine.LinkVideoVN)))
                    return Json(new { statusCode = "ERROR", statusName = "Loại bài học không phải Videos, không được nhập link" }, JsonRequestBehavior.AllowGet);
                var _courserContentLine = Mapper.Map<CourseContentLine>(_courseContentService.GetBy(courseContentLine.IdCourse, courseContentLine.LineNo));
                _courserContentLine.Title = courseContentLine.Title;
                _courserContentLine.Times = courseContentLine.Times;
                _courserContentLine.LinkVideoVN = courseContentLine.LinkVideoVN;
                _courserContentLine.LinkVideoEN = courseContentLine.LinkVideoEN;
                _courserContentLine.SortOrder = courseContentLine.SortOrder;
                _courserContentLine.ParentSortOrder = courseContentLine.ParentSortOrder;
                _courserContentLine.IsHidden = courseContentLine.IsHidden;
                _courserContentLine.IsExperience = courseContentLine.IsExperience;
                _courserContentLine.LessonTypeCode = courseContentLine.LessonTypeCode;
                _courserContentLine.LessonTypeName = _lessonTypeInfo.Name;
                hasValue = _courseContentService.Update(Mapper.Map<E_CourseContent>(_courserContentLine));
                hasValue = SubCourseContents(courseContentLine.IdCourse, courseContentLine.LineNo, courseContentLine.ReviewCodeArrs);
                hasValue = _vuViecService.Commit();
                return Json(new { statusCode = string.IsNullOrEmpty(hasValue) ? "SUCCESS" : "ERROR", statusName = hasValue }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { statusCode = "ERROR", statusName = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Delete(int idCourse, int lineNo)
        {
            try
            {
                hasValue = _courseContentService.Delete(idCourse, lineNo);
                hasValue = _vuViecService.Commit();
                return Json(new { statusCode = string.IsNullOrEmpty(hasValue) ? "SUCCESS" : "ERROR", statusName = hasValue }, JsonRequestBehavior.AllowGet);
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }
        public List<OptionInt> ReviewList(int idCourse)
        {
            try
            {
                using (var conn = new SqlConnection(stnhdConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "COURSE.OTB.LIST");
                    paramaters.Add("@IdCouse", idCourse);
                    return conn.Query<OptionInt>("SP_Cource", paramaters, null, true, null, System.Data.CommandType.StoredProcedure).ToList();
                }
            }
            catch { return new List<OptionInt>(); }
        }
        private string SubCourseContents(int idCourse, int lineNo, int[] subLines)
        {
            if (subLines == null)
                return string.Empty;
            try
            {
                var _subLines = new List<SubEcourceContent>();
                var _subLine = new SubEcourceContent();
                foreach (var item in subLines)
                {
                    _subLine = new SubEcourceContent();
                    _subLine.IdCourse = idCourse;
                    _subLine.LineNo = lineNo;
                    _subLine.SubLineNo = item;
                    _subLines.Add(_subLine);
                }
                return _subSourseContentService.Insert(idCourse, lineNo, Mapper.Map<IEnumerable<E_SubEcourceContent>>(_subLines));
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
    }
}