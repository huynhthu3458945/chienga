﻿using System.Web.Mvc;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using System.Net;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using GCSOFT.MVC.Data;
using GCSOFT.MVC.Model.MasterData;
using System.Web;
using System.Text.RegularExpressions;
using System.IO;
using GCSOFT.MVC.Web.Areas.MasterData.Models;
using System.Data.SqlClient;
using System.Configuration;
using Dapper;
using GCSOFT.MVC.Service.ExeclOfficeEngine;
using System.Data.OleDb;
using System.Data;

namespace GCSOFT.MVC.Web.Areas.MasterData.Controllers
{
	[CompressResponseAttribute]
    public class CourseController : BaseController
    {
		#region -- Properties --
		private readonly IVuViecService _vuViecService;
		private readonly ICourseService _courseService;
        private readonly ICourseContentService _courseContentService;
        private readonly ICourseResourceService _courseResourceService;
        private readonly ITeacherService _teacherService;
        private readonly IClassService _classService;
        private readonly IResultEntryService _resultEntryService;
        private readonly IQuestionService _questionService;
        private readonly ISubEcourceContentService _subSourseContentService;
        private string sqlSTNHDConnection = ConfigurationManager.ConnectionStrings["GCConnection"].ConnectionString;
        private string sysCategory = "COURSE";
        private bool? permission = false;
        private CourseCard courseCard;
        private E_Course e_Course;
        //private IEnumerable<Question_Card> questions;
        private string hasValue;
		#endregion
		
		#region -- Contructor --
		public CourseController
            (
				IVuViecService vuViecService
                , ICourseService courseService
                , ICourseContentService courseContentService
                , ITeacherService teacherService
                , ICourseResourceService courseResourceService
                , IClassService classService
                , IResultEntryService resultEntryService
                , IQuestionService questionService
                , ISubEcourceContentService subSourseContentService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _courseService = courseService;
            _courseContentService = courseContentService;
            _teacherService = teacherService;
            _courseResourceService = courseResourceService;
            _classService = classService;
            _resultEntryService = resultEntryService;
            _questionService = questionService;
            _subSourseContentService = subSourseContentService;
        }
		#endregion

        public ActionResult Index()
        {
			#region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            CourcePage course = new CourcePage()
            {
                ClassList = Mapper.Map<IEnumerable<VM_Class>>(_classService.FindAll()).ToList().ToSelectListItems(0)
            };
            return View(course);
        }

        public ActionResult Details(int? id)
        {
			if (id == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.ViewDetail);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            courseCard = Mapper.Map<CourseCard>(_courseService.GetBy3(id ?? 0));
            if (courseCard == null)
                return HttpNotFound();
            return View(courseCard);
        }

        public ActionResult Create()
        {
			#region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            courseCard = new CourseCard();

            var contentLines = new List<CourseContentLine>();
            var contentLine = new CourseContentLine();
            //contentLine.LineNo = 0;
            //contentLine.CourseType = CourseType.Mindmap;
            //contentLines.Add(contentLine);
            //courseCard.CourseContents = contentLines;

            var videoLines = new List<CourseContentLine>();
            var videoLine = new CourseContentLine();
            videoLine.LineNo = 0;
            contentLine.CourseType = CourseType.Videos;
            videoLines.Add(videoLine);
            courseCard.CourseVideos = videoLines;

            var contentResources = new List<CourseResourceLine>();
            var contentResource = new CourseResourceLine();
            contentResource.LineNo = 0;
            contentResources.Add(contentResource);
            courseCard.CourseResources = contentResources;

            courseCard.TeacherList = Mapper.Map<IEnumerable<TeacherList>>(_teacherService.FindAll()).ToSelectListItems(0);
            courseCard.ClassList = Mapper.Map<IEnumerable<VM_Class>>(_classService.FindAll()).ToList().ToSelectListItems(0);
            courseCard.SqlCourseContents = new List<SqlCourseContent>();
            return View(courseCard);
        }

        [HttpPost]
        public ActionResult Create(CourseCard _vm_Course)
        {
			try
            {
                SetData(_vm_Course, true);
                hasValue = _courseService.Insert(e_Course);
				hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = e_Course.Id, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        public ActionResult Edit(int? id)
        {
			if (id == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
			courseCard = Mapper.Map<CourseCard>(_courseService.GetBy(id ?? 0));
            if (courseCard == null) 
				return HttpNotFound();
            courseCard.TeacherList = Mapper.Map<IEnumerable<TeacherList>>(_teacherService.FindAll()).ToSelectListItems(courseCard.IdTeacher);
            courseCard.ClassList = Mapper.Map<IEnumerable<VM_Class>>(_classService.FindAll()).ToList().ToSelectListItems(courseCard.ClassId);
            courseCard.SqlCourseContents = ExecSqlCourseContentList(id ?? 0, 1);
            ViewBag.idCourse = id;
            ViewBag.gradeId = courseCard.ClassId;
            return View(courseCard);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">Course Id</param>
        /// <returns></returns>
        public ActionResult Videos(int? id)
        {
            if (id == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            ViewBag.courseInfo = Mapper.Map<CourseList>(_courseService.GetBy_2(id ?? 0));
            var courseContent = Mapper.Map<IEnumerable<CourseContentLine>>(_courseContentService.FindAll(id ?? 0, CourseType.Videos));
            if (courseContent == null)
                return HttpNotFound();
            ViewBag.courseId = id;
            return View(courseContent);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">Course Id</param>
        /// <returns></returns>
        public ActionResult Mindmap(int? id)
        {
            if (id == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var courseContent = Mapper.Map<IEnumerable<CourseContentLine>>(_courseContentService.FindAll(id ?? 0, CourseType.Mindmap));
            if (courseContent == null)
                return HttpNotFound();
            ViewBag.courseId = id;
            return View(courseContent);
        }

        [HttpPost]
        public ActionResult Edit(CourseCard _vm_Course)
        {
			try
            {
                SetData(_vm_Course, false);
                hasValue = _courseService.Update(e_Course);
                //hasValue = _questionService.Update(Mapper.Map<IEnumerable<M_Question>>(questions));
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = e_Course.Id, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        public ActionResult Deletes(string id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                int[] _codes = id.Split(',').Select(item => int.Parse(item)).ToArray();
                var courseResources = Mapper.Map<IEnumerable<CourseResourceLine>>(_courseResourceService.FindAll(_codes)).ToList();
                string pathProImgs = HttpContext.Server.MapPath("~/Files/CourseResource/");

                var courseImgs = Mapper.Map<IEnumerable<CourseCard>>(_courseService.FindAll(_codes)).Where(d => !string.IsNullOrEmpty(d.Image)).ToList();
                string pathCourses = HttpContext.Server.MapPath("~/Files/Course/");

                hasValue = _courseService.Delete(_codes);
                hasValue = _resultEntryService.Delete(ResultType.Cource, _codes);
				hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                {
                    UploadHelper _upload = new UploadHelper();
                    courseResources.ForEach(item =>
                    {
                        _upload.DeleteFileOnServer(pathProImgs + item.IdCourse.ToString() + "/", item.Image);
                    });
                    courseImgs.ForEach(item => {
                        _upload.DeleteFileOnServer(pathCourses + item.Id.ToString() + "/", item.Image);
                    });
                    return Json(new { v = true, count = _codes.Count() });
                }
                return Json("Xóa dữ liệu không thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }
        public JsonResult DeleteImg(int id, string img)
        {
            hasValue = _courseService.DeleteImg(id);
            hasValue = _vuViecService.Commit();
            if (string.IsNullOrEmpty(hasValue))
            {
                UploadHelper _upload = new UploadHelper();
                string PathSave = HttpContext.Server.MapPath("~/Files/Course/" + id.ToString() + "/");
                _upload.DeleteFileOnServer(PathSave, img);
            }
            return Json(string.IsNullOrEmpty(hasValue));
        }
        public PartialViewResult ReloadVideosContentLine(int courseId)
        {
            var courseInfo = Mapper.Map<CourseCard>(_courseService.GetBy_2(courseId));
            var courseContent = ExecSqlCourseContentList(courseId, 1);
            ViewBag.idCourse = courseId;
            ViewBag.gradeId = courseInfo.ClassId;
            return PartialView("_CardVideosLine", courseContent);
        }
        [HttpPost]
        public JsonResult ImportDataVideos(int idCourse)
        {
            var PostedFile = Request.Files[0];
            string filename = PostedFile.FileName;
            string targetpath = Server.MapPath("/Areas/MasterData/Files/");
            if (!Directory.Exists(targetpath))
                Directory.CreateDirectory(targetpath);
            PostedFile.SaveAs(targetpath + filename);
            string pathToExcelFile = targetpath + filename;
            var connectionString = "";
            if (filename.EndsWith(".xls"))
            {
                connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=Excel 8.0;", pathToExcelFile);
            }
            else if (filename.EndsWith(".xlsx"))
            {
                connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";", pathToExcelFile);
            }
            var adapter = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", connectionString);
            var ds = new DataSet();
            adapter.Fill(ds, "ExcelTable");
            DataTable dtable = ds.Tables["ExcelTable"];
            int index = 0;
            var lessonTypeCode = string.Empty;
            var _lineNo1Tiet = string.Empty;
            var _lineNo = _courseContentService.GetID(idCourse) - 100;
            var courseContents = new List<CourseContentLine>();
            var courseContentOTCs = new List<CourseContentLine>();
            var courseContent = new CourseContentLine();
            hasValue = _courseContentService.Delete(idCourse, "OTC");
            foreach (DataRow item in dtable.Rows)
            {
                var lineNo = item["LineNo"].ToString();
                if (!string.IsNullOrEmpty(lineNo))
                {
                    lessonTypeCode = item["MaLoai"].ToString();
                    if (lessonTypeCode.Equals("OTC"))
                    {
                        _lineNo += 100;
                        courseContent = new CourseContentLine();
                        courseContent.IdCourse = idCourse;
                        courseContent.LineNo = _lineNo;
                        courseContent.CourseType = CourseType.Videos;
                        courseContent.SortOrder = int.Parse(item["SoThuThu"].ToString());
                        courseContent.ParentSortOrder = int.Parse(item["MaCha"].ToString());
                        courseContent.Title = "Kiểm tra 1 tiết";
                        courseContent.Times = "45:00";
                        courseContent.LessonTypeCode = "OTC";
                        courseContent.LessonTypeName = "Ôn Tập Chương";
                        _lineNo1Tiet = item["LineNoOTCs"].ToString();
                        courseContent.ReviewCodeArrs = _lineNo1Tiet.Split(';').Select(d => int.Parse(d)).ToArray();
                        courseContentOTCs.Add(courseContent);
                    } else
                    {
                        courseContent = Mapper.Map<CourseContentLine>(_courseContentService.GetBy(idCourse, int.Parse(item["LineNo"].ToString()), CourseType.Videos));
                        courseContent.SortOrder = int.Parse(item["SoThuThu"].ToString());
                        courseContent.ParentSortOrder = int.Parse(item["MaCha"].ToString());
                        courseContent.Title = item["MucLuc"]?.ToString() ?? "";
                        courseContent.Times = item["ThoiGian"]?.ToString() ?? "";
                        courseContents.Add(courseContent);
                    }
                    
                }
                index++;
            }
            hasValue = _courseContentService.Updates(Mapper.Map<IEnumerable<E_CourseContent>>(courseContents));
            hasValue = _courseContentService.Insert(Mapper.Map<IEnumerable<E_CourseContent>>(courseContentOTCs));
            hasValue = _vuViecService.Commit();
            foreach (var item in courseContentOTCs)
            {
                hasValue = SubCourseContents(item.IdCourse, item.LineNo, item.ReviewCodeArrs);
                hasValue = _vuViecService.Commit();
            }
            return Json(new { retCode = 0, retText = "Thành công" });
        }
        public ActionResult DownloadVideosData(int courseId)
        {
            var courseContent = ExecSqlCourseContentList(courseId, 1);
            var dataModel = courseContent.ToList().ConvertToDataTable();
            dataModel.TableName = "Model";
            var stream = Excel.ExportReport(dataModel, HttpContext.Server.MapPath("/Areas/MasterData/Report/VideoContentList.xlsx"), false, OfficeType.XLSX);
            return File(stream, OfficeContentType.XLSX, "VideosContentList.xlsx");
        }
        
        #region -- Json --
        public JsonResult LoadData(GridSettings grid, int? classId, string title, bool status, bool isTNTD)
        {
            if (classId == 0)
                classId = null;
            var list = Mapper.Map<IEnumerable<CourseList>>(_courseService.FindAll(classId, title, status, isTNTD)).AsQueryable();
            list = JqGrid.SetupGrid<CourseList>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Load Course By Class
        /// </summary>
        /// <param name="c">idCourse</param>
        /// <returns></returns>
        public JsonResult FetchCourses(int c)
        {
            return Json(Mapper.Map<IEnumerable<CourseList>>(_courseService.GetCourseByClass(c)), JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region -- Functions --
        private void SetData(CourseCard _vm_Course, bool isCreate)
        {
            UploadHelper _uploadHelper = new UploadHelper();
            courseCard = _vm_Course;
            if (isCreate)
            {
                courseCard.Id = _courseService.GetID();
                courseCard.DateCreate = DateTime.Now;
            }
            
            courseCard.DateModify = DateTime.Now;
            var teacher = Mapper.Map<TeacherList>(_teacherService.GetBy(courseCard.IdTeacher)) ?? new TeacherList();
            courseCard.TeacherName = teacher.FullName;

            var classInfo = Mapper.Map<VM_Class>(_classService.GetBy(courseCard.ClassId)) ?? new VM_Class();
            courseCard.ClassName = classInfo.Name;

            _uploadHelper.PathSave = HttpContext.Server.MapPath("~/Files/Course/" + courseCard.Id.ToString() + "/");
            _uploadHelper.UploadFile(Request.Files["fileUpload"]);
            if (!string.IsNullOrEmpty(_uploadHelper.FileSave))
            {
                courseCard.Image = _uploadHelper.FileSave;
            }
           
            int lineNo = 100;
            int lineNoLast = courseCard.CourseResources.Where(d => d.LineNo != 0).OrderByDescending(d => d.LineNo).Select(d => d.LineNo).FirstOrDefault();
            var courseResourceLine = new List<CourseResourceLine>();
            courseCard.CourseResources.ToList().ForEach(item => {
                if (item.LineNo == 0)
                {
                    lineNoLast += lineNo;
                    item.LineNo = lineNoLast;
                }
                item.IdCourse = courseCard.Id;
                courseResourceLine.Add(item);
            });
            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFileBase file = Request.Files[i];
                var namControl = Request.Files.AllKeys[i].ToString();
                if (!namControl.Equals("fileUpload"))
                {
                    _uploadHelper = new UploadHelper();
                    _uploadHelper.PathSave = HttpContext.Server.MapPath("~/Files/CourseResource/" + courseCard.Id.ToString() + "/");
                    _uploadHelper.UploadFile(file);
                    string index = Regex.Match(namControl, @"\d+").Value;
                    if (!string.IsNullOrEmpty(courseResourceLine[int.Parse(index)].Image) && !string.IsNullOrEmpty(_uploadHelper.FileSave))
                        _uploadHelper.DeleteFileOnServer(_uploadHelper.PathSave, courseResourceLine[int.Parse(index)].Image);
                    if (!string.IsNullOrEmpty(_uploadHelper.FileSave))
                        courseResourceLine[int.Parse(index)].Image = _uploadHelper.FileSave;
                }
            }
            courseCard.CourseResources = courseResourceLine;
            //-- Add Course Resource End
            //-- Add Course Support
            var courseSupports = new List<CourseSupportLine>();
            courseCard.CourseSupports.ToList().ForEach(itemSupport => {
                itemSupport.IdCourse = courseCard.Id;
                courseSupports.Add(itemSupport);
            });
            courseCard.CourseSupports = courseSupports;
            //-- Add Course Support End
            //-- Add Couse Categroy
            var courseCategories = new List<CourseCategoryLine>();
            courseCard.CourseCategories.ToList().ForEach(itemCate => {
                itemCate.IdCourse = courseCard.Id;
                courseCategories.Add(itemCate);
            });
            courseCard.CourseCategories = courseCategories;
            //-- Add Couse Categroy End
            e_Course = Mapper.Map<E_Course>(courseCard);
        }
        #endregion
        private IEnumerable<SqlCourseContent> ExecSqlCourseContentList(int courseId, int courseType)
        {
            try
            {
                using (var conn = new SqlConnection(sqlSTNHDConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@CourseId", courseId);
                    paramaters.Add("@CourseType", courseType);
                    return conn.Query<SqlCourseContent>("SP_CourseContentExcel", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }
            }
            catch { return new List<SqlCourseContent>(); }
        }
        private string SubCourseContents(int idCourse, int lineNo, int[] subLines)
        {
            if (subLines == null)
                return string.Empty;
            try
            {
                var _subLines = new List<SubEcourceContent>();
                var _subLine = new SubEcourceContent();
                foreach (var item in subLines)
                {
                    _subLine = new SubEcourceContent();
                    _subLine.IdCourse = idCourse;
                    _subLine.LineNo = lineNo;
                    _subLine.SubLineNo = item;
                    _subLines.Add(_subLine);
                }
                return _subSourseContentService.Insert(idCourse, lineNo, Mapper.Map<IEnumerable<E_SubEcourceContent>>(_subLines));
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
    }
}