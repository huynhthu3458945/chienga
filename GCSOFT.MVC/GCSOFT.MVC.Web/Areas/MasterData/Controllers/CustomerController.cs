﻿using AutoMapper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Areas.MasterData.Models;
using GCSOFT.MVC.Web.Areas.Transaction.Data;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.MasterData.Controllers
{
    [CompressResponseAttribute]
    public class CustomerController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly ICustomerService _customerService;
        private readonly IOptionLineService _optionLineService;
        private readonly ICardLineService _cardLineService;
        private readonly ICardEntryService _cardEntryService;
        private readonly IMailSetupService _mailSetupService;
        private readonly ITemplateService _templateService;
        private string sysCategory = "CUST";
        private bool? permission = false;
        private CustomerViewModel customerViewModel;
        private M_Customer mCustomer;
        private string hasValue;
        #endregion

        #region -- Contructor --
        public CustomerController
            (
                IVuViecService vuViecService
                , ICustomerService customerService
                , IOptionLineService optionLineService
                , ICardLineService cardLineService
                , ICardEntryService cardEntryService
                , IMailSetupService mailSetupService
                , ITemplateService templateService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _customerService = customerService;
            _optionLineService = optionLineService;
            _cardLineService = cardLineService;
            _cardEntryService = cardEntryService;
            _mailSetupService = mailSetupService;
            _templateService = templateService;
        }
        #endregion
        // GET: MasterData/Customer
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }
        public ActionResult Edit(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            customerViewModel = Mapper.Map<CustomerViewModel>(_customerService.GetBy(id));
            if (customerViewModel == null)
                return HttpNotFound();
            return View(customerViewModel);
        }
        [HttpPost]
        public ActionResult Edit(CustomerViewModel _customerViewModel)
        {
            try
            {
                SetData(_customerViewModel);
                hasValue = _customerService.Update(mCustomer);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = mCustomer.Id, msg = "2" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public JsonResult LoadData(GridSettings grid)
        {
            var list = Mapper.Map<IEnumerable<CustomerViewModel>>(_customerService.FindAll("LO2103-013")).AsQueryable();
            list = JqGrid.SetupGrid<CustomerViewModel>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
        private void SetData(CustomerViewModel _customerViewModel)
        {
            customerViewModel = Mapper.Map<CustomerViewModel>(_customerService.GetBy2(_customerViewModel.Id));
            customerViewModel.FullName = _customerViewModel.FullName;
            customerViewModel.Email = _customerViewModel.Email;
            customerViewModel.PhoneNo = _customerViewModel.PhoneNo;
            customerViewModel.Owner = _customerViewModel.Owner;
            var ReasonInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("STA_CUST", customerViewModel.StatusCode)) ?? new VM_OptionLine();
            customerViewModel.StatusId = ReasonInfo.LineNo;
            customerViewModel.StatusCode = ReasonInfo.Code;
            customerViewModel.StatusName = ReasonInfo.Name;
            mCustomer = Mapper.Map<M_Customer>(customerViewModel);
        }
        public JsonResult ChangeStatus(int id) {
            try
            {
                customerViewModel = Mapper.Map<CustomerViewModel>(_customerService.GetBy2(id));
                
                var ReasonInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("STA_CUST", "300")) ?? new VM_OptionLine();
                customerViewModel.StatusId = ReasonInfo.LineNo;
                customerViewModel.StatusCode = ReasonInfo.Code;
                customerViewModel.StatusName = ReasonInfo.Name;
                hasValue = _customerService.Update(Mapper.Map<M_Customer>(customerViewModel));
                hasValue = _vuViecService.Commit();
                var _status = string.IsNullOrEmpty(hasValue) ? "Oke" : "Error";
                var _msg = string.IsNullOrEmpty(hasValue) ? "" : hasValue;
                return Json(new { s = _status, m = _msg }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex) { return Json(new { s = "Error", m = ex.ToString() }, JsonRequestBehavior.AllowGet); }
        }
        public JsonResult AssignCode(int id)
        {
            try
            {
                var idCard = _cardLineService.GetID();
                var userInfo = GetUser();
                customerViewModel = Mapper.Map<CustomerViewModel>(_customerService.GetBy2(id));
                var ReasonInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("STA_CUST", "200")) ?? new VM_OptionLine();
                customerViewModel.StatusId = ReasonInfo.LineNo;
                customerViewModel.StatusCode = ReasonInfo.Code;
                customerViewModel.StatusName = ReasonInfo.Name;
                //-- Build Ma Kich Hoat
                var builder = new StringBuilder();
                Guid g = Guid.NewGuid();
                builder.Append(g.ToString().Split('-')[0].ToString().ToUpper());
                var randomNumber = new RandomNumberGenerator();
                builder.Append(randomNumber.RandomNumber(10, 99));
                var _carNo = builder.ToString();
                //-- Build Ma Kich Hoat +
                var cryptorEngine = new CryptorEngine2();
                var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("CARD", "450"));
                var cardLine = new CardLine()
                {
                    Id = idCard,
                    LotNumber = customerViewModel.LotNumber,
                    SerialNumber = StringUtil.GetProductID("C", (idCard - 1).ToString(), 9),
                    CardNo = cryptorEngine.Encrypt(_carNo, true, "TTLSTNHD@CARD"),
                    Price = 1468000,
                    UserName = customerViewModel.UserOwner,
                    UserFullName = customerViewModel.FullName,
                    DateCreate = DateTime.Now,

                    UserType = UserType.TTL,
                    UserNameActive = userInfo.Username,
                    UserActiveFullName = userInfo.FullName,
                    LastDateUpdate = DateTime.Now,
                    StatusId = statusInfo.LineNo,
                    StatusCode = statusInfo.Code,
                    StatusName = statusInfo.Name,
                    LastStatusId = statusInfo.LineNo,
                    LastStatusCode = statusInfo.Code,
                    LastStatusName = statusInfo.Name,

                    DurationId = 44,
                    DurationCode = "ALL",
                    DurationName = "Vĩnh Viễn",

                    ReasonId = 45,
                    ReasonCode = "BH",
                    ReasonName = "Bán Hàng"
                };
                var cardEntry = new CardEntry()
                {
                    LotNumber = cardLine.LotNumber,
                    CardLineId = idCard,
                    SerialNumber = cardLine.SerialNumber,
                    CardNo = cardLine.CardNo,
                    Price = cardLine.Price,
                    UserName = cardLine.UserName,
                    UserFullName = cardLine.UserFullName,
                    DateCreate = cardLine.DateCreate,
                    UserType = UserType.TTL,
                    UserNameActive = cardLine.UserNameActive,
                    UserActiveFullName = cardLine.UserActiveFullName,
                    Date = cardLine.DateCreate,
                    StatusId = statusInfo.LineNo,
                    StatusCode = statusInfo.Code,
                    StatusName = statusInfo.Name,
                    Source = CardSource.CardSTNHD,
                    SourceNo = cardLine.LotNumber,
                    DurationId = cardLine.DurationId,
                    DurationCode = cardLine.DurationCode,
                    DurationName = cardLine.DurationName,
                    ReasonId = cardLine.ReasonId,
                    ReasonCode = cardLine.ReasonCode,
                    ReasonName = cardLine.ReasonName
                };
                customerViewModel.CardNo = cardLine.CardNo;
                customerViewModel.SerialNumber = cardLine.SerialNumber;
                customerViewModel.DateCard = DateTime.Now;
                hasValue = _customerService.Update(Mapper.Map<M_Customer>(customerViewModel));
                hasValue = _cardLineService.Insert(Mapper.Map<M_CardLine>(cardLine));
                hasValue = _cardEntryService.Insert(Mapper.Map<M_CardEntry>(cardEntry));
                hasValue = _vuViecService.Commit();
                hasValue = SendCardViaSMS(customerViewModel);
                var _status = string.IsNullOrEmpty(hasValue) ? "Oke" : "Error";
                var _msg = string.IsNullOrEmpty(hasValue) ? "" : hasValue;
                return Json(new { s = _status, m = _msg }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { s = "Error", m = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        private string SendCardViaSMS(CustomerViewModel customerVM)
        {
            try
            {
                if (StringUtil.Validate(customerVM.Email))
                {
                    var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
                    var mailTemplate = Mapper.Map<Template>(_templateService.Get("EMAILACTIVECODE"));
                    var mailTitle = mailTemplate.Title;
                    var mailContent = mailTemplate.Content.Replace("{{FULLNAME}}", customerVM.FullName);
                    mailContent = mailContent.Replace("{{ACTIVECODE}}", CryptorEngine.Decrypt(customerVM.CardNo, true, "TTLSTNHD@CARD"));
                    var emailTos = new List<string>();
                    emailTos.Add(customerVM.Email);
                    var _mailHelper = new EmailHelper()
                    {
                        EmailTos = emailTos,
                        DisplayName = emailInfo.DisplayName,
                        Title = mailTitle,
                        Body = mailContent,
                        MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
                    };
                    hasValue = _mailHelper.DoSendMail();
                }
                if (!string.IsNullOrEmpty(customerVM.PhoneNo))
                {
                    var newPhoneNo = customerVM.PhoneNo.Trim();
                    var firstNumber = int.Parse(newPhoneNo[0].ToString());
                    if (firstNumber != 0)
                    {
                        newPhoneNo = "0" + newPhoneNo;
                    }
                    var smsTemplate = Mapper.Map<Template>(_templateService.Get("SMS_MAKICHHOAT"));
                    var smsHelper = new SMSHelper()
                    {
                        PhoneNo = newPhoneNo,
                        Content = StringUtil.StripTagsRegexCompiled(smsTemplate.Content).Replace("{{NAME}}", "vao 5 Phut Thuoc Bai").Replace("{{MAKICHHOAT}}", CryptorEngine.Decrypt(customerVM.CardNo, true, "TTLSTNHD@CARD")).Replace("{{LINK}}", "https://sieutrinhohocduong.com/student")
                    };
                    hasValue = smsHelper.Send();
                }
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
    }
}