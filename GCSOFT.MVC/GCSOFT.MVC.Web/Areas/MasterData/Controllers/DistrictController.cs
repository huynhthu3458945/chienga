﻿using AutoMapper;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Areas.MasterData.Models;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.MasterData.Controllers
{
    [CompressResponseAttribute]
    public class DistrictController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IDistrictService _districtService;
        private string sysCategory = "DISTRICT";
        private bool? permission = false;
        private DistrictViewModel districtViewModel;
        private M_District mDistrict;
        private string hasValue;
        #endregion

        #region -- Contructor --
        public DistrictController
            (
                IVuViecService vuViecService
                , IDistrictService districtService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _districtService = districtService;
        }
        #endregion
        /// <summary>
        /// Load District
        /// </summary>
        /// <param name="p">province id</param>
        /// <returns></returns>
        public JsonResult FetchDistricts(int provinceId)
        {
            return Json(Mapper.Map<IEnumerable<DistrictViewModel>>(_districtService.FindAll(provinceId)), JsonRequestBehavior.AllowGet);
        }
    }
}