﻿using AutoMapper;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Areas.MasterData.Models;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.MasterData.Controllers
{
    [CompressResponseAttribute]
    public class LanguageController : BaseController
    {
        #region -- Properties --
        private readonly ILanguageService _languageService;
        private string sysCategory = "LANGUAGE";
        private bool? permission = false;
        private VM_Language vm_Language;
        private M_Language m_Language;
        private VM_OptionType setupImageInfo;
        private string hasValue;
        #endregion

        #region -- Contructor --
        public LanguageController
            (
                IVuViecService vuViecService
                , ILanguageService languageService
            ) : base(vuViecService)
        {
            _languageService = languageService;
        }
        #endregion

        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }
        public JsonResult LoadData(GridSettings grid)
        {
            var list = Mapper.Map<IEnumerable<VM_Language>>(_languageService.FindAll()).AsQueryable();
            list = JqGrid.SetupGrid<VM_Language>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
    }
}