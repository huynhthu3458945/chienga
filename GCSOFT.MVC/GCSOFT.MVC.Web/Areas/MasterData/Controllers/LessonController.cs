﻿using System.Web.Mvc;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using System.Net;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Web.Areas.MasterData.Models;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Data.Repositories.MasterDataRepositories.Interface;
using System.Web;
using System.Text.RegularExpressions;
using ImageResizer;
using System.Web.Helpers;

namespace GCSOFT.MVC.Web.Areas.MasterData.Controllers
{
	[CompressResponseAttribute]
    public class LessonController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IQuestionService _questionService;
        private readonly ITeacherService _techerService;
        private readonly IParentsService _parentsService;
        private readonly IOptionLineService _optionLineService;
        private readonly IClassService _classService;
        private readonly IResultEntryService _resultEntryService;
        private readonly ICourseService _courseService;
        private readonly ICourseContentService _courseContentService;
        private readonly IFileResourceService _fileResourceService;
        private string sysCategory = "LESSON";
        private bool? permission = false;
        private Question_Card questionCard;
        private M_Question mQuestion;
        private string hasValue;
        private string ImagePath = "~/Files/Sample/{0}/{1}";
        #endregion

        #region -- Contructor --
        public LessonController
            (
                IVuViecService vuViecService
                , IQuestionService questionService
                , ITeacherService techerService
                , IParentsService parentsService
                , IClassService classService
                , IOptionLineService optionLineService
                , IResultEntryService resultEntryService
                , ICourseService courseService
                , ICourseContentService courseContentService
                , IFileResourceService fileResourceService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _questionService = questionService;
            _techerService = techerService;
            _parentsService = parentsService;
            _classService = classService;
            _optionLineService = optionLineService;
            _resultEntryService = resultEntryService;
            _courseService = courseService;
            _courseContentService = courseContentService;
            _fileResourceService = fileResourceService;
        }
        #endregion

        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }
        public ActionResult IndexPopup()
        {
            return View("_IndexPopup");
        }
        public ActionResult Details(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.ViewDetail);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            questionCard = Mapper.Map<Question_Card>(_questionService.GetBy(id));
            if (questionCard == null)
                return HttpNotFound();
            return View(questionCard);
        }

        public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            questionCard = new Question_Card();
            questionCard.OwnerList = Mapper.Map<IEnumerable<TeacherList>>(_techerService.FindAll()).ToSelectListItems(0);
            questionCard.ClassList = Mapper.Map<IEnumerable<VM_Class>>(_classService.FindAll(ClassType.Class)).ToList().ToSelectListItems(0);
            questionCard.CourseList = Mapper.Map<IEnumerable<CourseList>>(_courseService.GetCourseByClass(0)).ToSelectListItems(0);
            questionCard.CourseContentList = Mapper.Map<IEnumerable<CourseContentLine>>(_courseContentService.FindAll(0)).ToSelectListItems(0);
            return View(questionCard);
        }
        public ActionResult CreateMultiple(int? classId, int? courseId)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            questionCard = new Question_Card();
            questionCard.ClassList = Mapper.Map<IEnumerable<VM_Class>>(_classService.FindAll(ClassType.Class)).ToList().ToSelectListItems(classId ?? 0);
            questionCard.CourseList = Mapper.Map<IEnumerable<CourseList>>(_courseService.GetCourseByClass(classId ?? 0)).ToSelectListItems(courseId ?? 0);
            questionCard.CourseContentList = Mapper.Map<IEnumerable<CourseContentLine>>(_courseContentService.FindAll(courseId ?? 0)).ToSelectListItems(0);
            return View(questionCard);
        }

        [HttpPost]
        public ActionResult Create(Question_Card _questionCard)
        {
            try
            {
                SetData(_questionCard, true);
                hasValue = _questionService.Insert(mQuestion);
                hasValue = _resultEntryService.Delete(ResultType.Sample, mQuestion.Id);
                hasValue = _resultEntryService.Insert(GetResultEntry(mQuestion));
                hasValue = _fileResourceService.Insert(mQuestion.FileResources);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = mQuestion.Id, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult CreateMultiple(Question_Card _questionCard)
        {
            try
            {
                var _questions = new List<Question_Card>();
                var _question = new Question_Card();
                var _fileResources = new List<VM_FileResource>();
                var _fileResource = new VM_FileResource();

                var _courseContents = new List<CourseContentLine>();
                var _courseContent = new CourseContentLine();
                int lineNo = _courseContentService.GetLastLineNo(_questionCard.CourseId);

                var questionID = _questionService.GetID() - 1;
                var _uploadHelper = new UploadHelper();
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    questionID += 1;
                    _question = new Question_Card();
                    _question.Id = questionID;
                    _question.DateQuestion = DateTime.Now;
                    _question.DateAnswer = DateTime.Now;
                    _question.Type = QuestionType.Sample;
                    var classInfo = Mapper.Map<VM_Class>(_classService.GetBy(_questionCard.ClassId)) ?? new VM_Class();
                    _question.ClassId = classInfo.Id;
                    _question.ClassCode = classInfo.Code;
                    _question.ClassName = classInfo.Name;
                    _question.LevelId = classInfo.LevelId;
                    _question.LevelName = classInfo.LevelName;
                    var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get(3, 9)) ?? new VM_OptionLine();
                    _question.StatusId = 9;
                    _question.StatusCode = statusInfo.Code;
                    _question.StatusName = statusInfo.Name;
                    _question.CourseId = _questionCard.CourseId;
                    _question.CourseName = (Mapper.Map<CourseList>(_courseService.GetBy_2(_questionCard.CourseId))).Title;
                    

                    HttpPostedFileBase file = Request.Files[i];
                    _uploadHelper = new UploadHelper();
                    _uploadHelper.PathSave = HttpContext.Server.MapPath("~/Files/Sample/" + questionID.ToString() + "/");
                    _uploadHelper.UploadFile(file);
                    _fileResource = new VM_FileResource() {
                        LineNo = 100,
                        ReferId = questionID,
                        Type = ResultType.Sample,
                        FileType = FileType.ImagePath,
                        Source = Source.BackEnd,
                        ImageName = _uploadHelper.FileSave,
                        ImagePath = string.Format("~/Files/Sample/{0}/{1}", questionID, _uploadHelper.FileSave) //HttpContext.Server.MapPath("~/Files/Sample/" + questionID.ToString() + "/") + _uploadHelper.FileSave
                    };
                    //Resize image
                    var path = Server.MapPath(_fileResource.ImagePath);
                    ResizeSettings resizeSetting = new ResizeSettings
                    {
                        Width = 1920,
                        Height = 1920,
                        Format = "jpg"
                    };
                    ImageBuilder.Current.Build(path, path, resizeSetting);
                    //Resize image +
                    _fileResources.Add(_fileResource);

                    lineNo += 100;
                    _courseContent = new CourseContentLine();
                    _courseContent.IdCourse = _question.CourseId;
                    _courseContent.LineNo = lineNo;
                    _courseContent.Title = file.FileName.Replace(".jpg","").Replace(".png", "");
                    _courseContent.SortOrder = lineNo;
                    _courseContents.Add(_courseContent);

                    _question.CourseContentLineNo = _courseContent.LineNo;
                    _question.CourseContentName = _courseContent.Title;
                    _questions.Add(_question);
                }


                hasValue = _questionService.Insert(Mapper.Map<IEnumerable<M_Question>>(_questions));
                hasValue = _fileResourceService.Insert(Mapper.Map<IEnumerable<M_FileResource>>(_fileResources));
                hasValue = _courseContentService.Insert(Mapper.Map<IEnumerable<E_CourseContent>>(_courseContents));
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("CreateMultiple", new { classId = _questionCard.ClassId, courseId = _questionCard.CourseId, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        public ActionResult Edit(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            questionCard = Mapper.Map<Question_Card>(_questionService.GetBy(id));
            if (questionCard == null)
                return HttpNotFound();
            questionCard.DateQuestionStr = string.Format("{0:dd/MM/yyyy}", questionCard.DateQuestion);
            questionCard.DateAnswerStr = string.Format("{0:dd/MM/yyyy}", questionCard.DateAnswer);
            questionCard.OwnerList = Mapper.Map<IEnumerable<TeacherList>>(_techerService.FindAll()).ToSelectListItems(questionCard.OwnerId);
            questionCard.ClassList = Mapper.Map<IEnumerable<VM_Class>>(_classService.FindAll(ClassType.Class)).ToList().ToSelectListItems(questionCard.ClassId);
            questionCard.CourseList = Mapper.Map<IEnumerable<CourseList>>(_courseService.GetCourseByClass(questionCard.ClassId)).ToSelectListItems(questionCard.CourseContentLineNo);
            questionCard.CourseContentList = Mapper.Map<IEnumerable<CourseContentLine>>(_courseContentService.FindAll(questionCard.CourseId)).ToSelectListItems(questionCard.CourseContentLineNo);
            questionCard.FileResources = Mapper.Map<IEnumerable<VM_FileResource>>(_fileResourceService.FindAll(ResultType.Sample, id));
            if (questionCard.FileResources.Count() == 0)
            {
                var _fileResources = new List<VM_FileResource>();
                var _fileResource = new VM_FileResource();
                _fileResource.LineNo = 0;
                _fileResources.Add(_fileResource);
                questionCard.FileResources = _fileResources;
            }
            return View(questionCard);
        }

        [HttpPost]
        public ActionResult Edit(Question_Card _questionCard)
        {
            try
            {
                SetData(_questionCard, false);
                hasValue = _questionService.Update(mQuestion);
                hasValue = _resultEntryService.Delete(ResultType.Sample, mQuestion.Id);
                hasValue = _resultEntryService.Insert(GetResultEntry(mQuestion));
                hasValue = _fileResourceService.Update(mQuestion.FileResources, ResultType.Sample, mQuestion.Id);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = mQuestion.Id, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        public ActionResult Deletes(string id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                int[] _codes = id.Split(',').Select(item => int.Parse(item)).ToArray();
                hasValue = _questionService.Delete(_codes);
                hasValue = _resultEntryService.Delete(ResultType.Sample, _codes);
                hasValue = _fileResourceService.Delete(ResultType.Sample, _codes);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return Json(new { v = true, count = _codes.Count() });
                return Json("Xóa dữ liệu không thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }
        #region -- Json --
        public JsonResult LoadData(GridSettings grid)
        {
            var list = Mapper.Map<IEnumerable<Question_List>>(_questionService.FindAll(QuestionType.Sample)).AsQueryable();
            list = JqGrid.SetupGrid<Question_List>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region -- Functions --
        private void SetData(Question_Card _questionCard, bool isCreate)
        {
            questionCard = _questionCard;
            if (isCreate)
            {
                questionCard.Id = _questionService.GetID();
                questionCard.DateQuestion = DateTime.Now;
                questionCard.DateAnswer = DateTime.Now;
            }
            questionCard.Type = QuestionType.Sample;
            questionCard.OwnerName = (Mapper.Map<TeacherCard>(_techerService.GetBy(questionCard.OwnerId)) ?? new TeacherCard()).FullName;
            var classInfo = Mapper.Map<VM_Class>(_classService.GetBy(questionCard.ClassId)) ?? new VM_Class();
            questionCard.ClassCode = classInfo.Code;
            questionCard.ClassName = classInfo.Name;
            questionCard.LevelId = classInfo.LevelId;
            questionCard.LevelName = classInfo.LevelName;
            var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get(3, 9)) ?? new VM_OptionLine();
            questionCard.StatusCode = statusInfo.Code;
            questionCard.StatusName = statusInfo.Name;
            questionCard.CourseName = (Mapper.Map<CourseList>(_courseService.GetBy_2(_questionCard.CourseId))).Title;
            questionCard.CourseContentName = (Mapper.Map<CourseContentLine>(_courseContentService.GetBy(_questionCard.CourseId, questionCard.CourseContentLineNo))).Title;
            //-- Add Resource
            var _uploadHelper = new UploadHelper();
            var imageNames = new List<string>();
            int lineNo = 100;
            int lineNoLast = _questionCard.FileResources.Where(d => d.LineNo != 0).OrderByDescending(d => d.LineNo).Select(d => d.LineNo).FirstOrDefault();
            var _fileResources = new List<VM_FileResource>();
            questionCard.FileResources.ToList().ForEach(item => {
                if (item.LineNo == 0)
                {
                    lineNoLast += lineNo;
                    item.LineNo = lineNoLast;
                }
                item.ReferId = questionCard.Id;
                item.Type = ResultType.Sample;
                item.FileType = FileType.ImagePath;
                item.Source = Source.BackEnd;
                imageNames.Add(item.ImageName);
                _fileResources.Add(item);
            });
            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFileBase file = Request.Files[i];
                var namControl = Request.Files.AllKeys[i].ToString();
                _uploadHelper = new UploadHelper(); 
                _uploadHelper.PathSave = HttpContext.Server.MapPath(string.Format(ImagePath, questionCard.Id, ""));
                _uploadHelper.UploadFile(file);
                string index = Regex.Match(namControl, @"\d+").Value;
                if (!string.IsNullOrEmpty(_fileResources[int.Parse(index)].ImageName) && !string.IsNullOrEmpty(_uploadHelper.FileSave))
                    _uploadHelper.DeleteFileOnServer(_uploadHelper.PathSave, _fileResources[int.Parse(index)].ImageName);
                if (!string.IsNullOrEmpty(_uploadHelper.FileSave))
                {
                    _fileResources[int.Parse(index)].ImageName = _uploadHelper.FileSave;
                    _fileResources[int.Parse(index)].ImagePath = string.Format(ImagePath, questionCard.Id, _uploadHelper.FileSave);
                    //Resize image
                    var path = Server.MapPath(_fileResources[int.Parse(index)].ImagePath);
                    //WebImage img = new WebImage(path);
                    //if (img.Width > 1920)
                    //    img.Resize(1920, 1920);
                    //img.Save(path);

                    //ResizeSettings resizeSetting = new ResizeSettings
                    //{
                    //    Width = 1920,
                    //    Height = 1352,
                    //    Format = "jpg"
                    //};
                    //ImageBuilder.Current.Build(path, path, resizeSetting);
                    //Resize image +
                    imageNames.Add(_fileResources[int.Parse(index)].ImageName);
                }
            }
            var imageNameToDeletes = Mapper.Map<IEnumerable<VM_FileResource>>(_fileResourceService.GetImageNameToDeletes(ResultType.Sample, questionCard.Id, imageNames));
            
            foreach (var item in imageNameToDeletes)
            {
                _uploadHelper = new UploadHelper();
                _uploadHelper.DeleteFileOnServer(HttpContext.Server.MapPath(string.Format(ImagePath, questionCard.Id, "")), item.ImageName);
            }
            questionCard.FileResources = _fileResources.Where(d => d.ImageName != null);
            //-- Add Resource End
            mQuestion = Mapper.Map<M_Question>(questionCard);
        }
        
        private M_ResultEntry GetResultEntry(M_Question _questionCard)
        {
            var _resultEntry = new VM_ResultEntry();
            _resultEntry.Type = ResultType.Sample;
            _resultEntry.ReferId = _questionCard.Id;
            _resultEntry.Question = _questionCard.Question;
            _resultEntry.Answer = _questionCard.Answer;
            _resultEntry.QuestionUnsigned = "";
            _resultEntry.AnswerUnsigned = "";
            _resultEntry.QuestionDate = _questionCard.DateQuestion;
            _resultEntry.AnswerDate = _questionCard.DateAnswer;
            _resultEntry.Questioner_Id = _questionCard.QuestionerId;
            _resultEntry.Questioner_FB_Link = _questionCard.QuestionerFB;
            _resultEntry.Questioner_FB_Name = _questionCard.QuestionerName;
            _resultEntry.Supporter_Id = _questionCard.OwnerId;
            _resultEntry.Supporter_Name = _questionCard.OwnerName;
            _resultEntry.Parent_Id = _questionCard.ParentsId;
            _resultEntry.parent_Name = _questionCard.ParentsName;
            _resultEntry.Level_Id = _questionCard.LevelId;
            _resultEntry.Level_Name = _questionCard.LevelName;
            _resultEntry.Class_Id = _questionCard.ClassId;
            _resultEntry.Class_Name = _questionCard.ClassName;
            _resultEntry.Course_Id = _questionCard.CourseId;
            _resultEntry.Course_Name = _questionCard.CourseName;
            _resultEntry.Method_Id = _questionCard.MethodId;
            _resultEntry.method_Name = _questionCard.MethodName;
            _resultEntry.Group_Id = _questionCard.GroupId;
            _resultEntry.GroupName = _questionCard.GroupName;
            _resultEntry.Status_Id = _questionCard.StatusId;
            _resultEntry.Status_Name = _questionCard.StatusName;
            return Mapper.Map<M_ResultEntry>(_resultEntry);
        }
        #endregion
    }
}
