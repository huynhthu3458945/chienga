﻿using System.Web.Mvc;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using System.Net;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using GCSOFT.MVC.Model.MasterData;

namespace GCSOFT.MVC.Web.Areas.MasterData.Controllers
{
	[CompressResponseAttribute]
    public class OptionTypeController : BaseController
    {
		#region -- Properties --
		private readonly IVuViecService _vuViecService;
		private readonly IOptionTypeService _optiontypeService;
        private readonly IOptionLineService _optionLineService;
        private string sysCategory = "OPTIONTYPE";
        private bool? permission = false;
        private VM_OptionType vm_OptionType;
        private M_OptionType m_OptionType;
        private string hasValue;
		#endregion
		
		#region -- Contructor --
		public OptionTypeController
            (
				IVuViecService vuViecService
                , IOptionTypeService optiontypeService
                , IOptionLineService optionLineService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _optiontypeService = optiontypeService;
            _optionLineService = optionLineService;
        }
		#endregion

        public ActionResult Index()
        {
			#region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }

        public ActionResult Details(int? id)
        {
			if (id == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.ViewDetail);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            vm_OptionType = Mapper.Map<VM_OptionType>(_optiontypeService.GetBy(id ?? 0));
            if (vm_OptionType == null)
                return HttpNotFound();
            return View(vm_OptionType);
        }

        public ActionResult Create()
        {
			#region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            vm_OptionType = new VM_OptionType();
            vm_OptionType.ID = _optiontypeService.GetID();
            return View(vm_OptionType);
        }

        [HttpPost]
        public ActionResult Create(VM_OptionType _vm_OptionType)
        {
			try
            {
                SetData(_vm_OptionType, true);
                hasValue = _optiontypeService.Insert(m_OptionType);
				hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = m_OptionType.ID, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        public ActionResult Edit(int? id)
        {
			if (id == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
			vm_OptionType = Mapper.Map<VM_OptionType>(_optiontypeService.GetBy(id ?? 0));
            if (vm_OptionType == null) 
				return HttpNotFound();
            return View(vm_OptionType);
        }

        [HttpPost]
        public ActionResult Edit(VM_OptionType _vm_OptionType)
        {
			try
            {
                SetData(_vm_OptionType, false);
                hasValue = _optiontypeService.Update(m_OptionType);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = m_OptionType.ID, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        public ActionResult Delete(string id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                int[] _codes = id.Split(',').Select(item => int.Parse(item)).ToArray();
                hasValue = _optiontypeService.Delete(_codes);
				hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return Json(new { v = true, count = _codes.Count() });
                return Json("Xóa dữ liệu không thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }
		#region -- Json --
		public JsonResult LoadData(GridSettings grid)
        {
            var list = Mapper.Map<IEnumerable<VM_OptionType>>(_optiontypeService.FindAll()).AsQueryable();
            list = JqGrid.SetupGrid<VM_OptionType>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
		#endregion
		#region -- Functions --
		private void SetData(VM_OptionType _vm_OptionType, bool isCreate)
        {
            vm_OptionType = _vm_OptionType;
            if (isCreate)
                vm_OptionType.ID = _optiontypeService.GetID();
            int _lineNo = _optionLineService.GetID() - 1;
            List<VM_OptionLine> _optionLines = new List<VM_OptionLine>();
            vm_OptionType.Optionlines.ToList().ForEach(item => {
                if (item.LineNo == 0)
                {
                    _lineNo += 1;
                    item.LineNo = _lineNo;
                }
                item.OptionHeaderCode = vm_OptionType.Code;
                item.OptionHeaderID = vm_OptionType.ID;
                item.OptionHeaderName = vm_OptionType.Name;
                _optionLines.Add(item);
            });
            vm_OptionType.Optionlines = _optionLines;
            m_OptionType = Mapper.Map<M_OptionType>(vm_OptionType);
        }
		#endregion
    }
}