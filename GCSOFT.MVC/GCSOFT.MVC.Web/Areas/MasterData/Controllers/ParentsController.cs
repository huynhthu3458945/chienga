﻿using System.Web.Mvc;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using GCSOFT.MVC.Model.MasterData;
using System.IO;
using GCSOFT.MVC.Web.Areas.MasterData.Models;
using GCSOFT.MVC.Data.Common;
using System.Text;

namespace GCSOFT.MVC.Web.Areas.MasterData.Controllers
{
	[CompressResponseAttribute]
    public class ParentsController : BaseController
    {
		#region -- Properties --
		private readonly IVuViecService _vuViecService;
		private readonly IParentsService _parentsService;
        private readonly IContactService _contactService;
        private readonly IChildrenService _childrenService;
        private readonly IClassService _classService;
        private string sysCategory = "PARENTS";
        private bool? permission = false;
        private Parent_Card parentsCard;
        private M_Parents mParents;
        private VM_Class vmClass;
        private string hasValue;
		#endregion
		
		#region -- Contructor --
		public ParentsController
            (
				IVuViecService vuViecService
                , IParentsService parentsService
                , IContactService contactService
                , IChildrenService childrenService
                , IClassService classService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _parentsService = parentsService;
            _contactService = contactService;
            _childrenService = childrenService;
            _classService = classService;
        }
		#endregion

        public ActionResult Index()
        {
			#region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }
       
        public ActionResult Details(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.ViewDetail);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            parentsCard = Mapper.Map<Parent_Card>(_parentsService.GetBy(id));
            if (parentsCard == null)
                return HttpNotFound();
            return View(parentsCard);
        }

        public ActionResult Create()
        {
			#region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            parentsCard = new Parent_Card();
            foreach (var item in parentsCard.Childrens)
            {
                item.ClassList = Mapper.Map<IEnumerable<VM_Class>>(_classService.FindAll()).ToList().ToSelectListItems(0);
            }
            ViewBag.ClassListStr = ClassListStr();
            return View(parentsCard);
        }

        [HttpPost]
        public ActionResult Create(Parent_Card _parentsCard)
        {
			try
            {
                SetData(_parentsCard, true);
                hasValue = _parentsService.Insert(mParents);
				hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = _parentsCard.Id, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        public ActionResult Edit(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
			parentsCard = Mapper.Map<Parent_Card>(_parentsService.GetBy(id));
            if (parentsCard == null) 
				return HttpNotFound();
            foreach (var item in parentsCard.Childrens)
            {
                item.ClassList = Mapper.Map<IEnumerable<VM_Class>>(_classService.FindAll()).ToList().ToSelectListItems(item.Class_Id);
            }
            ViewBag.ClassListStr = ClassListStr();
            return View(parentsCard);
        }

        [HttpPost]
        public ActionResult Edit(Parent_Card _parentsCard)
        {
			try
            {
                SetData(_parentsCard, false);
                hasValue = _parentsService.Update(mParents);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = mParents.Id, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        public ActionResult Deletes(string id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                int[] _codes = id.Split(',').Select(item => int.Parse(item)).ToArray();
                hasValue = _parentsService.Delete(_codes);
				hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return Json(new { v = true, count = _codes.Count() });
                return Json("Xóa dữ liệu không thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }
        #region -- Json --
        public JsonResult LoadData(GridSettings grid)
        {
            var list = Mapper.Map<IEnumerable<Parent_List>>(_parentsService.FindAll()).AsQueryable();
            list = JqGrid.SetupGrid<Parent_List>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region -- Functions --
        private void SetData(Parent_Card _parentsCard, bool isCreate)
        {
            parentsCard = _parentsCard;
            if (isCreate)
                parentsCard.Id = _parentsService.GetID();
            //-- Add Contact
            var _contacts = new List<VM_Contact>();
            var idLine = _contactService.GetID();
            parentsCard.Contacts.ToList().ForEach(item => {
                item.Id = item.Id == 0 ? idLine += 1 : item.Id;
                item.ReferId = parentsCard.Id;
                item.Type = 0;
                item.PhoneSystem = StringUtil.RebuildPhoneNo(item.Phone);
                _contacts.Add(item);
            });
            parentsCard.Contacts = _contacts;
            //-- Add Contact +
            //-- Add Childrens
            idLine = _childrenService.GetID();
            var _childrens = new List<VM_Childrens>();
            parentsCard.Childrens.ToList().ForEach(item => {
                item.Id = item.Id == 0 ? idLine += 1 : item.Id;
                item.ParentsId = parentsCard.Id;
                item.FullName = string.Format("{0} {1}", item.LastName, item.FirstName);
                vmClass = Mapper.Map<VM_Class>(_classService.GetBy(item.Class_Id));
                item.Class_Code = vmClass.Code;
                item.Class_Name = vmClass.Name;
                _childrens.Add(item);
            });
            parentsCard.Childrens = _childrens;
            //-- Add Childrens +
            mParents = Mapper.Map<M_Parents>(parentsCard);
        }
        private string ClassListStr()
        {
            var ddlClassStr = new StringBuilder();
            ddlClassStr.Append("<select id=\"Childrens_{{stt}}__Class_Id\" name=\"Childrens[{{stt}}].Class_Id\" class=\"md-input label-fixed\" accesskey=\"ddkChildrensKeyNewLine\">");
            ddlClassStr.Append("<option value=\"\">[ Chọn ]</option>");
            var classList = Mapper.Map<IEnumerable<VM_Class>>(_classService.FindAll()).ToList();
            foreach (var item in classList)
            {
                ddlClassStr.Append(string.Format("<option value=\"{0}\">{1}</option>", item.Id, item.Name));
            }
            ddlClassStr.Append("</select>");
            return ddlClassStr.ToString();
        }
		#endregion
    }
}
