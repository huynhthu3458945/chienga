﻿using System.Web.Mvc;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using System.Net;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Web.Areas.MasterData.Models;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using GCSOFT.MVC.Data.Common;
using System.Web;
using System.IO;
using System.Data.OleDb;
using System.Data;
using LinqToExcel;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using System.Text;

namespace GCSOFT.MVC.Web.Areas.MasterData.Controllers
{
	[CompressResponseAttribute]
    public class QuestionController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IQuestionService _questionService;
        private readonly ITeacherService _techerService;
        private readonly IParentsService _parentsService;
        private readonly IOptionLineService _optionLineService;
        private readonly IClassService _classService;
        private readonly IResultEntryService _resultEntryService;
        private readonly IUserService _userService;
        private readonly IUserInfoService _userInfoService;
        private readonly IMailSetupService _mailSetupService;
        private readonly ITemplateService _templateService;
        private string sysCategory = "QUESTION";
        private bool? permission = false;
        private Question_Card questionCard;
        private M_Question mQuestion;
        private string hasValue;
        #endregion

        #region -- Contructor --
        public QuestionController
            (
                IVuViecService vuViecService
                , IQuestionService questionService
                , ITeacherService techerService
                , IParentsService parentsService
                , IClassService classService
                , IOptionLineService optionLineService
                , IResultEntryService resultEntryService
                , IUserService userService
                , IUserInfoService userInfoService
                , IMailSetupService mailSetupService
                , ITemplateService templateService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _questionService = questionService;
            _techerService = techerService;
            _parentsService = parentsService;
            _classService = classService;
            _optionLineService = optionLineService;
            _resultEntryService = resultEntryService;
            _userService = userService;
            _userInfoService = userInfoService;
            _mailSetupService = mailSetupService;
            _templateService = templateService;
        }
        #endregion

        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var search = new QuestionFilter()
            {
                Status = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("TT")).ToSelectListItems("CTL")
            };
            return View(search);
        }
        public ActionResult IndexPopup()
        {
            return View("_IndexPopup");
        }
        public ActionResult Details(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.ViewDetail);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            questionCard = Mapper.Map<Question_Card>(_questionService.GetBy(id));
            if (questionCard == null)
                return HttpNotFound();
            return View(questionCard);
        }

        public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            questionCard = new Question_Card();
            questionCard.OwnerList = Mapper.Map<IEnumerable<UserVM>>(_userService.FindAll("SOCIAL")).ToSelectListItems(0);
            questionCard.ParentsList = Mapper.Map<IEnumerable<Parent_List>>(_parentsService.FindAll()).ToSelectListItems(0);
            questionCard.GroupList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll(1)).ToSelectListItems(0);
            questionCard.ClassList = Mapper.Map<IEnumerable<VM_Class>>(_classService.FindAll()).ToList().ToSelectListItems(0);
            questionCard.MethodList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll(2)).ToSelectListItems(0);
            questionCard.StatusList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("TT")).ToSelectListItems(0);
            return View(questionCard);
        }

        [HttpPost]
        public ActionResult Create(Question_Card _questionCard)
        {
            try
            {
                SetData(_questionCard, true);
                hasValue = _questionService.Insert(mQuestion);
                if (mQuestion.StatusCode.Equals("DTL") || mQuestion.StatusCode.Equals("DX"))
                {
                    hasValue = _resultEntryService.Delete(ResultType.Question, mQuestion.Id);
                    hasValue = _resultEntryService.Insert(GetResultEntry(mQuestion));
                }
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = mQuestion.Id, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        public ActionResult Edit(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            questionCard = Mapper.Map<Question_Card>(_questionService.GetBy(id));
            if (questionCard == null)
                return HttpNotFound();
            questionCard.DateQuestionStr = string.Format("{0:dd/MM/yyyy}", questionCard.DateQuestion);
            questionCard.DateAnswerStr = string.Format("{0:dd/MM/yyyy}", questionCard.DateAnswer);
            questionCard.OwnerList = Mapper.Map<IEnumerable<UserVM>>(_userService.FindAll("SOCIAL")).ToSelectListItems(questionCard.OwnerId);
            questionCard.ParentsList = Mapper.Map<IEnumerable<Parent_List>>(_parentsService.FindAll()).ToSelectListItems(questionCard.ParentsId);
            questionCard.GroupList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll(1)).ToSelectListItems(questionCard.GroupId);
            questionCard.ClassList = Mapper.Map<IEnumerable<VM_Class>>(_classService.FindAll()).ToList().ToSelectListItems(questionCard.ClassId);
            questionCard.MethodList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll(2)).ToSelectListItems(questionCard.MethodId);
            questionCard.StatusList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("TT")).ToSelectListItems(questionCard.StatusId);
            return View(questionCard);
        }
        public ActionResult ImportSocial()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.ExportExcel);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }

        [HttpPost]
        public ActionResult Edit(Question_Card _questionCard)
        {
            try
            {
                SetData(_questionCard, false);
                hasValue = _questionService.Update(mQuestion);
                if (mQuestion.StatusCode.Equals("DTL") || mQuestion.StatusCode.Equals("DX"))
                {
                    hasValue = _resultEntryService.Delete(ResultType.Question, mQuestion.Id);
                    hasValue = _resultEntryService.Insert(GetResultEntry(mQuestion));
                }
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                {
                    if (mQuestion.StatusCode.Equals("DDTL") || mQuestion.StatusCode.Equals("DTL"))
                        SendQuestionInfo(questionCard);
                    return RedirectToAction("Edit", new { id = mQuestion.Id, msg = "1" });
                }
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        public ActionResult Deletes(string id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                int[] _codes = id.Split(',').Select(item => int.Parse(item)).ToArray();
                hasValue = _questionService.Delete(_codes);
                hasValue = _resultEntryService.Delete(ResultType.Question, _codes);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return Json(new { v = true, count = _codes.Count() });
                return Json("Xóa dữ liệu không thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }
        #region -- Json --
        public JsonResult LoadData(GridSettings grid, QuestionFilter questionFilter)
        {
            var _questionFilter = Mapper.Map<M_QuestionFilter>(questionFilter);
            var list = Mapper.Map<IEnumerable<Question_List>>(_questionService.Socials(QuestionType.Question, _questionFilter)).AsQueryable();
            list = JqGrid.SetupGrid<Question_List>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region -- Functions --
        private void SetData(Question_Card _questionCard, bool isCreate)
        {
            questionCard = _questionCard;
            if (isCreate)
                questionCard.Id = _questionService.GetID();
            if (!string.IsNullOrEmpty(questionCard.DateQuestionStr))
                questionCard.DateQuestion = DateTime.Parse(StringUtil.ConvertStringToDate(questionCard.DateQuestionStr));
            if (!string.IsNullOrEmpty(questionCard.DateAnswerStr))
                questionCard.DateAnswer = DateTime.Parse(StringUtil.ConvertStringToDate(questionCard.DateAnswerStr));
            else
                questionCard.DateAnswer = null;
            questionCard.Type = QuestionType.Question;
            questionCard.OwnerName = (Mapper.Map<UserVM>(_userService.GetBy(questionCard.OwnerId)) ?? new UserVM()).FullName;
            questionCard.ParentsName = (Mapper.Map<Parent_Card>(_parentsService.GetBy(questionCard.ParentsId)) ?? new Parent_Card()).FullName;
            questionCard.GroupName = (Mapper.Map<VM_OptionLine>(_optionLineService.Get(1, questionCard.GroupId)) ?? new VM_OptionLine()).Name;
            var classInfo = Mapper.Map<VM_Class>(_classService.GetBy(questionCard.ClassId)) ?? new VM_Class();
            questionCard.ClassCode = classInfo.Code;
            questionCard.ClassName = classInfo.Name;
            var methodInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get(2, questionCard.MethodId)) ?? new VM_OptionLine();
            questionCard.MethodCode = methodInfo.Code;
            questionCard.MethodName = methodInfo.Name;
            var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get(3, questionCard.StatusId)) ?? new VM_OptionLine();
            questionCard.StatusCode = statusInfo.Code;
            questionCard.StatusName = statusInfo.Name;
            mQuestion = Mapper.Map<M_Question>(questionCard);
        }
        private M_ResultEntry GetResultEntry(M_Question _questionCard)
        {
            var _resultEntry = new VM_ResultEntry();
            _resultEntry.Type = ResultType.Question;
            _resultEntry.ReferId = _questionCard.Id;
            _resultEntry.Question = _questionCard.Question;
            _resultEntry.Answer = _questionCard.Answer;
            _resultEntry.QuestionUnsigned = "";
            _resultEntry.AnswerUnsigned = "";
            _resultEntry.QuestionDate = _questionCard.DateQuestion;
            _resultEntry.AnswerDate = _questionCard.DateAnswer;
            _resultEntry.Questioner_Id = _questionCard.QuestionerId;
            _resultEntry.Questioner_FB_Link = _questionCard.QuestionerFB;
            _resultEntry.Questioner_FB_Name = _questionCard.QuestionerName;
            _resultEntry.Supporter_Id = _questionCard.OwnerId;
            _resultEntry.Supporter_Name = _questionCard.OwnerName;
            _resultEntry.Parent_Id = _questionCard.ParentsId;
            _resultEntry.parent_Name = _questionCard.ParentsName;
            _resultEntry.Level_Id = null;
            _resultEntry.Level_Name = null;
            _resultEntry.Class_Id = _questionCard.ClassId;
            _resultEntry.Class_Name = _questionCard.ClassName;
            _resultEntry.Course_Id = null;
            _resultEntry.Course_Name = null;
            _resultEntry.Method_Id = _questionCard.MethodId;
            _resultEntry.method_Name = _questionCard.MethodName;
            _resultEntry.Group_Id = _questionCard.GroupId;
            _resultEntry.GroupName = _questionCard.GroupName;
            _resultEntry.Status_Id = _questionCard.StatusId;
            _resultEntry.Status_Name = _questionCard.StatusName;
            return Mapper.Map<M_ResultEntry>(_resultEntry);
        }
        [HttpPost]
        #endregion
        public ActionResult Import(HttpPostedFileBase FileUpload)
        {
            try
            {
                XoaAllFile();
                if (FileUpload.ContentType == "application/vnd.ms-excel" || FileUpload.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                {
                    string filename = FileUpload.FileName;
                    string targetpath = Server.MapPath("~/Files/ImportSocial/");
                    if (!Directory.Exists(targetpath))
                        Directory.CreateDirectory(targetpath);
                    FileUpload.SaveAs(targetpath + filename);
                    string pathToExcelFile = targetpath + filename;
                    var connectionString = "";
                    if (filename.EndsWith(".xls"))
                    {
                        connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=Excel 8.0;", pathToExcelFile);
                    }
                    else if (filename.EndsWith(".xlsx"))
                    {
                        connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";", pathToExcelFile);
                    }
                    var adapter = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", connectionString);
                    var ds = new DataSet();
                    adapter.Fill(ds, "ExcelTable");
                    DataTable dtable = ds.Tables["ExcelTable"];
                    string sheetName = "Sheet1";
                    var excelFile = new ExcelQueryFactory(pathToExcelFile);
                    var customers = from a in excelFile.Worksheet<QuestionExcel>(sheetName) select a;
                    var methodInfo = new VM_OptionLine();
                    foreach (var item in customers)
                    {
                        if (!string.IsNullOrEmpty(item.Status) && !string.IsNullOrEmpty(item.Anser))
                        {
                            questionCard = Mapper.Map<Question_Card>(_questionService.GetBy2(item.ID));
                            if (questionCard.Id != 0)
                            {
                                questionCard.DateAnswer = DateTime.Now;
                                questionCard.Answer = item.Anser;
                                if (item.Status.Equals("1"))
                                    methodInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("TT", "DDTL")) ?? new VM_OptionLine();
                                else
                                    methodInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("TT", "DTL")) ?? new VM_OptionLine();
                                questionCard.StatusId = methodInfo.LineNo;
                                questionCard.StatusCode = methodInfo.Code;
                                questionCard.StatusName = methodInfo.Name;
                                hasValue = _questionService.Update(Mapper.Map<M_Question>(questionCard));
                                hasValue = _vuViecService.Commit();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.ToString();
            }
            
            return View("ImportSocial");
        }
        private string XoaAllFile()
        {
            try
            {
                string[] filePaths = Directory.GetFiles(HttpContext.Server.MapPath("~/Files/ImportSocial/"));
                foreach (string filePath in filePaths)
                    System.IO.File.Delete(filePath);
                return null;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        private string SendQuestionInfo(Question_Card _questionCard)
        {
            try
            {
                var userInfo = Mapper.Map<VM_UserInfo>(_userInfoService.GetBy(_questionCard.userName));
                var mailTemplate = Mapper.Map<Template>(_templateService.Get("SOCIAL"));
                //-- Send Email
                var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
                var mailTitle = mailTemplate.Title;
                var mailContent = mailTemplate.Content.Replace("{{FULLNAME}}", _questionCard.fullName);
                mailContent = mailContent.Replace("{{STATUSNAME}}", _questionCard.StatusName);
                mailContent = mailContent.Replace("{{QUESTION}}", _questionCard.Question);
                mailContent = mailContent.Replace("{{ANSER}}", _questionCard.Answer);
                var emailTos = new List<string>();
                emailTos.Add(userInfo.Email);
                var _mailHelper = new EmailHelper()
                {
                    EmailTos = emailTos,
                    DisplayName = emailInfo.DisplayName,
                    Title = mailTitle,
                    Body = mailContent,
                    MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
                };
                hasValue = _mailHelper.DoSendMail();
                //-- Send Email +
                //-- Send SMS
                var smsHelper = new SMSHelper()
                {
                    PhoneNo = userInfo.Phone,
                    Content = mailTemplate.SMSContent
                };
                hasValue = smsHelper.Send();
                //-- Send SMS +
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
    }
    public class QuestionExcel
    {
        public int STT { get; set; }
        public int ID { get; set; }
        public string Question { get; set; }
        public string Status { get; set; }
        public string Anser { get; set; }
    }
}
