﻿using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Service.StoreProcedure.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.MasterData.Controllers
{
    [CompressResponseAttribute]
    public class SearchController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IStoreProcedureService _storeProcedureService;
        private string sysCategory = "SERCH";
        private bool? permission = false;
        #endregion

        #region -- Contructor --
        public SearchController
            (
                IVuViecService vuViecService
                , IStoreProcedureService storeProcedureService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _storeProcedureService = storeProcedureService;
        }
        #endregion
        public ActionResult CustomerInfo()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }
        public ActionResult UserInfo()
        {
            #region -- Role user --
            permission = GetPermission("ASE_USERINFO", Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }
        public PartialViewResult CustomerInfoResult(ParamCustomer pc)
        {
            string CardNo2 = "";
            if (!string.IsNullOrEmpty(pc.CardNo))
                CardNo2 = CryptorEngine.Encrypt(pc.CardNo, true, "TTLSTNHD@CARD");
            var userInfos = _storeProcedureService.SP_CustomerInfo(CardNo2 ?? "", pc.PhoneNo ?? "", pc.Email ?? "", pc.FullName ?? "", pc.SerialNumber ?? "",pc.UserName ?? "", pc.DurationCode ?? "");
            foreach (var item in userInfos)
            {
                item.CardNo2 = CryptorEngine.Decrypt(item.CardNo, true, "TTLSTNHD@CARD");
            }
            return PartialView("_CustomerInfo", userInfos);
        }
        public PartialViewResult UserInfoResult(ParamCustomer pc)
        {
            string CardNo2 = "";
            if (!string.IsNullOrEmpty(pc.CardNo))
                CardNo2 = CryptorEngine.Encrypt(pc.CardNo, true, "TTLSTNHD@CARD");
            var userInfos = _storeProcedureService.SP_UserInfo(pc.UserName ?? "", pc.FullName ?? "", pc.Email ?? "", pc.PhoneNo ?? "", pc.SerialNumber ?? "", CardNo2 ?? "");
            var userAdmins = new List<string>();
            userAdmins.Add("admin");
            userAdmins.Add("thanhtuyen");
            userAdmins.Add("mydung");
            ViewBag.isSalesAdmin = userAdmins.Contains(GetUser().Username) ? true : false;
            return PartialView("_UserInfo", userInfos);
        }
    }
    public class ParamCustomer
    {
        public string CardNo { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public string SerialNumber { get; set; }
        public string UserName { get; set; }
        public string DurationCode { get; set; }
    }
}