﻿using System.Web.Mvc;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using System.Net;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Web.Areas.MasterData.Models;
using GCSOFT.MVC.Model.MasterData;
using System.IO;
using System.Runtime.InteropServices.ComTypes;
using System.Collections;

namespace GCSOFT.MVC.Web.Areas.MasterData.Controllers
{
	[CompressResponseAttribute]
    public class SuggestionsController : BaseController
    {
        #region -- Properties --
        private readonly IQuestionService _questionService;
        private readonly IFileResourceService _fileResourceService;
        private readonly IVuViecService _vuViecService;
        private readonly ISetupService _setupService;
        private readonly IResultEntryService _resultEntryService;
        private readonly ICommentService _commentService;
        private string sysCategory = "SUGGEST";
        private bool? permission = false;
        private Suggestions suggestions;
        private M_Question mQuestion;
        private string hasValue;
        private string ImagePath = "~/Files/Sample/{0}/{1}";
        #endregion

        #region -- Contructor --
        public SuggestionsController
            (
                IVuViecService vuViecService
                , IQuestionService questionService
                , IFileResourceService fileResourceService
                , ISetupService setupService
                , IResultEntryService resultEntryService
                , ICommentService commentService
            ) : base(vuViecService)
        {
            _questionService = questionService;
            _fileResourceService = fileResourceService;
            _vuViecService = vuViecService;
            _setupService = setupService;
            _resultEntryService = resultEntryService;
            _commentService = commentService;
        }
        #endregion

        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }
        /// <summary>
        /// Get Info
        /// </summary>
        /// <param name="id">Id Comment</param>
        /// <returns></returns>
        public ActionResult Details(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.ViewDetail);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var commentInfo = Mapper.Map<VM_Comment>(_commentService.GetBy(id));

            suggestions = Mapper.Map<Suggestions>(_questionService.GetBy(id));
            if (suggestions == null)
                return HttpNotFound();
            suggestions.CommentId = commentInfo.Id;
            suggestions.Answer = commentInfo.Content ?? string.Empty;
            suggestions.fullName = commentInfo.FullName;
            suggestions.StatusName = commentInfo.StatusName;
            suggestions.DateQuestionStr = string.Format("{0:dd/MM/yyyy}", commentInfo.DateCreate);
            suggestions.FileResources = Mapper.Map<IEnumerable<VM_FileResource>>(_fileResourceService.FindAllByComment(ResultType.Commment, id));
            suggestions.StatusCode = commentInfo.StatusCode;
            suggestions.StatusId = commentInfo.StatusId;
            suggestions.StatusName = commentInfo.StatusName;
            var setup = _setupService.Get();
            foreach (var item in suggestions.FileResources)
            {
                item.Domain = item.Source == Source.BackEnd ? setup.DomainBackEnd : setup.DomainFontEnd;
                item.ImagePathFull = item.ImagePath.Replace("~", item.Domain);
            }
            return View(suggestions);
        }
        public ActionResult Deletes(string id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                int[] _codes = id.Split(',').Select(item => int.Parse(item)).ToArray();
                hasValue = _commentService.Delete(_codes);
                hasValue = _fileResourceService.Delete(ResultType.Sample, _codes);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return Json(new { v = true, count = _codes.Count() });
                return Json("Xóa dữ liệu không thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }
        #region -- Json --
        public JsonResult LoadData(GridSettings grid)
        {
            var Comments = _commentService.FindAll(ResultType.Sample);
            var idRefers = Comments.Select(d => d.ReferId).Distinct().ToArray();
            var questions = _questionService.FindAll(QuestionType.Sample, idRefers);
            var sql = from c in Comments
                      join q in questions on c.ReferId equals q.Id into sr
                      from x in sr.DefaultIfEmpty()
                      select new Question_List
                      {
                          Id = x.Id,
                          Question = c.Content,
                          DateQuestion = c.DateCreate,
                          StatusName = c.StatusName,
                          fullName = c.FullName,
                          ClassName = x.ClassName,
                          CourseName = x.CourseName,
                          CourseContentName = x.CourseContentName,
                          IdComment = c.Id
                      };
            var list = Mapper.Map<IEnumerable<Question_List>>(sql).AsQueryable();
            list = JqGrid.SetupGrid<Question_List>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Cập Nhật Status
        /// </summary>
        /// <param name="id">Id Comment</param>
        /// <param name="s">Status Code</param>
        /// <returns></returns>
        public JsonResult UpdateStatus(int id, string s)
        {
            try
            {
                var _fileResource = Mapper.Map<IEnumerable<VM_FileResource>>(_fileResourceService.FindAllByComment(ResultType.Commment, id));

                var commentInfo = Mapper.Map<VM_Comment>(_commentService.GetBy(id));

                var _question = Mapper.Map<Question_Card>(_questionService.GetBy(commentInfo.ReferId));
                var statusInfo = OptionLine(3, s);
                _question.Id = _questionService.GetID();
                _question.Type = QuestionType.Suggestions;
                _question.StatusId = statusInfo.LineNo;
                _question.StatusCode = statusInfo.Code;
                _question.StatusName = statusInfo.Name;
                _question.Answer = commentInfo.Content;
                _question.CommentId = commentInfo.Id;
                _question.DateQuestion = commentInfo.DateCreate;
                _question.DateAnswer = DateTime.Now;
                _question.userName = commentInfo.UserName;
                _question.fullName = commentInfo.FullName;

                commentInfo.StatusId = statusInfo.LineNo;
                commentInfo.StatusCode = statusInfo.Code;
                commentInfo.StatusName = statusInfo.Name;

                foreach (var item in _fileResource)
                {
                    item.Type = ResultType.Suggestions;
                    item.ReferId = _question.Id;
                }

                var question = Mapper.Map<M_Question>(_question);
                hasValue = _questionService.Insert(question);
                hasValue = _commentService.Update(Mapper.Map<M_Comment>(commentInfo));
                hasValue = _fileResourceService.Insert(Mapper.Map<IEnumerable<M_FileResource>>(_fileResource));

                if (_question.StatusCode.Equals("DX"))
                    hasValue = _resultEntryService.Insert(GetResultEntry(question));
                hasValue = _vuViecService.Commit();
                return Json(new { code = string.IsNullOrEmpty(hasValue) ? "oke" : "error", name = string.IsNullOrEmpty(hasValue) ? question.StatusName : hasValue }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex) { return Json(new { code = "error", name = ex.ToString() }, JsonRequestBehavior.AllowGet); ; }
        }
        private M_ResultEntry GetResultEntry(M_Question _questionCard)
        {
            var _resultEntry = new VM_ResultEntry();
            _resultEntry.Type = ResultType.Suggestions;
            _resultEntry.ReferId = _questionCard.Id;
            _resultEntry.Question = _questionCard.Question;
            _resultEntry.Answer = _questionCard.Answer;
            _resultEntry.QuestionUnsigned = "";
            _resultEntry.AnswerUnsigned = "";
            _resultEntry.QuestionDate = _questionCard.DateQuestion;
            _resultEntry.AnswerDate = _questionCard.DateAnswer;
            _resultEntry.Questioner_Id = _questionCard.QuestionerId;
            _resultEntry.Questioner_FB_Link = _questionCard.QuestionerFB;
            _resultEntry.Questioner_FB_Name = _questionCard.QuestionerName;
            _resultEntry.Supporter_Id = _questionCard.OwnerId;
            _resultEntry.Supporter_Name = _questionCard.OwnerName;
            _resultEntry.Parent_Id = _questionCard.ParentsId;
            _resultEntry.parent_Name = _questionCard.ParentsName;
            _resultEntry.Level_Id = _questionCard.LevelId;
            _resultEntry.Level_Name = _questionCard.LevelName;
            _resultEntry.Class_Id = _questionCard.ClassId;
            _resultEntry.Class_Name = _questionCard.ClassName;
            _resultEntry.Course_Id = _questionCard.CourseId;
            _resultEntry.Course_Name = _questionCard.CourseName;
            _resultEntry.Method_Id = _questionCard.MethodId;
            _resultEntry.method_Name = _questionCard.MethodName;
            _resultEntry.Group_Id = _questionCard.GroupId;
            _resultEntry.GroupName = _questionCard.GroupName;
            _resultEntry.Status_Id = _questionCard.StatusId;
            _resultEntry.Status_Name = _questionCard.StatusName;
            _resultEntry.userName = _questionCard.userName;
            _resultEntry.fullName = _questionCard.fullName;
            return Mapper.Map<M_ResultEntry>(_resultEntry);
        }
        #endregion
    }
}
