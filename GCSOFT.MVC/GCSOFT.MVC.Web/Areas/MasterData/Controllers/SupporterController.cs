﻿using AutoMapper;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Areas.MasterData.Models;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.MasterData.Controllers
{
    [CompressResponseAttribute]
    public class SupporterController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly ISupporterService _supporterService;
        private readonly IFileResourceService _fileResourceService;
        private string sysCategory = "SUPPORTER";
        
        private bool? permission = false;
        private VM_Supporter vmSupporter;
        private M_Supporter supporter;
        private string hasValue;
        private string ImagePath = "~/Files/Supporter/{0}/{1}";
        #endregion

        #region -- Contructor --
        public SupporterController
            (
                IVuViecService vuViecService
                , ISupporterService supporterService
                , IFileResourceService fileResourceService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _supporterService = supporterService;
            _fileResourceService = fileResourceService;
        }
        #endregion
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }

        public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            vmSupporter = new VM_Supporter();
            return View(vmSupporter);
        }
        [HttpPost]
        public ActionResult Create(VM_Supporter _supporter)
        {
            try
            {
                SetData(_supporter, true);
                hasValue = _supporterService.Insert(supporter);
                hasValue = _fileResourceService.Insert(supporter.FileResources);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = supporter.Id, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult Edit(VM_Supporter _supporter)
        {
            try
            {
                SetData(_supporter, false);
                hasValue = _supporterService.Update(supporter);
                hasValue = _fileResourceService.Update(supporter.FileResources, ResultType.Supporter, supporter.Id);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = supporter.Id, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public ActionResult Edit(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            vmSupporter = Mapper.Map<VM_Supporter>(_supporterService.GetBy(id));
            if (vmSupporter == null)
                return HttpNotFound();
            vmSupporter.FileResources = Mapper.Map<IEnumerable<VM_FileResource>>(_fileResourceService.FindAll(ResultType.Supporter, id));
            if (vmSupporter.FileResources.Count() == 0)
            {
                var _fileResources = new List<VM_FileResource>();
                var _fileResource = new VM_FileResource();
                _fileResource.LineNo = 0;
                _fileResources.Add(_fileResource);
                vmSupporter.FileResources = _fileResources;
            }
            return View(vmSupporter);
        }
        public ActionResult Deletes(string id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                int[] _codes = id.Split(',').Select(item => int.Parse(item)).ToArray();
                hasValue = _supporterService.Delete(_codes);
                hasValue = _fileResourceService.Delete(ResultType.Supporter, _codes);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return Json(new { v = true, count = _codes.Count() });
                return Json("Xóa dữ liệu không thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }
        public JsonResult LoadData(GridSettings grid)
        {
            var list = Mapper.Map<IEnumerable<VM_Supporter>>(_supporterService.FindAll()).AsQueryable();
            list = JqGrid.SetupGrid<VM_Supporter>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
        private void SetData(VM_Supporter _supporter, bool isCreate)
        {
            vmSupporter = _supporter;
            if (isCreate)
                vmSupporter.Id = _supporterService.GetID();
            //-- Add Resource
            var _uploadHelper = new UploadHelper();
            var imageNames = new List<string>();
            int lineNo = 100;
            int lineNoLast = vmSupporter.FileResources.Where(d => d.LineNo != 0).OrderByDescending(d => d.LineNo).Select(d => d.LineNo).FirstOrDefault();
            var _fileResources = new List<VM_FileResource>();
            vmSupporter.FileResources.ToList().ForEach(item => {
                if (item.LineNo == 0)
                {
                    lineNoLast += lineNo;
                    item.LineNo = lineNoLast;
                }
                item.ReferId = vmSupporter.Id;
                item.Type = ResultType.Supporter;
                item.FileType = FileType.ImagePath;
                item.Source = Source.BackEnd;
                imageNames.Add(item.ImageName);
                _fileResources.Add(item);
            });
            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFileBase file = Request.Files[i];
                var namControl = Request.Files.AllKeys[i].ToString();
                _uploadHelper = new UploadHelper();
                _uploadHelper.PathSave = HttpContext.Server.MapPath(string.Format(ImagePath, vmSupporter.Id, ""));
                _uploadHelper.UploadFile(file);
                string index = Regex.Match(namControl, @"\d+").Value;
                if (!string.IsNullOrEmpty(_fileResources[int.Parse(index)].ImageName) && !string.IsNullOrEmpty(_uploadHelper.FileSave))
                    _uploadHelper.DeleteFileOnServer(_uploadHelper.PathSave, _fileResources[int.Parse(index)].ImageName);
                if (!string.IsNullOrEmpty(_uploadHelper.FileSave))
                {
                    _fileResources[int.Parse(index)].ImageName = _uploadHelper.FileSave;
                    _fileResources[int.Parse(index)].ImagePath = string.Format(ImagePath, vmSupporter.Id, _uploadHelper.FileSave);
                    imageNames.Add(_fileResources[int.Parse(index)].ImageName);
                    ImageHelper imgHelper = new ImageHelper()
                    {
                        pathImg = HttpContext.Server.MapPath(string.Format(ImagePath, vmSupporter.Id, _uploadHelper.FileSave))
                    };
                    _fileResources[int.Parse(index)].ImageBase64 = imgHelper.ImageToBase64();
                }
            }
            var imageNameToDeletes = Mapper.Map<IEnumerable<VM_FileResource>>(_fileResourceService.GetImageNameToDeletes(ResultType.Supporter, vmSupporter.Id, imageNames));

            foreach (var item in imageNameToDeletes)
            {
                _uploadHelper = new UploadHelper();
                _uploadHelper.DeleteFileOnServer(HttpContext.Server.MapPath(string.Format(ImagePath, vmSupporter.Id, "")), item.ImageName);
            }
            //vmSupporter.FileResources = _fileResources.Where(d => d.ImageName != null);
            //-- Add Resource End
            supporter = Mapper.Map<M_Supporter>(vmSupporter);
        }
    }
}