﻿using System.Web.Mvc;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using GCSOFT.MVC.Model.MasterData;
using System.IO;

namespace GCSOFT.MVC.Web.Areas.MasterData.Controllers
{
	[CompressResponseAttribute]
    public class TeacherController : BaseController
    {
		#region -- Properties --
		private readonly IVuViecService _vuViecService;
		private readonly ITeacherService _teacherService;
        private readonly IOptionLineService _optionLineService;
        private string sysCategory = "TEACHER";
        private bool? permission = false;
        private TeacherCard teacherCard;
        private E_Teacher e_Teacher;
        private string hasValue;
		#endregion
		
		#region -- Contructor --
		public TeacherController
            (
				IVuViecService vuViecService
                , ITeacherService teacherService
                , IOptionLineService optionLineService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _teacherService = teacherService;
            _optionLineService = optionLineService;
        }
		#endregion

        public ActionResult Index()
        {
			#region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }
        public ActionResult IndexPopup()
        {
            return View("_IndexPopup");
        }
        public ActionResult Details(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.ViewDetail);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            teacherCard = Mapper.Map<TeacherCard>(_teacherService.GetBy(id));
            if (teacherCard == null)
                return HttpNotFound();
            return View(teacherCard);
        }

        public ActionResult Create()
        {
			#region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            teacherCard = new TeacherCard();
            teacherCard.LevelList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("LEVEL")).ToSelectListItems("");
            return View(teacherCard);
        }

        [HttpPost]
        public ActionResult Create(TeacherCard _vm_Teacher)
        {
			try
            {
                SetData(_vm_Teacher, true);
                hasValue = _teacherService.Insert(e_Teacher);
				hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = e_Teacher.Id, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        public ActionResult Edit(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
			teacherCard = Mapper.Map<TeacherCard>(_teacherService.GetBy(id));
            if (teacherCard == null) 
				return HttpNotFound();
            teacherCard.LevelList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("LEVEL")).ToSelectListItems(teacherCard.LevelCode);
            return View(teacherCard);
        }

        [HttpPost]
        public ActionResult Edit(TeacherCard _vm_Teacher)
        {
			try
            {
                SetData(_vm_Teacher,false);
                hasValue = _teacherService.Update(e_Teacher);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = e_Teacher.Id, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        public ActionResult Deletes(string id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                int[] _codes = id.Split(',').Select(item => int.Parse(item)).ToArray();
                var teacherImgs = _teacherService.FindAll(_codes).Where(d => !string.IsNullOrEmpty(d.Image)).ToList();

                hasValue = _teacherService.Delete(_codes);
				hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                {
                    UploadHelper _upload = new UploadHelper();
                    string pathImgTeacher = HttpContext.Server.MapPath("~/Files/Teacher/");
                    teacherImgs.ForEach(item =>
                    {
                        _upload.DeleteFileOnServer(pathImgTeacher + item.Id.ToString() + "/", item.Image);
                    });
                    //-- Delete Foloder
                    if (Directory.Exists(pathImgTeacher + teacherImgs.Select(d => d.Id).FirstOrDefault()))
                        Directory.Delete(pathImgTeacher + teacherImgs.Select(d => d.Id).FirstOrDefault(), true);
                    return Json(new { v = true, count = _codes.Count() });
                }
                return Json("Xóa dữ liệu không thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }
        public JsonResult RemoveImg(int id, string img)
        {
            hasValue = _teacherService.DeleteImg(id);
            hasValue = _vuViecService.Commit();
            if (string.IsNullOrEmpty(hasValue))
            {
                UploadHelper _upload = new UploadHelper();
                string PathSave = HttpContext.Server.MapPath("~/Files/Teacher/" + id.ToString() + "/");
                _upload.DeleteFileOnServer(PathSave, img);
            }
            return Json(string.IsNullOrEmpty(hasValue));
        }
        #region -- Json --
        public JsonResult LoadData(GridSettings grid)
        {
            var list = Mapper.Map<IEnumerable<TeacherList>>(_teacherService.FindAll()).AsQueryable();
            list = JqGrid.SetupGrid<TeacherList>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
        public JsonResult LoadDataPopup(GridSettings grid, string idTeacherChoosed)
        {
            List<int> idTeacherChooseds = new List<int>();
            if (!string.IsNullOrEmpty(idTeacherChoosed))
                idTeacherChooseds = idTeacherChoosed.Split(',').Select(item => int.Parse(item)).ToList();
            var list = Mapper.Map<IEnumerable<TeacherList>>(_teacherService.FindAll(idTeacherChooseds)).AsQueryable();
            list = JqGrid.SetupGrid<TeacherList>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region -- Functions --
        private void SetData(TeacherCard _vm_Teacher, bool isCreate)
        {
            teacherCard = _vm_Teacher;
            if (isCreate)
                teacherCard.Id = _teacherService.GetID();
            var ReasonInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("LEVEL", _vm_Teacher.LevelCode)) ?? new VM_OptionLine();
            _vm_Teacher.LevelId = ReasonInfo.LineNo;
            _vm_Teacher.LevelCode = ReasonInfo.Code;
            UploadHelper _uploadHelper = new UploadHelper();
            _uploadHelper.PathSave = HttpContext.Server.MapPath("~/Files/Teacher/" + teacherCard.Id.ToString() + "/");
            _uploadHelper.UploadFile(Request.Files["fileUpload"]);
            if (!string.IsNullOrEmpty(_uploadHelper.FileSave))
                teacherCard.Image = _uploadHelper.FileSave;

            e_Teacher = Mapper.Map<E_Teacher>(teacherCard);
        }
		#endregion
    }
}
