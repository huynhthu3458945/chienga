﻿using AutoMapper;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Areas.MasterData.Models;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.MasterData.Controllers
{
    [CompressResponseAttribute]
    public class TemplateController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly ITemplateService _templateService;
        private string sysCategory = "TEMPLATE";
        private bool? permission = false;
        private Template template;
        private M_Template mTemplate;
        private string hasValue;
        #endregion

        #region -- Contructor --
        public TemplateController
            (
                IVuViecService vuViecService
                , ITemplateService templateService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _templateService = templateService;
        }
        #endregion
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }
        public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            template = new Template();
            return View(template);
        }

        [HttpPost]
        public ActionResult Create(Template _template)
        {
            try
            {
                SetData(_template, true);
                hasValue = _templateService.Insert(mTemplate);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = mTemplate.Code, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        public ActionResult Edit(string id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            template = Mapper.Map<Template>(_templateService.Get(id));
            if (template == null)
                return HttpNotFound();
            return View(template);
        }

        [HttpPost]
        public ActionResult Edit(Template _template)
        {
            try
            {
                SetData(_template, false);
                hasValue = _templateService.Update(mTemplate);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = mTemplate.Code, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        public ActionResult Deletes(string id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                var _codes = id.Split(',').ToList();
                hasValue = _templateService.Delete(_codes);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return Json(new { v = true, count = _codes.Count() });
                return Json("Xóa dữ liệu không thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }
        #region -- Json --
        public JsonResult LoadData(GridSettings grid)
        {
            var list = Mapper.Map<IEnumerable<Template>>(_templateService.FindAll()).AsQueryable();
            list = JqGrid.SetupGrid<Template>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region -- Functions --
        private void SetData(Template _template, bool isCreate)
        {
            mTemplate = Mapper.Map<M_Template>(_template);
        }
        #endregion
    }
}