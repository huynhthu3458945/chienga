﻿//####################################################################
//# Copyright (C) 2021-2022, TTL JSC.  All Rights Reserved. 
//#
//# History:
//#     Date Time       Updater         Comment
//#     24/08/2021      Huỳnh Thử       Update
//####################################################################

using AutoMapper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.ExeclOfficeEngine;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.StoreProcedure.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Areas.MasterData.Models;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.MasterData.Controllers
{
    [CompressResponseAttribute]
    public class UserinfoController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IUserInfoService _userInfoService;
        private readonly ICardLineService _cardLineService;
        private readonly IHistoryChangeClass _historyChangeClassService;
        private readonly IClassService _classService;
        private readonly IStudentClassService _studentClassService;
        private readonly IStoreProcedureService _storeProcedureService;
        private readonly HttpClientHelper _httpClientHelper;
        private string sysCategory = "USERINFO";
        private bool? permission = false;
        private VM_UserInfo vmUserInfo;
        private M_UserInfo mUserInfo;
        private string hasValue;
        #endregion

        #region -- Contructor --
        public UserinfoController
            (
                IVuViecService vuViecService
                , IUserInfoService userInfoService
                , ICardLineService cardLineService
                , IHistoryChangeClass historyChangeClassService
                , IClassService classService
                , IStudentClassService studentClassService
                , IStoreProcedureService storeProcedureService
                , HttpClientHelper httpClientHelper
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _userInfoService = userInfoService;
            _cardLineService = cardLineService;
            _historyChangeClassService = historyChangeClassService;
            _classService = classService;
            _studentClassService = studentClassService;
            _storeProcedureService = storeProcedureService;
            _httpClientHelper = httpClientHelper;
        }
        #endregion
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }
        public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            vmUserInfo = new VM_UserInfo();
            ViewBag.id = vmUserInfo.Id;
            return View(vmUserInfo);
        }
        public ActionResult Edit(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var userInfo = _userInfoService.GetBy(id);
            vmUserInfo = Mapper.Map<VM_UserInfo>(userInfo);
            vmUserInfo.ClassIdOld = userInfo.ClassId;
            var historyCreateDate = _historyChangeClassService.GetByUserName(userInfo.userName).Max(z => z.CreateDate);
            if (historyCreateDate != null)
                vmUserInfo.IsNotChangeClass = userInfo.DateActive.Value.AddDays(7) < DateTime.Now.Date;

            var cardInfo = _cardLineService.GetBySerialNumber(vmUserInfo.Serinumber);
            vmUserInfo.LevelCode = cardInfo.LevelCode;
            if (vmUserInfo == null)
                return HttpNotFound();
            ViewBag.id = vmUserInfo.Id;
            return View(vmUserInfo);
        }
        [HttpPost]
        public ActionResult Create(VM_UserInfo _userInfo)
        {
            try
            {
                SetData(_userInfo);
                hasValue = _userInfoService.Insert(mUserInfo);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                {
                    int id = _userInfoService.GetId();
                    return RedirectToAction("Edit", new { id = id, msg = "1" });
                }
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult Edit(VM_UserInfo _userInfo)
        {
            try
            {
                SetData(_userInfo);
                hasValue = _userInfoService.Update(mUserInfo);
                // add History
                UserLoginVMs userLogin = GetUser();
                M_HistoryChangeClass modelHistoryChangeClass = new M_HistoryChangeClass();
                modelHistoryChangeClass.ClassIdOld = _userInfo.ClassIdOld;
                modelHistoryChangeClass.ClassId = mUserInfo.ClassId;
                modelHistoryChangeClass.UserName = mUserInfo.userName;
                modelHistoryChangeClass.CreateDate = DateTime.Now;
                modelHistoryChangeClass.CreateUserID = userLogin.Username;
                hasValue = _historyChangeClassService.Insert(modelHistoryChangeClass);

                string levelCode = string.Empty;
                if (_userInfo.ClassId <= 8)
                    levelCode = "cap1";
                else if (_userInfo.ClassId <= 15)
                    levelCode = "cap2";
                else if (_userInfo.ClassId <= 18)
                    levelCode = "cap3";

                var cardInfo = _cardLineService.GetBySerialNumber(_userInfo.Serinumber);
                if (cardInfo.LevelCode.Equals("200"))
                    AddStudent2Class(levelCode, _userInfo.userName);
                if (cardInfo.LevelCode.Equals("300"))
                {
                    AddStudent2Class2(_userInfo.ClassId, _userInfo.userName, _userInfo.DateExpired);
                }

                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = mUserInfo.Id, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public JsonResult CheckUser(int id, string u, string e, string p)
        {
            //var hasUsername = !string.IsNullOrEmpty(Mapper.Map<VM_UserInfo>(_userInfoService.GetBy(u, id)).userName);
            var hasEmail = !string.IsNullOrEmpty(Mapper.Map<VM_UserInfo>(_userInfoService.GetByEmail(e, id)).userName);
            var hasPhone = !string.IsNullOrEmpty(Mapper.Map<VM_UserInfo>(_userInfoService.GetByPhone(p, id)).userName);
            var hasValue = new StringBuilder();
            hasValue.Append("");
            //if (hasUsername)
            //    hasValue.Append("- Tên đăng nhập đã tồn tại<BR />");
            if (hasEmail)
                hasValue.Append("- Email đã tồn tại<BR />");
            if (hasPhone)
                hasValue.Append("- Số điện thoại đã tồn tại");
            return Json(hasValue.ToString(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GeneralPassword()
        {
            RandomNumberGenerator randomNumber = new RandomNumberGenerator();
            return Json(randomNumber.RandomNumber(10000, 99999), JsonRequestBehavior.AllowGet);
        }
        public JsonResult LoadData(GridSettings grid)
        {
            var list = Mapper.Map<IEnumerable<VM_UserInfo>>(_userInfoService.FindAll()).AsQueryable();
            list = JqGrid.SetupGrid<VM_UserInfo>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
        private void SetData(VM_UserInfo _vmUserinfo)
        {
            vmUserInfo = Mapper.Map<VM_UserInfo>(_userInfoService.GetById(_vmUserinfo.Id));
            vmUserInfo.userName = _vmUserinfo.userName;
            vmUserInfo.fullName = _vmUserinfo.fullName;
            vmUserInfo.Email = _vmUserinfo.Email;
            vmUserInfo.Phone = _vmUserinfo.Phone;
            vmUserInfo.ActivePhone = _vmUserinfo.ActivePhone;
            vmUserInfo.ClassId = _vmUserinfo.ClassId;
            mUserInfo = Mapper.Map<M_UserInfo>(vmUserInfo);
        }
        /// <summary>
        /// Buy card
        /// </summary>
        /// <param name="id">Seri number</param>
        /// <returns></returns>
        public ActionResult EditUserInfo(string id)
        {
            var userInfo = Mapper.Map<VM_UserInfo>(_userInfoService.GetBy(int.Parse(id)));
            return PartialView("_EditUserInfo", userInfo);
        }
        [HttpPost]
        public ActionResult EditUserInfo(VM_UserInfo _userInfo)
        {
            var _user = Mapper.Map<VM_UserInfo>(_userInfoService.GetById(_userInfo.Id));
            _user.fullName = _userInfo.fullName;
            _user.userName = _userInfo.userName;
            _user.Email = _userInfo.Email;
            _user.Phone = _userInfo.Phone;
            if (!string.IsNullOrEmpty(_userInfo.Password))
                _user.Password = _userInfo.Password;
            hasValue = _userInfoService.Update(Mapper.Map<M_UserInfo>(_user));
            hasValue = _vuViecService.Commit();
            return Json(string.IsNullOrEmpty(hasValue) ? "" : hasValue);
        }
        public JsonResult ResendSMS(int id)
        {
            var _user = Mapper.Map<VM_UserInfo>(_userInfoService.GetById(id));
            var content = string.Format("5 Phut Thuoc Bai gui anh chi thong tin dang nhap, Ten Dang Nhap: {0}, Mat Khau: {1}", _user.userName, _user.Password);
            var smsHelper = new SMSHelper()
            {
                PhoneNo = _user.Phone,
                Content = content
            };
            hasValue = smsHelper.Send();
            return Json(hasValue, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// LoadClassByLevel
        /// </summary>
        /// <param name="lvCode"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [24/08/2021]
        /// </history>
        public PartialViewResult LoadClassByLevel(string lvCode)
        {
            if (lvCode.Equals("cap1"))
                return PartialView("_Level1");
            else if (lvCode.Equals("cap2"))
                return PartialView("_Level2");
            else if (lvCode.Equals("cap3"))
                return PartialView("_Level3");
            else
                return PartialView("Level");
        }

        /// <summary>
        /// Add Student To ClassList By LevelCode 200
        /// </summary>
        /// <param name="lc"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [24/08/2021]
        /// </history>
        public string AddStudent2Class(string lc, string userName)
        {
            try
            {
                var idLevels = new List<int>();
                if (lc.Equals("cap1"))
                {
                    idLevels.Add(4); idLevels.Add(5); idLevels.Add(6); idLevels.Add(7); idLevels.Add(8);
                }
                if (lc.Equals("cap2"))
                {
                    idLevels.Add(9); idLevels.Add(13); idLevels.Add(14); idLevels.Add(15);
                }
                if (lc.Equals("cap3"))
                {
                    idLevels.Add(16); idLevels.Add(17); idLevels.Add(18);
                }
                var _studentClasses = new List<StudentClassViewModel>();
                var _studentClass = new StudentClassViewModel();
                var classes = Mapper.Map<IEnumerable<ClassList>>(_classService.FindAll(ClassType.Class, "SHOW")).ToList();
                foreach (var item in classes)
                {
                    _studentClass = new StudentClassViewModel();
                    _studentClass.userName = userName;
                    _studentClass.ClassId = item.Id;
                    _studentClass.ClassName = item.Name;
                    _studentClass.IsActive = false;
                    _studentClass.FromDate = DateTime.Now;
                    _studentClass.ToDate = DateTime.Now;
                    if (idLevels.Contains(item.Id))
                        _studentClass.IsActive = true;
                    _studentClasses.Add(_studentClass);
                }
                hasValue = _studentClassService.Delete(userName);
                hasValue = _studentClassService.Insert(Mapper.Map<IEnumerable<M_StudentClass>>(_studentClasses));
                return string.Empty;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        /// <summary>
        /// Add Student To ClassList By LevelCode 300
        /// </summary>
        /// <param name="lc"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [24/08/2021]
        /// </history>
        public string AddStudent2Class2(int classId, string userName, DateTime? dateExpired)
        {
            try
            {
                var idLevels = new List<int>();
                idLevels.Add(classId);
                //var _classRefers = _classReferService.FindAll(classId);
                //foreach (var item in _classRefers)
                //{
                //    idLevels.Add(item.ClassReferId);
                //}
                var _studentClasses = new List<StudentClassViewModel>();
                var _studentClass = new StudentClassViewModel();
                var classes = Mapper.Map<IEnumerable<ClassList>>(_classService.FindAll(ClassType.Class, "SHOW")).ToList();
                foreach (var item in classes)
                {
                    _studentClass = new StudentClassViewModel();
                    _studentClass.userName = userName;
                    _studentClass.ClassId = item.Id;
                    _studentClass.ClassName = item.Name;
                    _studentClass.IsActive = false;
                    _studentClass.FromDate = DateTime.Now;
                    _studentClass.ToDate = DateTime.Now;
                    if (idLevels.Contains(item.Id))
                    {
                        if (dateExpired != null)
                            _studentClass.ToDate = dateExpired ?? DateTime.Now;
                        _studentClass.IsActive = true;
                    }
                    _studentClasses.Add(_studentClass);
                }
                hasValue = _studentClassService.Delete(userName);
                hasValue = _studentClassService.Insert(Mapper.Map<IEnumerable<M_StudentClass>>(_studentClasses));
                return string.Empty;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        /// <summary>
        /// CustomerInfoResult 
        /// </summary>
        /// <param name="pc"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [27/08/2021]
        /// </history>
        public PartialViewResult CustomerInfoResult(ParamCustomer pc)
        {
            var userInfos = _storeProcedureService.SP_UserInfo(pc.UserName ?? "", pc.FullName ?? "", pc.Email ?? "", pc.PhoneNo ?? "", pc.SerialNumber ?? "", string.Empty);

            return PartialView("_CustomerInfo", userInfos);
        }

        /// <summary>
        /// CustomerInfoResult 
        /// </summary>
        /// <param name="pc"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [30/08/2021]
        /// </history>
        public ActionResult Download(string UserName, string PhoneNo, string Email, string FullName)
        {

            var res = _storeProcedureService.SP_UserInfo(UserName ?? "", FullName ?? "", Email ?? "", PhoneNo ?? "", string.Empty, string.Empty); ;
            var dataModel = res.ToList().ConvertToDataTable();
            dataModel.TableName = "Model";
            var stream = Excel.ExportReport(dataModel, HttpContext.Server.MapPath("/Areas/MasterData/Report/UserSTNHD.xlsx"), false, OfficeType.XLSX);
            return File(stream, OfficeContentType.XLSX, "UserSTNHD.xlsx");
        }

        public ActionResult RemoveDevice()
        {
            #region -- Role user --
            //permission = GetPermission(sysCategory, Authorities.View);
            //if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            //if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }
        public JsonResult FuncRemoveDevice(string userName)
        {
            try
            {
                var userInfo = _userInfoService.GetBy(userName);
                //string urlGet = $"https://us-central1-tamtriluc-1a443.cloudfunctions.net/apiV2/userdevicelogin/get?user_id={userInfo.Id}";
                //string urlDelete = $"https://us-central1-tamtriluc-1a443.cloudfunctions.net/apiV2/userdevicelogin/delete";
                //var res = _httpClientHelper.GetTokeAsync<ModelDevice>(urlGet).GetAwaiter().GetResult();
                //if(res.data != null)
                //    foreach (var item in res.data)
                //    {
                //        ModelDelete modelDelete = new ModelDelete() { id = item.Id };
                //        var resPost = _httpClientHelper.DeleteAsync(urlDelete, modelDelete);
                //    }

                string urlDelete = $"https://apipro.5phutthuocbai.com/api/MstUserDeviceLogin/DeleteByUserid?userId={userInfo.Id}";
                var resPost = _httpClientHelper.DeleteAsync<string>(urlDelete).Result;
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }

        }

        private class ModelDevice
        {
            public string Id { get; set; }
            public string User_id { get; set; }
            public string Unique_id { get; set; }
        }

        private class ModelDelete
        {
            public int userId { get; set; }
            public string appName { get; set; }
        }
    }
}