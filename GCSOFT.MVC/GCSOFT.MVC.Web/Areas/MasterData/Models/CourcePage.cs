﻿using GCSOFT.MVC.Web.ViewModels.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.MasterData.Models
{
    public class CourcePage
    {
        public int ClassId { get; set; }
        public IEnumerable<SelectListItem> ClassList { get; set; }
        public string Title { get; set; }
        public bool Status { get; set; }
        public bool IsTNTD { get; set; }
        public IEnumerable<CourseList> CourseList { get; set; }
    }
}