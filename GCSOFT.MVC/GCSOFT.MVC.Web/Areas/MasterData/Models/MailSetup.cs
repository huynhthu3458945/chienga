﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.MasterData.Models
{
    public class MailSetup
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string DisplayName { get; set; }
        public string Password { get; set; }
        public string MailReply { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public bool EnableSsl { get; set; }
        public bool UseDefaultCredentials { get; set; }
    }
    public class Template
    {
        [DisplayName("Mã")]
        public string Code { get; set; }
        [DisplayName("Tiêu Đề")]
        public string Title { get; set; }
        [AllowHtml]
        [DisplayName("Nội Dung")]
        public string Content { get; set; }
        [DisplayName("Nội Dung SMS")]
        public string SMSContent { get; set; }
    }
}