﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.Web.Areas.MasterData.Models
{
    public class SubEcourceContent
    {
        public int IdCourse { get; set; }
        public int LineNo { get; set; }
        public int SubLineNo { get; set; }
        public string Title { get; set; }
    }
}