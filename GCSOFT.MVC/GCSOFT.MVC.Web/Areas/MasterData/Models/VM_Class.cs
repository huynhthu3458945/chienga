﻿using GCSOFT.MVC.Model.MasterData;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Web.Mvc;
namespace GCSOFT.MVC.Web.Areas.MasterData.Models
{
    public class VM_Class
    {
        public ClassType ClassType { get; set; }
        public int Id { get; set; }
        [DisplayName("Mã Lớp")]
        public string Code { get; set; }
        [DisplayName("Tên Lớp")]
        public string Name { get; set; }
        [DisplayName("Tên Lớp (EN)")]
        public string NameEN { get; set; }
        [DisplayName("Ghi Chú")]
        public string Remark { get; set; }
        [DisplayName("Thứ Tự")]
        public int SortOrder { get; set; }
        [DisplayName("Trạng Thái")]
        public int StatusId { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }
        public IEnumerable<SelectListItem> StatusList { get; set; }
        [DisplayName("Cấp")]
        public int LevelId { get; set; }
        public string LevelName { get; set; }
        public IEnumerable<SelectListItem> LevelList { get; set; }
        public VM_Class()
        {
            SortOrder = 0;
        }
    }
}