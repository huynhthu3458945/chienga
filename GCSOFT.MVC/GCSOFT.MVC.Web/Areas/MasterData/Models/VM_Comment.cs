﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.MasterData.Models
{
    public class VM_Comment
    {
        public int Id { get; set; }
        public ResultType Type { get; set; }
        public int ReferId { get; set; }
        public DateTime DateCreate { get; set; }
        public int? UserId { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public DateTime DateModified { get; set; }
        public int? ParentId { get; set; }
        public int? ReplyId { get; set; }
        [AllowHtml]
        public string Content { get; set; }
        public string ProfilePicture { get; set; }
        public int StatusId { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }
        public IEnumerable<VM_FileResource> FileResources { get; set; }
        public VM_Comment()
        {
            var _fileResources = new List<VM_FileResource>();
            var _fileResource = new VM_FileResource();
            _fileResource.LineNo = 0;
            _fileResources.Add(_fileResource);
            FileResources = _fileResources;
        }
    }
}