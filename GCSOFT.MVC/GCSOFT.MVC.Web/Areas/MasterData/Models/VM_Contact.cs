﻿using System.ComponentModel;


namespace GCSOFT.MVC.Web.Areas.MasterData.Models
{
    public class VM_Contact
    {
        public int Id { get; set; }
        public int Type { get; set; }
        public int ReferId { get; set; }
        [DisplayName("Điện Thoại")]
        public string Phone { get; set; }
        [DisplayName("Điện Thoại")]
        public string PhoneSystem { get; set; }
        [DisplayName("Email")]
        public string Email { get; set; }
        
    }
}