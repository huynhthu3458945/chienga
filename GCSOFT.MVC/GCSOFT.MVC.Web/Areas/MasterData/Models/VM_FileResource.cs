﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using GCSOFT.MVC.Model.MasterData;
namespace GCSOFT.MVC.Web.Areas.MasterData.Models
{
    public class VM_FileResource
    {
        public ResultType Type { get; set; }
        public int ReferId { get; set; }
        public int LineNo { get; set; }
        [DisplayName("Mô Tả")]
        public string Title { get; set; }
        [DisplayName("Tiêu Đề 1")]
        public string Title2 { get; set; }
        [DisplayName("Tiêu Đề 2")]
        public string Title3 { get; set; }
        [DisplayName("Tiêu Đề 3")]
        public string Title4 { get; set; }
        [DisplayName("Hình Ảnh")]
        public string ImageName { get; set; }
        public string ImagePath { get; set; }
        public string ImageUpload { get; set; }
        public FileType FileType { get; set; }
        public Source Source { get; set; }
        public string ImageBase64 { get; set; }
        public string Domain { get; set; }
        public string ImagePathFull { get; set; }
        public int CommentId { get; set; }
        public string ImageBase64Url { 
            get {
                return  string.Format("data:image/png;base64,{0}", ImageBase64);
            } 
        }
    }
}