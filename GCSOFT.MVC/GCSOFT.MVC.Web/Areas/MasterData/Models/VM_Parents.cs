﻿using System.ComponentModel;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.MasterData.Models
{
    public class Parent_List
    {
        public int Id { get; set; }
        [DisplayName("Họ và Tên Phụ Huynh")]
        public string FullName { get; set; }
        [DisplayName("Link FB")]
        public string FB_Link { get; set; }
        [DisplayName("Link FB")]
        public string FB_Name { get; set; }
        public string Facebook {
            get 
            {
                if (string.IsNullOrEmpty(FB_Name))
                    FB_Name = FB_Link;    
                if (!string.IsNullOrEmpty(FB_Link))
                    return string.Format("<a href=\"{0}\">{1}</a>", FB_Link, FB_Name);
                return "";
            }
        }
    }
    public class Parent_Card
    {
        public int Id { get; set; }
        [DisplayName("Tên Phụ Huynh")]
        public string FirstName { get; set; }
        [DisplayName("Họ Và Tên Lót")]
        public string LastName { get; set; }
        [DisplayName("Họ Và Tên Phụ Huynh")]
        public string FullName { get; set; }
        [DisplayName("Link FB")]
        public string FB_Link { get; set; }
        [DisplayName("Tên FB")]
        public string FB_Name { get; set; }
        public virtual IEnumerable<VM_Childrens> Childrens { get; set; }
        public virtual IEnumerable<VM_Contact> Contacts { get; set; }
        public Parent_Card()
        {
            var _childrens = new List<VM_Childrens>();
            var _children = new VM_Childrens();
            _children.Id = 0;
            _childrens.Add(_children);
            Childrens = _childrens;

            var _contacts = new List<VM_Contact>();
            var _contact = new VM_Contact();
            _contact.Id = 0;
            _contacts.Add(_contact);
            Contacts = _contacts;
        }
    }
    public class VM_Childrens
    {
        public int Id { get; set; }
        public int ParentsId { get; set; }
        [DisplayName("Tên Con")]
        public string FirstName { get; set; }
        [DisplayName("Họ Và Tên Lót")]
        public string LastName { get; set; }
        [DisplayName("Họ Và Tên")]
        public string FullName { get; set; }
        [DisplayName("Lớp")]
        public int Class_Id { get; set; }
        [DisplayName("Lớp")]
        public string Class_Code { get; set; }
        [DisplayName("Lớp")]
        public string Class_Name { get; set; }
        public IEnumerable<SelectListItem> ClassList { get; set; }
    }
}