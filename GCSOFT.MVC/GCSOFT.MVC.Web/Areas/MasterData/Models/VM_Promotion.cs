﻿// #################################################################
// # Copyright (C) 2021-2022, Tâm Trí Lực JSC.  All Rights Reserved.                       
// #
// # History：                                                                        
// #	Date Time	    Updated		    Content                	
// #    25/08/2021	    Huỳnh Thử 		Update
// ##################################################################

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.Web.Areas.MasterData.Models
{
    public class VM_Promotion
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string Prefix { get; set; }
        public string Remark { get; set; }
    }
}