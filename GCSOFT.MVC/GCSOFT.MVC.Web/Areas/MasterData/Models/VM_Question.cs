﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;
using GCSOFT.MVC.Model.MasterData;
using System.Collections.Generic;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.MasterData.Models
{
    public class Question_List
    {
        public int Id { get; set; }
        [DisplayName("Người Hỏi")]
        public string fullName { get; set; }

        [DisplayName("Câu Hỏi")]
        public string Question { get; set; }
        [DisplayName("Ngày Hỏi")]
        public DateTime DateQuestion { get; set; }
        [DisplayName("Ngày Hỏi")]
        public string DateQuestionStr { get { return string.Format("{0:dd/MM/yyyy}", DateQuestion); } }


        [DisplayName("Câu Trả Lời")]
        public string Answer { get; set; }
        [DisplayName("Ngày Trả Lời")]
        public DateTime? DateAnswer { get; set; }
        [DisplayName("Ngày Trả Lời")]
        public string DateAnswerStr { get { return string.Format("{0:dd/MM/yyyy}", DateQuestion); } }

        [DisplayName("Giáo Viên")]
        public string OwnerName { get; set; }
        [DisplayName("Trạng Thái")]
        public string StatusName { get; set; }
        public string ClassName { get; set; }
        public string CourseName { get; set; }
        public string CourseContentName { get; set; }
        public int IdComment { get; set; }
    }
    public class Question_Card
    {
        public int Id { get; set; }
        public QuestionType Type { get; set; }
        [AllowHtml]
        [DisplayName("Câu Hỏi")]
        public string Question { get; set; }

        [AllowHtml]
        [DisplayName("Câu Trả Lời")]
        public string Answer { get; set; }

        [DisplayName("Ngày Hỏi")]
        public DateTime DateQuestion { get; set; }
        [DisplayName("Ngày Hỏi")]
        public string DateQuestionStr { get; set; }

        [DisplayName("Ngày Trả Lời")]
        public DateTime? DateAnswer { get; set; }
        [DisplayName("Ngày Trả Lời")]
        public string DateAnswerStr { get; set; }

        public int QuestionerId { get; set; }
        [DisplayName("Facebook Người Hỏi")]
        public string QuestionerFB { get; set; }
        [DisplayName("Họ Tên Người Hỏi")]
        public string QuestionerName { get; set; }

        [DisplayName("Giáo Viên Hổ Trợ")]
        public int OwnerId { get; set; }
        public string OwnerName { get; set; }
        public IEnumerable<SelectListItem> OwnerList { get; set; }

        [DisplayName("Phụ Huynh Hổ Trợ")]
        public int ParentsId { get; set; }
        public string ParentsName { get; set; }
        public IEnumerable<SelectListItem> ParentsList { get; set; }

        [DisplayName("Nhóm")]
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public IEnumerable<SelectListItem> GroupList { get; set; }

        [DisplayName("Khối Lớp")]
        public int LevelId { get; set; }
        public string LevelName { get; set; }
        public IEnumerable<SelectListItem> LevelList { get; set; }

        [DisplayName("Khối Lớp")]
        public int ClassId { get; set; }
        public string ClassCode { get; set; }
        public string ClassName { get; set; }
        public IEnumerable<SelectListItem> ClassList { get; set; }

        [DisplayName("Môn Học")]
        public int CourseId { get; set; }
        public string CourseName { get; set; }
        public IEnumerable<SelectListItem> CourseList { get; set; }

        [DisplayName("Bài Học")]
        public int CourseContentLineNo { get; set; }
        public string CourseContentName { get; set; }
        public IEnumerable<SelectListItem> CourseContentList { get; set; }

        [DisplayName("Học Tập")]
        public int MethodId { get; set; }
        public string MethodCode { get; set; }
        public string MethodName { get; set; }
        public IEnumerable<SelectListItem> MethodList { get; set; }

        [DisplayName("Trạng Thái")]
        public int StatusId { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }
        public string userName { get; set; }
        [DisplayName("Học Viên")]
        public string fullName { get; set; }
        public int CommentId { get; set; }
        public IEnumerable<SelectListItem> StatusList { get; set; }
        public IEnumerable<VM_FileResource> FileResources { get; set; }
        public Question_Card()
        {
            var _fileResources = new List<VM_FileResource>();
            var _fileResource = new VM_FileResource();
            _fileResource.LineNo = 0;
            _fileResources.Add(_fileResource);
            FileResources = _fileResources;
        }
    }
    public class Suggestions
    {
        public int Id { get; set; }
        [AllowHtml]
        [DisplayName("Nội Dung")]
        public string Answer { get; set; }
        public DateTime DateQuestion { get; set; }
        [DisplayName("Ngày Gửi")]
        public string DateQuestionStr { get; set; }

        public int ClassId { get; set; }
        public string ClassCode { get; set; }
        [DisplayName("Khối Lớp")]
        public string ClassName { get; set; }

        public int StatusId { get; set; }
        public string StatusCode { get; set; }
        [DisplayName("Trạng Thái")]
        public string StatusName { get; set; }
        public int CourseId { get; set; }
        [DisplayName("Môn Học")]
        public string CourseName { get; set; }

        public int CourseContentLineNo { get; set; }
        [DisplayName("Bài Học")]
        public string CourseContentName { get; set; }

        public string userName { get; set; }
        [DisplayName("Học Viên Góp Ý")]
        public string fullName { get; set; }
        public IEnumerable<VM_FileResource> FileResources { get; set; }
        public int CommentId { get; set; }
    }
    public class QuestionFilter
    {
        [DisplayName("Học Viên")]
        public string Questioner { get; set; }
        [DisplayName("Câu Hỏi")]
        public string QuestionTitle { get; set; }
        [DisplayName("Giáo Viên")]
        public string TeacherName { get; set; }
        [DisplayName("Trạng Thái")]
        public IEnumerable<SelectListItem> Status { get; set; }
        [DisplayName("Trạng Thái")]
        public string StatusCode { get; set; }
        public Question_List QuestionList { get; set; }
    }
}