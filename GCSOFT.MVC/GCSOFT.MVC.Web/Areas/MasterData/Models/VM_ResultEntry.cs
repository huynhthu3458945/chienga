﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GCSOFT.MVC.Model.MasterData;
namespace GCSOFT.MVC.Web.Areas.MasterData.Models
{
    public class VM_ResultEntry
    {
        public int Id { get; set; }
        public ResultType Type { get; set; }
        public int ReferId { get; set; }
        public int? LineNo { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public string QuestionUnsigned { get; set; }
        public string AnswerUnsigned { get; set; }
        public DateTime? QuestionDate { get; set; }
        public DateTime? AnswerDate { get; set; }
        public int? Questioner_Id { get; set; }
        public string Questioner_FB_Link { get; set; }
        public string Questioner_FB_Name { get; set; }
        public int? Supporter_Id { get; set; }
        public string Supporter_Name { get; set; }
        public int? Parent_Id { get; set; }
        public string parent_Name { get; set; }
        public int? Level_Id { get; set; }
        public string Level_Name { get; set; }
        public int? Class_Id { get; set; }
        public string Class_Name { get; set; }
        public int? Course_Id { get; set; }
        public string Course_Name { get; set; }
        public int? Method_Id { get; set; }
        public string method_Name { get; set; }
        public int? Group_Id { get; set; }
        public string GroupName { get; set; }
        public int? Status_Id { get; set; }
        public string Status_Name { get; set; }
        public string userName { get; set; }
        public string fullName { get; set; }
    }
}