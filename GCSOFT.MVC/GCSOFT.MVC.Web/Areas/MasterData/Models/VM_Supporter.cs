﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.Web.Areas.MasterData.Models
{
    public class VM_Supporter
    {
        public int Id { get; set; }
        [DisplayName("Tiêu Đề (Vi)")]
        public string Name { get; set; }
        [DisplayName("Tiêu Đề (EN)")]
        public string NameEN { get; set; }
        [DisplayName("Thứ Tự")]
        public int SortOrder { get; set; }
        [DisplayName("Số Cột")]
        public int NoOfColumn { get; set; }
        public IEnumerable<VM_FileResource> FileResources { get; set; }
        public VM_Supporter()
        {
            NoOfColumn = 4;
            var _fileResources = new List<VM_FileResource>();
            var _fileResource = new VM_FileResource();
            _fileResource.LineNo = 0;
            _fileResources.Add(_fileResource);
            FileResources = _fileResources;
        }
    }
}