﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.Web.Areas.MasterData.Models
{
    public class VM_UserInfo
    {
        public int Id { get; set; }
        [DisplayName("Tên Đăng Nhập")]
        public string userName { get; set; }
        [DisplayName("Họ và Tên")]
        public string fullName { get; set; }
        [DisplayName("Mật Khẩu")]
        public string Password { get; set; }
        [DisplayName("Email")]
        public string Email { get; set; }
        [DisplayName("Điện Thoại")]
        public string Phone { get; set; }
        public string Phone2 { get; set; }
        [DisplayName("Cho Phép Sử Dụng")]
        public bool ActivePhone { get; set; }
        [DisplayName("Địa Chỉ")]
        public string Address { get; set; }
        [DisplayName("Đã Chọn Lớp")]
        public bool HasClass { get; set; }
        [DisplayName("Facebook")]
        public string facebook { get; set; }
        [DisplayName("Zalo")]
        public string zalo { get; set; }
        [DisplayName("Viber")]
        public string viber { get; set; }
        [DisplayName("Serinumber")]
        public string Serinumber { get; set; }
        [DisplayName("Lớp Học")]
        public int ClassId { get; set; }
        public int ClassIdOld { get; set; }
        public string LevelCode { get; set; }
        public bool IsUpdateLevelCode300 { get; set; }
        public DateTime? DateExpired { get; set; }
        public DateTime? DateActive { get; set; }
        public bool IsNotChangeClass { get; set; }
        public int? AgencyId { get; set; }
        public STNHDType? STNHDType { get; set; }
        public string Avartar { get; set; }
        public DateTime? DateCreate { get; set; }
        public DateTime? Dayofbirth { get; set; }
        public string GenderCode { get; set; }
        public string GenderName { get; set; }
        public string FromSource { get; set; }
        public string IsDaiSu { get; set; }
        public string ReferralCode { get; set; }
        public string RoleCode { get; set; }
        public string RoleName { get; set; }
        public int maxdevicelogin { get; set; }
        public string BackgroundUrl { get; set; }
        public VM_UserInfo()
        {
            Id = 0;
        }
    }
}