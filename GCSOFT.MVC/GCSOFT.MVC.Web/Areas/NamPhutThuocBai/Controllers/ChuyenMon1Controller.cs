﻿using System.Web.Mvc;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using System.Net;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Service.NamPhutThuocBaiService.Interface;
using GCSOFT.MVC.Web.Areas.NamPhutThuocBai.Data;
using GCSOFT.MVC.Model.NamPhutThuocBai;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using GCSOFT.MVC.Service.StoreProcedure.Interface;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Web.ViewModels.MasterData;

namespace GCSOFT.MVC.Web.Areas.NamPhutThuocBai.Controllers
{
	[CompressResponseAttribute]
    public class ChuyenMon1Controller : BaseController
    {
		#region -- Properties --
		private readonly IVuViecService _vuViecService;
		private readonly INopBaiService _nopBaiService;
        private readonly ITrongTaiService _trongTaiService;
        private readonly IUserService _userService;
        private readonly IUserThuocNhomService _userThuocNhomService;
        private readonly IStoreProcedureService _storeService;
        private readonly IMindMapPointService _mindMapPointService;
        private readonly IOptionLineService _optionLIneService;
        private string sysCategory = "CHUYENMON1"; //Chấm Mindmap
        private bool? permission = false;
        private NopBaiCard nopBaiCard;
        private TB_NopBai tbNopBai;
        private string hasValue;
		#endregion
		
		#region -- Contructor --
		public ChuyenMon1Controller
            (
				IVuViecService vuViecService
                , INopBaiService nopBaiService
                , ITrongTaiService trongTaiService
                , IUserService userService
                , IUserThuocNhomService userThuocNhomService
                , IStoreProcedureService storeService
                , IMindMapPointService mindMapPointService
                , IOptionLineService optionLIneService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _nopBaiService = nopBaiService;
            _trongTaiService = trongTaiService;
            _userService = userService;
            _userThuocNhomService = userThuocNhomService;
            _storeService = storeService;
            _mindMapPointService = mindMapPointService;
            _optionLIneService = optionLIneService;
        }
		#endregion
		
		#region -- Get --
		public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }
		
		public ActionResult Edit(int? id)
        {
			if (id == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            
			nopBaiCard = Mapper.Map<NopBaiCard>(_nopBaiService.GetBy(id ?? 0));
            if (nopBaiCard == null) 
				return HttpNotFound();
            var trongTaiTypes = new List<string>();
            nopBaiCard.ChuyenMon1List = Mapper.Map<IEnumerable<TrongTaiList>>(_trongTaiService.FindAll("CM1")).ToList().ToSelectListItems(nopBaiCard.IdTrongTaiChuyenMon1);
            nopBaiCard.MindMapPoints = Mapper.Map<IEnumerable<MindMapPoint>>(_mindMapPointService.FindAll(id ?? 0));
            if (nopBaiCard.MindMapPoints.Count() == 0)
            {
                var optionLines = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLIneService.FindAll("MINDMAP"));
                var mindMapPoints = new List<MindMapPoint>();
                var mindMapPoint = new MindMapPoint();
                foreach (var item in optionLines)
                {
                    mindMapPoint = new MindMapPoint();
                    mindMapPoint.IdNopBai = id ?? 0;
                    mindMapPoint.PointCode = item.Code;
                    mindMapPoint.PointDescription = item.Name;
                    mindMapPoints.Add(mindMapPoint);
                }
                nopBaiCard.MindMapPoints = mindMapPoints;
            }
            return View(nopBaiCard);
        }
		#endregion
		
		#region -- Post --
		
		 [HttpPost]
        public ActionResult Edit(NopBaiCard _nopBaiCard)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            try
            {
                SetData(_nopBaiCard);
                hasValue = _nopBaiService.Update(tbNopBai);
                hasValue = _mindMapPointService.Detele(_nopBaiCard.Id);
                hasValue = _mindMapPointService.Insert(Mapper.Map<IEnumerable<TB_MindMapPoint>>(_nopBaiCard.MindMapPoints));
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { area = "NamPhutThuocBai", id = tbNopBai.Id, msg = "2" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
		#endregion
		
		#region -- Json --
		public JsonResult LoadData(GridSettings grid)
        {
            var userInfo = GetUser();
            var listTrongTai = _storeService.sp_QuanLyTrongTai(userInfo.Username).Select(d => d.userName).ToList();
            var list = Mapper.Map<IEnumerable<NopBaiList>>(_nopBaiService.FindAllChuyenMon1(listTrongTai)).AsQueryable();
            var userThuocNhoms = Mapper.Map<IEnumerable<UserThuocNhomVM>>(_userThuocNhomService.GetMany(userInfo.ID));
            if (userThuocNhoms.Select(d => d.UserGroupCode).Contains("ADMIN_DAOTAO"))
            {
                list = Mapper.Map<IEnumerable<NopBaiList>>(_nopBaiService.FindAllChuyenMon1()).AsQueryable();
            }
            //list = list.Where(d => d.CodeDethi.Equals("TUAN3.29072021"));
            list = JqGrid.SetupGrid<NopBaiList>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
		#endregion
		
		#region -- Functions --
		private void SetData(NopBaiCard nopBaiCard)
        {
            var _nopBaiInfo = Mapper.Map<NopBaiCard>(_nopBaiService.Get(nopBaiCard.Id));
            //_nopBaiInfo.ChuyenMon1_NoiDung = nopBaiCard.ChuyenMon1_NoiDung;
            //_nopBaiInfo.ChuyenMon1_TrinhBay1 = nopBaiCard.ChuyenMon1_TrinhBay1;
            //_nopBaiInfo.ChuyenMon1_TrinhBay2 = nopBaiCard.ChuyenMon1_TrinhBay2;
            //_nopBaiInfo.ChuyenMon1_TrinhBay3 = nopBaiCard.ChuyenMon1_TrinhBay3;
            _nopBaiInfo.ChuyenMon1_Mindmap = nopBaiCard.ChuyenMon1_Mindmap;

            _nopBaiInfo.IdTrongTaiChuyenMon1 = nopBaiCard.IdTrongTaiChuyenMon1;
            _nopBaiInfo.Remark2 = nopBaiCard.Remark2;
            var trongTai = Mapper.Map<TrongTaiCard>(_trongTaiService.Get(nopBaiCard.IdTrongTaiChuyenMon1)) ?? new TrongTaiCard();
            _nopBaiInfo.UserTrongTaiChuyenMon1 = trongTai.UserName;

            _nopBaiInfo.ChuyenMon1_StatusCode = "OKE";
            _nopBaiInfo.ChuyenMon1_StatusName = "Đã Chấm Bài";
            if ((_nopBaiInfo.ChuyenMon1_StatusCode ?? "").Equals("OKE") && (_nopBaiInfo.ChuyenMon2_StatusCode ?? "").Equals("OKE"))
            {
                var tongDiem1 = ((_nopBaiInfo.ChuyenMon1_NoiDung * Persent(30)) + (_nopBaiInfo.ChuyenMon1_Mindmap * Persent(30)) + (_nopBaiInfo.ChuyenMon1_TrinhBay1 * Persent(15)) + (_nopBaiInfo.ChuyenMon1_TrinhBay2 * Persent(20)) + (_nopBaiInfo.ChuyenMon1_TrinhBay3 * Persent(5)));
                _nopBaiInfo.TongDiem = (int)tongDiem1;
            }
            tbNopBai = Mapper.Map<TB_NopBai>(_nopBaiInfo);
        }

        private double Persent(int persent)
        {
            try
            {
                return persent / 100;
            }
            catch { return 0; }
        }
        #endregion
    }
}
