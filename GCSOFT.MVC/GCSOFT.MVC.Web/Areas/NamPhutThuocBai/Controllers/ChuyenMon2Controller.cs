﻿using System.Web.Mvc;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using System.Net;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Service.NamPhutThuocBaiService.Interface;
using GCSOFT.MVC.Web.Areas.NamPhutThuocBai.Data;
using GCSOFT.MVC.Model.NamPhutThuocBai;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using GCSOFT.MVC.Service.StoreProcedure.Interface;

namespace GCSOFT.MVC.Web.Areas.NamPhutThuocBai.Controllers
{
	[CompressResponseAttribute]
    public class ChuyenMon2Controller : BaseController
    {
		#region -- Properties --
		private readonly IVuViecService _vuViecService;
		private readonly INopBaiService _nopBaiService;
        private readonly ITrongTaiService _trongTaiService;
        private readonly IUserService _userService;
        private readonly IUserThuocNhomService _userThuocNhomService;
        private readonly IStoreProcedureService _storeService;
        private string sysCategory = "CHUYENMON2";
        private bool? permission = false;
        private NopBaiCard nopBaiCard;
        private TB_NopBai tbNopBai;
        private string hasValue;
		#endregion
		
		#region -- Contructor --
		public ChuyenMon2Controller
            (
				IVuViecService vuViecService
                , INopBaiService nopBaiService
                , ITrongTaiService trongTaiService
                , IUserService userService
                 , IUserThuocNhomService userThuocNhomService
                , IStoreProcedureService storeService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _nopBaiService = nopBaiService;
            _trongTaiService = trongTaiService;
            _userService = userService;
            _userThuocNhomService = userThuocNhomService;
            _storeService = storeService;
        }
		#endregion
		
		#region -- Get --
		public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }
		
		public ActionResult Edit(int? id)
        {
			if (id == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
         
			nopBaiCard = Mapper.Map<NopBaiCard>(_nopBaiService.GetBy(id ?? 0));
            if (nopBaiCard == null) 
				return HttpNotFound();
            nopBaiCard.ChuyenMon2List = Mapper.Map<IEnumerable<TrongTaiList>>(_trongTaiService.FindAll("CM2")).ToList().ToSelectListItems(nopBaiCard.IdTrongTaiChuyenMon2);
            return View(nopBaiCard);
        }
		#endregion
		
		#region -- Post --
		
		 [HttpPost]
        public ActionResult Edit(NopBaiCard _nopBaiCard)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            try
            {

                SetData(_nopBaiCard);
                hasValue = _nopBaiService.Update(tbNopBai);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { area = "NamPhutThuocBai", id = tbNopBai.Id, msg = "2" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
		#endregion
		
		#region -- Json --
		public JsonResult LoadData(GridSettings grid)
        {
            var userInfo = GetUser();
            var listTrongTai = _storeService.sp_QuanLyTrongTai(userInfo.Username).Select(d => d.userName).ToList();
            var list = Mapper.Map<IEnumerable<NopBaiList>>(_nopBaiService.FindAllChuyenMon2(listTrongTai)).AsQueryable();
            var userThuocNhoms = Mapper.Map<IEnumerable<UserThuocNhomVM>>(_userThuocNhomService.GetMany(userInfo.ID));
            if (userThuocNhoms.Select(d => d.UserGroupCode).Contains("ADMIN_DAOTAO"))
            {
                list = Mapper.Map<IEnumerable<NopBaiList>>(_nopBaiService.FindAllChuyenMon2()).AsQueryable();
            }
            list = JqGrid.SetupGrid<NopBaiList>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
		#endregion
		
		#region -- Functions --
		private void SetData(NopBaiCard nopBaiCard)
        {
            var _nopBaiInfo = Mapper.Map<NopBaiCard>(_nopBaiService.Get(nopBaiCard.Id));
            _nopBaiInfo.ChuyenMon2_NoiDung = nopBaiCard.ChuyenMon2_NoiDung;
            _nopBaiInfo.ChuyenMon2_Mindmap = nopBaiCard.ChuyenMon2_Mindmap;
            _nopBaiInfo.ChuyenMon2_TrinhBay1 = nopBaiCard.ChuyenMon2_TrinhBay1;
            _nopBaiInfo.ChuyenMon2_TrinhBay2 = nopBaiCard.ChuyenMon2_TrinhBay2;
            _nopBaiInfo.ChuyenMon2_TrinhBay3 = nopBaiCard.ChuyenMon2_TrinhBay3;
            _nopBaiInfo.IdTrongTaiChuyenMon2 = nopBaiCard.IdTrongTaiChuyenMon2;
            _nopBaiInfo.Remark3 = nopBaiCard.Remark3;
            var trongTai = Mapper.Map<TrongTaiCard>(_trongTaiService.Get(nopBaiCard.IdTrongTaiChuyenMon2)) ?? new TrongTaiCard();
            _nopBaiInfo.UserTrongTaiChuyenMon2 = trongTai.UserName;
            _nopBaiInfo.ChuyenMon2_StatusCode = "OKE";
            _nopBaiInfo.ChuyenMon2_StatusName = "Đã Chấm Bài";
            if ((_nopBaiInfo.ChuyenMon1_StatusCode ?? "").Equals("OKE") && (_nopBaiInfo.ChuyenMon2_StatusCode ?? "").Equals("OKE"))
            {
                var tongDiem1 = ((_nopBaiInfo.ChuyenMon1_NoiDung * Persent(30)) + (_nopBaiInfo.ChuyenMon1_Mindmap * Persent(30)) + (_nopBaiInfo.ChuyenMon1_TrinhBay1 * Persent(15)) + (_nopBaiInfo.ChuyenMon1_TrinhBay2 * Persent(20)) + (_nopBaiInfo.ChuyenMon1_TrinhBay3 * Persent(5)));
                var tongDiem2 = ((_nopBaiInfo.ChuyenMon2_NoiDung * Persent(30)) + (_nopBaiInfo.ChuyenMon2_Mindmap * Persent(30)) + (_nopBaiInfo.ChuyenMon2_TrinhBay1 * Persent(15)) + (_nopBaiInfo.ChuyenMon2_TrinhBay2 * Persent(20)) + (_nopBaiInfo.ChuyenMon2_TrinhBay3 * Persent(5)));
                _nopBaiInfo.TongDiem = (int)tongDiem1 + (int)tongDiem2;
            }
            tbNopBai = Mapper.Map<TB_NopBai>(_nopBaiInfo);
        }
        private int Persent(int persent)
        {
            try
            {
                return persent / 100;
            }
            catch { return 0; }
        }
		#endregion
    }
}
