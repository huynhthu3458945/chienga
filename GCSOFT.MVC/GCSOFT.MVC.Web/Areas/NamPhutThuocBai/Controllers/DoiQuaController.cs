﻿using AutoMapper;
using GCSOFT.MVC.Model.NamPhutThuocBai;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.NamPhutThuocBaiService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Areas.NamPhutThuocBai.Data;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.NamPhutThuocBai.Controllers
{
    [CompressResponseAttribute]
    public class DoiQuaController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly ITransactionGiftService _transGiftService;
        private readonly IOptionLineService _optionLineService;
        private string sysCategory = "TRANSGIFT";
        private bool? permission = false;
        private TransactionGift transGift;
        private TB_TransactionGift tb_transGift;
        private string hasValue;
        #endregion

        #region -- Contructor --
        public DoiQuaController
            (
                IVuViecService vuViecService
                , ITransactionGiftService transGiftService
                , IOptionLineService optionLineService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _transGiftService = transGiftService;
            _optionLineService = optionLineService;
        }
        #endregion
        // GET: NamPhutThuocBai/DoiQua
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }
        public ActionResult Edit(string code)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            transGift = Mapper.Map<TransactionGift>(_transGiftService.GetBy(code));
            if (transGift == null)
                return HttpNotFound();
            transGift.StatusList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("GIFT_STATUS")).ToSelectListItems(transGift.StatusCode);
            return View(transGift);
        }
        [HttpPost]
        public ActionResult Edit(TransactionGift _transGift)
        {
            try
            {
                var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("GIFT_STATUS", _transGift.StatusCode));
                transGift = Mapper.Map<TransactionGift>(_transGiftService.GetBy(_transGift.Code));
                transGift.StatusCode = _transGift.StatusCode;
                transGift.StatusName = statusInfo.Name;
                transGift.HistoryTransfer = _transGift.HistoryTransfer;
                transGift.LastDateUpdate = DateTime.Now;
                transGift.ModifyBy = GetUser().Username;
                hasValue = _transGiftService.Update(Mapper.Map<TB_TransactionGift>(transGift));
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = _transGift.Code, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public JsonResult LoadData(GridSettings grid)
        {
            var list = Mapper.Map<IEnumerable<TrongTaiList>>(_transGiftService.FindAll()).AsQueryable();
            list = JqGrid.SetupGrid<TrongTaiList>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
    }
}