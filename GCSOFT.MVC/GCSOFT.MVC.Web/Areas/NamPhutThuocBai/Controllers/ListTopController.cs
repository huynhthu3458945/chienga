﻿using System.Web.Mvc;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using System.Net;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Service.NamPhutThuocBaiService.Interface;
using GCSOFT.MVC.Web.Areas.NamPhutThuocBai.Data;
using GCSOFT.MVC.Model.NamPhutThuocBai;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using GCSOFT.MVC.Service.StoreProcedure.Interface;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Web.ViewModels.MasterData;

namespace GCSOFT.MVC.Web.Areas.NamPhutThuocBai.Controllers
{
    [CompressResponseAttribute]
    public class ListTopController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly INopBaiService _nopBaiService;
        private readonly ITrongTaiService _trongTaiService;
        private readonly IUserService _userService;
        private readonly IUserThuocNhomService _userThuocNhomService;
        private readonly IStoreProcedureService _storeService;
        private readonly IMindMapPointService _mindMapPointService;
        private readonly IOptionLineService _optionLineService;
        private string sysCategory = "LISTTOP"; //Danh sách Top
        private bool? permission = false;
        private NopBaiCard nopBaiCard;
        private TB_NopBai tbNopBai;
        private string optionHeaderCode = "CodeDeThi";
        private string hasValue;
        #endregion

        #region -- Contructor --
        public ListTopController
            (
                IVuViecService vuViecService
                , INopBaiService nopBaiService
                , ITrongTaiService trongTaiService
                , IUserService userService
                , IUserThuocNhomService userThuocNhomService
                , IStoreProcedureService storeService
                , IMindMapPointService mindMapPointService
                , IOptionLineService optionLIneService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _nopBaiService = nopBaiService;
            _trongTaiService = trongTaiService;
            _userService = userService;
            _userThuocNhomService = userThuocNhomService;
            _storeService = storeService;
            _mindMapPointService = mindMapPointService;
            _optionLineService = optionLIneService;
        }
        #endregion

        #region -- Get --
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            ListTopIndex model = new ListTopIndex();
            model.CodeDethiList = _optionLineService.FindAll(optionHeaderCode).ToSelectListItems(string.Empty);
            model.Top = 20;
            return View(model);
        }

        #endregion

        #region -- Post --

        #endregion

        #region -- Json --
        public JsonResult LoadData(GridSettings grid, string codeDethi, int top)
        {
            var list = Mapper.Map<IEnumerable<ListTop>>(_storeService.SP_ListTop("SELECT", codeDethi, top)).AsQueryable();
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }

        public JsonResult MoveVideo(string codeDethi, int top)
        {
            _storeService.SP_ListTop("MOVE", codeDethi, top);
            return Json("Đã Chuyển qua phần chấm Video", JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region -- Functions --

        #endregion
    }
}
