﻿using System.Web.Mvc;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using System.Net;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Service.NamPhutThuocBaiService.Interface;
using GCSOFT.MVC.Web.Areas.NamPhutThuocBai.Data;
using GCSOFT.MVC.Model.NamPhutThuocBai;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using GCSOFT.MVC.Service.StoreProcedure.Interface;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using GCSOFT.MVC.Service.ExeclOfficeEngine;

namespace GCSOFT.MVC.Web.Areas.NamPhutThuocBai.Controllers
{
    [CompressResponseAttribute]
    public class ListTopTuanController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly INopBaiService _nopBaiService;
        private readonly ITrongTaiService _trongTaiService;
        private readonly IUserService _userService;
        private readonly IUserThuocNhomService _userThuocNhomService;
        private readonly IStoreProcedureService _storeService;
        private readonly IMindMapPointService _mindMapPointService;
        private readonly IOptionLineService _optionLineService;
        private string sysCategory = "LISTTOPTUAN"; //Danh sách Top Tuần
        private bool? permission = false;
        private NopBaiCard nopBaiCard;
        private TB_NopBai tbNopBai;
        private string optionHeaderCode = "CodeDeThi";
        private string hasValue;
        #endregion

        #region -- Contructor --
        public ListTopTuanController
            (
                IVuViecService vuViecService
                , INopBaiService nopBaiService
                , ITrongTaiService trongTaiService
                , IUserService userService
                , IUserThuocNhomService userThuocNhomService
                , IStoreProcedureService storeService
                , IMindMapPointService mindMapPointService
                , IOptionLineService optionLIneService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _nopBaiService = nopBaiService;
            _trongTaiService = trongTaiService;
            _userService = userService;
            _userThuocNhomService = userThuocNhomService;
            _storeService = storeService;
            _mindMapPointService = mindMapPointService;
            _optionLineService = optionLIneService;
        }
        #endregion

        #region -- Get --
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            ListTopIndex model = new ListTopIndex();
            model.CodeDethiList = _optionLineService.FindAll(optionHeaderCode).ToSelectListItems(string.Empty);
            model.Top = 20;
            return View(model);
        }

        /// <summary>
        ///     Download Danh sách top tuần
        /// </summary>
        /// <param name="p"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [19/12/2021]
        /// </history>
        public ActionResult DownloadListTopTuan(string codeDethi, int top)
        {
            var res = Mapper.Map<IEnumerable<ListTop>>(_storeService.SP_ListTop("SELECTTOPTUAN", codeDethi, top)).AsQueryable();
            var optionLine = _optionLineService.FindAll(optionHeaderCode).FirstOrDefault(z=>z.Code == codeDethi);
            var dataModel = res.ToList().ConvertToDataTable();
            dataModel.TableName = "Model";
            List<KeyValuePair<string, object>> excelItems = new List<KeyValuePair<string, object>>();
            excelItems.Add(new KeyValuePair<string, object>("Tuan", optionLine.Name));
            Excel.ExcelItems = excelItems;
            var stream = Excel.ExportReport(dataModel, HttpContext.Server.MapPath("/Areas/NamPhutThuocBai/Report/ListTopTuan.xlsx"), false, OfficeType.XLSX);
            return File(stream, OfficeContentType.XLSX, "ListTopTuan.xlsx");
        }
        #endregion

        #region -- Post --

        #endregion

        #region -- Json --
        public JsonResult LoadData(GridSettings grid, string codeDethi, int top)
        {
            var list = Mapper.Map<IEnumerable<ListTop>>(_storeService.SP_ListTop("SELECTTOPTUAN", codeDethi, top)).AsQueryable();
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region -- Functions --

        #endregion
    }
}
