﻿using System.Web.Mvc;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using System.Net;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Service.NamPhutThuocBaiService.Interface;
using GCSOFT.MVC.Web.Areas.NamPhutThuocBai.Data;
using GCSOFT.MVC.Model.NamPhutThuocBai;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using GCSOFT.MVC.Service.StoreProcedure.Interface;

namespace GCSOFT.MVC.Web.Areas.NamPhutThuocBai.Controllers
{
	[CompressResponseAttribute]
    public class NopBaiController : BaseController
    {
		#region -- Properties --
		private readonly IVuViecService _vuViecService;
		private readonly INopBaiService _nopBaiService;
        private readonly ITrongTaiService _trongTaiService;
        private readonly IUserService _userService;
        private readonly IUserThuocNhomService _userThuocNhomService;
        private readonly IStoreProcedureService _storeService;
        private string sysCategory = "NOPBAI";
        private bool? permission = false;
        private NopBaiCard nopBaiCard;
        private TB_NopBai tbNopBai;
        private string hasValue;
		#endregion
		
		#region -- Contructor --
		public NopBaiController
            (
				IVuViecService vuViecService
                , INopBaiService nopBaiService
                , ITrongTaiService trongTaiService
                , IUserService userService
                , IUserThuocNhomService userThuocNhomService
                , IStoreProcedureService storeService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _nopBaiService = nopBaiService;
            _trongTaiService = trongTaiService;
            _userService = userService;
            _userThuocNhomService = userThuocNhomService;
            _storeService = storeService;
        }
		#endregion
		
		#region -- Get --
		public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }
		
		public ActionResult Edit(int? id)
        {
			if (id == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
			nopBaiCard = Mapper.Map<NopBaiCard>(_nopBaiService.GetBy(id ?? 0));
            if (nopBaiCard == null) 
				return HttpNotFound();
            nopBaiCard.TrongTaiVong1List = Mapper.Map<IEnumerable<TrongTaiList>>(_trongTaiService.FindAll("VONG1")).ToList().ToSelectListItems(nopBaiCard.IdTrongTaiVong1);
            var userThuocNhoms = Mapper.Map<IEnumerable<UserThuocNhomVM>>(_userThuocNhomService.GetMany(GetUser().ID));
            if (userThuocNhoms.Select(d => d.UserGroupCode).Contains("ADMIN_DAOTAO"))
                ViewBag.disabled = "";
            else
                ViewBag.disabled = "disabled";
            return View(nopBaiCard);
        }
		#endregion
		
		#region -- Post --
		
		 [HttpPost]
        public ActionResult Edit(NopBaiCard _nopBaiCard)
        {
            try
            {
                SetData(_nopBaiCard);
                hasValue = _nopBaiService.Update(tbNopBai);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { area = "NamPhutThuocBai", id = tbNopBai.Id, msg = "2" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
		#endregion
		
		#region -- Json --
		public JsonResult LoadData(GridSettings grid)
        {
            var userInfo = GetUser();
            var listTrongTai = _storeService.sp_QuanLyTrongTai(userInfo.Username).Select(d => d.userName).ToList();
            var list = Mapper.Map<IEnumerable<NopBaiList>>(_nopBaiService.FindAll(listTrongTai)).AsQueryable();
            var userThuocNhoms = Mapper.Map<IEnumerable<UserThuocNhomVM>>(_userThuocNhomService.GetMany(userInfo.ID));
            if (userThuocNhoms.Select(d => d.UserGroupCode).Contains("ADMIN_DAOTAO"))
            {
                list = Mapper.Map<IEnumerable<NopBaiList>>(_nopBaiService.FindAll()).AsQueryable();
            }else
            {
                list = list.Where(d => d.CodeDethi.Contains("TUAN18"));
            }
            //var userInfo = GetUser();
            //var list = Mapper.Map<IEnumerable<NopBaiList>>(_nopBaiService.FindAll(userInfo.Username)).AsQueryable();
            //var userThuocNhoms = Mapper.Map<IEnumerable<UserThuocNhomVM>>(_userThuocNhomService.GetMany(userInfo.ID));
            //if (userThuocNhoms.Select(d => d.UserGroupCode).Contains("ADMIN_DAOTAO"))
            //{
            //    list = Mapper.Map<IEnumerable<NopBaiList>>(_nopBaiService.FindAll()).AsQueryable();
            //}
            
            list = JqGrid.SetupGrid<NopBaiList>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
        public JsonResult UpdateStatusNDTXuatSac(int id)
        {
            try
            {
                var nopBaiInfo = Mapper.Map<NopBaiCard>(_nopBaiService.Get(id));
                var nopBais = Mapper.Map<IEnumerable<NopBaiCard>>(_nopBaiService.FindAll(nopBaiInfo.ClassId));
                foreach (var item in nopBais)
                {
                    item.NhaDtXuatSacCode = "";
                    item.NhaDtXuatSacName = "";
                    if (item.Id == id) {
                        item.NhaDtXuatSacCode = "SUCCESS";
                        item.NhaDtXuatSacName = "Nhà Đào Tạo Xuất Sắc";
                    }
                }
                hasValue = _nopBaiService.Update(Mapper.Map<IEnumerable<TB_NopBai>>(nopBais));
                hasValue = _vuViecService.Commit();
                return Json(new { msg = "Chúc mừng Nhà Đào Tạo Xuất Sắc" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex) { return Json(new { msg = ex.ToString() }, JsonRequestBehavior.AllowGet); }
        }
        public JsonResult UpdateStatusNDTTruyenCamHung(int id)
        {
            try
            {
                var nopBaiInfo = Mapper.Map<NopBaiCard>(_nopBaiService.Get(id));
                var nopBais = Mapper.Map<IEnumerable<NopBaiCard>>(_nopBaiService.FindAll(nopBaiInfo.ClassId));
                foreach (var item in nopBais)
                {
                    item.NhaDTTruyenCamHungCode = "";
                    item.NhaDTTruyenCamHungName = "";
                    if (item.Id == id)
                    {
                        item.NhaDTTruyenCamHungCode = "SUCCESS";
                        item.NhaDTTruyenCamHungName = "Nhà Đào Tạo Truyền Cảm Hứng";
                    }
                }
                hasValue = _nopBaiService.Update(Mapper.Map<IEnumerable<TB_NopBai>>(nopBais));
                hasValue = _vuViecService.Commit();
                return Json(new { msg = "Chúc mừng Nhà Đào Tạo Truyền Cảm Hứng" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex) { return Json(new { msg = ex.ToString() }, JsonRequestBehavior.AllowGet); }
        }
        #endregion

        #region -- Functions --
        private void SetData(NopBaiCard nopBaiCard)
        {
            var _nopBaiInfo = Mapper.Map<NopBaiCard>(_nopBaiService.Get(nopBaiCard.Id));
            _nopBaiInfo.TieuChi1 = nopBaiCard.TieuChi1;
            _nopBaiInfo.TieuChi2 = nopBaiCard.TieuChi2;
            _nopBaiInfo.TieuChi3 = nopBaiCard.TieuChi3;
            _nopBaiInfo.TieuChi4 = nopBaiCard.TieuChi4;
            _nopBaiInfo.TieuChi5 = nopBaiCard.TieuChi5;
            _nopBaiInfo.LuotView = nopBaiCard.LuotView;
            _nopBaiInfo.LuotInteract = nopBaiCard.LuotInteract;
            _nopBaiInfo.LuotShare = nopBaiCard.LuotShare;
            _nopBaiInfo.LuotComment = nopBaiCard.LuotComment;
            _nopBaiInfo.IdTrongTaiVong1 = nopBaiCard.IdTrongTaiVong1;
            _nopBaiInfo.Remark1 = nopBaiCard.Remark1;

            var trongTai = Mapper.Map<TrongTaiCard>(_trongTaiService.Get(nopBaiCard.IdTrongTaiVong1)) ?? new TrongTaiCard();
            _nopBaiInfo.UserTrongTaiVong1 = trongTai.UserName;
            var totalPoint = nopBaiCard.TieuChi1 + nopBaiCard.TieuChi2 + nopBaiCard.TieuChi3 + nopBaiCard.TieuChi4;
            _nopBaiInfo.TongDiemVong1 = nopBaiCard.TieuChi1 + nopBaiCard.TieuChi2 + nopBaiCard.TieuChi3 + nopBaiCard.TieuChi4 + nopBaiCard.TieuChi5;
            _nopBaiInfo.Point = totalPoint == 4 ? 1 : 0;
            _nopBaiInfo.Vong1StatusCode = "OKE";
            _nopBaiInfo.Vong1StatusName = "Đã Chấm Bài";

            _nopBaiInfo.StatusCode = totalPoint == 4 ? "OKE" : "NOT_OKE";
            _nopBaiInfo.StatusName = totalPoint == 4 ? "Đạt" : "Không Đạt";

            _nopBaiInfo.IsChuyenMon = totalPoint == 4 ? nopBaiCard.IsChuyenMon : false;
            _nopBaiInfo.IsChuyenMonVong1 = totalPoint == 4 ? nopBaiCard.IsChuyenMonVong1 : false;
            tbNopBai = Mapper.Map<TB_NopBai>(_nopBaiInfo);
        }
		#endregion
    }
}
