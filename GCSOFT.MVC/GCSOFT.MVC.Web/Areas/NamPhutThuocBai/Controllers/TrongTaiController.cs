﻿using System.Web.Mvc;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using System.Net;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Service.NamPhutThuocBaiService.Interface;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Web.Areas.NamPhutThuocBai.Data;
using GCSOFT.MVC.Model.NamPhutThuocBai;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using GCSOFT.MVC.Web.Areas.MasterData.Models;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.SystemModels;

namespace GCSOFT.MVC.Web.Areas.NamPhutThuocBai.Controllers
{
	[CompressResponseAttribute]
    public class TrongTaiController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly ITrongTaiService _trongTaiService;
        private readonly IOptionLineService _optionLineService;
        private readonly IClassService _classService;
        private readonly IUserService _userService;
        private readonly IUserThuocNhomService _userThuocNhomService;
        private string sysCategory = "TRONGTAI";
        private bool? permission = false;
        private TrongTaiCard trongTaiCard;
        private TB_TrongTai tbTrongTai;
        private string hasValue;
        #endregion

        #region -- Contructor --
        public TrongTaiController
            (
                IVuViecService vuViecService
                , ITrongTaiService trongTaiService
                , IOptionLineService optionLineService
                , IClassService classService
                , IUserService userService
                , IUserThuocNhomService userThuocNhomService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _trongTaiService = trongTaiService;
            _optionLineService = optionLineService;
            _classService = classService;
            _userService = userService;
            _userThuocNhomService = userThuocNhomService;
        }
        #endregion

        public ActionResult Index(int? id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            if (id == 5)
            {
                var teacherList = Mapper.Map<IEnumerable<TrongTaiCard>>(_trongTaiService.FindAll());
                var techerList = teacherList.Where(d => d.UserName.StartsWith("V"));
                foreach (var item in teacherList)
                {
                    CreateUser(item);
                }
            }
            return View();
        }
        public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            trongTaiCard = new TrongTaiCard();
            trongTaiCard.TrongTaiList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("TRONGTAI")).ToSelectListItems("");
            trongTaiCard.ClassList = Mapper.Map<IEnumerable<VM_Class>>(_classService.FindAll()).ToList().ToSelectListItems(0);
            return View(trongTaiCard);
        }

        [HttpPost]
        public ActionResult Create(TrongTaiCard _trongTaiCard)
        {
            try
            {
                SetData(trongTaiCard, true);
                hasValue = _trongTaiService.Insert(tbTrongTai);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                {
                    tbTrongTai.Id = _trongTaiService.GetMax();
                    return RedirectToAction("Edit", new { id = tbTrongTai.Id, msg = "1" });
                }
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        public ActionResult Edit(int id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            trongTaiCard = Mapper.Map<TrongTaiCard>(_trongTaiService.GetBy(id));
            if (trongTaiCard == null)
                return HttpNotFound();
            trongTaiCard.TrongTaiList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("TRONGTAI")).ToSelectListItems(trongTaiCard.TrongTaiCode);
            trongTaiCard.ClassList = Mapper.Map<IEnumerable<VM_Class>>(_classService.FindAll()).ToList().ToSelectListItems(trongTaiCard.ClassId);
            return View(trongTaiCard);
        }

        [HttpPost]
        public ActionResult Edit(TrongTaiCard _trongTaiCard)
        {
            try
            {
                SetData(_trongTaiCard, false);
                hasValue = _trongTaiService.Update(tbTrongTai);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = tbTrongTai.Id, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public JsonResult LoadData(GridSettings grid)
        {
            var list = Mapper.Map<IEnumerable<TrongTaiList>>(_trongTaiService.FindAll()).AsQueryable();
            list = JqGrid.SetupGrid<TrongTaiList>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
        private void SetData(TrongTaiCard _trongTaiCard, bool isCreate)
        {
            trongTaiCard = _trongTaiCard;
            var trongTaiInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("TRONGTAI", _trongTaiCard.TrongTaiCode)) ?? new VM_OptionLine();
            trongTaiCard.TrongTaiCode = trongTaiInfo.Code;
            trongTaiCard.TrongTaiName = trongTaiInfo.Name;
            var classInfo = Mapper.Map<VM_Class>(_classService.GetBy(trongTaiCard.ClassId)) ?? new VM_Class();
            trongTaiCard.ClassName = classInfo.Name;
            tbTrongTai = Mapper.Map<TB_TrongTai>(trongTaiCard);
        }
        public string CreateUser(TrongTaiCard trongTaiCard)
        {
            try
            {
                var userInfo = Mapper.Map<UserCard>(_userService.GetBy(trongTaiCard.UserName));
                if (userInfo != null)
                    return "";
                var userCard = new UserCard()
                {
                    ID = _userService.GetID(),
                    Password = CryptorEngine.Encrypt(trongTaiCard.Password, true, "Hieu.Le-GC.Soft"),
                    Username = trongTaiCard.UserName,
                    Email = trongTaiCard.Email,
                    Phone = trongTaiCard.Phone,
                    Description = "Giáo viên chấm bài",
                    Position = trongTaiCard.Rule,
                    FirstName = "",
                    LastName = "",
                    FullName = trongTaiCard.FullName,
                    Address = ""
                };
                hasValue = _userService.Insert2(Mapper.Map<HT_Users>(userCard));
                hasValue = _vuViecService.Commit();
                userInfo = Mapper.Map<UserCard>(_userService.GetBy(userCard.Username));
                var _userThuocNhom = new UserThuocNhomVM()
                {
                    UserID = userInfo.ID,
                    UserGroupCode = trongTaiCard.Rule
                };
                hasValue = _userThuocNhomService.Insert2(Mapper.Map<HT_UserThuocNhom>(_userThuocNhom));
                hasValue = _vuViecService.Commit();
                return hasValue;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
    }
}
