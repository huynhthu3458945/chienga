﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.Web.Areas.NamPhutThuocBai.Data
{
    public class ContestantsList
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string IdentificationNumber { get; set; }
        public string ChildName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
    public class ContestantsCard
    {
        public int Id { get; set; }
        public bool IsAccountSTNHD { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string IdentificationNumber { get; set; }
        public string ParentName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string ChildName { get; set; }
        public string SchoolName { get; set; }
        public int GradeId { get; set; }
        public string GradeName { get; set; }
        public int CityId { get; set; }
        public string CityName { get; set; }
        public int DistrictId { get; set; }
        public string DistrictName { get; set; }
        public string Address { get; set; }
        public DateTime DateCreate { get; set; }
    }
}