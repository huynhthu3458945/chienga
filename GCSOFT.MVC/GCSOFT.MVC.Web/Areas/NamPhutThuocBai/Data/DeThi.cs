﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.Web.Areas.NamPhutThuocBai.Data
{
    public class DeThiList
    {
        public int Id { get; set; }
        [DisplayName("Tuần")]
        public string Code { get; set; }
        [DisplayName("Lớp")]
        public string ClassName { get; set; }
        public DateTime StartDate { get; set; }
        public string StartDateStr { get { return string.Format("{0:dd/MM/yyyy}", StartDate); } }
        public DateTime EndDate { get; set; }
        public string EndDateStr { get { return string.Format("{0:dd/MM/yyyy}", EndDate); } }
    }
    public class DeThiCard
    {
        public int Id { get; set; }
        public int ClassId { get; set; }
        public string ClassName { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }
    }
}