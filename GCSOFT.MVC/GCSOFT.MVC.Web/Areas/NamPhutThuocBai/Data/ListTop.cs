﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.NamPhutThuocBai.Data
{
    public class ListTop
    {
        [DisplayName("Số Báo Danh")]
        public string SBD { get; set; }
        [DisplayName("Họ Tên")]
        public string FullName { get; set; }
        [DisplayName("Lớp")]
        public string ClassName { get; set; }
        [DisplayName("Chấm Video")]
        public string IsChuyenMonStr { get; set; }
        [DisplayName("Tổng Điểm")]
        public int TongDiem { get; set; }
        [DisplayName("Tổng Video")]
        public int TongVideo { get; set; }
        [DisplayName("Tổng View")]
        public int TongView { get; set; }
        [DisplayName("Tổng Tương Tác")]
        public int TongTuongTac { get; set; }
        [DisplayName("Tổng Share")]
        public int TongShare { get; set; }
        [DisplayName("Tổng Comment")]
        public int TongComment { get; set; }
    }

    public class ListTopIndex
    {
        [DisplayName("Tuần")]
        public string CodeDethi { get; set; }
        public IEnumerable<SelectListItem> CodeDethiList { get; set; }
        public int Top { get; set; }
        public IEnumerable<ListTop> ListTop { get; set; }
    }
}