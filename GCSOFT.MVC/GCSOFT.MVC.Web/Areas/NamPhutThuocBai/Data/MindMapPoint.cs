﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.Web.Areas.NamPhutThuocBai.Data
{
    public class MindMapPoint
    {
        public int Id { get; set; }
        public int IdNopBai { get; set; }
        public string PointCode { get; set; }
        [DisplayName("Diễn Giải")]
        public string PointDescription { get; set; }
        [DisplayName("Điểm")]
        public int Point { get; set; }
        [DisplayName("Ghi Chú")]
        public string Remark { get; set; }
    }
}