﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.NamPhutThuocBai.Data
{
    public class NopBaiList
    {
        public string CodeDethi { get; set; }
        public int Id { get; set; }
        [DisplayName("Số Báo Danh")]
        public string SBD { get; set; }
        [DisplayName("Họ Tên Thí Sinh")]
        public string FullName { get; set; }
        [DisplayName("Lớp")]
        public string ClassName { get; set; }
        [DisplayName("Ngày Nộp Bài")]
        public DateTime DateInput { get; set; }
        [DisplayName("Ngày Nộp Bài")]
        public string DateInputStr { get { return string.Format("{0:dd/MM/yyyy}", DateInput); } }
        [DisplayName("Điểm Vòng 1")]
        public int TongDiemVong1 { get; set; }
        [DisplayName("TT bài thi")]
        public string StatusName { get; set; }
        [DisplayName("TT Vòng Loại 1")]
        public string Vong1StatusName { get; set; }
        [DisplayName("TT Chuyên Môn 1")]
        public string ChuyenMon1_StatusName { get; set; }
        [DisplayName("TT Chuyên Môn 2")]
        public string ChuyenMon2_StatusName { get; set; }
        [DisplayName("Tổng Điểm")]
        public int TongDiem { get; set; }
        public bool IsChuyenMon { get; set; }
        [DisplayName("Chuyên Môn Mindmap")]
        public string IsChuyenMonStr { get { return IsChuyenMon ? "Có" : "Không"; } }
        public bool IsChuyenMonVong1 { get; set; }
        [DisplayName("Chuyên Môn Vòng 1")]
        public string IsChuyenMonVong1Str { get { return IsChuyenMonVong1 ? "Có" : "Không"; } }
        public string FullNameTrongTaiVong1 { get; set; }
        public string FullNameTrongTaiChuyenMon1 { get; set; }
        public string FullNameTrongTaiChuyenMon2 { get; set; }
        [DisplayName("Lượt view")]
        public int LuotView { get; set; }
        [DisplayName("Lượt tương tác")]
        public int LuotInteract { get; set; }
        [DisplayName("Lượt Share")]
        public int LuotShare { get; set; }
        [DisplayName("Lượt Comment")]
        public int LuotComment { get; set; }
        public int ClassId { get; set; }
        public string NhaDtXuatSacName { get; set; }
        public string NhaDTTruyenCamHungName { get; set; }

        public int ChuyenMon1_Mindmap { get; set; }
        public int ChuyenMon2_Mindmap { get; set; }

    }
    public class NopBaiVong1
    {
        public string SBD { get; set; }
        public string FullName { get; set; }
        public string LinkFacebok { get; set; }
        public int ClassId { get; set; }
        public string ClassName { get; set; }
        public int LuotView { get; set; }
        public int LuotInteract { get; set; }
        public int LuotShare { get; set; }
        public int LuotComment { get; set; }
        public int IdTrongTaiVong1 { get; set; }
        public int TieuChi1 { get; set; }
        public int TieuChi2 { get; set; }
        public int TieuChi3 { get; set; }
        public int TieuChi4 { get; set; }
        public int TieuChi5 { get; set; }
        public bool IsChuyenMon { get; set; }
        public string Vong1StatusName { get; set; }
    }
    public class NopBaiChuyenMon1
    {
        public string SBD { get; set; }
        public string FullName { get; set; }
        public string LinkFacebok { get; set; }
        public int ChuyenMon1_NoiDung { get; set; }
        public int ChuyenMon1_Mindmap { get; set; }
        public int ChuyenMon1_TrinhBay { get; set; }
        public string ChuyenMon1_StatusName { get; set; }
    }
    public class NopBaiChuyenMon2
    {
        public string SBD { get; set; }
        public string FullName { get; set; }
        public string LinkFacebok { get; set; }
        public int ChuyenMon2_NoiDung { get; set; }
        public int ChuyenMon2_Mindmap { get; set; }
        public int ChuyenMon2_TrinhBay { get; set; }
        public string ChuyenMon2_StatusName { get; set; }
    }
    public class NopBaiCard
    {
        public int Id { get; set; }
        [DisplayName("Tên đăng nhập")]
        public string userName { get; set; }
        [DisplayName("Số báo danh")]
        public string SBD { get; set; }
        [DisplayName("Họ và tên")]
        public string FullName { get; set; }
        [DisplayName("Mã đề thi")]
        public string CodeDethi { get; set; }
        public int IdContestants { get; set; }
        [DisplayName("Đường dẫn facebook")]
        public string LinkFacebok { get; set; }
        [DisplayName("Lượt view")]
        public int LuotView { get; set; }
        [DisplayName("Lượt tương tác")]
        public int LuotInteract { get; set; }
        [DisplayName("Lượt Share")]
        public int LuotShare { get; set; }
        [DisplayName("Lượt Comment")]
        public int LuotComment { get; set; }
        [DisplayName("Tên trọng tài")]
        public int IdTrongTaiVong1 { get; set; }
        [DisplayName("Tên trọng tài")]
        public int IdTrongTaiChuyenMon1 { get; set; }
        [DisplayName("Tên trọng tài")]
        public int IdTrongTaiChuyenMon2 { get; set; }
        [DisplayName("Ngày nộp bài")]
        public DateTime DateInput { get; set; }
        [DisplayName("Tiêu chí 1")]
        public int TieuChi1 { get; set; }
        [DisplayName("Tiêu chí 2")]
        public int TieuChi2 { get; set; }
        [DisplayName("Tiêu chí 3")]
        public int TieuChi3 { get; set; }
        [DisplayName("Tiêu chí 4")]
        public int TieuChi4 { get; set; }
        [DisplayName("Tiêu chí 5")]
        public int TieuChi5 { get; set; }

        [DisplayName("Trình Bày 1")]
        public int ChuyenMon1_TrinhBay1 { get; set; }
        [DisplayName("Trình Bày 2")]
        public int ChuyenMon1_TrinhBay2 { get; set; }
        [DisplayName("Trình Bày 3")]
        public int ChuyenMon1_TrinhBay3 { get; set; }
        [DisplayName("Nội dung")]
        public int ChuyenMon1_NoiDung { get; set; }
        [DisplayName("Mindmap")]
        public int ChuyenMon1_Mindmap { get; set; }

        [DisplayName("Trình Bày 1")]
        public int ChuyenMon2_TrinhBay1 { get; set; }
        [DisplayName("Trình Bày 2")]
        public int ChuyenMon2_TrinhBay2 { get; set; }
        [DisplayName("Trình Bày 3")]
        public int ChuyenMon2_TrinhBay3 { get; set; }
        [DisplayName("Nội dung")]
        public int ChuyenMon2_NoiDung { get; set; }
        [DisplayName("Mindmap")]
        public int ChuyenMon2_Mindmap { get; set; }


        [DisplayName("Tổng điểm")]
        public int TongDiem { get; set; }
        [DisplayName("Điểm tích lũy")]
        public int Point { get; set; }
        [DisplayName("Trạng thái vòng 1")]
        public string Vong1StatusCode { get; set; }
        [DisplayName("Trạng thái vòng 1")]
        public string Vong1StatusName { get; set; }
        [DisplayName("Trạng thái")]
        public string StatusCode { get; set; }
        [DisplayName("Trạng thái")]
        public string StatusName { get; set; }
        public IEnumerable<SelectListItem> StatusList { get; set; }

        [DisplayName("Trạng thái chuyên môn 1")]
        public string ChuyenMon1_StatusCode { get; set; }
        [DisplayName("Trạng thái chuyên môn 1")]
        public string ChuyenMon1_StatusName { get; set; }
        public IEnumerable<SelectListItem> ChuyenMon1StatusList { get; set; }

        [DisplayName("Trạng thái chuyên môn 2")]
        public string ChuyenMon2_StatusCode { get; set; }
        [DisplayName("Trạng thái chuyên môn 2")]
        public string ChuyenMon2_StatusName { get; set; }
        public IEnumerable<SelectListItem> ChuyenMon2StatusList { get; set; }

        public IEnumerable<SelectListItem> TrongTaiVong1List { get; set; }
        public IEnumerable<SelectListItem> ChuyenMon1List { get; set; }
        public IEnumerable<SelectListItem> ChuyenMon2List { get; set; }
        [DisplayName("Chuyên Môn Mindmap")]
        public bool IsChuyenMon { get; set; }
        [DisplayName("Chuyên Môn Vòng 1")]
        public bool IsChuyenMonVong1 { get; set; }
        public string UserTrongTaiVong1 { get; set; }
        public string UserTrongTaiChuyenMon1 { get; set; }
        public string UserTrongTaiChuyenMon2 { get; set; }
        public int TongDiemVong1 { get; set; }
        [DisplayName("Ghi chú")]
        public string Remark1 { get; set; }
        [DisplayName("Nhận xét đã được thống nhất giữa hai trọng tài ")]
        public string Remark2 { get; set; }
        [DisplayName("Ghi chú")]
        public string Remark3 { get; set; }
        public int ClassId { get; set; }
        public string ClassName { get; set; }
        public string FullNameTrongTaiVong1 { get; set; }
        public string FullNameTrongTaiChuyenMon1 { get; set; }
        public string FullNameTrongTaiChuyenMon2 { get; set; }
        public string NhaDtXuatSacCode { get; set; }
        [DisplayName("Nhà Đào Tạo Xuất Sắc")]
        public string NhaDtXuatSacName { get; set; }
        public string NhaDTTruyenCamHungCode { get; set; }
        [DisplayName("Nhà Đào Tạo Truyển Cảm Hứng")]
        public string NhaDTTruyenCamHungName { get; set; }
        public IEnumerable<MindMapPoint> MindMapPoints { get; set; }
    }
}