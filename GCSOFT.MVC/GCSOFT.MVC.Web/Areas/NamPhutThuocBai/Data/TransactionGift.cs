﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.NamPhutThuocBai.Data
{
    public class TransGiftList
    {
        [DisplayName("Số phiếu đổi quà")]
        public string Code { get; set; }
        [DisplayName("Họ và tên phụ huynh")]
        public string FullName { get; set; }
        [DisplayName("Số điện thoại")]
        public string PhoneNo { get; set; }
        [DisplayName("Email")]
        public string Email { get; set; }
        [DisplayName("Ngày gửi yêu cầu")]
        public DateTime DateCreate { get; set; }
        [DisplayName("Trạng thái")]
        public string StatusName { get; set; }
    }
    public class TransactionGift
    {
        [DisplayName("Số phiếu đổi quà")]
        public string Code { get; set; }
        public string userName { get; set; }
        [DisplayName("Họ và tên phụ huynh")]
        public string FullName { get; set; }
        [DisplayName("Địa chỉ nhận quà")]
        public string Address { get; set; }
        [DisplayName("Số điện thoại")]
        public string PhoneNo { get; set; }
        [DisplayName("Email")]
        public string Email { get; set; }
        public string GiftCode { get; set; }
        [DisplayName("Quà nhận")]
        public string GiftName { get; set; }
        public DateTime DateCreate { get; set; }
        [DisplayName("Ngày gửi yêu cầu")]
        public string DateTimeStr { get { return string.Format("{0:dd/MM/yyyy hh:mm}", DateCreate); } }
        public DateTime LastDateUpdate { get; set; }
        [DisplayName("Ngày phản hồi sau cùng")]
        public string LastDateUpdateStr { get { return string.Format("{0:dd/MM/yyyy hh:mm}", LastDateUpdate); } }
        [DisplayName("Phụ huynh ghi chú")]
        public string Remark { get; set; }
        [DisplayName("Trạng thái")]
        public string StatusCode { get; set; }
        [DisplayName("Trạng thái")]
        public string StatusName { get; set; }
        [DisplayName("Số điểm quy đổi")]
        public int Point { get; set; }
        [DisplayName("Phản hồi chăm sóc khách hàng")]
        public string HistoryTransfer { get; set; }
        public string ModifyBy { get; set; }
        public IEnumerable<SelectListItem> StatusList { get; set; }
    }
}