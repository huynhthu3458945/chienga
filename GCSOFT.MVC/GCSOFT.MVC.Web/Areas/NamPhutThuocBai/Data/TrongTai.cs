﻿using GCSOFT.MVC.Model.NamPhutThuocBai;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.NamPhutThuocBai.Data
{
    public class TrongTaiList
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string ClassName { get; set; }
        public string TrongTaiName { get; set; }
    }
    public class TrongTaiCard
    {
        public int Id { get; set; }
        public string TrongTaiCode { get; set; }
        public string TrongTaiName { get; set; }
        [DisplayName("Họ và tên Giáo Viên")]
        public string FullName { get; set; }
        [DisplayName("Số điện thoại")]
        public string Phone { get; set; }
        [DisplayName("Email")]
        public string Email { get; set; }
        [DisplayName("Tên đăng nhập")]
        public string UserName { get; set; }
        [DisplayName("Lớp")]
        public int ClassId { get; set; }
        public string ClassName { get; set; }
        [DisplayName("Đường Dẫn Google Drive")]
        public string GoogleDrive { get; set; }
        public string Password { get; set; }
        public string Rule { get; set; }
        public string ParentUserName { get; set; }
        public string Group { get; set; }
        public IEnumerable<SelectListItem> TrongTaiList { get; set; }
        public IEnumerable<SelectListItem> ClassList { get; set; }
    }
}