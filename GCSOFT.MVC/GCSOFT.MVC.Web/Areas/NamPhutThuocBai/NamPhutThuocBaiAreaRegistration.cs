﻿using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.NamPhutThuocBai
{
    public class NamPhutThuocBaiAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "NamPhutThuocBai";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "NamPhutThuocBai_default",
                "NamPhutThuocBai/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}