﻿using AutoMapper;
using Dapper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.StoreProcedue;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.ExeclOfficeEngine;
using GCSOFT.MVC.Service.StoreProcedure.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Areas.Agency.Data;
using GCSOFT.MVC.Web.Areas.Report.Data;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.Report.Controllers
{
    [CompressResponseAttribute]
    public class AgencyBuyGeneralController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IStoreProcedureService _storeService;
        private readonly IAgencyService _agencyService;
        private string sysCategory = "RPT_AGENCY_GENE";
        private bool? permission = false;
        private string hasValue;
        private string connection = ConfigurationManager.ConnectionStrings["GCConnection"].ConnectionString;
        private IEnumerable<AgencyBuyGeneral> agencyBuyInfoList;
        #endregion

        #region -- Contructor --
        public AgencyBuyGeneralController
            (
                IVuViecService vuViecService
                , IStoreProcedureService storeService
                , IAgencyService agencyService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _storeService = storeService;
            _agencyService = agencyService;
        }
        #endregion
        public ActionResult Index2()
        {
            return View();
        }
        public ActionResult Index(int? p, AgencyBuyGeneralData c)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            try
            {
                var agencyInfo = c;

                var fromDateBuy = new DateTime();
                var toDateBuy = new DateTime();
                var fromDateActive = new DateTime();
                var toDateActive = new DateTime();
                if (string.IsNullOrEmpty(c.fdBuy))
                {
                    fromDateBuy = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    toDateBuy = fromDateBuy.AddMonths(1).AddDays(-1);

                    fromDateActive = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    toDateActive = fromDateActive.AddMonths(1).AddDays(-1);

                    agencyInfo.Paging = new Paging();
                    agencyInfo.agencyBuyGeneral = new List<AgencyBuyGeneral>();
                }
                else
                {
                    fromDateBuy = DateTime.Parse(StringUtil.ConvertStringToDate(c.fdBuy));
                    toDateBuy = DateTime.Parse(StringUtil.ConvertStringToDate(c.tdBuy));

                    fromDateActive = DateTime.Parse(StringUtil.ConvertStringToDate(c.fdActive));
                    toDateActive = DateTime.Parse(StringUtil.ConvertStringToDate(c.tdActive));

                    int pageString = p ?? 0;
                    int page = pageString == 0 ? 1 : pageString;

                    int totalRecord = 0;

                    using (var conn = new SqlConnection(connection))
                    {
                        if (conn.State == System.Data.ConnectionState.Closed)
                            conn.Open();
                        var paramaters = new DynamicParameters();
                        paramaters.Add("@AgencyId", 0);
                        paramaters.Add("@FromDateBuy", fromDateBuy);
                        paramaters.Add("@ToDateBuy", toDateBuy);
                        paramaters.Add("@FromDateActive", fromDateActive);
                        paramaters.Add("@ToDateActive", toDateActive);
                        paramaters.Add("@searchAgencyId", c.searchAgencyId ?? 0);
                        paramaters.Add("@searchPhone", c.searchPhone ?? "");
                        paramaters.Add("@searchEmail", c.searchEmail ?? "");
                        paramaters.Add("@GroupBy", c.GroupBy ?? "");
                        paramaters.Add("@SortBy", c.SortBy ?? "");
                        paramaters.Add("@Start", 0);
                        paramaters.Add("@Offset", 0);

                        agencyBuyInfoList = conn.Query<AgencyBuyGeneral>("SP_RPT_AgencyBuyGeneralDiffDate", paramaters, null, true, 60, System.Data.CommandType.StoredProcedure);
                        totalRecord = agencyBuyInfoList.Count();
                        agencyInfo.Paging = new Paging(totalRecord, page);
                    }

                    // totalRecord = _storeService.SP_RPT_AgencyBuyGeneralCount(0, DateTime.Now, DateTime.Now, c.searchAgencyId ?? 0, c.searchPhone ?? "", c.searchEmail ?? "", c.GroupBy ?? "", c.SortBy ?? "").FirstOrDefault().TotalRecord;

                    //var agencyBuyInfoList = _storeService.SP_RPT_AgencyBuyGeneralDiffDate(0, fromDateBuy, toDateBuy, fromDateActive, toDateActive, c.searchAgencyId ?? 0, c.searchPhone ?? "", c.searchEmail ?? "", c.GroupBy ?? "", c.SortBy ?? "", 0, 0);
                    //int totalRecord = agencyBuyInfoList.Count();
                    //agencyInfo.Paging = new Paging(totalRecord, page);

                    switch (c.SortBy)
                    {
                        case "100":
                            agencyInfo.agencyBuyGeneral = agencyBuyInfoList.OrderByDescending(d => d.Total_VIP).Skip(agencyInfo.Paging.start).Take(agencyInfo.Paging.offset);
                            break;
                        case "200":
                            agencyInfo.agencyBuyGeneral = agencyBuyInfoList.OrderByDescending(d => d.Total_LEVEL).Skip(agencyInfo.Paging.start).Take(agencyInfo.Paging.offset);
                            break;
                        case "300":
                            agencyInfo.agencyBuyGeneral = agencyBuyInfoList.OrderByDescending(d => d.Total_GRADE).Skip(agencyInfo.Paging.start).Take(agencyInfo.Paging.offset);
                            break;
                        case "400":
                            agencyInfo.agencyBuyGeneral = agencyBuyInfoList.OrderByDescending(d => d.CardBuy_VIP).Skip(agencyInfo.Paging.start).Take(agencyInfo.Paging.offset);
                            break;
                        case "500":
                            agencyInfo.agencyBuyGeneral = agencyBuyInfoList.OrderByDescending(d => d.CardBuy_LEVEL).Skip(agencyInfo.Paging.start).Take(agencyInfo.Paging.offset);
                            break;
                        case "600":
                            agencyInfo.agencyBuyGeneral = agencyBuyInfoList.OrderByDescending(d => d.CardBuy_GRADE).Skip(agencyInfo.Paging.start).Take(agencyInfo.Paging.offset);
                            break;
                        case "700":
                            agencyInfo.agencyBuyGeneral = agencyBuyInfoList.OrderByDescending(d => d.CardActive_VIP).Skip(agencyInfo.Paging.start).Take(agencyInfo.Paging.offset);
                            break;
                        case "800":
                            agencyInfo.agencyBuyGeneral = agencyBuyInfoList.OrderByDescending(d => d.CardActive_LEVEL).Skip(agencyInfo.Paging.start).Take(agencyInfo.Paging.offset);
                            break;
                        case "900":
                            agencyInfo.agencyBuyGeneral = agencyBuyInfoList.OrderByDescending(d => d.CardActive_GRADE).Skip(agencyInfo.Paging.start).Take(agencyInfo.Paging.offset);
                            break;
                        default:
                            agencyInfo.agencyBuyGeneral = agencyBuyInfoList.Skip(agencyInfo.Paging.start).Take(agencyInfo.Paging.offset);
                            break;
                    }

                    agencyInfo.TotalVIP = agencyBuyInfoList.Sum(d => d.Total_VIP);
                    agencyInfo.TotalLevel = agencyBuyInfoList.Sum(d => d.Total_LEVEL);
                    agencyInfo.TotalGrade = agencyBuyInfoList.Sum(d => d.Total_GRADE);

                    agencyInfo.BuyVIP = agencyBuyInfoList.Sum(d => d.CardBuy_VIP);
                    agencyInfo.BuyLevel = agencyBuyInfoList.Sum(d => d.CardBuy_LEVEL);
                    agencyInfo.BuyGrade = agencyBuyInfoList.Sum(d => d.CardBuy_GRADE);

                    agencyInfo.ActiveVIP = agencyBuyInfoList.Sum(d => d.CardActive_VIP);
                    agencyInfo.ActiveLevel = agencyBuyInfoList.Sum(d => d.CardActive_LEVEL);
                    agencyInfo.ActiveGrade = agencyBuyInfoList.Sum(d => d.CardActive_GRADE);

                    agencyInfo.BuyAllVIP = agencyBuyInfoList.Sum(d => d.CardBuyAll_VIP);
                    agencyInfo.BuyAllLevel = agencyBuyInfoList.Sum(d => d.CardBuyAll_LEVEL);
                    agencyInfo.BuyAllGrade = agencyBuyInfoList.Sum(d => d.CardBuyAll_GRADE);
                }
                agencyInfo.FromDateBuy = fromDateBuy;
                agencyInfo.ToDateBuy = toDateBuy;
                agencyInfo.FromDateActive = fromDateActive;
                agencyInfo.ToDateActive = toDateActive;
                agencyInfo.AgencyList = Mapper.Map<IEnumerable<AgencyList>>(_agencyService.FindAllByAgency(null)).ToSelectListItems(c.searchAgencyId ?? 0);
                agencyInfo.SortByList = CboSortByList().ToSelectListItems(c.SortBy);
                return View(agencyInfo);
            }
            catch (Exception ex)
            {
                var error = ex.ToString();
                ViewBag.Error = ex;
                return View("Error");
            }

        }
        public Dictionary<string, string> CboSortByList()
        {
            Dictionary<string, string> dics = new Dictionary<string, string>();
            dics["100"] = "Thẻ VIP - Nhiều Nhất";
            dics["200"] = "Thẻ Cấp - Nhiều Nhất";
            dics["300"] = "Thẻ Lớp - Nhiều Nhất";

            dics["400"] = "Thẻ VIP Bán - Nhiều Nhất";
            dics["500"] = "Thẻ Cấp Bán - Nhiều Nhất";
            dics["600"] = "Thẻ Lớp Bán - Nhiều Nhất";
            
            dics["700"] = "Thẻ VIP Kích Hoạt - Nhiều Nhất";
            dics["800"] = "Thẻ Cấp Kích Hoạt - Nhiều Nhất";
            dics["900"] = "Thẻ Lớp Kích Hoạt - Nhiều Nhất";
            return dics;
        }
        public ActionResult DownloadExcel(int? p, AgencyBuyGeneralData c)
        {
            var agencyInfo = c;

            var fromDateBuy = new DateTime();
            var toDateBuy = new DateTime();
            var fromDateActive = new DateTime();
            var toDateActive = new DateTime();
            if (string.IsNullOrEmpty(c.fdBuy))
            {
                fromDateBuy = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                toDateBuy = fromDateBuy.AddMonths(1).AddDays(-1);

                fromDateActive = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                toDateActive = fromDateActive.AddMonths(1).AddDays(-1);

                agencyInfo.Paging = new Paging();
                agencyInfo.agencyBuyGeneral = new List<AgencyBuyGeneral>();
            }
            else
            {
                fromDateBuy = DateTime.Parse(StringUtil.ConvertStringToDate(c.fdBuy));
                toDateBuy = DateTime.Parse(StringUtil.ConvertStringToDate(c.tdBuy));

                fromDateActive = DateTime.Parse(StringUtil.ConvertStringToDate(c.fdActive));
                toDateActive = DateTime.Parse(StringUtil.ConvertStringToDate(c.tdActive));

                int pageString = p ?? 0;
                int page = pageString == 0 ? 1 : pageString;
                using (var conn = new SqlConnection(connection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@AgencyId", 0);
                    paramaters.Add("@FromDateBuy", fromDateBuy);
                    paramaters.Add("@ToDateBuy", toDateBuy);
                    paramaters.Add("@FromDateActive", fromDateActive);
                    paramaters.Add("@ToDateActive", toDateActive);
                    paramaters.Add("@searchAgencyId", c.searchAgencyId ?? 0);
                    paramaters.Add("@searchPhone", c.searchPhone ?? "");
                    paramaters.Add("@searchEmail", c.searchEmail ?? "");
                    paramaters.Add("@GroupBy", c.GroupBy ?? "");
                    paramaters.Add("@SortBy", c.SortBy ?? "");
                    paramaters.Add("@Start", 0);
                    paramaters.Add("@Offset", 0);

                    agencyBuyInfoList = conn.Query<AgencyBuyGeneral>("SP_RPT_AgencyBuyGeneralDiffDate", paramaters, null, true, 60, System.Data.CommandType.StoredProcedure);
                }

                //var agencyBuyInfoList = _storeService.SP_RPT_AgencyBuyGeneralDiffDate(0, fromDateBuy, toDateBuy, fromDateActive, toDateActive, c.searchAgencyId ?? 0, c.searchPhone ?? "", c.searchEmail ?? "", c.GroupBy ?? "", c.SortBy ?? "", 0, 0);

                switch (c.SortBy)
                {
                    case "100":
                        agencyInfo.agencyBuyGeneral = agencyBuyInfoList.OrderByDescending(d => d.Total_VIP);
                        break;
                    case "200":
                        agencyInfo.agencyBuyGeneral = agencyBuyInfoList.OrderByDescending(d => d.Total_LEVEL);
                        break;
                    case "300":
                        agencyInfo.agencyBuyGeneral = agencyBuyInfoList.OrderByDescending(d => d.Total_GRADE);
                        break;
                    case "400":
                        agencyInfo.agencyBuyGeneral = agencyBuyInfoList.OrderByDescending(d => d.CardBuy_VIP);
                        break;
                    case "500":
                        agencyInfo.agencyBuyGeneral = agencyBuyInfoList.OrderByDescending(d => d.CardBuy_LEVEL);
                        break;
                    case "600":
                        agencyInfo.agencyBuyGeneral = agencyBuyInfoList.OrderByDescending(d => d.CardBuy_GRADE);
                        break;
                    case "700":
                        agencyInfo.agencyBuyGeneral = agencyBuyInfoList.OrderByDescending(d => d.CardActive_VIP);
                        break;
                    case "800":
                        agencyInfo.agencyBuyGeneral = agencyBuyInfoList.OrderByDescending(d => d.CardActive_LEVEL);
                        break;
                    case "900":
                        agencyInfo.agencyBuyGeneral = agencyBuyInfoList.OrderByDescending(d => d.CardActive_GRADE);
                        break;
                    default:
                        agencyInfo.agencyBuyGeneral = agencyBuyInfoList;
                        break;
                }
                //foreach (var item in agencyInfo.agencyBuyGeneral)
                //{
                //    item.FullName = $"{item.FullName}\n- ĐT:{item.Phone}\n- Quận/Huyện:{item.DistrictName}\n- Tỉnh/Thành Phố:{item.CityName}";
                //}
                agencyInfo.TotalVIP = agencyBuyInfoList.Sum(d => d.Total_VIP);
                agencyInfo.TotalLevel = agencyBuyInfoList.Sum(d => d.Total_LEVEL);
                agencyInfo.TotalGrade = agencyBuyInfoList.Sum(d => d.Total_GRADE);

                agencyInfo.BuyVIP = agencyBuyInfoList.Sum(d => d.CardBuy_VIP);
                agencyInfo.BuyLevel = agencyBuyInfoList.Sum(d => d.CardBuy_LEVEL);
                agencyInfo.BuyGrade = agencyBuyInfoList.Sum(d => d.CardBuy_GRADE);

                agencyInfo.ActiveVIP = agencyBuyInfoList.Sum(d => d.CardActive_VIP);
                agencyInfo.ActiveLevel = agencyBuyInfoList.Sum(d => d.CardActive_LEVEL);
                agencyInfo.ActiveGrade = agencyBuyInfoList.Sum(d => d.CardActive_GRADE);

                agencyInfo.BuyAllVIP = agencyBuyInfoList.Sum(d => d.CardBuyAll_VIP);
                agencyInfo.BuyAllLevel = agencyBuyInfoList.Sum(d => d.CardBuyAll_LEVEL);
                agencyInfo.BuyAllGrade = agencyBuyInfoList.Sum(d => d.CardBuyAll_GRADE);

                agencyInfo.TotalBuyAllVIP = agencyBuyInfoList.Sum(d => d.Total_VIP) - agencyBuyInfoList.Sum(d => d.CardBuyAll_VIP);
                agencyInfo.TotalBuyAllLevel = agencyBuyInfoList.Sum(d => d.Total_LEVEL) - agencyBuyInfoList.Sum(d => d.CardBuyAll_LEVEL);
                agencyInfo.TotalBuyAllGrade = agencyBuyInfoList.Sum(d => d.Total_GRADE) - agencyBuyInfoList.Sum(d => d.CardBuyAll_GRADE);

            }

            var res = agencyInfo.agencyBuyGeneral;
            var dataModel = res.ToList().ConvertToDataTable();
            dataModel.TableName = "Model";
            List<KeyValuePair<string, object>> excelItems = new List<KeyValuePair<string, object>>();
            excelItems.Add(new KeyValuePair<string, object>("TotalVIP", agencyInfo.TotalVIP));
            excelItems.Add(new KeyValuePair<string, object>("TotalLevel", agencyInfo.TotalLevel));
            excelItems.Add(new KeyValuePair<string, object>("TotalGrade", agencyInfo.TotalGrade));
            excelItems.Add(new KeyValuePair<string, object>("BuyVIP", agencyInfo.BuyVIP));
            excelItems.Add(new KeyValuePair<string, object>("BuyLevel", agencyInfo.BuyLevel));
            excelItems.Add(new KeyValuePair<string, object>("BuyGrade", agencyInfo.BuyGrade));
            excelItems.Add(new KeyValuePair<string, object>("ActiveVIP", agencyInfo.ActiveVIP));
            excelItems.Add(new KeyValuePair<string, object>("ActiveLevel", agencyInfo.ActiveLevel));
            excelItems.Add(new KeyValuePair<string, object>("ActiveGrade", agencyInfo.ActiveGrade));
            excelItems.Add(new KeyValuePair<string, object>("BuyAllVIP", agencyInfo.BuyAllVIP));
            excelItems.Add(new KeyValuePair<string, object>("BuyAllLevel", agencyInfo.BuyAllLevel));
            excelItems.Add(new KeyValuePair<string, object>("BuyAllGrade", agencyInfo.BuyAllGrade));
            excelItems.Add(new KeyValuePair<string, object>("TotalBuyAllVIP", agencyInfo.TotalBuyAllVIP));
            excelItems.Add(new KeyValuePair<string, object>("TotalBuyAllLevel", agencyInfo.TotalBuyAllLevel));
            excelItems.Add(new KeyValuePair<string, object>("TotalBuyAllGrade", agencyInfo.TotalBuyAllGrade));
            Excel.ExcelItems = excelItems;
            var stream = Excel.ExportReport(dataModel, HttpContext.Server.MapPath("/Areas/Report/Files/AgencyBuyGeneral.xlsx"), false, OfficeType.XLSX);
            return File(stream, OfficeContentType.XLSX, "AgencyBuyGeneral.xlsx");
        }
    }
}