﻿using AutoMapper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.StoreProcedue;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.ExeclOfficeEngine;
using GCSOFT.MVC.Service.StoreProcedure.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Areas.Agency.Data;
using GCSOFT.MVC.Web.Areas.Report.Data;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.Report.Controllers
{
    [CompressResponseAttribute]
    public class AgencyChildBuyGeneralController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IStoreProcedureService _storeService;
        private readonly IAgencyService _agencyService;
        private string sysCategory = "RPT_AGENCY_CHILD";
        private bool? permission = false;
        private string hasValue;
        #endregion

        #region -- Contructor --
        public AgencyChildBuyGeneralController
            (
                IVuViecService vuViecService
                , IStoreProcedureService storeService
                , IAgencyService agencyService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _storeService = storeService;
            _agencyService = agencyService;
        }
        #endregion
        public ActionResult Index(int? p, AgencyChildBuyGeneralData c)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            try
            {
                var agencyInfo = c;

                var fromDateBuy = new DateTime();
                var toDateBuy = new DateTime();
                var fromDateActive = new DateTime();
                var toDateActive = new DateTime();
                if (string.IsNullOrEmpty(c.fdBuy))
                {
                    fromDateBuy = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    toDateBuy = fromDateBuy.AddMonths(1).AddDays(-1);

                    fromDateActive = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    toDateActive = fromDateActive.AddMonths(1).AddDays(-1);

                    agencyInfo.Paging = new Paging();
                    agencyInfo.agencyChildBuyGeneral = new List<AgencyChildBuyGeneral>();
                }
                else
                {
                    fromDateBuy = DateTime.Parse(StringUtil.ConvertStringToDate(c.fdBuy));
                    toDateBuy = DateTime.Parse(StringUtil.ConvertStringToDate(c.tdBuy));

                    fromDateActive = DateTime.Parse(StringUtil.ConvertStringToDate(c.fdActive));
                    toDateActive = DateTime.Parse(StringUtil.ConvertStringToDate(c.tdActive));

                    int pageString = p ?? 0;
                    int page = pageString == 0 ? 1 : pageString;

                    var agencyBuyInfoList = _storeService.SP_RPT_AgencyChildBuyGeneral(c.AgencyId ?? 0, fromDateBuy, toDateBuy, fromDateActive, toDateActive, c.searchChildAgencyId ?? 0, 0, 0);
                    int totalRecord = agencyBuyInfoList.Count();
                    agencyInfo.Paging = new Paging(totalRecord, page);

                    agencyInfo.agencyChildBuyGeneral = agencyBuyInfoList.Skip(agencyInfo.Paging.start).Take(agencyInfo.Paging.offset);

                }
                agencyInfo.FromDateBuy = fromDateBuy;
                agencyInfo.ToDateBuy = toDateBuy;
                agencyInfo.FromDateActive = fromDateActive;
                agencyInfo.ToDateActive = toDateActive;
                agencyInfo.AgencyList = Mapper.Map<IEnumerable<AgencyList>>(_agencyService.FindAllByAgency(null)).ToSelectListItems(c.searchChildAgencyId ?? 0);
                agencyInfo.AgencyChildList = Mapper.Map<IEnumerable<AgencyList>>(_agencyService.FindAllByAgency(agencyInfo.AgencyId ?? 0)).ToSelectListItems(agencyInfo.searchChildAgencyId ?? 0);
                return View(agencyInfo);
            }
            catch (Exception ex)
            {
                var error = ex.ToString();
                ViewBag.Error = ex;
                return View("Error");
            }

        }
        public ActionResult DownloadExcel(int? p, AgencyChildBuyGeneralData c)
        {
            var agencyInfo = c;

            var fromDateBuy = new DateTime();
            var toDateBuy = new DateTime();
            var fromDateActive = new DateTime();
            var toDateActive = new DateTime();
            if (string.IsNullOrEmpty(c.fdBuy))
            {
                fromDateBuy = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                toDateBuy = fromDateBuy.AddMonths(1).AddDays(-1);

                fromDateActive = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                toDateActive = fromDateActive.AddMonths(1).AddDays(-1);

                agencyInfo.Paging = new Paging();
                agencyInfo.agencyChildBuyGeneral = new List<AgencyChildBuyGeneral>();
            }
            else
            {
                fromDateBuy = DateTime.Parse(StringUtil.ConvertStringToDate(c.fdBuy));
                toDateBuy = DateTime.Parse(StringUtil.ConvertStringToDate(c.tdBuy));

                fromDateActive = DateTime.Parse(StringUtil.ConvertStringToDate(c.fdActive));
                toDateActive = DateTime.Parse(StringUtil.ConvertStringToDate(c.tdActive));

                int pageString = p ?? 0;
                int page = pageString == 0 ? 1 : pageString;
                var agencyBuyInfoList = _storeService.SP_RPT_AgencyChildBuyGeneral(c.AgencyId ?? 0, fromDateBuy, toDateBuy, fromDateActive, toDateActive, c.searchChildAgencyId ?? 0, 0, 0);

                //foreach (var item in agencyInfo.agencyBuyGeneral)
                //{
                //    item.FullName = $"{item.FullName}\n- ĐT:{item.Phone}\n- Quận/Huyện:{item.DistrictName}\n- Tỉnh/Thành Phố:{item.CityName}";
                //}

                agencyInfo.agencyChildBuyGeneral = agencyBuyInfoList;

            }

            var res = agencyInfo.agencyChildBuyGeneral;
            var dataModel = res.ToList().ConvertToDataTable();
            dataModel.TableName = "Model";
            var stream = Excel.ExportReport(dataModel, HttpContext.Server.MapPath("/Areas/Report/Files/AgencyChidBuyGeneral.xlsx"), false, OfficeType.XLSX);
            return File(stream, OfficeContentType.XLSX, "AgencyChidBuyGeneral.xlsx");
        }
        public JsonResult FetchAgencys(int parentId)
        {
            return Json(Mapper.Map<IEnumerable<AgencyList>>(_agencyService.FindAllByAgency(parentId)).ToSelectListItems(parentId), JsonRequestBehavior.AllowGet);
        }

    }
}