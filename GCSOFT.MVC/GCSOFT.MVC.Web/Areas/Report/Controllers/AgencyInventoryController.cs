﻿using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Service.StoreProcedure.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Areas.Report.Data;
using GCSOFT.MVC.Web.Helper;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.Report.Controllers
{
    public class AgencyInventoryController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IStoreProcedureService _storeService;
        private string sysCategory = "INV_AGENCY";
        private bool? permission = false;
        private string hasValue;
        #endregion

        #region -- Contructor --
        public AgencyInventoryController
            (
                IVuViecService vuViecService
                , IStoreProcedureService storeService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _storeService = storeService;
        }
        #endregion
        // GET: Report/AgencyInventory
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var agencyInfo = new RptAgencyInfo();
            agencyInfo.FromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            agencyInfo.ToDate = agencyInfo.FromDate.AddMonths(1).AddDays(-1);
            agencyInfo.SortBy = LoadSortBy().ToSelectListItems("");
            agencyInfo.OrderBy = LoadOrderBy().ToSelectListItems("");
            return View(agencyInfo);
        }
        /// <summary>
        /// Load Agency Info
        /// </summary>
        /// <param name="fd">From Date</param>
        /// <param name="td">To Date</param>
        /// <param name="o">Order By</param>
        /// <param name="s">Sort By</param>
        /// <returns></returns>
        public ActionResult LoadAgencyInfo(string fd, string td, string o, string s)
        {
            var agencyInfo = LoadData(fd, td, o, s);
            return PartialView("_AgencyInfo", agencyInfo);
        }
        public void Download(string fd, string td, string o, string s)
        {
            var agencyInfo = LoadData(fd, td, o, s);
            ExcelPackage Ep = new ExcelPackage();
            ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("AgencyInfo");
            Sheet.Cells["A1:Q1"].Merge = true;
            Sheet.Cells["A1:Q1"].Value = "Báo Cáo Đại Lý";
            Sheet.Cells["A1:Q1"].Style.Font.Size = 14;
            Sheet.Cells["A1:Q1"].Style.Font.Bold = true;

            Sheet.Cells["A2:Q2"].Merge = true;
            Sheet.Cells["A2:Q2"].Value = string.Format("Từ Ngày {0} - Đến Ngày {1}", fd, td);

            Sheet.Cells["A3"].Value = "STT";
            Sheet.Cells["B3"].Value = "Tên Đại Lý";
            Sheet.Cells["C3"].Value = "Cấp";
            Sheet.Cells["D3"].Value = "Số Sao";

            Sheet.Cells["E3"].Value = "SL. Đại Lý";
            Sheet.Cells["F3"].Value = "SL. Cộng Tác Viên";
            Sheet.Cells["G3"].Value = "Tổng Đại Lý";

            Sheet.Cells["H3"].Value = "Số Thẻ Bán";
            Sheet.Cells["I3"].Value = "Số Thẻ Tặng";
            Sheet.Cells["J3"].Value = "Tổng Thẻ";
            
            Sheet.Cells["K3"].Value = "Đại Lý Đã Phân Phối";
            Sheet.Cells["L3"].Value = "% Phân Phối";

            Sheet.Cells["M3"].Value = "Đại Lý Đã Bán Mã Kích Hoạt";
            Sheet.Cells["N3"].Value = "Đại Lý Đăng Ký Thành Viên";
            Sheet.Cells["O3"].Value = "Tổng Thẻ Đại Lý Đã Bán";
            Sheet.Cells["P3"].Value = "% Đã Bán";
            Sheet.Cells["Q3"].Value = "Thẻ Đã Kích Hoạt";

            Sheet.Cells["A3:P3"].Style.Font.Bold = true;
            int row = 3;
            int stt = 0;
            double persentDMS = 0; 
            double persentSold = 0;
            foreach (var item in agencyInfo.agencyInfo)
            {
                if (item.AgencyDaBanDL == 0 || item.TotalCard == 0)
                    persentDMS = 0;
                else
                    persentDMS = ((double)item.AgencyDaBanDL / (double)item.TotalCard);
                if (item.TongTheDaBan == 0 || item.TotalCard == 0)
                    persentSold = 0;
                else
                    persentSold = ((double)item.TongTheDaBan / (double)item.TotalCard);

                ++row;
                Sheet.Cells[string.Format("A{0}", row)].Value = ++stt;
                Sheet.Cells[string.Format("B{0}", row)].Value = item.FullName;
                Sheet.Cells[string.Format("C{0}", row)].Value = item.LeavelName;
                Sheet.Cells[string.Format("D{0}", row)].Value = item.Rating;

                Sheet.Cells[string.Format("E{0}", row)].Value = item.NoOfAgency;
                Sheet.Cells[string.Format("F{0}", row)].Value = item.NoOfPartner;
                Sheet.Cells[string.Format("G{0}", row)].Value = item.NoOfAgency + item.NoOfPartner;

                Sheet.Cells[string.Format("H{0}", row)].Value = item.NoOfCard;
                Sheet.Cells[string.Format("I{0}", row)].Value = item.GiftCard;
                Sheet.Cells[string.Format("J{0}", row)].Value = item.TotalCard;

                Sheet.Cells[string.Format("K{0}", row)].Value = item.AgencyDaBanDL;
                Sheet.Cells[string.Format("L{0}", row)].Value = string.Format("{0:0.#%}", persentDMS);

                Sheet.Cells[string.Format("M{0}", row)].Value = item.BanMKH;
                Sheet.Cells[string.Format("N{0}", row)].Value = item.TuKichHoat;
                Sheet.Cells[string.Format("O{0}", row)].Value = item.TongTheDaBan;
                Sheet.Cells[string.Format("P{0}", row)].Value = string.Format("{0:0.#%}", persentSold);
                Sheet.Cells[string.Format("Q{0}", row)].Value = item.TheDaActive;

                Sheet.Cells[string.Format("F{0}:Q{0}", row)].Style.Numberformat.Format = "0:#,##0";
            }
            Sheet.Cells["A3:Q" + row].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            Sheet.Cells["A3:Q" + row].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            Sheet.Cells["A3:Q" + row].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            Sheet.Cells["A3:Q" + row].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            Sheet.Cells["A:AZ"].AutoFitColumns();
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment: filename=" + "AgencyInventory.xlsx");
            Response.BinaryWrite(Ep.GetAsByteArray());
            Response.End();
        }
        private RptAgencyInfo LoadData(string fd, string td, string o, string s)
        {
            var agencyInfo = new RptAgencyInfo();
            agencyInfo.FromDate = DateTime.Parse(StringUtil.ConvertStringToDate(fd));
            agencyInfo.ToDate = DateTime.Parse(StringUtil.ConvertStringToDate(td));
            agencyInfo.agencyInfo = _storeService.sp_rpt_agencyinfo(agencyInfo.FromDate, agencyInfo.ToDate);
            if (s.Equals("DESC"))
            {
                switch (o)
                {
                    case "TTDLDB":
                        agencyInfo.agencyInfo = agencyInfo.agencyInfo.OrderByDescending(d => d.TongTheDaBan).ToList();
                        break;
                    case "DLDKTV":
                        agencyInfo.agencyInfo = agencyInfo.agencyInfo.OrderByDescending(d => d.TuKichHoat).ToList();
                        break;
                    case "DLBMKH":
                        agencyInfo.agencyInfo = agencyInfo.agencyInfo.OrderByDescending(d => d.BanMKH).ToList();
                        break;
                    case "DDDPPT":
                        agencyInfo.agencyInfo = agencyInfo.agencyInfo.OrderByDescending(d => d.AgencyDaBanDL).ToList();
                        break;
                    case "TTDLDM":
                        agencyInfo.agencyInfo = agencyInfo.agencyInfo.OrderByDescending(d => d.TotalCard).ToList();
                        break;
                    case "SLTDLDT":
                        agencyInfo.agencyInfo = agencyInfo.agencyInfo.OrderByDescending(d => d.GiftCard).ToList();
                        break;
                    case "SLTDLDM":
                        agencyInfo.agencyInfo = agencyInfo.agencyInfo.OrderByDescending(d => d.NoOfCard).ToList();
                        break;
                    case "TTSCK":
                        agencyInfo.agencyInfo = agencyInfo.agencyInfo.OrderByDescending(d => d.TienSauCK).ToList();
                        break;
                    case "TTDLDCK":
                        agencyInfo.agencyInfo = agencyInfo.agencyInfo.OrderByDescending(d => d.DiscountAmt).ToList();
                        break;
                    case "TTCK":
                        agencyInfo.agencyInfo = agencyInfo.agencyInfo.OrderByDescending(d => d.TienTruocCK).ToList();
                        break;
                }
            }
            else
            {
                switch (o)
                {
                    case "TTDLDB":
                        agencyInfo.agencyInfo = agencyInfo.agencyInfo.OrderBy(d => d.TongTheDaBan).ToList();
                        break;
                    case "DLDKTV":
                        agencyInfo.agencyInfo = agencyInfo.agencyInfo.OrderBy(d => d.TuKichHoat).ToList();
                        break;
                    case "DLBMKH":
                        agencyInfo.agencyInfo = agencyInfo.agencyInfo.OrderBy(d => d.BanMKH).ToList();
                        break;
                    case "DDDPPT":
                        agencyInfo.agencyInfo = agencyInfo.agencyInfo.OrderBy(d => d.AgencyDaBanDL).ToList();
                        break;
                    case "TTDLDM":
                        agencyInfo.agencyInfo = agencyInfo.agencyInfo.OrderBy(d => d.TotalCard).ToList();
                        break;
                    case "SLTDLDT":
                        agencyInfo.agencyInfo = agencyInfo.agencyInfo.OrderBy(d => d.GiftCard).ToList();
                        break;
                    case "SLTDLDM":
                        agencyInfo.agencyInfo = agencyInfo.agencyInfo.OrderBy(d => d.NoOfCard).ToList();
                        break;
                    case "TTSCK":
                        agencyInfo.agencyInfo = agencyInfo.agencyInfo.OrderBy(d => d.TienSauCK).ToList();
                        break;
                    case "TTDLDCK":
                        agencyInfo.agencyInfo = agencyInfo.agencyInfo.OrderBy(d => d.DiscountAmt).ToList();
                        break;
                    case "TTCK":
                        agencyInfo.agencyInfo = agencyInfo.agencyInfo.OrderBy(d => d.TienTruocCK).ToList();
                        break;
                }
            }
            return agencyInfo;
        }
        private Dictionary<string, string> LoadSortBy()
        {
            var dicActivity = new Dictionary<string, string>();
            dicActivity.Add("TTDLDB", "Tổng Thẻ Đại Lý Đã Bán");
            dicActivity.Add("DLDKTV", "Đại Lý Đăng Ký Thành Viên");
            dicActivity.Add("DLBMKH", "Đại Lý Bán Mã Kích Hoạt");
            dicActivity.Add("DDDPPT", "Đại Lý Đã Phân Phối Thẻ");
            dicActivity.Add("TTDLDM", "Tổng Thẻ Mua + Tặng");
            dicActivity.Add("SLTDLDT", "Tổng Thẻ Tặng");
            dicActivity.Add("SLTDLDM", "Tổng Thẻ Mua");
            dicActivity.Add("TTSCK", "Tiền Sau Chiết Khấu");
            dicActivity.Add("TTDLDCK", "Tiền Chiết Khấu");
            dicActivity.Add("TTCK", "Tiền Trước Chiết Khấu");
            return dicActivity;
        }
        private Dictionary<string, string> LoadOrderBy()
        {
            var dicActivity = new Dictionary<string, string>();
            dicActivity.Add("DESC", "Giảm Dần");
            dicActivity.Add("ASC", "Tăng Dần");
            return dicActivity;
        }
    }
}