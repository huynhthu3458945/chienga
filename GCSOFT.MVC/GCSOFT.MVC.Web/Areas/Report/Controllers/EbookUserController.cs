﻿using AutoMapper;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.StoreProcedure.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Areas.MasterData.Models;
using GCSOFT.MVC.Web.Areas.Report.Data;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.Report.Controllers
{
    public class EbookUserController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IStoreProcedureService _storeService;
        private string sysCategory = "RPTEBOOKUSER";
        private bool? permission = false;
        private readonly IProvinceService _provinceService;
        private readonly IDistrictService _districtService;
        private string hasValue;
        #endregion

        #region -- Contructor --
        public EbookUserController
            (
                IVuViecService vuViecService
                , IStoreProcedureService storeService
                , IProvinceService provinceService
                , IDistrictService districtService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _storeService = storeService;
            _provinceService = provinceService;
            _districtService = districtService;
        }
        #endregion
        public ActionResult Index(int? p, int? cityId, int? districtId)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            int pageString = p ?? 0;
            int page = pageString == 0 ? 1 : pageString;
            var _EbookUserDetailByCities = _storeService.sp_rpt_EbookUserDetailByCity(cityId ?? 0, districtId ?? 0);
            int totalRecord = _EbookUserDetailByCities.Count();
            var rptEbookUser = new RptEbookUser();
            rptEbookUser.paging = new Paging(totalRecord, page);
            rptEbookUser.CityList = Mapper.Map<IEnumerable<ProvinceViewModel>>(_provinceService.FindAll()).ToSelectListItems(cityId ?? 0);
            rptEbookUser.DistrictList = Mapper.Map<IEnumerable<DistrictViewModel>>(_districtService.FindAll(cityId ?? 0)).ToSelectListItems(districtId ?? 0);
            rptEbookUser.EbookUserByCities = _storeService.sp_rpt_EbookUserByCity(cityId ?? 0, districtId ?? 0);
            rptEbookUser.EbookUserDetailByCities = _EbookUserDetailByCities.Skip(rptEbookUser.paging.start).Take(rptEbookUser.paging.offset);
            return View(rptEbookUser);
        }
        public ActionResult LoadEbookUser(int cityId, int districtId)
        {
            var rptEbookUser = new RptEbookUser()
            {
                CityList = Mapper.Map<IEnumerable<ProvinceViewModel>>(_provinceService.FindAll()).ToSelectListItems(cityId),
                DistrictList = Mapper.Map<IEnumerable<DistrictViewModel>>(_districtService.FindAll(0)).ToSelectListItems(districtId),
                EbookUserByCities = _storeService.sp_rpt_EbookUserByCity(cityId, districtId),
                EbookUserDetailByCities = _storeService.sp_rpt_EbookUserDetailByCity(cityId, districtId)
            };
            return PartialView("_EbookUser", rptEbookUser);
        }
    }
}