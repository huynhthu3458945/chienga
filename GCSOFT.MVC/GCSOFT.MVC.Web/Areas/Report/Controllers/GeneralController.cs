﻿using AutoMapper;
using Dapper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.StoreProcedue;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.StoreProcedure.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Areas.Report.Data;
using GCSOFT.MVC.Web.Areas.Transaction.Data;
using GCSOFT.MVC.Web.Helper;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.Report.Controllers
{
    public class GeneralController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IStoreProcedureService _storeService;
        private readonly ICustomerService _customerService;
        private readonly IAgencyCardService _agencyCardService;
        private string sysCategory = "RPT_GENERAL";
        private bool? permission = false;
        private string hasValue; 
        private string ptbConnection = ConfigurationManager.ConnectionStrings["GCConnection"].ConnectionString;
        #endregion

        #region -- Contructor --
        public GeneralController
            (
                IVuViecService vuViecService
                , IStoreProcedureService storeService
                , ICustomerService customerService
                , IAgencyCardService agencyCardService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _storeService = storeService;
            _customerService = customerService;
            _agencyCardService = agencyCardService;
        }
        #endregion
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var generalInfo = new GeneralData();
            generalInfo.FromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            generalInfo.ToDate = generalInfo.FromDate.AddMonths(1).AddDays(-1);
            generalInfo.LastFromDate = new DateTime(DateTime.Now.AddMonths(-1).Year, DateTime.Now.AddMonths(-1).Month, 1);
            generalInfo.LastToDate = generalInfo.LastFromDate.AddMonths(1).AddDays(-1);
            return View(generalInfo);
        }
        public ActionResult GeneralInfo(string fd, string td, string lfd, string ltd)
        {
            var generalData = new GeneralData();
            generalData.FromDate = DateTime.Parse(StringUtil.ConvertStringToDate(fd));
            generalData.ToDate = DateTime.Parse(StringUtil.ConvertStringToDate(td));
            generalData.LastFromDate = DateTime.Parse(StringUtil.ConvertStringToDate(lfd));
            generalData.LastToDate = DateTime.Parse(StringUtil.ConvertStringToDate(ltd));

            try
            {
                using (var conn = new SqlConnection(ptbConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();

                    paramaters.Add("@FromDate", generalData.FromDate);
                    paramaters.Add("@ToDate", generalData.ToDate);
                    paramaters.Add("@LastFromDate", generalData.LastFromDate);
                    paramaters.Add("@LastToDate", generalData.LastToDate);

                    generalData.generalInfo = conn.Query<SpGeneralInfo>("SP_RPT_GENERAL", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    generalData.ttlByCustDtl = conn.Query<SpTTLBuyCustomerDtl>("SP_RPT_TTLBUYCUSTOMERDTL", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    generalData.agencyLevelDtls = conn.Query<SpAgencyLevelDtl>("SP_RPT_AGENCYLEVELDTL", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }
            }
            catch
            {
                generalData.generalInfo = new List<SpGeneralInfo>();
                generalData.ttlByCustDtl = new List<SpTTLBuyCustomerDtl>();
                generalData.agencyLevelDtls = new List<SpAgencyLevelDtl>();
            }
            return PartialView("_GeneralInfo", generalData);

        }
        public void Download(string fd, string td, string lfd, string ltd)
        {
            var generalData = new GeneralData();
            generalData.FromDate = DateTime.Parse(StringUtil.ConvertStringToDate(fd));
            generalData.ToDate = DateTime.Parse(StringUtil.ConvertStringToDate(td));
            generalData.LastFromDate = DateTime.Parse(StringUtil.ConvertStringToDate(lfd));
            generalData.LastToDate = DateTime.Parse(StringUtil.ConvertStringToDate(ltd));
            try
            {
                using (var conn = new SqlConnection(ptbConnection))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();

                    paramaters.Add("@FromDate", generalData.FromDate);
                    paramaters.Add("@ToDate", generalData.ToDate);
                    paramaters.Add("@LastFromDate", generalData.LastFromDate);
                    paramaters.Add("@LastToDate", generalData.LastToDate);

                    generalData.generalInfo = conn.Query<SpGeneralInfo>("SP_RPT_GENERAL", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    generalData.ttlByCustDtl = conn.Query<SpTTLBuyCustomerDtl>("SP_RPT_TTLBUYCUSTOMERDTL", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                    generalData.agencyLevelDtls = conn.Query<SpAgencyLevelDtl>("SP_RPT_AGENCYLEVELDTL", paramaters, null, true, null, System.Data.CommandType.StoredProcedure);
                }
            }
            catch
            {
                generalData.generalInfo = new List<SpGeneralInfo>();
                generalData.ttlByCustDtl = new List<SpTTLBuyCustomerDtl>();
                generalData.agencyLevelDtls = new List<SpAgencyLevelDtl>();
            }
            ExcelPackage Ep = new ExcelPackage();
            ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("Thong Tin Chung");
            Sheet.Cells["A1:E1"].Merge = true;
            Sheet.Cells["A1:E1"].Value = "Thông Tin Chung";
            Sheet.Cells["A1:E1"].Style.Font.Size = 14;
            Sheet.Cells["A1:E1"].Style.Font.Bold = true;

            Sheet.Cells["A3"].Value = "STT";
            Sheet.Cells["B3"].Value = "Trạng Thái";
            Sheet.Cells["C3"].Value = string.Format("{0} - {1}", generalData.LastFromDateStr, generalData.LastToDateStr);
            Sheet.Cells["D3"].Value = string.Format("{0} - {1}", generalData.FromDateStr, generalData.ToDateStr);
            Sheet.Cells["E3"].Value = "Tổng";
            Sheet.Cells["F3"].Value = "TKVIP";
            Sheet.Cells["G3"].Value = "TKCAP";
            Sheet.Cells["H3"].Value = "TKLOP";

            Sheet.Cells["A3:H3"].Style.Font.Bold = true;
            int row = 3;
            int stt = 0;
            foreach (var item in generalData.generalInfo)
            {
                ++row;
                Sheet.Cells[string.Format("A{0}", row)].Value = ++stt;
                Sheet.Cells[string.Format("B{0}", row)].Value = item.RptName;
                
                Sheet.Cells[string.Format("C{0}", row)].Value = item.QtySamePeriod;
                Sheet.Cells[string.Format("C{0}", row)].Style.Numberformat.Format = "0:#,##0";
                
                Sheet.Cells[string.Format("D{0}", row)].Value = item.QtyPreriod;
                Sheet.Cells[string.Format("D{0}", row)].Style.Numberformat.Format = "0:#,##0";
                
                Sheet.Cells[string.Format("E{0}", row)].Value = item.QtyToday;
                Sheet.Cells[string.Format("E{0}", row)].Style.Numberformat.Format = "0:#,##0";

                Sheet.Cells[string.Format("F{0}", row)].Value = item.QtyPreriodVIP;
                Sheet.Cells[string.Format("F{0}", row)].Style.Numberformat.Format = "0:#,##0";

                Sheet.Cells[string.Format("G{0}", row)].Value = item.QtyPreriodCAP;
                Sheet.Cells[string.Format("G{0}", row)].Style.Numberformat.Format = "0:#,##0";

                Sheet.Cells[string.Format("H{0}", row)].Value = item.QtyPreriodLOP;
                Sheet.Cells[string.Format("H{0}", row)].Style.Numberformat.Format = "0:#,##0";
                if (item.Bold.Equals("Bold"))
                    Sheet.Cells[string.Format("A{0}:H{0}", row)].Style.Font.Bold = true;
            }
            Sheet.Cells["A3:H" + row].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            Sheet.Cells["A3:H" + row].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            Sheet.Cells["A3:H" + row].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            Sheet.Cells["A3:H" + row].Style.Border.Left.Style = ExcelBorderStyle.Thin;

            Sheet.Cells["J2:N2"].Merge = true;
            Sheet.Cells["J2:N2"].Value = "Chi Tiết TTL Cấp Khách Lẻ";
            Sheet.Cells["J2:N2"].Style.Font.Size = 14;
            Sheet.Cells["J2:N2"].Style.Font.Bold = true;

            Sheet.Cells["J3"].Value = "STT";
            Sheet.Cells["K3"].Value = "Loại";
            Sheet.Cells["L3"].Value = string.Format("{0} - {1}", generalData.LastFromDateStr, generalData.LastToDateStr);
            Sheet.Cells["M3"].Value = string.Format("{0} - {1}", generalData.FromDateStr, generalData.ToDateStr);
            Sheet.Cells["N3"].Value = "Tổng";
            Sheet.Cells["J3:N3"].Style.Font.Bold = true;
            row = 3;
            stt = 0;
            foreach (var item in generalData.ttlByCustDtl)
            {
                ++row;
                Sheet.Cells[string.Format("J{0}", row)].Value = ++stt;
                Sheet.Cells[string.Format("K{0}", row)].Value = item.ReasonName;

                Sheet.Cells[string.Format("L{0}", row)].Value = item.LastNoOfCard;
                Sheet.Cells[string.Format("L{0}", row)].Style.Numberformat.Format = "0:#,##0";

                Sheet.Cells[string.Format("M{0}", row)].Value = item.NoOfCard;
                Sheet.Cells[string.Format("M{0}", row)].Style.Numberformat.Format = "0:#,##0";

                Sheet.Cells[string.Format("N{0}", row)].Value = item.ToDayNoOfCard;
                Sheet.Cells[string.Format("N{0}", row)].Style.Numberformat.Format = "0:#,##0";
            }
            Sheet.Cells["J3:N" + row].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            Sheet.Cells["J3:N" + row].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            Sheet.Cells["J3:N" + row].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            Sheet.Cells["J3:N" + row].Style.Border.Left.Style = ExcelBorderStyle.Thin;

            ++row;
            int startRow = row;
            Sheet.Cells[string.Format("J{0}:N{0}", row)].Merge = true;
            Sheet.Cells[string.Format("J{0}:N{0}", row)].Value = "Chi Tiết Đại Lý";
            Sheet.Cells[string.Format("J{0}:N{0}", row)].Style.Font.Size = 14;
            Sheet.Cells[string.Format("J{0}:N{0}", row)].Style.Font.Bold = true;
            ++row; 
            Sheet.Cells[string.Format("J{0}", row)].Value = "STT";
            Sheet.Cells[string.Format("K{0}", row)].Value = "Cấp";
            Sheet.Cells[string.Format("L{0}", row)].Value = string.Format("{0} - {1}", generalData.LastFromDateStr, generalData.LastToDateStr);
            Sheet.Cells[string.Format("M{0}", row)].Value = string.Format("{0} - {1}", generalData.FromDateStr, generalData.ToDateStr);
            Sheet.Cells[string.Format("N{0}", row)].Value = "Tổng";
            Sheet.Cells[string.Format("J{0}:N{0}", row)].Style.Font.Bold = true;
            stt = 0;
            foreach (var item in generalData.agencyLevelDtls)
            {
                ++row;
                Sheet.Cells[string.Format("J{0}", row)].Value = ++stt;
                Sheet.Cells[string.Format("K{0}", row)].Value = item.LeavelName;
                Sheet.Cells[string.Format("L{0}", row)].Value = item.LastNoOfMember;
                Sheet.Cells[string.Format("L{0}", row)].Style.Numberformat.Format = "0:#,##0";
                Sheet.Cells[string.Format("M{0}", row)].Value = item.NoOfMember;
                Sheet.Cells[string.Format("M{0}", row)].Style.Numberformat.Format = "0:#,##0";
                Sheet.Cells[string.Format("N{0}", row)].Value = item.ToDayNoOfMember;
                Sheet.Cells[string.Format("N{0}", row)].Style.Numberformat.Format = "0:#,##0";
            }
            Sheet.Cells[string.Format("J{0}:N{1}", startRow, row)].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            Sheet.Cells[string.Format("J{0}:N{1}", startRow, row)].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            Sheet.Cells[string.Format("J{0}:N{1}", startRow, row)].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            Sheet.Cells[string.Format("J{0}:N{1}", startRow, row)].Style.Border.Left.Style = ExcelBorderStyle.Thin;

            Sheet.Cells["A:AZ"].AutoFitColumns();
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment: filename=" + "ThongTinChung.xlsx");
            Response.BinaryWrite(Ep.GetAsByteArray());
            Response.End();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ln">Lot Number</param>
        /// <param name="se">Status Email</param>
        /// <param name="fn">Full Name</param>
        /// <param name="p">Phone</param>
        /// <param name="e">Email</param>
        public void DownloadCardNo(string ln, string se, string fn, string p, string e)
        {
            var list = Mapper.Map<IEnumerable<CustomerViewModel>>(_customerService.FindAll(ln, se, fn, p, e));
            ExcelPackage Ep = new ExcelPackage();
            ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("Thong Tin");
            Sheet.Cells["A1"].Value = "Mã Học Viên";
            Sheet.Cells["B1"].Value = "Họ Tên";
            Sheet.Cells["C1"].Value = "Di Động";
            Sheet.Cells["D1"].Value = "Email";
            Sheet.Cells["E1"].Value = "Kiểm Tra Email";
            Sheet.Cells["F1"].Value = "Owner";
            Sheet.Cells["G1"].Value = "Số Seri";
            Sheet.Cells["H1"].Value = "Mã Kích Hoạt";
            int row = 2;
            foreach (var item in list)
            {
                Sheet.Cells[string.Format("A{0}", row)].Value = item.Code;
                Sheet.Cells[string.Format("B{0}", row)].Value = item.FullName;
                Sheet.Cells[string.Format("C{0}", row)].Value = item.PhoneNo;
                Sheet.Cells[string.Format("D{0}", row)].Value = item.Email;
                Sheet.Cells[string.Format("E{0}", row)].Value = item.IsEmail;
                Sheet.Cells[string.Format("F{0}", row)].Value = item.Owner;
                Sheet.Cells[string.Format("G{0}", row)].Value = item.SerialNumber;
                Sheet.Cells[string.Format("H{0}", row)].Value = CryptorEngine.Decrypt(item.CardNo, true, "TTLSTNHD@CARD");
                row++;
            }
            Sheet.Cells["A:AZ"].AutoFitColumns();
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment: filename=" + "ThongTinChung.xlsx");
            Response.BinaryWrite(Ep.GetAsByteArray());
            Response.End();
        }
        public void DownloadCardAgency(int agencyId)
        {
            var cards = _agencyCardService.FindAll(agencyId, null, null);
            ExcelPackage Ep = new ExcelPackage();
            ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("Thong Tin");
            Sheet.Cells["A1"].Value = "STT";
            Sheet.Cells["B1"].Value = "Số Seri";
            Sheet.Cells["C1"].Value = "Mã Kích Hoạt";
            Sheet.Cells["D1"].Value = "Giá";
            Sheet.Cells["E1"].Value = "Trạng Thái";
            Sheet.Cells["F1"].Value = "Ngày Cập Nhật";
            Sheet.Cells["G1"].Value = "Email";
            Sheet.Cells["H1"].Value = "Họ Tên";
            Sheet.Cells["I1"].Value = "Điện Thoại";
            int row = 2;
            int stt = 0;
            string _cardNo = "";
            foreach (var item in cards)
            {
                _cardNo = "";
                stt += 1;
                Sheet.Cells[string.Format("A{0}", row)].Value = stt;
                Sheet.Cells[string.Format("B{0}", row)].Value = item.SerialNumber;
                if (!string.IsNullOrEmpty(item.CardNo))
                {
                    _cardNo = CryptorEngine.Decrypt(item.CardNo, true, "TTLSTNHD@CARD");
                }
                Sheet.Cells[string.Format("C{0}", row)].Value = _cardNo;
                Sheet.Cells[string.Format("D{0}", row)].Value = item.Price;
                Sheet.Cells[string.Format("E{0}", row)].Value = item.StatusName;
                Sheet.Cells[string.Format("F{0}", row)].Value = string.Format("{0:dd/MM/yyyy}", item.LastDateUpdate);
                Sheet.Cells[string.Format("G{0}", row)].Value = item.VerifyInfo;
                Sheet.Cells[string.Format("H{0}", row)].Value = item.FulName;
                Sheet.Cells[string.Format("I{0}", row)].Value = item.PhoneNo;
                row++;
            }
            Sheet.Cells["A:AZ"].AutoFitColumns();
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment: filename=" + "CardInfo.xlsx");
            Response.BinaryWrite(Ep.GetAsByteArray());
            Response.End();
        }
    }
}