﻿using AutoMapper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.StoreProcedue;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.ExeclOfficeEngine;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.StoreProcedure.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Areas.Agency.Data;
using GCSOFT.MVC.Web.Areas.Report.Data;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.Report.Controllers
{
    [CompressResponseAttribute]
    public class ReportController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IStoreProcedureService _storeService;
        private readonly IAgencyService _agencyService;
        private readonly IOptionLineService _optionLineService;
        private readonly IAgencyCardService _agencyAndCardService;
        private readonly ICardLineService _cardLineService;
        private readonly ICardEntryService _cardEntryService;
        private readonly IHistoryRecall _historyRecall;
        private readonly IMailSetupService _mailSetupService;
        private bool? permission = false;
        private string sysCategory_DetailCustomer = "ADMIN_DETAILCUSTOMER";
        private string sysCategory_RetailCustomer = "ADMIN_RETAILCUSTOMER";
        #endregion

        #region -- Contructor --
        public ReportController
            (
                IVuViecService vuViecService
                , IStoreProcedureService storeService
                , IAgencyService agencyService
                , IOptionLineService optionLineService
                , IAgencyCardService agencyAndCardService
                , ICardLineService cardLineService
                , ICardEntryService cardEntryService
                , IHistoryRecall historyRecall
                , IMailSetupService mailSetupService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _storeService = storeService;
            _agencyService = agencyService;
            _optionLineService = optionLineService;
            _agencyAndCardService = agencyAndCardService;
            _cardLineService = cardLineService;
            _cardEntryService = cardEntryService;
            _historyRecall = historyRecall;
            _mailSetupService = mailSetupService;
        }
        #endregion


        /// <summary>
        ///     Chi tiết khách hàng
        /// </summary>
        /// <param name="p"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [15/11/2021]
        /// </history>
        public ActionResult DetailCustomer(int? p, DetailCustomerAgency c)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory_DetailCustomer, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var userInfo = GetUser();
            c.AgencyId = c.AgencyId ?? 0;
            var cardHistory = c;

            var fromDate = new DateTime();
            var toDate = new DateTime();
            if (string.IsNullOrEmpty(c.fd))
            {
                fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                toDate = fromDate.AddMonths(1).AddDays(-1);
                cardHistory.CustAgencyList = new List<SpAgencyDetailSales>();
                c.Paging = new Paging();
            }
            else
            {
                int pageString = p ?? 0;
                int page = pageString == 0 ? 1 : pageString;
                int totalRecord = 0;
                fromDate = DateTime.Parse(StringUtil.ConvertStringToDate(c.fd));
                toDate = DateTime.Parse(StringUtil.ConvertStringToDate(c.td));
                totalRecord = _storeService.SP_RPT_AgencyDetailSalesCount("COUNT.RECORD", c.AgencyId ?? 0, c.ViewBy ?? "", fromDate, toDate, 0, c.CardNo ?? "", c.LevelCode ?? "", c.SeriNumber ?? "", c.FullName ?? "", c.PhoneNo ?? "", c.Email ?? "", c.CardStatus ?? "", 0, 0).FirstOrDefault().NoOfNumber;
                cardHistory.Paging = new Paging(totalRecord, page);
                cardHistory.CustAgencyList = _storeService.SP_RPT_AgencyDetailSales("PAGING", c.AgencyId ?? 0, c.ViewBy ?? "", fromDate, toDate, 0, c.CardNo ?? "", c.LevelCode ?? "", c.SeriNumber ?? "", c.FullName ?? "", c.PhoneNo ?? "", c.Email ?? "", c.CardStatus ?? "", cardHistory.Paging.start, cardHistory.Paging.offset);
            }
            foreach (var item in cardHistory.CustAgencyList)
            {
                item.DoiTacTuyenTren = $"- {item.ParentAgencyName}\n- {item.AgencyTree}";
                item.PriceStr = string.Format("{0:#,##0}", item.Price);
            }
            cardHistory.FromDate = fromDate;
            cardHistory.ToDate = toDate;
            cardHistory.LevelList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("LV")).ToSelectListItems(c.LevelCode ?? "");
            cardHistory.AgencyDDL = Mapper.Map<IEnumerable<AgencyList>>(_agencyService.FindAllByAgency(null)).ToSelectListItems(c.AgencyId ?? 0);

            cardHistory.ViewBy = c.ViewBy;
            cardHistory.ViewByList = CboStatus().ToSelectListItems(c.ViewBy);
            cardHistory.CardStatus = c.CardStatus;
            cardHistory.CardStatusList = CboCardStatus().ToSelectListItems(c.CardStatus);
            return View(cardHistory);
        }

        /// <summary>
        ///     Download báo cáo kinh doanh đại lý
        /// </summary>
        /// <param name="p"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [15/11/2021]
        /// </history>
        public ActionResult DownloadDetailCustomer(DetailCustomerAgency c)
        {
            var userInfo = GetUser();
            c.AgencyId = c.AgencyId ?? 0;
            var cardHistory = c;

            var fromDate = new DateTime();
            var toDate = new DateTime();
            if (string.IsNullOrEmpty(c.fd))
            {
                fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                toDate = fromDate.AddMonths(1).AddDays(-1);
                cardHistory.CustAgencyList = new List<SpAgencyDetailSales>();
            }
            else
            {
                fromDate = DateTime.Parse(StringUtil.ConvertStringToDate(c.fd));
                toDate = DateTime.Parse(StringUtil.ConvertStringToDate(c.td));
                cardHistory.CustAgencyList = _storeService.SP_RPT_AgencyDetailSales("FINDALL", c.AgencyId ?? 0, c.ViewBy ?? "", fromDate, toDate, 0, c.CardNo ?? "", c.LevelCode ?? "", c.SeriNumber ?? "", c.FullName ?? "", c.PhoneNo ?? "", c.Email ?? "", c.CardStatus ?? "", 0, 0);
            }
            foreach (var item in cardHistory.CustAgencyList)
            {
                if (!string.IsNullOrEmpty(item.MaKichHoat))
                    item.MaKichHoat = CryptorEngine.Decrypt(item.MaKichHoat, true, "TTLSTNHD@CARD"); ;
                item.PriceStr = string.Format("{0:#,##0}", item.Price);
            }
            var res = cardHistory.CustAgencyList;
            var dataModel = res.ToList().ConvertToDataTable();
            dataModel.TableName = "Model";
            List<KeyValuePair<string, object>> excelItems = new List<KeyValuePair<string, object>>();
            excelItems.Add(new KeyValuePair<string, object>("FromToDate", string.Format("Từ ngày {0} đến ngày {1}", c.fd, c.td)));
            Excel.ExcelItems = excelItems;
            var stream = Excel.ExportReport(dataModel, HttpContext.Server.MapPath("/Areas/Report/Report/DetailCustomer.xlsx"), false, OfficeType.XLSX);
            return File(stream, OfficeContentType.XLSX, "DetailCustomer.xlsx");
        }
        /// <summary>
        ///     CboStatus
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [13/09/2021]
        /// </history>
        public Dictionary<string, string> CboStatus()
        {
            Dictionary<string, string> dics = new Dictionary<string, string>();
            dics["BUY"] = "Ngày Bán";
            dics["ACTIVE"] = "Ngày Khách Kích Hoạt";
            return dics;
        }
        /// <summary>
        ///     CboCardStatus
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [13/09/2021]
        /// </history>
        public Dictionary<string, string> CboCardStatus()
        {
            Dictionary<string, string> dics = new Dictionary<string, string>();
            dics["Activated"] = "Đã Kích Hoạt";
            dics["NotActivated"] = "Chưa Kích Hoạt";
            return dics;
        }

        /// <summary>
        ///     Bảng kê khách lẻ
        /// </summary>
        /// <param name="p"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [14/03/2022]
        /// </history>
        public ActionResult RetailCustomer(int? p, RetailCustomer c)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory_RetailCustomer, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var cardHistory = c;

            int pageString = p ?? 0;
            int page = pageString == 0 ? 1 : pageString;
            int totalRecord = 0;
            totalRecord = _storeService.SP_RPT_RetailCustomer("COUNT.RECORD", c.PhoneNo ?? "", c.Email ?? "", 0, 0).Count();
            cardHistory.Paging = new Paging(totalRecord, page);
            cardHistory.RetailCustomerList = _storeService.SP_RPT_RetailCustomer("PAGING", c.PhoneNo ?? "", c.Email ?? "", cardHistory.Paging.start, cardHistory.Paging.offset);
            foreach (var item in cardHistory.RetailCustomerList)
            {
                item.PriceStr = string.Format("{0:#,##0}", item.Price);
            }
            return View(cardHistory);
        }

        /// <summary>
        ///     Download báo cáo bảng kê khách lẻ
        /// </summary>
        /// <param name="p"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [14/03/2022]
        /// </history>
        public ActionResult DownloadRetailCustomer(RetailCustomer c)
        {
            
            var cardHistory = c;
            cardHistory.RetailCustomerList = _storeService.SP_RPT_RetailCustomer("FINDALL", c.PhoneNo ?? "", c.Email ?? "", 0, 0);
            foreach (var item in cardHistory.RetailCustomerList)
            {
                item.PriceStr = string.Format("{0:#,##0}", item.Price);
            }
            var res = cardHistory.RetailCustomerList;
            var dataModel = res.ToList().ConvertToDataTable();
            dataModel.TableName = "Model";
            var stream = Excel.ExportReport(dataModel, HttpContext.Server.MapPath("/Areas/Report/Report/RetailCustomer.xlsx"), false, OfficeType.XLSX);
            return File(stream, OfficeContentType.XLSX, "RetailCustomer.xlsx");
        }
    }
}