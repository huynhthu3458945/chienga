﻿using System.Web.Mvc;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using System.Net;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Service.NamPhutThuocBaiService.Interface;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Web.Areas.MasterData.Models;

namespace GCSOFT.MVC.Web.Areas.Report.Controllers
{
	[CompressResponseAttribute]
    public class ThuocBaiController : BaseController
    {
		#region -- Properties --
		private readonly IVuViecService _vuViecService;
		private readonly IContestantsService _contesService;
        private readonly IMailSetupService _mailSetupService;
        private readonly ITemplateService _templateService;
        private string hasValue;
		#endregion
		
		public ThuocBaiController
            (
				IVuViecService vuViecService
                , IContestantsService contesService
                , IMailSetupService mailSetupService
                , ITemplateService templateService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _contesService = contesService;
            _mailSetupService = mailSetupService;
            _templateService = templateService;
        }
		public ActionResult Index(int? id)
        {
            if (id == 1)
            {
                var mailTemplate = new Template();
                var contesInfo = _contesService.FindAll();
                var emailTos = new List<string>();
                foreach (var item in contesInfo)
                {
                    mailTemplate = Mapper.Map<Template>(_templateService.Get("RESEND_USER5P"));
                    emailTos = new List<string>();
                    emailTos.Add(item.Email);
                    var content = mailTemplate.Content.Replace("{{TENDANGNHAP}}", item.Username).Replace("{{MATKHAU}}", item.Password);
                    var _mailHelper = new EmailHelper()
                    {
                        EmailTos = emailTos,
                        Title = mailTemplate.Title,
                        Body = content
                    };
                    string hasValue = _mailHelper.SendEmailsThread();

                    //-- Send SMS
                    content = string.Format(mailTemplate.SMSContent, item.Username, item.Password);
                    var smsHelper = new SMSHelper()
                    {
                        PhoneNo = item.Phone,
                        Content = content
                    };
                    hasValue = smsHelper.Send();
                    //-- Send SMS +
                }
            }
            return View();
        }
    }
}