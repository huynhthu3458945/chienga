﻿using Dapper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.StoreProcedue;
using GCSOFT.MVC.Service.ExeclOfficeEngine;
using GCSOFT.MVC.Service.StoreProcedure.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Areas.Report.Data;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.Report.Controllers
{
    public class UserInfoController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IStoreProcedureService _storeService;
        private string sysCategory = "RPT_USERINFO";
        private bool? permission = false;
        private string connection = ConfigurationManager.ConnectionStrings["GCConnection"].ConnectionString;
        private string hasValue;
        #endregion

        #region -- Contructor --
        public UserInfoController
            (
                IVuViecService vuViecService
                , IStoreProcedureService storeService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _storeService = storeService;
        }
        #endregion
        // GET: Report/AgencyInfo
        public ActionResult Index(int? p, string fd, string td, string o)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            var userInfo = new RptUserInfo();
            userInfo.FromDate = DateTime.Now;
            userInfo.ToDate = userInfo.FromDate.AddMonths(1).AddDays(-1);
            userInfo.ListSTNHDType = GetSTNHDTypeSelectList(string.Empty);
            userInfo.Paging = new Paging(0, 0);
            userInfo.ListUserInfo = new List<SpUserInfo>();
            if (p != null)
                userInfo = LoadData(p, fd, td, o, false);
            return View(userInfo);
        }
       
        public ActionResult Download(int? p,string fd, string td, string o)
        {
            var agencyInfo = LoadData(p, fd, td, o, true);
           
            var dataModel = agencyInfo.ListUserInfo.ToList().ConvertToDataTable();
            dataModel.TableName = "Model";

            List<KeyValuePair<string, object>> excelItems = new List<KeyValuePair<string, object>>();
            excelItems.Add(new KeyValuePair<string, object>("Description", GetEnumDescription((STNHDType)Enum.Parse(typeof(STNHDType), o))));
            excelItems.Add(new KeyValuePair<string, object>("FromToDate", string.Format("Từ ngày {0} đến ngày {1}", fd, td)));
            Excel.ExcelItems = excelItems;

            var stream = Excel.ExportReport(dataModel, HttpContext.Server.MapPath("/Areas/Report/Report/UserInfo.xlsx"), false, OfficeType.XLSX);
            return File(stream, OfficeContentType.XLSX, "UserInfo.xlsx");
        }

        private RptUserInfo LoadData(int? p ,string fd, string td, string o, bool isDownLoad)
        {
            var userInfo = new RptUserInfo();
            userInfo.FromDate = DateTime.Parse(StringUtil.ConvertStringToDate(fd));
            userInfo.ToDate = DateTime.Parse(StringUtil.ConvertStringToDate(td));
            userInfo.ListSTNHDType = GetSTNHDTypeSelectList(o);
            userInfo.STNHDType = (STNHDType)Enum.Parse(typeof(STNHDType), o);
            int pageString = p ?? 0;
            int page = pageString == 0 ? 1 : pageString;
            int totalRecord = 0;
            

            //agencyInfo.agencyInfo = _storeService.sp_rpt_agencyinfo(agencyInfo.FromDate, agencyInfo.ToDate);
            using (var conn = new SqlConnection(connection))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@Action", "COUNT.RECORD");
                paramaters.Add("@FromDate", userInfo.FromDate);
                paramaters.Add("@ToDate", userInfo.ToDate);
                paramaters.Add("@STNHDType", o);

                totalRecord = conn.Query<SpUserInfo>("SP_RPT_USERINFO", paramaters, null, true, 60, System.Data.CommandType.StoredProcedure).Count();
                userInfo.Paging = new Paging(totalRecord, page);

                paramaters = new DynamicParameters();
                if(isDownLoad)
                    paramaters.Add("@Action", "COUNT.RECORD");
                else 
                    paramaters.Add("@Action", "PAGING");
                paramaters.Add("@FromDate", userInfo.FromDate);
                paramaters.Add("@ToDate", userInfo.ToDate);
                paramaters.Add("@STNHDType", o);
                paramaters.Add("@Start", userInfo.Paging.start);
                paramaters.Add("@Offset", userInfo.Paging.offset);
                userInfo.ListUserInfo = conn.Query<SpUserInfo>("SP_RPT_USERINFO", paramaters, null, true, 60, System.Data.CommandType.StoredProcedure);
            }
            return userInfo;
        }

        public List<SelectListItem> GetSTNHDTypeSelectList(string value)
        {
            Array values = Enum.GetValues(typeof(STNHDType));
            List<SelectListItem> items = new List<SelectListItem>(values.Length);

            foreach (var i in values)
            {
                items.Add(new SelectListItem
                {
                    Text = GetEnumDescription((STNHDType)i),
                    Value = ((byte)(STNHDType)i).ToString(),
                    Selected = ((byte)(STNHDType)i).ToString() == value ? true : false
                });
            }

            return items;
        }

        public string GetEnumDescription(STNHDType enumValue)
        {
            var fieldInfo = enumValue.GetType().GetField(enumValue.ToString());

            var descriptionAttributes = (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);

            return descriptionAttributes.Length > 0 ? descriptionAttributes[0].Description : enumValue.ToString();
        }
    }
}