﻿using GCSOFT.MVC.Model.StoreProcedue;
using GCSOFT.MVC.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.Report.Data
{
    public class AgencyBuyGeneralData
    {
        public int Start { get; set; }
        public int Offset { get; set; }
        public DateTime FromDateBuy { get; set; }
        public string fdBuy { get; set; }
        public string FromDateBuyStr { get { return string.Format("{0:dd/MM/yyyy}", FromDateBuy); } }
        public DateTime ToDateBuy { get; set; }
        public string tdBuy { get; set; }
        public string ToDateBuyStr { get { return string.Format("{0:dd/MM/yyyy}", ToDateBuy); } }

        public DateTime FromDateActive { get; set; }
        public string fdActive { get; set; }
        public string FromDateActiveStr { get { return string.Format("{0:dd/MM/yyyy}", FromDateActive); } }
        public DateTime ToDateActive { get; set; }
        public string tdActive { get; set; }
        public string ToDateActiveStr { get { return string.Format("{0:dd/MM/yyyy}", ToDateActive); } }

        public Paging Paging { get; set; }
        public int? searchAgencyId { get; set; }
        public string searchPhone { get; set; }
        public string searchEmail { get; set; }
        public string GroupBy { get; set; }
        public string SortBy { get; set; }
        public IEnumerable<SelectListItem> SortByList { get; set; }
        public IEnumerable<SelectListItem> AgencyList { get; set; }
        public IEnumerable<AgencyBuyGeneral> agencyBuyGeneral { get; set; }

        public int TotalVIP { get; set; }
        public int TotalLevel { get; set; }
        public int TotalGrade { get; set; }

        public int BuyVIP { get; set; }
        public int BuyLevel { get; set; }
        public int BuyGrade { get; set; }

        public int ActiveVIP { get; set; }
        public int ActiveLevel { get; set; }
        public int ActiveGrade { get; set; }


        public int BuyAllVIP { get; set; }
        public int BuyAllLevel { get; set; }
        public int BuyAllGrade { get; set; }

        public int TotalBuyAllVIP { get; set; }
        public int TotalBuyAllLevel { get; set; }
        public int TotalBuyAllGrade { get; set; }
    }
}