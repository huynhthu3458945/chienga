﻿using GCSOFT.MVC.Model.StoreProcedue;
using GCSOFT.MVC.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.Report.Data
{
    public class AgencyChildBuyGeneralData
    {
        public int Start { get; set; }
        public int Offset { get; set; }
        public DateTime FromDateBuy { get; set; }
        public string fdBuy { get; set; }
        public string FromDateBuyStr { get { return string.Format("{0:dd/MM/yyyy}", FromDateBuy); } }
        public DateTime ToDateBuy { get; set; }
        public string tdBuy { get; set; }
        public string ToDateBuyStr { get { return string.Format("{0:dd/MM/yyyy}", ToDateBuy); } }

        public DateTime FromDateActive { get; set; }
        public string fdActive { get; set; }
        public string FromDateActiveStr { get { return string.Format("{0:dd/MM/yyyy}", FromDateActive); } }
        public DateTime ToDateActive { get; set; }
        public string tdActive { get; set; }
        public string ToDateActiveStr { get { return string.Format("{0:dd/MM/yyyy}", ToDateActive); } }

        public Paging Paging { get; set; }
        public int? AgencyId { get; set; }
        public int? searchChildAgencyId { get; set; }
        public IEnumerable<SelectListItem> AgencyChildList { get; set; }
        public IEnumerable<SelectListItem> AgencyList { get; set; }
        public IEnumerable<AgencyChildBuyGeneral> agencyChildBuyGeneral { get; set; }

    }
}