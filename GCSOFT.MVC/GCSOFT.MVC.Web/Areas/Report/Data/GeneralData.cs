﻿using GCSOFT.MVC.Model.StoreProcedue;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.Web.Areas.Report.Data
{
    public class GeneralData
    {
        [DisplayName("Từ Ngày")]
        public DateTime FromDate { get; set; }
        public string FromDateStr { get { return string.Format("{0:dd/MM/yyyy}", FromDate); } }
        [DisplayName("Đến Ngày")]
        public DateTime ToDate { get; set; }
        public string ToDateStr { get { return string.Format("{0:dd/MM/yyyy}", ToDate); } }
        [DisplayName("Từ Ngày (Cùng Kỳ)")]
        public DateTime LastFromDate { get; set; }
        public string LastFromDateStr { get { return string.Format("{0:dd/MM/yyyy}", LastFromDate); } }
        [DisplayName("Đến Ngày (Cùng Kỳ)")]
        public DateTime LastToDate { get; set; }
        public string LastToDateStr { get { return string.Format("{0:dd/MM/yyyy}", LastToDate); } }
        public IEnumerable<SpGeneralInfo> generalInfo { get; set; }
        public IEnumerable<SpTTLBuyCustomerDtl> ttlByCustDtl { get; set; }
        public IEnumerable<SpAgencyLevelDtl> agencyLevelDtls { get; set; }
    }
}