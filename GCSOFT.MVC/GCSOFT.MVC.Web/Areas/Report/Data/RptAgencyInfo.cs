﻿using GCSOFT.MVC.Model.StoreProcedue;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.Report.Data
{
    public class RptAgencyInfo
    {
        [DisplayName("Từ Ngày")]
        public DateTime FromDate { get; set; }
        public string FromDateStr { get { return string.Format("{0:dd/MM/yyyy}", FromDate); } }
        [DisplayName("Đến Ngày")]
        public DateTime ToDate { get; set; }
        public string ToDateStr { get { return string.Format("{0:dd/MM/yyyy}", ToDate); } }
        [DisplayName("Sắp Xếp")]
        public IEnumerable<SelectListItem> SortBy { get; set; }
        public string SortByCode { get; set; }
        [DisplayName("Tăng Dần/Giảm Dần")]
        public IEnumerable<SelectListItem> OrderBy { get; set; }
        public string OrderByCode { get; set; }
        public IEnumerable<SpAgencyinfo> agencyInfo { get; set; }
    }
}