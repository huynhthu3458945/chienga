﻿using GCSOFT.MVC.Model.StoreProcedue;
using GCSOFT.MVC.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.Report.Data
{
    public class RptEbookUser
    {
        public Paging paging { get; set; }
        public int CityId { get; set; }
        public int DistrictId { get; set; }
        public IEnumerable<SelectListItem> CityList { get; set; }
        public IEnumerable<SelectListItem> DistrictList { get; set; }
        public IEnumerable<EbookUserByCity> EbookUserByCities { get; set; }
        public IEnumerable<EbookUserDetailByCity> EbookUserDetailByCities { get; set; }
    }
}