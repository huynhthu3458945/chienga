﻿using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.StoreProcedue;
using GCSOFT.MVC.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.Report.Data
{
    public class RptUserInfo
    {
        [DisplayName("Từ Ngày")]
        public DateTime FromDate { get; set; }
        public string FromDateStr { get { return string.Format("{0:dd/MM/yyyy}", FromDate); } }
        [DisplayName("Đến Ngày")]
        public DateTime ToDate { get; set; }
        public string ToDateStr { get { return string.Format("{0:dd/MM/yyyy}", ToDate); } }
        [DisplayName("Loại Tài Khoản")]
        public IEnumerable<SelectListItem> ListSTNHDType { get; set; }
        public STNHDType STNHDType { get; set; }
        public IEnumerable<SpUserInfo> ListUserInfo { get; set; }
        public Paging Paging { get; set; }
    }
}