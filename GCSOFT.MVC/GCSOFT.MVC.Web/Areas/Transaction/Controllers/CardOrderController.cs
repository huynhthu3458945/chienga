﻿using AutoMapper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Areas.Transaction.Data;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.Transaction.Controllers
{
    [CompressResponseAttribute]
    public class CardOrderController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IPurchaseHeaderService _purchaseHdrService;
        private string sysCategory = "CARDORDER";
        private bool? permission = false;
        private CardOrder cardOrder;
        private M_PurchaseHeader purchaseHdr;
        private PurchaseCardType purchType = PurchaseCardType.PurchaseOrder;
        private string hasValue;
        #endregion
        public CardOrderController
            (
                IVuViecService vuViecService
                , IPurchaseHeaderService purchaseHdrService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _purchaseHdrService = purchaseHdrService;
        }
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }
        public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            cardOrder = new CardOrder();
            cardOrder.Code = StringUtil.CheckLetter("CO", _purchaseHdrService.GetMax(purchType), 4);
            cardOrder.OrderDateStr = string.Format("{0:dd/MM/yyyy}", cardOrder.OrderDate);
            return View(cardOrder);
        }
        public ActionResult Edit(string id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            cardOrder = Mapper.Map<CardOrder>(_purchaseHdrService.GetBy(purchType, id));
            if (cardOrder == null)
                return HttpNotFound();
            cardOrder.OrderDateStr = string.Format("{0:dd/MM/yyyy}", cardOrder.OrderDate);
            cardOrder.DatePrintStr = string.Format("{0:dd/MM/yyyy}", cardOrder.DatePrint);
            return View(cardOrder);
        }
        [HttpPost]
        public ActionResult Create(CardOrder _cardOrder)
        {
            try
            {
                SetData(_cardOrder, true);
                hasValue = _purchaseHdrService.Insert(purchaseHdr);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = purchaseHdr.Code, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult Edit(CardOrder _cardOrder)
        {
            try
            {
                SetData(_cardOrder, false);
                hasValue = _purchaseHdrService.Insert(purchaseHdr);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = purchaseHdr.Code, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        private void SetData(CardOrder _cardOrder, bool isCreate)
        {
            cardOrder = _cardOrder;
            if (isCreate)
                cardOrder.Code = StringUtil.CheckLetter("CO", _purchaseHdrService.GetMax(purchType), 4);
            if (!string.IsNullOrEmpty(cardOrder.OrderDateStr))
                cardOrder.OrderDate = DateTime.Parse(StringUtil.ConvertStringToDate(cardOrder.OrderDateStr));
            if (!string.IsNullOrEmpty(cardOrder.DatePrintStr))
                cardOrder.DatePrint = DateTime.Parse(StringUtil.ConvertStringToDate(cardOrder.DatePrintStr));
            else
                cardOrder.DatePrint = null;
            cardOrder.Type = purchType;

            purchaseHdr = Mapper.Map<M_PurchaseHeader>(cardOrder);
        }
        public JsonResult LoadData(GridSettings grid)
        {
            var list = Mapper.Map<IEnumerable<CardList>>(_purchaseHdrService.FindAll(purchType)).AsQueryable();
            list = JqGrid.SetupGrid<CardList>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
        public ActionResult Deletes(string id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                var _codes = id.Split(',').Select(item => item).ToList();
                hasValue = _purchaseHdrService.Delete(purchType, _codes);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return Json(new { v = true, count = _codes.Count() });
                return Json("Xóa dữ liệu không thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }
    }
}