﻿using AutoMapper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Model.Approval;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.ApprovalService.Interface;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.StoreProcedure.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Service.Transaction.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Areas.Agency.Data;
using GCSOFT.MVC.Web.Areas.Approval.Data;
using GCSOFT.MVC.Web.Areas.Transaction.Data;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.Transaction.Controllers
{
    [CompressResponseAttribute]
    public class ConvertCardController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly ISalesHeaderService _salesHdrService;
        private readonly ISalesLineService _salesLineService;
        private readonly IAgencyService _agencyService;
        private readonly IOptionLineService _optionLineService;
        private readonly ICardLineService _cardLineService;
        private readonly ICardEntryService _cardEntryService;
        private readonly IUserThuocNhomService _userThuocNhomService;
        private readonly IUserService _userService;
        private readonly IFlowService _flowService;
        private readonly IUserGroupService _userGroupService;
        private readonly ITemplateService _templateService;
        private readonly IMailSetupService _mailSetupService;
        private readonly IStoreProcedureService _storeProcedureService;
        private readonly IAgencyCardService _agencyAndCardService;
        private string sysCategory = "CONVERT";
        private bool? permission = false;
        private SalesOrderCard salesOrderCard;
        private M_SalesHeader mSalesHeader;
        private SalesType salesType = SalesType.Convert;
        private string hasValue;
        #endregion
        public ConvertCardController
            (
                IVuViecService vuViecService
                , ISalesHeaderService salesHdrService
                , ISalesLineService salesLineService
                , IAgencyService agencyService
                , IOptionLineService optionLineService
                , ICardLineService cardLineService
                , ICardEntryService cardEntryService
                , IUserThuocNhomService userThuocNhomService
                , IUserService userService
                , IFlowService flowService
                , IUserGroupService userGroupService
                , ITemplateService templateService
                , IMailSetupService mailSetupService
                , IStoreProcedureService storeProcedureService
                , IAgencyCardService agencyAndCardService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _salesHdrService = salesHdrService;
            _salesLineService = salesLineService;
            _agencyService = agencyService;
            _optionLineService = optionLineService;
            _cardLineService = cardLineService;
            _cardEntryService = cardEntryService;
            _userThuocNhomService = userThuocNhomService;
            _userService = userService;
            _flowService = flowService;
            _userGroupService = userGroupService;
            _templateService = templateService;
            _mailSetupService = mailSetupService;
            _storeProcedureService = storeProcedureService;
            _agencyAndCardService = agencyAndCardService;
        }
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }
        public JsonResult LoadData(GridSettings grid)
        {
            var list = Mapper.Map<IEnumerable<SalesOrderList>>(_salesHdrService.FindAll(salesType)).AsQueryable();
            list = JqGrid.SetupGrid<SalesOrderList>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
        public ActionResult Create()
        {
            try
            {
                #region -- Role user --
                permission = GetPermission(sysCategory, Authorities.Add);
                if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
                if (!permission.Value) return View("AccessDenied");
                #endregion
                var userInfo = GetUser();
                salesOrderCard = new SalesOrderCard();
                salesOrderCard.Code = StringUtil.CheckLetter("CV", _salesHdrService.GetMax(salesType), 4);
                salesOrderCard.DocumentDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.DocumentDate);
                salesOrderCard.CustomerList = Mapper.Map<IEnumerable<AgencyList>>(_agencyService.FindAll(AgencyType.Agency, null)).ToSelectListItems(0);
                var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("ORDERSTATUS", "700"));
                salesOrderCard.StatusId = statusInfo.LineNo;
                salesOrderCard.StatusCode = statusInfo.Code;
                salesOrderCard.StatusName = statusInfo.Name;
                var cardtypes = _optionLineService.FindAll("LV");
                salesOrderCard.FromLevelList = Mapper.Map<IEnumerable<VM_OptionLine>>(cardtypes).ToSelectListItems("");
                salesOrderCard.ToLevelList = Mapper.Map<IEnumerable<VM_OptionLine>>(cardtypes).ToSelectListItems("");
                ViewBag.IsCreate = true;
                var _flow = new FlowViewModel();
                var _flows = new List<FlowViewModel>();
                statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("APPSTATUS", "100")); //Chờ Duyệt
                var userIds = _userThuocNhomService.GetMany("APPROVAL_SALES").Select(d => d.UserID).ToList() ?? new List<int>();
                _flow = new FlowViewModel()
                {
                    Type = ApprovalType.Convert,
                    Code = salesOrderCard.Code,
                    LineNo = 100,
                    GroupCode = "APPROVAL_SALES",
                    GroupName = Mapper.Map<NhomUserCard>(_userGroupService.GetBy2("APPROVAL_SALES")).Name,
                    ApprovalNo = string.Empty,
                    Priority = 100,
                    StatusId = statusInfo.LineNo,
                    StatusCode = statusInfo.Code,
                    StatusName = statusInfo.Name,
                    ApprovalList = Mapper.Map<IEnumerable<UserVM>>(_userService.FindAll(userIds)).ToList().ToSelectListItems("")
                };
                _flows.Add(_flow);
                userIds = _userThuocNhomService.GetMany("APPROVAL_ACC").Select(d => d.UserID).ToList() ?? new List<int>();
                _flow = new FlowViewModel()
                {
                    Type = ApprovalType.Convert,
                    Code = salesOrderCard.Code,
                    LineNo = 200,
                    GroupCode = "APPROVAL_ACC",
                    GroupName = Mapper.Map<NhomUserCard>(_userGroupService.GetBy2("APPROVAL_ACC")).Name,
                    ApprovalNo = string.Empty,
                    Priority = 200,
                    StatusId = statusInfo.LineNo,
                    StatusCode = statusInfo.Code,
                    StatusName = statusInfo.Name,
                    ApprovalList = Mapper.Map<IEnumerable<UserVM>>(_userService.FindAll(userIds)).ToList().ToSelectListItems("")
                };
                _flows.Add(_flow);
                salesOrderCard.Flows = _flows;

                ViewBag.isSendApproval = "disabled";
                ViewBag.IsMakeOrder = "disabled";
                return View(salesOrderCard);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }

        }
        public ActionResult Edit(string id, int? p)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            salesOrderCard = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(salesType, id));
            salesOrderCard.Flows = Mapper.Map<IEnumerable<FlowViewModel>>(_flowService.FindAll(ApprovalType.Convert, id));
            if (salesOrderCard == null)
                return HttpNotFound();
            salesOrderCard.DocumentDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.DocumentDate);
            salesOrderCard.PostingDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.PostingDate);
            salesOrderCard.CustomerList = Mapper.Map<IEnumerable<AgencyList>>(_agencyService.FindAll(AgencyType.Agency, null)).ToSelectListItems(salesOrderCard.CustomerId);
            var cardtypes = _optionLineService.FindAll("LV");
            salesOrderCard.FromLevelList = Mapper.Map<IEnumerable<VM_OptionLine>>(cardtypes).ToSelectListItems(salesOrderCard.FromLevelCode);
            salesOrderCard.ToLevelList = Mapper.Map<IEnumerable<VM_OptionLine>>(cardtypes).ToSelectListItems(salesOrderCard.ToLevelCode);
            var userIds = new List<int>();
            if (salesOrderCard.Flows.Count() == 0)
            {
                var _flow = new FlowViewModel();
                var _flows = new List<FlowViewModel>();
                var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("APPSTATUS", "100")); //Chờ Duyệt
                userIds = _userThuocNhomService.GetMany("APPROVAL_SALES").Select(d => d.UserID).ToList() ?? new List<int>();
                _flow = new FlowViewModel()
                {
                    Type = ApprovalType.Convert,
                    Code = salesOrderCard.Code,
                    LineNo = 100,
                    GroupCode = "APPROVAL_SALES",
                    GroupName = Mapper.Map<NhomUserCard>(_userGroupService.GetBy2("APPROVAL_SALES")).Name,
                    ApprovalNo = string.Empty,
                    Priority = 100,
                    StatusId = statusInfo.LineNo,
                    StatusCode = statusInfo.Code,
                    StatusName = statusInfo.Name,
                    ApprovalList = Mapper.Map<IEnumerable<UserVM>>(_userService.FindAll(userIds)).ToList().ToSelectListItems("")
                };
                _flows.Add(_flow);
                userIds = _userThuocNhomService.GetMany("APPROVAL_ACC").Select(d => d.UserID).ToList() ?? new List<int>();
                _flow = new FlowViewModel()
                {
                    Type = ApprovalType.Convert,
                    Code = salesOrderCard.Code,
                    LineNo = 200,
                    GroupCode = "APPROVAL_ACC",
                    GroupName = Mapper.Map<NhomUserCard>(_userGroupService.GetBy2("APPROVAL_ACC")).Name,
                    ApprovalNo = string.Empty,
                    Priority = 200,
                    StatusId = statusInfo.LineNo,
                    StatusCode = statusInfo.Code,
                    StatusName = statusInfo.Name,
                    ApprovalList = Mapper.Map<IEnumerable<UserVM>>(_userService.FindAll(userIds)).ToList().ToSelectListItems("")
                };
                _flows.Add(_flow);
                salesOrderCard.Flows = _flows;
            } 
            else
            {
                salesOrderCard.Flows.ToList().ForEach(item => {
                    userIds = _userThuocNhomService.GetMany(item.GroupCode).Select(d => d.UserID).ToList() ?? new List<int>();
                    item.ApprovalList = Mapper.Map<IEnumerable<UserVM>>(_userService.FindAll(userIds)).ToList().ToSelectListItems(item.ApprovalNo);
                });
            }
            ViewBag.IsCreate = false;
            ViewBag.isSendApproval = salesOrderCard.StatusCode.Equals("700") ? "" : "disabled";
            ViewBag.userName = GetUser().Username;
            return View(salesOrderCard);
        }
        [HttpPost]
        public ActionResult Create(SalesOrderCard _salesOrderCard)
        {
            try
            {
                SetData(_salesOrderCard, true);
                _salesHdrService.Insert(mSalesHeader);
                _flowService.Insert(mSalesHeader.Flows);
                //_storeProcedureService.sp_ChangeCard(mSalesHeader.CustomerId, "100", "200", mSalesHeader.TotalCard);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = mSalesHeader.Code, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult Edit(SalesOrderCard _salesOrderCard)
        {
            try
            {
                SetData(_salesOrderCard, false);
                hasValue = _salesHdrService.Update(mSalesHeader);
                hasValue = _flowService.Delete(ApprovalType.Convert, mSalesHeader.Code);
                hasValue = _flowService.Insert(mSalesHeader.Flows);
                //_storeProcedureService.sp_ChangeCard(mSalesHeader.CustomerId, "100", "200", mSalesHeader.TotalCard);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = mSalesHeader.Code, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        private void SetData(SalesOrderCard _salesOrderCard, bool isCreate)
        {
            var userInfo = GetUser();
            salesOrderCard = _salesOrderCard;
            if (isCreate)
            {
                salesOrderCard.Code = StringUtil.CheckLetter("CV", _salesHdrService.GetMax(salesType), 4);
                salesOrderCard.CreateBy = userInfo.Username;
                salesOrderCard.DateCreate = DateTime.Now;
                
            }
            else
            {
                var _flows = salesOrderCard.Flows;
                salesOrderCard = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(salesType, _salesOrderCard.Code));
                salesOrderCard.TotalCard = _salesOrderCard.TotalCard;
                salesOrderCard.Flows = _flows;
                salesOrderCard.SuggestedNoOfCard = _salesOrderCard.SuggestedNoOfCard;
                salesOrderCard.Remark = _salesOrderCard.Remark;
                salesOrderCard.TTLRemark = _salesOrderCard.TTLRemark;
                salesOrderCard.LastModifyBy = userInfo.Username;
                salesOrderCard.LastModifyDate = _salesOrderCard.DateCreate;
            }
            salesOrderCard.LastModifyBy = userInfo.Username;
            salesOrderCard.LastModifyDate = DateTime.Now;
            if (!string.IsNullOrEmpty(_salesOrderCard.ReceiptDateStr))
                salesOrderCard.ReceiptDate = DateTime.Parse(StringUtil.ConvertStringToDate(_salesOrderCard.ReceiptDateStr));
            else
                salesOrderCard.ReceiptDate = null;
            if (!string.IsNullOrEmpty(_salesOrderCard.PostingDateStr))
                salesOrderCard.PostingDate = DateTime.Parse(StringUtil.ConvertStringToDate(_salesOrderCard.PostingDateStr));
            else
                salesOrderCard.PostingDate = null;

            if (!string.IsNullOrEmpty(_salesOrderCard.DocumentDateStr))
                salesOrderCard.DocumentDate = DateTime.Parse(StringUtil.ConvertStringToDate(_salesOrderCard.DocumentDateStr));
            else
                salesOrderCard.DocumentDate = null;
            var customerInfo = Mapper.Map<AgencyCard>(_agencyService.GetBy2(_salesOrderCard.CustomerId));
            salesOrderCard.CustomerName = customerInfo.FullName;
            salesOrderCard.CustomerAddress = customerInfo.Address;
            salesOrderCard.CustomerEmail = customerInfo.Email;
            salesOrderCard.CustomerPhoneNo = customerInfo.Phone;
            salesOrderCard.SalesType = salesType;
            var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("ORDERSTATUS", "700"));
            salesOrderCard.StatusId = statusInfo.LineNo;
            salesOrderCard.StatusCode = statusInfo.Code;
            salesOrderCard.StatusName = statusInfo.Name;

            var lineNo = 0;
            salesOrderCard.Flows.OrderBy(d => d.Priority).ToList().ForEach(item => {
                lineNo += 100;
                item.Type = ApprovalType.Convert;
                item.Code = _salesOrderCard.Code;
                item.LineNo = lineNo;
                var _userInfo = Mapper.Map<UserVM>(_userService.GetBy(item.ApprovalNo));
                if (_userInfo != null)
                {
                    item.ApprovalName = _userInfo.FullName;
                    item.ApprovalEmail = _userInfo.Email;
                }
                statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("APPSTATUS", item.StatusCode));
                item.StatusId = statusInfo.LineNo;
                item.StatusCode = statusInfo.Code;
                item.StatusName = statusInfo.Name;
            });

            mSalesHeader = Mapper.Map<M_SalesHeader>(salesOrderCard);
        }
        /// <summary>
        /// Lấy Số Lượng Thẻ Mới của Sub Agency
        /// </summary>
        /// <param name="idc"></param>
        /// <returns></returns>
        public JsonResult NoOfCard(int idc)
        {
            var statusCodes = new List<string>();
            statusCodes.Add("100");
            var _noOfCards = _agencyAndCardService.TotalCard(idc, statusCodes, "100");
            return Json(_noOfCards, JsonRequestBehavior.AllowGet);
        }


        #region -- Json --
        /// <summary>
        /// Từ chối đơn chuyển đổi thẻ
        /// </summary>
        /// <param name="t">Approval Type</param>
        /// <param name="c">Code</param>
        /// <returns></returns>
        public JsonResult CancelCode(string c)
        {
            try
            {
                // từ chối
                var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("ORDERSTATUS", "1100"));
                var purchaseOrder = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(SalesType.Convert, c));
                purchaseOrder.StatusId = statusInfo.LineNo;
                purchaseOrder.StatusCode = statusInfo.Code;
                purchaseOrder.StatusName = statusInfo.Name;
                hasValue = _salesHdrService.Update(Mapper.Map<M_SalesHeader>(purchaseOrder));

                hasValue = _vuViecService.Commit();
                var _status = string.IsNullOrEmpty(hasValue) ? "Oke" : "Error";
                var _msg = string.IsNullOrEmpty(hasValue) ? "" : hasValue;
                return Json(new { s = _status, m = _msg }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { s = "Error", m = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}