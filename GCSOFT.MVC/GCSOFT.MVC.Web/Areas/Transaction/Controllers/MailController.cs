﻿using AutoMapper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Areas.Agency.Data;
using GCSOFT.MVC.Web.Areas.MasterData.Models;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.Transaction.Controllers
{
    [CompressResponseAttribute]
    public class MailController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IMailSetupService _mailSetupService;
        private readonly ITemplateService _templateService;
        private readonly IAgencyService _agencyService;
        private readonly IUserService _userService;
        #endregion

        #region -- Contructor --
        public MailController
            (
                IVuViecService vuViecService
                , IMailSetupService mailSetupService
                , ITemplateService templateService
                , IAgencyService agencyService
                , IUserService userService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _mailSetupService = mailSetupService;
            _templateService = templateService;
            _agencyService = agencyService;
            _userService = userService;
        }
        #endregion
        /// <summary>
        /// Gửi lại password cho đại lý
        /// </summary>
        /// <param name="id">Id Đại Lý</param>
        /// <returns></returns>
        public JsonResult ResendPassForAgency(int id)
        {
            var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
            var agencyInfo = Mapper.Map<AgencyCard>(_agencyService.GetBy(id));
            if (string.IsNullOrEmpty(agencyInfo.Email))
                return Json("Email không tồn tại", JsonRequestBehavior.AllowGet);
            var mailTemplate = Mapper.Map<Template>(_templateService.Get("AGENCYINFO"));
            var userInfo = Mapper.Map<UserLoginVMs>(_userService.GetBy(agencyInfo.UserName));
            var emailTos = new List<string>();
            emailTos.Add(agencyInfo.Email);
            var _mailHelper = new EmailHelper() {
                EmailTos = emailTos,
                DisplayName = emailInfo.DisplayName,
                Title = mailTemplate.Title,
                Body = mailTemplate.Content.Replace("{{Name}}", agencyInfo.FullName).Replace("{{Username}}", agencyInfo.UserName).Replace("{{Password}}", CryptorEngine.Decrypt(userInfo.Password, true, "Hieu.Le-GC.Soft")),
                MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
            };
            string hasValue = _mailHelper.DoSendMail();
            //-- Send SMS
            var content = string.Format("5 Phut Thuoc Bai gui A/C tai khoan doi tac, duong dan he thong https://agency.stnhd.com , Ten Dang Nhap: {0} , Mat Khau {1} vui long kiem tra email de nhan thong tin chi tiet.", agencyInfo.UserName, CryptorEngine.Decrypt(userInfo.Password, true, "Hieu.Le-GC.Soft"));
            var smsHelper = new SMSHelper()
            {
                PhoneNo = agencyInfo.Phone,
                Content = content
            };
            if (smsHelper.IsMobiphone())
            {
                var smsTemplate = Mapper.Map<Template>(_templateService.Get("MOBI_TK_DAILY"));
                smsHelper.Content = string.Format(smsTemplate.SMSContent, agencyInfo.UserName, CryptorEngine.Decrypt(userInfo.Password, true, "Hieu.Le-GC.Soft"));
                hasValue = smsHelper.SendMobiphone();
            }
            else
                hasValue = smsHelper.Send();
            //-- Send SMS +
            return Json(string.IsNullOrEmpty(hasValue) ? "Đã gửi thông tin đăng nhập đại lý qua mail" : hasValue, JsonRequestBehavior.AllowGet);
        }
    }
}