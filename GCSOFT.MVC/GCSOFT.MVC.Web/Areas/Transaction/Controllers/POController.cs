﻿using AutoMapper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Model.Approval;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.ApprovalService.Interface;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Service.Transaction.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Areas.Agency.Data;
using GCSOFT.MVC.Web.Areas.Approval.Data;
using GCSOFT.MVC.Web.Areas.MasterData.Models;
using GCSOFT.MVC.Web.Areas.Transaction.Data;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.Transaction.Controllers
{
    [CompressResponseAttribute]
    public class POController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly ISalesHeaderService _salesHdrService;
        private readonly ISalesLineService _salesLineService;
        private readonly IAgencyService _agencyService;
        private readonly IOptionLineService _optionLineService;
        private readonly ICardLineService _cardLineService;
        private readonly ICardEntryService _cardEntryService;
        private readonly IUserThuocNhomService _userThuocNhomService;
        private readonly IUserService _userService;
        private readonly IFlowService _flowService;
        private readonly IUserGroupService _userGroupService;
        private readonly ITemplateService _templateService;
        private readonly IMailSetupService _mailSetupService;
        private string sysCategory = "PO2";
        private bool? permission = false;
        private SalesOrderCard salesOrderCard;
        private M_SalesHeader mSalesHeader;
        private SalesType salesType = SalesType.PurchaseOrder;
        private string hasValue;
        #endregion
        public POController
            (
                IVuViecService vuViecService
                , ISalesHeaderService salesHdrService
                , ISalesLineService salesLineService
                , IAgencyService agencyService
                , IOptionLineService optionLineService
                , ICardLineService cardLineService
                , ICardEntryService cardEntryService
                , IUserThuocNhomService userThuocNhomService
                , IUserService userService
                , IFlowService flowService
                , IUserGroupService userGroupService
                , ITemplateService templateService
                , IMailSetupService mailSetupService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _salesHdrService = salesHdrService;
            _salesLineService = salesLineService;
            _agencyService = agencyService;
            _optionLineService = optionLineService;
            _cardLineService = cardLineService;
            _cardEntryService = cardEntryService;
            _userThuocNhomService = userThuocNhomService;
            _userService = userService;
            _flowService = flowService;
            _userGroupService = userGroupService;
            _templateService = templateService;
            _mailSetupService = mailSetupService;
        }
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }
        public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            salesOrderCard = new SalesOrderCard();
            salesOrderCard.Code = StringUtil.CheckLetter("PO", _salesHdrService.GetMax(salesType), 4);
            salesOrderCard.DocumentDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.DocumentDate);
            salesOrderCard.CustomerList = Mapper.Map<IEnumerable<AgencyList>>(_agencyService.FindAll(AgencyType.Agency, null)).ToSelectListItems(0);
            var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("ORDERSTATUS", "700"));
            salesOrderCard.StatusId = statusInfo.LineNo;
            salesOrderCard.StatusCode = statusInfo.Code;
            salesOrderCard.StatusName = statusInfo.Name;
            salesOrderCard.DurationList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("CARDINFO")).ToSelectListItems("");
            salesOrderCard.ReasonList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("REASON")).ToSelectListItems("");

            ViewBag.IsEdit = false;
            var _flow = new FlowViewModel();
            var _flows = new List<FlowViewModel>();
            statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("APPSTATUS", "100")); //Chờ Duyệt

            var userIds = _userThuocNhomService.GetMany("APPROVAL_SALES").Select(d => d.UserID).ToList() ?? new List<int>();
            _flow = new FlowViewModel()
            {
                Type = ApprovalType.PurchaseOrder,
                Code = salesOrderCard.Code,
                LineNo = 100,
                GroupCode = "APPROVAL_SALES",
                GroupName = Mapper.Map<NhomUserCard>(_userGroupService.GetBy2("APPROVAL_SALES")).Name,
                ApprovalNo = string.Empty,
                Priority = 100,
                StatusId = statusInfo.LineNo,
                StatusCode = statusInfo.Code,
                StatusName = statusInfo.Name,
                ApprovalList = Mapper.Map<IEnumerable<UserVM>>(_userService.FindAll(userIds)).ToList().ToSelectListItems("")
            };
            _flows.Add(_flow);
            userIds = _userThuocNhomService.GetMany("APPROVAL_ACC").Select(d => d.UserID).ToList() ?? new List<int>();
            _flow = new FlowViewModel()
            {
                Type = ApprovalType.PurchaseOrder,
                Code = salesOrderCard.Code,
                LineNo = 200,
                GroupCode = "APPROVAL_ACC",
                GroupName = Mapper.Map<NhomUserCard>(_userGroupService.GetBy2("APPROVAL_ACC")).Name,
                ApprovalNo = string.Empty,
                Priority = 200,
                StatusId = statusInfo.LineNo,
                StatusCode = statusInfo.Code,
                StatusName = statusInfo.Name,
                ApprovalList = Mapper.Map<IEnumerable<UserVM>>(_userService.FindAll(userIds)).ToList().ToSelectListItems("")
            };
            _flows.Add(_flow);
            salesOrderCard.Flows = _flows;
            ViewBag.IsMakeOrder = "disabled";
            ViewBag.isSendApproval = "disabled";
            ViewBag.isConfirm = "disabled";
            return View(salesOrderCard);
        }
        public ActionResult Edit(string id, int? p)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            try
            {
                salesOrderCard = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(salesType, id));
                if (salesOrderCard == null)
                    return HttpNotFound();
                salesOrderCard.DocumentDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.DocumentDate);
                salesOrderCard.PostingDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.PostingDate);
                salesOrderCard.CustomerList = Mapper.Map<IEnumerable<AgencyList>>(_agencyService.FindAll(AgencyType.Agency, null)).ToSelectListItems(salesOrderCard.CustomerId);
                salesOrderCard.Flows = Mapper.Map<IEnumerable<FlowViewModel>>(_flowService.FindAll(ApprovalType.PurchaseOrder, salesOrderCard.Code));
                salesOrderCard.DurationList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("CARDINFO")).ToSelectListItems(salesOrderCard.DurationCode);
                salesOrderCard.ReasonList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("REASON")).ToSelectListItems(salesOrderCard.ReasonCode);
                var userIds = _userThuocNhomService.GetMany("APPROVAL_SALES").Select(d => d.UserID).ToList() ?? new List<int>();
                if (salesOrderCard.Flows.Count() == 0)
                {
                    var _flow = new FlowViewModel();
                    var _flows = new List<FlowViewModel>();
                    var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("APPSTATUS", "100")); //Chờ Duyệt
                    _flow = new FlowViewModel()
                    {
                        Type = ApprovalType.PurchaseOrder,
                        Code = salesOrderCard.Code,
                        LineNo = 100,
                        GroupCode = "APPROVAL_SALES",
                        GroupName = Mapper.Map<NhomUserCard>(_userGroupService.GetBy2("APPROVAL_SALES")).Name,
                        ApprovalNo = string.Empty,
                        Priority = 100,
                        StatusId = statusInfo.LineNo,
                        StatusCode = statusInfo.Code,
                        StatusName = statusInfo.Name,
                        ApprovalList = Mapper.Map<IEnumerable<UserVM>>(_userService.FindAll(userIds)).ToList().ToSelectListItems("")
                    };
                    _flows.Add(_flow);
                    userIds = _userThuocNhomService.GetMany("APPROVAL_ACC").Select(d => d.UserID).ToList() ?? new List<int>();
                    _flow = new FlowViewModel()
                    {
                        Type = ApprovalType.PurchaseOrder,
                        Code = salesOrderCard.Code,
                        LineNo = 200,
                        GroupCode = "APPROVAL_ACC",
                        GroupName = Mapper.Map<NhomUserCard>(_userGroupService.GetBy2("APPROVAL_ACC")).Name,
                        ApprovalNo = string.Empty,
                        Priority = 200,
                        StatusId = statusInfo.LineNo,
                        StatusCode = statusInfo.Code,
                        StatusName = statusInfo.Name,
                        ApprovalList = Mapper.Map<IEnumerable<UserVM>>(_userService.FindAll(userIds)).ToList().ToSelectListItems("")
                    };
                    _flows.Add(_flow);
                    salesOrderCard.Flows = _flows;
                }
                userIds = new List<int>();
                salesOrderCard.Flows.ToList().ForEach(item => {
                    userIds = _userThuocNhomService.GetMany(item.GroupCode).Select(d => d.UserID).ToList() ?? new List<int>();
                    item.ApprovalList = Mapper.Map<IEnumerable<UserVM>>(_userService.FindAll(userIds)).ToList().ToSelectListItems(item.ApprovalNo);
                });
                
                return View(salesOrderCard);
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.ToString();
                return View("error");
            }
        }
        [HttpPost]
        public ActionResult Create(SalesOrderCard _salesOrderCard)
        {
            try
            {
                SetData(_salesOrderCard, true);
                hasValue = _salesHdrService.Insert(mSalesHeader);
                hasValue = _flowService.Delete(ApprovalType.PurchaseOrder, mSalesHeader.Code);
                hasValue = _flowService.Insert(mSalesHeader.Flows);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = mSalesHeader.Code, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult Edit(SalesOrderCard _salesOrderCard)
        {
            try
            {
                SetData(_salesOrderCard, false);
                _salesHdrService.Update(mSalesHeader);
                _flowService.Delete(ApprovalType.PurchaseOrder, mSalesHeader.Code);
                _flowService.Insert(mSalesHeader.Flows);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = mSalesHeader.Code, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public ActionResult Delete(string id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                hasValue = _salesHdrService.Delete(salesType, id);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return Json(new { v = true, count = 1 });
                return Json("Xóa dữ liệu không thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }
        private void SetData(SalesOrderCard _salesOrderCard, bool isCreate)
        {
            var userInfo = GetUser();
            salesOrderCard = _salesOrderCard;
            if (isCreate)
            {
                salesOrderCard.Code = StringUtil.CheckLetter("PO", _salesHdrService.GetMax(salesType), 4);
                salesOrderCard.CreateBy = userInfo.Username;
                salesOrderCard.DateCreate = DateTime.Now;
                salesOrderCard.LastModifyBy = userInfo.Username;
                salesOrderCard.LastModifyDate = salesOrderCard.DateCreate;
            }
            else
            {
                salesOrderCard.LastModifyBy = userInfo.Username;
                salesOrderCard.LastModifyDate = DateTime.Now;
            }
            if (!string.IsNullOrEmpty(salesOrderCard.PostingDateStr))
                salesOrderCard.PostingDate = DateTime.Parse(StringUtil.ConvertStringToDate(salesOrderCard.PostingDateStr));
            else
                salesOrderCard.PostingDate = null;
            
            if (!string.IsNullOrEmpty(salesOrderCard.DocumentDateStr))
                salesOrderCard.DocumentDate = DateTime.Parse(StringUtil.ConvertStringToDate(salesOrderCard.DocumentDateStr));
            else
                salesOrderCard.DocumentDate = null;
            salesOrderCard.ReceiptDate = null;
            var customerInfo = Mapper.Map<AgencyCard>(_agencyService.GetBy(salesOrderCard.CustomerId));
            salesOrderCard.CustomerName = customerInfo.FullName;
            salesOrderCard.CustomerAddress = customerInfo.Address;
            salesOrderCard.CustomerEmail = customerInfo.Email;
            salesOrderCard.CustomerPhoneNo = customerInfo.Phone;
            salesOrderCard.SalesType = salesType;
            
            var DurationInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("CARDINFO", salesOrderCard.DurationCode)) ?? new VM_OptionLine();
            var ReasonInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("REASON", salesOrderCard.ReasonCode)) ?? new VM_OptionLine();
            salesOrderCard.DurationCode = DurationInfo.Code;
            salesOrderCard.DurationName = DurationInfo.Name;
            salesOrderCard.ReasonId = ReasonInfo.LineNo;
            salesOrderCard.ReasonCode = ReasonInfo.Code;
            salesOrderCard.ReasonName = ReasonInfo.Name;

            var lineNo = 0;
            var statusInfo = new VM_OptionLine();
            salesOrderCard.Flows.OrderBy(d => d.Priority).ToList().ForEach(item => {
                lineNo += 100;
                item.Type = ApprovalType.PurchaseOrder;
                item.Code = salesOrderCard.Code;
                item.LineNo = lineNo;
                var _userInfo = Mapper.Map<UserVM>(_userService.GetBy(item.ApprovalNo));
                if (_userInfo != null)
                {
                    item.ApprovalName = _userInfo.FullName;
                    item.ApprovalEmail = _userInfo.Email;
                }
                statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("APPSTATUS", item.StatusCode));
                item.StatusId = statusInfo.LineNo;
                item.StatusCode = statusInfo.Code;
                item.StatusName = statusInfo.Name;
            });
            mSalesHeader = Mapper.Map<M_SalesHeader>(salesOrderCard);
        }
        public JsonResult LoadData(GridSettings grid)
        {
            var list = Mapper.Map<IEnumerable<SalesOrderList>>(_salesHdrService.FindAll(salesType, "100")).AsQueryable();
            list = JqGrid.SetupGrid<SalesOrderList>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
        public JsonResult ConfirmPo(string c)
        {
            try
            {
                salesOrderCard = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(salesType, c));
                var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("ORDERSTATUS", "800"));
                salesOrderCard.StatusId = statusInfo.LineNo;
                salesOrderCard.StatusCode = statusInfo.Code;
                salesOrderCard.StatusName = statusInfo.Name;
                hasValue = _salesHdrService.Update(Mapper.Map<M_SalesHeader>(salesOrderCard));
                hasValue = _vuViecService.Commit();
                //-- Gửi mail cho Tâm Trí Lực, cc đại lý
                var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
                var mailTemplate = Mapper.Map<Template>(_templateService.Get("POAGENCYSEND"));
                var mailTitle = mailTemplate.Title.Replace("{{PO_NO}}", salesOrderCard.Code);
                mailTitle = mailTitle.Replace("{{AGENCY}}", salesOrderCard.CustomerName);
                mailTitle = mailTitle.Replace("{{NOOFCARD}}", salesOrderCard.TotalCard.ToString());
                mailTitle = mailTitle.Replace("{{USER}}", salesOrderCard.CustomerName);
                mailTitle = mailTitle.Replace("{{STATUS_NAME}}", statusInfo.Name);
                var content = mailTemplate.Content.Replace("{{FULLNAME}}", salesOrderCard.CustomerName);
                content = content.Replace("{{PO_NO}}", salesOrderCard.Code);
                content = content.Replace("{{DOCUMENT_DATE}}", string.Format("{0:dd/MM/yyyy}", salesOrderCard.DocumentDate));
                content = content.Replace("{{AGENCY_NAME}}", salesOrderCard.CustomerName);
                content = content.Replace("{{AGENCY_ADD}}", salesOrderCard.CustomerAddress);
                content = content.Replace("{{AGENCY_EMAIL}}", salesOrderCard.CustomerEmail);
                content = content.Replace("{{AGENCY_PHONE}}", salesOrderCard.CustomerPhoneNo);
                content = content.Replace("{{NOOFCARD}}", string.Format("{0:#,##0}", salesOrderCard.NoOfCard));
                content = content.Replace("{{GIFT_CARD}}", string.Format("{0:#,##0}", salesOrderCard.GiftCard));
                content = content.Replace("{{AMOUNT}}", string.Format("{0:#,##0}", salesOrderCard.Amount));
                content = content.Replace("{{DISCOUNTPERCENT}}", string.Format("{0:#,##0}%", salesOrderCard.DiscountPercent));
                content = content.Replace("{{DISCOUNT_AMT}}", string.Format("{0:#,##0}", salesOrderCard.DiscountAmt));
                content = content.Replace("{{TOTALAMOUNT}}", string.Format("{0:#,##0}", salesOrderCard.TotalAmount));
                content = content.Replace("{{REMARK}}", salesOrderCard.Remark);

                var userIds = _userThuocNhomService.GetMany("SALES").Select(d => d.UserID).ToList() ?? new List<int>();
                var emailCCs = Mapper.Map<IEnumerable<UserVM>>(_userService.FindAll(userIds)).Where(d => !string.IsNullOrEmpty(d.Email)).Select(d => d.Email).ToList();
                var emailTos = new List<string>();
                emailTos.Add(salesOrderCard.CustomerEmail);
                var _mailHelper = new EmailHelper()
                {
                    EmailTos = emailTos,
                    DisplayName = emailInfo.DisplayName,
                    Title = mailTitle,
                    Body = content,
                    MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
                };
                hasValue = _mailHelper.DoSendMail();
                //-- Gửi mail cho Tâm Trí Lực +

                var _status = string.IsNullOrEmpty(hasValue) ? "Oke" : "Error";
                var _msg = string.IsNullOrEmpty(hasValue) ? "" : hasValue;
                return Json(new { s = _status, m = _msg }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { s = "Error", m = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        private bool AllowConfirm(string groupCode)
        {
            var userIds = _userThuocNhomService.GetMany(groupCode).Select(d => d.UserID).ToList() ?? new List<int>();
            return Mapper.Map<IEnumerable<UserVM>>(_userService.FindAll(userIds)).Where(d => d.Username.Equals(GetUser().Username)).Any();
        }
    }
}