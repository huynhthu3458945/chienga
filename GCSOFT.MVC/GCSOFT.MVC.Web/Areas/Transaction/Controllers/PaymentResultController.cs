﻿using AutoMapper;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Areas.Transaction.Data;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.Transaction.Controllers
{
    [CompressResponseAttribute]
    public class PaymentResultController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IPaymentResultService _paymentResultService;
        private string sysCategory = "PR";
        private bool? permission = false;
        private PaymentResultViewModel vmPaymentResult;
        private M_PaymentResult mPaymentResult;
        private PaymentType paymentType = PaymentType.Order;
        private string hasValue;
        #endregion
        public PaymentResultController
            (
                IVuViecService vuViecService
                , IPaymentResultService paymentResultService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _paymentResultService = paymentResultService;
        }
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }
        public JsonResult LoadData(GridSettings grid)
        {
            var list = Mapper.Map<IEnumerable<PaymentResultViewModel>>(_paymentResultService.FindAll(paymentType)).AsQueryable();
            list = JqGrid.SetupGrid<PaymentResultViewModel>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
    }
}