﻿using AutoMapper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Service.Transaction.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Areas.Agency.Data;
using GCSOFT.MVC.Web.Areas.MasterData.Models;
using GCSOFT.MVC.Web.Areas.Transaction.Data;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.Transaction.Controllers
{
    [CompressResponseAttribute]
    public class PotentialController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IPotentialCardService _potenticalCardService;
        private readonly IPotentialCustomerService _potenticalCustService;
        private readonly IClassService _classService;
        private readonly IAgencyService _agencyService;
        private readonly IProvinceService _provinceService;
        private readonly IDistrictService _districtService;
        private string sysCategory = "AD_POTENTICAL";
        private bool? permission = false;
        private string hasValue;
        private PotentialCard potentialCard;
        private M_PotentialCard mPotentialCard;
        #endregion
        public PotentialController
            (
                IVuViecService vuViecService
                , IPotentialCardService potenticalCardService
                , IPotentialCustomerService potenticalCustService
                , IClassService classService
                , IAgencyService agencyService
                , IProvinceService provinceService
                , IDistrictService districtService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _potenticalCardService = potenticalCardService;
            _potenticalCustService = potenticalCustService;
            _classService = classService;
            _agencyService = agencyService;
            _provinceService = provinceService;
            _districtService = districtService;
        }
        // GET: Transaction/Potential
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }
        public ActionResult Create()
        {
            try
            {
                #region -- Role user --
                permission = GetPermission(sysCategory, Authorities.Add);
                if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
                if (!permission.Value) return View("AccessDenied");
                #endregion
                potentialCard = new PotentialCard();
                potentialCard.Code = StringUtil.CheckLetter("PO", _potenticalCardService.GetMax(), 4);
                potentialCard.AgencyList = Mapper.Map<IEnumerable<AgencyList>>(_agencyService.FindAll(AgencyType.Agency, null)).ToSelectListItems(0);
                potentialCard.CityList = Mapper.Map<IEnumerable<ProvinceViewModel>>(_provinceService.FindAll()).ToSelectListItems(0);
                potentialCard.DistrictList = Mapper.Map<IEnumerable<DistrictViewModel>>(_districtService.FindAll(0)).ToSelectListItems(0);
                return View(potentialCard);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public ActionResult Edit(string id, int? p)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            try
            {
                potentialCard = Mapper.Map<PotentialCard>(_potenticalCardService.GetBy(id));
                if (potentialCard == null)
                    return HttpNotFound();
                potentialCard.AgencyList = Mapper.Map<IEnumerable<AgencyList>>(_agencyService.FindAll(AgencyType.Agency, null)).ToSelectListItems(potentialCard.AgencyId);
                potentialCard.CityList = Mapper.Map<IEnumerable<ProvinceViewModel>>(_provinceService.FindAll()).ToSelectListItems(potentialCard.CityId);
                potentialCard.DistrictList = Mapper.Map<IEnumerable<DistrictViewModel>>(_districtService.FindAll(potentialCard.CityId)).ToSelectListItems(potentialCard.DistrictId);
                return View(potentialCard);
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.ToString();
                return View("error");
            }
        }
        [HttpPost]
        public ActionResult Create(PotentialCard _potenticalCard)
        {
            try
            {
                SetData(_potenticalCard, true);
                hasValue = _potenticalCardService.Insert(mPotentialCard);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = mPotentialCard.Code, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult Edit(PotentialCard _potenticalCard)
        {
            try
            {
                SetData(_potenticalCard, false);
                hasValue = _potenticalCardService.Update(mPotentialCard);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = mPotentialCard.Code, msg = "1" });
                ViewBag.Error = hasValue;

                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public ActionResult Delete(string id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                hasValue = _potenticalCardService.Delete(id);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return Json(new { v = true, count = 1 });
                return Json("Xóa dữ liệu không thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        } 
        private void SetData(PotentialCard _potentialCard, bool isCreate)
        {
            var userInfo = GetUser();
            potentialCard = _potentialCard;
            if (isCreate)
                potentialCard.Code = StringUtil.CheckLetter("P", _potenticalCardService.GetMax(), 4);
            potentialCard.DateCreate = DateTime.Now;
            potentialCard.DateModify = DateTime.Now;
            potentialCard.UserCreate = GetUser().Username;
            var agencyInfo = Mapper.Map<AgencyCard>(_agencyService.GetBy2(potentialCard.AgencyId));
            potentialCard.AgencyNames = agencyInfo.FullName;
            var provinceInfo = Mapper.Map<ProvinceViewModel>(_provinceService.GetBy(potentialCard.CityId)) ?? new ProvinceViewModel();
            potentialCard.CityName = provinceInfo.name;
            var districtInfo = Mapper.Map<DistrictViewModel>(_districtService.GetBy(potentialCard.DistrictId)) ?? new DistrictViewModel();
            potentialCard.DistrictName = districtInfo.name;
            mPotentialCard = Mapper.Map<M_PotentialCard>(potentialCard);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="grid"></param>
        /// <param name="s">Status Codes</param>
        /// <returns></returns>
        public JsonResult LoadData(GridSettings grid)
        {
            var list = Mapper.Map<IEnumerable<PotentialList>>(_potenticalCardService.FindAll()).AsQueryable();
            list = JqGrid.SetupGrid<PotentialList>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
    }
}