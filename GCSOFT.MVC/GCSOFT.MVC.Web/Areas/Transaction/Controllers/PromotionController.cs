﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using AutoMapper;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Web.Areas.Transaction.Data;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.MasterData;

namespace GCSOFT.MVC.Web.Areas.Transaction.Controllers
{
    [CompressResponseAttribute]
    public class PromotionController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IPromotionService _promotionService;
        private bool? permission = false;
        private string sysCategory = "PROMOTION";
        private M_Promotion mPromotion;
        private string hasValue;
        #endregion

        public PromotionController(
            IVuViecService vuViecService,
            IPromotionService promotionService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _promotionService = promotionService;
        }

        /// <summary>   
        ///     Index
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [25/08/2021]
        /// </history>
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }
        /// <summary>
        ///     Create
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [25/08/2021]
        /// </history>
        public ActionResult Create()
        {
            ViewModelPromotion model = new ViewModelPromotion();
            model.FromDateStr = string.Format("{0:dd/MM/yyyy}", model.FromDate);
            model.ToDateStr = string.Format("{0:dd/MM/yyyy}", model.ToDate);
            return View(model);
        }

        /// <summary>
        ///     Edit
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [25/08/2021]
        /// </history>
        public ActionResult Edit(string id)
        {
            ViewModelPromotion model = Mapper.Map<ViewModelPromotion>(_promotionService.GetByCode(id));
            model.FromDateStr = string.Format("{0:dd/MM/yyyy}", model.FromDate);
            model.ToDateStr = string.Format("{0:dd/MM/yyyy}", model.ToDate);
            return View(model);
        }

        /// <summary>
        ///     Create
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [25/08/2021]
        /// </history>
        [HttpPost]
        public ActionResult Create(ViewModelPromotion model)
        {
            try
            {
                SetData(model);
                hasValue = _promotionService.Insert(mPromotion);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = model.Code, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        /// <summary>
        ///     Edit
        /// </summary>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [25/08/2021]
        /// </history>
        [HttpPost]
        public ActionResult Edit(ViewModelPromotion model)
        {
            try
            {
                SetData(model);
                hasValue = _promotionService.Update(mPromotion);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = model.Code, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

        /// <summary>
        /// SetData
        /// </summary>
        /// <param name="viewModelPromotion"></param>
        /// <history>
        ///     [Huỳnh Thử] Create [25/08/2021]
        /// </history>
        public void SetData(ViewModelPromotion viewModelPromotion)
        {
            if (!string.IsNullOrEmpty(viewModelPromotion.FromDateStr))
                viewModelPromotion.FromDate = DateTime.Parse(StringUtil.ConvertStringToDate(viewModelPromotion.FromDateStr));
            else
                viewModelPromotion.FromDate = null;

            if (!string.IsNullOrEmpty(viewModelPromotion.ToDateStr))
                viewModelPromotion.ToDate = DateTime.Parse(StringUtil.ConvertStringToDate(viewModelPromotion.ToDateStr));
            else
                viewModelPromotion.ToDate = null;

            mPromotion = Mapper.Map<M_Promotion>(viewModelPromotion);
        }

        /// <summary>
        /// Deletes
        /// </summary>
        /// <param name="id">Code</param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [25/08/2021]
        /// </history>
        public JsonResult Deletes(string id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                string[] _codes = id.Split(',').Select(item => item).ToArray();
                string hasValue = _promotionService.Remove(_codes);
                _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return Json(new { v = true, count = _codes.Count() });
                return Json("Xóa dữ liệu không thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }
        /// <summary>
        /// Deletes
        /// </summary>
        /// <param name="id">Code</param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [25/08/2021]
        /// </history>
        public JsonResult CheckUnique(string code)
        {
            try
            {
                var res = _promotionService.GetByCode(code);
                if (res != null) 
                    return Json(new {status = true }, JsonRequestBehavior.AllowGet);
                else 
                    return Json(new { status = false }, JsonRequestBehavior.AllowGet);
            }
            catch { return Json(new { status = false }, JsonRequestBehavior.AllowGet); }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="grid"></param>
        /// <param name="s">Status Codes</param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [25/08/2021]
        /// </history>
        public JsonResult LoadData(GridSettings grid, string s)
        {
            var statusCodes = s.Split(',').ToList();
            var list = Mapper.Map<IEnumerable<ViewModelPromotionList>>(_promotionService.GetAll()).AsQueryable();
            list = JqGrid.SetupGrid<ViewModelPromotionList>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
    }
}