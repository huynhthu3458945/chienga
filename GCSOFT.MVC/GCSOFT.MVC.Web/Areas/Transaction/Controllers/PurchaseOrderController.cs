﻿using AutoMapper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Model.Approval;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.ApprovalService.Interface;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Service.Transaction.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Areas.Agency.Data;
using GCSOFT.MVC.Web.Areas.Approval.Data;
using GCSOFT.MVC.Web.Areas.MasterData.Models;
using GCSOFT.MVC.Web.Areas.Transaction.Data;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.Transaction.Controllers
{
    [CompressResponseAttribute]
    public class PurchaseOrderController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly ISalesHeaderService _salesHdrService;
        private readonly ISalesLineService _salesLineService;
        private readonly IAgencyService _agencyService;
        private readonly IOptionLineService _optionLineService;
        private readonly ICardLineService _cardLineService;
        private readonly ICardEntryService _cardEntryService;
        private readonly IUserThuocNhomService _userThuocNhomService;
        private readonly IUserService _userService;
        private readonly IFlowService _flowService;
        private readonly IUserGroupService _userGroupService;
        private readonly ITemplateService _templateService;
        private readonly IMailSetupService _mailSetupService;
        private readonly IApprovalInfoService _approvalInfoService;
        private readonly IPromotionService _promotionService;
        private string sysCategory = "PO";
        private bool? permission = false;
        private SalesOrderCard salesOrderCard;
        private M_SalesHeader mSalesHeader;
        private SalesType salesType = SalesType.PurchaseOrder;
        private string hasValue;
        #endregion
        public PurchaseOrderController
            (
                IVuViecService vuViecService
                , ISalesHeaderService salesHdrService
                , ISalesLineService salesLineService
                , IAgencyService agencyService
                , IOptionLineService optionLineService
                , ICardLineService cardLineService
                , ICardEntryService cardEntryService
                , IUserThuocNhomService userThuocNhomService
                , IUserService userService
                , IFlowService flowService
                , IUserGroupService userGroupService
                , ITemplateService templateService
                , IMailSetupService mailSetupService
                , IApprovalInfoService approvalInfoService
                , IPromotionService promotionService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _salesHdrService = salesHdrService;
            _salesLineService = salesLineService;
            _agencyService = agencyService;
            _optionLineService = optionLineService;
            _cardLineService = cardLineService;
            _cardEntryService = cardEntryService;
            _userThuocNhomService = userThuocNhomService;
            _userService = userService;
            _flowService = flowService;
            _userGroupService = userGroupService;
            _templateService = templateService;
            _mailSetupService = mailSetupService;
            _approvalInfoService = approvalInfoService;
            _promotionService = promotionService;
        }
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }
        public ActionResult Create()
        {
            try
            {
                #region -- Role user --
                permission = GetPermission(sysCategory, Authorities.Add);
                if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
                if (!permission.Value) return View("AccessDenied");
                #endregion
                salesOrderCard = new SalesOrderCard();
                salesOrderCard.Code = StringUtil.CheckLetter("PO", _salesHdrService.GetMax(salesType), 4);
                salesOrderCard.DocumentDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.DocumentDate);
                salesOrderCard.CustomerList = Mapper.Map<IEnumerable<AgencyList>>(_agencyService.FindAll(AgencyType.Agency, null)).ToSelectListItems(0);
                var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("ORDERSTATUS", "700"));
                salesOrderCard.StatusId = statusInfo.LineNo;
                salesOrderCard.StatusCode = statusInfo.Code;
                salesOrderCard.StatusName = statusInfo.Name;
                salesOrderCard.DurationList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("CARDINFO")).ToSelectListItems("");
                salesOrderCard.ReasonList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("REASON")).ToSelectListItems("");
                salesOrderCard.LevelList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("LV")).ToSelectListItems("");
                var promotionList = _promotionService.GetAll();
                salesOrderCard.PromotionList = Mapper.Map<IEnumerable<VM_Promotion>>(promotionList.Where(z => z.FromDate.Value.Date <= DateTime.Now.Date && DateTime.Now.Date <= z.ToDate.Value.Date)).ToSelectListItems(string.Empty);
                ViewBag.IsEdit = false;
                var _flow = new FlowViewModel();
                var _flows = new List<FlowViewModel>();
                statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("APPSTATUS", "100")); //Chờ Duyệt

                var userIds = _userThuocNhomService.GetMany("APPROVAL_SALES").Select(d => d.UserID).ToList() ?? new List<int>();
                _flow = new FlowViewModel()
                {
                    Type = ApprovalType.PurchaseOrder,
                    Code = salesOrderCard.Code,
                    LineNo = 100,
                    GroupCode = "APPROVAL_SALES",
                    GroupName = Mapper.Map<NhomUserCard>(_userGroupService.GetBy2("APPROVAL_SALES")).Name,
                    ApprovalNo = string.Empty,
                    Priority = 100,
                    StatusId = statusInfo.LineNo,
                    StatusCode = statusInfo.Code,
                    StatusName = statusInfo.Name,
                    ApprovalList = Mapper.Map<IEnumerable<UserVM>>(_userService.FindAll(userIds)).ToList().ToSelectListItems("")
                };
                _flows.Add(_flow);
                userIds = _userThuocNhomService.GetMany("APPROVAL_ACC").Select(d => d.UserID).ToList() ?? new List<int>();
                _flow = new FlowViewModel()
                {
                    Type = ApprovalType.PurchaseOrder,
                    Code = salesOrderCard.Code,
                    LineNo = 200,
                    GroupCode = "APPROVAL_ACC",
                    GroupName = Mapper.Map<NhomUserCard>(_userGroupService.GetBy2("APPROVAL_ACC")).Name,
                    ApprovalNo = string.Empty,
                    Priority = 200,
                    StatusId = statusInfo.LineNo,
                    StatusCode = statusInfo.Code,
                    StatusName = statusInfo.Name,
                    ApprovalList = Mapper.Map<IEnumerable<UserVM>>(_userService.FindAll(userIds)).ToList().ToSelectListItems("")
                };
                _flows.Add(_flow);
                salesOrderCard.Flows = _flows;
                ViewBag.IsMakeOrder = "disabled";
                ViewBag.isSendApproval = "disabled";
                ViewBag.isConfirm = "disabled";
                return View(salesOrderCard);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
            
        }
        public ActionResult Edit(string id, int? p)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            try
            {
                salesOrderCard = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(salesType, id));
                if (salesOrderCard == null)
                    return HttpNotFound();
                salesOrderCard.DocumentDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.DocumentDate);
                salesOrderCard.PostingDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.PostingDate);
                salesOrderCard.CustomerList = Mapper.Map<IEnumerable<AgencyList>>(_agencyService.FindAll(AgencyType.Agency, null)).ToSelectListItems(salesOrderCard.CustomerId);
                salesOrderCard.Flows = Mapper.Map<IEnumerable<FlowViewModel>>(_flowService.FindAll(ApprovalType.PurchaseOrder, salesOrderCard.Code));
                if(!string.IsNullOrEmpty(salesOrderCard.DurationCode))
                    salesOrderCard.DurationList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("CARDINFO")).ToSelectListItems(salesOrderCard.DurationCode);
                else
                {
                    string durationCode = string.Empty;
                    switch (salesOrderCard.LevelCode)
                    {

                        case "100":
                            durationCode = "ALL";
                            break;
                        case "300":
                            durationCode = "1Y";
                            break;
                        case "500":
                            durationCode = "2Y";
                            break;
                        case "600":
                            durationCode = "3Y";
                            break;
                        case "700":
                            durationCode = "4Y";
                            break;
                        case "800":
                            durationCode = "5YY";
                            break;

                    }
                    salesOrderCard.DurationList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("CARDINFO")).ToSelectListItems(durationCode);
                }
                salesOrderCard.ReasonList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("REASON")).ToSelectListItems(salesOrderCard.ReasonCode);
                salesOrderCard.LevelList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("LV")).ToSelectListItems(salesOrderCard.LevelCode);
                var promotionList = _promotionService.GetAll();
                salesOrderCard.PromotionList = Mapper.Map<IEnumerable<VM_Promotion>>(promotionList.Where(z => z.FromDate.Value.Date <= DateTime.Now.Date && DateTime.Now.Date <= z.ToDate.Value.Date)).ToSelectListItems(salesOrderCard.PromotionCode);
                var userIds = _userThuocNhomService.GetMany("APPROVAL_SALES").Select(d => d.UserID).ToList() ?? new List<int>();
                if (salesOrderCard.Flows.Count() == 0)
                {
                    var _flow = new FlowViewModel();
                    var _flows = new List<FlowViewModel>();
                    var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("APPSTATUS", "100")); //Chờ Duyệt

                    _flow = new FlowViewModel()
                    {
                        Type = ApprovalType.PurchaseOrder,
                        Code = salesOrderCard.Code,
                        LineNo = 100,
                        GroupCode = "APPROVAL_SALES",
                        GroupName = Mapper.Map<NhomUserCard>(_userGroupService.GetBy2("APPROVAL_SALES")).Name,
                        ApprovalNo = string.Empty,
                        Priority = 100,
                        StatusId = statusInfo.LineNo,
                        StatusCode = statusInfo.Code,
                        StatusName = statusInfo.Name,
                        ApprovalList = Mapper.Map<IEnumerable<UserVM>>(_userService.FindAll(userIds)).ToList().ToSelectListItems("")
                    };
                    _flows.Add(_flow);
                    userIds = _userThuocNhomService.GetMany("APPROVAL_ACC").Select(d => d.UserID).ToList() ?? new List<int>();
                    _flow = new FlowViewModel()
                    {
                        Type = ApprovalType.PurchaseOrder,
                        Code = salesOrderCard.Code,
                        LineNo = 200,
                        GroupCode = "APPROVAL_ACC",
                        GroupName = Mapper.Map<NhomUserCard>(_userGroupService.GetBy2("APPROVAL_ACC")).Name,
                        ApprovalNo = string.Empty,
                        Priority = 200,
                        StatusId = statusInfo.LineNo,
                        StatusCode = statusInfo.Code,
                        StatusName = statusInfo.Name,
                        ApprovalList = Mapper.Map<IEnumerable<UserVM>>(_userService.FindAll(userIds)).ToList().ToSelectListItems("")
                    };
                    _flows.Add(_flow);
                    salesOrderCard.Flows = _flows;
                }
                userIds = new List<int>();
                salesOrderCard.Flows.ToList().ForEach(item => {
                    userIds = _userThuocNhomService.GetMany(item.GroupCode).Select(d => d.UserID).ToList() ?? new List<int>();
                    item.ApprovalList = Mapper.Map<IEnumerable<UserVM>>(_userService.FindAll(userIds)).ToList().ToSelectListItems(item.ApprovalNo);
                });
                ViewBag.IsEdit = true;
                //ViewBag.isConfirm = salesOrderCard.StatusCode.Equals("700") ? "" : "disabled";
                //ViewBag.isSendApproval = salesOrderCard.StatusCode.Equals("800") || salesOrderCard.StatusCode.Equals("500") ? "" : "disabled";
                ViewBag.isSendApproval = salesOrderCard.StatusCode.Equals("700") || salesOrderCard.StatusCode.Equals("500") ? "" : "disabled";
                ViewBag.IsMakeOrder = salesOrderCard.StatusCode.Equals("400") && !string.IsNullOrEmpty(salesOrderCard.LotNumber) ? "" : "disabled";
                var statusAllowRejects = new List<string>();
                statusAllowRejects.Add("300");
                statusAllowRejects.Add("400");
                statusAllowRejects.Add("700");
                statusAllowRejects.Add("800");
                
                ViewBag.IsReject = statusAllowRejects.Contains(salesOrderCard.StatusCode) ? "" : "disabled";
                ViewBag.userName = GetUser().Username;
                return View(salesOrderCard);
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.ToString();
                return View("error");
            }
        }
        [HttpPost]
        public ActionResult Create(SalesOrderCard _salesOrderCard)
        {
            try
            {
                SetData(_salesOrderCard, true);
                hasValue = _salesHdrService.Insert(mSalesHeader);
                hasValue = _flowService.Delete(ApprovalType.PurchaseOrder, mSalesHeader.Code);
                hasValue = _flowService.Insert(mSalesHeader.Flows);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = mSalesHeader.Code, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult Edit(SalesOrderCard _salesOrderCard)
        {
            try
            {
                SetData(_salesOrderCard, false);
                _salesHdrService.Update(mSalesHeader);
                _flowService.Delete(ApprovalType.PurchaseOrder, mSalesHeader.Code);
                _flowService.Insert(mSalesHeader.Flows);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = mSalesHeader.Code, msg = "1" });
                ViewBag.Error = hasValue;
                
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public ActionResult Delete(string id)
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                hasValue = _salesHdrService.Delete(salesType, id);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return Json(new { v = true, count = 1 });
                return Json("Xóa dữ liệu không thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }
        private void SetData(SalesOrderCard _salesOrderCard, bool isCreate)
        {
            var userInfo = GetUser();
            salesOrderCard = _salesOrderCard;
            if (isCreate)
            {
                salesOrderCard.Code = StringUtil.CheckLetter("PO", _salesHdrService.GetMax(salesType), 4);
                salesOrderCard.CreateBy = userInfo.Username;
                salesOrderCard.DateCreate = DateTime.Now;
                salesOrderCard.LastModifyBy = userInfo.Username;
                salesOrderCard.LastModifyDate = salesOrderCard.DateCreate;
            }
            else
            {
                salesOrderCard.LastModifyBy = userInfo.Username;
                salesOrderCard.LastModifyDate = DateTime.Now;
            }
            if (!string.IsNullOrEmpty(salesOrderCard.PostingDateStr))
                salesOrderCard.PostingDate = DateTime.Parse(StringUtil.ConvertStringToDate(salesOrderCard.PostingDateStr));
            else
                salesOrderCard.PostingDate = null;
            
            if (!string.IsNullOrEmpty(salesOrderCard.DocumentDateStr))
                salesOrderCard.DocumentDate = DateTime.Parse(StringUtil.ConvertStringToDate(salesOrderCard.DocumentDateStr));
            else
                salesOrderCard.DocumentDate = null;
            salesOrderCard.ReceiptDate = null;
            var customerInfo = Mapper.Map<AgencyCard>(_agencyService.GetBy2(salesOrderCard.CustomerId));
            salesOrderCard.CustomerName = customerInfo.FullName;
            salesOrderCard.CustomerAddress = customerInfo.Address;
            salesOrderCard.CustomerEmail = customerInfo.Email;
            salesOrderCard.CustomerPhoneNo = customerInfo.Phone;
            salesOrderCard.SalesType = salesType;
            
            var DurationInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("CARDINFO", salesOrderCard.DurationCode)) ?? new VM_OptionLine();
            var ReasonInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("REASON", salesOrderCard.ReasonCode)) ?? new VM_OptionLine();
            var LevelInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("LV", salesOrderCard.LevelCode)) ?? new VM_OptionLine();
            salesOrderCard.DurationId = DurationInfo.LineNo;
            salesOrderCard.DurationCode = DurationInfo.Code;
            salesOrderCard.DurationName = DurationInfo.Name;
            salesOrderCard.ReasonId = ReasonInfo.LineNo;
            salesOrderCard.ReasonCode = ReasonInfo.Code;
            salesOrderCard.ReasonName = ReasonInfo.Name;
            salesOrderCard.LevelId = LevelInfo.LineNo;
            salesOrderCard.LevelCode = LevelInfo.Code;
            salesOrderCard.LevelName = LevelInfo.Name;

            var lineNo = 0;
            var statusInfo = new VM_OptionLine();
            salesOrderCard.Flows.OrderBy(d => d.Priority).ToList().ForEach(item => {
                lineNo += 100;
                item.Type = ApprovalType.PurchaseOrder;
                item.Code = salesOrderCard.Code;
                item.LineNo = lineNo;
                var _userInfo = Mapper.Map<UserVM>(_userService.GetBy(item.ApprovalNo));
                if (_userInfo != null)
                {
                    item.ApprovalName = _userInfo.FullName;
                    item.ApprovalEmail = _userInfo.Email;
                }
                statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("APPSTATUS", item.StatusCode));
                item.StatusId = statusInfo.LineNo;
                item.StatusCode = statusInfo.Code;
                item.StatusName = statusInfo.Name;
            });
            mSalesHeader = Mapper.Map<M_SalesHeader>(salesOrderCard);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="grid"></param>
        /// <param name="s">Status Codes</param>
        /// <returns></returns>
        public JsonResult LoadData(GridSettings grid, string s)
        {
            var statusCodes = s.Split(',').ToList();
            var list = Mapper.Map<IEnumerable<SalesOrderList>>(_salesHdrService.FindAll(salesType, statusCodes)).AsQueryable();
            list = JqGrid.SetupGrid<SalesOrderList>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
        public JsonResult ConfirmPo(string c)
        {
            try
            {
                salesOrderCard = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(salesType, c));
                var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("ORDERSTATUS", "800"));
                salesOrderCard.StatusId = statusInfo.LineNo;
                salesOrderCard.StatusCode = statusInfo.Code;
                salesOrderCard.StatusName = statusInfo.Name;
                hasValue = _salesHdrService.Update(Mapper.Map<M_SalesHeader>(salesOrderCard));
                hasValue = _vuViecService.Commit();
                //-- Gửi mail cho Tâm Trí Lực, cc đại lý
                var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
                var mailTemplate = Mapper.Map<Template>(_templateService.Get("POAGENCYSEND"));
                var mailTitle = mailTemplate.Title.Replace("{{PO_NO}}", salesOrderCard.Code);
                mailTitle = mailTitle.Replace("{{AGENCY}}", salesOrderCard.CustomerName);
                mailTitle = mailTitle.Replace("{{NOOFCARD}}", salesOrderCard.TotalCard.ToString());
                mailTitle = mailTitle.Replace("{{USER}}", salesOrderCard.CustomerName);
                mailTitle = mailTitle.Replace("{{STATUS_NAME}}", statusInfo.Name);
                var content = mailTemplate.Content.Replace("{{FULLNAME}}", salesOrderCard.CustomerName);
                content = content.Replace("{{PO_NO}}", salesOrderCard.Code);
                content = content.Replace("{{DOCUMENT_DATE}}", string.Format("{0:dd/MM/yyyy}", salesOrderCard.DocumentDate));
                content = content.Replace("{{AGENCY_NAME}}", salesOrderCard.CustomerName);
                content = content.Replace("{{AGENCY_ADD}}", salesOrderCard.CustomerAddress);
                content = content.Replace("{{AGENCY_EMAIL}}", salesOrderCard.CustomerEmail);
                content = content.Replace("{{AGENCY_PHONE}}", salesOrderCard.CustomerPhoneNo);
                content = content.Replace("{{NOOFCARD}}", string.Format("{0:#,##0}", salesOrderCard.NoOfCard));
                content = content.Replace("{{GIFT_CARD}}", string.Format("{0:#,##0}", salesOrderCard.GiftCard));
                content = content.Replace("{{AMOUNT}}", string.Format("{0:#,##0}", salesOrderCard.Amount));
                content = content.Replace("{{DISCOUNTPERCENT}}", string.Format("{0:#,##0}%", salesOrderCard.DiscountPercent));
                content = content.Replace("{{DISCOUNT_AMT}}", string.Format("{0:#,##0}", salesOrderCard.DiscountAmt));
                content = content.Replace("{{TOTALAMOUNT}}", string.Format("{0:#,##0}", salesOrderCard.TotalAmount));
                content = content.Replace("{{REMARK}}", salesOrderCard.Remark);

                var userIds = _userThuocNhomService.GetMany("SALES").Select(d => d.UserID).ToList() ?? new List<int>();
                var emailCCs = Mapper.Map<IEnumerable<UserVM>>(_userService.FindAll(userIds)).Where(d => !string.IsNullOrEmpty(d.Email)).Select(d => d.Email).ToList();
                var emailTos = new List<string>();
                emailTos.Add(salesOrderCard.CustomerEmail);
                var _mailHelper = new EmailHelper()
                {
                    EmailTos = emailTos,
                    DisplayName = emailInfo.DisplayName,
                    Title = mailTitle,
                    Body = content,
                    MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
                };
                hasValue = _mailHelper.DoSendMail();
                //-- Gửi mail cho Tâm Trí Lực +

                var _status = string.IsNullOrEmpty(hasValue) ? "Oke" : "Error";
                var _msg = string.IsNullOrEmpty(hasValue) ? "" : hasValue;
                return Json(new { s = _status, m = _msg }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { s = "Error", m = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult RejectPO(string c)
        {
            try
            {
                var userInfo = GetUser();
                var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("ORDERSTATUS", "1100"));
                var purchaseOrder = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(SalesType.PurchaseOrder, c));
                purchaseOrder.StatusId = statusInfo.LineNo;
                purchaseOrder.StatusCode = statusInfo.Code;
                purchaseOrder.StatusName = statusInfo.Name;
                hasValue = _salesHdrService.Update(Mapper.Map<M_SalesHeader>(purchaseOrder));

                statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("APPSTATUS", "300"));
                var approvalInfo = new ApprovalInfoViewModel()
                {
                    Type = ApprovalType.PurchaseOrder,
                    Code = c,
                    SendBy = userInfo.Username,
                    SendByName = userInfo.FullName,
                    SendDate = DateTime.Now,
                    StatusId = statusInfo.LineNo,
                    StatusCode = statusInfo.Code,
                    StatusName = statusInfo.Name,
                    ApprovalNo = userInfo.Username,
                    ApprovalName = userInfo.FullName,
                    ApprovalEmail = userInfo.Email,
                    Description = string.Format("{0} của đại lý {1} số lượng thẻ {2}", purchaseOrder.Code, purchaseOrder.CustomerName, purchaseOrder.TotalCard)
                };
                hasValue = _approvalInfoService.Delete(ApprovalType.PurchaseOrder, c);
                hasValue = _approvalInfoService.Insert(Mapper.Map<M_ApprovalInfo>(approvalInfo));
                hasValue = _vuViecService.Commit();
                var _status = string.IsNullOrEmpty(hasValue) ? "Oke" : "Error";
                var _msg = string.IsNullOrEmpty(hasValue) ? "" : hasValue;
                return Json(new { s = _status, m = _msg }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { s = "Error", m = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}