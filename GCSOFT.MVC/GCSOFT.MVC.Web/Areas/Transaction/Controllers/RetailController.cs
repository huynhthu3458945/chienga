﻿using AutoMapper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Areas.MasterData.Models;
using GCSOFT.MVC.Web.Areas.Transaction.Data;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using LinqToExcel;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.Transaction.Controllers
{
    [CompressResponseAttribute]
    public class RetailController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly ICardHeaderService _cardHeaderService;
        private readonly ICardLineService _cardLineService;
        private readonly ICardEntryService _cardEntryService;
        private readonly IOptionLineService _optionLineService;
        private readonly ICustomerService _customerService;
        private readonly ITemplateService _templateService;
        private readonly IMailSetupService _mailSetupService;
        private string sysCategory = "RETAIL";
        private bool? permission = false;
        private CardHeaderCard cardHeader;
        private M_CardHeader mCardHeader;
        private string hasValue;
        private string fileNameDK = String.Empty;
        private string filePathSave = String.Empty;
        private CardType cardType = CardType.CustomerCard;
        #endregion

        #region -- Contructor --
        public RetailController
            (
                IVuViecService vuViecService
                , ICardHeaderService cardHeaderService
                , ICardLineService cardLineService
                , ICardEntryService cardEntryService
                , IOptionLineService optionLineService
                , ICustomerService customerService
                , ITemplateService templateService
                , IMailSetupService mailSetupService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _cardHeaderService = cardHeaderService;
            _cardLineService = cardLineService;
            _cardEntryService = cardEntryService;
            _optionLineService = optionLineService;
            _customerService = customerService;
            _templateService = templateService;
            _mailSetupService = mailSetupService;
        }
        #endregion
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }
        public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            cardHeader = new CardHeaderCard();
            cardHeader.LotNumber = StringUtil.CheckLetter("LO", _cardHeaderService.GetMax(), 3);
            var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("TTT", "50"));
            cardHeader.StatusName = statusInfo.Name;
            cardHeader.DurationList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("CARDINFO")).ToSelectListItems("");
            cardHeader.ReasonList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("REASON")).ToSelectListItems("");
            cardHeader.UserFullName = GetUser().FullName;
            ViewBag.IsAllowGeneralSeri = (GetPermission(sysCategory, Authorities.Access) ?? false) ? "" : "disabled";
            ViewBag.IsAllowSendMail = (GetPermission(sysCategory, Authorities.Access) ?? false) ? "" : "disabled";
            return View(cardHeader); 
        }
       
        public ActionResult Edit(string id, int? p)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            cardHeader = Mapper.Map<CardHeaderCard>(_cardHeaderService.GetBy(id));
            if (cardHeader == null)
                return HttpNotFound();
            cardHeader.DurationList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("CARDINFO")).ToSelectListItems(cardHeader.DurationCode);
            cardHeader.ReasonList = Mapper.Map<IEnumerable<VM_OptionLine>>(_optionLineService.FindAll("REASON")).ToSelectListItems(cardHeader.ReasonCode);

            ViewBag.IsAllowGeneralSeri = (GetPermission(sysCategory, Authorities.Access) ?? false) ? "" : "disabled";
            ViewBag.IsAllowSendMail = (GetPermission(sysCategory, Authorities.Access) ?? false)  ? "" : "disabled";
            return View(cardHeader);
        }
        [HttpPost]
        public ActionResult Create(CardHeaderCard _cardHdr, HttpPostedFileBase FileUpload)
        {
            try
            {
                SetData(_cardHdr, true, FileUpload);
                _cardHeaderService.Insert(mCardHeader);
                hasValue = _customerService.Insert(mCardHeader.Customers);
                _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = mCardHeader.LotNumber, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult Edit(CardHeaderCard _cardHdr, HttpPostedFileBase FileUpload)
        {
            try
            {
                SetData(_cardHdr, false, FileUpload);
                hasValue = _cardHeaderService.Update(mCardHeader);
                hasValue = _customerService.Delete(mCardHeader.LotNumber);
                hasValue = _customerService.Insert(mCardHeader.Customers);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = mCardHeader.LotNumber, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        public JsonResult LoadData(GridSettings grid)
        {
            var list = Mapper.Map<IEnumerable<CardHeaderList>>(_cardHeaderService.FindAll(cardType)).AsQueryable();
            list = JqGrid.SetupGrid<CardHeaderList>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="grid"></param>
        /// <param name="lotNumber">Lot number</param>
        /// <param name="se">Status Email</param>
        /// <param name="fn">Full name</param>
        /// <param name="p">Phone</param>
        /// <param name="e">Email</param>
        /// <returns></returns>
        public JsonResult LoadDataCustomer(GridSettings grid, string lotNumber, string se,string fn,string p,string e)
        {
            var list = Mapper.Map<IEnumerable<CustomerViewModel>>(_customerService.FindAll(lotNumber, se, fn, p,e)).AsQueryable();
            foreach (var item in list)
            {
                if (!string.IsNullOrEmpty(item.CardNo))
                    item.SerialNumber = CryptorEngine.Decrypt(item.CardNo, true, "TTLSTNHD@CARD");
            }
            list = JqGrid.SetupGrid<CustomerViewModel>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ln">Lot Number</param>
        /// <param name="se">Status Email</param>
        /// <param name="fn">Full Name</param>
        /// <param name="p">Phone</param>
        /// <param name="e">Email</param>
        public void Download(string ln, string se, string fn, string p, string e)
        {
            var list = Mapper.Map<IEnumerable<CustomerViewModel>>(_customerService.FindAll(ln, se, fn, p, e));
            ExcelPackage Ep = new ExcelPackage();
            ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("Thong Tin");
            Sheet.Cells["A1"].Value = "Mã Học Viên";
            Sheet.Cells["B1"].Value = "Họ Tên";
            Sheet.Cells["C1"].Value = "Di Động";
            Sheet.Cells["D1"].Value = "Email";
            Sheet.Cells["E1"].Value = "Kiểm Tra Email";
            Sheet.Cells["F1"].Value = "Owner";
            Sheet.Cells["G1"].Value = "Số Seri";
            Sheet.Cells["H1"].Value = "Mã Kích Hoạt";
            //int row = 2;
            //foreach (var item in list)
            //{
            //    Sheet.Cells[string.Format("A{0}", row)].Value = item.Code;
            //    Sheet.Cells[string.Format("B{0}", row)].Value = item.FullName;
            //    Sheet.Cells[string.Format("C{0}", row)].Value = item.PhoneNo;
            //    Sheet.Cells[string.Format("D{0}", row)].Value = item.Email;
            //    Sheet.Cells[string.Format("E{0}", row)].Value = item.IsEmail;
            //    Sheet.Cells[string.Format("F{0}", row)].Value = item.Owner;
            //    Sheet.Cells[string.Format("G{0}", row)].Value = item.SerialNumber;
            //    Sheet.Cells[string.Format("H{0}", row)].Value = CryptorEngine.Decrypt(item.CardNo, true, "TTLSTNHD@CARD");
            //    row++;
            //}
            Sheet.Cells["A:AZ"].AutoFitColumns();
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment: filename=" + "ThongTinChung.xlsx");
            Response.BinaryWrite(Ep.GetAsByteArray());
            Response.End();
        }
        //public MemoryStream GetStream(XLWorkbook excelWorkbook)
        //{
        //    MemoryStream fs = new MemoryStream();
        //    excelWorkbook.SaveAs(fs);
        //    fs.Position = 0;
        //    return fs;
        //}
        private void SetData(CardHeaderCard _cardHdr, bool isCreate, HttpPostedFileBase FileUpload)
        {
            cardHeader = _cardHdr;
            if (isCreate)
            {
                cardHeader.LotNumber = StringUtil.CheckLetter("LO", _cardHeaderService.GetMax(), 3);
                cardHeader.DateCreate = DateTime.Now;
                cardHeader.DateOrder = DateTime.Now;
                var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("TTT", "100"));
                
                cardHeader.StatusId = statusInfo.LineNo;
                cardHeader.StatusCode = statusInfo.Code;
                cardHeader.StatusName = statusInfo.Name;
                cardHeader.UserName = GetUser().Username;
                cardHeader.UserFullName = GetUser().FullName;
            }
            var DurationInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("CARDINFO", cardHeader.DurationCode));
            var ReasonInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("REASON", cardHeader.ReasonCode));
            cardHeader.DurationId = DurationInfo.LineNo;
            cardHeader.DurationCode = DurationInfo.Code;
            cardHeader.DurationName = DurationInfo.Name;

            cardHeader.ReasonId = ReasonInfo.LineNo;
            cardHeader.ReasonCode = ReasonInfo.Code;
            cardHeader.ReasonName = ReasonInfo.Name;
            cardHeader.CardType = cardType;
            //-- Upload file
            if (FileUpload != null)
            {
                XoaAllFile();
                if (FileUpload.ContentType == "application/vnd.ms-excel" || FileUpload.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                {
                    string filename = FileUpload.FileName;
                    string targetpath = Server.MapPath("~/Files/CapMaKichHoat/");
                    if (!Directory.Exists(targetpath))
                        Directory.CreateDirectory(targetpath);
                    FileUpload.SaveAs(targetpath + filename);
                    string pathToExcelFile = targetpath + filename;
                    var connectionString = "";
                    if (filename.EndsWith(".xls"))
                    {
                        connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=Excel 8.0;", pathToExcelFile);
                    }
                    else if (filename.EndsWith(".xlsx"))
                    {
                        connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";", pathToExcelFile);
                    }
                    var adapter = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", connectionString);
                    var ds = new DataSet();
                    adapter.Fill(ds, "ExcelTable");
                    DataTable dtable = ds.Tables["ExcelTable"];
                    string sheetName = "Sheet1";
                    var excelFile = new ExcelQueryFactory(pathToExcelFile);
                    var customers = from a in excelFile.Worksheet<CustomerViewModel>(sheetName) select a;
                    var userInfo = GetUser();
                    var _customer = new CustomerViewModel();
                    var _customers = new List<CustomerViewModel>();
                    foreach (var item in customers)
                    {
                        _customer = item;
                        _customer.LotNumber = cardHeader.LotNumber;
                        _customer.SerialNumber = null;
                        _customer.CardNo = null;
                        _customer.UserOwner = userInfo.Username;
                        _customer.IsEmail = StringUtil.Validate(item.Email) ? "Đúng" : "Sai Email";
                        _customers.Add(_customer);
                    }
                    cardHeader.Customers = _customers;
                }
            }
            //-- Upload file +
            mCardHeader = Mapper.Map<M_CardHeader>(cardHeader);
        }
        private string XoaAllFile()
        {
            try
            {
                string[] filePaths = Directory.GetFiles(HttpContext.Server.MapPath("~/Files/CapMaKichHoat/"));
                foreach (string filePath in filePaths)
                    System.IO.File.Delete(filePath);
                return null;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="l">Lot number</param>
        /// <returns></returns>
        public JsonResult GeneralSerinumber(string l)
        {
            try
            {
                var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("TTT", "100"));
                cardHeader = Mapper.Map<CardHeaderCard>(_cardHeaderService.GetBy(l));
                cardHeader.StatusId = statusInfo.LineNo;
                cardHeader.StatusCode = statusInfo.Code;
                cardHeader.StatusName = statusInfo.Name;

                var cardLine = new CardLine();
                var cardLines = new List<CardLine>();
                var idCard = _cardLineService.GetID() - 1;

                var cardEntry = new CardEntry();
                var cardEntries = new List<CardEntry>();

                var randomNumber = new RandomNumberGenerator();
                var builder = new StringBuilder();

                statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("CARD", "450"));
                var _customers = Mapper.Map<IEnumerable<CustomerViewModel>>(_customerService.FindAll(l));
                var _carNo = "";
                var cryptorEngine = new CryptorEngine2();
                foreach (var item in _customers)
                {
                    cryptorEngine = new CryptorEngine2();
                    cryptorEngine = new CryptorEngine2();
                    builder = new StringBuilder();
                    Guid g = Guid.NewGuid();
                    builder.Append(g.ToString().Split('-')[0].ToString().ToUpper());
                    randomNumber = new RandomNumberGenerator();
                    builder.Append(randomNumber.RandomNumber(10, 99));
                    _carNo = builder.ToString();
                    idCard += 1;
                    cardLine = new CardLine()
                    {
                        Id = idCard,
                        LotNumber = cardHeader.LotNumber,
                        SerialNumber = StringUtil.GetProductID("C", (idCard - 1).ToString(), 9),
                        CardNo = cryptorEngine.Encrypt(_carNo, true, "TTLSTNHD@CARD"),
                        Price = cardHeader.Price,
                        UserName = cardHeader.UserName,
                        UserFullName = cardHeader.UserFullName,
                        DateCreate = cardHeader.DateCreate,

                        UserType = UserType.TTL,
                        UserNameActive = cardHeader.UserName,
                        UserActiveFullName = cardHeader.UserFullName,
                        LastDateUpdate = cardHeader.DateCreate,
                        StatusId = statusInfo.LineNo,
                        StatusCode = statusInfo.Code,
                        StatusName = statusInfo.Name,
                        LastStatusId = statusInfo.LineNo,
                        LastStatusCode = statusInfo.Code,
                        LastStatusName = statusInfo.Name,
                        DurationId = cardHeader.DurationId,
                        DurationCode = cardHeader.DurationCode,
                        DurationName = cardHeader.DurationName,
                        ReasonId = cardHeader.ReasonId,
                        ReasonCode = cardHeader.ReasonCode,
                        ReasonName = cardHeader.ReasonName
                    };
                    cardLines.Add(cardLine);

                    cardEntry = new CardEntry()
                    {
                        LotNumber = cardHeader.LotNumber,
                        CardLineId = idCard,
                        SerialNumber = cardLine.SerialNumber,
                        CardNo = cardLine.CardNo,
                        Price = cardLine.Price,
                        UserName = cardLine.UserName,
                        UserFullName = cardLine.UserFullName,
                        DateCreate = cardLine.DateCreate,
                        UserType = UserType.TTL,
                        UserNameActive = cardLine.UserNameActive,
                        UserActiveFullName = cardLine.UserActiveFullName,
                        Date = cardLine.DateCreate,
                        StatusId = statusInfo.LineNo,
                        StatusCode = statusInfo.Code,
                        StatusName = statusInfo.Name,
                        Source = CardSource.CardSTNHD,
                        SourceNo = cardHeader.LotNumber,
                        DurationId = cardHeader.DurationId,
                        DurationCode = cardHeader.DurationCode,
                        DurationName = cardHeader.DurationName,
                        ReasonId = cardHeader.ReasonId,
                        ReasonCode = cardHeader.ReasonCode,
                        ReasonName = cardHeader.ReasonName
                    };
                    cardEntries.Add(cardEntry);

                    item.SerialNumber = cardLine.SerialNumber;
                    item.CardNo = cardLine.CardNo;
                }
                hasValue = _cardHeaderService.Update(Mapper.Map<M_CardHeader>(cardHeader));
                hasValue = _customerService.Update(Mapper.Map<IEnumerable<M_Customer>>(_customers));
                hasValue = _cardLineService.Insert(Mapper.Map<IEnumerable<M_CardLine>>(cardLines));
                hasValue = _cardEntryService.Insert(Mapper.Map<IEnumerable<M_CardEntry>>(cardEntries));
                hasValue = _vuViecService.Commit();
                var _status = string.IsNullOrEmpty(hasValue) ? "Oke" : "Error";
                var _msg = string.IsNullOrEmpty(hasValue) ? "" : hasValue;
                return Json(new { s = _status, m = _msg }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { s = "Error", m = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// Gửi mail cho khách mua lẻ
        /// </summary>
        /// <param name="l">Lot number</param>
        /// <returns></returns>
        public JsonResult SendMail(string l)
        {
            try
            {
                var customers = Mapper.Map<IEnumerable<CustomerViewModel>>(_customerService.FindAll(l));
                foreach (var item in customers)
                {
                    if (item.IsEmail.Equals("Đúng"))
                    {
                        //-- Gửi qua email
                        var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
                        var mailTemplate = Mapper.Map<Template>(_templateService.Get("EMAILACTIVECODE"));
                        var mailTitle = mailTemplate.Title;
                        var mailContent = mailTemplate.Content.Replace("{{FULLNAME}}", item.FullName);
                        mailContent = mailContent.Replace("{{ACTIVECODE}}", CryptorEngine.Decrypt(item.CardNo, true, "TTLSTNHD@CARD"));
                        var emailTos = new List<string>();
                        emailTos.Add(item.Email);
                        //emailTos.Add("hieu.online1988@gmail.com");
                        var _mailHelper = new EmailHelper()
                        {
                            EmailTos = emailTos,
                            DisplayName = emailInfo.DisplayName,
                            Title = mailTitle,
                            Body = mailContent,
                            MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
                        };
                        string hasValue = _mailHelper.DoSendMail();
                    }
                    if (!string.IsNullOrEmpty(item.PhoneNo))
                    {
                        var newPhoneNo = item.PhoneNo.Trim();
                        var firstNumber = int.Parse(newPhoneNo[0].ToString());
                        if (firstNumber != 0)
                        {
                            newPhoneNo = "0" + newPhoneNo;
                        }
                        //-- Gui qua SMS
                        var smsTemplate = Mapper.Map<Template>(_templateService.Get("SMS_MAKICHHOAT"));
                        var smsHelper = new SMSHelper()
                        {
                            PhoneNo = newPhoneNo, //item.PhoneNo
                            Content = StringUtil.StripTagsRegexCompiled(smsTemplate.Content).Replace("{{NAME}}", "vao 5 Phut Thuoc Bai").Replace("{{MAKICHHOAT}}", CryptorEngine.Decrypt(item.CardNo, true, "TTLSTNHD@CARD")).Replace("{{LINK}}", "https://sieutrinhohocduong.com/student")
                        };
                        hasValue = smsHelper.Send();
                        //-- Gui qua SMS +
                    }
                }
                //-- Gửi qua email +
                return Json(new { s = "Oke", m = "" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { s = "Error", m = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SendMailAll()
        {
            try
            {
                var customers = Mapper.Map<IEnumerable<CustomerViewModel>>(_customerService.FindAllNotActive());
                foreach (var item in customers)
                {
                    if (item.IsEmail.Equals("Đúng"))
                    {
                        //-- Gửi qua email
                        var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
                        var mailTemplate = Mapper.Map<Template>(_templateService.Get("EMAILACTIVECODE"));
                        var mailTitle = mailTemplate.Title;
                        var mailContent = mailTemplate.Content.Replace("{{FULLNAME}}", item.FullName);
                        mailContent = mailContent.Replace("{{ACTIVECODE}}", CryptorEngine.Decrypt(item.CardNo, true, "TTLSTNHD@CARD"));
                        var emailTos = new List<string>();
                        emailTos.Add(item.Email);
                        //emailTos.Add("hieu.online1988@gmail.com");
                        var _mailHelper = new EmailHelper()
                        {
                            EmailTos = emailTos,
                            DisplayName = emailInfo.DisplayName,
                            Title = mailTitle,
                            Body = mailContent,
                            MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
                        };
                        string hasValue = _mailHelper.DoSendMail();
                    }
                    if (!string.IsNullOrEmpty(item.PhoneNo))
                    {
                        var newPhoneNo = item.PhoneNo.Trim();
                        var firstNumber = int.Parse(newPhoneNo[0].ToString());
                        if (firstNumber != 0)
                        {
                            newPhoneNo = "0" + newPhoneNo;
                        }
                        //-- Gui qua SMS
                        var smsTemplate = Mapper.Map<Template>(_templateService.Get("SMS_MAKICHHOAT"));
                        var smsHelper = new SMSHelper()
                        {
                            PhoneNo = newPhoneNo, //item.PhoneNo
                            Content = StringUtil.StripTagsRegexCompiled(smsTemplate.Content).Replace("{{NAME}}", "vao 5 Phut Thuoc Bai").Replace("{{MAKICHHOAT}}", CryptorEngine.Decrypt(item.CardNo, true, "TTLSTNHD@CARD")).Replace("{{LINK}}", "https://sieutrinhohocduong.com/student")
                        };
                        hasValue = smsHelper.Send();
                        //-- Gui qua SMS +
                    }
                }
                //--Gửi qua email +Oke
                return Json(new { s = "Error", m = customers.Count() }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { s = "Error", m = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}