﻿using AutoMapper;
using Dapper;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Service.ExeclOfficeEngine;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Service.Transaction.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Areas.Transaction.Data;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.Transaction.Controllers
{
    [CompressResponseAttribute]
    public class RetailSalesOrderController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly IRetailSalesOrderService _retailSalesService;
        private readonly IHistoryInvoice _historyInvoice;
        private string sysCategory = "RETAI.S.ORDER";
        private bool? permission = false;
        private string hasValue;
        private string connection = ConfigurationManager.ConnectionStrings["GCConnection"].ConnectionString;
        #endregion

        private readonly HttpClientHelper _http;

        public RetailSalesOrderController
            (
                IVuViecService vuViecService
                , IRetailSalesOrderService retailSalesService
                , IHistoryInvoice historyInvoice
                , HttpClientHelper httpClient

            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _retailSalesService = retailSalesService;
            _historyInvoice = historyInvoice;
            _http = httpClient;
        }
        // GET: Transaction/RetailSalesOrder
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }
        public JsonResult LoadData(GridSettings grid, string s)
        {
            var statusCodes = s.Split(',').ToList();
            var list = new List<RetailSalesOrder>().AsQueryable();
            if (s.Equals("PaySuccess"))
            {
                var res = Mapper.Map<IEnumerable<RetailSalesOrder>>(_retailSalesService.FindSUCCESS(s, "SUCCESS")).AsQueryable();
                var resHistory = _historyInvoice.GetAll();
                foreach (var itemHistory in resHistory)
                {
                    var item = res.FirstOrDefault(z => z.Code == itemHistory.Code);
                    if (item != null)
                    {
                        item.UserName_Change = itemHistory.UserName_Change;
                        item.ReasonName = itemHistory.ReasonName;
                        item.DateChange = itemHistory.DateChange;
                    }
                }
                list = res;
            }
            else
            {
                list = Mapper.Map<IEnumerable<RetailSalesOrder>>(_retailSalesService.FindAllNot("PaySuccess", "SUCCESS")).AsQueryable();
                foreach (var item in list)
                {
                    item.Action = $"<a class=\"md-btn md-btn-warning md-btn-wave-light\" href=\"javascript:void(0)\" name=\"activationCode\" id=\"{item.Code}\" accesskey=\"\"><i class=\"material-icons\">style</i>&nbsp;Gửi Mã Kích Hoạt</a>";
                }
            }
            list = JqGrid.SetupGrid<RetailSalesOrder>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
        public ActionResult Download(string s)
        {
            var agencyBuyInfoList = new List<RetailSalesOrder>();
            using (var conn = new SqlConnection(connection))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@StatusCode", s);
                agencyBuyInfoList = conn.Query<RetailSalesOrder>("SP_RPT_RetailSalesOrder", paramaters, null, true, 60, System.Data.CommandType.StoredProcedure).ToList();
            }

            List<KeyValuePair<string, object>> excelItems = new List<KeyValuePair<string, object>>();
            string statusCode = s.Equals("PaySuccess") ? "Giao dịch Thành công" : "Giao dịch chưa Thành công";
            excelItems.Add(new KeyValuePair<string, object>("StatusCode", statusCode));
            Excel.ExcelItems = excelItems;
            var dataModel = agencyBuyInfoList.ConvertToDataTable();
            dataModel.TableName = "Model";
            var stream = Excel.ExportReport(dataModel, HttpContext.Server.MapPath("/Areas/Transaction/Report/RetailSalesOrder.xlsx"), false, OfficeType.XLSX);
            return File(stream, OfficeContentType.XLSX, "RetailSalesOrder.xlsx");
        }

        public ActionResult PopupActivationCode(string code)
        {
            var model = new M_HistoryInvoice();
            var user = GetUser();
            model.Code = code;
            model.UserId_Change = user.ID;
            model.UserName_Change = user.Username;
            model.DateChange = DateTime.Now.Date;
            return PartialView("_PopupActivationCode", model);
        }
        [HttpPost]
        public ActionResult PopupActivationCode(M_HistoryInvoice model)
        {
            if (ModelState.IsValid)
            {
                var res = ActivationCode(model.Code);
                if (res)
                {
                    model.DateChange = DateTime.Now.Date;
                    _historyInvoice.Insert(model);
                    _vuViecService.Commit();
                    return Json("1");
                }
            }
            return Json("0");
        }

        public bool ActivationCode(string code)
        {
            try
            {
                var model = new InvoiceUpdateRequest();
                model.code = code;
                model.statusCode = "SUCCESS";
                string api = "https://apittl.stnhd.com/api/vi-VN/SieuTriNhoHocDuong/InvoiceUpdate";
                var res = _http.PostUrlAsync(api, model);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public class InvoiceUpdateRequest
        {
            public string code { get; set; }
            public string statusCode { get; set; }
        }
    }
}