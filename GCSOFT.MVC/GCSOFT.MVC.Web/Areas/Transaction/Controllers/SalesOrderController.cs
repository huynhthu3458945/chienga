﻿using AutoMapper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.StoreProcedure.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Service.Transaction.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Areas.Agency.Data;
using GCSOFT.MVC.Web.Areas.MasterData.Models;
using GCSOFT.MVC.Web.Areas.Transaction.Data;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.Transaction.Controllers
{
    [CompressResponseAttribute]
    public class SalesOrderController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly ISalesHeaderService _salesHdrService;
        private readonly ISalesLineService _salesLineService;
        private readonly IAgencyService _agencyService;
        private readonly IOptionLineService _optionLineService;
        private readonly ICardLineService _cardLineService;
        private readonly ICardEntryService _cardEntryService;
        private readonly ITemplateService _templateService;
        private readonly IMailSetupService _mailSetupService;
        private readonly IStoreProcedureService _storeProcService;
        private string sysCategory = "SALESORDER";
        private bool? permission = false;
        private SalesOrderCard salesOrderCard;
        private M_SalesHeader mSalesHeader;
        private SalesOrderCard purchaseOrderCard;
        private SalesType salesType = SalesType.SalesShipment;
        private string hasValue;
        #endregion
        public SalesOrderController
            (
                IVuViecService vuViecService
                , ISalesHeaderService salesHdrService
                , ISalesLineService salesLineService
                , IAgencyService agencyService
                , IOptionLineService optionLineService
                , ICardLineService cardLineService
                , ICardEntryService cardEntryService
                , ITemplateService templateService
                , IMailSetupService mailSetupService
                , IStoreProcedureService storeProcService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _salesHdrService = salesHdrService;
            _salesLineService = salesLineService;
            _agencyService = agencyService;
            _optionLineService = optionLineService;
            _cardLineService = cardLineService;
            _cardEntryService = cardEntryService;
            _templateService = templateService;
            _mailSetupService = mailSetupService;
            _storeProcService = storeProcService;
        }
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }
        public ActionResult Create()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            salesOrderCard = new SalesOrderCard();
            salesOrderCard.Code = StringUtil.CheckLetter("SO", _salesHdrService.GetMax(salesType), 4);
            salesOrderCard.DocumentDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.DocumentDate);
            salesOrderCard.CustomerList = Mapper.Map<IEnumerable<AgencyList>>(_agencyService.FindAll(AgencyType.Agency, null)).ToSelectListItems(salesOrderCard.CustomerId);
            var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("DH", "100"));
            salesOrderCard.StatusId = statusInfo.LineNo;
            salesOrderCard.StatusCode = statusInfo.Code;
            salesOrderCard.StatusName = statusInfo.Name;
            return View(salesOrderCard);
        }
        /// <summary>
        /// Tạo mới đơn hàng từ PO
        /// </summary>
        /// <param name="id">Code PO</param>
        /// <returns></returns>
        public ActionResult CreateFromPO(string id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            salesOrderCard = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(SalesType.PurchaseOrder, id));
            salesOrderCard.Code = StringUtil.CheckLetter("SM", _salesHdrService.GetMax(salesType), 4);
            salesOrderCard.DocumentDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.DocumentDate);
            salesOrderCard.CustomerList = Mapper.Map<IEnumerable<AgencyList>>(_agencyService.FindAll(AgencyType.Agency, null)).ToSelectListItems(0);
            var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("DH", "100"));
            salesOrderCard.StatusId = statusInfo.LineNo;
            salesOrderCard.StatusCode = statusInfo.Code;
            salesOrderCard.StatusName = statusInfo.Name;
            salesOrderCard.FromSoure = SalesType.PurchaseOrder;
            salesOrderCard.SourceNo = id;
            ViewBag.IsAllowSendShippment = "";
            return View(salesOrderCard);
        }
        public ActionResult Edit(string id, int? p)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            salesOrderCard = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(salesType, id));
            if (salesOrderCard == null)
                return HttpNotFound();
            salesOrderCard.DocumentDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.DocumentDate);
            salesOrderCard.PostingDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.PostingDate);
            salesOrderCard.CustomerList = Mapper.Map<IEnumerable<AgencyList>>(_agencyService.FindAll(AgencyType.Agency, null)).ToSelectListItems(salesOrderCard.CustomerId);
            //-- Paging
            int pageString = p ?? 0;
            int page = pageString == 0 ? 1 : pageString;
            var totalRecord = Mapper.Map<IEnumerable<SalesLine>>(_salesLineService.FindAll(salesType, id)).Count();
            salesOrderCard.paging = new Paging(totalRecord, page);
            salesOrderCard.SalesLines = Mapper.Map<IEnumerable<SalesLine>>(_salesLineService.FindAll(salesType, id, salesOrderCard.paging.start, salesOrderCard.paging.offset));
            //-- Paging +
            ViewBag.IsAllowSendShippment = (salesOrderCard.NoOfCard != 0 && salesOrderCard.StatusCode.Equals("100")) ? "" : "disabled";
            return View(salesOrderCard);
        }
        [HttpPost]
        public ActionResult Create(SalesOrderCard _salesOrderCard)
        {
            try
            {
                SetData(_salesOrderCard, true);
                _salesHdrService.Insert(mSalesHeader);
                _salesHdrService.Update(Mapper.Map<M_SalesHeader>(purchaseOrderCard));
                _salesLineService.Insert(mSalesHeader.SalesLines);
                _cardLineService.Update(mSalesHeader.CardLines);
                //_cardEntryService.Insert(mSalesHeader.CardEntries);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = mSalesHeader.Code, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult CreateFromPO(SalesOrderCard _salesOrderCard)
        {
            try
            {
                SetData(_salesOrderCard, true);
                _salesHdrService.Insert(mSalesHeader);
                _salesHdrService.Update(Mapper.Map<M_SalesHeader>(purchaseOrderCard));
                hasValue = _vuViecService.Commit();
                if (!string.IsNullOrEmpty(hasValue))
                {
                    ViewBag.Error = hasValue;
                    return View("Error");
                }
                var userInfo = GetUser();
                var sprocResult = _storeProcService.InsertSO(mSalesHeader.Code, mSalesHeader.LotNumber, userInfo.Username, userInfo.FullName, (int)mSalesHeader.FromSoure, mSalesHeader.SourceNo);
                if (string.IsNullOrEmpty(sprocResult.HasValue))
                {
                    //-- Send mail cho dai ly
                    var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
                    var mailTemplate = Mapper.Map<Template>(_templateService.Get("RECEIPT"));
                    var mailTitle = mailTemplate.Title.Replace("{{STATUS}}", "Chờ xác nhận");
                    mailTitle = mailTitle.Replace("{{DOCNO}}", mSalesHeader.Code);
                    var content = mailTemplate.Content.Replace("{{FULLNAME}}", mSalesHeader.CustomerName);
                    content = content.Replace("{{DOC_NO}}", mSalesHeader.Code);
                    content = content.Replace("{{STATUSNAME}}", mSalesHeader.StatusName);
                    content = content.Replace("{{LINKAPPROVAL}}", string.Format("https://agency.stnhd.com/Transaction/SalesShipment/Details/{0}", mSalesHeader.Code));
                    content = content.Replace("{{DOCUMENT_DATE}}", string.Format("{0:dd/MM/yyyy}", mSalesHeader.DocumentDate));
                    content = content.Replace("{{AGENCY_NAME}}", mSalesHeader.CustomerName);
                    content = content.Replace("{{AGENCY_ADD}}", mSalesHeader.CustomerAddress);
                    content = content.Replace("{{AGENCY_EMAIL}}", mSalesHeader.CustomerEmail);
                    content = content.Replace("{{AGENCY_PHONE}}", mSalesHeader.CustomerPhoneNo);
                    content = content.Replace("{{NOOFCARD}}", string.Format("{0:#,##0}", mSalesHeader.NoOfCard));
                    content = content.Replace("{{GIFT_CARD}}", string.Format("{0:#,##0}", mSalesHeader.GiftCard));
                    content = content.Replace("{{AMOUNT}}", string.Format("{0:#,##0}", mSalesHeader.Amount));
                    content = content.Replace("{{DISCOUNTPERCENT}}", string.Format("{0:#,##0}%", mSalesHeader.DiscountPercent));
                    content = content.Replace("{{DISCOUNT_AMT}}", string.Format("{0:#,##0}", mSalesHeader.DiscountAmt));
                    content = content.Replace("{{TOTALAMOUNT}}", string.Format("{0:#,##0}", mSalesHeader.TotalAmount));
                    content = content.Replace("{{REMARK}}", mSalesHeader.Remark);
                    var emailTos = new List<string>();
                    emailTos.Add(mSalesHeader.CustomerEmail);
                    var emailCCs = new List<string>();
                    emailCCs.Add(GetUser().Email);
                    var _mailHelper = new EmailHelper()
                    {
                        EmailTos = emailTos,
                        DisplayName = emailInfo.DisplayName,
                        Title = mailTitle,
                        Body = content,
                        MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
                    };
                    hasValue = _mailHelper.DoSendMail();
                    //-- Send mail cho dai ly +
                    //-- Send SMS
                    var smsTemplate = Mapper.Map<Template>(_templateService.Get("SMS_DH_WAIT"));
                    var smsHelper = new SMSHelper()
                    {
                        PhoneNo = mSalesHeader.CustomerPhoneNo,
                        Content = StringUtil.StripTagsRegexCompiled(smsTemplate.Content).Replace("{{DOCUMENT}}", mSalesHeader.Code).Replace("{{NAME}}", "Tam Tri Luc").Replace("{{LINK}}", "https://agency.stnhd.com")
                    };
                    hasValue = smsHelper.Send();
                    //-- Send SMS +
                    return RedirectToAction("Edit", new { id = mSalesHeader.Code, msg = "1" });
                }
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        private void SetData(SalesOrderCard _salesOrderCard, bool isCreate)
        {
            salesOrderCard = _salesOrderCard;
            var userInfo = GetUser();
            var statusInfo = new VM_OptionLine();
            if (isCreate)
            {
                salesOrderCard.Code = StringUtil.CheckLetter("SM", _salesHdrService.GetMax(salesType), 4);
                //-- Cap nhat lai trang thai don hang
                purchaseOrderCard = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(salesOrderCard.FromSoure, salesOrderCard.SourceNo));
                statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("ORDERSTATUS", "900")); //-- Đã Xuất Kho
                purchaseOrderCard.StatusId = statusInfo.LineNo;
                purchaseOrderCard.StatusCode = statusInfo.Code;
                purchaseOrderCard.StatusName = statusInfo.Name;

                salesOrderCard.CreateBy = userInfo.Username;
                salesOrderCard.DateCreate = DateTime.Now;
                salesOrderCard.LastModifyBy = userInfo.Username;
                salesOrderCard.LastModifyDate = salesOrderCard.DateCreate;
                statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("DH", "250"));//-- Chờ Xác Nhận
                salesOrderCard.StatusId = statusInfo.LineNo;
                salesOrderCard.StatusCode = statusInfo.Code;
                salesOrderCard.StatusName = statusInfo.Name;
                salesOrderCard.DurationId = purchaseOrderCard.DurationId;
                salesOrderCard.DurationCode = purchaseOrderCard.DurationCode;
                salesOrderCard.DurationName = purchaseOrderCard.DurationName;
                salesOrderCard.ReasonId = purchaseOrderCard.ReasonId;
                salesOrderCard.ReasonCode = purchaseOrderCard.ReasonCode;
                salesOrderCard.ReasonName = purchaseOrderCard.ReasonName;
                salesOrderCard.LevelId = purchaseOrderCard.LevelId;
                salesOrderCard.LevelCode = purchaseOrderCard.LevelCode;
                salesOrderCard.LevelName = purchaseOrderCard.LevelName;

            } else
            {
                salesOrderCard.LastModifyBy = userInfo.Username;
                salesOrderCard.LastModifyDate = salesOrderCard.DateCreate;
            }
            if (!string.IsNullOrEmpty(salesOrderCard.ReceiptDateStr))
                salesOrderCard.ReceiptDate = DateTime.Parse(StringUtil.ConvertStringToDate(salesOrderCard.ReceiptDateStr));
            else
                salesOrderCard.ReceiptDate = null;

            salesOrderCard.PostingDate = DateTime.Now;

            if (!string.IsNullOrEmpty(salesOrderCard.DocumentDateStr))
                salesOrderCard.DocumentDate = DateTime.Parse(StringUtil.ConvertStringToDate(salesOrderCard.DocumentDateStr));
            else
                salesOrderCard.DocumentDate = null;
            var customerInfo = Mapper.Map<AgencyCard>(_agencyService.GetBy(salesOrderCard.CustomerId));
            salesOrderCard.CustomerName = customerInfo.FullName;
            salesOrderCard.CustomerAddress = customerInfo.Address;
            salesOrderCard.CustomerEmail = customerInfo.Email;
            salesOrderCard.CustomerPhoneNo = customerInfo.Phone;
            salesOrderCard.SalesType = salesType;
            mSalesHeader = Mapper.Map<M_SalesHeader>(salesOrderCard);
        }
        public JsonResult LoadData(GridSettings grid)
        {
            var list = Mapper.Map<IEnumerable<SalesOrderList>>(_salesHdrService.FindAll(salesType)).AsQueryable();
            list = JqGrid.SetupGrid<SalesOrderList>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
    }
}