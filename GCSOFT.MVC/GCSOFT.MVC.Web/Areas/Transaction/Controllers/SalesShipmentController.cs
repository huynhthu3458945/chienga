﻿using AutoMapper;
using GCSOFT.MVC.Data.Common;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Service.AgencyService.Interface;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Service.Transaction.Interface;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Web.Areas.Agency.Data;
using GCSOFT.MVC.Web.Areas.MasterData.Models;
using GCSOFT.MVC.Web.Areas.Transaction.Data;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.ViewModels;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.Transaction.Controllers
{
    [CompressResponseAttribute]
    public class SalesShipmentController : BaseController
    {
        #region -- Properties --
        private readonly IVuViecService _vuViecService;
        private readonly ISalesHeaderService _salesHdrService;
        private readonly ISalesLineService _salesLineService;
        private readonly IAgencyService _agencyService;
        private readonly IOptionLineService _optionLineService;
        private readonly ICardLineService _cardLineService;
        private readonly ICardEntryService _cardEntryService;
        private readonly ITemplateService _templateService;
        private readonly IMailSetupService _mailSetupService;
        private string sysCategory = "SALESSHIPMENT";
        private bool? permission = false;
        private SalesOrderCard salesOrderCard;
        private SalesOrderCard salesOrderSource;
        private M_SalesHeader mSalesHeader;
        private SalesType salesType = SalesType.SalesShipment;
        private string hasValue;
        
        #endregion
        public SalesShipmentController
            (
                IVuViecService vuViecService
                , ISalesHeaderService salesHdrService
                , ISalesLineService salesLineService
                , IAgencyService agencyService
                , IOptionLineService optionLineService
                , ICardLineService cardLineService
                , ICardEntryService cardEntryService
                , ITemplateService templateService
                , IMailSetupService mailSetupService
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _salesHdrService = salesHdrService;
            _salesLineService = salesLineService;
            _agencyService = agencyService;
            _optionLineService = optionLineService;
            _cardLineService = cardLineService;
            _cardEntryService = cardEntryService;
            _templateService = templateService;
            _mailSetupService = mailSetupService;
        }
        public ActionResult Index()
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }
        /// <summary>
        /// Tạo phiếu giao hàng
        /// </summary>
        /// <param name="id">Code of Sales Order</param>
        /// <returns></returns>
        public ActionResult Create(string id)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            salesOrderCard = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(SalesType.SalesOrder, id));
            salesOrderCard.Code = StringUtil.CheckLetter("SM", _salesHdrService.GetMax(salesType), 4);
            salesOrderCard.DocumentDate = DateTime.Now;
            salesOrderCard.DocumentDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.DocumentDate);
            salesOrderCard.FromSoure = SalesType.SalesShipment;
            salesOrderCard.SourceNo = id;

            var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("DH", "250"));
            salesOrderCard.StatusId = statusInfo.LineNo;
            salesOrderCard.StatusCode = statusInfo.Code;
            salesOrderCard.StatusName = statusInfo.Name;
            return PartialView("_Create", salesOrderCard);
        }
        public ActionResult Edit(string id, int? p)
        {
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            salesOrderCard = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(salesType, id));
            if (salesOrderCard == null)
                return HttpNotFound();
            salesOrderCard.DocumentDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.DocumentDate);
            salesOrderCard.ReceiptDateStr = string.Format("{0:dd/MM/yyyy}", salesOrderCard.ReceiptDate);
            //-- Paging
            int pageString = p ?? 0;
            int page = pageString == 0 ? 1 : pageString;
            var totalRecord = Mapper.Map<IEnumerable<SalesLine>>(_salesLineService.FindAll(salesType, id)).Count();
            salesOrderCard.paging = new Paging(totalRecord, page);
            salesOrderCard.SalesLines = Mapper.Map<IEnumerable<SalesLine>>(_salesLineService.FindAll(salesType, id, salesOrderCard.paging.start, salesOrderCard.paging.offset));
            //-- Paging +
            return View(salesOrderCard);
        }
        [HttpPost]
        public ActionResult Create(SalesOrderCard _salesOrderCard)
        {
            try
            {
                SetData(_salesOrderCard, true);
                hasValue = _salesHdrService.Insert(mSalesHeader);
                hasValue = _salesHdrService.Update(Mapper.Map<M_SalesHeader>(salesOrderSource));
                hasValue = _salesLineService.Insert(mSalesHeader.SalesLines);
                hasValue = _cardLineService.Update(mSalesHeader.CardLines);
                hasValue = _vuViecService.Commit();
                //hasValue = _cardEntryService.Insert(mSalesHeader.CardEntries);
                //hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                {
                    //-- Send mail cho dai ly
                    var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
                    var mailTemplate = Mapper.Map<Template>(_templateService.Get("RECEIPT"));
                    var mailTitle = mailTemplate.Title.Replace("{{STATUS}}", mSalesHeader.StatusName);
                    mailTitle = mailTitle.Replace("{{DOCNO}}", mSalesHeader.Code);
                    var content = mailTemplate.Content.Replace("{{FULLNAME}}", mSalesHeader.CustomerName);
                    content = content.Replace("{{DOC_NO}}", mSalesHeader.Code);
                    content = content.Replace("{{STATUSNAME}}", mSalesHeader.StatusName);
                    content = content.Replace("{{LINKAPPROVAL}}", string.Format("https://agency.stnhd.com/Transaction/SalesShipment/Details/{0}", mSalesHeader.Code));
                    content = content.Replace("{{DOCUMENT_DATE}}", string.Format("{0:dd/MM/yyyy}", mSalesHeader.DocumentDate));
                    content = content.Replace("{{AGENCY_NAME}}", mSalesHeader.CustomerName);
                    content = content.Replace("{{AGENCY_ADD}}", mSalesHeader.CustomerAddress);
                    content = content.Replace("{{AGENCY_EMAIL}}", mSalesHeader.CustomerEmail);
                    content = content.Replace("{{AGENCY_PHONE}}", mSalesHeader.CustomerPhoneNo);
                    content = content.Replace("{{NOOFCARD}}", string.Format("{0:#,##0}", mSalesHeader.NoOfCard));
                    content = content.Replace("{{GIFT_CARD}}", string.Format("{0:#,##0}", mSalesHeader.GiftCard));
                    content = content.Replace("{{AMOUNT}}", string.Format("{0:#,##0}", mSalesHeader.Amount));
                    content = content.Replace("{{DISCOUNTPERCENT}}", string.Format("{0:#,##0}%", mSalesHeader.DiscountPercent));
                    content = content.Replace("{{DISCOUNT_AMT}}", string.Format("{0:#,##0}", mSalesHeader.DiscountAmt));
                    content = content.Replace("{{TOTALAMOUNT}}", string.Format("{0:#,##0}", mSalesHeader.TotalAmount));
                    content = content.Replace("{{REMARK}}", mSalesHeader.Remark);
                    var emailTos = new List<string>();
                    emailTos.Add(mSalesHeader.CustomerEmail);
                    var _mailHelper = new EmailHelper()
                    {
                        EmailTos = emailTos,
                        DisplayName = emailInfo.DisplayName,
                        Title = mailTitle,
                        Body = content,
                        MailReply = string.IsNullOrEmpty(emailInfo.MailReply) ? emailInfo.Email : emailInfo.MailReply
                    };
                    hasValue = _mailHelper.DoSendMail();
                    //-- Send mail cho dai ly +
                    return Json("", JsonRequestBehavior.AllowGet);
                }
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }
        private void SetData(SalesOrderCard _salesOrderCard, bool isCreate)
        {
            salesOrderSource = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(SalesType.SalesOrder, _salesOrderCard.SourceNo));
            var statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("DH", "200")); //Đã Giao Hàng
            salesOrderSource.StatusId = statusInfo.LineNo;
            salesOrderSource.StatusCode = statusInfo.Code;
            salesOrderSource.StatusName = statusInfo.Name;
            salesOrderSource.PostingDate = DateTime.Now;

            salesOrderCard = Mapper.Map<SalesOrderCard>(_salesHdrService.GetBy(SalesType.SalesOrder, _salesOrderCard.SourceNo));
            salesOrderCard.Code = StringUtil.CheckLetter("SM", _salesHdrService.GetMax(SalesType.SalesShipment), 4);
            salesOrderCard.DocumentDate = salesOrderSource.PostingDate;
            statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("DH", "250")); //Chờ Xác Nhận
            salesOrderCard.StatusId = statusInfo.LineNo;
            salesOrderCard.StatusCode = statusInfo.Code;
            salesOrderCard.StatusName = statusInfo.Name;
            salesOrderCard.SalesType = salesType;

            salesOrderCard.SourceNo = _salesOrderCard.SourceNo;
            salesOrderCard.FromSoure = SalesType.SalesOrder;
            salesOrderCard.Remark = _salesOrderCard.Remark;

            var cardLine = new CardLine();
            var cardLines = new List<CardLine>();

            var cardEntry = new CardEntry();
            var cardEntries = new List<CardEntry>();

            var idCardEntry = _cardEntryService.GetID() - 1;
            statusInfo = Mapper.Map<VM_OptionLine>(_optionLineService.Get("CARD", "200")); //Thẻ Chờ Xác Nhận.
            var userInfo = GetUser();

            var _salesLines = Mapper.Map<IEnumerable<SalesLine>>(_salesLineService.FindAll(salesOrderCard.FromSoure, salesOrderCard.SourceNo));
            _salesLines.ToList().ForEach(item => {
                item.SalesType = SalesType.SalesShipment;
                item.DocumentCode = salesOrderCard.Code;
                cardLine = Mapper.Map<CardLine>(_cardLineService.GetBySerialNumber(item.SerialNumber));
                cardLine.UserType = UserType.TTL;
                cardLine.UserNameActive = userInfo.Username;
                cardLine.UserActiveFullName = userInfo.FullName;
                cardLine.LastDateUpdate = salesOrderCard.DocumentDate ?? DateTime.Now;
                cardLine.StatusId = statusInfo.LineNo;
                cardLine.StatusCode = statusInfo.Code;
                cardLine.StatusName = statusInfo.Name;
                cardLine.LastStatusId = statusInfo.LineNo;
                cardLine.LastStatusCode = statusInfo.Code;
                cardLine.LastStatusName = statusInfo.Name;
                cardLines.Add(cardLine);

                //idCardEntry += 1;
                //cardEntry = new CardEntry()
                //{
                //    Id = idCardEntry,
                //    LotNumber = cardLine.LotNumber,
                //    CardLineId = cardLine.Id,
                //    SerialNumber = cardLine.SerialNumber,
                //    CardNo = cardLine.CardNo,
                //    Price = cardLine.Price,
                //    UserName = cardLine.UserName,
                //    UserFullName = cardLine.UserFullName,
                //    DateCreate = cardLine.DateCreate,
                //    UserType = UserType.TTL,
                //    UserNameActive = cardLine.UserNameActive,
                //    UserActiveFullName = cardLine.UserActiveFullName,
                //    Date = cardLine.LastDateUpdate,
                //    StatusId = cardLine.StatusId,
                //    StatusCode = cardLine.StatusCode,
                //    StatusName = cardLine.StatusName,
                //    Source = CardSource.SalesShipment,
                //    SourceNo = salesOrderCard.Code
                //};
                //cardEntries.Add(cardEntry);
            });
            salesOrderCard.SalesLines = _salesLines;
            salesOrderCard.CardLines = cardLines;
            //salesOrderCard.CardEntries = cardEntries;

            mSalesHeader = Mapper.Map<M_SalesHeader>(salesOrderCard);
        }
        public JsonResult LoadData(GridSettings grid)
        {
            var list = Mapper.Map<IEnumerable<SalesOrderList>>(_salesHdrService.FindAll(salesType)).AsQueryable();
            list = JqGrid.SetupGrid<SalesOrderList>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
    }
}