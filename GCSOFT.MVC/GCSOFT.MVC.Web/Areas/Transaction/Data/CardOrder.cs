﻿using GCSOFT.MVC.Model.Transaction;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.Web.Areas.Transaction.Data
{
    public class CardList
    {
        public PurchaseCardType Type { get; set; }
        [DisplayName("Mã Phiếu")]
        public string Code { get; set; }
        [DisplayName("Ngày Đặt In")]
        public DateTime OrderDate { get; set; }
        [DisplayName("Ngày Đặt In")]
        public string OrderDateStr { get { return string.Format("{0:dd/MM/yyyy}", OrderDate); } }
        [DisplayName("Ngày In")]
        public DateTime DatePrint { get; set; }
        [DisplayName("Ngày In")]
        public string DatePrintStr { get { return DatePrint == null ? "" : string.Format("{0:dd/MM/yyyy}", DatePrint); } }
        [DisplayName("Số Lượng Thẻ")]
        public int NoOfCard { get; set; }
    }
    public class CardOrder
    {
        public PurchaseCardType Type { get; set; }
        [DisplayName("Mã Phiếu")]
        public string Code { get; set; }
        [DisplayName("Ngày Đặt In")]
        public DateTime OrderDate { get; set; }
        [DisplayName("Ngày Đặt In")]
        public string OrderDateStr { get; set; }
        [DisplayName("Ngày Đi In")]
        public DateTime? DatePrint { get; set; }
        [DisplayName("Ngày Đi In")]
        public string DatePrintStr { get; set; }
        [DisplayName("Số Lượng Thẻ")]
        public int NoOfCard { get; set; }
        [DisplayName("Ghi Chú")]
        public string Remark { get; set; }
        public CardOrder()
        {
            OrderDate = DateTime.Now;
        }
    }
}