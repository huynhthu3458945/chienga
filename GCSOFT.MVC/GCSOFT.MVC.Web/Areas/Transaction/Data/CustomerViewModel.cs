﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.Web.Areas.Transaction.Data
{
    public class CustomerViewModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        [DisplayName("Họ và Tên")]
        public string FullName { get; set; }
        [DisplayName("Điện Thoại")]
        public string PhoneNo { get; set; }
        public string IsEmail { get; set; }
        [DisplayName("Email")]
        public string Email { get; set; }
        public DateTime? RegisterDate { get; set; }
        public string RegistrationDate { get; set; }
        [DisplayName("Ngày Đăng Ký")]
        public string RegisterDateStr { get { return string.Format("{0:dd/MM/yyyy}", RegisterDate); } }
        public string Owner { get; set; }
        public int? NoOfCard { get; set; }
        public string LotNumber { get; set; }
        public string SerialNumber { get; set; }
        public string CardNo { get; set; }
        public string UserOwner { get; set; }
        public DateTime? DateCard { get; set; }
        public int StatusId { get; set; }
        public string StatusCode { get; set; }
        [DisplayName("Trạng Thái")]
        public string StatusName { get; set; }
    }
}