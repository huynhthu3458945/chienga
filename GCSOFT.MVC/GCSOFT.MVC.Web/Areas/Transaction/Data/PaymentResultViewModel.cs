﻿using GCSOFT.MVC.Model.Transaction;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.Web.Areas.Transaction.Data
{
    public class PaymentResultViewModel
    {
        public PaymentType PaymentType { get; set; }
        [DisplayName("Mã")]
        public string TransactionID { get; set; }
        public string TransactionDateTime { get; set; }
        [DisplayName("Tổng Tiền")]
        public decimal TotalAmount { get; set; }
        public string TotalAmountStr { get { return string.Format("{0:#,##0}", TotalAmount); } }
        [DisplayName("Số Thẻ")]
        public string CardNumber { get; set; }
        public string ExpiryDate { get; set; }
        public string SubscriptionType { get; set; }
        public string CardType { get; set; }
        [DisplayName("Loại Thẻ")]
        public string CardName { get; set; }
        public string AccountNo { get; set; }
        public string SubscriptionSource { get; set; }
        public string Token { get; set; }
        public string ResponseCode { get; set; }
        [DisplayName("Tình Trạng")]
        public string ResponseName { get; set; }
        [DisplayName("Nội Dung")]
        public string Description { get; set; }
        public DateTime DatePayment { get; set; }
        [DisplayName("Ngày Tạo")]
        public string DatePaymentStr { get { return string.Format("{0:dd/MM/yyyy}", DatePayment); } }
        public int UserId { get; set; }
        [DisplayName("User Active")]
        public string Username { get; set; }
    }
}