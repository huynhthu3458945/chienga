﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.Transaction.Data
{
    public class PotentialList
    {
        [DisplayName("Mã Đơn")]
        public string Code { get; set; }
        [DisplayName("Đối Tác")]
        public string AgencyNames { get; set; }
        [DisplayName("Tỉnh/Thành Phố")]
        public string CityName { get; set; }
        [DisplayName("Quận Huyện")]
        public string DistrictName { get; set; }
    }
    public class PotentialCard
    {
        [DisplayName("Mã Đơn")]
        public string Code { get; set; }
        public DateTime DateCreate { get; set; }
        public DateTime DateModify { get; set; }
        [DisplayName("Đối Tác")]
        public int AgencyId { get; set; }
        public string AgencyNames { get; set; }
        [DisplayName("Tỉnh Thành Phố")]
        public int CityId { get; set; }
        public string CityName { get; set; }
        [DisplayName("Quận Huyện")]
        public int DistrictId { get; set; }
        public string DistrictName { get; set; }
        public int StatusId { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }
        public string UserCreate { get; set; }
        public string Remark { get; set; }
        public IEnumerable<SelectListItem> AgencyList { get; set; }
        public IEnumerable<SelectListItem> DistrictList { get; set; }
        public IEnumerable<SelectListItem> CityList { get; set; }
    }
}