﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.Web.Areas.Transaction.Data
{
    public class RetailSalesOrder
    {
        [DisplayName("Mã Đơn Hàng")]
        public string Code { get; set; }
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Description { get; set; }
        [DisplayName("Khách Hàng")]
        public string CustomerName { get; set; }
        [DisplayName("Email")]
        public string CustomerEmail { get; set; }
        [DisplayName("Điện Thoại")]
        public string CustomerPhone { get; set; }
        [DisplayName("Địa Chỉ")]
        public string Address { get; set; }
        [DisplayName("Mã Hàng")]
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public double TotalAmount { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }
        [DisplayName("Ngày Mua")]
        public DateTime CreateTime { get; set; }
        [DisplayName("Ngày Mua")]
        public string CreateTimeStr { get { return string.Format("{0:dd/MM/yyyy}", CreateTime); } }
        public string TransactionInfo { get; set; }
        public string ActionCode { get; set; }
        public int LevelId { get; set; }
        public string LevelCode { get; set; }
        [DisplayName("Gói")]
        public string LevelName { get; set; }
        public int DurationId { get; set; }
        public string DurationCode { get; set; }
        [DisplayName("Thời Hạn")]
        public string DurationName { get; set; }
        public string UserName_Change { get; set; }
        public string ReasonName { get; set; }
        public DateTime? DateChange { get; set; }
        public string DateChangeStr { get { return string.Format("{0:dd/MM/yyyy}", DateChange); } }
        public string Action { get; set; }
        public string StatusActive { get; set; }
    }
}