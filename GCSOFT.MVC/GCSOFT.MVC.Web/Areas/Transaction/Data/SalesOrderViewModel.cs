﻿using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Web.Areas.Approval.Data;
using GCSOFT.MVC.Web.Areas.MasterData.Models;
using GCSOFT.MVC.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Areas.Transaction.Data
{
    public class SalesOrderList
    {
        [DisplayName("Mã Đơn Hàng")]
        public string Code { get; set; }
        [DisplayName("Đại Lý")]
        public string CustomerName { get; set; }
        [DisplayName("Điện Thoại")]
        public string CustomerPhoneNo { get; set; }
        [DisplayName("Ngày")]
        public DateTime DocumentDate { get; set; }
        [DisplayName("Ngày")]
        public string DocumentDateStr { get { return string.Format("{0:dd/MM/yyyy}", DocumentDate); } }
        public DateTime? PostingDate { get; set; }
        [DisplayName("Ngày Giao Hàng")]
        public string PostingDateStr { get { return string.Format("{0:dd/MM/yyyy}", PostingDate); } }
        public DateTime? ReceiptDate { get; set; }
        [DisplayName("Ngày Nhận Hàng")]
        public string ReceiptDateStr { get { return string.Format("{0:dd/MM/yyyy}", ReceiptDate); } }
        [DisplayName("Số Lượng Thẻ")]
        public int NoOfCard { get; set; }
        [DisplayName("Số Lượng Thẻ")]
        public string NoOfCardStr { get { return string.Format("{0:#,##0}", NoOfCard); } }
        public int GiftCard { get; set; }
        [DisplayName("Số Thẻ Tặng")]
        public string GiftCardStr { get { return string.Format("{0:#,##0}", GiftCard); } }
        public int TotalCard { get; set; }
        [DisplayName("Tổng Số Lượng")]
        public string TotalCardStr { get { return string.Format("{0:#,##0}", TotalCard); } }
        [DisplayName("Trạng Thái")]
        public string StatusName { get; set; }
        [DisplayName("Giá Trị")]
        public Decimal TotalAmount { get; set; }
        [DisplayName("Tổng Giá Trị")]
        public string TotalAmountStr { get { return string.Format("{0:#,##0}", TotalAmount); } }
    }
    public class SalesOrderCard
    {
        public SalesType SalesType { get; set; }
        [DisplayName("Mã Đơn Hàng")]
        public string Code { get; set; }

        public DateTime? DocumentDate { get; set; }
        [DisplayName("Ngày (*)")]
        public string DocumentDateStr { get; set; }

        public DateTime? PostingDate { get; set; }
        [DisplayName("Ngày Giao Hàng")]
        public string PostingDateStr { get; set; }

        public DateTime? ReceiptDate { get; set; }
        [DisplayName("Ngày Nhận Hàng")]
        public string ReceiptDateStr { get; set; }

        [DisplayName("Đại Lý (*)")]
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        [DisplayName("Địa Chỉ")]
        public string CustomerAddress { get; set; }
        [DisplayName("Email")]
        public string CustomerEmail { get; set; }
        [DisplayName("Điện Thoại")]
        public string CustomerPhoneNo { get; set; }
        public IEnumerable<SelectListItem> CustomerList { get; set; }
        [DisplayName("Số Lượng Thẻ (*)")]
        public int NoOfCard { get; set; }
        [DisplayName("Số Thẻ Tặng")]
        public int GiftCard { get; set; }
        [DisplayName("Tổng Thẻ Nhận")]
        public int TotalCard { get; set; }
        [DisplayName("Từ Số")]
        public string FromSeriesNumber { get; set; }
        [DisplayName("Đến Số")]
        public string ToSeriesNumber { get; set; }
        [DisplayName("Số Lô Thẻ")]
        public string LotNumber { get; set; }
        [DisplayName("Tổng Tiền")]
        public decimal Amount { get; set; }
        [DisplayName("Chiết Khấu (%)")]
        public decimal DiscountPercent { get; set; }
        [DisplayName("Tiền Chiết Khấu")]
        public decimal DiscountAmt { get; set; }
        [DisplayName("Tổng Tiền")]
        public decimal TotalAmount { get; set; }
        [DisplayName("Ghi Chú")]
        public string Remark { get; set; }

        [DisplayName("Trạng Thái")]
        public int StatusId { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }
        [DisplayName("Thời Hạn")]
        public int DurationId { get; set; }
        public string DurationCode { get; set; }
        public string DurationName { get; set; }
        [DisplayName("Lý Do Bán")]
        public int ReasonId { get; set; }
        public string ReasonCode { get; set; }
        public string ReasonName { get; set; }
        public SalesType FromSoure { get; set; }
        public string SourceNo { get; set; }
        public string CreateBy { get; set; }
        public DateTime DateCreate { get; set; }
        public string LastModifyBy { get; set; }
        public DateTime LastModifyDate { get; set; }
        public string ReceiveBy { get; set; }
        public string ReceiveName { get; set; }
        public DateTime? ReceiveDate { get; set; }
        public Paging paging { get; set; }
        [DisplayName("Đơn Giá (*)")]
        public decimal? Price { get; set; }
        [DisplayName("Gói (*)")]
        public int LevelId { get; set; }
        public string LevelCode { get; set; }
        public string LevelName { get; set; }
        public int NoOfCardAgency { get; set; }
        [DisplayName("Loại Thẻ (*)")]
        public string FromLevelCode { get; set; }
        public string ToLevelCode { get; set; }
        [DisplayName("Số Lượng Giao Thực Tế")]
        public int? SuggestedNoOfCard { get; set; }
        [DisplayName("Ghi Chú Đơn Hàng")]
        public string TTLRemark { get; set; }
        [DisplayName("Khuyến mãi")]
        public string PromotionCodeId { get; set; }
        public string PromotionCode { get; set; }
        public string TypeSale { get; set; }
        public ConvertType ConvertType { get; set; }
        public IEnumerable<SelectListItem> PromotionList { get; set; }
        public IEnumerable<SelectListItem> FromLevelList { get; set; }
        public IEnumerable<SelectListItem> ToLevelList { get; set; }

        public IEnumerable<SelectListItem> DurationList { get; set; }
        public IEnumerable<SelectListItem> ReasonList { get; set; }
        public IEnumerable<SelectListItem> LevelList { get; set; }
        public virtual ApprovalInfoViewModel ApprovalInfo { get; set; }
        public virtual IEnumerable<FlowViewModel> Flows { get; set; }
        public virtual IEnumerable<SalesLine> SalesLines { get; set; }
        public virtual IEnumerable<CardLine> CardLines { get; set; }
        public virtual IEnumerable<CardEntry> CardEntries { get; set; }
        public SalesOrderCard() 
        {
            DocumentDate = DateTime.Now;
            paging = new Paging();
            this.SalesLines = new List<SalesLine>();
        }
    }
    public class SalesLine
    {
        public SalesType SalesType { get; set; }
        public string DocumentCode { get; set; }
        public long LineNo { get; set; }
        public string LotNumber { get; set; }
        public long CardId { get; set; }
        public string SerialNumber { get; set; }
        public decimal Price { get; set; }
        public string PriceStr { get { return string.Format("{0:#,##0}", Price); } }
    }
}