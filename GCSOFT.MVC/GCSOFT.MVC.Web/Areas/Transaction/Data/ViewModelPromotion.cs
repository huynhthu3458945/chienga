﻿// #################################################################
// # Copyright (C) 2021-2022, Tâm Trí Lực JSC.  All Rights Reserved.                       
// #
// # History：                                                                        
// #	Date Time	    Updated		    Content                	
// #    25/08/2021	    Huỳnh Thử 		Update
// ##################################################################

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace GCSOFT.MVC.Web.Areas.Transaction.Data
{
    public class ViewModelPromotionList
    {
        [DisplayName("Mã Khuyến Mãi")]
        public string Code { get; set; }
        [DisplayName("Tên Khuyến Mãi")]
        public string Title { get; set; }
        [DisplayName("Diễn Giải")]
        public string Description { get; set; }
       
        public DateTime? FromDate { get; set; }
        [DisplayName("Từ Ngày")]
        public string FromDateStr { get { return string.Format("{0:dd/MM/yyyy}", FromDate); } }
        public DateTime? ToDate { get; set; }
        [DisplayName("Đến Ngày")]
        public string ToDateStr { get { return string.Format("{0:dd/MM/yyyy}", ToDate); } }
        [DisplayName("Tiền Tố")]
        public string Prefix { get; set; }
        [DisplayName("Ghi Chú")]
        public string Remark { get; set; }
    }
    public class ViewModelPromotion
    {
        public int Id { get; set; }
        [DisplayName("Mã Khuyến Mãi")]
        public string Code { get; set; }
        [DisplayName("Tên Khuyến Mãi")]
        public string Title { get; set; }
        [DisplayName("Diễn Giải")]
        public string Description { get; set; }
        public DateTime? FromDate { get; set; }
        [DisplayName("Từ Ngày")]
        public string FromDateStr { get; set; }
        public DateTime? ToDate { get; set; }
        [DisplayName("Đến Ngày")]
        public string ToDateStr { get; set; }
        [DisplayName("Tiền Tố")]
        public string Prefix { get; set; }
        [DisplayName("Ghi Chú")]
        public string Remark { get; set; }
    }
}