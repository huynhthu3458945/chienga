﻿<#@ template language="C#" HostSpecific="True" Debug="True" #>
<#@ output extension="cs" #>
<#@ assembly name="System.Core" #>
<#@ assembly name="System.Data.Linq" #>
<#@ ScaffoldingAssembly Processor="ScaffoldingAssemblyLoader" #>
<#
string routePrefix;
if (String.IsNullOrEmpty(AreaName)) 
{
    routePrefix = ControllerRootName;
}
else
{
    routePrefix = AreaName + "/" + ControllerRootName;
}
#>
<#
	string _controllerName = ControllerName.Replace("Controller", "");
	string _varControllerName = _controllerName.ToLower();
#>
<#@ import namespace="System.Collections" #>
<#@ import namespace="System.Collections.Generic" #>
<#@ import namespace="System.Linq" #>
<#@ import namespace="Microsoft.AspNet.Scaffolding.Core.Metadata" #>
<#@ parameter type="System.String" name="ControllerName" #>
<#@ parameter type="System.String" name="ControllerRootName" #>
<#@ parameter type="System.String" name="Namespace" #>
<#@ parameter type="System.String" name="AreaName" #>
<#@ parameter type="System.String" name="ContextTypeName" #>
<#@ parameter type="System.String" name="ModelTypeName" #>
<#@ parameter type="System.String" name="ModelVariable" #>
<#@ parameter type="Microsoft.AspNet.Scaffolding.Core.Metadata.ModelMetadata" name="ModelMetadata" #>
<#@ parameter type="System.String" name="EntitySetVariable" #>
<#@ parameter type="System.Boolean" name="UseAsync" #>
<#@ parameter type="System.Boolean" name="IsOverpostingProtectionRequired" #>
<#@ parameter type="System.String" name="BindAttributeIncludeText" #>
<#@ parameter type="System.String" name ="OverpostingWarningMessage" #>
<#@ parameter type="System.Collections.Generic.HashSet<System.String>" name="RequiredNamespaces" #>
using System.Web.Mvc;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System;
using GCSOFT.MVC.Service.SystemService.Interface;
using GCSOFT.MVC.Web.Helper;
using GCSOFT.MVC.Web.ViewModels.jqGrid;
using System.Net;
using GCSOFT.MVC.Web.Helper.ActionFilters;
using GCSOFT.MVC.Web.Areas.Administrator.Controllers;
using GCSOFT.MVC.Service.MasterDataService.Interface;
using GCSOFT.MVC.Web.ViewModels.MasterData;
<# if (UseAsync) { #>
using System.Threading.Tasks;
<# } #>
<# foreach (var namespaceName in RequiredNamespaces) { #>
using <#= namespaceName #>;
<# } #>

namespace <#= Namespace #>
{
<#
    var contextTypeName = ContextTypeName;
    var entitySetName = ModelMetadata.EntitySetName;
    var entitySetVar = EntitySetVariable ?? (String.IsNullOrEmpty(entitySetName) ? entitySetName : (entitySetName.Substring(0, length:1).ToLowerInvariant() + entitySetName.Substring(1)));
    var primaryKeyName = ModelMetadata.PrimaryKeys[0].PropertyName;
    var primaryKeyShortTypeName = ModelMetadata.PrimaryKeys[0].ShortTypeName;
    var primaryKeyDefaultValue = ModelMetadata.PrimaryKeys[0].DefaultValue;
    var primaryKeyType = ModelMetadata.PrimaryKeys[0].TypeName;
    var primaryKeyNullableTypeName = GetNullableTypeName(primaryKeyType, primaryKeyShortTypeName);
    var lambdaVar = ModelVariable[0];
    var relatedProperties = ModelMetadata.RelatedEntities.ToDictionary(item => item.AssociationPropertyName);

    string bindAttribute;
    if (IsOverpostingProtectionRequired)
    {
        bindAttribute = String.Format("[Bind(Include = \"{0}\")] ", BindAttributeIncludeText);
    }
    else
    {
        bindAttribute = String.Empty;
    }
#>
	[CompressResponseAttribute]
    public class <#= ControllerName #> : BaseController
    {
		#region -- Properties --
		private readonly IVuViecService _vuViecService;
		private readonly I<#= _controllerName #>Service _<#= _varControllerName #>Service;
        private string sysCategory = "<#= _controllerName.ToUpper() #>";
        private bool? permission = false;
        private VM_<#= _controllerName #> vm_<#= _controllerName #>;
        private M_<#= _controllerName #> m_<#= _controllerName #>;
        private string hasValue;
		#endregion
		
		#region -- Contructor --
		public <#= _controllerName #>Controller
            (
				IVuViecService vuViecService
                , I<#= _controllerName #>Service <#= _varControllerName #>Service
            ) : base(vuViecService)
        {
            _vuViecService = vuViecService;
            _<#= _varControllerName #>Service = <#= _varControllerName #>Service;
        }
		#endregion

<# if (UseAsync) { #>
        public async Task<ActionResult> Index()
<# } else { #>
        public ActionResult Index()
<# } #>
        {
			#region -- Role user --
            permission = GetPermission(sysCategory, Authorities.View);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            return View();
        }

<# if (UseAsync) { #>
        public async Task<ActionResult> Details(<#= primaryKeyNullableTypeName #> id)
<# } else { #>
        public ActionResult Details(<#= primaryKeyNullableTypeName #> id)
<# } #>
        {
			if (id == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.ViewDetail);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            vm_<#= _controllerName #> = Mapper.Map<VM_<#= _controllerName #>>(_<#= _varControllerName #>Service.GetBy(id));
            if (vm_<#= _controllerName #> == null)
                return HttpNotFound();
            return View(vm_<#= _controllerName #>);
        }

        public ActionResult Create()
        {
			#region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Add);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
            vm_<#= _controllerName #> = new VM_<#= _controllerName #>();
            return View(vm_<#= _controllerName #>);
        }

<# if (IsOverpostingProtectionRequired) {
    foreach (var line in OverpostingWarningMessage.Split(new string[] { Environment.NewLine }, StringSplitOptions.None)) { 
#>
<# } } #>
        [HttpPost]
<# if (UseAsync) { #>
        public async Task<ActionResult> Create(<#= ModelTypeName #> _vm_<#= _controllerName #>)
<# } else { #>
        public ActionResult Create(VM_<#= _controllerName #> _vm_<#= _controllerName #>)
<# } #>
        {
			try
            {
                SetData(_vm_<#= _controllerName #>);
                hasValue = _<#= _varControllerName #>Service.Insert(m_<#= _controllerName #>);
				hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = m_<#= _controllerName #>.Id, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

<# if (UseAsync) { #>
        public async Task<ActionResult> Edit(<#= primaryKeyNullableTypeName #> id)
<# } else { #>
        public ActionResult Edit(<#= primaryKeyNullableTypeName #> id)
<# } #>
        {
			if (id == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            #region -- Role user --
            permission = GetPermission(sysCategory, Authorities.Edit);
            if (!permission.HasValue) return RedirectToAction("Index", "Login", new { area = "administrator", url = CurrentUrl() });
            if (!permission.Value) return View("AccessDenied");
            #endregion
			vm_<#= _controllerName #> = Mapper.Map<VM_<#= _controllerName #>>(_<#= _varControllerName #>Service.GetBy(id));
            if (vm_<#= _controllerName #> == null) 
				return HttpNotFound();
            return View(vm_<#= _controllerName #>);
        }

<# if (IsOverpostingProtectionRequired) {
    foreach (var line in OverpostingWarningMessage.Split(new string[] { Environment.NewLine }, StringSplitOptions.None)) { 
#>
<# } } #>
        [HttpPost]
<# if (UseAsync) { #>
        public async Task<ActionResult> Edit(<#= bindAttribute #><#= ModelTypeName #> <#= ModelVariable #>)
<# } else { #>
        public ActionResult Edit(VM_<#= _controllerName #> _vm_<#= _controllerName #>)
<# } #>
        {
			try
            {
                SetData(_vm_<#= _controllerName #>);
                hasValue = _<#= _varControllerName #>Service.Update(m_<#= _controllerName #>);
                hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return RedirectToAction("Edit", new { id = m_<#= _controllerName #>.ID, msg = "1" });
                ViewBag.Error = hasValue;
                return View("Error");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                return View("Error");
            }
        }

<# if (UseAsync) { #>
        public async Task<ActionResult> Delete(<#= primaryKeyNullableTypeName #> id)
<# } else { #>
        public ActionResult Delete(string id)
<# } #>
        {
            #region -- Roles --
            permission = GetPermission(sysCategory, Authorities.Del);
            if (!permission.HasValue) return Json("Bạn đã hết phiên làm việc<BR />Hãy thoát ra và đăng nhập lại");
            if (!permission.Value) return Json("Bạn không có quyền thực hiện chức năng này.<BR />Vui lòng liên hệ với Administrator để được hổ trợ.");
            #endregion
            try
            {
                int[] _codes = id.Split(',').Select(item => int.Parse(item)).ToArray();
                hasValue = _<#= _varControllerName #>Service.Delete(_codes);
				hasValue = _vuViecService.Commit();
                if (string.IsNullOrEmpty(hasValue))
                    return Json(new { v = true, count = _codes.Count() });
                return Json("Xóa dữ liệu không thành công !");
            }
            catch { return Json("Xóa dữ liệu không thành công"); }
        }
		#region -- Json --
		public JsonResult LoadData(GridSettings grid)
        {
            var list = Mapper.Map<IEnumerable<VM_<#= _controllerName #>>>(_<#= _varControllerName #>Service.FindAll()).AsQueryable();
            list = JqGrid.SetupGrid<VM_<#= _controllerName #>>(grid, list);
            return Json(list.ToJqGridData(grid.PageIndex, grid.PageSize, null, null, null), JsonRequestBehavior.AllowGet);
        }
		#endregion
		#region -- Functions --
		private void SetData(VM_<#= _controllerName #> _vm_<#= _controllerName #>)
        {
            vm_<#= _controllerName #> = _vm_<#= _controllerName #>;
            vm_<#= _controllerName #>.Id = _<#= _varControllerName #>Service.GetID();
            m_<#= _controllerName #> = Mapper.Map<M_<#= _controllerName #>>(vm_<#= _controllerName #>);
        }
		#endregion
    }
}
<#+
// This function converts the primary key short type name to its nullable equivalent when possible. This is required to make
// sure that an HTTP 400 error is thrown when the user tries to access the edit, delete, or details action with null values.
    string GetNullableTypeName(string typeName, string shortTypeName)
    {
        // The exceptions are caught because if for any reason the type is user defined, then the short type name will be used.
        // In that case the user will receive a server error if null is passed to the edit, delete, or details actions.
        Type primaryKeyType = null;
        try
        {
            primaryKeyType = Type.GetType(typeName);
        }
        catch
        {
        }
        if (primaryKeyType != null && (primaryKeyType.IsPrimitive || IsGuid(typeName)))
        {
            return shortTypeName + "?";
        }
        return shortTypeName;
    }

    bool IsGuid(string typeName) {
        return String.Equals("System.Guid", typeName, StringComparison.OrdinalIgnoreCase);
    }
#>