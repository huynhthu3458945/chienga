﻿using GCSOFT.MVC.Web.Helper.ActionFilters;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Controllers
{
    [CompressResponseAttribute]
    public class HomeController : Controller
    {
        public HomeController()
        {
           
        }
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Dashboard", new { area = "administrator" });
        }
    }
}