﻿using GCSOFT.MVC.Data;
using GCSOFT.MVC.Web.App_Start;
using System.Configuration;
using System.Data.Entity;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using License = ASOFT.ERP.ASoftLicense;

namespace GCSOFT.MVC.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            // Init database
            License.ASoftReportLicense.SetExcelLicense();
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            // Autofac and Automapper configurations
            Bootstrapper.Run();
            
        }
    }
}
