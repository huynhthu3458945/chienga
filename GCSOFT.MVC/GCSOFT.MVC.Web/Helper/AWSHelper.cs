﻿using Amazon;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GCSOFT.MVC.Web.Helper
{
    public class AWSHelper
    {
        public string displayName { get; set; }
        public string fromEmail { get; set; }
        public List<string> EmailTos { get; set; }
        public List<string> EmailCCs { get; set; }
        public List<string> EmailBCCs { get; set; }
        public string subject { get; set; }
        public string body { get; set; }
        public string replyTo { get; set; }
        public string returnTo { get; set; }
        private readonly string AWSAccessKeyId = "AKIAYH6QPHY2JOZNW7MZ";
        private readonly string AWSSecretKey = "BM9FOOV8h1zYMLvMetXiaAlNCN/n0XfuaedH/xXxZnB9";
        public string Sendmail()
        {
            replyTo = string.IsNullOrEmpty(replyTo) ? fromEmail : replyTo;
            using (var client = new AmazonSimpleEmailServiceClient(AWSAccessKeyId, AWSSecretKey, RegionEndpoint.USWest2))
            {

                var sendRequest = new SendEmailRequest
                {
                    Source = fromEmail, //displayName + "<" + fromEmail + ">"
                    Destination = new Destination { ToAddresses = EmailTos, CcAddresses = EmailCCs ?? new List<string>(), BccAddresses = EmailBCCs ?? new List<string>() },
                    Message = new Message
                    {
                        Subject = new Content(subject),
                        Body = new Body { Html = new Content { Data = body, Charset = "UTF-8" } }
                    },
                    ReplyToAddresses = new List<string> { replyTo },
                    ReturnPath = returnTo
                };
                try
                {
                    var response = client.SendEmail(sendRequest);
                    return string.Empty;
                }
                catch (Exception ex) { return ex.ToString(); }
            }
        }
    }
}