﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace GCSOFT.MVC.Web.Helper
{
    public class HttpClientHelper
    {
        private string  DOMAINAPI = ConfigurationManager.AppSettings["DOMAINAPI"];
        private string TOKEN = ConfigurationManager.AppSettings["TOKEN"];
        private const string PORTAPI = "44348";
        private HttpClientHandler clientHandler = new HttpClientHandler();

        /// <summary>
        ///     PostAsync
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="url"></param>
        /// <param name="model"></param>
        /// <param name="contentType"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [27/09/2021]
        /// </history>
        public async Task<ResponseAPI<T>> PostAsync<T>(string url, T model, string contentType = "application/json")
        {
            try
            {
                using (var httpClient = new HttpClient(clientHandler, false))
                {
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(contentType));
                    httpClient.Timeout = TimeSpan.FromMinutes(10);

                    //HttpResponseMessage response =  httpClient.PostAsJsonAsync($"{DOMAINAPI}{PORTAPI}{url}", model).Result;
                    HttpResponseMessage response = httpClient.PostAsJsonAsync($"{DOMAINAPI}{url}", model).Result;
                    httpClient.DefaultRequestHeaders.ConnectionClose = true;
                    return (await response.Content.ReadAsAsync<ResponseAPI<T>>());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///     PostTokenAsync
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="url"></param>
        /// <param name="model"></param>
        /// <param name="contentType"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [27/09/2021]
        /// </history>
        public async Task<ResponseDataAPI> PostUrlAsync<T>(string url, T model, string contentType = "application/json")
        {
            try
            {
                using (var httpClient = new HttpClient(clientHandler, false))
                {
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(contentType));
                    //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TOKEN);
                    //httpClient.DefaultRequestHeaders.Add("app-secret", "tamtriluc-firebase");
                    httpClient.Timeout = TimeSpan.FromMinutes(10);

                    //HttpResponseMessage response =  httpClient.PostAsJsonAsync($"{DOMAINAPI}{PORTAPI}{url}", model).Result;
                    HttpResponseMessage response = httpClient.PostAsJsonAsync($"{url}", model).Result;
                    httpClient.DefaultRequestHeaders.ConnectionClose = true;
                    return (await response.Content.ReadAsAsync<ResponseDataAPI>());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///     PostAsync Not List Data
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="url"></param>
        /// <param name="model"></param>
        /// <param name="contentType"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [27/09/2021]
        /// </history>
        public async Task<ResponseNotListDataAPI<T>> PostNotListDataAsync<T>(string url, T model, string contentType = "application/json")
        {
            try
            {
                using (var httpClient = new HttpClient(clientHandler, false))
                {
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(contentType));
                    httpClient.Timeout = TimeSpan.FromMinutes(10);

                    //HttpResponseMessage response =  httpClient.PostAsJsonAsync($"{DOMAINAPI}{PORTAPI}{url}", model).Result;
                    HttpResponseMessage response = httpClient.PostAsJsonAsync($"{DOMAINAPI}{url}", model).Result;
                    httpClient.DefaultRequestHeaders.ConnectionClose = true;
                    return (await response.Content.ReadAsAsync<ResponseNotListDataAPI<T>>());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///     GetAsync
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="url"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [27/09/2021]
        /// </history>
        public async Task<ResponseAPI<T>> GetAsync<T>(string url)
        {
            try
            {
                using (HttpClient httpClient = new HttpClient(clientHandler, false))
                {
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    httpClient.Timeout = TimeSpan.FromMinutes(10);
                    //var response =  httpClient.GetAsync($"{DOMAINAPI}{PORTAPI}{url}").Result;
                    var response = httpClient.GetAsync($"{DOMAINAPI}{url}").Result;
                    httpClient.DefaultRequestHeaders.ConnectionClose = true;
                    return (await response.Content.ReadAsAsync<ResponseAPI<T>>());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///     GetAsync
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="url"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [27/09/2021]
        /// </history>
        public async Task<ResponseAPI<T>> GetTokeAsync<T>(string url)
        {
            try
            {
                using (HttpClient httpClient = new HttpClient(clientHandler, false))
                {
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TOKEN);
                    httpClient.DefaultRequestHeaders.Add("app-secret", "tamtriluc-firebase");
                    httpClient.Timeout = TimeSpan.FromMinutes(10);
                    //var response =  httpClient.GetAsync($"{DOMAINAPI}{PORTAPI}{url}").Result;
                    var response = httpClient.GetAsync($"{url}").Result;
                    httpClient.DefaultRequestHeaders.ConnectionClose = true;
                    return (await response.Content.ReadAsAsync<ResponseAPI<T>>());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        ///     DeleteAsync
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="url"></param>
        /// <param name="model"></param>
        /// <param name="contentType"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [27/09/2021]
        /// </history>
        public async Task<ResponseNotListDataAPI<T>> DeleteAsync<T>(string url, T model, string contentType = "application/json", bool isToken = false)
        {
            try
            {
                using (var httpClient = new HttpClient(clientHandler, false))
                {
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(contentType));
                    if(isToken)
                        httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TOKEN);
                    httpClient.DefaultRequestHeaders.Add("app-secret", "tamtriluc-firebase");
                    httpClient.Timeout = TimeSpan.FromMinutes(10);

                    //HttpResponseMessage response =  httpClient.PostAsJsonAsync($"{DOMAINAPI}{PORTAPI}{url}", model).Result;

                    HttpRequestMessage request = new HttpRequestMessage
                    {
                        Content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, contentType),
                        Method = HttpMethod.Delete,
                        RequestUri = new Uri(url)
                    };
                    HttpResponseMessage response = httpClient.SendAsync(request).Result;
                    httpClient.DefaultRequestHeaders.ConnectionClose = true;
                    return (await response.Content.ReadAsAsync<ResponseNotListDataAPI<T>>());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///     DeleteAsync
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="url"></param>
        /// <param name="model"></param>
        /// <param name="contentType"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [27/09/2021]
        /// </history>
        public async Task<ResponseNotListDataAPI<T>> DeleteAsync<T>(string url,  string contentType = "application/json", bool isToken = false)
        {
            try
            {
                using (var httpClient = new HttpClient(clientHandler, false))
                {
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(contentType));
                    if (isToken)
                        httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TOKEN);
                    httpClient.DefaultRequestHeaders.Add("app-secret", "tamtriluc-firebase");
                    httpClient.Timeout = TimeSpan.FromMinutes(10);

                    HttpResponseMessage response =  httpClient.DeleteAsync($"{url}").Result;

                    httpClient.DefaultRequestHeaders.ConnectionClose = true;
                    return (await response.Content.ReadAsAsync<ResponseNotListDataAPI<T>>());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class ResponseAPI<T>
        {
            public string retCode { get; set; }
            public string retText { get; set; }
            public List<T> data { get; set; }
        }

        public class ResponseNotListDataAPI<T>
        {
            public string retCode { get; set; }
            public string retText { get; set; }
            public T data { get; set; }
        }
        public class ResponseDataAPI
        {
            public string retCode { get; set; }
            public string retText { get; set; }
            public string data { get; set; }
        }
    }
}