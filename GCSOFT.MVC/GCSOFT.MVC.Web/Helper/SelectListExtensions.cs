﻿// #################################################################
// # Copyright (C) 2021-2022, Tâm Trí Lực JSC.  All Rights Reserved.                       
// #
// # History：                                                                        
// #	Date Time	    Updated		    Content                	
// #    25/08/2021	    Huỳnh Thử 		Update
// ##################################################################

using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Web.Areas.Agency.Data;
using GCSOFT.MVC.Web.Areas.API.Data;
using GCSOFT.MVC.Web.Areas.MasterData.Models;
using GCSOFT.MVC.Web.Areas.NamPhutThuocBai.Data;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.Helper
{
    public static class SelectListExtensions
    {
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<ProvinceViewModel> provinces, int selectedId)
        {
            return provinces.Select(item => new SelectListItem
            {
                Selected = (item.id == selectedId),
                Text = item.name,
                Value = item.id.ToString()
            });
        }
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<DistrictViewModel> districts, int selectedId)
        {
            return districts.Select(item => new SelectListItem
            {
                Selected = (item.id == selectedId),
                Text = item.name,
                Value = item.id.ToString()
            });
        }
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<PackageList> packages, int selectedId)
        {
            return packages.Select(item => new SelectListItem
            {
                Selected = (item.Id == selectedId),
                Text = item.LeavelName,
                Value = item.Id.ToString()
            });
        }
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<AgencyList> agencies, int selectedId)
        {
            return agencies.Select(item => new SelectListItem
            {
                Selected = (item.Id == selectedId),
                Text = string.Format("{0} - {1}", item.FullName, item.Phone),
                Value = item.Id.ToString()
            });
        }
        
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<TeacherList> parents, int selectedId)
        {
            return parents.Select(item => new SelectListItem
            {
                Selected = (item.Id == selectedId),
                Text = item.FullName,
                Value = item.Id.ToString()
            });
        }
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<Parent_List> parents, int selectedId)
        {
            return parents.Select(item => new SelectListItem
            {
                Selected = (item.Id == selectedId),
                Text = item.FullName,
                Value = item.Id.ToString()
            });
        }
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<VM_OptionLine> parents, int selectedId)
        {
            return parents.Select(item => new SelectListItem
            {
                Selected = (item.LineNo == selectedId),
                Text = item.Name,
                Value = item.LineNo.ToString()
            });
        }
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<VM_OptionLine> parents, string selectedId, bool plusName = false)
        {
            return parents.Select(item => new SelectListItem
            {
                Selected = (item.Code == selectedId),
                Text = plusName ? $"{item.Code} - {item.Name}" : item.Name,
                Value = item.Code.ToString()
            });
        }
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<CategoryListVMs> parents, string selectedId)
        {
            return parents.Select(item => new SelectListItem
            {
                Selected = (item.Code == selectedId),
                Text = item.Link,
                Value = item.Code
            });
        }
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<E_Category> parents, int selectedId)
        {
            return parents.Select(item => new SelectListItem
            {
                Selected = (item.Id == selectedId),
                Text = item.SearchName,
                Value = item.Id.ToString()
            });
        }
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<CourseList> courseList, int selectedId)
        {
            return courseList.Select(item => new SelectListItem
            {
                Selected = (item.Id == selectedId),
                Text = item.Title,
                Value = item.Id.ToString()
            });
        }
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<CourseContentLine> courseContentList, int selectedId)
        {
            return courseContentList.Select(item => new SelectListItem
            {
                Selected = (item.LineNo == selectedId),
                Text = item.Title,
                Value = item.LineNo.ToString()
            });
        }
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<VM_Class> classes, int selectedId)
        {
            return classes.Select(item => new SelectListItem
            {
                Selected = (item.Id == selectedId),
                Text = item.Name,
                Value = item.Id.ToString()
            });
        }
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<UserVM> userVMs, string userName)
        {
            return userVMs.Select(item => new SelectListItem
            {
                Selected = (item.Username == userName),
                Text = item.FullName,
                Value = item.Username.ToString()
            });
        }
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<UserVM> userVMs, int userId)
        {
            return userVMs.Select(item => new SelectListItem
            {
                Selected = (item.ID == userId),
                Text = item.FullName,
                Value = item.ID.ToString()
            });
        }
        public static IEnumerable<SelectListItem> ToSelectListItems(this Dictionary<string, string> parents, string selectedId)
        {
            return parents.Select(item => new SelectListItem
            {
                Selected = (item.Key == selectedId),
                Text = item.Value,
                Value = item.Key.ToString()
            });
        }
        public static IEnumerable<SelectListItem> ToSelectListItems(this Dictionary<int, string> parents, int selectedId)
        {
            return parents.Select(item => new SelectListItem
            {
                Selected = (item.Key == selectedId),
                Text = item.Value,
                Value = item.Key.ToString()
            });
        }
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<TrongTaiList> trongTaiList, int selectedId)
        {
            return trongTaiList.Select(item => new SelectListItem
            {
                Selected = (item.Id == selectedId),
                Text = item.FullName,
                Value = item.Id.ToString()
            });
        }

        /// <summary>
        ///     ToSelectListItems
        /// </summary>
        /// <param name="parents"></param>
        /// <param name="selectedId"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [25/08/2021]
        /// </history>
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<VM_Promotion> parents, string selectedId)
        {
            return parents.Select(item => new SelectListItem
            {
                Selected = (item.Code == selectedId),
                Text = item.Title,
                Value = item.Code.ToString()
            });
        }

        /// <summary>
        ///     ToSelectListItems
        /// </summary>
        /// <param name="parents"></param>
        /// <param name="selectedId"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [25/08/2021]
        /// </history>
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<ImageAndVideo> parents, string selectedId)
        {
            return parents.Select(item => new SelectListItem
            {
                Selected = (item.GroupCode == selectedId),
                Text = item.GroupName,
                Value = item.GroupCode.ToString()
            });
        }

        /// <summary>
        ///     ToSelectListItems
        /// </summary>
        /// <param name="parents"></param>
        /// <param name="selectedId"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [01/10/2021]
        /// </history>
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<Teacher> parents, string selectedId)
        {
            return parents.Select(item => new SelectListItem
            {
                Selected = (item.LevelCode == selectedId),
                Text = item.LevelName,
                Value = item.LevelCode.ToString()
            });
        }
        /// <summary>
        ///     ToSelectListItems
        /// </summary>
        /// <param name="parents"></param>
        /// <param name="selectedId"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [01/10/2021]
        /// </history>
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<AgencyAPI> parents, string selectedId)
        {
            return parents.Select(item => new SelectListItem
            {
                Selected = (item.AgencyGroupCode == selectedId),
                Text = item.AgencyGroupName,
                Value = string.IsNullOrEmpty(item.AgencyGroupCode) ? "Other" : item.AgencyGroupCode.ToString()
            });
        }

        /// <summary>
        ///     ToSelectListItems
        /// </summary>
        /// <param name="parents"></param>
        /// <param name="selectedId"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [15/10/2021]
        /// </history>
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<News> parents, string selectedId)
        {
            return parents.Select(item => new SelectListItem
            {
                Selected = (item.GroupCode == selectedId),
                Text = item.GroupName,
                Value = item.GroupCode
            });
        }
        /// <summary>
        ///     ToSelectListItems
        /// </summary>
        /// <param name="parents"></param>
        /// <param name="selectedId"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [15/10/2021]
        /// </history>
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<SupportQuestion> parents, string selectedId)
        {
            return parents.Select(item => new SelectListItem
            {
                Selected = (item.GroupCode == selectedId),
                Text = item.GroupName,
                Value = item.GroupCode
            });
        }
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<CourseContentCard> courseContents, int selectedId)
        {
            return courseContents.OrderBy(d => d.SortOrder).Select(item => new SelectListItem
            {
                Selected = (item.SortOrder == selectedId),
                Text = item.SortOrder.ToString() + " - " + item.Title,
                Value = item.SortOrder.ToString()
            });
        }
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<CourseContentReview> courseContents, int selectedId)
        {
            return courseContents.OrderBy(d => d.LineNo).Select(item => new SelectListItem
            {
                Selected = (item.LineNo == selectedId),
                Text = item.RenewTitle,
                Value = item.LineNo.ToString()
            });
        }
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<OptionInt> courseContents, int selectedId)
        {
            return courseContents.OrderBy(d => d.Key).Select(item => new SelectListItem
            {
                Selected = (item.Key == selectedId),
                Text = item.Value,
                Value = item.Key.ToString()
            });
        }

        /// <summary>
        ///     ToSelectListItems
        /// </summary>
        /// <param name="parents"></param>
        /// <param name="selectedId"></param>
        /// <returns></returns>
        /// <history>
        ///     [Huỳnh Thử] Create [15/10/2021]
        /// </history>
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<M_OptionLine> parents, string selectedId)
        {
            var selectlistItem = new List<SelectListItem>();
            var selectItem = new SelectListItem();
            selectItem.Selected = false;
            selectItem.Text = "Tất cả";
            selectItem.Value = "All";
            selectlistItem.Add(selectItem);
            foreach(var item in parents)
            {
                var select = new SelectListItem()
                {
                    Selected = (item.Code == selectedId),
                    Text = item.Name,
                    Value = item.Code
                };
                selectlistItem.Add(select);
            }
            return selectlistItem;
        }
    }
}