﻿using GCSOFT.MVC.Web.ViewModels.jqGrid;
using System.Collections.Generic;
using System.Linq;

namespace GCSOFT.MVC.Web.Helper
{
    public class JqGrid
    {
        /// <summary>
        /// Thiết lập JQUERY GRID
        /// </summary>
        /// <typeparam name="T">Kiểu dữ liệu</typeparam>
        /// <param name="grid">Thông tin GRID</param>
        /// <param name="model">Model dạng IQueryable</param>
        /// <returns>GRID đã thiết lập dưới dạng IQueryable</returns>
        public static IQueryable<T> SetupGrid<T>(GridSettings grid, IQueryable<T> model)
        {
            if (grid.Where != null)
            {
                //TÌM KIẾM
                if (grid.IsSearch)
                {
                    //TỔ HỢP CÁC ĐIỀU KIỆN (AND)
                    if (grid.Where.groupOp == "AND")
                        foreach (var rule in grid.Where.rules)
                            model = model.Where(rule.field, rule.data, (WhereOperation)StringEnum.Parse(typeof(WhereOperation), rule.op));
                    else
                    {
                        //MỘT TRONG CÁC ĐIỀU KIỆN (OR)
                        var temp = (new List<T>()).AsQueryable();
                        foreach (var rule in grid.Where.rules)
                        {
                            var t = model.Where(rule.field, rule.data, (WhereOperation)StringEnum.Parse(typeof(WhereOperation), rule.op));
                            temp = temp.Concat(t);
                        }
                        //XÓA CÁC TRƯỜNG BỊ LẶP LẠI
                        model = temp.Distinct();
                    }
                }
            }
            else if(grid.IsSearch)
            {
                model = model.Where(grid.SearchField, grid.SearchString, (WhereOperation)StringEnum.Parse(typeof(WhereOperation), grid.SearchOper));
            }

            model = model.OrderBy(grid.SortColumn, grid.SortOrder);
            return model;
        }

    }
}
