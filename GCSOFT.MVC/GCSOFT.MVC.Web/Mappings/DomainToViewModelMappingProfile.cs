﻿using AutoMapper;
using GCSOFT.MVC.Model;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Model.Approval;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.NamPhutThuocBai;
using GCSOFT.MVC.Model.StoreProcedue;
using GCSOFT.MVC.Model.SystemModels;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Web.Areas.Agency.Data;
using GCSOFT.MVC.Web.Areas.API.Data;
using GCSOFT.MVC.Web.Areas.Approval.Data;
using GCSOFT.MVC.Web.Areas.MasterData.Models;
using GCSOFT.MVC.Web.Areas.NamPhutThuocBai.Data;
using GCSOFT.MVC.Web.Areas.Transaction.Data;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels.Administrator;

namespace GCSOFT.MVC.Web.Mappings
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "DomainToViewModelMappings"; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<HT_Users, UserLoginVMs>();
            Mapper.CreateMap<HT_Users, UserVM>();
            Mapper.CreateMap<HT_VuViec, VuViecVMs>();
            Mapper.CreateMap<HT_CongViec, CategoryUser>();
            Mapper.CreateMap<HT_CongViec, CategoryListVMs>();
            Mapper.CreateMap<HT_CongViec, CategoryCard>();
            Mapper.CreateMap<HT_VuViecCuaCongViec, CategoryFunctionVMs>();
            //-- Nhom User
            Mapper.CreateMap<HT_NhomUser, NhomUsers>();
            Mapper.CreateMap<HT_NhomUser, NhomUserCard>();
            Mapper.CreateMap<HT_UserThuocNhom, UserThuocNhomVM>();
            //-- Nhom User End
            //-- User
            Mapper.CreateMap<HT_Users, UserList>();
            Mapper.CreateMap<HT_Users, UserCard>();
            //-- User End
            Mapper.CreateMap<M_OptionType, VM_OptionType>();
            Mapper.CreateMap<M_OptionLine, VM_OptionLine>();
            Mapper.CreateMap<E_Category, VM_Category>();
            Mapper.CreateMap<E_Category, VM_Category_Index>();
            Mapper.CreateMap<E_Category, CategoryMenu>();
            Mapper.CreateMap<E_Teacher, TeacherList>();
            Mapper.CreateMap<E_Teacher, TeacherCard>();

            Mapper.CreateMap<E_Course, CourseList>();
            Mapper.CreateMap<E_Course, CourseCard>();
            Mapper.CreateMap<E_CourseContent, CourseContentLine>();
            Mapper.CreateMap<E_CourseContent, CourseContentCard>();
            Mapper.CreateMap<E_CourseContent, CourseContentReview>();
            Mapper.CreateMap<E_CourseSupport, CourseSupportLine>();
            Mapper.CreateMap<E_CourseCategory, CourseCategoryLine>();
            Mapper.CreateMap<E_CourseResource, CourseResourceLine>();
            Mapper.CreateMap<E_Class, VM_Class>();

            Mapper.CreateMap<M_Parents, Parent_List>();
            Mapper.CreateMap<M_Parents, Parent_Card>();
            Mapper.CreateMap<M_Children, VM_Childrens>();
            Mapper.CreateMap<M_Contact, VM_Contact>();

            Mapper.CreateMap<M_Question, Question_List>();
            Mapper.CreateMap<M_Question, Question_Card>();
            Mapper.CreateMap<M_ResultEntry, VM_ResultEntry>();
            Mapper.CreateMap<M_FileResource, VM_FileResource>();
            Mapper.CreateMap<M_Question, Suggestions>();
            Mapper.CreateMap<M_Supporter, VM_Supporter>();
            Mapper.CreateMap<M_Comment, VM_Comment>();
            Mapper.CreateMap<M_PurchaseHeader, CardList>();
            Mapper.CreateMap<M_PurchaseHeader, CardOrder>();
            Mapper.CreateMap<M_UserInfo, VM_UserInfo>();
            Mapper.CreateMap<M_Package, PackageList>();
            Mapper.CreateMap<M_Package, PackageCard>();
            Mapper.CreateMap<M_Agency, AgencyList>();
            Mapper.CreateMap<M_Agency, AgencyCard>();
            Mapper.CreateMap<M_CardHeader, CardHeaderList>();
            Mapper.CreateMap<M_CardHeader, CardHeaderCard>();
            Mapper.CreateMap<M_CardLine, CardLine>();
            Mapper.CreateMap<M_CardEntry, CardEntry>();
            Mapper.CreateMap<M_SalesHeader, SalesOrderList>();
            Mapper.CreateMap<M_SalesHeader, SalesOrderCard>();
            Mapper.CreateMap<M_SalesLine, SalesLine>();
            Mapper.CreateMap<M_District, DistrictViewModel>();
            Mapper.CreateMap<M_Province, ProvinceViewModel>();
            Mapper.CreateMap<M_MailSetup, MailSetup>();
            Mapper.CreateMap<M_Template, Template>();
            Mapper.CreateMap<M_ApprovalInfo, ApprovalInfoViewModel>();
            Mapper.CreateMap<M_Flow, FlowViewModel>();
            Mapper.CreateMap<M_Flow, FlowApproval>();
            Mapper.CreateMap<M_Customer, CustomerViewModel>();
            Mapper.CreateMap<M_QuestionFilter, QuestionFilter>();
            Mapper.CreateMap<M_PaymentResult, PaymentResultViewModel>();
            Mapper.CreateMap<M_Ticket, TicketViewModel>();
            Mapper.CreateMap<M_Ticket, TicketList>();
            Mapper.CreateMap<M_Language, VM_Language>();
            Mapper.CreateMap<TB_NopBai, NopBaiList>();
            Mapper.CreateMap<TB_NopBai, NopBaiCard>();
            Mapper.CreateMap<TB_TrongTai, TrongTaiList>();
            Mapper.CreateMap<TB_TrongTai, TrongTaiCard>();
            Mapper.CreateMap<M_RetailSalesOrder, RetailSalesOrder>();
            Mapper.CreateMap<TB_MindMapPoint, MindMapPoint>();
            Mapper.CreateMap<E_Class, ClassList>();
            Mapper.CreateMap<M_StudentClass, StudentClassViewModel>();
            Mapper.CreateMap<M_Promotion, VM_Promotion>();
            Mapper.CreateMap<M_Promotion, ViewModelPromotionList>();
            Mapper.CreateMap<M_Promotion, ViewModelPromotion>();
            Mapper.CreateMap<TB_TransactionGift, TransactionGift>();
            Mapper.CreateMap<SP_SYS_Categroy_Tree, CategoryListVMs>();
            Mapper.CreateMap<M_PotentialCard, PotentialList>();
            Mapper.CreateMap<M_PotentialCard, PotentialCard>();
            Mapper.CreateMap<E_SubEcourceContent, SubEcourceContent>();
            
            Mapper.CreateMap<M_UserInfoOfGroup, VMUserInfoOfGroup>();
            Mapper.CreateMap<M_OptionLine, VMOptionLine>();
            Mapper.CreateMap<SP_ListTop, ListTop>();
            Mapper.CreateMap<M_Firebase, VMFirebase>();
            Mapper.CreateMap<M_UserInfo, VMUserInfo>();
            Mapper.CreateMap<M_MessageMaster, MessageVMs>();
        }
    }
}