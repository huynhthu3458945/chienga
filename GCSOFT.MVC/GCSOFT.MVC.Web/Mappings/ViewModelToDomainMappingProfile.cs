﻿using AutoMapper;
using GCSOFT.MVC.Model;
using GCSOFT.MVC.Model.AgencyModel;
using GCSOFT.MVC.Model.Approval;
using GCSOFT.MVC.Model.MasterData;
using GCSOFT.MVC.Model.NamPhutThuocBai;
using GCSOFT.MVC.Model.StoreProcedue;
using GCSOFT.MVC.Model.SystemModels;
using GCSOFT.MVC.Model.Transaction;
using GCSOFT.MVC.Web.Areas.Agency.Data;
using GCSOFT.MVC.Web.Areas.API.Data;
using GCSOFT.MVC.Web.Areas.Approval.Data;
using GCSOFT.MVC.Web.Areas.MasterData.Models;
using GCSOFT.MVC.Web.Areas.NamPhutThuocBai.Data;
using GCSOFT.MVC.Web.Areas.Transaction.Data;
using GCSOFT.MVC.Web.ViewModels.MasterData;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels;
using GCSOFT.MVC.Web.ViewModels.SystemViewModels.Administrator;

namespace GCSOFT.MVC.Web.Mappings
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "ViewModelToDomainMappings"; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<UserLoginVMs, HT_Users>();
            Mapper.CreateMap<UserVM, HT_Users>();
            Mapper.CreateMap<HT_VuViec, VuViecVMs>();
            Mapper.CreateMap<CategoryUser, HT_CongViec>();
            Mapper.CreateMap<CategoryListVMs, HT_CongViec>();
            Mapper.CreateMap<CategoryCard, HT_CongViec>();
            Mapper.CreateMap<CategoryFunctionVMs, HT_VuViecCuaCongViec>();
            Mapper.CreateMap<NhomUsers, HT_NhomUser>();
            Mapper.CreateMap<NhomUserCard, HT_NhomUser>();
            Mapper.CreateMap<UserThuocNhomVM, HT_UserThuocNhom>();
            Mapper.CreateMap<UserList, HT_Users>();
            Mapper.CreateMap<UserCard, HT_Users>();
            Mapper.CreateMap<VM_OptionType, M_OptionType>();
            Mapper.CreateMap<VM_OptionLine, M_OptionLine>();
            Mapper.CreateMap<VM_Category, E_Category>();
            Mapper.CreateMap<VM_Category_Index, E_Category>();
            Mapper.CreateMap<CategoryMenu, E_Category>();
            Mapper.CreateMap<TeacherList, E_Teacher>();
            Mapper.CreateMap<TeacherCard, E_Teacher>();
            Mapper.CreateMap<CourseList, E_Course>();
            Mapper.CreateMap<CourseCard, E_Course>();
            Mapper.CreateMap<CourseContentLine, E_CourseContent>();
            Mapper.CreateMap<CourseContentCard, E_CourseContent>();
            Mapper.CreateMap<CourseContentReview, E_CourseContent>();
            Mapper.CreateMap<CourseSupportLine, E_CourseSupport>();
            Mapper.CreateMap<CourseCategoryLine, E_CourseCategory>();
            Mapper.CreateMap<CourseResourceLine, E_CourseResource>();
            Mapper.CreateMap<VM_Class, E_Class>();

            Mapper.CreateMap<Parent_List, M_Parents>();
            Mapper.CreateMap<Parent_Card, M_Parents>();
            Mapper.CreateMap<VM_Childrens, M_Children>();
            Mapper.CreateMap<VM_Contact, M_Contact>();

            Mapper.CreateMap<Question_List, M_Question>();
            Mapper.CreateMap<Question_Card, M_Question>();
            Mapper.CreateMap<VM_ResultEntry, M_ResultEntry>();
            Mapper.CreateMap<VM_FileResource, M_FileResource>();
            Mapper.CreateMap<Suggestions, M_Question>();
            Mapper.CreateMap<VM_Supporter, M_Supporter>();
            Mapper.CreateMap<VM_Comment, M_Comment>();
            Mapper.CreateMap<CardList, M_PurchaseHeader>();
            Mapper.CreateMap<CardOrder, M_PurchaseHeader>();
            Mapper.CreateMap<VM_UserInfo, M_UserInfo>();
            Mapper.CreateMap<PackageList, M_Package>();
            Mapper.CreateMap<PackageCard, M_Package>();
            Mapper.CreateMap<AgencyList, M_Agency>();
            Mapper.CreateMap<AgencyCard, M_Agency>();
            Mapper.CreateMap<CardHeaderList, M_CardHeader>();
            Mapper.CreateMap<CardHeaderCard, M_CardHeader>();
            Mapper.CreateMap<CardLine, M_CardLine>();
            Mapper.CreateMap<CardEntry, M_CardEntry>();
            Mapper.CreateMap<SalesOrderList, M_SalesHeader>();
            Mapper.CreateMap<SalesOrderCard, M_SalesHeader>();
            Mapper.CreateMap<SalesLine, M_SalesLine>();
            Mapper.CreateMap<DistrictViewModel, M_District>();
            Mapper.CreateMap<ProvinceViewModel, M_Province>();
            Mapper.CreateMap<MailSetup, M_MailSetup>();
            Mapper.CreateMap<Template, M_Template>();
            Mapper.CreateMap<ApprovalInfoViewModel, M_ApprovalInfo>();
            Mapper.CreateMap<FlowViewModel, M_Flow>();
            Mapper.CreateMap<FlowApproval, M_Flow>();
            Mapper.CreateMap<CustomerViewModel, M_Customer>();
            Mapper.CreateMap<QuestionFilter, M_QuestionFilter>();
            Mapper.CreateMap<PaymentResultViewModel, M_PaymentResult>();
            Mapper.CreateMap<TicketViewModel, M_Ticket>();
            Mapper.CreateMap<TicketList, M_Ticket>();
            Mapper.CreateMap<NopBaiList, TB_NopBai>();
            Mapper.CreateMap<NopBaiCard, TB_NopBai>();
            Mapper.CreateMap<TrongTaiList, TB_TrongTai>();
            Mapper.CreateMap<TrongTaiCard, TB_TrongTai>();
            Mapper.CreateMap<RetailSalesOrder, M_RetailSalesOrder>();
            Mapper.CreateMap<MindMapPoint, TB_MindMapPoint>();
            Mapper.CreateMap<MindMapPoint, TB_MindMapPoint>();
            Mapper.CreateMap<ClassList, E_Class>();
            Mapper.CreateMap<StudentClassViewModel, M_StudentClass>();
            Mapper.CreateMap<ViewModelPromotion, M_Promotion>();
            Mapper.CreateMap<TransactionGift, TB_TransactionGift>();
            Mapper.CreateMap<CategoryListVMs, SP_SYS_Categroy_Tree>();
            Mapper.CreateMap<PotentialList, M_PotentialCard>();
            Mapper.CreateMap<PotentialCard, M_PotentialCard>();
            Mapper.CreateMap<VMUserInfoOfGroup, M_UserInfoOfGroup>();
            Mapper.CreateMap<SubEcourceContent, E_SubEcourceContent>();
            Mapper.CreateMap<VMFirebase, M_Firebase>();
            Mapper.CreateMap<VMUserInfo, M_UserInfo>();
            Mapper.CreateMap<MessageVMs, M_MessageMaster>();
        }
    }
}