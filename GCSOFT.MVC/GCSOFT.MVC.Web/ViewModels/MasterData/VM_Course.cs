﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.ViewModels.MasterData
{
    public class CourseList
    {
        public int Id { get; set; }
        [DisplayName("Khối Lớp")]
        public string ClassName { get; set; }
        [DisplayName("Môn Học")]
        public string Title { get; set; }
        [DisplayName("Ẩn")]
        public bool Status { get; set; }
        public bool IsTNTD { get; set; }
        [DisplayName("Sắp Xếp")]
        public int SortOrder { get; set; }
        public int IdVideos { get; set; }
        public bool IsExperience { get; set; }
    }
    public class CourseCard
    {
        public int Id { get; set; }
        [DisplayName("Giáo Viên")]
        public int IdTeacher { get; set; }
        public string TeacherName { get; set; }
        public IEnumerable<SelectListItem> TeacherList { get; set; }
        [DisplayName("Môn Học")]
        public string Title { get; set; }
        [DisplayName("Môn Học (EN)")]
        public string TitleEN { get; set; }
        [DisplayName("Đường Dẫn")]
        public string URL { get; set; }
        [AllowHtml]
        [DisplayName("Nội Dung Vắn Tắt")]
        public string ShortDescription { get; set; }
        [AllowHtml]
        [DisplayName("Nội Dung Đầy Đủ")]
        public string FullDescription { get; set; }
        [DisplayName("Môn Học 5PTB")]
        public bool Status { get; set; }
        [DisplayName("Thứ Tự")]
        public int SortOrder { get; set; }
        [DisplayName("Ngày Tạo")]
        public DateTime DateCreate { get; set; }
        [DisplayName("Ngày Sửa")]
        public DateTime DateModify { get; set; }
        [DisplayName("Hình Đại Diện")]
        public string Image { get; set; }
        public int AvartarLineNo { get; set; }
        [DisplayName("Liên kế Youtube")]
        public string LinkYoutube { get; set; }
        [DisplayName("Khối Lớp")]
        public int ClassId { get; set; }
        [DisplayName("ID Videos")]
        public int IdVideos { get; set; }
        public string ClassName { get; set; }
        [DisplayName("Cho Phép Trải Nghiệm")]
        public bool IsExperience { get; set; }
        [DisplayName("Môn Học Trắc Nghiệm")]
        public bool IsTNTD { get; set; }
        public string FontColor { get; set; }
        public IEnumerable<SelectListItem> ClassList { get; set; }
        public virtual IEnumerable<CourseContentLine> CourseContents { get; set; }
        public virtual IEnumerable<CourseContentLine> CourseVideos { get; set; }
        public virtual IEnumerable<CourseSupportLine> CourseSupports { get; set; }
        public virtual IEnumerable<CourseCategoryLine> CourseCategories { get; set; }
        public virtual IEnumerable<CourseResourceLine> CourseResources { get; set; }
        public virtual IEnumerable<SqlCourseContent> SqlCourseContents { get; set; }
        public CourseCard()
        {
            CourseContents = new List<CourseContentLine>();
            CourseSupports = new List<CourseSupportLine>();
            CourseCategories = new List<CourseCategoryLine>();
            CourseResources = new List<CourseResourceLine>();
        }
    }
    public class CourseContentLine
    {
        public int IdCourse { get; set; }
        public int LineNo { get; set; }
        [DisplayName("Mục Lục")]
        public string Title { get; set; }
        [DisplayName("Chữ Đậm")]
        public bool Bold { get; set; }
        [DisplayName("In Ngiêng")]
        public bool Italic { get; set; }
        [DisplayName("Ẩn")]
        public bool IsHidden { get; set; }
        [DisplayName("ID Videos")]
        public string LinkYoutube { get; set; }
        [AllowHtml]
        [DisplayName("Diễn Giải")]
        public string FullDescription { get; set; }
        [DisplayName("Code ( Số Thứ Tự )")]
        public int SortOrder { get; set; }
        public CourseType CourseType { get; set; }
        [DisplayName("Thời Gian")]
        public string Times { get; set; }
        [DisplayName("Mã Cha")]
        public int? ParentSortOrder { get; set; }
        [DisplayName("Link Endlish Sub")]
        public string LinkVideoVN { get; set; }
        [DisplayName("Link Việt Sub")]
        public string LinkVideoEN { get; set; }
        [DisplayName("Ẩn Học Kỳ 2")]
        public bool IsHiddenHK2 { get; set; }
        [DisplayName("Trải Nghiệm")]
        public bool IsExperience { get; set; }
        [DisplayName("Loại Bài Học")]
        public string LessonTypeCode { get; set; }
        public string LessonTypeName { get; set; }
        [DisplayName("Các bài kiểm tra Chương")]
        public int[] ReviewCodeArrs { get; set; }
        public IEnumerable<SelectListItem> ReviewList { get; set; }
        public virtual IEnumerable<CourseContentLine> CourseContents { get; set; }
        public IEnumerable<SelectListItem> ParentSortOrderList { get; set; }
        public IEnumerable<SelectListItem> LessonTypeList { get; set; }
    }
    public class CourseContentCard
    {
        public int SortOrder { get; set; }
        public int LineNo { get; set; }
        [DisplayName("Nội Dung Bài Học")]
        public string Title { get; set; }
        [DisplayName("Đường Dẫn Youtube")]
        public string LinkYoutube { get; set; }
        [DisplayName("Nội Dung")]
        public string FullDescription { get; set; }
    }
    public class CourseContentReview
    {
        public int LineNo { get; set; }
        public string Title { get; set; }
        public int SortOrder { get; set; }
        public string RenewTitle { get { return SortOrder.ToString() + " - " + Title; } }
    }
    public class CourseSupportLine
    {
        public int IdCourse { get; set; }
        public int IdTeachcher { get; set; }
        [DisplayName("Họ Tên Hổ Trợ")]
        public string TeachcherName { get; set; }
        [DisplayName("Ghi Chú")]
        public string Remark { get; set; }
        public TeacherCard TecherInfomation { get; set; }
    }
    public class CourseCategoryLine
    {
        public int IdCourse { get; set; }
        public int IdCategory { get; set; }
        [DisplayName("Tên")]
        public string CategroySearchName { get; set; }
        [DisplayName("Ghi Chú")]
        public string Remark { get; set; }
    }
    public class CourseResourceLine
    {
        public int IdCourse { get; set; }
        public int LineNo { get; set; }
        [DisplayName("Tên File")]
        public string Title { get; set; }
        [DisplayName("Hình Ảnh")]
        public string Image { get; set; }
        public string ImageUpload { get; set; }
        public string ImagePath { get { return string.Format("/Files/CourseResource/{0}/{1}", IdCourse, Image); } }
    }
    public class SqlCourseContent
    {
        public int IdCourse { get; set; }
        public int LineNo { get; set; }
        public int SortOrder { get; set; }
        public string Title { get; set; }
        public string LinkVideoEN { get; set; }
        public string LinkVideoVN { get; set; }
        public bool IsHidden { get; set; }
        public string Times { get; set; }
        public int ParentSortOrder { get; set; }
        public string ParentSortOrderName { get; set; }
        public bool IsExperience { get; set; }
        public string LessonTypeCode { get; set; }
        public string LessonTypeName { get; set; }
        public string rn { get; set; }
    }
}