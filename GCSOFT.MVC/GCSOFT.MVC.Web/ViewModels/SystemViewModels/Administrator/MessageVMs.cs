﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.ViewModels.SystemViewModels.Administrator
{
    public class MessageVMs : M_MessageMaster
    {
        public IEnumerable<SelectListItem> TypeList { get; set; }
        public string CreateDateStr { get; set; }
        [AllowHtml]
        public string DescriptionVM { get; set; }
        public List<M_MessageDetail> ListMessageDetail { get; set; }
    }


    public class MessageVMIndex
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string FullName { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateDateStr { get; set; }
    }

    public class VMUserInfo
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public bool Check { get; set; }
    }
}