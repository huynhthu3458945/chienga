﻿using GCSOFT.MVC.Model.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.ViewModels.SystemViewModels
{
    public class StudentVMs
    {
        public string GroupCode { get; set; }
        public IEnumerable<SelectListItem> GroupCodeList { get; set; }
        public IEnumerable<VMUserInfoOfGroup> UserInfoOfGroupList { get; set; }
        public List<M_UserInfoOfGroup> ModelUserInfoOfGroup { get; set; }

        public StudentVMs()
        {
            GroupCodeList = new List<SelectListItem>();
            UserInfoOfGroupList = new List<VMUserInfoOfGroup>();
            ModelUserInfoOfGroup = new List<M_UserInfoOfGroup>();
        }

        
    }
    public class VMUserInfoOfGroup : M_UserInfoOfGroup
    {
        public string fullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }

    public class PhanQuyen {
        public string UserName { get; set; }
        public IEnumerable<VMOptionLine> VMOptionLineList { get; set; }
    }

    public class VMOptionLine : M_OptionLine
    {
        public bool Check { get; set; }
    }
}