﻿namespace GCSOFT.MVC.Web.ViewModels.SystemViewModels.HT_CongViecVaVuViec
{
    public class CongViecVaVuViecVM
    {
        /// <summary>
        /// Category Code
        /// </summary>
        public string c { get; set; }
        /// <summary>
        /// UserGroupCode
        /// </summary>
        public string u { get; set; }
        /// <summary>
        /// Function Code
        /// </summary>
        public string f { get; set; }
        /// <summary>
        /// Function Array
        /// </summary>
        public string[] fs
        {
            get { return string.IsNullOrEmpty(f) ? new string[] { "" } : f.Split(','); }
        }
    }
}