﻿using System;
using System.Linq;

namespace GCSOFT.MVC.Web.ViewModels.jqGrid.cbGrid
{
    public static class CbGridExtensions
    {
        public static CbGridData ToCbGridData<T>(this IQueryable<T> query, CbgSettings cbg)
        {
            double count = query.Count();
            double total_pages = 0;
            if (count > 0)
                total_pages = Math.Ceiling(count / cbg.limit);
            if (cbg.page > total_pages)
                cbg.page = total_pages;
            int start = int.Parse((cbg.limit * cbg.page - cbg.limit).ToString());
            int _limit = int.Parse(cbg.limit.ToString());
            if (total_pages != 0)
                query = query.Skip(start).Take(_limit);
            return new CbGridData { page = cbg.page, total = total_pages, records = count, rows = query.ToList() };

        }
    }
}