﻿using System;
using System.Web.Mvc;

namespace GCSOFT.MVC.Web.ViewModels.jqGrid.cbGrid
{
    public class ComboboxModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            try
            {
                var request = controllerContext.HttpContext.Request;
                return new CbgSettings
                {
                    page = double.Parse(request["page"] ?? "1"),
                    limit = double.Parse(request["rows"] ?? "10"),
                    sidx = request["sidx"] ?? "",
                    sord = string.IsNullOrEmpty(request["sord"]) ? "" : request["sord"],
                    searchTerm = request["searchTerm"] ?? ""
                };
            }
            catch
            {
                return null;
            }
        }
    }
}