﻿using System.Web.Mvc;

namespace GCSOFT.MVC.Web.ViewModels.jqGrid
{
    [ModelBinder(typeof(GridModelBinder))]
    public class GridSettings
    {
        public bool IsSearch { get; set; }
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public string SortColumn { get; set; }
        public string SortOrder { get; set; }
        public string SearchField { get; set; }
        public string SearchString { get; set; }
        public string SearchOper { get; set; }

        public Filter Where { get; set; }
    }
}